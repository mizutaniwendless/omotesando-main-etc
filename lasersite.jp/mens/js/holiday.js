$(function() {
	// お盆や年末年始の休日を指定
	//例 : var holidays = [ '2016-07-08','2016-07-09'];
	var holidays = [];

	$("#datepicker,#datepicker02,#datepicker03").datepicker({
		//dateFormat: "yy年mm月dd日(D)",
		minDate: 0,
		showButtonPanel: true,
		beforeShowDay: function(date) {
			// 休日の判定
			for (var i = 0; i < holidays.length; i++) {
				var htime = Date.parse( holidays[i] );		// 休日を 'YYYY-MM-DD' から time へ変換
				var holiday = new Date();
				holiday.setTime(htime);						// 上記 time を Date へ設定

				// 祝日
				if (holiday.getYear() == date.getYear() &&
				holiday.getMonth() == date.getMonth() &&
				holiday.getDate() == date.getDate()) {
					return [false, 'holiday'];
				}
			}

			//定休日はreturnをfalseに変更

			// 日曜日
			if (date.getDay() == 0) {
				return [true, 'sunday'];
			}
			// 月曜日
			if (date.getDay() == 1) {
				return [true, ''];
			}
			// 火曜日
			if (date.getDay() == 2) {
				return [true, ''];
			}
			// 水曜日
			if (date.getDay() == 3) {
				return [false, ''];
			}
			// 木曜日
			if (date.getDay() == 4) {
				return [true, ''];
			}
			// 金曜日
			if (date.getDay() == 5) {
				return [true, ''];
			}
			// 土曜日
			if (date.getDay() == 6) {
				return [true, 'saturday'];
			}
			// 平日
			return [true, ''];
		},

		onSelect: function(dateText, inst) {
			$("#date_val").val(dateText);
		},

		onClose: function(dateText, inst) {
			$("#datepicker,#datepicker02,#datepicker03").blur();
		}

	});
});
