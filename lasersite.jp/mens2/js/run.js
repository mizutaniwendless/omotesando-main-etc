MJL.event.add(window, "load", function(event) {
    // MJL に関係するコードはこの中に記述
    MJL.enable.rollover("roll");
    MJL.enable.heightEqualizer("equalize");
    MJL.enable.heightEqualizer("equalize2", {groupBy: 2});
}, false);

// MJL と無関係なコードはこの先に記述
