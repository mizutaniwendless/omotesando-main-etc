﻿window.onload = function() {
	colorRows();
	initRollovers();
}

/* ============================================================================== colorRows - 'rowTint'
	Automatic coloured rows | Bite Size Standards
	http://bitesizestandards.com/bites/automatic-coloured-rows
*/
function colorRows() {
	var myTR = document.getElementsByTagName('tr');
	for (var i=0;i<myTR.length;i++) {
		if (i%2) {
			myTR[i].className = 'rowTint';
		}
	}
}

/* ============================================================================== initRollovers - 'imgover'
	Standards Compliant Rollover Script
	Author : Daniel Nolan
	http://www.bleedingego.co.uk/webdev.php
*/
function smartRollover() {
if(document.getElementsByTagName) {
var images = document.getElementsByTagName("img");

for(var i=0; i < images.length; i++) {
if(images[i].getAttribute("src").match("_off."))
{
images[i].onmouseover = function() {
this.setAttribute("src", this.getAttribute("src").replace("_off.", "_on."));
}
images[i].onmouseout = function() {
this.setAttribute("src", this.getAttribute("src").replace("_on.", "_off."));
}
}
}
}
}

if(window.addEventListener) {
window.addEventListener("load", smartRollover, false);
}
else if(window.attachEvent) {
window.attachEvent("onload", smartRollover);
}

/* ============================================================================== makeBlankLink - 'blankLink'

function makeBlankLink(){
	var allLink = document.getElementsByTagName('a');
	for(i=0; i<allLink.length; i++){
		if (allLink[i].className == 'blankLink'){
			allLink[i].target = '_blank';
		}
	}	
}
*/
