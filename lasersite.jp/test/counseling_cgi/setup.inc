#!/usr/bin/perl -w

############################################################
# 環境
############################################################
# メールコマンド(環境に合わせて変えてください)
$g_MailCmd = '/usr/sbin/sendmail';

############################################################
# 変更不可
############################################################
# 作成ファイルのパス
$g_DataFile = './data/data.csv';
# エラー表示判断格納ファイル
$g_ErrFile = './data/error.dat';
# エラーＨＴＭＬ
$g_ErrorHtmlFile = './error.html';
# 確認ＨＴＭＬ
$g_ConfirmHtmlFile = './confirm.html';
# 終了ＨＴＭＬ
$g_EndHtmlFile = './end.html';
# 店舗様に送るメールベース
$g_ToMailFile = './data/tomail.txt';
# 顧客様に送るメールベース
$g_FroMmailFile = './data/frommail.txt';

############################################################
# 設定
############################################################
# メール送信先（メールを飛ばす先のアドレスです）複数可（例：test1@cns-inc.co.jp,test2@cns-inc.co.jp）
$s_MailTo = 'info@clubjdc.com';

#メール送信先（心斎橋店） 
$s_Mail_Shinsaibashi = 'jdcshinsaibashi@wonder.ocn.ne.jp';
#メール送信先（梅田店）
$s_Mail_Umeda = 'bssumeda@clock.ocn.ne.jp';
#メール送信先（京橋店）
$s_Mail_Kyobashi = 'bsskyobashi@extra.ocn.ne.jp';
#メール送信先（堺東店）
$s_Mail_Sakai = 'jdc-sakai@air.ocn.ne.jp';
#メール送信先（神戸店）
$s_Mail_Kobe = 'jdc-kobe@air.ocn.ne.jp';
#メール送信先（京都店）
$s_Mail_Kyoto = 'jdc-kyoto@galaxy.ocn.ne.jp';
#メール送信先（奈良店）
$s_Mail_Nara = 'jdc-nara@herb.ocn.ne.jp';
#メール送信先（銀座メルサ店）
$s_Mail_Ginza = 'ginzamerusa@herb.ocn.ne.jp';
#メール送信先（西銀座デパート店）
$s_Mail_Ginza2 = 'jdc-ginza@air.ocn.ne.jp';
#メール送信先（目黒店）
$s_Mail_Meguro = 'bssmeguro@song.ocn.ne.jp';


# 店舗件名（店舗に届くメールの件名です）
$g_Subject = '脱毛無料カウンセリング';
# 顧客件名（顧客に届くメールの件名です）
$g_SubjectK = '【JDC 日本脱毛センター】脱毛無料カウンセリングにお申し込みいただきありがとうございました';
# メール送信後のＵＲＬ（メール送信後のＵＲＬです）
$g_EndUrl = 'http://www.clubjdc.com/info/counseling.html';
# 戻り先のＵＲＬ
$g_ReturnUrl = 'http://www.clubjdc.com/info/counseling.html';

# 作成ファイルへのデータ書き込み設定（書き込む場合は「1」、書き込まない場合は「0」をいれてください）
$g_DataSet = 0;

# 確認画面に有無（確認画面が必要ならば「1」を、いらないならば「0」をいれてください。）
$g_Kakunin = 1;
# 送信後の処理（終了画面が必要ならば「1」を、送信後ＵＲＬに飛ばすならば「0」をいれてください。）
$g_End = 1;

############################################################
# 色設定
############################################################
# テーブルボーダーカラー
$g_TableBorderColor = '#000000';
# 項目のバックカラー
$g_LavelBackColor ='#ffffff';
# 値のバックカラー
$g_DataBackColor ='#ffffff';
# 項目の文字色
$g_LavelTextColor ='#000000';
# 値の文字色
$g_DataTextColor ='#000000';

true;
