function getScrollLeft() { 
 if ((navigator.appName.indexOf("Microsoft Internet Explorer",0) != -1)) {

  /*WinIE6標準モード稼動オブジェクト差異対応
    WinIE6標準モードとそれ以外で、それぞれ稼動しているオブジェクトを取り出す*/
  docele=(document.compatMode=="CSS1Compat")?document.documentElement:document.body;

  return docele.scrollLeft;

 } else if (window.pageXOffset) {
  return window.pageXOffset;
 } else {
  return 0;
 }
}

function getScrollTop() { 
 if ((navigator.appName.indexOf("Microsoft Internet Explorer",0) != -1)) {

  /*WinIE6標準モード稼動オブジェクト差異対応
    WinIE6標準モードとそれ以外で、それぞれ稼動しているオブジェクトを取り出す*/
  docele=(document.compatMode=="CSS1Compat")?document.documentElement:document.body;
  
  return docele.scrollTop;

 } else if (window.pageYOffset) {
  return window.pageYOffset;
 } else {
  return 0;
 }
}

var pageScrollTimer;

function pageScroll(toX,toY,frms,cuX,cuY) { 
 if (pageScrollTimer) clearTimeout(pageScrollTimer);
 if (!toX || toX < 0) toX = 0;
 if (!toY || toY < 0) toY = 0;
 if (!cuX) cuX = 0 + getScrollLeft();
 if (!cuY) cuY = 0 + getScrollTop();
 if (!frms) frms = 6;

 if (toY > cuY && toY > (getAnchorPosObj('end','enddiv').y) - getInnerSize().height) toY = (getAnchorPosObj('end','enddiv').y - getInnerSize().height) + 1;
 cuX += (toX - getScrollLeft()) / frms; if (cuX < 0) cuX = 0;
 cuY += (toY - getScrollTop()) / frms;  if (cuY < 0) cuY = 0;
 var posX = Math.floor(cuX);
 var posY = Math.floor(cuY);
 window.scrollTo(posX, posY);

 if (posX != toX || posY != toY) {
  pageScrollTimer = setTimeout("pageScroll("+toX+","+toY+","+frms+","+cuX+","+cuY+")",16);
 }
}

function jumpToPageTop() { 
  pageScroll(0,0,3);
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}