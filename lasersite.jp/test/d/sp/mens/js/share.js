// 読み込み後アドレスバー非表示
function doScroll() { if (window.pageYOffset === 0) { window.scrollTo(0,1); } }
window.onload = function() { setTimeout(doScroll, 100); }

// UA判定 Android振り分け
function is_mobile () {
  var useragents = [
    'Android',        // 1.5+ Android
    'dream',          // Pre 1.5 Android
    'CUPCAKE'        // 1.5+ Android
    ];
  var pattern = new RegExp(useragents.join('|'), 'i');
  return pattern.test(navigator.userAgent);
}
if (is_mobile()) {
  var link = document.createElement('link');
  link.rel = 'stylesheet';
  link.type = 'text/css';
  link.href = './css/android.css';
  document.getElementsByTagName('head')[0].appendChild(link);
}

$(function(){

$(".accordion p").click(function(){
	$(this).next("ul").slideToggle();
	$(this).children("span").toggleClass("open");	
});
	
});


/* ============================================================================== smoothscroll
*/
$(function(){
   // #で始まるアンカーをクリックした場合に処理
   $('a[href^=#]').click(function() {
      // スクロールの速度
      var speed = 400; // ミリ秒
      // アンカーの値取得
      var href= $(this).attr("href");
      // 移動先を取得
      var target = $(href == "#" || href == "" ? 'html' : href);
      // 移動先を数値で取得
      var position = target.offset().top;
      // スムーススクロール
      $('body,html').animate({scrollTop:position}, speed, 'swing');
      return false;
   });
});

/* ============================================================================== dropdownMenu
*/
$(window).load(function() {
$("#btn_menu").click(function(){
 if($(this).attr("src") == "img/common/btn_menu.png"){
 	$(this).attr("src", "img/common/btn_menu_close.png");
 }else{
 	$(this).attr("src", "img/common/btn_menu.png");
 }
 });
});

$(function(){
    $(".menu").css("display","none");
    $(".button-toggle").on("click", function() {
		$(this).toggleClass("active");
        $(".menu").slideToggle();
    });
});




jQuery(function($){
$("#menu_close").click(function(){
    $("#btn_menu").trigger("click");
  });
});


/* ============================================================================== divClick
*/
$(function(){
	$("#realtimenav li").click(function(){
		if($(this).find("a").attr("target")=="_blank"){
			window.open($(this).find("a").attr("href"), '_blank');
		}else{
			window.location=$(this).find("a").attr("href");
		}
		return false;
	});
});


/* ============================================================================== fix_Box
*/

$(function() {
    var showFlag = false;
    var fixBox = $('#fix_Box');    
    fixBox.css('bottom', '-80px');
    var showFlag = false;

    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            if (showFlag == false) {
                showFlag = true;
                fixBox.stop().animate({'bottom' : '0'}, 200); 
            }
        } else {
            if (showFlag) {
                showFlag = false;
                fixBox.stop().animate({'bottom' : '-80px'}, 200); 
            }
        }
    });
});