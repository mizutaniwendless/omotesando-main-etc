<?php
ini_set('display_errors','Off');
ini_set('display_startup_errors',FALSE);
ini_set('log_errors',TRUE);
//define('__DFORM_DEBUGLOGFILENAME' , 'debug.log'); // デバッグログを出すのであればこの定数にファイル名を定義する
        
include_once( dirname(__FILE__) ."/dform/lib/dform.class.php" );

class dformEx extends dform {
	function validateEx($isOK) {
		return $isOK;
	}
}

debuglog('y_01.php:step1');

$dform = new dformEx('women_y01');
debuglog('y_01.php:step2');
$dform->suffix_html = '.html';
$dform->main();
debuglog('y_01.php:step3');
