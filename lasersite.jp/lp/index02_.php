<?php
ini_set('display_errors','Off');
ini_set('display_startup_errors',FALSE);
ini_set('log_errors',TRUE);
//define('__DFORM_DEBUGLOGFILENAME' , 'debug.log'); // デバッグログを出すのであればこの定数にファイル名を定義する
        
include_once( dirname(__FILE__) ."/dform/lib/dform.class.php" );

class dformEx extends dform {
	function validateEx($isOK) {
		return $isOK;
	}
}

debuglog('l_02_.php:step1');

$dform = new dformEx('women_l02_');
debuglog('l_02_.php:step2');
$dform->suffix_html = '.html';
$dform->main();
debuglog('l_02_.php:step3');
