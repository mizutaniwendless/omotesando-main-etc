<?php

return array(

# 宛先 管理者メールアドレス
'email' => 'info@tk-honin.com,tk-honin@mrfusion.co.jp',

# 件名
'subject' => '資料請求申込み通知',

# 本文
'message' => '
資料請求より申込がありました。

申込日時 : {entry_date}

【お名前】
{name}

【フリガナ】
{kana}

【郵便番号】
{zip1}-{zip2}

【都道府県】
{pref}

【市区町村】
{addr1}

【丁目番地】
{addr2}

【電話番号】
{tel}

【メールアドレス】
{email}

-- 
表参道スキンクリニック
Tokyo Cosmetic Sergery

',

);

