<?php

return array(

# 宛先 管理者メールアドレス
'email' => 'info@tk-honin.com,tk-honin@mrfusion.co.jp',

# 件名
'subject' => '無料メール相談申込み通知',

# 本文
'message' => '
無料メール相談より申込がありました。

申込日時 : {entry_date}

【お名前】
{name}

【フリガナ】
{kana}

【電話番号】
{tel}

【メールアドレス】
{email}

【ご相談箇所】
{parts}

【ご相談内容】
{message}

-- 
表参道スキンクリニック
Tokyo Cosmetic Sergery

',

);

