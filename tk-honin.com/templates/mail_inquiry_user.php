<?php

return array(

# 宛先 ユーザーが入力したメールアドレス

# 件名
'subject' => '無料メール相談申込み承りました',


# 本文
'message' => '
{name} 様

お申し込みありがとうございます。
下記内容にて無料メール相談のお申込みを承りました。
------------------------------------------------------------------

申込日時 : {entry_date}

【お名前】
{name}

【フリガナ】
{kana}

【電話番号】
{tel}

【メールアドレス】
{email}

【ご相談箇所】
{parts}

【ご相談内容】
{message}

------------------------------------------------------------------
表参道スキンクリニック
〒150-0001 東京都渋谷区神宮前5-9-13喜多重ビル4階
フリーダイヤル0120‐334‐270
',

);
