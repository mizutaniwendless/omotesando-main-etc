<?php

return array(

# 宛先 管理者メールアドレス
'email' => 'tokyobiyougeka@yahoo.co.jp,tk-honin@mrfusion.co.jp',

# 件名
'subject' => 'エントリーフォーム申込み通知',

# 本文
'message' => '
エントリーフォームより申込がありました。

申込日時 : {entry_date}

【お名前】
{name}

【フリガナ】
{kana}

【年齢】
{age} 歳

【電話番号】
{tel}

【連絡先メールアドレス】
{email}

【希望職種】
{occupation}

【志望動機】
{application}

【その他】
{other}

-- 
表参道スキンクリニック
Tokyo Cosmetic Sergery

',

);

