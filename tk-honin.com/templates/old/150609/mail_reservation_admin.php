<?php

return array(

# 宛先 管理者メールアドレス
'email' => 'info@omotesando-sc.com',

# 件名
'subject' => '無料カウンセリング予約申込み通知',

# 本文
'message' => '
無料カウンセリング予約より申込がありました。

申込日時 : {entry_date}

【お名前】
{name}

【フリガナ】
{kana}

【年齢】
{age} 歳

【郵便番号】
{zip1}-{zip2}

【都道府県】
{pref}

【市区町村】
{addr1}

【丁目番地】
{addr2}

【電話番号】
{tel1}-{tel2}-{tel3}

【メールアドレス】
{email}

【当クリニックをどこで知りましたか？】
{reason}

【ご相談箇所】
{parts}

【ご相談内容】
{message}

【ご相談希望日時】
第一希望 : {date1} {time1}
第二希望 : {date2} {time2}
第三希望 : {date3} {time3}

-- 
表参道スキンクリニック
Tokyo Cosmetic Sergery

',

);

