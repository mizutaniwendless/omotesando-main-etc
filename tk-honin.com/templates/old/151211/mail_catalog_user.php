<?php

return array(

# 宛先 ユーザーが入力したメールアドレス

# 件名
'subject' => '資料請求申込み承りました',


# 本文
'message' => '
{name} 様

お申し込みありがとうございます。
下記内容にて資料請求のお申込みを承りました。

申込日時 : {entry_date}

【お名前】
{name}

【フリガナ】
{kana}

【郵便番号】
{zip1}-{zip2}

【都道府県】
{pref}

【市区町村】
{addr1}

【丁目番地】
{addr2}

【電話番号】
{tel}

【メールアドレス】
{email}


------------------------------------------------------------------
表参道スキンクリニック
Tokyo Cosmetic Sergery
〒150-0001 東京都渋谷区神宮前5-9-13喜多重ビル4階
フリーダイヤル0120‐334‐270
',

);
