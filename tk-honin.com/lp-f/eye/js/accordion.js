jQuery(document).ready(function($){

/* ==========================================================

　　　　アコーディオン

========================================================== */
	$(".accordion").next().hide();
	$(".accordion").click(function () {
		$(this).next().slideToggle();
		$(this).toggleClass('on');
		$(this).next().toggleClass('open_t');
    });

});