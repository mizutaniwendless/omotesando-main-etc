<?php
ini_set('display_errors','Off');
ini_set('display_startup_errors',FALSE);
ini_set('log_errors',TRUE);
define('__DFORM_DEBUGLOGFILENAME' , ''); // デバッグログを出すのであればこの定数にファイル名を定義する
//-------------------------------------------------------------------------
// DForm -- design-free mail form system
// 
// (C) 2011 en-pc service. All Rights Reserved.
// This code cannot be redistributed without permission from www.en-pc.jp.
//-------------------------------------------------------------------------
// DForm Call Program
//-------------------------------------------------------------------------
include_once("dform/lib/dform.class.php" );

class dformEx extends dform {
    function validateEx($isOK) {
        if($this->next == 'confirm'){
            // 確認ページ遷移時に、第1希望日時の入力チェックを行う
            if ( (@$this->reqdata['day1_req']['value'] == '') || (@$this->reqdata['day1_req_period']['value'] == '') ) {
                $this->reqdata['day1_date']['error'][] = 'req';
                $isOK = FALSE;
            }
        }
        return $isOK;
    }
}

$dform = new dformEx('l_02');
$dform->main();
