<?php
ini_set('display_errors','Off');
ini_set('display_startup_errors',FALSE);
ini_set('log_errors',TRUE);
define('__DFORM_DEBUGLOGFILENAME' , ''); // デバッグログを出すのであればこの定数にファイル名を定義する
//-------------------------------------------------------------------------
// DForm -- design-free mail form system
// 
// (C) 2011 en-pc service. All Rights Reserved.
// This code cannot be redistributed without permission from www.en-pc.jp.
//-------------------------------------------------------------------------
// DForm Call Program
//-------------------------------------------------------------------------
include_once("dform/lib/dform.class.php" );

class dformEx extends dform {
	function validateEx($isOK) {
		return $isOK;
	}
}


$dform = new dformEx('l_01');
$dform->main();
