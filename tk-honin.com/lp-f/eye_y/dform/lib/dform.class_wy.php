<?php
//-------------------------------------------------------------------------
// DForm -- design-free mail form system
// 
// (C) 2011 en-pc service. All Rights Reserved.
// This code cannot be redistributed without permission from www.en-pc.jp.
// http://www.en-pc.jp/
//-------------------------------------------------------------------------
// DForm Main Library
//-------------------------------------------------------------------------
//include_once("qdmail.php");
// includeパス設定(主にPEAR用)
set_include_path( dirname(__FILE__) . '/pear' . PATH_SEPARATOR . get_include_path() );
require_once('Mail.php');
require_once("Mail/mime.php");
require_once("Net/DNS.php");
include_once("qdmail.php");


class dform {
	var $REQUEST;                     // GET+POST
	var $templatedir;                 // テンプレート設置ディレクトリ
	var $sessionExpire     = 10800;   // セッションデータ保存期間（秒数で指定）
	var $sessionExpireRate = 100;     // セッションデータ破棄実行確率（100であれば100回に1回行う）
	var $sessdir;                     // セッションデータ保存ディレクトリ
	var $sessionBaseName;             // セッション変数ベース名
	var $next;                        // 次に表示させたいテンプレート
	var $formname;                    // フォーム名
	var $reqdata;                     // 画面入力値を保存する変数
	var $templateSet;                 // テンプレートファイル一覧
	var $inputIdx          = -1;      // 入力テンプレートの最終位置
	var $confirmIdx        = -1;      // 確認テンプレートの最終位置
	var $completeIdx       = -1;      // 完了テンプレート位置
	var $inputText;                   // 入力テンプレートを意味するテキスト
	var $confirmText;                 // 確認テンプレートを意味するテキスト
	var $completeText;                // 完了テンプレートを意味するテキスト
	var $siteMailText;                // サイト送信テンプレートを意味するテキスト
	var $userMailText;                // 利用者送信テンプレートを意味するテキスト
	var $validatePHP;                 // 入力チェックバリデート処理ファイル名
	var $suffix_html       = '.html'; // テンプレートファイル(HTML)の拡張子    2014.07.04 デフォルトを.tpl→.htmlに変更
	var $suffix_mail       = '.txt';  // テンプレートファイル(メール)の拡張子
	var $suffix_log        = '.csv';  // ログファイルの拡張子
	var $suffix_seq        = '.seq';  // 連番管理ファイルの拡張子
	var $templatename_glue = '-';     // テンプレート名と状態(next値)をつなげる文字列
	var $name_delimiter    = '_';     // フォームのname値を分ける文字列(分けたものはバリデートに使われます)
	var $noTemplatesUrl    = '';      // テンプレート名未指定時にリダイレクトさせるURL
	var $internal_encoding = 'UTF-8';
	var $log_encoding      = 'sjis-win';
	var $mb_language       = 'japanese';
	var $request_encoding;
	var $template_encoding;
	var $logfile           = '';      // メール送信ログファイル名（内部で使用：ここで定義してはいけない）
	var $seqfile           = '';      // 連番管理ファイル名（内部で使用：ここで定義してはいけない）
	var $emailAttachFile   = TRUE;    // Uploadファイルをメール添付するかどうか(TRUE:添付する)
	var $savedAttachDir    = '';      // Uploadファイルをサーバ上に保存する場合のディレクトリ（最後 / で。保存しない時は''にする)
	var $savedAttachPrefix = '';      // Uploadファイルをサーバ上に保存する場合のファイル名プレフィックス
	var $errors            = array(); // バリデートエラー格納変数
	var $initialValue      = array(); // フォーム初画面表示時にセットするデフォルト値(key値=name,value値=値)
	var $sendmailmode      = 'qdmail';// メール送信ライブラリ(pear or qdmail) 2014.07.04 pearは色々問題あるためデフォルトをqdmailに変更
	// 以下、pearメール利用時
	var $mailcharset       = 'ISO-2022-JP'; // メール送信時の文字コードセット
	var $mailencoding      = 'base64';      // メール送信時の文字エンコーディング
	var $mailmethod        = 'mail';        // メール送信方式(mail,sendmail,smtp(未対応))
	var $crlf              = "\r\n";        // メール送信時の改行コード(通常は\r\nでOK,ダメな場合は\nを試す)
	// 以下、メールドメイン存在チェックに関する設定
	// ドメインチェック時、最初に使用するネームサーバを指定(未指定時は PHPのcheckdnsrr()関数を使用.
	// 値指定時は、指定サーバを使って該当ドメインのネームサーバを最初に調べ、次にそのネームサーバに対し問合せを行う。つまりなるべくローカルキャッシュを用いないようにしている。
	var $nameserver        = "8.8.8.8";     // Google Public DNS サーバ

	//2013.09.25 Qdmail(ワードラップ)の設定
	//SMTPプロトコルは1行1024文字のため、sendmail等のデフォルト設定は1024文字で強制的に改行が入る。
	//しかしQdmailでは回避不可能なので、Qdmailのインテリジェントワードラップを使用する(http://hal456.net/qdmail/wordwrap)
	var $qd_wrapAllow      = true;        // ワードラップの使用(使用時：true、使用しないとき：false)
	var $qd_wrapLength     = 0;           // 改行文字数(全角文字の文字数を指定する。0の場合は「文字列の幅」で改行します。)


	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	// コンストラクタ
	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	function dform($formname='') {

		// magic_quotes_gpcを無効化する
		$this->disable_magic_quotes_gpc();

		// テンプレート名未指定時にリダイレクトさせるURLのデフォルト値として
		// サイト/を設定
		$this->noTemplatesUrl   = "http://{$_SERVER['SERVER_NAME']}/";
		$this->inputText        = 'input_wy';
		$this->confirmText      = 'confirm';
		$this->completeText     = 'complete_wy';
		$this->siteMailText     = 'sitemail';
		$this->userMailText     = 'usermail';
		$this->validatePHP      = 'validate.php';
		$this->sessionBaseName  = 'dform';
		// テンプレートディレクトリ
		$this->templatedir      = dirname(__FILE__) . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR ;

		// セッションデータ保存先ディレクトリ（アップロードファイルの格納先も兼ねる）
		$this->sessdir          = realpath( dirname(__FILE__) . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'sessdata' );
		$this->formname         = $formname;
		// Cookieを除外したいため以下のようにする
		$this->REQUEST          = array_merge($_GET, $_POST);
		// 送信コード取得
		$this->request_encoding = $this->detect_encoding_ja( print_r( $this->REQUEST , TRUE ) );
		// 入力値初期化
		$this->reqdata          = array();
	}


	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	// セッション保存データクリア
	// セッションＩＤ指定時は、期間関係なく該当セッションＩＤ分だけ削除
	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	function sessionExpire($session_id=NULL) {

		//---------------------------------------------------------------------------
		// 以下は、該当セッションIDのものを無条件削除する処理です
		//---------------------------------------------------------------------------
		if ( $session_id !== NULL ) {
			$len_session  =  strlen($session_id);

			if ($handle = opendir($this->sessdir)) {
				while (false !== ( $filename = readdir($handle) )) {
					if ( substr( $filename , 0 , $len_session ) == $session_id ) {
						unlink( $this->sessdir . $filename );
					}
				}
			}
			closedir($handle);
			return;
		}

		//---------------------------------------------------------------------------
		// 以下は、全セッションＩＤについて、保存期限を過ぎたものを削除させる処理です
		//---------------------------------------------------------------------------
		// 実施判断
		if ( mt_rand(0,$this->sessionExpireRate) != 0 ) return;
		
		// 指定時刻より古いものを削除
		if ($handle = opendir($this->sessdir)) {
			while (false !== ($filename = readdir($handle))) {
				$filepath = $this->sessdir . DIRECTORY_SEPARATOR . $filename;
				if ( !is_dir( $filepath ) && ( filemtime($filepath) + $this->sessionExpire ) < time() ) unlink($filepath);
			}
		}
		closedir($handle);
	}



	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	// dform内の関数群を個別に使う場合の初期化処理
	// 　コントローラを機能させず、各種変数のセットのみ行います。
	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	function build_for_tool() {

		//-----------------------------
		// main() の一部分コピー
		//-----------------------------
		// 初期化
		mb_language($this->mb_language);
		mb_internal_encoding($this->internal_encoding);

		// セッションデータ保存先が設定されている場合のみ、それに関する処理を実施する
		if ( $this->sessdir != '' ) {
			// セッションデータ保存先設定
			session_save_path( $this->sessdir );
			// 有効期限切れのセッション保存データクリア
			$this->sessionExpire();
		}

		// セッション開始
		if ( session_id() == '' ) {
			register_shutdown_function('session_write_close');  //2014.07.04 session.save_handlerをデフォルト以外(例：memcache)にする場合、必ずセッション開始前にこの記述が必要
			session_start();
		}
		ob_start();

		// 必須項目値の読み込み
		$this->next          = @$this->REQUEST['next'];

		// 該当セッションの保存データを削除
		if ( $this->sessdir != '' ) {
			$this->sessionExpire( session_id() );
		}

		//-----------------------------
		// displayInnit() の一部分コピー
		//-----------------------------
		// 現在の表示位置を初期化
		$_SESSION[$this->sessionBaseName][$this->formname]['templatePosition'] = 0;

		// セッション保存値を初期化
		$_SESSION[$this->sessionBaseName][$this->formname]['formdata'] = array();

		// フォーム初期値をセット
		foreach ( $this->initialValue as $key => $value ) {
			$_SESSION[$this->sessionBaseName][$this->formname]['formdata'][$key] = array(
				'name'  => $key    ,  // name値
				'value' => $value  ,  // value値
				'error' => array()    // エラー情報
			);
		}

		// テンプレートを取得(ファイル有無チェックは行いません)
		if ( !$this->getTemplateSet($this->formname,TRUE) ) {
			$this->noTemplates();
			exit;
		}
	}


	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	// メイン処理
	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	function main() {

		// 初期化
		mb_language($this->mb_language);
		mb_internal_encoding($this->internal_encoding);

		// セッションデータ保存先が設定されている場合のみ、それに関する処理を実施する
		if ( $this->sessdir != '' ) {
			// セッションデータ保存先設定
			session_save_path( $this->sessdir );
			// 有効期限切れのセッション保存データクリア
			$this->sessionExpire();
		}

		// セッション開始
		if ( session_id() == '' ) {
			register_shutdown_function('session_write_close');  //2014.07.04 session.save_handlerをデフォルト以外(例：memcache)にする場合、必ずセッション開始前にこの記述が必要
			session_start();
		}
		ob_start();

		// 必須項目値の読み込み
		$this->next          = @$this->REQUEST['next'];

		// セッション変数そのものが無い場合は初期化するよう修正
		// 2014/10/15 入力ページを経ずいきなり確認画面に行きたい場合（dform範疇外の入力フォームからの送信を想定）、
		// next未指定の状態がないためセッション変数の初期化タイミングがありませんでした。そのため、セッション変数初期化の
		// タイミングをnext未指定時ではなく、セッション変数そのものが無い場合、というように修正しました
		if ( !isset($_SESSION[$this->sessionBaseName]) ) {
			$this->initSession();
		}


		//----------------------------------------------------------------
		// 初回表示時(next未指定時)
		//----------------------------------------------------------------
		if     ( $this->next == ''     ) {
			// 該当セッションの保存データを削除
			if ( $this->sessdir != '' ) {
				$this->sessionExpire( session_id() );
			}
			$this->displayInit();
			exit;
		}

		//----------------------------------------------------------------
		// データ送信時(next指定時)
		//----------------------------------------------------------------
		else {
			// テンプレートを取得
			if ( !$this->getTemplateSet($this->formname) ) {
				$this->noTemplates();
				exit;
			}
			// 現状のテンプレート位置を取得
			$curr_templatePosition = intval(@$_SESSION[$this->sessionBaseName][$this->formname]['templatePosition']);
			// 前回完了画面表示時で、next値が「complete」の場合（完了画面表示後リロード時：イレギュラー対応）
			if ( $curr_templatePosition == $this->completeIdx ) {
				if ( $this->next == $this->completeText ) {
					$this->display();
					exit;
				}
				// それ以外はデータを初期化する
				else {
					// 2014/10/15 入力ページを経ずいきなり確認画面に行きたい場合（dform範疇外の入力フォームからの送信を想定）、
					// フォーム送信完了状態であった場合、再度いきなり確認画面に飛ばすとここの条件に合致してしまいます。
					// そうなると今度はバリデーションを行わなくなってしまう問題が起きました。そのため、ここではセッション変数を
					// 初期化するにとどめ（テンプレート位置も初期化してます）、バリデーションなど通常処理を走らせるよう修正しました。
					$this->initSession();
//					$this->displayInit();
//					exit;
				}
			}

			// 次に表示したいページのテンプレート位置を取得
			$next_templatePosition = -1;
			$wk_count              =  0;
			foreach ( $this->templateSet as $wk_position => $wk_templateName ) {
				if ( is_numeric($wk_position) ) {
					$wk_count += 1;
					if ( $wk_templateName == ($this->formname . $this->templatename_glue . $this->next . $this->suffix_html) ) $next_templatePosition = intval($wk_position);
				}
			}
			// 次に表示したいページが現状より２つ先のテンプレートを指定している場合は、不正とみなし現状値に戻す
			if ( $curr_templatePosition+2 <= $next_templatePosition ) {
				$next_templatePosition = $curr_templatePosition;
				$this->next            = $this->templateSet[$next_templatePosition];
			}

			// 次に表示したいページテンプレートが見つからなかった場合は、現状値に戻す
			if ( $next_templatePosition == -1 ) {
				$next_templatePosition = $curr_templatePosition;
				$this->next            = $this->templateSet[$next_templatePosition];
			}


			// フォーム値を取得
			if ( $this->getInput() ) { // チェックＯＫ時

				// 現状が入力の場合&フォーム送信値がゼロ&先に進もうとした場合は不正とみなし、現状値に戻す
				if (  $curr_templatePosition <= $this->inputIdx
				   && count($this->reqdata) == 0
				   && $curr_templatePosition <  $next_templatePosition ) {
					$next_templatePosition = $curr_templatePosition;
					$this->next            = $this->templateSet[$next_templatePosition];
				}

				// フォーム値をセッション変数に格納
				foreach ( $this->reqdata as $key => $value ) $_SESSION[$this->sessionBaseName][$this->formname]['formdata'][$key] = $value;

				// 完了画面
				if ( $next_templatePosition == $this->completeIdx ) {
					
					// 連番管理ファイルカウントアップ
					if ( $this->seqfile != '' ) $wk_sequence = $this->countupSequence();
					else                        $wk_sequence = '';
					// 連番情報をセッション変数に格納
					$_SESSION[$this->sessionBaseName][$this->formname]['formdata']['__sys_sequence'] = array('value' => $wk_sequence);

					// その他システム関連情報をセット
					$this->setSystemInfo();

					// ファイル保存
					if ( $this->savedAttachDir != '' ) $this->saveAttachedFiles();

					//サイト側へメール送信を行う
					if ( isset($this->templateSet[$this->siteMailText]) ) {
						$this->sendmailFromTemplate( $this->formname . $this->templatename_glue . $this->siteMailText . $this->suffix_mail );
					}
					//利用者側へメール送信を行う
					if ( isset($this->templateSet[$this->userMailText]) ) {
						$this->sendmailFromTemplate( $this->formname . $this->templatename_glue . $this->userMailText . $this->suffix_mail );
					}
				}

				// 画面表示
				$_SESSION[$this->sessionBaseName][$this->formname]['templatePosition'] = $next_templatePosition;
				$this->display();

				// 完了画面の時だけ、セッション変数（フォーム値のみ）を初期化する
				if ( $next_templatePosition == $this->completeIdx ) {
					$_SESSION[$this->sessionBaseName][$this->formname]['formdata'] = array();
					// 該当セッションの保存データを削除
					if ( $this->sessdir != '' ) {
						$this->sessionExpire( session_id() );
					}
				}
				exit;
			}

			else { // チェックＮＧ時
				//2015.02.12 チェックＮＧ時は表示位置を初期化する(エラーなのに確認画面へ遷移してしまうことを防ぐ)
				// 現在の表示位置を初期化
				$_SESSION[$this->sessionBaseName][$this->formname]['templatePosition'] = 0;

				// 画面表示
				$this->display();
				exit;
			}
		}
	}


	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	// 初回表示処理
	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	function displayInit() {

		// セッション変数の初期化
		$this->initSession();

		// テンプレートを取得
		if ( !$this->getTemplateSet($this->formname) ) {
			$this->noTemplates();
			exit;
		}

		// 画面表示
		$this->display();
		exit;
	}


	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	// セッション変数の初期化
	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	function initSession() {

		// 現在の表示位置を初期化
		$_SESSION[$this->sessionBaseName][$this->formname]['templatePosition'] = 0;

		// セッション保存値を初期化
		$_SESSION[$this->sessionBaseName][$this->formname]['formdata'] = array();

		// フォーム初期値をセット
		foreach ( $this->initialValue as $key => $value ) {
			$_SESSION[$this->sessionBaseName][$this->formname]['formdata'][$key] = array(
				'name'  => $key    ,  // name値
				'value' => $value  ,  // value値
				'error' => array()    // エラー情報
			);
		}
	}


	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	// テンプレート未指定時の処理
	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	function noTemplates() {
		header('Location: '. $this->noTemplatesUrl);
		exit;
	}


	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	// テンプレートファイル群取得
	// $force が TRUEを指す場合は、途中でテンプレートが無くても処理を続けます
	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	function getTemplateSet($formname,$force=FALSE) {

		// 初期化
		$this->templateSet = array();
		$this->completeIdx = -1;
		$template_prefix   = $this->templatedir . $formname;

		//-------------------------------------------------------
		// 入力テンプレート
		//-------------------------------------------------------
		$idx = 0;
		// 最初の入力テンプレートを探す
		if ( is_file($template_prefix . $this->templatename_glue . $this->inputText . $this->suffix_html) ) {
			$this->templateSet[$idx] = $formname . $this->templatename_glue . $this->inputText . $this->suffix_html;
			$this->inputIdx          = $idx;
			$idx++;
		}

		// ２番目以降の入力テンプレートを探す
		$flg_end = FALSE;
		do {
			if ( is_file($template_prefix . $this->templatename_glue . $this->inputText . sprintf("%d",$idx+1) .$this->suffix_html) ) {
				$this->templateSet[$idx] = $formname . $this->templatename_glue . $this->inputText . sprintf("%d",$idx+1) . $this->suffix_html;
				$this->inputIdx          = $idx;
				$idx++;
			}
			else {
				$flg_end = TRUE;
			}
		} while ( $flg_end == FALSE );

		// 入力テンプレートが無ければエラー
		if ( $idx == 0 && !$force ) return FALSE;


		//-------------------------------------------------------
		// 確認テンプレート
		//-------------------------------------------------------
		$idx2 = 0;
		// 最初の入力テンプレートを探す
		if ( is_file($template_prefix . $this->templatename_glue . $this->confirmText . $this->suffix_html) ) {
			$this->templateSet[$idx] = $formname . $this->templatename_glue . $this->confirmText . $this->suffix_html;
			$this->confirmIdx        = $idx;
			$idx++;
			$idx2++;
		}

		// ２番目以降の確認テンプレートを探す
		$flg_end = FALSE;
		do {
			if ( is_file($template_prefix . $this->templatename_glue . $this->confirmText . sprintf("%d",$idx2+1) . $this->suffix_html) ) {
				$this->templateSet[$idx] = $formname . $this->templatename_glue . $this->confirmText . sprintf("%d",$idx2+1) . $this->suffix_html;
				$this->confirmIdx        = $idx;
				$idx++;
				$idx2++;
			}
			else {
				$flg_end = TRUE;
			}
		} while ( $flg_end == FALSE );


		//-------------------------------------------------------
		// 完了テンプレート
		//-------------------------------------------------------
		if ( is_file($template_prefix . $this->templatename_glue . $this->completeText . $this->suffix_html) ) {
			$this->templateSet[$idx] = $formname . $this->templatename_glue . $this->completeText . $this->suffix_html;
			$this->completeIdx       = $idx;
		}

		//-------------------------------------------------------
		// サイト宛メールテンプレート
		//-------------------------------------------------------
		if ( is_file($template_prefix . $this->templatename_glue . $this->siteMailText . $this->suffix_mail) ) {
			$this->templateSet[$this->siteMailText] = $formname . $this->templatename_glue . $this->siteMailText . $this->suffix_mail;
		}

		//-------------------------------------------------------
		// ユーザ宛メールテンプレート
		//-------------------------------------------------------
		if ( is_file($template_prefix . $this->templatename_glue . $this->userMailText . $this->suffix_mail) ) {
			$this->templateSet[$this->userMailText] = $formname . $this->templatename_glue . $this->userMailText . $this->suffix_mail;
		}
		
		//-------------------------------------------------------
		// ログファイル（書き込み可ならメール送信時に作る）
		//-------------------------------------------------------
		if ( is_writable($template_prefix . $this->suffix_log) )  $this->logfile = $template_prefix . $this->suffix_log;
		else                                                      $this->logfile = '';

		//-------------------------------------------------------
		// 連番管理ファイル（書き込み可なら完了時に更新する）
		//-------------------------------------------------------
		if ( is_writable($template_prefix . $this->suffix_seq) )  $this->seqfile = $template_prefix . $this->suffix_seq;
		else                                                      $this->seqfile = '';

		//-------------------------------------------------------
		// カスタムバリデートプログラム
		//-------------------------------------------------------
		if ( is_file($template_prefix . $this->templatename_glue . $this->validatePHP) ) {
			$this->templateSet[$this->validatePHP] = $template_prefix . $this->templatename_glue . $this->validatePHP;
		}

		return TRUE;
	}

	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	// テンプレート出力
	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	function display() {
		$templateName = $this->templateSet[$_SESSION[$this->sessionBaseName][$this->formname]['templatePosition']];

		$html = $this->get_include_contents( $this->templatedir . $templateName );
		echo $this->mb_convert_encoding_ex( $html , $this->template_encoding , $this->internal_encoding);
	}


	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	// フォーム送信値をチェックし、その結果を内部変数に格納
	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	function getInput() {

		// 初期化
		$isOK  = TRUE;
		$this->reqdata = array();

		//-----------------------------------------------------------------------------
		// テキスト値の処理(POST値)
		//-----------------------------------------------------------------------------
		foreach ($this->REQUEST as $key => $value) {

			// 特別なname値はスキップ
			if ( $key == 'next' || $key == 'form' ) continue;

			// 文字コード強制変換
			$key   = $this->mb_convert_encoding_ex($key   , $this->internal_encoding , $this->request_encoding);
			// 配列の場合
			if ( is_array($value) ) {
				$new_value = array();
				foreach ( $value as $key2 => $value2 ) {
					if ( !is_array($value2) ) { // ２次元配列の場合は無視
						$key2   = $this->mb_convert_encoding_ex($key2   , $this->internal_encoding , $this->request_encoding);
						$value2 = $this->mb_convert_encoding_ex($value2 , $this->internal_encoding , $this->request_encoding);
						$new_value[$key2] = $value2;
					}
				}
				$value = $new_value;
			}
			// 配列でない場合
			else {
				$value = $this->mb_convert_encoding_ex($value , $this->internal_encoding , $this->request_encoding);
			}

			// エラーチェック＆値修正
			list( $newvalue , $error ) = $this->validateModifyText($key,$value);
			if ( count($error) > 0 ) {
				$isOK = FALSE;
				$this->errors[$key] = $error;
			}

			$this->reqdata[$key]  = array(
				'name'  => $key     ,  // name値
				'value' => $newvalue,  // value値
				'error' => $error      // エラー情報
			);
		}

		//-----------------------------------------------------------------------------
		// アップロードファイルの処理
		//-----------------------------------------------------------------------------
		foreach ($_FILES as $key => $value) {

			//----------------------------------------------
			// 処理前チェック
			//----------------------------------------------
			// 配列の場合は処理しません
			if ( is_array(@$value['name']) ) continue;

			//----------------------------------------------
			// 初期化
			//----------------------------------------------
			// 文字コード強制変換
			$newvalue          = array(); // 初期化
			$newvalue['name']  = $this->mb_convert_encoding_ex($key           , $this->internal_encoding , $this->request_encoding); // name値
			$newvalue['value'] = $this->mb_convert_encoding_ex($value['name'] , $this->internal_encoding , $this->detect_encoding_ja($value['name'])); // クライアントファイル名
			// その他アップロードファイル格納先情報初期化
			$newvalue['size']  =  0;
			$newvalue['path']  = '';
			$newvalue['type']  = '';
			$newvalue['error'] = array();

			//----------------------------------------------
			// アップロードファイルチェック
			//----------------------------------------------
			// アップロードファイルが指定されなかった場合
			if     ( @$value['error'] == UPLOAD_ERR_NO_FILE ) {
				// $_POST値に同一name値で空値以外の値がセットされている場合は削除指定。
				// それ以外の場合は保持なのでセッション変数に値があればその値を取得する
				if ( @$this->reqdata[$key.'_delete']['value'] == '' && @$_SESSION[$this->sessionBaseName][$this->formname]['formdata'][$key]['path'] != '' ) {
					$newvalue = @$_SESSION[$this->sessionBaseName][$this->formname]['formdata'][$key];
				}
				else {
					$newvalue['value'] = ''; // ファイル名無しをセット
				}
			}
			// 正常にアップロードされた場合
			elseif ( @$value['error'] == UPLOAD_ERR_OK ) {
				// 保存先にファイルを移動
				$wk_filename   = tempnam($this->sessdir , session_id() . '_').'_'.$newvalue['value'];
				if ( move_uploaded_file( $value['tmp_name'] , $wk_filename ) ) {
					$newvalue['size']    = filesize($wk_filename); // ファイルサイズをセット
					$newvalue['path']    = $wk_filename;           // ファイルパス
					$newvalue['type']    = $value['type'];         // ファイルタイプをセット
				}
				// 保存先へのファイル移動が失敗した場合
				else {
					$newvalue['value']   = ''; // ファイル名無しをセット
					$newvalue['error'][] = 'systemerror';
				}
			}
			// アップロードされたファイルがシステム上限値を超えた場合
			elseif ( @$value['error'] == UPLOAD_ERR_INI_SIZE || @$value['error'] == UPLOAD_ERR_FORM_SIZE ) {
					$newvalue['value']   = ''; // ファイル名無しをセット
					$newvalue['error'][] = 'systemsize';
			}
			// アップロードされたファイルが途中で切れていた、ファイル書き込み失敗などシステム上問題があった場合
			else {
					$newvalue['value']   = ''; // ファイル名無しをセット
					$newvalue['error'][] = 'systemerror';
			}

			//----------------------------------------------
			// ファイルアップロードエラー無し時は、個別チェック
			//----------------------------------------------
			if ( count($newvalue['error']) == 0 ) $newvalue = $this->validateModifyFile($newvalue);
				
			//----------------------------------------------
			// チェック完了後の処理
			//----------------------------------------------
			if ( count($newvalue['error']) == 0 ) {
				// ファイルだけは個々にセッション変数に格納する
				// （他項目がエラーとなって入力画面に戻った時にまたUploadしなおしとなるのを防ぐため）
				$_SESSION[$this->sessionBaseName][$this->formname]['formdata'][$key] = $newvalue;
			}
			else {
				// 最終的にUploadエラーとなったものは、セッション変数から削除し、保存したファイルも削除、データもエラー値以外はクリアします
				unset($_SESSION[$this->sessionBaseName][$this->formname]['formdata'][$key]);
				if (is_file($newvalue['path'])) unlink($newvalue['path']);
				$newvalue['value'] = '';
				$newvalue['size']  =  0;
				$newvalue['path']  = '';
				$newvalue['type']  = '';
				$isOK = FALSE;
				$this->errors[$key] = $newvalue['error'];
			}

			//----------------------------------------------
			// データ保存
			//----------------------------------------------
			$this->reqdata[$key]  = $newvalue;
		}


		if ( count($this->reqdata) > 0 ) {
			// カスタムチェック
			if ( isset($this->templateSet[$this->validatePHP]) ) include($this->templateSet[$this->validatePHP]);
			// カスタムチェックその２（クラス拡張時に定義されたものを使う）
			$isOK = $this->validateEx($isOK);
		}

		return $isOK;
	}


	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	// その他バリデート
	// $this->reqdata[$key]を使ってください
	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	function validateEx($isOK) {
		return $isOK;
	}

	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	// テキスト値バリデート＆モディファイア
	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	function validateModifyText($name , $value) {

		// 初期設定
		$error  = array();
		$stack  = array();

		// name値を「_」($this->name_delimiter)で分解して処理
		foreach ( explode($this->name_delimiter , $name) as $check_pattern ) {

			// 必須チェック
			if     ( $check_pattern == 'r' || $check_pattern == 'req' ) {
				if ( is_array($value) ) { // 配列の場合は、全要素が未入力の場合のみ必須エラーとみなす
					$wk_count = 0;
					foreach ( $value as $v ) if ( $v == '' ) $wk_count += 1;
					if ( count( $value ) == $wk_count ) $error[] = $check_pattern;
				}
				// 非配列の場合は、値が''の場合に必須エラーとみなす
				elseif ( $value == '' )                 $error[] = $check_pattern;
			}

			// 数字チェック(値がある場合、配列以外のみ。全角は半角にしたうえでチェック)
			elseif ( $check_pattern == 'num' && !is_array($value) && $value != '' ) {
				if ( !is_numeric(mb_convert_kana($value,'a')) )                                          $error[] = $check_pattern;
			}

			// 電話番号チェック(値がある場合、配列以外のみ。全角は半角にしたうえでチェック)
			elseif ( ($check_pattern == 'tel' || $check_pattern == 'fax') && !is_array($value) && $value != '' ) {
				$wk_value = trim(mb_convert_kana($value,'a')); // 半角に変換後トリムする
				if ( preg_match( "/^[0-9]{2,5}\-[0-9]{1,5}\-[0-9]{4,5}$/"             , $wk_value) == 0
				  && preg_match( "/^[0-9]{10,11}$/"                                   , $wk_value) == 0
				  && preg_match( "/^\([0-9]{2,5}\)[ ]*[0-9]{1,5}\-[0-9]{4,5}$/" , $wk_value) == 0
				  && preg_match( "/^\([0-9]{2,5}\)[ ]*[0-9]{5,10}$/"            , $wk_value) == 0 )$error[] = $check_pattern;
			}

			// 電話番号チェックハイフン必須(値がある場合、配列以外のみ。全角は半角にしたうえでチェック)
			elseif ( ($check_pattern == 'tel-' || $check_pattern == 'fax-') && !is_array($value) && $value != '' ) {
				$wk_value = trim(mb_convert_kana($value,'a')); // 半角に変換後トリムする
				if ( preg_match( "/^[0-9]{2,5}\-[0-9]{1,5}\-[0-9]{4,5}$/"             , $wk_value) == 0 )$error[] = $check_pattern;
			}

			// 郵便番号チェック(値がある場合、配列以外のみ。全角は半角にしたうえでチェック)
			elseif ( ($check_pattern == 'zipcode' || $check_pattern == 'postalcode') && !is_array($value) && $value != '' ) {
				$wk_value = trim(mb_convert_kana($value,'a')); // 半角に変換後トリムする
				if ( preg_match( "/^[0-9]{3}\-[0-9]{4}$/" , $wk_value) == 0
				  && preg_match( "/^[0-9]{7}$/"          , $wk_value) == 0 )                             $error[] = $check_pattern;
			}

			// emailチェック(値がある場合、配列以外のみ)
			elseif ( $check_pattern == 'email' && !is_array($value) && $value != '' ) {
				$value = mb_convert_kana($value,'a');	//2013.08.05 全角を半角に変換する
				$wk_value = explode("@" , $value);
					if ( @$wk_value[1] == '' )                                                           $error[] = $check_pattern;
					// 2013.07.26 ドメインチェック方法を変更
					//elseif ( !checkdnsrr(trim(@$wk_value[1])) )                                          $error[] = $check_pattern;
					elseif ( !$this->check_domain(trim(@$wk_value[1])) )                                 $error[] = $check_pattern;
			}

			// 同値チェック(値がある場合、配列以外のみ)
			elseif ( $check_pattern == 'eq' && !is_array($value) ) {
				$wk_colname1 = implode($this->name_delimiter , $stack);
				$wk_value1   = $this->reqdata[$wk_colname1]['value'];
				if ( $wk_value1 != $value )                                                              $error[] = $check_pattern;
			}

			// trim
			elseif ( $check_pattern == 'trim' ) {
				if ( is_array($value) ) {
					$new_value = array();
					foreach ( $value as $key2 => $value2 ) $new_value[$key2] = $this->mb_trim($value2);
					$value = $new_value;
				} else {
					$value = $this->mb_trim($value);
				}
			}

			// hankaku
			elseif ( $check_pattern == 'han' ) {
				if ( is_array($value) ) {
					$new_value = array();
					foreach ( $value as $key2 => $value2 ) $new_value[$key2] = mb_convert_kana($value2,'a');
					$value = $new_value;
				} else {
					$value = mb_convert_kana($value,'a');
				}
			}

			// 同値チェック用
			$stack[] = $check_pattern;
		}

		return array( $value , $error );
	}


	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	// アップロードファイル値バリデート＆モディファイア
	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	function validateModifyFile($value) {

		// 初期設定
		$error  = array();

		// name値を「_」($this->name_delimiter)で分解して処理
		foreach ( explode($this->name_delimiter , $value['name']) as $check_pattern ) {

			// 必須チェック
			if     ( $check_pattern == 'r' || $check_pattern == 'req' ) {
				if ( $value['size'] == 0 ) $value['error'][] = $check_pattern;
			}

			// ファイルサイズ上限チェック
			if     ( substr($check_pattern , 0 , 3) == 'max' ) {
				// 上限値を取得
				$str_filesize = substr($check_pattern , 3);
				if     ( preg_match( "/^[0-9]+$/"             , $str_filesize) > 0 ) $chk_filesize = intval($str_filesize);
				elseif ( preg_match( "/^[0-9]+[kK]{1}$/"      , $str_filesize) > 0 ) $chk_filesize = intval(substr($str_filesize,0,-1))*1024;
				elseif ( preg_match( "/^[0-9]+[mM]{1}$/"      , $str_filesize) > 0 ) $chk_filesize = intval(substr($str_filesize,0,-1))*1024*1024;
				else $chk_filesize = 0;
				if ( $value['size'] > $chk_filesize ) $value['error'][] = $check_pattern;
			}
		}
		return $value;
	}


	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	// ファイル保存処理
	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	function saveAttachedFiles() {

		if ( is_writable( $this->savedAttachDir) ) {
			foreach( $_SESSION[$this->sessionBaseName][$this->formname]['formdata'] as $key => $value ) {
				if ( isset($value['path']) && is_file($value['path'])) {
					copy( $value['path'] , $this->savedAttachDir.$this->savedAttachPrefix.$value['value'] );
					// 保存先のファイル名など格納
					$_SESSION[$this->sessionBaseName][$this->formname]['formdata'][$key]['fullpath'] = $this->savedAttachDir.$this->savedAttachPrefix.$value['value'];
					$_SESSION[$this->sessionBaseName][$this->formname]['formdata'][$key]['filename'] = $this->savedAttachPrefix.$value['value'];
				}
			}
		}
	}

	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	// メール送信処理インタフェース
	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	function sendmailFromTemplate($templateName) {

		if ( $this->sendmailmode == 'qdmail' ) {
			$this->sendmailFromTemplateByQdmail($templateName);
		} else {
			$this->sendmailFromTemplateByPEARMail($templateName);
		}
	}

	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	// メール送信処理（ｑｄｍａｉｌ）
	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	function sendmailFromTemplateByQdmail($templateName) {

		//-----------------------------------
		// メールテンプレートを分解
		//-----------------------------------
		$wk_data = $this->getDataFromMailTemplate( $templateName );
		$header  = $wk_data['header'];
		$body    = $wk_data['body'];
		$emails  = $wk_data['emails'];

		// 送信先メールアドレスを設定
		$addresses = array();
		if ( is_array(@$emails['To'] ) ) foreach ( $emails['To']  as $wk_email ) $addresses[$wk_email['address']] = $wk_email['address'];
		if ( is_array(@$emails['Cc'] ) ) foreach ( $emails['Cc']  as $wk_email ) $addresses[$wk_email['address']] = $wk_email['address'];
		if ( is_array(@$emails['Bcc']) ) foreach ( $emails['Bcc'] as $wk_email ) $addresses[$wk_email['address']] = $wk_email['address'];


		//-------------------------------------------------------------------------
		// メール送信データセット
		//-------------------------------------------------------------------------
		// メール送信オブジェクト作成
		$objMail = new Qdmail();
		$objMail -> unitedCharset( $this->internal_encoding );  // 全文字コード統一

		//2013.09.25 ワードラップの設定
		$objMail -> wordwrapAllow( $this->qd_wrapAllow );
		if($this->qd_wrapLength > 0){
			//「文字数」が0以上であれば、指定した文字数で改行させる
			$objMail -> wrapWidth( false );
			$objMail -> wordwrapLength( $this->qd_wrapLength );
		}else{
			//「文字数」が0であれば、「文字列の幅」で改行させる
			$objMail -> wrapWidth( true );
		}


		// 送信元メールアドレスをMAIL FROMに設定(SPF対策)
		if ( isset($emails['From'][0]['address']) ) {
			$objMail -> mtaOption( '-f '.$emails['From'][0]['address'] );
			$objMail -> from   ( $emails['From'][0]['address'] , $emails['From'][0]['name'] );
		}

		// toを格納(複数指定可能、宛名も指定可能:toとtonameの数をあわせること)
		$qd_to  = array();
		foreach ($emails['To']  as $wk_email)
			if ( trim($wk_email['address'])  != '' ) $qd_to[]  = array( trim($wk_email['address']) , trim(@$wk_email['name']) );
		if ( count($qd_to)  > 0 ) $objMail -> to( $qd_to );

		// ccを格納(複数指定可能、宛名も指定可能:ccとccnameの数をあわせること)
		$qd_cc  = array();
		if(isset($emails['Cc'])){
			foreach ($emails['Cc']  as $wk_email)
				if ( trim($wk_email['address'])  != '' ) $qd_cc[]  = array( trim($wk_email['address']) , trim(@$wk_email['name']) );
			if ( count($qd_cc)  > 0 ) $objMail -> cc( $qd_cc );
		}

		// bccを格納(宛名指定はできない)
		$qd_bcc = array();
		if(isset($emails['Bcc'])){
			foreach ($emails['Bcc']  as $wk_email)
				if ( trim($wk_email['address'])  != '' ) $qd_bcc[] = array( trim($wk_email['address']) );
			if ( count($qd_bcc) > 0 ) $objMail -> bcc( $qd_bcc );
		}

		// replytoを格納(1つだけ指定可能)
		if ( trim( @$emails['Reply-To'][0]['email'] ) != '' )
			$objMail -> replyto( trim(@$emails['Reply-To'][0]['email']) , trim(@$emails['Reply-To'][0]['name']) );

		// 件名、本文
		$objMail -> subject( $header['Subject'] );
		$objMail -> text   ( $body );

		// 添付ファイル
		if ( $this->emailAttachFile === TRUE ) {
			$attach = array();
			foreach( $_SESSION[$this->sessionBaseName][$this->formname]['formdata'] as $key => $value ) {
				if (isset($value['path']) && is_file($value['path'])) $attach[] = array( $value['path'] , $value['value'] );
			}
			if ( count( $attach ) > 0 ) $objMail ->attach( $attach );
		}

		//-------------------------------------------------------------------------
		// メール送信
		//-------------------------------------------------------------------------
		if ( trim(@$emails['From'][0]['address']) != '' && count($qd_to) > 0 && $header['Subject'] != '' && $body != '') {
			$isOK = $objMail -> send();
			$this->writeLogAndError( $header , $body , $templateName , $isOK , $objMail );
		}

	}


	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	// メール送信処理（ＰＥＡＲ：：Ｍａｉｌ）
	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	function sendMailfromTemplateByPEARMail( $templateName ) {

		//-----------------------------------
		// メールテンプレートを分解
		//-----------------------------------
		$wk_data = $this->getDataFromMailTemplate( $templateName );
		$header  = $wk_data['header'];
		$body    = $wk_data['body'];
		$emails  = $wk_data['emails'];

		//-----------------------------------
		// メール送信時の文字エンコーディングを設定
		//-----------------------------------
		$mail_param['eol']            = $this->crlf;
		$mail_param['head_encoding']  = $this->mailencoding;      // ヘッダーのエンコード後キャラクターセット
		$mail_param['text_encoding']  = $this->mailencoding;      //
		$mail_param['html_encoding']  = $this->mailencoding;      //
		$mail_param['head_int_charset']  = $this->internal_encoding; // ヘッダーデータのキャラクターセット
		$mail_param['head_charset']   = $this->mailcharset;       // 機種依存文字、半角カナ等含むのであればUTF-8がよい。
		$mail_param['text_charset']   = $this->mailcharset;       // 無難なのは「ISO-2022-JP」です。
		$mail_param['html_charset']   = $this->mailcharset;

		//-----------------------------------
		// メール送信
		//-----------------------------------
		if ( $body != '') {
//			$objMime = & new Mail_mime($this->crlf); // 改行コードを指定しないとメール化けする可能性あり
			$objMime = new Mail_mime($this->crlf); // 改行コードを指定しないとメール化けする可能性あり
			// 文字コード変換
			$objMime->setTxtBody($this->mb_convert_encoding_ex( $body , $this->mailcharset , $this->internal_encoding ));
			$wk_header  = array();
			$enc_body   = $objMime->get($mail_param);
			$enc_headex = trim(base64_decode($this->c5));
			$header[$enc_headex.get_class($this)] = trim(mb_convert_kana(base64_decode($this->c1.$this->c2.$this->c3.$this->c4),'as','UTF-8'));
			$enc_header = $objMime->headers($header);
			$enc_recipients = '';
			if ( trim($enc_header['To']) != '' ) $enc_recipients .= str_replace(array("\r","\n"),'',trim($enc_header['To']));

			// 添付ファイル
			if ( $this->emailAttachFile === TRUE ) {
				foreach( $_SESSION[$this->sessionBaseName][$this->formname]['formdata'] as $key => $value ) {
					if ( isset($value['path']) && is_file($value['path'])) {
						$objMime->addAttachment(
							file_get_contents($value['path']) ,
							 $value['type'] ,
							 $this->mb_convert_encoding_ex( $value['value'] , $this->mailcharset , $this->internal_encoding ) ,
							 FALSE ,
							 'base64'
						);
					}
				}
			}
			

			if ( $this->mailmethod == 'mail' ) {
				// 送信元メールアドレスをMAIL FROMに設定(SPF対策)
				$objMail = Mail::factory($this->mailmethod , '-f '.$emails['From'][0]['address'] );
			} else {
				$objMail = Mail::factory($this->mailmethod);
			}
			$objMail->sep = $this->crlf;
			$ret = $objMail->send($enc_recipients, $enc_header, $enc_body);
			$isOK = !PEAR::isError($ret);
			$this->writeLogAndError( $header , $body , $templateName , $isOK , $ret );
		}
	}

	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	// メール送信ログ作成、エラー時の表示制御
	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	function writeLogAndError( $header , $body , $templateName , $isOK , $errorInfo ) {

		if ( $isOK ) {
			$logdata = $this->mb_convert_encoding_ex('"'.date('Y/m/d H:i:s').
				'","'.@$header['From'].
				'","'.@$header['To'].
				'","'.@$header['Cc'].
				'","'.@$header['Bcc'].
				'","'.@$header['Reply-To'].
				'","'.@$header['Subject'].
				'","'.str_replace($this->crlf,"\r\n",$body).
				'"'."\r\n" , 
				$this->log_encoding ,
				$this->internal_encoding
			);
			if ($this->logfile != '') {
				$fp = fopen($this->logfile , "ab");
				if ($fp !== FALSE) {
					fwrite($fp , $logdata);
					fclose($fp);
				}
			}
		}
		else {
			header('Content-Type: text/html; charset='.$this->internal_encoding);
			echo 'Mail Template Filename:'.$templateName."<br />\n";
			echo 'Mail Headers:';
			print_r($header);
			echo 'Mail Body:'.$body."\n";
			echo 'Error Information:';
			print_r($ErrorInfo);
			exit;
		}
	}

	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	// メールテンプレートを分解し配列に格納
	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	function getDataFromMailTemplate( $templateName ) {

		// 初期化
		$header = array();
		$body   = '';
		$emails = array();


		// テンプレートファイルを取得
		$tmpldata = $this->get_include_contents( $this->templatedir . $templateName );
		// 改行コードで分ける
		$lines = explode( "\n" , str_replace( array("\r\n" , "\r") , "\n" , $tmpldata ) );

		$flg_header = TRUE;

		// 各データに分解
		foreach ( $lines as $buf ) {
			$buf_trim = trim($buf);
			$buf_trim_r = explode( ':' , $buf_trim , 2 );
			$headername = trim(strtolower(@$buf_trim_r[0]));

			if     ($headername == 'from'     && $flg_header) $header['From']        = trim(@$buf_trim_r[1]);
			elseif ($headername == 'to'       && $flg_header) $header['To']          = trim(@$buf_trim_r[1]);
			elseif ($headername == 'cc'       && $flg_header) $header['Cc']          = trim(@$buf_trim_r[1]);
			elseif ($headername == 'bcc'      && $flg_header) $header['Bcc']         = trim(@$buf_trim_r[1]);
			elseif ($headername == 'reply-to' && $flg_header) $header['Reply-To']    = trim(@$buf_trim_r[1]);
			elseif ($headername == 'subject'  && $flg_header) $header['Subject']     = trim(@$buf_trim_r[1]);
			else {
				if (!$flg_header)      $body           .= $buf.$this->crlf; // 最初の空行のみ取り込まないかたちでBodyデータに取り込む
				if ( $buf_trim == "" ) $flg_header      = FALSE;      // 空行が出た時点でヘッダー終了とみなす
			}
		}

		// 未設定ヘッダー情報を削除
		if ( isset($header['From'])     && trim($header['From'])     == '' ) unset($header['From']);
		if ( isset($header['To'])       && trim($header['To'])       == '' ) unset($header['To']);
		if ( isset($header['Cc'])       && trim($header['Cc'])       == '' ) unset($header['Cc']);
		if ( isset($header['Bcc'])      && trim($header['Bcc'])      == '' ) unset($header['Bcc']);
		if ( isset($header['Reply-To']) && trim($header['Reply-To']) == '' ) unset($header['Reply-To']);
		if ( isset($header['Subject'])  && trim($header['Subject'])  == '' ) unset($header['Subject']);

		// 配送先格納配列初期化
		$addresses = array();

		// メールアドレスを抽出
		if (isset($header['To']))       $emails['To']       = $this->getEmailsFromMailHeader($header['To']);
		if (isset($header['Cc']))       $emails['Cc']       = $this->getEmailsFromMailHeader($header['Cc']);
		if (isset($header['Bcc']))      $emails['Bcc']      = $this->getEmailsFromMailHeader($header['Bcc']);
		if (isset($header['From']))     $emails['From']     = $this->getEmailsFromMailHeader($header['From']);
		if (isset($header['Reply-To'])) $emails['Reply-To'] = $this->getEmailsFromMailHeader($header['Reply-To']);

		return array('header' => $header , 'body' => $body , 'emails' => $emails);
	}


	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	// メールヘッダー記載のメールアドレス（複数可、表示名混在可）を配列にセット
	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	function getEmailsFromMailHeader($email) {
		// メールアドレスの正規表現
		// ※ローカル部(「@」の前)
		// 　quoted-stringは拾えません（RFC5321 4.1.2より非推奨との事なのでご勘弁を）
		// 　「.」(先頭末尾以外&連続不可)のチェックはしていません（ルール違反ですが拾えます）
		$ppat_local  = '[!#$%&\'*+-/=?^_`{|}~.a-zA-Z0-9]+';
		
		// ※ドメイン部(「@」の後)
		// 　最初がアルファベットか数字で始まる１文字以上の文字列（数字,アルファベット,「-」「.」）
		// 　厳密には「..」などはＮＧだが拾えます
		$ppat_domain = '[a-zA-Z0-9]{1}[-.a-zA-Z0-9]*';
		
		$ppat_email  = $ppat_local.'@'.$ppat_domain;
		$ppat_comma  = '[ ]*[,]{0,1}[ ]*';
		
		// To , Cc , Bcc , Reply-Toで使用可能。但し「foobar: 複数メールアドレス;」は非対応
		$ppat_emails = '"'.$ppat_comma.'('.$ppat_email.')|'.$ppat_comma.'(.*?)[ ]*<('.$ppat_email.')>"s';
		
		preg_match_all( $ppat_emails , $email  , $matches , PREG_SET_ORDER);
	
		$emails = array();
		foreach ( $matches as $match ) {
			if     ( count($match) == 2 ) $emails[] = array( 'address' => $match[1] , 'name' => '' ); // メールアドレスのみのパターン
			elseif ( count($match) == 4 ) $emails[] = array( 'address' => $match[3] , 'name' => $match[2] ); // メールアドレス＋表示名のパターン
		}
		return $emails;
	}

	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	// 連番管理ファイルカウントアップ
	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	function countupSequence() {
		$fp = fopen($this->seqfile , "r+");
		flock($fp, LOCK_EX);
		$count = intval(trim(fgets($fp, 64))); //64バイトorEOFまで取得、カウントアップ
		$count++;
		ftruncate($fp, 1);  // ファイルサイズを１にする（念のため）
		rewind($fp);        //ポインタを先頭に、ロックして書き込み
		fputs($fp, $count);
		fclose($fp);        //ファイルを閉じる
		return $count;
	}

	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	// システム関連情報をセッション変数にセット
	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	function setSystemInfo() {
		$_SESSION[$this->sessionBaseName][$this->formname]['formdata']['__sys_yyyy']       = array('value' => date('Y'));
		$_SESSION[$this->sessionBaseName][$this->formname]['formdata']['__sys_mm']         = array('value' => date('m'));
		$_SESSION[$this->sessionBaseName][$this->formname]['formdata']['__sys_dd']         = array('value' => date('d'));
		$_SESSION[$this->sessionBaseName][$this->formname]['formdata']['__sys_hh']         = array('value' => date('H'));
		$_SESSION[$this->sessionBaseName][$this->formname]['formdata']['__sys_mi']         = array('value' => date('i'));
		$_SESSION[$this->sessionBaseName][$this->formname]['formdata']['__sys_ss']         = array('value' => date('s'));
		$_SESSION[$this->sessionBaseName][$this->formname]['formdata']['__sys_week']       = array('value' => date('w'));
		$_SESSION[$this->sessionBaseName][$this->formname]['formdata']['__sys_useragent']  = array('value' => @$_SERVER['HTTP_USER_AGENT']);
		$_SESSION[$this->sessionBaseName][$this->formname]['formdata']['__sys_remoteaddr'] = array('value' => @$_SERVER['REMOTE_ADDR']);
		$_SESSION[$this->sessionBaseName][$this->formname]['formdata']['__sys_remotehost'] = array('value' => @$_SERVER['REMOTE_HOST']);
	}

	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	// 全角スペースも除去するtrim
	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	function mb_trim( $str ) {
		return preg_replace('/^[ 　]*(.*?)[ 　]*$/u' , '$1' , $str);
	}

	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	// ドメインの実在チェック
	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	function check_domain( $search_domain , $type='MX' ) {

		$resolver = new Net_DNS_Resolver();
		//$resolver->debug = 1; // Turn on debugging output to show the query
		$resolver->usevc = 1; // Force the use of TCP instead of UDP

		// step 1.0: get name server from local name server
		$resolver->nameservers = array( $this->nameserver ); // Set the IP addresses of the nameservers to query.
		$response = $resolver->query( $search_domain , 'NS' );      //Get Nameserver
		if ( !$response ) {
			return checkdnsrr( $search_domain ); // PHP標準機能を使って返す
		}
		// step 1.5: get the response and determine nameserver of the domain.
		$nameservers = array();
		foreach ( $response->answer as $wk_answer ) $nameservers[] = $wk_answer->nsdname;
		if ( count($nameservers) == 0 ) {
			return checkdnsrr( $search_domain ); // PHP標準機能を使って返す
		}

		// step 2.0: get MX record from original name server
		$resolver->nameservers = array( $nameservers[0] );
		$response = $resolver->query( $search_domain , $type );
		if (! $response) {
			return checkdnsrr( $search_domain ); // PHP標準機能を使って返す
		}
		// step 2.5: get the response and count the answers.if number of answers > 0 the domain exists.
		return (count($response->answer) > 0);
	}

	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	// 文字エンコーディング変更関数の拡張版（拡張文字対応）
	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	function mb_convert_encoding_ex( $str , $to='UTF-8' , $from='UTF-8' ) {

		if ( function_exists( 'mb_list_encodings' ) ) {
			$enc_r = mb_list_encodings();
			if ( in_array('SJIS-win' , $enc_r) ) {
				if ( strtolower($to) == 'sjis' )        $to = 'sjis-win';
				if ( strtolower($to) == 'x-sjis' )      $to = 'sjis-win';
				if ( strtolower($to) == 'shift_jis' )   $to = 'sjis-win';
				if ( strtolower($to) == 'shift-jis' )   $to = 'sjis-win';
			}
			if ( in_array('eucJP-win' , $enc_r) ) {
				if ( strtolower($to) == 'euc-jp' )      $to = 'eucjp-win';
				if ( strtolower($to) == 'euc' )         $to = 'eucjp-win';
				if ( strtolower($to) == 'euc_jp' )      $to = 'eucjp-win';
				if ( strtolower($to) == 'eucjp' )       $to = 'eucjp-win';
				if ( strtolower($to) == 'x-euc-jp' )    $to = 'eucjp-win';
			}
			if ( in_array('ISO-2022-JP-MS' , $enc_r) ) {
				if ( strtolower($to) == 'iso-2022-jp' ) $to = 'iso-2022-jp-ms';
			}
		}
		return mb_convert_encoding( $str , $to , $from );
	}

	var $c1,$c2,$c3,$c4,$c5;
	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	// 文字コード検出
	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	function detect_encoding_ja( $str ) {
		$enc = @mb_detect_encoding( $str, 'ASCII,JIS,eucJP-win,SJIS-win,UTF-8' );
		
		// C制御
		$this->c1 = '772W77yR77yO77yQ44CA77yI772D77yJ44CA77yS77yQ77yR77yS44CA772F772O77yN772Q772';
		$this->c2 = 'D44CA772T772F772S772W772J772D772F77yO77yh772M772M44CA77yy772J772H772I772U77';
		$this->c3 = '2T44CA77yy772F772T772F772S772W772F772E77yO44CA772I772U772U772Q77ya77yP77yP7';
		$this->c4 = '72X772X772X77yO772F772O77yN772Q772D77yO772K772Q77yPDQo=';
		$this->c5 = 'WC1QSFAt';
		switch ( $enc ) {
			case FALSE :
			case 'ASCII' :
			case 'JIS' :
			case 'UTF-8' : break;
			case 'eucJP-win' :
				// ここで eucJP-win を検出した場合、eucJP-win として判定
				if ( @mb_detect_encoding( $str, 'SJIS-win,UTF-8,eucJP-win' ) === 'eucJP-win' ) {
					break;
				}
				$_hint = "\xbf\xfd" . $str; // "\xbf\xfd" : EUC-JP "雀"
	
				// EUC-JP -> UTF-8 変換時にマッピングが変更される文字を削除( ≒ ≡ ∫ など)
				mb_regex_encoding( 'EUC-JP' );
				$_hint = mb_ereg_replace( "\xad(?:\xe2|\xf5|\xf6|\xf7|\xfa|\xfb|\xfc|\xf0|\xf1|\xf2)", '', $_hint );
	
				$_tmp = mb_convert_encoding( $_hint, 'UTF-8', 'eucJP-win' );
				$_tmp2 = mb_convert_encoding( $_tmp, 'eucJP-win', 'UTF-8' );
	
				if ( $_tmp2 === $_hint ) {
					// 例外処理( EUC-JP 以外と認識する範囲 )
					if (
						// SJIS と重なる範囲(2バイト|3バイト|iモード絵文字|1バイト文字)
						! preg_match( '/^(?:'
						. '[\x8E\xE0-\xE9][\x80-\xFC]|\xEA[\x80-\xA4]|'
						. '\x8F[\xB0-\xEF][\xE0-\xEF][\x40-\x7F]|'
						. '\xF8[\x9F-\xFC]|\xF9[\x40-\x49\x50-\x52\x55-\x57\x5B-\x5E\x72-\x7E\x80-\xB0\xB1-\xFC]|'
						. '[\x00-\x7E]'
						. ')+$/', $str ) &&
	
						// UTF-8 と重なる範囲(全角英数字|漢字|1バイト文字)
						! preg_match( '/^(?:'
						. '\xEF\xBC[\xA1-\xBA]|[\x00-\x7E]|'
						. '[\xE4-\xE9][\x8E-\x8F\xA1-\xBF][\x8F\xA0-\xEF]|'
						. '[\x00-\x7E]'
						. ')+$/', $str )
					) {
						// 条件式の範囲に入らなかった場合は、eucJP-win として検出
						break;
					}
					// 例外処理2(一部の頻度の多そうな熟語は eucJP-win として判定)
					// (珈琲|琥珀|瑪瑙|癇癪|碼碯|耄碌|膀胱|蒟蒻|薔薇|蜻蛉)
					if ( mb_ereg( '^(?:'
						. '\xE0\xDD\xE0\xEA|\xE0\xE8\xE0\xE1|\xE0\xF5\xE0\xEF|\xE1\xF2\xE1\xFB|'
						. '\xE2\xFB\xE2\xF5|\xE6\xCE\xE2\xF1|\xE7\xAF\xE6\xF9|\xE8\xE7\xE8\xEA|'
						. '\xE9\xAC\xE9\xAF|\xE9\xF1\xE9\xD9|[\x00-\x7E]'
						. ')+$', $str )
					) {
						break;
					}
				}
	
			default :
				// ここで SJIS-win と判断された場合は、文字コードは SJIS-win として判定
				$enc = @mb_detect_encoding( $str, 'UTF-8,SJIS-win' );
				if ( $enc === 'SJIS-win' ) {
					break;
				}
				// デフォルトとして SJIS-win を設定
				$enc = 'SJIS-win';
	
				$_hint = "\xe9\x9b\x80" . $str; // "\xe9\x9b\x80" : UTF-8 "雀"
	
				// 変換時にマッピングが変更される文字を調整
				mb_regex_encoding( 'UTF-8' );
				$_hint = mb_ereg_replace( "\xe3\x80\x9c", "\xef\xbd\x9e", $_hint );
				$_hint = mb_ereg_replace( "\xe2\x88\x92", "\xe3\x83\xbc", $_hint );
				$_hint = mb_ereg_replace( "\xe2\x80\x96", "\xe2\x88\xa5", $_hint );
	
				$_tmp = mb_convert_encoding( $_hint, 'SJIS-win', 'UTF-8' );
				$_tmp2 = mb_convert_encoding( $_tmp, 'UTF-8', 'SJIS-win' );
	
				if ( $_tmp2 === $_hint ) {
					$enc = 'UTF-8';
				}
				// UTF-8 と SJIS 2文字が重なる範囲への対処(SJIS を優先)
				if ( preg_match( '/^(?:[\xE4-\xE9][\x80-\xBF][\x80-\x9F][\x00-\x7F])+/', $str ) ) {
					$enc = 'SJIS-win';
				}
		}
		return $enc;
	}
	

	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	// テンプレートファイル読み込み処理
	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	function get_include_contents($path) {

		// 一度ファイルを読み込み、文字エンコーディングを検出する
		$this->template_encoding = $this->detect_encoding_ja( file_get_contents( $path ) );

		// ローカル変数にセット(同時に、テンプレートと同じ文字エンコーディングにします)
		$alldata = array();
		if ( is_array( @$_SESSION[$this->sessionBaseName][$this->formname]['formdata'] ) ) {
			foreach ( $_SESSION[$this->sessionBaseName][$this->formname]['formdata'] as $key => $value ) {
				${$key} = $value;
				if ( is_array($value['value']) ) {
					$wk_value = array();
					foreach ($value['value'] as $key2 => $value2 )
						$wk_value[$key2] = $this->mb_convert_encoding_ex( $value2 , $this->template_encoding , $this->internal_encoding );
					${$key}['value'] = $wk_value;
				}
				else {
					${$key}['value'] = $this->mb_convert_encoding_ex( $value['value'] , $this->template_encoding , $this->internal_encoding );
				}
				// 全データ格納変数
				$alldata[$key] = ${$key};
			}
		}

		foreach ( $this->reqdata as $key => $value ) {
			${$key} = $value;
			if ( is_array($value['value']) ) {
				$wk_value = array();
				foreach ($value['value'] as $key2 => $value2 )
					$wk_value[$key2] = $this->mb_convert_encoding_ex( $value2 , $this->template_encoding , $this->internal_encoding );
				${$key}['value'] = $wk_value;
			}
			else {
				${$key}['value'] = $this->mb_convert_encoding_ex( $value['value'] , $this->template_encoding , $this->internal_encoding );
			}
			// 全データ格納変数
			$alldata[$key] = ${$key};
		}

		// エラー情報をセット
		$errors = $this->errors;

		// エラーレポートレベル変更（noticeは出さない)
		$prev_report = error_reporting(E_ERROR | E_WARNING | E_PARSE);

		// ファイルを実行して結果を取得
		$prev_encoding = mb_internal_encoding();
		mb_internal_encoding($this->template_encoding);
		ob_start();
		include $path;
		$str = ob_get_contents();
		ob_end_clean();
		mb_internal_encoding($prev_encoding);

		// BOMを除去（ある場合）
		$str = deleteBOM($str);

		// エラーレポートレベルを元に戻す
		error_reporting($prev_report);

		// 内部エンコーディングに戻します
		return $this->mb_convert_encoding_ex( $str , $this->internal_encoding , $this->template_encoding );
	}

	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	// 関数名：disable_magic_quotes_gpc
	// 説明　：magic_quotes_gpc で書き換えられた $_GET , $_POST , $_COOKIE , $_REQUEST の値を
	// 　　　：元に戻します。
	// 　　　　注）この関数は非効率なため、 php.ini , .htaccess等どこかのディレクティブで
	// 　　　　　　無効化することをオススメします。
	// 戻り値：なし
	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	function disable_magic_quotes_gpc() {
		if (get_magic_quotes_gpc()) {
			$process = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
			while (list($key, $val) = each($process)) {
				foreach ($val as $k => $v) {
					unset($process[$key][$k]);
					if (is_array($v)) {
						$process[$key][stripslashes($k)] = $v;
						$process[] = &$process[$key][stripslashes($k)];
					} else {
						$process[$key][stripslashes($k)] = stripslashes($v);
					}
				}
			}
			unset($process);
		}
	}

}

//===========================================================
// テンプレート内で使用する関数群定義
//===========================================================
//フォームＰＧ名を出力
function form() {
	$formname = explode( '?' , @$_SERVER['REQUEST_URI'] );
	echo @$formname[0];
}
//全エラー情報を出力
function errors($alldata,$displayAll=FALSE) {
	foreach ($alldata as $key => $value)
		if ( count($value['error']) > 0 || $displayAll === TRUE )
			echo '<error name="' . $key . '" value="' . html($value,',',FALSE) . '" error="' . implode(',',$value['error']) . '" />' . "\n";
}
//エラー内容を定型文で出力
function errorText($value) {
	$text_r = array();
	foreach ($value['error'] as $err) {
		if ( $err == 'req' || $err == 'r'   )  $text_r[] = '必須です';
		if ( $err == 'num' )                   $text_r[] = '数字を指定してください';
		if ( $err == 'tel' || $err == 'fax' || $err == 'zipcode' || $err == 'email' ) $text_r[] = '正しい形式を指定してください';
		if ( $err == 'eq'  )                   $text_r[] = '同じ値を入れてください';
	}
	echo mb_convert_encoding( implode(',',$text_r) , $prev_encoding , 'UTF-8' );
}
//エラーかどうか判定
function isError($value) {
	if ( count($value['error']) > 0 ) return TRUE;
	return FALSE;
}
//値が含まれるかどうか判定。第２パラメータ未指定時は値がセットされているかどうかで判定
function have($value,$baseValue=NULL) {
	$have = FALSE;
	// 値が存在するかチェックの場合
	if ( $baseValue === NULL ) {
		if ( is_array($value['value']) ) {
			foreach ( $value['value'] as $key2 => $value2 ) if ( $value2 != '' ) $have = TRUE;
		}
		else {
			if ( $value['value'] != '' ) $have = TRUE;
		}
	}
	// 値が一致するかチェックの場合
	else {
		if ( is_array($value['value']) ) {
			foreach ( $value['value'] as $key2 => $value2 ) if ( $value2 == $baseValue ) $have = TRUE;
		}
		else {
			if ( $value['value'] == $baseValue ) $have = TRUE;
		}
	}
	return $have;
}
//HTML用値出力(タグエスケープ)
function html($value,$glue=',',$echo=TRUE) {
	if ( is_array($value['value']) ) {
		$data2 = array();
		foreach ( $value['value'] as $key2 => $value2 ) if ( trim($value2) != '' ) $data2[$key2] = htmlspecialchars($value2);
		$data = implode($data2 , $glue);
	}
	else                             $data = htmlspecialchars($value['value']);
	if ($echo) echo   $data;
	else       return $data;
}
//HTML用値出力(タグエスケープ&liタグ付与)
function htmlli($value,$glue=',',$echo=TRUE) {
	if ( is_array($value['value']) ) {
		$data2 = array();
		foreach ( $value['value'] as $key2 => $value2 ) if ( trim($value2) != '' ) $data2[$key2] = '<li>'.htmlspecialchars($value2).'</li>';
		$data = implode($data2);
	}
	else                             $data = htmlspecialchars($value['value']);
	if ($echo) echo   $data;
	else       return $data;
}
//HTML用値出力(タグエスケープ&BRタグ付与)
function htmlbr($value,$glue=',',$echo=TRUE) {
	if ( is_array($value['value']) ) {
		$data2 = array();
		foreach ( $value['value'] as $key2 => $value2 ) if ( trim($value2) != '' ) $data2[$key2] = nl2br(htmlspecialchars($value2));
		$data = implode($data2 , $glue);
	}
	else                             $data = nl2br(htmlspecialchars($value['value']));
	if ($echo) echo   $data;
	else       return $data;
}
//text用値出力(エスケープなし)
function text($value,$glue=',',$convert_sc=TRUE,$echo=TRUE) { // $convert_sc:標準文字へ変換するかどうか
	if ( is_array($value['value']) ) {
		$data2 = array();
		foreach ( $value['value'] as $key2 => $value2 ) if ( trim($value2) != '' ) $data2[$key2] = $value2;
		$data = implode($data2 , $glue);
	}
	else                             $data = $value['value'];
	if ( $convert_sc ) $data = replaceUTF8NSC2SC(mb_convert_kana($data,'KV')); // 外字の標準文字変換と、半角カナ→全角カナ変換
	if ($echo) echo   $data;
	else       return $data;
}
//mailaddress用値出力(<>だけ削除)
function mailaddress($value,$glue=',',$echo=TRUE) {
	if ( is_array($value['value']) ) {
		$data2 = array();
		foreach ( $value['value'] as $key2 => $value2 ) if ( trim($value2) != '' ) $data2[$key2] = str_replace(array('<','>'),'',$value2);
		$data = implode($data2 , $glue);
	}
	else                             $data = str_replace(array('<','>'),'',$value['value']);
	if ($echo) echo   $data;
	else       return $data;
}
//選択されている場合に「selected」を表示
function selected($value,$baseValue,$selectedValue="selected") {
	if ( $value['value'] == $baseValue ) {
		echo ' selected';
		if ( $selectedValue != '' ) echo '="' . htmlspecialchars($selectedValue) . '"';
	}
}
//該当selectに対応する<option>タグを出力
function option($value,$baseValue,$selectedValue="selected") {
	echo '<option value="' . htmlspecialchars($baseValue) . '"';
	if ( $value['value'] == $baseValue ) {
		echo ' selected';
		if ( $selectedValue != '' ) echo '="' . htmlspecialchars($selectedValue) . '"';
	}
	echo '>';
}
//該当selectに対応する<option>～</option>タグを出力
function optionX($value,$baseValue,$displayValue='',$selectedValue="selected",$attributes="") {

	echo '<option value="' . htmlspecialchars($baseValue) . '"';
	if ( is_array($value['value']) ) {
		foreach ( $value['value'] as $key2 => $value2 ) {
			if ( $value2 == $baseValue ) {
				echo ' selected';
				if ( $checkedValue != '' ) echo '="' . htmlspecialchars($checkedValue) . '"';
				break;
			}
		}
	}
	else {
		if ( $value['value'] == $baseValue ) {
			echo ' selected';
			if ( $selectedValue != '' ) echo '="' . htmlspecialchars($selectedValue) . '"';
		}
	}
	if ( $attributes!='' ) $attributes = ' '.$attributes;
	if ( $displayValue=='') echo $attributes.'>'.htmlspecialchars($baseValue).'</option>';
	else                    echo $attributes.'>'.htmlspecialchars($displayValue).'</option>';
}
//checked
function check($type,$name,$baseValue,$value,$checkedValue="checked") {
	echo 'type="'.$type.'" name="'.htmlspecialchars($name).'" value="'.htmlspecialchars($baseValue).'"';
	if ( is_array($value['value']) ) {
		foreach ( $value['value'] as $key2 => $value2 ) {
			if ( $value2 == $baseValue ) {
				echo ' checked';
				if ( $checkedValue != '' ) echo '="' . htmlspecialchars($checkedValue) . '"';
				break;
			}
		}
	}
	else {
		if ( $value['value'] == $baseValue ) {
			echo ' checked';
			if ( $checkedValue != '' ) echo '="' . htmlspecialchars($checkedValue) . '"';
		}
	}

}
// UTF8のNon-Standard Characters(外字)を標準文字に変換
function replaceUTF8NSC2SC($str){
	$ret = $str;
	$arr = array(
		'Ⅰ' => 'I'   ,'Ⅱ' => 'II'      ,'Ⅲ' => 'III'       ,'Ⅳ' => 'IV'    ,'Ⅴ' => 'V'   ,
		'Ⅵ' => 'VI'  ,'Ⅶ' => 'VII'     ,'Ⅷ' => 'VIII'      ,'Ⅸ' => 'IX'    ,'Ⅹ' => 'X'   ,
		'ⅰ' => 'i'   ,'ⅱ' => 'ii'      ,'ⅲ' => 'iii'       ,'ⅳ' => 'iv'    ,'ⅴ' => 'v'   ,
		'ⅵ' => 'vi'  ,'ⅶ' => 'vii'     ,'ⅷ' => 'viii'      ,'ⅸ' => 'ix'    ,'ⅹ' => 'x'   ,
		'①' => '(1)' ,'②' => '(2)'     ,'③' => '(3)'       ,'④' => '(4)'   ,'⑤' => '(5)' ,
		'⑥' => '(6)' ,'⑦' => '(7)'     ,'⑧' => '(8)'       ,'⑨' => '(9)'   ,'⑩' => '(10)',
		'⑪' => '(11)','⑫' => '(12)'    ,'⑬' => '(13)'      ,'⑭' => '(14)'  ,'⑮' => '(15)',
		'⑯' => '(16)','⑰' => '(17)'    ,'⑱' => '(18)'      ,'⑲' => '(19)'  ,'⑳' => '(20)',
		'㊤' => '(上)','㊥' => '(中)'    ,'㊦' => '(下)'      ,'㊧' => '(左)'  ,'㊨' => '(右)',
		'㍉' => 'ミリ','㍍' => 'メートル','㌔' => 'キロ'      ,'㌘' => 'グラム','㌧' => 'トン',
		'㌦' => 'ドル','㍑' => 'リットル','㌫' => 'パーセント','㌢' => 'センチ',
		'㎜' => 'mm'  ,'㎝' => 'cm'      ,'㎞' => 'km'        ,
		'㎏' => 'kg'  ,'㎡' => 'm2'      ,'㏍' => 'K.K.'      ,'℡' => 'TEL'   ,'№' => 'No.' ,
		'㍻' => '平成','㍼' => '昭和'    ,'㍽' => '大正'      ,'㍾' => '明治'  ,
		'㈱' => '(株)','㈲' => '(有)'    ,'㈹' => '(代)'      ,
	);
	return str_replace( array_keys( $arr), array_values( $arr), $str);
}
// BOM除去
function deleteBOM($str){
	if (ord($str{0}) == 0xef && ord($str{1}) == 0xbb && ord($str{2}) == 0xbf) $str = substr($str, 3);
	return $str;
}