
jQuery(document).ready(function($){

	if(location.hash=="#formtop"){
		// $('.noform').hide();
		// $("html,body").animate({scrollTop:$('.__form').offset().top},400,"easeInOutQuart");
		$("html, body").animate({ scrollTop: "17600px" });
	}

	if($('p').hasClass('error-m') == false){
     	$('input[name="clinic_req"]:radio' ).prop('checked', false);
	}
	var vj;
	if($('.yyzj').children().is('p')) {
		vj=1;
	}
 	if($('p').hasClass('error-m') && vj != 1){
 		$('input[name="clinic_req"]:radio' ).prop('checked', false);
 		$('.yy00').append('<p class="error-m">必須です。医院を選択してください。</p>');
 	}
	 	//ラジオボタンが選択されない場合
 	var vs=$('input:radio[name="clinic_req"]:checked').val();
 	if(vs == null){
 		$('input[name="day1_req"]').val('医院を選択してください');
 		$('input[name="day2"]').val('医院を選択してください');
 	}

/*	$( 'input[name="clinic_req"]:radio' ).change( function() {  

		if($( this ).attr('id') == 'omotesando'){
*/

			$('input[name="day1_req"]').val('');
			$('select[name="day1_period_req"]').val('');
			$('input[name="day1_req"]').attr("id","");
			$('input[name="day1_req"]').attr("class","");
			$('input[name="day1_req"]').addClass("datepicker");
			
			$('input[name="day2"]').val('');
			$('select[name="day2_period"]').val('');
			$('input[name="day2"]').attr("id","");
			$('input[name="day2"]').attr("class","");
			$('input[name="day2"]').addClass("datepicker");

				$('.datepicker').datepicker({

        closeText: "閉じる",
        prevText: "&#x3C;前",
        nextText: "次&#x3E;",
        currentText: "今日",
        monthNames: [ "1月","2月","3月","4月","5月","6月","7月","8月","9月","10月","11月","12月" ],
        monthNamesShort: [ "1月","2月","3月","4月","5月","6月","7月","8月","9月","10月","11月","12月" ],
        dayNames: [ "日曜日","月曜日","火曜日","水曜日","木曜日","金曜日","土曜日" ],
        dayNamesShort: [ "日","月","火","水","木","金","土" ],
        dayNamesMin: [ "日","月","火","水","木","金","土" ],
        weekHeader: "週",
        dateFormat: "yy/mm/dd",
        // firstDay: 0, // 週の初めは日曜
        isRTL: false,
        showMonthAfterYear: true,
        yearSuffix: "年",

        // jquery-workshop customized.
        firstDay: 1, // 週の初めは月曜
        showButtonPanel: true, // "今日"ボタン, "閉じる"ボタンを表示する

					dateFormat:"yy年mm月dd日(D)",
					minDate:   0,
					maxDate: 120,
					beforeShowDay: function(day) {

						var dd = day.getFullYear() + "/" + (day.getMonth() + 1) + "/" + day.getDate();
			            var hName = ktHolidayName(dd);
						// if ( day.getDay() == 3 ) return [false,'','休診日']; // 2016-02-09 水曜日も選択できるように修正
						if ( day.getDay() == 3 ) return [false,'','休診日']; // 2017-03-09 水曜日も選択できないように修正
						if ( day.getDay() == 0 || hName != "" ) return [true,'class-holiday','10:30?18:30'];
						return [true,'','11:00?20:00'];
					},
					onSelect: function(dateText, obj) {					
								var date1 = $(this).datepicker( 'getDate' ) || new Date();
								var the_day=date1.getDay();
								var dd = date1.getFullYear() + "/" + (date1.getMonth() + 1) + "/" + date1.getDate();
			                    var hName = ktHolidayName(dd);
								if(the_day == 0 || hName != ""){
									dateText+='<option value="">時間帯を選択</option><option value="10:00">10:00</option><option value="10:30">10:30</option><option value="11:00">11:00</option><option value="11:30">11:30</option><option value="12:00">12:00</option><option value="12:30">12:30</option><option value="13:00">13:00</option><option value="13:30">13:30</option><option value="14:00">14:00</option><option value="14:30">14:30</option><option value="15:00">15:00</option><option value="15:30">15:30</option><option value="16:00">16:00</option><option value="16:30">16:30</option><option value="17:00">17:00</option>';
								}else{
									dateText+='<option value="">時間帯を選択</option><option value="12:00">12:00</option><option value="12:30">12:30</option><option value="13:00">13:00</option><option value="13:30">13:30</option><option value="14:00">14:00</option><option value="14:30">14:30</option><option value="15:00">15:00</option><option value="15:30">15:30</option><option value="16:00">16:00</option><option value="16:30">16:30</option><option value="17:00">17:00</option><option value="17:30">17:30</option><option value="18:00">18:00</option><option value="18:30">18:30</option><option value="19:00">19:00</option>';
								}
								if($(this).attr('name') == 'day1_req'){
									$('select[name="day1_period_req"] option').remove();
							 	    $('select[name="day1_period_req"]').append(dateText);
								}if($(this).attr('name') == 'day2'){
									$('select[name="day2_period"] option').remove();
							 	    $('select[name="day2_period"]').append(dateText);
								}
					}
				});
/*		}
		else
		{
			$('input[name="day1_req"]').val('');
			$('select[name="day1_period_req"]').val('');
			$('input[name="day1_req"]').attr("id","");
			$('input[name="day1_req"]').attr("class","");
			$('input[name="day1_req"]').addClass("datepicker");
			
			$('input[name="day2"]').val('');
			$('select[name="day2_period"]').val('');
			$('input[name="day2"]').attr("id","");
			$('input[name="day2"]').attr("class","");
			$('input[name="day2"]').addClass("datepicker");

			$('.datepicker').datepicker({

					dateFormat:"yy年mm月dd日(D)",
					minDate:   0,
					maxDate: 120,
					beforeShowDay: function(day) {

						var dd = day.getFullYear() + "/" + (day.getMonth() + 1) + "/" + day.getDate();
			            var hName = ktHolidayName(dd);
						if ( day.getDay() == 1 ) return [false,'','休診日'];
						if ( day.getDay() == 2 ) return [false,'','休診日'];
						// if ( day.getDay() == 0) return [true,'class-holiday',''];
						if ( day.getDay() == 0 || hName != "" ) return [true,'class-holiday','10:30?18:30'];
						return [true,'','11:00?20:00'];
						// return [true,'',''];
					},
					onSelect: function(dateText, obj) {					
								var date1 = $(this).datepicker( 'getDate' ) || new Date();
								var the_day=date1.getDay();
								var dd = date1.getFullYear() + "/" + (date1.getMonth() + 1) + "/" + date1.getDate();
			                    var hName = ktHolidayName(dd);
								if(the_day == 0 || hName != ""){
									dateText+='<option value="">時間帯を選択</option><option value="10:00">10:00</option><option value="10:30">10:30</option><option value="11:00">11:00</option><option value="11:30">11:30</option><option value="12:00">12:00</option><option value="12:30">12:30</option><option value="13:00">13:00</option><option value="13:30">13:30</option><option value="14:00">14:00</option><option value="14:30">14:30</option><option value="15:00">15:00</option><option value="15:30">15:30</option><option value="16:00">16:00</option><option value="16:30">16:30</option><option value="17:00">17:00</option><option value="17:30">17:30</option>';
								}else{
									dateText+='<option value="">時間帯を選択</option><option value="12:00">12:00</option><option value="12:30">12:30</option><option value="13:00">13:00</option><option value="13:30">13:30</option><option value="14:00">14:00</option><option value="14:30">14:30</option><option value="15:00">15:00</option><option value="15:30">15:30</option><option value="16:00">16:00</option><option value="16:30">16:30</option><option value="17:00">17:00</option><option value="17:30">17:30</option><option value="18:00">18:00</option><option value="18:30">18:30</option><option value="19:00">19:00</option>';
								}
								if($(this).attr('name') == 'day1_req'){
									$('select[name="day1_period_req"] option').remove();
							 	    $('select[name="day1_period_req"]').append(dateText);
								}if($(this).attr('name') == 'day2'){
									$('select[name="day2_period"] option').remove();
							 	    $('select[name="day2_period"]').append(dateText);
								}
					}
				});

		}
	}); 
*/
});