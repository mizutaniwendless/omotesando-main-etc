<?php
ini_set('display_errors','Off');
ini_set('display_startup_errors',FALSE);
//ini_set('display_startup_errors',FALSE);
ini_set('log_errors',TRUE);
//define('__DFORM_DEBUGLOGFILENAME' , 'debug.log'); // デバッグログを出すのであればこの定数にファイル名を定義する
        
include_once( dirname(dirname(__FILE__)) ."/photofacial/dform/lib/dform.class.php" );

class dformEx extends dform {
    function validateEx($isOK) {
        if($this->next == 'confirm'){
            // 確認ページ遷移時に、第1希望日時の入力チェックを行う
            if ( (@$this->reqdata['day1_req']['value'] == '') || (@$this->reqdata['day1_req_period']['value'] == '') ) {
                $this->reqdata['day1_date']['error'][] = 'req';
                $isOK = FALSE;
            }
        }
        return $isOK;
    }
}

debuglog('y_01.php:step1');

$dform = new dformEx('y_01');
debuglog('y_01.php:step2');
$dform->suffix_html = '.html';
$dform->main();
debuglog('y_01.php:step3');