<?php
ini_set('display_errors','OFF');
//ini_set('display_startup_errors',TRUE);
ini_set('display_startup_errors',FALSE);
ini_set('log_errors',TRUE);
//define('__DFORM_DEBUGLOGFILENAME' , 'debug.log'); // デバッグログを出すのであればこの定数にファイル名を定義する
        
include_once( dirname(dirname(__FILE__)) ."/photofacial/dform/lib/dform.class.php" );

class dformEx extends dform {
	function validateEx($isOK) {
		return $isOK;
	}
}

debuglog('index.php:step1');

$dform = new dformEx('index');
debuglog('index.php:step2');
$dform->suffix_html = '.html';
$dform->main();
debuglog('index.php:step3');