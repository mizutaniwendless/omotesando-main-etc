jQuery(document).ready(function($){
  //SP用
  if(location.hash=="#formtop"){
    $('.noform').hide();
  }
  
  if($('p').hasClass('error-m') == false){
     $('input[name="clinic_req"]:radio' ).prop('checked', false);
  }
  var vj;
  if($('.yyzj').children().is('p')) {
    vj=1;
  }
  if($('p').hasClass('error-m') && vj != 1){
    $('input[name="clinic_req"]:radio' ).prop('checked', false);
    $('.radio_area').append('<p class="error-m">必須です。医院を選択してください。</p>');
  }
  //ラジオボタンが選択されない場合
  var vs=$('input:radio[name="clinic_req"]:checked').val();
  if(vs == null){
    $('input[name="day1_req"]').val('医院を選択してください');
    $('input[name="day2"]').val('医院を選択してください');
  }

  $( 'input[name="clinic_req"]:radio' ).change( function() {  


    if($( this ).attr('id') == 'omotesando'){

        $('select[name="day1_period_req"]').val('');
        $('select[name="day2_period"]').val('');

        $("input[name='day1_req']").remove();
        var $d01='<input type="text" class="w30 datepicker" id="day1_req" name="day1_req"> ';
        $('select[name="day1_period_req"]').before($d01);
        
        
        $("input[name='day2']").remove();
        var $d02='<input type="text" class="w30 datepicker" id="day2" name="day2"> ';
        $('select[name="day2_period"]').before($d02);


        $('.datepicker').datepicker({
          dateFormat:"yy年mm月dd日(D)",
          minDate:   0,
          maxDate: 120,
          beforeShowDay: function(day) {

            var dd = day.getFullYear() + "/" + (day.getMonth() + 1) + "/" + day.getDate();
                  var hName = ktHolidayName(dd);
            // if ( day.getDay() == 3 ) return [false,'','休診日']; // 2016-02-09 水曜日も選択できるように修正
            if ( day.getDay() == 3 ) return [false,'','休診日']; // 2017-03-09 水曜日も選択できないように修正
            if ( day.getDay() == 0 || hName != "" ) return [true,'class-holiday','10:30?18:30'];
            return [true,'','11:00?20:00'];
          },
          onSelect: function(dateText, obj) {         
                var date1 = $(this).datepicker( 'getDate' ) || new Date();
                var the_day=date1.getDay();
                var dd = date1.getFullYear() + "/" + (date1.getMonth() + 1) + "/" + date1.getDate();
                          var hName = ktHolidayName(dd);
                if(the_day == 0 || hName != ""){
                  dateText+='<option value="">▼選択してください</option><option value="10:00">10:00</option><option value="10:30">10:30</option><option value="11:00">11:00</option><option value="11:30">11:30</option><option value="12:00">12:00</option><option value="12:30">12:30</option><option value="13:00">13:00</option><option value="13:30">13:30</option><option value="14:00">14:00</option><option value="14:30">14:30</option><option value="15:00">15:00</option><option value="15:30">15:30</option><option value="16:00">16:00</option><option value="16:30">16:30</option><option value="17:00">17:00</option>';
                }else{
                  dateText+='<option value="">▼選択してください</option><option value="12:00">12:00</option><option value="12:30">12:30</option><option value="13:00">13:00</option><option value="13:30">13:30</option><option value="14:00">14:00</option><option value="14:30">14:30</option><option value="15:00">15:00</option><option value="15:30">15:30</option><option value="16:00">16:00</option><option value="16:30">16:30</option><option value="17:00">17:00</option><option value="17:30">17:30</option><option value="18:00">18:00</option><option value="18:30">18:30</option><option value="19:00">19:00</option>';
                }
                if($(this).attr('name') == 'day1_req'){
                  $('select[name="day1_period_req"] option').remove();
                    $('select[name="day1_period_req"]').append(dateText);
                }if($(this).attr('name') == 'day2'){
                  $('select[name="day2_period"] option').remove();
                    $('select[name="day2_period"]').append(dateText);
                }
          }
        });
    }
    else
    {
        $('select[name="day1_period_req"]').val('');
        $('select[name="day2_period"]').val('');

        $("input[name='day1_req']").remove();
        var $d01='<input type="text" class="w30 datepicker" id="day1_req" name="day1_req"> ';
        $('select[name="day1_period_req"]').before($d01);
        
        
        $("input[name='day2']").remove();
        var $d02='<input type="text" class="w30 datepicker" id="day2" name="day2"> ';
        $('select[name="day2_period"]').before($d02);

      $('.datepicker').datepicker({

          dateFormat:"yy年mm月dd日(D)",
          minDate:   0,
          maxDate: 120,
          beforeShowDay: function(day) {

            var dd = day.getFullYear() + "/" + (day.getMonth() + 1) + "/" + day.getDate();
                  var hName = ktHolidayName(dd);
           if ( day.getDay() == 1 ) return [false,'','休診日'];
		   if ( day.getDay() == 2 ) return [false,'','休診日'];
            if ( day.getDay() == 0 || hName != "" ) return [true,'class-holiday','10:30?18:30'];
			return [true,'','11:00?20:00'];
          },
          onSelect: function(dateText, obj) {         
                var date1 = $(this).datepicker( 'getDate' ) || new Date();
                var the_day=date1.getDay();
                var dd = date1.getFullYear() + "/" + (date1.getMonth() + 1) + "/" + date1.getDate();
                          var hName = ktHolidayName(dd);
                if(the_day == 0 || hName != ""){
                  dateText+='<option value="">▼選択してください</option><option value="10:00">10:00</option><option value="10:30">10:30</option><option value="11:00">11:00</option><option value="11:30">11:30</option><option value="12:00">12:00</option><option value="12:30">12:30</option><option value="13:00">13:00</option><option value="13:30">13:30</option><option value="14:00">14:00</option><option value="14:30">14:30</option><option value="15:00">15:00</option><option value="15:30">15:30</option><option value="16:00">16:00</option><option value="16:30">16:30</option><option value="17:00">17:00</option><option value="17:30">17:30</option>';
                }else{
                  dateText+='<option value="">▼選択してください</option><option value="12:00">12:00</option><option value="12:30">12:30</option><option value="13:00">13:00</option><option value="13:30">13:30</option><option value="14:00">14:00</option><option value="14:30">14:30</option><option value="15:00">15:00</option><option value="15:30">15:30</option><option value="16:00">16:00</option><option value="16:30">16:30</option><option value="17:00">17:00</option><option value="17:30">17:30</option><option value="18:00">18:00</option><option value="18:30">18:30</option><option value="19:00">19:00</option>';
                }
                if($(this).attr('name') == 'day1_req'){
                  $('select[name="day1_period_req"] option').remove();
                    $('select[name="day1_period_req"]').append(dateText);
                }if($(this).attr('name') == 'day2'){
                  $('select[name="day2_period"] option').remove();
                    $('select[name="day2_period"]').append(dateText);
                }
          }
        });

    }
  }); 

});