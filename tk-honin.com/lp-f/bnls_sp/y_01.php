<?php
ini_set('display_errors','On');
ini_set('display_startup_errors',TRUE);
ini_set('log_errors',TRUE);
//define('__DFORM_DEBUGLOGFILENAME' , 'debug.log'); // デバッグログを出すのであればこの定数にファイル名を定義する

date_default_timezone_set('Asia/Tokyo');

        
include_once( dirname(__FILE__) ."/dform/lib/dform.class.php" );

class dformEx extends dform {
    function validateEx($isOK) {
        if($this->next == 'confirm'){
            // 確認ページ遷移時に、第1希望日時の入力チェックを行う
            if ( (@$this->reqdata['day1_req']['value'] == '') || (@$this->reqdata['day1_req_period']['value'] == '') ) {
                $this->reqdata['day1_date']['error'][] = 'req';
                $isOK = FALSE;
            }
        }
        return $isOK;
    }
}


$dform = new dformEx('y_01');
$dform->suffix_html = '.html';
$dform->main();
