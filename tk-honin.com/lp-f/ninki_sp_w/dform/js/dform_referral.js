/*!
 * It gets referral URL and sets to Cookie 'dform_referral'.
 * 'referral' means website that accessed to this website except itself.
 * 
 * Note: If browser does not send 'Referer-Header' , it is not working , as expected.
 * 
 * (C) 2016 en-pc service. All Rights Reserved.
 * This code cannot be redistributed without permission from www.en-pc.jp.
 * http://www.en-pc.jp/
 */
function getHostname(url){
    var a = document.createElement('a');
    a.href=url;
    return a.hostname;
}
function setCookie( name , value ){
    document.cookie = name + '=' + value;
}
function getCookie( name ){
    var res  = null;
    var cn   = name + '=';
    var ac   = document.cookie;
    var pos  = ac.indexOf(cn);
    if( pos != -1 ){
        var si = pos + cn.length;
        var ei = ac.indexOf( ';', si );
        if( ei == -1 ) ei = ac.length;
        res = decodeURIComponent(ac.substring(si, ei));
    }
    return res;
}
if ( getHostname(document.referrer) != location.hostname ) setCookie('dform_referral' , document.referrer);
