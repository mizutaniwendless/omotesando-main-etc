
$(function() {
 
    /**
     * 現在スクロール位置によるグローバルナビのアクティブ表示
     */
    var scrollMenu = function() {
        // 配列宣言
        // ここにスクロールで点灯させる箇所のidを記述する
        // 数値は全て0でOK
        var array = {
            '#top': 0,
            '#botox': 0,
            '#hyaluronan': 0,
            '#eye-maibotsu': 0,
            '#shimi': 0,
            '#ultracell': 0,
            '#step': 0,
            '#guide': 0,
            '#doctor': 0
        };
 
        var $globalNavi = new Array();
 
        // 各要素のスクロール値を保存
        for (var key in array) {
            if ($(key).offset()) {
                array[key] = $(key).offset().top - 10; // 数値丁度だとずれるので10px余裕を作る
                $globalNavi[key] = $('#navi li a[href="' + key + '"]');
            }
        }
 
        // スクロールイベントで判定
        $(window).scroll(function () {
            for (var key in array) {
                if ($(window).scrollTop() > array[key] - 50) {
                    $('#navi li a').each(function() {
                        $(this).removeClass('on');
                    });
                    $globalNavi[key].addClass('on');
                }
            }
        });
    }
 
    // 実行
    scrollMenu();
});



$(function() {
    var topBtn = $('#page-top');   
    topBtn.hide();
    //スクロールが100に達したらボタン表示
    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            topBtn.fadeIn();
        } else {
            topBtn.fadeOut();
        }
    });
    //スクロールしてトップ
    topBtn.click(function () {
		$("#navi").css({
			opacity: 1
		}, 500, function() {
			$('body,html').animate({
				scrollTop: 0
			}, 500);
        });
        
        return false;
    });
});

