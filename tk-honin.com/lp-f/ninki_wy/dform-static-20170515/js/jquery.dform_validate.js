(function($) {

	$.fn.dform_validate = function() {

		var formelm = this;
		formelm.find('input,select,textarea').each(function(idx,elm) {
			var target  = $(elm);
			var nameArr = target.prop('name').split("[");
			nameArr     = nameArr[0].split("_");
			for ( var i=0; i<nameArr.length; i++ ) {

				if ( (nameArr[i] == 'req' || nameArr[i] == 'r')  && !target.prop('required') ) {
					if ( target.prop('type').toLowerCase() != 'checkbox' && target.prop('type').toLowerCase() != 'hidden' ) {
						target.prop('required' , true); // checkboxは除外(別途処理します)
					}
				}

				if ( nameArr[i] == 'tel' ||  nameArr[i] == 'fax' ) {
					if ( !target.prop('pattern')     ) target.prop('pattern'     , "\\[d\\-]{10,13}");
					if ( !target.prop('placeholder') ) target.prop('placeholder' , "0ABXXXXYYYY");
				}

				if ( nameArr[i] == 'telh' ||  nameArr[i] == 'faxh' ) {
					if ( !target.prop('pattern')     ) target.prop('pattern'     , "\\d{2,4}-\\d{2,4}-\\d{3,4}");
					if ( !target.prop('placeholder') ) target.prop('placeholder' , "0AB-XXXX-YYYY形式");
				}

				if ( nameArr[i] == 'zipcode' || nameArr[i] == 'postalcode' ) {
					if ( !target.prop('pattern')     ) target.prop('pattern'     , "\\d{3}-\\d{4}");
					if ( !target.prop('placeholder') ) target.prop('placeholder' , "111-2222形式");
					// 値補正(ハイフンを入れる)
					target.blur(function(){
						var str = target.val();
						if ( str.match(/^[0-9]{7}$/) ) target.val( str.slice(0,3) + '-' + str.slice(3) );
					});
				}

				if ( nameArr[i] == 'trim' ) {
					// 値補正(トリム)
					target.on('blur change' , function() {
						target.val( target.val().trim() );
					});
				}

				if ( nameArr[i] == 'zen' ) {
					// 値補正(全角変換)
					target.on('blur change' , function() {
						target.val( widenarrow.ToWideAll( target.val() ,true ) );
					});
				}

				if ( nameArr[i] == 'han' ) {
					// 値補正(半角変換)
					target.on('blur change' , function() {
						target.val( widenarrow.ToNarrowAll( target.val() ,true ) );
					});
				}

				if ( nameArr[i] == 'date' ) {
					if ( !target.prop('pattern')     ) target.prop('pattern'     , "^[0-9]{4}\\/[0-9]{2}\\/[0-9]{2}$");
					if ( !target.prop('placeholder') ) target.prop('placeholder' , "yyyy/mm/dd形式");
					//バリデーション
					target.on('change' , function() {
						target[0].setCustomValidity(''); // 初期化は必ず行う
						if ( target.val() != '' ) {  // 未入力時はチェックしない
							var dateArr = target.val().split("/");
							dt  = new Date( dateArr[0],dateArr[1]-1,dateArr[2] );
							if ( dt.getFullYear()!=dateArr[0] || dt.getMonth()!=dateArr[1]-1 || dt.getDate()!=dateArr[2] ) target[0].setCustomValidity('日付が正しくありません');
						}
					});
				}

				if ( nameArr[i] == 'num' ) {   // 数字
					if ( !target.prop('pattern')     ) target.prop('pattern'     , "[+-]?[0-9]*[\\.]?[0-9]+");
					if ( !target.prop('placeholder') ) target.prop('placeholder' , "半角数字");
				}

				if ( nameArr[i] == 'pnum' ) { // 正の数字
					if ( !target.prop('pattern')     ) target.prop('pattern'     , "[0-9]*[\\.]?[0-9]+");
					if ( !target.prop('placeholder') ) target.prop('placeholder' , "半角数字");
					//バリデーション
					target.on('change' , function() {
						target[0].setCustomValidity(''); // 初期化は必ず行う
						if ( target.val() != '' && parseFloat(target.val()) <= 0 ) target[0].setCustomValidity('正の数字にしてください');
					});
				}

				if ( nameArr[i] == 'int' ) { // 整数
					if ( !target.prop('pattern')     ) target.prop('pattern'     , "[+-]?\\d*");
					if ( !target.prop('placeholder') ) target.prop('placeholder' , "半角数字");
				}

				if ( nameArr[i] == 'pint' ) { // 正の整数
					if ( !target.prop('pattern')     ) target.prop('pattern'     , "[+-]?\\d*");
					if ( !target.prop('placeholder') ) target.prop('placeholder' , "半角数字");
					//バリデーション
					target.on('change' , function() {
						target[0].setCustomValidity(''); // 初期化は必ず行う
						if ( target.val() != '' && parseInt(target.val()) <= 0 ) target[0].setCustomValidity('正の整数にしてください');
					});
				}
			}
		});

		// 全チェックボックスのうち必須(req)指定のあるものに対し、name値が同じものの数を数える
		var checkboxCounter = {}; // キーにname値,値に個数が入る

		for (var i=0; i< formelm.find('input[type=checkbox]').length; i++) {
			var target2  = formelm.find('input[type=checkbox]').eq(i);
			var name2    = target2.prop('name');

			var nameArr2 = name2.split("[");
			nameArr2     = nameArr2[0].split("_");

			for ( var j=0; j<nameArr2.length; j++ ) {
				if ( nameArr2[j] == 'req' || nameArr2[j] == 'r' ) {
					if ( checkboxCounter[name2] === undefined ) {
						checkboxCounter[name2] = 0;
					}
					checkboxCounter[name2] += 1;
				}
			}
		}
		// 必須チェック指定チェックボックスに対し、requiredの付与を行う
		for ( name_string in checkboxCounter ) {
			var name_esc = name_string.replace(/[ !"#$%&'()*+,.\/:;<=>?@\[\\\]^`{|}~]/g, '\\$&'); // 要素値のエスケープ
			// 初回表示時に required指定を行う
			if ( formelm.find('input[name='+name_esc+']:checked').length == 0 ) formelm.find('input[name='+name_esc+'][type=checkbox]').prop('required',true);
			else                                                             formelm.find('input[name='+name_esc+'][type=checkbox]').prop('required',false);
			// 変更時
			$('input[name='+name_esc+'][type=checkbox]').on('change',function(){
				var elmname = $(this).prop('name').replace(/[ !"#$%&'()*+,.\/:;<=>?@\[\\\]^`{|}~]/g, '\\$&');
				if ( formelm.find('input[name='+elmname+']:checked').length == 0 ) formelm.find('input[name='+elmname+'][type=checkbox]').prop('required',true);
				else                                                            formelm.find('input[name='+elmname+'][type=checkbox]').prop('required',false);
			});
		}

		var noSbcRegex = /[^\x00-\x7E]+/g; // 非ASCII文字
		formelm.find('.dform_sbc').on('keydown blur paste keyup change', function(e) {
			// 2バイト文字が入力されたら削除
			var target = $(this);
			if( !target.val().match(noSbcRegex) ) return;
			window.setTimeout( function() {
				target.val( target.val().replace(noSbcRegex, '') );
			}, 1);
		});

		// 大文字に変換
		formelm.find('.dform_uppercase').on('keydown blur paste keyup change', function(e) {
			var target = $(this);
			window.setTimeout( function() {
				target.val( target.val().toUpperCase() );
			}, 1);
		});

		// 小文字に変換
		formelm.find('.dform_lowercase').on('keydown blur paste keyup change', function(e) {
			var target = $(this);
			window.setTimeout( function() {
				target.val( target.val().toLowerCase() );
			}, 1);
		});
		return this;
	};

	// class CharWideNarrow
	// http://blog.livedoor.jp/midorityo/archives/51065584.html

	var narrowdicASCII = {
		"～": "~" , "｝": "}" , "｜": "|" , "｛": "{" , "ｚ": "z" ,"ｙ": "y" , "ｘ": "x" , "ｗ": "w" , "ｖ": "v" , "ｕ": "u" ,
		"ｔ": "t" , "ｓ": "s" , "ｒ": "r" , "ｑ": "q" , "ｐ": "p" ,"ｏ": "o" , "ｎ": "n" , "ｍ": "m" , "ｌ": "l" , "ｋ": "k" ,
		"ｊ": "j" , "ｉ": "i" , "ｈ": "h" , "ｇ": "g" , "ｆ": "f" ,"ｅ": "e" , "ｄ": "d" , "ｃ": "c" , "ｂ": "b" , "ａ": "a" ,
		"‘": "`" , "＿": "_" , "＾": "^" , "］": "]" , "￥": "\\","［": "[" , "Ｚ": "Z" , "Ｙ": "Y" , "Ｘ": "X" , "Ｗ": "W" ,
		"Ｖ": "V" , "Ｕ": "U" , "Ｔ": "T" , "Ｓ": "S" , "Ｒ": "R" ,"Ｑ": "Q" , "Ｐ": "P" , "Ｏ": "O" , "Ｎ": "N" , "Ｍ": "M" ,
		"Ｌ": "L" , "Ｋ": "K" , "Ｊ": "J" , "Ｉ": "I" , "Ｈ": "H" ,"Ｇ": "G" , "Ｆ": "F" , "Ｅ": "E" , "Ｄ": "D" , "Ｃ": "C" ,
		"Ｂ": "B" , "Ａ": "A" , "＠": "@" , "？": "?" , "＞": ">" ,"＝": "=" , "＜": "<" , "；": ";" , "：": ":" , "９": "9" ,
		"８": "8" , "７": "7" , "６": "6" , "５": "5" , "４": "4" ,"３": "3" , "２": "2" , "１": "1" , "０": "0" , "／": "/" ,
		"．": "." , "－": "-" , "，": "," , "＋": "+" , "＊": "*" ,"）": ")" , "（": "(" , "’": "'" , "＆": "&" , "％": "%" ,
		"＄": "$" , "＃": "#" , "”": "\"", "！": "!" , "　": " "
	}
	// 逆引き表の作成
	var widedicASCII= new Object();
	for (var keyString in narrowdicASCII) {
		widedicASCII[ narrowdicASCII[keyString] ] = keyString ;
	}

	var narrowdicANK = {
		"゜": "ﾟ" , "゛": "ﾞ" , "ヶ": "ｹ" , "ヵ": "ｶ" , "ヴ": "ｳﾞ","ン": "ﾝ" , "ヲ": "ｦ" , "ヱ": "ｳｪ", "ヰ": "ｳｨ", "ワ": "ﾜ" ,
		"ヮ": "ﾜ" , "ロ": "ﾛ" , "レ": "ﾚ" , "ル": "ﾙ" , "リ": "ﾘ" ,"ラ": "ﾗ" , "ヨ": "ﾖ" , "ョ": "ｮ" , "ユ": "ﾕ" , "ュ": "ｭ" ,
		"ヤ": "ﾔ" , "ャ": "ｬ" , "モ": "ﾓ" , "メ": "ﾒ" , "ム": "ﾑ" ,"ミ": "ﾐ" , "マ": "ﾏ" , "ポ": "ﾎﾟ", "ボ": "ﾎﾞ", "ホ": "ﾎ" ,
		"ペ": "ﾍﾟ", "ベ": "ﾍﾞ", "ヘ": "ﾍ" , "プ": "ﾌﾟ", "ブ": "ﾌﾞ","フ": "ﾌ" , "ピ": "ﾋﾟ", "ビ": "ﾋﾞ", "ヒ": "ﾋ" , "パ": "ﾊﾟ",
		"バ": "ﾊﾞ", "ハ": "ﾊ" , "ノ": "ﾉ" , "ネ": "ﾈ" , "ヌ": "ﾇ" ,"ニ": "ﾆ" , "ナ": "ﾅ" , "ド": "ﾄﾞ", "ト": "ﾄ" , "デ": "ﾃﾞ",
		"テ": "ﾃ" , "ヅ": "ﾂﾞ", "ツ": "ﾂ" , "ッ": "ｯ" , "ヂ": "ﾁﾞ","チ": "ﾁ" , "ダ": "ﾀﾞ", "タ": "ﾀ" , "ゾ": "ｿﾞ", "ソ": "ｿ" ,
		"ゼ": "ｾﾞ", "セ": "ｾ" , "ズ": "ｽﾞ", "ス": "ｽ" , "ジ": "ｼﾞ","シ": "ｼ" , "ザ": "ｻﾞ", "サ": "ｻ" , "ゴ": "ｺﾞ", "コ": "ｺ" ,
		"ゲ": "ｹﾞ", "ケ": "ｹ" , "グ": "ｸﾞ", "ク": "ｸ" , "ギ": "ｷﾞ","キ": "ｷ" , "ガ": "ｶﾞ", "カ": "ｶ" , "オ": "ｵ" , "ォ": "ｫ" ,
		"エ": "ｴ" , "ェ": "ｪ" , "ウ": "ｳ" , "ゥ": "ｩ" , "イ": "ｲ" ,"ィ": "ｨ" , "ア": "ｱ" , "ァ": "ｧ" , "ー": "ｰ" , "・": "･" ,
		"、": "､" , "」": "｣" , "「": "｢" , "。": "｡"
	}
	// 逆引き表の作成
	var widedicANK= new Object();
	for (var keyString in narrowdicANK) {
		widedicANK[ narrowdicANK[keyString] ] = keyString ;
	}

	var CharWideNarrow = function(){}

	CharWideNarrow.prototype.ToNarrowAll = function( str ){
		var rtn ="" ; var char_ ; var trns_ ;

		var max_ = str.length ;
		for (i=0; i < max_ ; i++){
			char_ = str.charAt(i) ;
			if( char_ in narrowdicASCII ){
				trns_ = narrowdicASCII[ char_ ] ;
			} else {
				if( char_ in narrowdicANK ){
					trns_ = narrowdicANK[ char_ ] ;
				} else {
					trns_ = char_ ;
				}
			}
			rtn = rtn + trns_ ;
		}
		return rtn ;
	}

	CharWideNarrow.prototype.ToNarrowASCII = function( str ){
		var rtn = "" ; var char_ ; var trns_ ;

		var max_ = str.length ;
		for (i=0; i < max_ ; i++){
			char_ = str.charAt(i) ;
			if( char_ in narrowdicASCII ){
				trns_ = narrowdicASCII[ char_ ] ;
			} else {
				trns_ = char_ ;
			}
			rtn = rtn + trns_ ;
		}
		return rtn ;
	}

	CharWideNarrow.prototype.ToNarrowKANA = function( str ){
		var rtn = "" ; var char_ ; var trns_ ;

		var max_ = str.length ;
		for (i=0; i < max_ ; i++){
			char_ = str.charAt(i) ;
			if( char_ in narrowdicANK ){
				trns_ = narrowdicANK[ char_ ] ;
			} else {
				trns_ = char_ ;
			}
			rtn = rtn + trns_ ;
		}
		return rtn ;
	}

	CharWideNarrow.prototype.ToWideAll = function( str , option_ ){
		var rtn = "" ; var char_ ; var trns_ ; var next_c ; var flg_nextc_trns = false ; var k ;

		var max_ = str.length ;
		for (i=0; i < max_ ; i++){
			if( flg_nextc_trns ){
				flg_nextc_trns = false ;
			} else {
				char_  = str.charAt(i)  ;
				next_c = str.charAt(i+1);
				switch( next_c ){
					case "ﾟ" :
					case "ﾞ" :
						k = char_ + next_c ;
						if( k in widedicANK ){
							char_ = k ;
							flg_nextc_trns = true  ;
						}
						break ;
					case "ｨ" :
					case "ｪ" :
						if( option_ = true ){
							k = char_ + next_c ;
							if( k in widedicANK ){
								char_ = k ;
								flg_nextc_trns = true ;
							}
						}
						break;
				}
				if (char_ in widedicASCII){
					trns_ = widedicASCII[ char_ ] ;
				} else {
					if(char_ in widedicANK){
						trns_ = widedicANK[ char_ ] ;
					} else {
						trns_ = char_ ;
					}
				}
				rtn = rtn + trns_ ;
			}
		}
		return rtn ;
	}

	CharWideNarrow.prototype.ToWideASCII = function( str ){
		var rtn = "" ; var char_ ; var trns_ ;

		var max_ = str.length ;
		for (i=0;i < max_ ; i++){
			char_ = str.charAt(i) ;
			if( char_ in widedicASCII ){
				trns_ = widedicASCII[ char_ ] ;
			} else {
				trns_ = char_ ;
			}
			rtn = rtn + trns_ ;
		}
		return rtn ;
	}

	CharWideNarrow.prototype.ToWideKANA = function( str , option_ ){
		var rtn = "" ; var char_ ; var trns_ ; var next_c ; var flg_nextc_trns = false ; var k ;

		var max_ = str.length ;
		for (i=0 ; i < max_ ; i++ ){
			if( flg_nextc_trns ){
				flg_nextc_trns = false ;
			} else {
				char_  = str.charAt(i)  ;
				next_c = str.charAt(i+1);

				switch( next_c ){
					case "ﾟ" :
					case "ﾞ" :
						k = char_ + next_c ;
						if( k in widedicANK ){
							char_ = k ;
							flg_nextc_trns = true  ;
						}
						break ;
					case "ｨ" :
					case "ｪ" :
						if( option_ = true ){
							k = char_ + next_c ;
							if( k in widedicANK ){
								char_ = k ;
								flg_nextc_trns = true  ;
							}
						}
						break;
				}
				if(char_ in widedicANK){
					trns_ = widedicANK[ char_ ] ;
				} else {
					trns_ = char_ ;
				}
				rtn = rtn + trns_ ;
			}
		}
		return rtn ;
	}
	// class CharWideNarrow End
	var widenarrow = new CharWideNarrow;


	$.fn.dform_setValidation = function(type) {

		var elms = $(this);

		var req_checkbox_counter = 0; // checkboxに対するrequire時に対象要素をカウントするための変数
		var req_checked_counter  = 0; // チェックされた対象要素をカウントするための変数
		elms.each(function(){
			var target  = $(this);
			if ( type=='req' || type=='r' ) {
				if ( target.prop('type').toLowerCase() != 'checkbox' && target.prop('type').toLowerCase() != 'hidden' ) {
					target.prop('required' , true); // checkboxは除外(別途処理します)
				}

//要修正(eachは使えない)
//				// チェックボックスに対するreq指定
//				if ( target.prop('type').toLowerCase() == 'checkbox' ) {
//					req_checkbox_counter += 1;
//					if ( target.prop('checked') ) req_checked_counter += 1;
//				}
			}

			if ( type=='tel' || type=='fax' ) {
				if ( !target.prop('pattern')     ) target.prop('pattern'     , "\\d{10,11}");
				if ( !target.prop('placeholder') ) target.prop('placeholder' , "0ABXXXXYYYY");
			}

			if ( type=='telh' || type=='faxh' ) {
				if ( !target.prop('pattern')     ) target.prop('pattern'     , "\\d{2,4}-\\d{2,4}-\\d{3,4}");
				if ( !target.prop('placeholder') ) target.prop('placeholder' , "0AB-XXXX-YYYY形式");
			}

			if ( type=='zipcode' || type=='postalcode' ) {
				if ( !target.prop('pattern')     ) target.prop('pattern'     , "\\d{3}-\\d{4}");
				if ( !target.prop('placeholder') ) target.prop('placeholder' , "111-2222形式");
				// 値補正(ハイフンを入れる)
				target.blur(function(){
					var str = target.val();
					if ( str.match(/^[0-9]{7}$/) ) target.val( str.slice(0,3) + '-' + str.slice(3) );
				});
			}

			if ( type=='trim' ) {
				// 値補正(トリム)
				target.on('blur change' , function() {
					target.val( target.val().trim() );
				});
			}

			if ( type=='zen' ) {
				// 値補正(全角変換)
				target.on('blur change' , function() {
					target.val( widenarrow.ToWideAll( target.val() ,true ) );
				});
			}

			if ( type=='han' ) {
				// 値補正(半角変換)
				target.on('blur change' , function() {
					target.val( widenarrow.ToNarrowAll( target.val() ,true ) );
				});
			}

			if ( type=='date' ) {
				if ( !target.prop('pattern')     ) target.prop('pattern'     , "^[0-9]{4}\\/[0-9]{2}\\/[0-9]{2}$");
				if ( !target.prop('placeholder') ) target.prop('placeholder' , "yyyy/mm/dd形式");
				//バリデーション
				target.on('change' , function() {
					target[0].setCustomValidity(''); // 初期化は必ず行う
					if ( target.val() != '' ) {  // 未入力時はチェックしない
						var dateArr = target.val().split("/");
						dt  = new Date( dateArr[0],dateArr[1]-1,dateArr[2] );
						if ( dt.getFullYear()!=dateArr[0] || dt.getMonth()!=dateArr[1]-1 || dt.getDate()!=dateArr[2] ) target[0].setCustomValidity('日付が正しくありません');
					}
				});
			}
	
			if ( type=='num' ) {   // 数字
				if ( !target.prop('pattern')     ) target.prop('pattern'     , "[+-]?[0-9]*[\\.]?[0-9]+");
				if ( !target.prop('placeholder') ) target.prop('placeholder' , "半角数字");
			}

			if ( type=='pnum' ) { // 正の数字
				if ( !target.prop('pattern')     ) target.prop('pattern'     , "[0-9]*[\\.]?[0-9]+");
				if ( !target.prop('placeholder') ) target.prop('placeholder' , "半角数字");
				//バリデーション
				target.on('change' , function() {
					target[0].setCustomValidity(''); // 初期化は必ず行う
					if ( target.val() != '' && parseFloat(target.val()) <= 0 ) target[0].setCustomValidity('正の数字にしてください');
				});
			}

			if ( type=='int' ) { // 整数
				if ( !target.prop('pattern')     ) target.prop('pattern'     , "[+-]?\\d*");
				if ( !target.prop('placeholder') ) target.prop('placeholder' , "半角数字");
			}

			if ( type=='pint' ) { // 正の整数
				if ( !target.prop('pattern')     ) target.prop('pattern'     , "[+-]?\\d*");
				if ( !target.prop('placeholder') ) target.prop('placeholder' , "半角数字");
				//バリデーション
				target.on('change' , function() {
					target[0].setCustomValidity(''); // 初期化は必ず行う
					if ( target.val() != '' && parseInt(target.val()) <= 0 ) target[0].setCustomValidity('正の整数にしてください');
				});
			}
		});

		// 対象がチェックボックスの場合でreq指定の場合
		// 全くチェックが無ければ全チェックボックスに対しrequiredをセット
		if ( type=='req' || type=='r' ) {
			checkbox_req();
			elms.on('change', checkbox_req); // チェックボックス変更時の処理
		}
		function checkbox_req(e) {
			var checked = false;
			var elms_length = elms.length;
			for ( var i=0;i<elms_length ; i++ ) { // 全チェックボックスに対しチェックされたものが無いかチェック
				if ( elms.eq(i).prop('checked') === true ) checked = true;
			}
			for ( var i=0;i<elms_length ; i++ ) { // 全チェックボックスに対しrequiredの有無をセット
				if ( elms.eq(i).prop('type') == 'checkbox' ) elms.eq(i).prop('required',!checked);
			}
		}
	}



	$.fn.dform_clearValidation = function() {

		var jQueryObject = this;
		jQueryObject.prop('required', false);
		jQueryObject.prop('pattern' , ''   );
		jQueryObject.removeAttr('required');
		jQueryObject.removeAttr('pattern');
//		jQueryObject.unbind();
//		jQueryObject.off('blur change');
		if ( jQueryObject[0] === undefined ) {
			jQueryObject.each(function(){
				this.get(0).setCustomValidity(''); // 初期化
				this.get(0).noValidate = true;
			});
		} else {
			jQueryObject[0].setCustomValidity(''); // 初期化
			jQueryObject[0].noValidate = true;
		}

		return this;
	}



	$.fn.dform_toWide = function() {
		this.val( widenarrow.ToWideAll(this.val() , true) );
	}



	$.fn.dform_clearAllValidate = function() {
		var formelm = this;
		formelm.find('input,select,textarea').each(function(idx,elm) {
			var target  = $(elm);
			target.prop('required',false);
			target.prop('pattern' ,'');
			target.removeAttr('required');
			target.removeAttr('pattern');
//			target.unbind();
		});
	}
})( jQuery );
