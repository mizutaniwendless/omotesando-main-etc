<?php
//====================================================================
// メール操作関連ヘルパークラス
//   PHPMailerが必要です
//====================================================================
class mailHelper {

	private $conf;
	private $mail;
	public  $errorInfo;
	public  $debugLog;
	public  $originalValues; // セット時の情報（エンコード等処理前の情報）

	//====================================================================
	// コンストラクタ
	//====================================================================
	function __construct($conf) {

		$this->conf      = $conf;
		$this->mail      = new PHPMailer;
		$this->errorInfo = '';
		$this->debugLog  = array();

		// 言語設定
		$this->mail->CharSet     = isset($this->conf['mail']['charset'    ]) ? $this->conf['mail']['charset'    ] : 'iso-2022-jp';
		$this->mail->Encoding    = isset($this->conf['mail']['encoding'   ]) ? $this->conf['mail']['encoding'   ] : '7bit'; // "8bit", "7bit", "binary", "base64", and "quoted-printable".
		$this->mail->ContentType = isset($this->conf['mail']['contenttype']) ? $this->conf['mail']['contenttype'] : 'text/plain'; // text/plain(default) , text/html , multipart/alternative

		// SMTP設定(未設定の場合はsendmail使用)
		if ( isset($this->conf['mail']['smtp']['hostname'])
		  && isset($this->conf['mail']['smtp']['port'])
		  && isset($this->conf['mail']['smtp']['protocol']) ) {

			$this->mail->isSMTP();

			$this->mail->host     = $this->conf['mail']['smtp']['hostname'];
			$this->mail->port     = $this->conf['mail']['smtp']['port'];
			$this->mail->protocol = $this->conf['mail']['smtp']['protocol'];

			// 認証情報がある場合はその設定
			if ( isset($this->conf['mail']['smtp']['username'])
			  && isset($this->conf['mail']['smtp']['password']) ) {

				$this->mail->SMTPAuth =  TRUE;
				$this->mail->Username =  $this->conf['mail']['smtp']['username'];
				$this->mail->Password =  $this->conf['mail']['smtp']['password'];
				$this->mail->AuthType = @$this->conf['mail']['smtp']['authtype'];
			}

			// デバッグ情報の設定（0:none 1:echo server message 2:echo server and client message(recommend)）
			$this->mail->SMTPDebug    = isset($this->conf['mail']['smtp']['debug'])     ? $this->conf['mail']['smtp']['debug']     : 0;
			$this->mail->Debugoutput  = isset($this->conf['mail']['smtp']['dbgoutput']) ? $this->conf['mail']['smtp']['dbgoutput'] : 'error_log';

			// デフォルトで行うSSL検証をOFFにする
			if ( @$this->conf['mail']['smtp']['verity_ssl'] !== TRUE ) {
				$this->mail->SMTPOptions  = array('ssl' => array('verify_peer' => FALSE, 'verify_peer_name' => FALSE , 'allow_self_signed' => TRUE));
			}
		}

	}

	//====================================================================
	// CharSet設定(iso-2022-jp/utf-8等)
	//====================================================================
	public function setCharset( $charset ) {
		$this->mail->CharSet     = $charset;
	}

	//====================================================================
	// Encoding設定(8bit,7bit,binary,base64,quoted-printable)
	//====================================================================
	public function setEncoding( $encoding ) {
		$this->mail->Encoding    = $encoding;
	}

	//====================================================================
	// Content-Type設定(text/plain,text/html,multipart/alternative)
	//====================================================================
	public function setContentType( $contenttype ) {
		$this->mail->ContentType = $contenttype;
	}


	//====================================================================
	// From設定
	//====================================================================
	public function setFrom( $address , $name=NULL ) {
		if ( $name === NULL ) {
			$this->originalValues['from'] = array('address'=>$address,'name'=>null);
			$this->mail->setFrom( $address );
		}
		else {
			$this->originalValues['from'] = array('address'=>$address,'name'=>$name);
			$this->mail->setFrom( $address , mb_encode_mimeheader( $name , $this->mail->CharSet ) );
		}
		// Return-Path もここでセットする
		if ( $this->mail->ReturnPath == '' ) {
			$this->mail->ReturnPath = $address;
		}
	}
	public function getFrom($original=TRUE) {
		if ( $original ) return $this->originalValues['from'];
		else             return $this->mail->From;
	}

	//====================================================================
	// Return-Path設定
	//====================================================================
	public function setReturnPath( $address ) {
			$this->mail->ReturnPath = $address;
	}

	//====================================================================
	// To追加(複数回セット可能)
	//====================================================================
	public function addTo( $address , $name=NULL , $clear=FALSE ) {

		if ( $clear === TRUE ) {
			$this->mail->ClearAddresses();  // 過去追加したアドレスをクリア
			$this->originalValues['to'] = array();
		}

		if ( $name === NULL ) {
			$this->originalValues['to'][] = array('address'=>$address,'name'=>null);
			$this->mail->addAddress( $address );
		}
		else {
			$this->originalValues['to'][] = array('address'=>$address,'name'=>$name);
			$this->mail->addAddress( $address , mb_encode_mimeheader( $name , $this->mail->CharSet ) );
		}
	}
	public function getTo($original=TRUE) {
		if ( $original ) return $this->originalValues['to'];
		else             return serialize( $this->mail->getToAddresses() );
	}

	//====================================================================
	// Cc追加(複数回セット可能)
	//====================================================================
	public function addCc( $address , $name=NULL , $clear=FALSE ) {

		if ( $clear === TRUE ) {
			$this->mail->ClearCCs();  // 過去追加したアドレスをクリア
			$this->originalValues['cc'] = array();
		}

		if ( $name === NULL ) {
			$this->originalValues['cc'][] = array('address'=>$address,'name'=>null);
			$this->mail->addCC( $address );
		}
		else {
			$this->originalValues['cc'][] = array('address'=>$address,'name'=>$name);
			$this->mail->addCC( $address , mb_encode_mimeheader( $name , $this->mail->CharSet ) );
		}
	}
	public function getCc($original=TRUE) {
		if ( $original ) return $this->originalValues['cc'];
		else             return serialize( $this->mail->getCcAddresses() );
	}

	//====================================================================
	// Bcc追加(複数回セット可能)
	//====================================================================
	public function addBcc( $address , $name=NULL , $clear=FALSE ) {

		if ( $clear === TRUE ) {
			$this->mail->ClearBCCs();  // 過去追加したアドレスをクリア
			$this->originalValues['bcc'] = array();
		}

		if ( $name === NULL ) {
			$this->originalValues['bcc'][] = array('address'=>$address,'name'=>null);
			$this->mail->addBCC( $address );
		}
		else {
			$this->originalValues['bcc'][] = array('address'=>$address,'name'=>$name);
			$this->mail->addBCC( $address , mb_encode_mimeheader( $name , $this->mail->CharSet ) );
		}
	}
	public function getBcc($original=TRUE) {
		if ( $original ) return $this->originalValues['bcc'];
		else             return serialize( $this->mail->getBccAddresses() );
	}

	//====================================================================
	// ReplyTo追加(複数回セット可能)
	//====================================================================
	public function addReplyTo( $address , $name=NULL , $clear=FALSE ) {

		if ( $clear === TRUE ) {
			$this->mail->ClearReplyTos();   // 過去追加したアドレスをクリア
			$this->originalValues['replyto'] = array();
		}

		if ( $name === NULL ) {
			$this->originalValues['replyto'][] = array('address'=>$address,'name'=>null);
			$this->mail->addReplyTo( $address );
		}
		else {
			$this->originalValues['replyto'][] = array('address'=>$address,'name'=>$name);
			$this->mail->addReplyTo( $address , mb_encode_mimeheader( $name , $this->mail->CharSet ) );
		}
	}
	public function getReplyTo($original=TRUE) {
		if ( $original ) return $this->originalValues['replyto'];
		else             return serialize( $this->mail->getReplyToAddresses() );
	}

	//====================================================================
	// 添付ファイル追加
	//====================================================================
	public function addAttachment( $filename , $name=NULL , $clear=FALSE ) {

		if ( $clear === TRUE ) $this->mail->ClearAttachments();  // 過去追加した添付ファイルをクリア

		if ( $name === NULL ) {
			$this->mail->addAttachment( $filename );
		}
		else {
			$this->mail->addAttachment( $filename , mb_encode_mimeheader(mb_convert_encoding( $name , $this->mail->CharSet )) );
		}
	}

	//====================================================================
	// Subject設定
	//====================================================================
	public function setSubject( $subject ) {

		$this->originalValues['subject'] = $subject;
		$this->mail->Subject = mb_encode_mimeheader($subject, $this->mail->CharSet);

	}
	public function getSubject($original=TRUE) {
		if ( $original ) return $this->originalValues['subject'];
		else             return $this->mail->Subject;
	}

	//====================================================================
	// Body設定
	//====================================================================
	public function setBody( $plainbody , $htmlbody=NULL ) {

		$this->originalValues['body'] = array('plain'=>$plainbody,'html'=>$htmlbody);
		if ( $htmlbody===NULL ) { // plain text
			$this->mail->isHTML(FALSE);
			$this->mail->Body    = mb_convert_encoding($plainbody , $this->mail->CharSet);
		}

		else {
			$this->mail->isHTML(TRUE);
			$this->mail->Body    = mb_convert_encoding($htmlbody  , $this->mail->CharSet);
			$this->mail->AltBody = mb_convert_encoding($plainbody , $this->mail->CharSet);
		}
	}
	public function getBody($original=TRUE) {
		if ( $original ) return $this->originalValues['body'];
		else             return $this->mail->Body;
	}

	//====================================================================
	// 送信
	//====================================================================
	public function send() {

		if( !$this->mail->send() ) return $this->mail->ErrorInfo;
		return TRUE;
	}



	//====================================================================
	// メールテンプレートを用いたセット処理
	//====================================================================
	function setFromTemplate($str) {

		//-----------------------------------
		// 初期化
		//-----------------------------------
		$header = array();
		$params = array();
		$body   = '';
		$emails = array();

		//-----------------------------------
		// メールテンプレートを行で分解
		//-----------------------------------
		// 改行コードで分ける
		$lines = explode( "\n" , str_replace( array("\r\n" , "\r") , $this->mail->LE , $str ) );

		// 各データに分解
		$flg_header = TRUE;
		foreach ( $lines as $buf ) {
			$buf_trim = trim($buf);
			$buf_trim_r = explode( ':' , $buf_trim , 2 );
			$headername = trim(strtolower(@$buf_trim_r[0]));

			if     ($headername == 'from'        && $flg_header) $header['From']        = trim(@$buf_trim_r[1]);
			elseif ($headername == 'to'          && $flg_header) $header['To']          = trim(@$buf_trim_r[1]);
			elseif ($headername == 'cc'          && $flg_header) $header['Cc']          = trim(@$buf_trim_r[1]);
			elseif ($headername == 'bcc'         && $flg_header) $header['Bcc']         = trim(@$buf_trim_r[1]);
			elseif ($headername == 'reply-to'    && $flg_header) $header['Reply-To']    = trim(@$buf_trim_r[1]);
			elseif ($headername == 'return-path' && $flg_header) $header['Return-Path'] = trim(@$buf_trim_r[1]);
			elseif ($headername == 'subject'     && $flg_header) $header['Subject']     = trim(@$buf_trim_r[1]);
			// 以下は厳密にはメールヘッダーではないが、メールエンコード指定など行う場合に使う設定
			elseif ($headername == 'charset'     && $flg_header) $params['charset']     = trim(@$buf_trim_r[1]);
			elseif ($headername == 'encoding'    && $flg_header) $params['encoding']    = trim(@$buf_trim_r[1]);
			elseif ($headername == 'contenttype' && $flg_header) $params['contenttype'] = trim(@$buf_trim_r[1]);
			else {
				if (!$flg_header)      $body           .= $buf.$this->mail->LE; // 最初の空行のみ取り込まないかたちでBodyデータに取り込む
				if ( $buf_trim == "" ) $flg_header      = FALSE;                // 空行が出た時点でヘッダー終了とみなす
			}
		}

		// CharSet,Encodingは最初にセットする（setBodyで使うため）
		if ( isset($params['encoding']) ) $this->setEncoding($params['encoding']);
		if ( isset($params['charset'] ) ) $this->setCharSet ($params['charset' ]);

		// 未設定ヘッダー情報を削除
		if ( isset($header['From'])        && trim($header['From'])        == '' ) unset($header['From']);
		if ( isset($header['To'])          && trim($header['To'])          == '' ) unset($header['To']);
		if ( isset($header['Cc'])          && trim($header['Cc'])          == '' ) unset($header['Cc']);
		if ( isset($header['Bcc'])         && trim($header['Bcc'])         == '' ) unset($header['Bcc']);
		if ( isset($header['Reply-To'])    && trim($header['Reply-To'])    == '' ) unset($header['Reply-To']);
		if ( isset($header['Return-Path']) && trim($header['Return-Path']) == '' ) unset($header['Return-Path']);
		if ( isset($header['Subject'])     && trim($header['Subject'])     == '' ) unset($header['Subject']);

		// 配送先格納配列初期化
		$addresses = array();

		// メールアドレスを抽出
		if (isset($header['To']))          $emails['To']          = $this->getEmailsFromMailHeader($header['To']);
		if (isset($header['Cc']))          $emails['Cc']          = $this->getEmailsFromMailHeader($header['Cc']);
		if (isset($header['Bcc']))         $emails['Bcc']         = $this->getEmailsFromMailHeader($header['Bcc']);
		if (isset($header['From']))        $emails['From']        = $this->getEmailsFromMailHeader($header['From']);
		if (isset($header['Reply-To']))    $emails['Reply-To']    = $this->getEmailsFromMailHeader($header['Reply-To']);
		if (isset($header['Return-Path'])) $emails['Return-Path'] = $this->getEmailsFromMailHeader($header['Return-Path']);


		//-----------------------------------
		// mailオブジェクトへのセット
		//-----------------------------------
		// MAIL FROM設定(1つだけ)
		if ( isset($emails['From'][0]['address']) ) {
			if ( @$emails['From'][0]['name'] == '' ) {
				$this->setFrom( $emails['From'][0]['address'] );
			}
			else {
				$this->setFrom( $emails['From'][0]['address'] , $emails['From'][0]['name'] );
			}
		}
		else {
			// Fromが無い場合はエラー
			return 'mailfrom_not_found';
		}

		// Return-Path設定(1つだけ)
		if ( isset($emails['Return-Path'][0]['address']) ) {
			$this->setReturnPath( $emails['Return-Path'][0]['address'] );
		}

		// toを格納(複数指定可能)
		$wk_count = 0;
		foreach ($emails['To']  as $wk_email) {
			if     ( trim($wk_email['address'])  != '' && trim($wk_email['name'])  != '' ) {
				$this->addTo( trim($wk_email['address']) , trim($wk_email['name']) );
				$wk_count += 1;
			}
			elseif ( trim($wk_email['address'])  != '' ) {
				$this->addTo( trim($wk_email['address']) );
				$wk_count += 1;
			}
		}
		if ( $wk_count == 0 ) return 'mailto_not_address';

		// ccを格納(複数指定可能)
		if(isset($emails['Cc'])){
			foreach ($emails['Cc']  as $wk_email) {
				if     ( trim($wk_email['address'])  != '' && trim($wk_email['name'])  != '' ) {
					$this->addCc( trim($wk_email['address']) , trim($wk_email['name']) );
				}
				elseif ( trim($wk_email['address'])  != '' ) {
					$this->addCc( trim($wk_email['address']) );
				}
			}
		}

		// bccを格納(複数指定可能)
		if(isset($emails['Bcc'])){
			foreach ($emails['Bcc']  as $wk_email) {
				if     ( trim($wk_email['address'])  != '' && trim($wk_email['name'])  != '' ) {
					$this->addBcc( trim($wk_email['address']) , trim($wk_email['name']) );
				}
				elseif ( trim($wk_email['address'])  != '' ) {
					$this->addBcc( trim($wk_email['address']) );
				}
			}
		}

		// replytoを格納(複数指定可能)
		if(isset($emails['Reply-To'])){
			foreach ($emails['Reply-To']  as $wk_email) {
				if     ( trim($wk_email['address'])  != '' && trim($wk_email['name'])  != '' ) {
					$this->addReplyTo( trim($wk_email['address']) , trim($wk_email['name']) );
				}
				elseif ( trim($wk_email['address'])  != '' ) {
					$this->addReplyTo( trim($wk_email['address']) );
				}
			}
		}

		// 件名
		$this->setSubject( $header['Subject'] );
		// 本文
		if ( strtolower(@$params['contenttype']) == 'text/html' ||  strtolower(@$params['contenttype']) == 'multipart/alternative' ) {
			$this->setBody   ( $body , str_replace("\n","<br>",$body) );
			$this->setContentType('multipart/alternative'); // htmlの場合、プレインテキストも混在させるためこうするしかない
		} else {
			$this->setBody   ( $body );
		}

		return TRUE;
	}

	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	// メールヘッダー記載のメールアドレス（複数可、表示名混在可）を配列にセット
	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	function getEmailsFromMailHeader($email) {
		// メールアドレスの正規表現
		// ※ローカル部(「@」の前)
		// 　quoted-stringは拾えません（RFC5321 4.1.2より非推奨との事なのでご勘弁を）
		// 　「.」(先頭末尾以外&連続不可)のチェックはしていません（ルール違反ですが拾えます）
		$ppat_local  = '[!#$%&\'*+-/=?^_`{|}~.a-zA-Z0-9]+';
		
		// ※ドメイン部(「@」の後)
		// 　最初がアルファベットか数字で始まる１文字以上の文字列（数字,アルファベット,「-」「.」）
		// 　厳密には「..」などはＮＧだが拾えます
		$ppat_domain = '[a-zA-Z0-9]{1}[-.a-zA-Z0-9]*';
		
		$ppat_email  = $ppat_local.'@'.$ppat_domain;
		$ppat_comma  = '[ ]*[,]{0,1}[ ]*';
		
		// To , Cc , Bcc , Reply-Toで使用可能。但し「foobar: 複数メールアドレス;」は非対応
		$ppat_emails = '"'.$ppat_comma.'('.$ppat_email.')|'.$ppat_comma.'(.*?)[ ]*<('.$ppat_email.')>"s';
		
		preg_match_all( $ppat_emails , $email  , $matches , PREG_SET_ORDER);
	
		$emails = array();
		foreach ( $matches as $match ) {
			if     ( count($match) == 2 ) $emails[] = array( 'address' => $match[1] , 'name' => '' ); // メールアドレスのみのパターン
			elseif ( count($match) == 4 ) $emails[] = array( 'address' => $match[3] , 'name' => $match[2] ); // メールアドレス＋表示名のパターン
		}
		return $emails;
	}

}
