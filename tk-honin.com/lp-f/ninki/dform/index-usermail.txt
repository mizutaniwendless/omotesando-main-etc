From:    info@tk-honin.com
To:      <?php mailaddress($email_req); ?> 
Subject: [表参道スキンクリニック]無料カウンセリングのご予約を受け付けました

<?php text($name_req); ?> 様
表参道スキンクリニックです。
このたびは無料カウンセリングにお申込みいただき、ありがとうございます。

当医院スタッフより、折り返しお電話にて確認のご連絡をさせていただきます。
*お電話でご相談の日時を確認後、予約が確定となります。

～ご注意～
本メールを受信された段階では、予約は確定しておりません。
本メールは自動で送信しておりますため、返信されましても、対応いたしかねますのでご了承ください。

■アクセス
地図：http://tk-honin.com/access/index.html
電話番号：0120-334-270
※当日場所がわからない場合はお電話でお問い合わせください

【お名前】
<?php text($name_req); ?> 

【電話番号】
<?php text($tel_req); ?> 

【メールアドレス】
<?php text($email_req); ?> 

【年齢】
<?php text($age_req); ?> 歳

【ご相談内容（複数選択可）】
<?php text($soudan_req); ?> 

【相談日　第一希望】
<?php text($day1_req); ?> <?php text($day1_req_period); ?> 

【相談日　第二希望】
<?php text($day2); ?> <?php text($day2_period); ?> 

【連絡方法のご希望】
<?php text($howtocontact); ?> 

【お問い合わせ内容】
<?php text($message); ?> 
