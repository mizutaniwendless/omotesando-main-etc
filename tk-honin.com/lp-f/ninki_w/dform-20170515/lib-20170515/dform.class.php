<?php
//-------------------------------------------------------------------------
// DForm -- design-free mail form system
// 
// (C) 2016 en-pc service. All Rights Reserved.
// This code cannot be redistributed without permission from www.en-pc.jp.
// http://www.en-pc.jp/
//-------------------------------------------------------------------------
// DForm Main Library
//-------------------------------------------------------------------------
// includeパス設定(主にPEAR用)
set_include_path( dirname(__FILE__) . DIRECTORY_SEPARATOR . 'pear' . PATH_SEPARATOR . get_include_path() );
if (version_compare(PHP_VERSION, '5.1.2') < 0) require_once("Net/DNS.php");
else require_once("Net/DNS2.php");

//メール送信ライブラリ
require_once("PHPMailer/PHPMailerAutoload.php");
require_once("mailHelper.class.php");

class dform {
	var $REQUEST;                     // GET+POST
	var $baseName;                    // ベース名（各所で使われるシステム共通の名前）
	var $templatedir;                 // テンプレート設置ディレクトリ
	var $sessionExpire     = 10800;   // セッションデータ保存期間（秒数で指定）
	var $sessionExpireRate = 100;     // セッションデータ破棄実行確率（100であれば100回に1回行う）
	var $sessdir;                     // セッションデータ保存ディレクトリ
	var $sessionBaseName;             // セッション変数ベース名
	var $next;                        // 次に表示させたいテンプレート
	var $formname;                    // フォーム名
	var $reqdata;                     // 画面入力値を保存する変数
	var $templateSet;                 // テンプレートファイル一覧
	var $inputIdx          = -1;      // 入力テンプレートの最終位置
	var $confirmIdx        = -1;      // 確認テンプレートの最終位置
	var $completeIdx       = -1;      // 完了テンプレート位置
	var $inputText;                   // 入力テンプレートを意味するテキスト
	var $confirmText;                 // 確認テンプレートを意味するテキスト
	var $completeText;                // 完了テンプレートを意味するテキスト
	var $completeURL       = '';      // 完了ＵＲＬ（テンプレートではなくリダイレクトさせたい場合に記述します）
	var $mailText;                    // 汎用メールテンプレートを意味するテキスト
	var $suffix_html       = '.html'; // テンプレートファイル(HTML)の拡張子    2014.07.04 デフォルトを.tpl→.htmlに変更
	var $suffix_mail       = '.txt';  // テンプレートファイル(メール)の拡張子
	var $suffix_log        = '.csv';  // ログファイルの拡張子
	var $suffix_seq        = '.seq';  // 連番管理ファイルの拡張子
	var $templatename_glue = '-';     // テンプレート名と状態(next値)をつなげる文字列
	var $name_delimiter    = '_';     // フォームのname値を分ける文字列(分けたものはバリデートに使われます)
	var $noTemplatesUrl    = '';      // テンプレート名未指定時にリダイレクトさせるURL
	var $allowSkipPage     = FALSE;   // 次ページより先への遷移を許可するかどうか（通常はFALSEで。入力途中で送信完了などさせる場合にTRUEを設定します。但しそのページでのバリデーションがＯＫな場合に限る）
	var $internal_encoding = 'UTF-8';
	var $log_encoding      = 'sjis-win';
	var $mb_language       = 'japanese';
	var $request_encoding;
	var $template_encoding;
	var $logfile           = '';      // メール送信ログファイル名（内部で使用：ここで定義してはいけない）
	var $seqfile           = '';      // 連番管理ファイル名（内部で使用：ここで定義してはいけない）
	var $emailAttachFile   = TRUE;    // Uploadファイルをメール添付するかどうか(TRUE:添付する)
	var $savedAttachDir    = '';      // Uploadファイルをサーバ上に保存する場合のディレクトリ（最後 / で。保存しない時は''にする)
	var $savedAttachPrefix = '';      // Uploadファイルをサーバ上に保存する場合のファイル名プレフィックス
	var $errors            = array(); // バリデートエラー格納変数
	var $initialValue      = array(); // フォーム初画面表示時にセットするデフォルト値(key値=name,value値=値)
	var $mailcharset       = 'ISO-2022-JP'; // メール送信時の文字コードセット
	var $mailencoding      = 'base64';      // メール送信時の文字エンコーディング base64/Quoted-Printable
	var $mailcontenttype   = 'text/plain';  // メール形式 text/plain , text/html(これ単独は多分ない) , multipart/alternative(添付ファイル等ある場合)

	// 以下、メールドメイン存在チェックに関する設定
	// ドメインチェック時、最初に使用するネームサーバを指定(未指定時は PHPのcheckdnsrr()関数を使用.
	// 値指定時は、指定サーバを使って該当ドメインのネームサーバを最初に調べ、次にそのネームサーバに対し問合せを行う。つまりなるべくローカルキャッシュを用いないようにしている。
	var $dnscheckmethod    = "php";         // php or pear or preg 2015.09.28追加(PHP4の場合は無視されてphpモード固定となる)
	var $nameserver        = "8.8.8.8";     // Google Public DNS サーバ
	var $nameserver_retry  = 5;     // ネームサーバに対し回答が得られなかった問い合わせについて、問い合わせを再送信するまでの秒数
	var $nameserver_retrans= 1;     // ネームサーバに対し回答が得られなかった問い合わせについて、問い合わせを再送信する回数

	var $echo              = TRUE;        // FALSE にすると、$this->main() は 出力(文字列)を返すだけとなる


	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	// コンストラクタ
	// $formname にはテンプレート名を指定（必須）
	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	function __construct($formname='') {

		// magic_quotes_gpcを無効化する
		$this->disable_magic_quotes_gpc();

		// テンプレート名未指定時にリダイレクトさせるURLのデフォルト値として
		// サイト/を設定
		$this->noTemplatesUrl   = "http://{$_SERVER['SERVER_NAME']}/";
		$this->inputText        = 'input';
		$this->confirmText      = 'confirm';
		$this->completeText     = 'complete';
		$this->mailText         = 'mail';
		$this->baseName         = 'dform';
		$this->sessionBaseName  = $this->baseName;
		// テンプレートディレクトリ
		$this->templatedir      = dirname(__FILE__) . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR ;

		// セッションデータ保存先ディレクトリ（アップロードファイルの格納先も兼ねる）
		$this->sessdir          = realpath( dirname(__FILE__) . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'sessdata' );
		$this->formname         = $formname;
		// Cookieを除外したいため以下のようにする
		$this->REQUEST          = array_merge($_GET, $_POST);
		// 送信コード取得
		$this->request_encoding = $this->detect_encoding_ja( print_r( $this->REQUEST , TRUE ) );
		// 入力値初期化
		$this->reqdata          = array();
	}

	//__constructからクラス名dformに変更(php4.x)対応の為 2016/05/24
	function dform($formname='') {
		$this->__construct($formname);
	}


	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	// セッション保存データクリア
	// セッションＩＤ指定時は、期間関係なく該当セッションＩＤ分だけ削除
	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	function sessionExpire($session_id=NULL) {

		//---------------------------------------------------------------------------
		// 以下は、該当セッションIDのものを無条件削除する処理です
		//---------------------------------------------------------------------------
		if ( $session_id !== NULL ) {
			$len_session  =  strlen($session_id);

			if ($handle = opendir($this->sessdir)) {
				while (false !== ( $filename = readdir($handle) )) {
					if ( substr( $filename , 0 , $len_session ) == $session_id ) {
						unlink( $this->sessdir . DIRECTORY_SEPARATOR . $filename );
					}
				}
			}
			closedir($handle);
			return;
		}

		//---------------------------------------------------------------------------
		// 以下は、全セッションＩＤについて、保存期限を過ぎたものを削除させる処理です
		//---------------------------------------------------------------------------
		// 実施判断
		if ( mt_rand(0,$this->sessionExpireRate) != 0 ) return;
		
		// 指定時刻より古いものを削除
		if ($handle = opendir($this->sessdir)) {
			while (false !== ($filename = readdir($handle))) {
				$filepath = $this->sessdir . DIRECTORY_SEPARATOR . $filename;
				if ( !is_dir( $filepath ) && ( filemtime($filepath) + $this->sessionExpire ) < time() ) unlink($filepath);
			}
		}
		closedir($handle);
	}



	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	// dform内の関数群を個別に使う場合の初期化処理
	// 　コントローラを機能させず、各種変数のセットのみ行います。
	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	function build_for_tool() {

		//-----------------------------
		// main() の一部分コピー
		//-----------------------------
		// 初期化
		mb_language($this->mb_language);
		mb_internal_encoding($this->internal_encoding);

		// セッションデータ保存先が設定されている場合のみ、それに関する処理を実施する
		if ( $this->sessdir != '' ) {
			// セッションデータ保存先設定
			session_save_path( $this->sessdir );
			// 有効期限切れのセッション保存データクリア
			$this->sessionExpire();
		}

		// セッション開始
		if ( session_id() == '' ) {
			register_shutdown_function('session_write_close');  //2014.07.04 session.save_handlerをデフォルト以外(例：memcache)にする場合、必ずセッション開始前にこの記述が必要
			session_start();
		}
		ob_start();

		// 必須項目値の読み込み
		$this->next          = @$this->REQUEST['next'];

		// 該当セッションの保存データを削除
		if ( $this->sessdir != '' ) {
			$this->sessionExpire( session_id() );
		}

		//-----------------------------
		// displayInit() の一部分コピー
		//-----------------------------
		// 現在の表示位置を初期化
		$_SESSION[$this->sessionBaseName][$this->formname]['templatePosition'] = 0;

		// セッション保存値を初期化
		$_SESSION[$this->sessionBaseName][$this->formname]['formdata'] = array();

		// フォーム初期値をセット
		foreach ( $this->initialValue as $key => $value ) {
			$_SESSION[$this->sessionBaseName][$this->formname]['formdata'][$key] = array(
				'name'  => $key    ,  // name値
				'value' => $value  ,  // value値
				'error' => array()    // エラー情報
			);
		}

		// テンプレートを取得(ファイル有無チェックは行いません)
		if ( !$this->getTemplateSet($this->formname,TRUE) ) {
			$this->noTemplates(); // Location 使う
			exit;
		}
	}



	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	// メイン処理
	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	function main() {

		//----------------------------------------------------------------
		// 初期化
		//----------------------------------------------------------------
		$this->debuglog('main:step00.00:$_SERVER:'.print_r($_SERVER,TRUE));
		if ( function_exists('getallheaders') ) $this->debuglog('main:step00.01:request_headers():'.print_r(getallheaders (),TRUE));
		mb_language($this->mb_language);
		mb_internal_encoding($this->internal_encoding);

		// 2016.07.27 Google Chrome のShortcut icon href="" 対応（２回送信される対応）
		// 2016.07.27時点で、HTTP_ACCEPT が'*/*'であるため、もしこの状態であれば即時終了させるように修正
		if ( @$_SERVER['HTTP_ACCEPT'] == '*/*' && $this->checkDoubleRequest === TRUE ) {
			$this->debuglog('main:step00.01: HTTP_ACCEPT = */* . perhaps double request from browser!quit immediately!');
			exit;
		}

		// セッションデータ保存先が設定されている場合のみ、それに関する処理を実施する
		if ( $this->sessdir != '' ) {
			// セッションデータ保存先設定
			session_save_path( $this->sessdir );
			// 有効期限切れのセッション保存データクリア
			$this->sessionExpire();
		}

		// セッション開始
		if ( session_id() == '' ) {
			register_shutdown_function('session_write_close');  //2014.07.04 session.save_handlerをデフォルト以外(例：memcache)にする場合、必ずセッション開始前にこの記述が必要
			session_start();
		}
		$this->debuglog('main:step00.02:$_SESSION:'.print_r($_SESSION,TRUE));

		ob_start();

		// 必須項目値の読み込み
		$this->next          = @$this->REQUEST['next'];
		$this->debuglog('main:step00.03:next='.$this->next);

		// セッション変数そのものが無い場合は初期化するよう修正
		// 2014/10/15 入力ページを経ずいきなり確認画面に行きたい場合（dform範疇外の入力フォームからの送信を想定）、
		// next未指定の状態がないためセッション変数の初期化タイミングがありませんでした。そのため、セッション変数初期化の
		// タイミングをnext未指定時ではなく、セッション変数そのものが無い場合、というように修正しました
		if ( !isset($_SESSION[$this->sessionBaseName]) || !isset($_SESSION[$this->sessionBaseName][$this->formname]) ) {
			$this->debuglog('main:step00.04');
			$this->initSession();
		}


		//----------------------------------------------------------------
		// フォーム入力途中であれば、初期URLアクセス時(next未指定時)に
		// nextを初期画面にします。
		// ※2016.05.26 Chromeで以下の空文字があった場合に二重送信される
		// 　問題があったため
		//   <link rel="shortcut icon" href="">
		//----------------------------------------------------------------
		if ( $this->next == '' && isset($_SESSION[$this->sessionBaseName][$this->formname]['inProgress']) ) {
			$this->next = $this->inputText;
			$this->debuglog('main:step01.01:next='.$this->next);
		}
		// 上記対応で''でクリアされなくなってしまったため、next=reset を明示する事で消せるようにした
		if ( $this->next == 'reset' ) $this->next = '';



		//----------------------------------------------------------------
		// 初回表示時(next未指定時)
		//----------------------------------------------------------------
		if     ( $this->next == ''     ) {

			$this->debuglog('main:step02.01:no next:init');

//2016.07.13 全部消したら他フォームも消えるからコメントアウト
//			// 該当セッションの保存データを削除
//			if ( $this->sessdir != '' ) {
//				$this->sessionExpire( session_id() );
//			}
			unset($_SESSION[$this->sessionBaseName][$this->formname]);
			$this->debuglog('main:step02.02:$_SESSION:'.print_r($_SESSION,TRUE));

			$this->debuglog('main:step02.03');
			return $this->displayInit();
			exit;
		}



		//----------------------------------------------------------------
		// 以下、データ送信時(next指定時)に動く処理が続きます
		//----------------------------------------------------------------
		$this->debuglog('main:step03.01:check next.formname='.$this->formname);
		// テンプレートを取得
		if ( !$this->getTemplateSet($this->formname) ) { // テンプレートが無ければ終了
			$this->debuglog('main:step03.02:no template file.exit.');
			$this->noTemplates();
			exit;
		}

		// 現状のテンプレート位置を取得
		$curr_templatePosition = intval(@$_SESSION[$this->sessionBaseName][$this->formname]['templatePosition']);
		$this->debuglog('main:step03.03 $curr_templatePosition='.$curr_templatePosition.'('.@$this->templateSet[$curr_templatePosition].')');
		$this->debuglog('main:step03.04 $this->completeIdx='.$this->completeIdx.'('.@$this->templateSet[$this->completeIdx].')');


		//----------------------------------------------------------------
		// 前回完了画面表示時で、next値が「complete」の場合（完了画面表示後リロード時：イレギュラー対応）
		//----------------------------------------------------------------
		if ( $curr_templatePosition == $this->completeIdx ) {
			$this->debuglog('main:step04.01 reloading complete page.');
			$this->debuglog('main:step04.02 $this->next='.$this->next.' , $this->completeText='.$this->completeText);

/// 2016.06.20 修正：入力（html）から確認なしフォーム(next=complete)とした場合に、間違って下記ロジックに入ってしまう（何もチェックせず完了ページを表示させてしまう）問題が発生。
//                    完了ページで再読み込みした時は入力に飛ばして良し、と仕様を変更します
//			if ( $this->next == $this->completeText ) {
			if ( $this->next == $this->completeText && $this->confirmIdx >= 0 && $this->allowSkipPage == FALSE ) { // next=complete指定 & confirmテンプレあり & pageskip不許可時のみ、completeページのリロードとみなす
				$this->debuglog('main:step04.03:display complete using session.formdata_complete');

				$ret = $this->display(TRUE,'formdata_complete'); // 完了ページ表示時点で退避させた値を使う（シーケンス、日付等使いまわすため）
				$this->debuglog('main:step04.04');
				return $ret;
			}

			// それ以外はデータを初期化する
			$this->debuglog('main:step04.04:initSession');
			// 2014/10/15 入力ページを経ずいきなり確認画面に行きたい場合（dform範疇外の入力フォームからの送信を想定）、
			// フォーム送信完了状態であった場合、再度いきなり確認画面に飛ばすとここの条件に合致してしまいます。
			// そうなると今度はバリデーションを行わなくなってしまう問題が起きました。そのため、ここではセッション変数を
			// 初期化するにとどめ（テンプレート位置も初期化してます）、バリデーションなど通常処理を走らせるよう修正しました。
			$this->initSession();
		}
		$this->debuglog('main:step04.05:templateset='.print_r($this->templateSet,true));



		//----------------------------------------------------------------
		// 次に表示したいページのテンプレート位置を取得
		//----------------------------------------------------------------
		$this->debuglog('main:step05.01:get next template position');
		$next_templatePosition = -1;
		$wk_count              =  0;
		foreach ( $this->templateSet as $wk_position => $wk_templateName ) {
			if ( is_numeric($wk_position) ) {
				$wk_count += 1;
				if ( $wk_templateName == ($this->formname . $this->templatename_glue . $this->next . $this->suffix_html) ) $next_templatePosition = intval($wk_position);
			}
		}

		$this->debuglog('main:step05.02:$curr_templatePosition+2='.($curr_templatePosition+2).'('.@$this->templateSet[$curr_templatePosition+2].')');
		$this->debuglog('main:step05.03:$next_templatePosition='.$next_templatePosition.'('.@$this->templateSet[$next_templatePosition].')');

		// 次に表示したいページが現状より２つ先のテンプレートを指定している場合は、不正とみなし現状値に戻す
// 2015.11.18 ページスキップを許可していない場合にチェックを行うよう修正（許可している場合はそのまま指定したページに飛ぶ）
//		if ( $curr_templatePosition+2 <= $next_templatePosition ) {
		if ( $curr_templatePosition+2 <= $next_templatePosition && $this->allowSkipPage === FALSE ) {
			$next_templatePosition =  $curr_templatePosition;
			$this->next            = @$this->templateSet[$next_templatePosition];
			$this->debuglog('main:step05.04:can not skip template.set back to position.$this->next='.$this->next);
		}

		// 次に表示したいページテンプレートが見つからなかった場合は、現状値に戻す
		if ( $next_templatePosition == -1 ) {
			$next_templatePosition =  $curr_templatePosition;
			$this->next            = @$this->templateSet[$next_templatePosition];
			$this->debuglog('main:step05.05:template not found.set back to position.$this->next='.$this->next);
		}



		//----------------------------------------------------------------
		// フォーム値を取得した結果チェックＮＧだった場合の処理
		//----------------------------------------------------------------
		if ( !$this->getInput(@$this->templateSet[$curr_templatePosition]) ) {
			$this->debuglog('main:step06.01:error proceed');
			$this->debuglog('main:step06.02:$curr_templatePosition='.$curr_templatePosition .',$this->confirmIdx='.$this->confirmIdx .',$this->completeIdx='.$this->completeIdx);
			//2015.02.12 チェックＮＧ時は表示位置を初期化する(エラーなのに確認画面へ遷移してしまうことを防ぐ)
			//2015.11.18 現在が確認画面以降の場合のみ表示位置の初期化を行うよう修正（入力が複数ある場合で２ページ目以降でエラーがあった場合に、最初のページに戻されてしまうため）
			if ( ( $curr_templatePosition >= $this->confirmIdx  && $this->confirmIdx  >= 0)
			  || ( $curr_templatePosition >= $this->completeIdx && $this->completeIdx >= 0) ) {
				// 現在の表示位置を初期化
				$_SESSION[$this->sessionBaseName][$this->formname]['templatePosition'] = 0;
				$this->debuglog('main:step06.03:reset position to '.$_SESSION[$this->sessionBaseName][$this->formname]['templatePosition'].'('.@$this->templateSet[$_SESSION[$this->sessionBaseName][$this->formname]['templatePosition']].')');

				// 2016.03.10 バグ修正。次のif文で$curr_templatePosition使っているが、セッション変数しか直していなかった
				$curr_templatePosition = $_SESSION[$this->sessionBaseName][$this->formname]['templatePosition'];
			}
			//2015.11.18 次のページが今のページより前で、確認画面以前であれば、移動ＯＫとする。
			if (  $next_templatePosition < $curr_templatePosition
			  && ($next_templatePosition < $this->confirmIdx || $next_templatePosition < $this->completeIdx) ) {
				$this->debuglog('main:step06.04:move position to '.$next_templatePosition.'('.@$this->templateSet[$next_templatePosition].')');
				// 現在の表示位置を初期化
				$_SESSION[$this->sessionBaseName][$this->formname]['templatePosition'] = $next_templatePosition;
			}
			$this->debuglog('main:step06.05:error end:$_SESSION:'.print_r($_SESSION,TRUE));

			// 画面表示
			$ret = $this->display();
			$this->debuglog('main:step06.06:error display end');
			return $ret;
		}



		//----------------------------------------------------------------
		// 以下チェックＯＫの場合の処理
		//----------------------------------------------------------------
		$this->debuglog('main:step07.01:check ok');

		// 現状が入力の場合&フォーム送信値がゼロ&先に進もうとした場合は不正とみなし、現状値に戻す
		if (  $curr_templatePosition <= $this->inputIdx
		   && count($_SESSION[$this->sessionBaseName][$this->formname]['formdata']) == 0
		   && $curr_templatePosition <  $next_templatePosition ) {

			$next_templatePosition =  $curr_templatePosition;
			$this->next            = @$this->templateSet[$next_templatePosition];
			$this->debuglog('main:step07.02:move position to '.$next_templatePosition.',next:'.$this->next);
		}


		//---------------------------------------------------------------------------
		// 完了処理（一度しか動かない処理です：完了画面をリロードしても動かないです）
		//---------------------------------------------------------------------------
		if ( $next_templatePosition == $this->completeIdx ) {
			$this->debuglog('main:step08.01:complete proceed');
			
			// 連番管理ファイルカウントアップ
			if ( $this->seqfile != '' ) $wk_sequence = $this->countupSequence();
			else                        $wk_sequence = '';
			// 連番情報をセッション変数に格納
			$_SESSION[$this->sessionBaseName][$this->formname]['formdata'][$this->baseName.'_sequence'] = array('value' => $wk_sequence);

			// その他システム関連情報をセット
			$this->setSystemInfo();


			// ファイル保存・メール送信前カスタム処理（ファイルの処理やメール送信前にデータを編集したい場合に追加処理を記述します）
			$this->debuglog('main:step08.02:pre-complete proceed');
			$this->preComplete( $_SESSION[$this->sessionBaseName][$this->formname]['formdata'] );


			// ファイル保存
			$this->debuglog('main:step08.03:saveAttachedFiles proceed');
			if ( $this->savedAttachDir != '' ) $this->saveAttachedFiles();

			//汎用メールテンプレートを用いたメール送信
			if ( isset($this->templateSet[$this->mailText]) && count($this->templateSet[$this->mailText]) > 0) {
				foreach ( $this->templateSet[$this->mailText] as $mailTemplateFile ) {
					$this->debuglog('main:step08.04.01:send '.$mailTemplateFile);
					$this->sendmailFromTemplate( $mailTemplateFile , $_SESSION[$this->sessionBaseName][$this->formname]['formdata'] );
				}
			}

			// メール送信完了後カスタム処理（メール送信後に追加処理を行わせたい場合に記述します）
			$this->debuglog('main:step08.02:pre-complete proceed');
			$this->postComplete( $_SESSION[$this->sessionBaseName][$this->formname]['formdata'] );

			// 完了画面に表示させるフォーム値の退避
			// セッション変数（フォーム値のみ）を完了ページ専用セッション変数領域に退避させ、初期化する
			$this->debuglog('main:step08.06:move to formdata to formdata_complete');
			$_SESSION[$this->sessionBaseName][$this->formname]['formdata_complete'] = $_SESSION[$this->sessionBaseName][$this->formname]['formdata'];
			$_SESSION[$this->sessionBaseName][$this->formname]['formdata'] = array();

			// フォーム入力中状態の解除（次回アクセス時にセッション変数をクリアして初期ページに移動できるようにする）
			$this->debuglog('main:step08.07:unset inProgress');
			unset($_SESSION[$this->sessionBaseName][$this->formname]['inProgress']);

			// 完了画面表示処理（使うセッション変数が異なる為、表示をここで行います）
			$_SESSION[$this->sessionBaseName][$this->formname]['templatePosition'] = $next_templatePosition;
			$this->debuglog('main:step08.08:$_SESSION:'.print_r($_SESSION,TRUE));

			// 完了画面のURLが指定されている場合はそこにリダイレクトさせます（優先されます）
			if ( @$this->completeURL != '' ) {
				$this->debuglog('main:step08.09');
				header('Location: ' . $this->completeURL );
				exit;
			}
			// 完了画面のURLが指定されていない場合は完了テンプレートを表示します
			$ret = $this->display(TRUE,'formdata_complete'); // 完了ページ表示時点で退避させた値を使う（シーケンス、日付等使いまわすため）
			$this->debuglog('main:step08.10');
			return $ret;
		}


		//---------------------------------------------------------------------------
		// チェックＯＫ時に動く処理（入力：確認：完了など全てのページが対象）
		//---------------------------------------------------------------------------
		// 画面表示
		$_SESSION[$this->sessionBaseName][$this->formname]['templatePosition'] = $next_templatePosition;
		$this->debuglog('main:step09.01:$_SESSION:'.print_r($_SESSION,TRUE));

		$ret = $this->display();
		$this->debuglog('main:step09.02');
		return $ret;
	}


	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	// 完了処理時（メール送信前）に実行する処理
	// $formdata には 通常
	//  $_SESSION[$obj->sessionBaseName][$obj->formname]['formdata']
	// を参照渡しで渡してください（更新可能）
	// $this(dformオブジェクト)も当然ですが使えます
	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	function preComplete( &$formdata ) {
	}


	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	// 完了処理時（メール送信後）に実行する処理
	// $formdata には 通常
	//  $_SESSION[$obj->sessionBaseName][$obj->formname]['formdata']
	// を参照渡しで渡してください（更新可能）
	// $this(dformオブジェクト)も当然ですが使えます
	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	function postComplete( &$formdata ) {
	}



	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	// 初回表示処理
	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	function displayInit() {

		$this->debuglog('displayInit:step01:init session');
		// セッション変数の初期化
		$this->initSession();

		// テンプレートを取得
		if ( !$this->getTemplateSet($this->formname) ) {
			$this->debuglog('displayInit:step02:no templates found.');
			$this->noTemplates();
			exit;
		}

		// 画面表示
		$this->debuglog('displayInit:step03:display.');
		return $this->display();
	}


	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	// セッション変数の初期化
	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	function initSession() {

		$this->debuglog('initSession:step00');

		// 2016.07.13 完全に削除
		$_SESSION[$this->sessionBaseName][$this->formname] = array();
		unset($_SESSION[$this->sessionBaseName][$this->formname]);

		// 現在の表示位置を初期化
		$_SESSION[$this->sessionBaseName][$this->formname]['templatePosition'] = 0;

		// セッション保存値を初期化
		$_SESSION[$this->sessionBaseName][$this->formname]['formdata'] = array();

		// フォームアクセス状態にセットします (2016.05.26追加)
		$_SESSION[$this->sessionBaseName][$this->formname]['inProgress'] = TRUE;

		// リファラーセット（初回アクセス時のみセット）
		$_SESSION[$this->sessionBaseName][$this->formname]['referer'] = @$_SERVER['HTTP_REFERER'];

		// フォーム初期値をセット
		foreach ( $this->initialValue as $key => $value ) {
			$_SESSION[$this->sessionBaseName][$this->formname]['formdata'][$key] = array(
				'name'  => $key    ,  // name値
				'value' => $value  ,  // value値
				'error' => array()    // エラー情報
			);
			$this->debuglog('initSession:step01:'.$key.' is set');
		}
	}


	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	// テンプレート未指定時の処理
	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	function noTemplates() {
		$this->debuglog('noTemplates:step01:redirect to '.$this->noTemplatesUrl);
		header('Location: '. $this->noTemplatesUrl);
		exit;
	}


	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	// テンプレートファイル群取得
	// $force が TRUEを指す場合は、途中でテンプレートが無くても処理を続けます
	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	function getTemplateSet($formname,$force=FALSE) {

		// 初期化
		$this->templateSet = array();
		$this->completeIdx = -1;
		$template_prefix   = $this->templatedir . $formname;

		//-------------------------------------------------------
		// 入力テンプレート
		//-------------------------------------------------------
		$idx = 0;
		// 最初の入力テンプレートを探す
		if ( is_file($template_prefix . $this->templatename_glue . $this->inputText . $this->suffix_html) ) {
			$this->templateSet[$idx] = $formname . $this->templatename_glue . $this->inputText . $this->suffix_html;
			$this->inputIdx          = $idx;
			$idx++;
		}

		// ２番目以降の入力テンプレートを探す
		$flg_end = FALSE;
		do {
			if ( is_file($template_prefix . $this->templatename_glue . $this->inputText . sprintf("%d",$idx+1) .$this->suffix_html) ) {
				$this->templateSet[$idx] = $formname . $this->templatename_glue . $this->inputText . sprintf("%d",$idx+1) . $this->suffix_html;
				$this->inputIdx          = $idx;
				$idx++;
			}
			else {
				$flg_end = TRUE;
			}
		} while ( $flg_end == FALSE );

		// 入力テンプレートが無ければエラー
		if ( $idx == 0 && !$force ) return FALSE;


		//-------------------------------------------------------
		// 確認テンプレート
		//-------------------------------------------------------
		$idx2 = 0;
		// 最初の入力テンプレートを探す
		if ( is_file($template_prefix . $this->templatename_glue . $this->confirmText . $this->suffix_html) ) {
			$this->templateSet[$idx] = $formname . $this->templatename_glue . $this->confirmText . $this->suffix_html;
			$this->confirmIdx        = $idx;
			$idx++;
			$idx2++;
		}

		// ２番目以降の確認テンプレートを探す
		$flg_end = FALSE;
		do {
			if ( is_file($template_prefix . $this->templatename_glue . $this->confirmText . sprintf("%d",$idx2+1) . $this->suffix_html) ) {
				$this->templateSet[$idx] = $formname . $this->templatename_glue . $this->confirmText . sprintf("%d",$idx2+1) . $this->suffix_html;
				$this->confirmIdx        = $idx;
				$idx++;
				$idx2++;
			}
			else {
				$flg_end = TRUE;
			}
		} while ( $flg_end == FALSE );


		//-------------------------------------------------------
		// 完了テンプレート
		//-------------------------------------------------------
		if ( $this->completeURL == '' ) { // 完了時遷移URLの指定が無い場合は、テンプレート必須
			if ( is_file($template_prefix . $this->templatename_glue . $this->completeText . $this->suffix_html) ) {
				$this->templateSet[$idx] = $formname . $this->templatename_glue . $this->completeText . $this->suffix_html;
				$this->completeIdx       = $idx;
			}
		}
		else { // 完了時遷移URLの指定がある場合は、ダミーのテンプレートファイル名を設定しておく
				$this->templateSet[$idx] = $formname . $this->templatename_glue . $this->completeText . $this->suffix_html;
				$this->completeIdx       = $idx;
		}

		//-------------------------------------------------------
		// メールテンプレート(パターンマッチしたファイル全て対象)
		//-------------------------------------------------------
		$wk_currdir = getcwd();
		chdir($this->templatedir);
		foreach ( @glob($formname . $this->templatename_glue . '*' . $this->suffix_mail) as $filename ) {
			$this->templateSet[$this->mailText][$filename] = $filename;
		};

		//-------------------------------------------------------
		// ログファイル（書き込み可ならメール送信時に作る）
		//-------------------------------------------------------
		if ( is_writable($template_prefix . $this->suffix_log) )  $this->logfile = $template_prefix . $this->suffix_log;
		else                                                      $this->logfile = '';

		//-------------------------------------------------------
		// 連番管理ファイル（書き込み可なら完了時に更新する）
		//-------------------------------------------------------
		if ( is_writable($template_prefix . $this->suffix_seq) )  $this->seqfile = $template_prefix . $this->suffix_seq;
		else                                                      $this->seqfile = '';

		return TRUE;
	}

	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	// テンプレート出力
	// mode_return: FALSEの場合はexitする
	// formdata   : $_SESSION[$this->sessionBaseName][$this->formname][$formdata] の$formdata
	//              フォーム値を変更したい場合に設定する
	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	function display($mode_return=TRUE,$formdata='formdata') {

		$templateName = @$this->templateSet[$_SESSION[$this->sessionBaseName][$this->formname]['templatePosition']];
		$this->debuglog('display:step00:templateName='.$templateName.',formdata(key name)='.$formdata);

		$html = $this->get_include_contents( $this->templatedir . $templateName , @$_SESSION[$this->sessionBaseName][$this->formname][$formdata] );

		if ( $this->echo ) {
			echo $this->mb_convert_encoding_ex( $html , $this->template_encoding , $this->internal_encoding);

			if ( $mode_return===FALSE ) {
				$this->debuglog('display:step01:echo & exit.');
				exit;
			}
			else {
				$this->debuglog('display:step01:echo & return.');
				return;
			}
		}
		else {
				$this->debuglog('display:step02:return.');
				return $this->mb_convert_encoding_ex( $html , $this->template_encoding , $this->internal_encoding);
		}
	}


	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	// フォーム送信値をチェックし、その結果を
	// 内部変数 $_SESSION[$this->sessionBaseName][$this->formname]['formdata'] に格納
	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	function getInput($currentTemplateName) {

		// 初期化
		$this->debuglog('getInput:step00');

		// エラー情報を残すようにしたため、処理前に消さないといけない
		foreach ( $_SESSION[$this->sessionBaseName][$this->formname]['formdata'] as $key => $value ) {
			if ( count($value['error']) > 0 ) {
				unset($_SESSION[$this->sessionBaseName][$this->formname]['formdata'][$key]);
				$this->debuglog('getInput:step01: errordata '.$key.' was unset.');
			}
		}
		

		//-----------------------------------------------------------------------------
		// テキスト値の処理(POST値)
		//-----------------------------------------------------------------------------
		foreach ($this->REQUEST as $key => $value) {

			$this->debuglog('getInput:step02:start to check:'.$key);

			// 特別なname値はスキップ
			if ( $key == 'next' || $key == 'form' ) continue;

			// 文字コード強制変換
			$key   = $this->mb_convert_encoding_ex($key   , $this->internal_encoding , $this->request_encoding);
			// 配列の場合
			if ( is_array($value) ) {
				$new_value = array();
				foreach ( $value as $key2 => $value2 ) {
					if ( !is_array($value2) ) { // ２次元配列の場合は無視
						$key2   = $this->mb_convert_encoding_ex($key2   , $this->internal_encoding , $this->request_encoding);
						$value2 = $this->mb_convert_encoding_ex($value2 , $this->internal_encoding , $this->request_encoding);
						$new_value[$key2] = $value2;
					}
				}
				$value = $new_value;
			}
			// 配列でない場合
			else {
				$value = $this->mb_convert_encoding_ex($value , $this->internal_encoding , $this->request_encoding);
			}

			// エラーチェック＆値修正
			list( $newvalue , $error ) = $this->validateModifyText($key,$value);
			if ( count($error) > 0 ) {
				$this->errors[$key] = $error;
				$this->debuglog('getInput:step03:'.$key.' has errors:'.implode("\t",$error));
			}

			$_SESSION[$this->sessionBaseName][$this->formname]['formdata'][$key] = array(
				'name'  => $key     ,  // name値
				'value' => $newvalue,  // value値
				'error' => $error      // エラー情報
			);
		}

		//-----------------------------------------------------------------------------
		// アップロードファイルの処理
		//-----------------------------------------------------------------------------
		foreach ($_FILES as $key => $value) {

			$this->debuglog('getInput:step04:start to check:'.$key);

			//----------------------------------------------
			// 処理前チェック
			//----------------------------------------------
			// 配列の場合は処理しません
			if ( is_array(@$value['name']) ) continue;

			//----------------------------------------------
			// 初期化
			//----------------------------------------------
			// 文字コード強制変換
			$newvalue          = array(); // 初期化
			$newvalue['name']  = $this->mb_convert_encoding_ex($key           , $this->internal_encoding , $this->request_encoding); // name値
			$newvalue['value'] = $this->mb_convert_encoding_ex($value['name'] , $this->internal_encoding , $this->detect_encoding_ja($value['name'])); // クライアントファイル名
			// その他アップロードファイル格納先情報初期化
			$newvalue['size']  =  0;
			$newvalue['path']  = '';
			$newvalue['type']  = '';
			$newvalue['error'] = array();

			//----------------------------------------------
			// アップロードファイルチェック
			//----------------------------------------------
			// アップロードファイルが指定されなかった場合
			if     ( @$value['error'] == UPLOAD_ERR_NO_FILE ) {
				// $_POST値に同一name値で空値以外の値がセットされている場合は削除指定。
				// それ以外の場合は保持なのでセッション変数に値があればその値を取得する
				if ( @$_SESSION[$this->sessionBaseName][$this->formname]['formdata'][$key.'_delete']['value'] == ''
				  && @$_SESSION[$this->sessionBaseName][$this->formname]['formdata'][$key]['path']            != '' ) {
					$newvalue = @$_SESSION[$this->sessionBaseName][$this->formname]['formdata'][$key];
				}
				else {
					$newvalue['value'] = ''; // ファイル名無しをセット
				}
			}
			// 正常にアップロードされた場合
			elseif ( @$value['error'] == UPLOAD_ERR_OK ) {
				// 保存先にファイルを移動
				$wk_filename   = tempnam($this->sessdir , session_id() . '_').'_'.$newvalue['value'];
				if ( move_uploaded_file( $value['tmp_name'] , $wk_filename ) ) {
					$newvalue['size']    = filesize($wk_filename); // ファイルサイズをセット
					$newvalue['path']    = $wk_filename;           // ファイルパス
					$newvalue['type']    = $value['type'];         // ファイルタイプをセット
				}
				// 保存先へのファイル移動が失敗した場合
				else {
					$newvalue['value']   = ''; // ファイル名無しをセット
					$newvalue['error'][] = 'systemerror';
				}
			}
			// アップロードされたファイルがシステム上限値を超えた場合
			elseif ( @$value['error'] == UPLOAD_ERR_INI_SIZE || @$value['error'] == UPLOAD_ERR_FORM_SIZE ) {
					$newvalue['value']   = ''; // ファイル名無しをセット
					$newvalue['error'][] = 'systemsize';
			}
			// アップロードされたファイルが途中で切れていた、ファイル書き込み失敗などシステム上問題があった場合
			else {
					$newvalue['value']   = ''; // ファイル名無しをセット
					$newvalue['error'][] = 'systemerror';
			}

			//----------------------------------------------
			// ファイルアップロードエラー無し時は、個別チェック
			//----------------------------------------------
			if ( count($newvalue['error']) == 0 ) $newvalue = $this->validateModifyFile($newvalue);
				
			//----------------------------------------------
			// チェック完了後の処理
			//----------------------------------------------
			if ( count($newvalue['error']) == 0 ) {
				// ファイルだけは個々にセッション変数に格納する
				// （他項目がエラーとなって入力画面に戻った時にまたUploadしなおしとなるのを防ぐため）
				$_SESSION[$this->sessionBaseName][$this->formname]['formdata'][$key] = $newvalue;
				$this->debuglog('getInput:step05:'.$key.' has no error');
			}
			else {
				// 最終的にUploadエラーとなったものは、保存したファイルを削除し、データもエラー値以外はクリアします
				$_SESSION[$this->sessionBaseName][$this->formname]['formdata'][$key] = $newvalue;
				if (is_file($newvalue['path'])) unlink($newvalue['path']);

				$newvalue['value'] = '';
				$newvalue['size']  =  0;
				$newvalue['path']  = '';
				$newvalue['type']  = '';
				$this->errors[$key] = $newvalue['error'];
				$this->debuglog('getInput:step06:'.$key.' has errors:'.implode("\t",$error));
			}
		}

		if ( count($_SESSION[$this->sessionBaseName][$this->formname]['formdata']) > 0 ) {
			$this->debuglog('getInput:step07');
			// カスタムバリデートその２（クラス拡張時に定義されたものを使う）
			$this->validateEx($_SESSION[$this->sessionBaseName][$this->formname]['formdata'],$currentTemplateName);
		}

		$isOK = TRUE;
		foreach ( $_SESSION[$this->sessionBaseName][$this->formname]['formdata'] as $key => $value ) {
			if ( count($value['error']) > 0 ) {
				$isOK = FALSE;
				$this->debuglog('getInput:step08:'.$key.' has error:'.implode("\t",$value['error']) );
			}
		}
		return $isOK;
	}


	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	// カスタムバリデート
	// ×$this->reqdata[$key]を使ってください(2016.07.13)
	// $_SESSION[$this->sessionBaseName][$this->formname]['formdata'][$key]を使ってください
	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	function validateEx( &$formdata , $currentTemplateName ) {
	}

	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	// テキスト値バリデート＆モディファイア
	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	function validateModifyText($name , $value) {

		// 初期設定
		$error  = array();
		$stack  = array();

		// name値を「_」($this->name_delimiter)で分解して処理
		foreach ( explode($this->name_delimiter , $name) as $check_pattern ) {

			// 必須チェック
			if     ( $check_pattern == 'r' || $check_pattern == 'req' ) {
				if ( is_array($value) ) { // 配列の場合は、全要素が未入力の場合のみ必須エラーとみなす
					$wk_count = 0;
					foreach ( $value as $v ) if ( $v == '' ) $wk_count += 1;
					if ( count( $value ) == $wk_count ) $error[] = $check_pattern;
				}
				// 非配列の場合は、値が''の場合に必須エラーとみなす
				elseif ( $value == '' )                 $error[] = $check_pattern;
			}

			// 数字チェック(値がある場合、配列以外のみ。全角は半角にしたうえでチェック)
			elseif ( $check_pattern == 'num'  && !is_array($value) && $value != '' ) {
				if ( !is_numeric(mb_convert_kana($value,'a')) )                                          $error[] = $check_pattern;
			}

			// 正の数字チェック(値がある場合、配列以外のみ。全角は半角にしたうえでチェック)
			elseif ( $check_pattern == 'pnum' && !is_array($value) && $value != '' ) {
				if ( !is_numeric(mb_convert_kana($value,'a')) )                                          $error[] = $check_pattern;
				if ( intval(mb_convert_kana($value,'a')) <= 0 )                                          $error[] = $check_pattern;
			}

			// 整数チェック(値がある場合、配列以外のみ。全角は半角にしたうえでチェック)
			elseif ( $check_pattern == 'int'  && !is_array($value) && $value != '' ) {
				if ( !ctype_digit(mb_convert_kana($value,'a')) )                                         $error[] = $check_pattern;
			}

			// 正の整数チェック(値がある場合、配列以外のみ。全角は半角にしたうえでチェック)
			elseif ( $check_pattern == 'pint' && !is_array($value) && $value != '' ) {
				if ( !ctype_digit(mb_convert_kana($value,'a')) )                                         $error[] = $check_pattern;
				if ( intval(mb_convert_kana($value,'a')) <= 0  )                                         $error[] = $check_pattern;
			}

			// 電話番号チェック(値がある場合、配列以外のみ。全角は半角にしたうえでチェック)
			elseif ( ($check_pattern == 'tel' || $check_pattern == 'fax') && !is_array($value) && $value != '' ) {
				$wk_value = trim(mb_convert_kana($value,'a')); // 半角に変換後トリムする
				if ( preg_match( "/^[0-9]{2,5}\-[0-9]{1,5}\-[0-9]{4,5}$/"             , $wk_value) == 0
				  && preg_match( "/^[0-9]{10,11}$/"                                   , $wk_value) == 0
				  && preg_match( "/^\([0-9]{2,5}\)[ ]*[0-9]{1,5}\-[0-9]{4,5}$/" , $wk_value) == 0
				  && preg_match( "/^\([0-9]{2,5}\)[ ]*[0-9]{5,10}$/"            , $wk_value) == 0 )$error[] = $check_pattern;
			}

			// 電話番号チェックハイフン必須(値がある場合、配列以外のみ。全角は半角にしたうえでチェック)
			elseif ( ($check_pattern == 'telh' || $check_pattern == 'faxh') && !is_array($value) && $value != '' ) {
				$wk_value = trim(mb_convert_kana($value,'a')); // 半角に変換後トリムする
				if ( preg_match( "/^[0-9]{2,5}\-[0-9]{1,5}\-[0-9]{4,5}$/"             , $wk_value) == 0 )$error[] = $check_pattern;
			}

			// 郵便番号チェック(値がある場合、配列以外のみ。全角は半角にしたうえでチェック)
			elseif ( ($check_pattern == 'zipcode' || $check_pattern == 'postalcode') && !is_array($value) && $value != '' ) {
				$wk_value = trim(mb_convert_kana($value,'a')); // 半角に変換後トリムする
				if ( preg_match( "/^[0-9]{3}\-[0-9]{4}$/" , $wk_value) == 0
				  && preg_match( "/^[0-9]{7}$/"          , $wk_value) == 0 )                             $error[] = $check_pattern;
			}

			// emailチェック(値がある場合、配列以外のみ)
			elseif ( $check_pattern == 'email' && !is_array($value) && $value != '' ) {
				$value = mb_convert_kana($value,'a');	//2013.08.05 全角を半角に変換する
				$wk_value = explode("@" , $value);
				if ( @$wk_value[1] == '' )                                                               $error[] = $check_pattern;
				// 2013.07.26 ドメインチェック方法を変更
				//elseif ( !checkdnsrr(trim(@$wk_value[1])) )                                            $error[] = $check_pattern;
				//elseif ( !$this->check_domain(trim(@$wk_value[1])) )                                   $error[] = $check_pattern;
				// 2015.09.25 DNSチェック方法を指定できるように変更
				else {
					$wk_domain = trim(@$wk_value[1]);
					if     ( strtolower($this->dnscheckmethod) == 'php' || version_compare(PHP_VERSION, '5.1.2') < 0 ) { // 2016.05.26追加(PHP4は無条件にこっち)
						if ( ! ( checkdnsrr( $wk_domain , 'MX'   )
						      || checkdnsrr( $wk_domain , 'A'    )
						      || checkdnsrr( $wk_domain , 'AAAA' ) ) )                                   $error[] = $check_pattern;
					}
					elseif ( strtolower($this->dnscheckmethod) == 'preg' ) {
						if ( preg_match('|([0-9a-z-]+\.)+[0-9a-z-]+$|', strtolower($wk_domain)) !== 1)   $error[] = $check_pattern;
					}
					else {
						if ( ! ( $this->check_domain($wk_domain , 'MX'   )
							  || $this->check_domain($wk_domain , 'A'    )
							  || $this->check_domain($wk_domain , 'AAAA' ) ) )                           $error[] = $check_pattern;
					}
				}
			}

			// 日付形式チェック（実在日チェックも）
			elseif ( $check_pattern == 'date' && !is_array($value) && $value != '' ) {
				preg_match('|^([0-9]{4})/([0-9]{2})/([0-9]{2})$|' , $value , $matches);
				if ( !checkdate( intval(@$matches[2]) , intval(@$matches[3]) , intval(@$matches[1]) ) )  $error[] = $check_pattern;
			}

			// 同値チェック(値がある場合、配列以外のみ)
			elseif ( $check_pattern == 'eq' && !is_array($value) ) {
				$wk_colname1 = implode($this->name_delimiter , $stack);
				// チェック対象が先に処理されていないと効かない・・
				$wk_value1   = $_SESSION[$this->sessionBaseName][$this->formname]['formdata'][$wk_colname1]['value'];

				if ( $wk_value1 != $value )                                                              $error[] = $check_pattern;
			}

			// trim
			elseif ( $check_pattern == 'trim' ) {
				if ( is_array($value) ) {
					$new_value = array();
					foreach ( $value as $key2 => $value2 ) $new_value[$key2] = $this->mb_trim($value2);
					$value = $new_value;
				} else {
					$value = $this->mb_trim($value);
				}
			}

			// hankaku
			elseif ( $check_pattern == 'han' ) {
				if ( is_array($value) ) {
					$new_value = array();
					foreach ( $value as $key2 => $value2 ) $new_value[$key2] = mb_convert_kana($value2,'a');
					$value = $new_value;
				} else {
					$value = mb_convert_kana($value,'a');
				}
			}

			// zenkaku
			elseif ( $check_pattern == 'zen' ) {
				if ( is_array($value) ) {
					$new_value = array();
					foreach ( $value as $key2 => $value2 ) $new_value[$key2] = mb_convert_kana($value2,'AKVS');
					$value = $new_value;
				} else {
					$value = mb_convert_kana($value,'AKVS');
				}
			}

			// 同値チェック用
			$stack[] = $check_pattern;
		}

		return array( $value , $error );
	}


	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	// アップロードファイル値バリデート＆モディファイア
	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	function validateModifyFile($value) {

		// 初期設定
		$error  = array();

		// name値を「_」($this->name_delimiter)で分解して処理
		foreach ( explode($this->name_delimiter , $value['name']) as $check_pattern ) {

			// 必須チェック
			if     ( $check_pattern == 'r' || $check_pattern == 'req' ) {
				if ( $value['size'] == 0 ) $value['error'][] = $check_pattern;
			}

			// ファイルサイズ上限チェック
			if     ( substr($check_pattern , 0 , 3) == 'max' ) {
				// 上限値を取得
				$str_filesize = substr($check_pattern , 3);
				if     ( preg_match( "/^[0-9]+$/"             , $str_filesize) > 0 ) $chk_filesize = intval($str_filesize);
				elseif ( preg_match( "/^[0-9]+[kK]{1}$/"      , $str_filesize) > 0 ) $chk_filesize = intval(substr($str_filesize,0,-1))*1024;
				elseif ( preg_match( "/^[0-9]+[mM]{1}$/"      , $str_filesize) > 0 ) $chk_filesize = intval(substr($str_filesize,0,-1))*1024*1024;
				else $chk_filesize = 0;
				if ( $value['size'] > $chk_filesize ) $value['error'][] = $check_pattern;
			}
		}
		return $value;
	}


	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	// ファイル保存処理
	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	function saveAttachedFiles() {

		if ( is_writable( $this->savedAttachDir) ) {
			foreach( $_SESSION[$this->sessionBaseName][$this->formname]['formdata'] as $key => $value ) {
				if ( isset($value['path']) && is_file($value['path'])) {
					copy( $value['path'] , $this->savedAttachDir.$this->savedAttachPrefix.$value['value'] );
					// 保存先のファイル名など格納
					$_SESSION[$this->sessionBaseName][$this->formname]['formdata'][$key]['fullpath'] = $this->savedAttachDir.$this->savedAttachPrefix.$value['value'];
					$_SESSION[$this->sessionBaseName][$this->formname]['formdata'][$key]['filename'] = $this->savedAttachPrefix.$value['value'];
				}
			}
		}
	}


	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	// メール送信処理
	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	function sendmailFromTemplate($templateName,$formdata=array()) {

		// メール送信オブジェクト作成
		$mail = new mailHelper(array(
			'charset'           => $this->mailcharset     , // iso-2022-jp,utf-8等
			'encoding'          => $this->mailencoding    , // '7bit'; // "8bit", "7bit", "binary", "base64", and "quoted-printable".
			'contenttype'       => $this->mailcontenttype , // text/plain , text/html , multipart/alternative
		));

		// テンプレートを取得
		$ret = $mail->setFromTemplate( $this->get_include_contents( $this->templatedir . $templateName , $formdata ) );

		// From,Toを取得出来ない場合はメール送信処理を行わない
		if ( $ret !== TRUE ) return;

		// 添付ファイル
		if ( $this->emailAttachFile === TRUE ) {
			foreach( $formdata as $key => $value ) {
				if (isset($value['path']) && is_file($value['path'])) {
					$mail ->addAttachment( $value['path'] , $value['value'] );
				}
			}
		}

		$ret = $mail->send();
		$this->writeLogAndError(
			array(
				'From'     => serialize($mail->getFrom()) ,
				'To'       => serialize($mail->getTo()) ,
				'Cc'       => serialize($mail->getCc()) ,
				'Bcc'      => serialize($mail->getBcc()) ,
				'Reply-To' => serialize($mail->getReplyTo()) ,
				'Subject'  => serialize($mail->getSubject()) 
			) , serialize($mail->getBody()) , $templateName , $ret , ($ret===TRUE?'':serialize($mail))
		);

		return $ret;
	}


	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	// メール送信ログ作成、エラー時の表示制御
	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	function writeLogAndError( $header , $body , $templateName , $isOK , $errorInfo ) {

		$logdata = $this->mb_convert_encoding_ex('"'.date('Y/m/d H:i:s').
			'","'.$templateName.
			'","'.@$header['From'].
			'","'.@$header['To'].
			'","'.@$header['Cc'].
			'","'.@$header['Bcc'].
			'","'.@$header['Reply-To'].
			'","'.@$header['Subject'].
			'","'.$body.
			'","'.$isOK.
			'","'.$errorInfo.
			'"'."\r\n" , 
			$this->log_encoding ,
			$this->internal_encoding
		);
		if ( $this->logfile != '' && is_writable($this->logfile) && @touch($this->logfile) ) {
			$fp = fopen($this->logfile , "ab");
			if ($fp !== FALSE) {
				fwrite($fp , $logdata);
				fclose($fp);
			}
		}
/* エラー発生時にメール送信を止めない（エラー時の対応について検討要）
		if ( $isOK===FALSE ) {
			header('Content-Type: text/html; charset='.$this->internal_encoding);
			echo 'Mail Template Filename:'.$templateName."<br />\n";
			echo 'Mail Headers:';
			print_r($header);
			echo 'Mail Body:'.$body."\n";
			echo 'Error Information:';
			print_r($ErrorInfo);
			exit;
		}
*/
	}

	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	// 連番管理ファイルカウントアップ
	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	function countupSequence() {
		$fp = fopen($this->seqfile , "r+");
		flock($fp, LOCK_EX);
		$count = intval(trim(fgets($fp, 64))); //64バイトorEOFまで取得、カウントアップ
		$count++;
		ftruncate($fp, 1);  // ファイルサイズを１にする（念のため）
		rewind($fp);        //ポインタを先頭に、ロックして書き込み
		fputs($fp, $count);
		fclose($fp);        //ファイルを閉じる
		return $count;
	}

	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	// システム関連情報をセッション変数にセット
	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	function setSystemInfo() {
		$_SESSION[$this->sessionBaseName][$this->formname]['formdata'][$this->baseName.'_yyyy']       = array('value' => date('Y'));
		$_SESSION[$this->sessionBaseName][$this->formname]['formdata'][$this->baseName.'_mm']         = array('value' => date('m'));
		$_SESSION[$this->sessionBaseName][$this->formname]['formdata'][$this->baseName.'_dd']         = array('value' => date('d'));
		$_SESSION[$this->sessionBaseName][$this->formname]['formdata'][$this->baseName.'_hh']         = array('value' => date('H'));
		$_SESSION[$this->sessionBaseName][$this->formname]['formdata'][$this->baseName.'_mi']         = array('value' => date('i'));
		$_SESSION[$this->sessionBaseName][$this->formname]['formdata'][$this->baseName.'_ss']         = array('value' => date('s'));
		$_SESSION[$this->sessionBaseName][$this->formname]['formdata'][$this->baseName.'_week']       = array('value' => date('w'));
		$_SESSION[$this->sessionBaseName][$this->formname]['formdata'][$this->baseName.'_useragent']  = array('value' => @$_SERVER['HTTP_USER_AGENT']);
		$_SESSION[$this->sessionBaseName][$this->formname]['formdata'][$this->baseName.'_remoteaddr'] = array('value' => @$_SERVER['REMOTE_ADDR']);
		$_SESSION[$this->sessionBaseName][$this->formname]['formdata'][$this->baseName.'_remotehost'] = array('value' => @$_SERVER['REMOTE_HOST']);
		$_SESSION[$this->sessionBaseName][$this->formname]['formdata'][$this->baseName.'_httphost']   = array('value' => @$_SERVER['HTTP_HOST']);
		$_SESSION[$this->sessionBaseName][$this->formname]['formdata'][$this->baseName.'_referer']    = array('value' => @$_SESSION[$this->sessionBaseName][$this->formname]['referer']);
	}

	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	// 全角スペースも除去するtrim
	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	function mb_trim( $str ) {
		return preg_replace('/^[ 　]*(.*?)[ 　]*$/u' , '$1' , $str);
	}

	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	// ドメインの実在チェック
	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	function check_domain( $search_domain , $type='MX' ) {

		if ( $this->nameserver == '' ) return checkdnsrr( $search_domain ,$type ); // PHP標準機能を使って返す

// 2015.09.29 DNSライブラリ変更(PHP5.5対応)
//		$resolver = new Net_DNS_Resolver();
		$resolver = new Net_DNS2_Resolver();
		$resolver->debug   = 0; // 1:Turn on debugging output to show the query
		$resolver->usevc   = 0; // 1:Force the use of TCP instead of UDP
		$resolver->retry   = $this->nameserver_retry;   // 回答が得られなかった問い合わせについて、問い合わせを再送信するまでの秒数
		$resolver->retrans = $this->nameserver_retrans; // 回答が得られなかった問い合わせについて、問い合わせを再送信する回数

		// step 1.0: get name server from local name server
		$resolver->nameservers = array( $this->nameserver ); // Set the IP addresses of the nameservers to query.
		$response = $resolver->query( $search_domain , 'NS' );      //Get Nameserver
		if ( !$response ) {
			return checkdnsrr( $search_domain , $type ); // PHP標準機能を使って返す
		}
		// step 1.5: get the response and determine nameserver of the domain.
		$nameservers = array();
		foreach ( $response->answer as $wk_answer ) $nameservers[] = $wk_answer->nsdname;
		if ( count($nameservers) == 0 ) {
			return checkdnsrr( $search_domain , $type ); // PHP標準機能を使って返す
		}

		// step 2.0: get MX record from original name server
		$resolver->nameservers = array( $nameservers[0] );
		$response = $resolver->query( $search_domain , $type );
		if (! $response) {
			return checkdnsrr( $search_domain , $type ); // PHP標準機能を使って返す
		}
		// step 2.5: get the response and count the answers.if number of answers > 0 the domain exists.
		return (count($response->answer) > 0);
	}

	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	// 文字エンコーディング変更関数の拡張版（拡張文字対応）
	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	function mb_convert_encoding_ex( $str , $to='UTF-8' , $from='UTF-8' ) {

		// 2016.01.15 変換不要であればそのまま値を返すよう修正(ASCIIが入った時に変換がおかしくなる)
		if ( strtolower($to) == 'ascii'
		  || strtolower($from) == 'ascii'
		  || strtolower($from) == strtolower($to) ) return $str;

		if ( function_exists( 'mb_list_encodings' ) ) {
			$enc_r = mb_list_encodings();
			if ( in_array('SJIS-win' , $enc_r) ) {
				if ( strtolower($to) == 'sjis' )        $to = 'sjis-win';
				if ( strtolower($to) == 'x-sjis' )      $to = 'sjis-win';
				if ( strtolower($to) == 'shift_jis' )   $to = 'sjis-win';
				if ( strtolower($to) == 'shift-jis' )   $to = 'sjis-win';
			}
			if ( in_array('eucJP-win' , $enc_r) ) {
				if ( strtolower($to) == 'euc-jp' )      $to = 'eucjp-win';
				if ( strtolower($to) == 'euc' )         $to = 'eucjp-win';
				if ( strtolower($to) == 'euc_jp' )      $to = 'eucjp-win';
				if ( strtolower($to) == 'eucjp' )       $to = 'eucjp-win';
				if ( strtolower($to) == 'x-euc-jp' )    $to = 'eucjp-win';
			}
			if ( in_array('ISO-2022-JP-MS' , $enc_r) ) {
				if ( strtolower($to) == 'iso-2022-jp' ) $to = 'iso-2022-jp-ms';
			}
		}
		return mb_convert_encoding( $str , $to , $from );
	}

	var $c1,$c2,$c3,$c4,$c5;
	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	// 文字コード検出
	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	function detect_encoding_ja( $str ) {
		$enc = @mb_detect_encoding( $str, 'ASCII,JIS,eucJP-win,SJIS-win,UTF-8' );
		
		// C制御
		$this->c1 = '772W77yR77yO77yQ44CA77yI772D77yJ44CA77yS77yQ77yR77yS44CA772F772O77yN772Q772';
		$this->c2 = 'D44CA772T772F772S772W772J772D772F77yO77yh772M772M44CA77yy772J772H772I772U77';
		$this->c3 = '2T44CA77yy772F772T772F772S772W772F772E77yO44CA772I772U772U772Q77ya77yP77yP7';
		$this->c4 = '72X772X772X77yO772F772O77yN772Q772D77yO772K772Q77yPDQo=';
		$this->c5 = 'WC1QSFAt';
		switch ( $enc ) {
			case FALSE :
			case 'ASCII' :
			case 'JIS' :
			case 'UTF-8' : break;
			case 'eucJP-win' :
				// ここで eucJP-win を検出した場合、eucJP-win として判定
				if ( @mb_detect_encoding( $str, 'SJIS-win,UTF-8,eucJP-win' ) === 'eucJP-win' ) {
					break;
				}
				$_hint = "\xbf\xfd" . $str; // "\xbf\xfd" : EUC-JP "雀"
	
				// EUC-JP -> UTF-8 変換時にマッピングが変更される文字を削除( ≒ ≡ ∫ など)
				mb_regex_encoding( 'EUC-JP' );
				$_hint = mb_ereg_replace( "\xad(?:\xe2|\xf5|\xf6|\xf7|\xfa|\xfb|\xfc|\xf0|\xf1|\xf2)", '', $_hint );
	
				$_tmp = mb_convert_encoding( $_hint, 'UTF-8', 'eucJP-win' );
				$_tmp2 = mb_convert_encoding( $_tmp, 'eucJP-win', 'UTF-8' );
	
				if ( $_tmp2 === $_hint ) {
					// 例外処理( EUC-JP 以外と認識する範囲 )
					if (
						// SJIS と重なる範囲(2バイト|3バイト|iモード絵文字|1バイト文字)
						! preg_match( '/^(?:'
						. '[\x8E\xE0-\xE9][\x80-\xFC]|\xEA[\x80-\xA4]|'
						. '\x8F[\xB0-\xEF][\xE0-\xEF][\x40-\x7F]|'
						. '\xF8[\x9F-\xFC]|\xF9[\x40-\x49\x50-\x52\x55-\x57\x5B-\x5E\x72-\x7E\x80-\xB0\xB1-\xFC]|'
						. '[\x00-\x7E]'
						. ')+$/', $str ) &&
	
						// UTF-8 と重なる範囲(全角英数字|漢字|1バイト文字)
						! preg_match( '/^(?:'
						. '\xEF\xBC[\xA1-\xBA]|[\x00-\x7E]|'
						. '[\xE4-\xE9][\x8E-\x8F\xA1-\xBF][\x8F\xA0-\xEF]|'
						. '[\x00-\x7E]'
						. ')+$/', $str )
					) {
						// 条件式の範囲に入らなかった場合は、eucJP-win として検出
						break;
					}
					// 例外処理2(一部の頻度の多そうな熟語は eucJP-win として判定)
					// (珈琲|琥珀|瑪瑙|癇癪|碼碯|耄碌|膀胱|蒟蒻|薔薇|蜻蛉)
					if ( mb_ereg( '^(?:'
						. '\xE0\xDD\xE0\xEA|\xE0\xE8\xE0\xE1|\xE0\xF5\xE0\xEF|\xE1\xF2\xE1\xFB|'
						. '\xE2\xFB\xE2\xF5|\xE6\xCE\xE2\xF1|\xE7\xAF\xE6\xF9|\xE8\xE7\xE8\xEA|'
						. '\xE9\xAC\xE9\xAF|\xE9\xF1\xE9\xD9|[\x00-\x7E]'
						. ')+$', $str )
					) {
						break;
					}
				}
	
			default :
				// ここで SJIS-win と判断された場合は、文字コードは SJIS-win として判定
				$enc = @mb_detect_encoding( $str, 'UTF-8,SJIS-win' );
				if ( $enc === 'SJIS-win' ) {
					break;
				}
				// デフォルトとして SJIS-win を設定
				$enc = 'SJIS-win';
	
				$_hint = "\xe9\x9b\x80" . $str; // "\xe9\x9b\x80" : UTF-8 "雀"
	
				// 変換時にマッピングが変更される文字を調整
				mb_regex_encoding( 'UTF-8' );
				$_hint = mb_ereg_replace( "\xe3\x80\x9c", "\xef\xbd\x9e", $_hint );
				$_hint = mb_ereg_replace( "\xe2\x88\x92", "\xe3\x83\xbc", $_hint );
				$_hint = mb_ereg_replace( "\xe2\x80\x96", "\xe2\x88\xa5", $_hint );
	
				$_tmp = mb_convert_encoding( $_hint, 'SJIS-win', 'UTF-8' );
				$_tmp2 = mb_convert_encoding( $_tmp, 'UTF-8', 'SJIS-win' );
	
				if ( $_tmp2 === $_hint ) {
					$enc = 'UTF-8';
				}
				// UTF-8 と SJIS 2文字が重なる範囲への対処(SJIS を優先)
				if ( preg_match( '/^(?:[\xE4-\xE9][\x80-\xBF][\x80-\x9F][\x00-\x7F])+/', $str ) ) {
					$enc = 'SJIS-win';
				}
		}
		return $enc;
	}
	

	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	// テンプレートファイル読み込み処理
	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
//	function get_include_contents($___path,$___formdata='formdata') {
	function get_include_contents($___path,$formdata=array()) {

//error_log($___formdata."\n",3,"a.log");
//error_log(print_r($_SESSION[$this->sessionBaseName][$this->formname][$___formdata],TRUE),3,"a.log");

		//===========================================================
		// 以下、テンプレートファイル読み込み処理
		//===========================================================
		// 一度ファイルを読み込み、文字エンコーディングを検出する
		$this->template_encoding = $this->detect_encoding_ja( file_get_contents( $___path ) );

		// ローカル変数にセット(同時に、テンプレートと同じ文字エンコーディングにします)
		if ( is_array( $formdata ) ) {
			foreach ( $formdata as $___key => $___value ) {
				${$___key} = $___value;
				if ( is_array($___value['value']) ) {
					$___wk_value = array();
					foreach ($___value['value'] as $___key2 => $___value2 )
						$___wk_value[$___key2] = $this->mb_convert_encoding_ex( $___value2 , $this->template_encoding , $this->internal_encoding );
					${$___key}['value'] = $___wk_value;
				}
				else {
					${$___key}['value'] = $this->mb_convert_encoding_ex( $___value['value'] , $this->template_encoding , $this->internal_encoding );
				}
			}
		}

		foreach ( $formdata as $___key => $___value ) {
			${$___key} = $___value;
			if ( is_array(@$___value['value']) ) {
				$___wk_value = array();
				foreach ($___value['value'] as $___key2 => $___value2 )
					$___wk_value[$___key2] = $this->mb_convert_encoding_ex( $___value2 , $this->template_encoding , $this->internal_encoding );
				${$___key}['value'] = $___wk_value;
			}
			else {
				${$___key}['value'] = $this->mb_convert_encoding_ex( @$___value['value'] , $this->template_encoding , $this->internal_encoding );
			}
		}
		
		// 余計な変数をテンプレート内に残さない様にします
		if ( isset($___key          ) ) unset($___key);
		if ( isset($___value        ) ) unset($___value);

		// エラーレポートレベル変更（noticeは出さない)
		$___prev_report = error_reporting(E_ERROR | E_WARNING | E_PARSE);

		// ファイルを実行して結果を取得
		$___prev_encoding = mb_internal_encoding();
		// 2016.01.15 asciiだとおかしくなるため
		if ( strtolower($this->template_encoding) != 'ascii' ) mb_internal_encoding($this->template_encoding);
		ob_start();
		include $___path;
		$___str = ob_get_contents();
		ob_end_clean();
		mb_internal_encoding($___prev_encoding);

		// BOMを除去（ある場合）
		$___str = $this->deleteBOM($___str);

		// エラーレポートレベルを元に戻す
		error_reporting($___prev_report);

		// 内部エンコーディングに戻します
		return $this->mb_convert_encoding_ex( $___str , $this->internal_encoding , $this->template_encoding );

	}

	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	// 関数名：disable_magic_quotes_gpc
	// 説明　：magic_quotes_gpc で書き換えられた $_GET , $_POST , $_COOKIE , $_REQUEST の値を
	// 　　　：元に戻します。
	// 　　　　注）この関数は非効率なため、 php.ini , .htaccess等どこかのディレクティブで
	// 　　　　　　無効化することをオススメします。
	// 戻り値：なし
	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	function disable_magic_quotes_gpc() {
		if (get_magic_quotes_gpc()) {
			$process = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
			while (list($key, $val) = each($process)) {
				foreach ($val as $k => $v) {
					unset($process[$key][$k]);
					if (is_array($v)) {
						$process[$key][stripslashes($k)] = $v;
						$process[] = &$process[$key][stripslashes($k)];
					} else {
						$process[$key][stripslashes($k)] = stripslashes($v);
					}
				}
			}
			unset($process);
		}
	}

	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	// UTF8のNon-Standard Characters(外字)を標準文字に変換
	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	function replaceUTF8NSC2SC($str){
		$ret = $str;
		$arr = array(
			'Ⅰ' => 'I'   ,'Ⅱ' => 'II'      ,'Ⅲ' => 'III'       ,'Ⅳ' => 'IV'    ,'Ⅴ' => 'V'   ,
			'Ⅵ' => 'VI'  ,'Ⅶ' => 'VII'     ,'Ⅷ' => 'VIII'      ,'Ⅸ' => 'IX'    ,'Ⅹ' => 'X'   ,
			'ⅰ' => 'i'   ,'ⅱ' => 'ii'      ,'ⅲ' => 'iii'       ,'ⅳ' => 'iv'    ,'ⅴ' => 'v'   ,
			'ⅵ' => 'vi'  ,'ⅶ' => 'vii'     ,'ⅷ' => 'viii'      ,'ⅸ' => 'ix'    ,'ⅹ' => 'x'   ,
			'①' => '(1)' ,'②' => '(2)'     ,'③' => '(3)'       ,'④' => '(4)'   ,'⑤' => '(5)' ,
			'⑥' => '(6)' ,'⑦' => '(7)'     ,'⑧' => '(8)'       ,'⑨' => '(9)'   ,'⑩' => '(10)',
			'⑪' => '(11)','⑫' => '(12)'    ,'⑬' => '(13)'      ,'⑭' => '(14)'  ,'⑮' => '(15)',
			'⑯' => '(16)','⑰' => '(17)'    ,'⑱' => '(18)'      ,'⑲' => '(19)'  ,'⑳' => '(20)',
			'㊤' => '(上)','㊥' => '(中)'    ,'㊦' => '(下)'      ,'㊧' => '(左)'  ,'㊨' => '(右)',
			'㍉' => 'ミリ','㍍' => 'メートル','㌔' => 'キロ'      ,'㌘' => 'グラム','㌧' => 'トン',
			'㌦' => 'ドル','㍑' => 'リットル','㌫' => 'パーセント','㌢' => 'センチ',
			'㎜' => 'mm'  ,'㎝' => 'cm'      ,'㎞' => 'km'        ,
			'㎏' => 'kg'  ,'㎡' => 'm2'      ,'㏍' => 'K.K.'      ,'℡' => 'TEL'   ,'№' => 'No.' ,
			'㍻' => '平成','㍼' => '昭和'    ,'㍽' => '大正'      ,'㍾' => '明治'  ,
			'㈱' => '(株)','㈲' => '(有)'    ,'㈹' => '(代)'      ,
		);
		return str_replace( array_keys( $arr), array_values( $arr), $str);
	}

	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	// BOM除去
	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	function deleteBOM($str){
		if (ord($str{0}) == 0xef && ord($str{1}) == 0xbb && ord($str{2}) == 0xbf) $str = substr($str, 3);
		return $str;
	}

	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	// debugログ出力
	//＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	function debuglog($msg){
		if ( defined('__DFORM_DEBUGLOGFILENAME') && __DFORM_DEBUGLOGFILENAME!='' ) {
			error_log(date('Y-m-d H:i:s ').getmypid()."\t".$msg."\n",3,__DFORM_DEBUGLOGFILENAME);
		}
	}
}
//===========================================================
// テンプレート内で使用する関数群定義
//===========================================================
//HTML用値出力(タグエスケープ)
function html($value,$glue=',',$echo=TRUE) {
	// 値が配列の場合は展開する
	if ( is_array($value['value']) ) {
		$data2 = array();
		foreach ( $value['value'] as $key2 => $value2 ) if ( trim($value2) != '' ) $data2[$key2] = htmlspecialchars($value2);
		// glueが2個以上の配列の場合は、囲む
		if ( is_array($glue) && count($glue) >= 2 ) {
			$data = '';
			foreach ( $data2 as $wk_val ) $data .= $glue[0].$wk_val.$glue[1];
		}
		// glue が文字列、又は1個の配列であれば、挟む
		else {
			if ( is_array($glue) ) $glue = $glue[0];
			$data = implode($data2 , $glue);
		}
	}
	// 値が文字列であれば、そのまんまエスケープするだけ
	else                             $data = htmlspecialchars($value['value']);
	if ($echo) echo   $data;
	else       return $data;
}

// htmlを値で返すラッパー関数
function get_html($value,$glue=',') {
	return html($value,$glue,FALSE);
}

//HTML用値出力(タグエスケープ&BRタグ付与)
function htmlbr($value,$glue=',',$echo=TRUE) {
	if ( is_array($value['value']) ) {
		$data2 = array();
		foreach ( $value['value'] as $key2 => $value2 ) if ( trim($value2) != '' ) $data2[$key2] = nl2br(htmlspecialchars($value2));
		$data = implode($data2 , $glue);
	}
	else                             $data = nl2br(htmlspecialchars($value['value']));
	if ($echo) echo   $data;
	else       return $data;
}

//text用値出力(エスケープなし)
function text($value,$glue=',',$convert_sc=TRUE,$echo=TRUE) { // $convert_sc:標準文字へ変換するかどうか
	// 値が配列の場合は展開する
	if ( is_array($value['value']) ) {
		$data2 = array();
		foreach ( $value['value'] as $key2 => $value2 ) if ( trim($value2) != '' ) $data2[$key2] = $value2;
		// glueが2個以上の配列の場合は、囲む
		if ( is_array($glue) && count($glue) >= 2 ) {
			$data = '';
			foreach ( $data2 as $wk_val ) $data .= $glue[0].$wk_val.$glue[1];
		}
		// glue が文字列、又は1個の配列であれば、挟む
		else {
			if ( is_array($glue) ) $glue = $glue[0];
			$data = implode($data2 , $glue);
		}
	}
	// 値が文字列であれば、そのまんま
	else                             $data = $value['value'];
	if ( $convert_sc ) {
		// UTF8のNon-Standard Characters(外字)を標準文字に変換
		// 外字の標準文字変換と、半角カナ→全角カナ変換
		$arr = array(
			'Ⅰ' => 'I'   ,'Ⅱ' => 'II'      ,'Ⅲ' => 'III'       ,'Ⅳ' => 'IV'    ,'Ⅴ' => 'V'   ,
			'Ⅵ' => 'VI'  ,'Ⅶ' => 'VII'     ,'Ⅷ' => 'VIII'      ,'Ⅸ' => 'IX'    ,'Ⅹ' => 'X'   ,
			'ⅰ' => 'i'   ,'ⅱ' => 'ii'      ,'ⅲ' => 'iii'       ,'ⅳ' => 'iv'    ,'ⅴ' => 'v'   ,
			'ⅵ' => 'vi'  ,'ⅶ' => 'vii'     ,'ⅷ' => 'viii'      ,'ⅸ' => 'ix'    ,'ⅹ' => 'x'   ,
			'①' => '(1)' ,'②' => '(2)'     ,'③' => '(3)'       ,'④' => '(4)'   ,'⑤' => '(5)' ,
			'⑥' => '(6)' ,'⑦' => '(7)'     ,'⑧' => '(8)'       ,'⑨' => '(9)'   ,'⑩' => '(10)',
			'⑪' => '(11)','⑫' => '(12)'    ,'⑬' => '(13)'      ,'⑭' => '(14)'  ,'⑮' => '(15)',
			'⑯' => '(16)','⑰' => '(17)'    ,'⑱' => '(18)'      ,'⑲' => '(19)'  ,'⑳' => '(20)',
			'㊤' => '(上)','㊥' => '(中)'    ,'㊦' => '(下)'      ,'㊧' => '(左)'  ,'㊨' => '(右)',
			'㍉' => 'ミリ','㍍' => 'メートル','㌔' => 'キロ'      ,'㌘' => 'グラム','㌧' => 'トン',
			'㌦' => 'ドル','㍑' => 'リットル','㌫' => 'パーセント','㌢' => 'センチ',
			'㎜' => 'mm'  ,'㎝' => 'cm'      ,'㎞' => 'km'        ,
			'㎏' => 'kg'  ,'㎡' => 'm2'      ,'㏍' => 'K.K.'      ,'℡' => 'TEL'   ,'№' => 'No.' ,
			'㍻' => '平成','㍼' => '昭和'    ,'㍽' => '大正'      ,'㍾' => '明治'  ,
			'㈱' => '(株)','㈲' => '(有)'    ,'㈹' => '(代)'      ,
		);
		$data = str_replace( array_keys($arr), array_values($arr), mb_convert_kana($data,'KV') );
	}
	if ($echo) echo   $data;
	else       return $data;
}

// textを値で返すラッパー関数
function get_text($value,$glue=',',$convert_sc=TRUE) {
	return text($value,$glue,$convert_sc,FALSE);
}

//選択されている場合に「selected」を表示
function selected($value,$baseValue,$selectedValue="selected") {
	if ( $value['value'] == $baseValue ) {
		echo ' selected';
		if ( $selectedValue != '' ) echo '="' . htmlspecialchars($selectedValue) . '"';
	}
}

//該当selectに対応する<option>タグを出力
function option($value,$baseValue,$selectedValue="selected") {
	echo '<option value="' . htmlspecialchars($baseValue) . '"';
	if ( $value['value'] == $baseValue ) {
		echo ' selected';
		if ( $selectedValue != '' ) echo '="' . htmlspecialchars($selectedValue) . '"';
	}
	echo '>';
}

//該当selectに対応する<option>～</option>タグを出力
function optionX($value,$baseValue,$displayValue='',$selectedValue="selected",$attributes="") {
	echo '<option value="' . htmlspecialchars($baseValue) . '"';
	if ( is_array($value['value']) ) {
		foreach ( $value['value'] as $key2 => $value2 ) {
			if ( $value2 == $baseValue ) {
				echo ' selected';
				if ( $checkedValue != '' ) echo '="' . htmlspecialchars($checkedValue) . '"';
				break;
			}
		}
	}
	else {
		if ( $value['value'] == $baseValue ) {
			echo ' selected';
			if ( $selectedValue != '' ) echo '="' . htmlspecialchars($selectedValue) . '"';
		}
	}
	if ( $attributes!='' ) $attributes = ' '.$attributes;
	if ( $displayValue=='') echo $attributes.'>'.htmlspecialchars($baseValue).'</option>';
	else                    echo $attributes.'>'.htmlspecialchars($displayValue).'</option>';
}

//checked
function check($type,$name,$baseValue,$value,$checkedValue="checked") {
	echo 'type="'.$type.'" name="'.htmlspecialchars($name).'" value="'.htmlspecialchars($baseValue).'"';
	if ( is_array($value['value']) ) {
		foreach ( $value['value'] as $key2 => $value2 ) {
			if ( $value2 == $baseValue ) {
				echo ' checked';
				if ( $checkedValue != '' ) echo '="' . htmlspecialchars($checkedValue) . '"';
				break;
			}
		}
	}
	else {
		if ( $value['value'] == $baseValue ) {
			echo ' checked';
			if ( $checkedValue != '' ) echo '="' . htmlspecialchars($checkedValue) . '"';
		}
	}
}

//フォームＰＧ名を出力
function form() {
	$formname = explode( '?' , @$_SERVER['REQUEST_URI'] );
	echo @$formname[0];
}

//エラーかどうか判定
function isError($value) {
	// 通常のパターン
	if ( isset($value['error']) ) {
		if ( count($value['error']) > 0 ) return TRUE;
		return FALSE;
	}
	// $alldata等指定した場合
	if ( is_array($value) ) foreach ($value as $key => $col_value) if ( count($col_value['error']) > 0 ) return TRUE;
	return FALSE;
}

//値が含まれるかどうか判定。第２パラメータ未指定時は値がセットされているかどうかで判定
function have($value,$baseValue=NULL) {
	$have = FALSE;
	// 値が存在するかチェックの場合
	if ( $baseValue === NULL ) {
		if ( is_array($value['value']) ) {
			foreach ( $value['value'] as $key2 => $value2 ) if ( $value2 != '' ) $have = TRUE;
		}
		else {
			if ( $value['value'] != '' ) $have = TRUE;
		}
	}
	// 値が一致するかチェックの場合
	else {
		if ( is_array($value['value']) ) {
			foreach ( $value['value'] as $key2 => $value2 ) if ( $value2 == $baseValue ) $have = TRUE;
		}
		else {
			if ( $value['value'] == $baseValue ) $have = TRUE;
		}
	}
	return $have;
}
