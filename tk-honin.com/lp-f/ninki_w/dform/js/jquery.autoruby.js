/*	
	AutoRuby plugin for $
	Usage:
	  $.autoruby('#target');   // Katakana(カタカナ)
	  $.autoruby('#target',1); // Hiragana(ひらがな)
*/
(function($){
	$.fn.autoruby = function(target,options) {
	
		// 各種設定
		var options = $.extend({
//			autoruby  :true      , // デフォルトautoruby実行
			class_kata:'katakana', // クラス名指定でカタカナ変換にする場合のクラス名
			class_hira:'hiragana', // クラス名指定でひらがな変換にする場合のクラス名
			katakana  :false     , // クラス名未指定時の変換(true:カタカナ変換,false:ひらがな変換)
			interval  : 30         // 自動変換タイマーインターバル(ミリ秒で指定)
			} , options);

		// ひらがな→カタカナ変換
		var convertKatakana  = function(val){
			var c, a = [];
			for(var i=val.length-1;0<=i;i--){
					c = val.charCodeAt(i);
					a[i] = (0x3041 <= c && c <= 0x3096) ? c + 0x0060 : c;
			}
			return String.fromCharCode.apply(null, a);
		}

		return $(this).each(function(){
			var baseVal = "";
			var base    = $(this);
	
			var setRuby = function(){
				// 前回入力値と今回入力値を比較
				var newVal = base.val();
				if (baseVal == newVal){return;}  // 同値の場合は何もしない
				if (newVal  == "") {             // クリアされた場合は、ルビ振り先をクリアし終了
					$(target).val("");
					baseVal = "";
					return;
				}
		
				// 前回入力値と今回入力値の差分を取得
				var addVal = newVal;
				for(var i=baseVal.length; i>=0; i--) {
					if (newVal.substr(0,i) == baseVal.substr(0,i)) {
						addVal = newVal.substr(i);
						break;
					}
				}
		
				// 今回入力値を退避
				baseVal = newVal;
		
				// 今回追加入力分のカナ文字だけ取得
				var addruby = addVal.replace( /[^ 　ぁあ-んァー]/g, "" );
				if (addruby == ""){return;}
		
				// 該当要素すべてに対しルビをふる
				$(target).each(function() {
					// カタカナ変換指定時はカタカナに変換
					var classname = $(this).attr("class");
					var addruby_conv = addruby;
					if ( classname.indexOf(options.class_kata) >= 0 ) addruby_conv = convertKatakana(addruby);
					else if ( classname.indexOf(options.class_hira) < 0 && options.katakana==true ) addruby_conv = convertKatakana(addruby);
		
					// かな文字を指定先に追加
					var newRuby = $(this).val()+ addruby_conv;
					$(this).val(newRuby);
				});
			}

			// ループタイマー（指定秒数毎に差分をチェック）
			var timerObj;
//			if ( options.autoruby == true ) {
				timerObj = setInterval(function(){
					setRuby();
				},options.interval);
//			}
		});
	}
})(jQuery);
