<?php
ini_set('display_errors','OFF');	//OFF
ini_set('display_startup_errors',FALSE);	//FALSE
ini_set('log_errors',TRUE);
//define('__DFORM_DEBUGLOGFILENAME' , 'debug.log'); // デバッグログを出すのであればこの定数にファイル名を定義する
//-------------------------------------------------------------------------
// DForm -- design-free mail form system
// 
// (C) 2011 en-pc service. All Rights Reserved.
// This code cannot be redistributed without permission from www.en-pc.jp.
//-------------------------------------------------------------------------
// DForm Call Program
//-------------------------------------------------------------------------
include_once("dform/lib/dform.class.php" );

$dform = new dform('index');
$dform->main();
