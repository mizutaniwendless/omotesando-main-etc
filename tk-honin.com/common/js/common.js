/*
 * COMMON FUNCTIONS
 */

// header gnav on 
$(document).ready(function() {
	var url = location.href;
		path = url.split("http://"+location.hostname)[1];
	var gnav_menu = path.split("/")[1];//location 2nd Depth
	$('#header #gnav li').each(function(){
		var li_gnav=$(this).find('a').attr('href');
		li_gnav = li_gnav.split(/http:\/\/[^\/]*/).pop().split("/")[1];//alink 2nd Depth
		if(!!li_gnav && !!gnav_menu && gnav_menu == li_gnav){
			$(this).find('img').attr("src", $(this).find('img').attr("src").replace(/^(.+)(\.[a-z]+)$/, "$1_on$2"));
			$(this).find('img').removeClass('rollover');
		} else {
			$(this).find('img').attr("src", $(this).find('img').attr("src").replace(/^(.+)_on(\.[a-z]+)$/, "$1$2"));
			$(this).find('img').addClass('rollover');
		}
	});
});

// sideNavi on current
$(document).ready(function() {
	var url = location.href;
	if(url.indexOf('menu')!=-1){
		var menu_cate = url.replace(/.*menu\//,'');
		if (menu_cate == "" || menu_cate == "index.html") {
			$('#contentsL .sideNavi .sideNaviInner').each(function(){
				$(this).slideUp();
				$(this).parent('dd').removeClass('on');
			});
			$('#contentsL .sideNavi .sideNaviInner li').each(function(){
				$(this).removeClass('current');
			});
		}else {
			$('#contentsL .sideNavi .sideNaviInner li').each(function(){
				$(this).removeClass('current');
				var li_link=$(this).find('a').attr('href');
				li_link = li_link.replace("/menu/","");
				
				if(li_link == menu_cate) {
					$(this).addClass('current');
				}
			});
			var ul_current = $('#contentsL .sideNavi .sideNaviInner li.current').parent().attr('id');
			$('#contentsL .sideNavi .sideNaviInner').each(function(){
				$(this).parent('dd').removeClass('on');
				if($(this).attr('id') == ul_current){
					$(this).parent('dd').addClass('on');
					$(this).slideDown();
				}
			});
		}
	}
});

// Rollover
﻿$(document).ready(function() {
    $("img.rollover,input.rollover").mouseover(function() {
        $(this).attr("src", $(this).attr("src").replace(/^(.+)(\.[a-z]+)$/, "$1_on$2"));
    }).mouseout(function() {
        $(this).attr("src", $(this).attr("src").replace(/^(.+)_on(\.[a-z]+)$/, "$1$2"));
    }).each(function() {
        $("<img>").attr("src", $(this).attr("src").replace(/^(.+)(\.[a-z]+)$/, "$1_on$2"));
    });
});

// Scroll Softly
﻿$(document).ready(function() {
    jQuery.easing.quart = function(x, t, b, c, d) {
        return -c * ((t = t / d - 1) * t * t * t - 1) + b;
    };
    $('a[href^=#]').click(function() {
        var targetOffset = $($(this).attr('href')).offset().top;
        $('html,body').animate({
            scrollTop: targetOffset
        }, 1000, 'quart');
        return false;
    });
});

// handle contents background
﻿$(document).ready(function() {
	//alert($(".wrapperInner").height());
	// if .wrapperInner is too low, hide its background image.
	if ($(".wrapperInner").height() < 864)
	{
		$(".wrapperInner").css('background','none')
	}
});

// sideNavi hover
$(document).ready(function() {
	$(".sideNavi dd.accordionHead span").click(function() {
		var parentId = "#"+($(this).parent().attr("id"));　// idの取得
		var childId = parentId.replace(/Parent/,'');
		//currentの為
		var parentClass = $(this).parent().attr("class");
		if (parentClass.indexOf("accordionHead on") == -1) {
			$(childId).slideDown();
			$(parentId).addClass("on");
		} else {
			$(childId).hide().show();
			$(childId).slideUp();
			$(parentId).removeClass("on");
		}
	});
});


$(document).ready(function() {
	$(".block_link a").each(function(){
	   $(this).attr('onClick','return false;');
    });
	
	$(".block_link").hover(function(e) {
		$(this).addClass('block_over');
	    var target = $('img.block_img_over',this); target.attr('src',target.attr('src').replace(/^(.+)(\.[a-z]+)$/, "$1_on$2"));
	},function(e){
	   $(this).removeClass('block_over');
	   var target = $('img.block_img_over',this); target.attr('src',target.attr('src').replace(/^(.+)_on(\.[a-z]+)$/, "$1$2"));
    });
	
	$(".block_link").click(function() {
		var href = $('a',this).attr('href');
		var target = $('a',this).attr('target');
		if(target == "_blank"){
		    window.open(href);
        }else{
            window.location.href = href;
        }
	});
});

