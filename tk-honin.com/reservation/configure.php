<?php
/**
 * 各種フォーム基本設定
 */
ob_start();
ini_set('display_erors', true); // TOOO
ini_set('log_errors', true);

mb_language('Japanese');
mb_internal_encoding('utf8');

date_default_timezone_set('Asia/Tokyo');

function getCheckboxes($content) {
	$checkboxes = array();
	if (preg_match_all('/<label><input type="checkbox".*?label>/', $content, $matches)) {
		for ($i=0; $i<count($matches[0]); $i++) {
			$match = $matches[0][$i];
			$name = preg_replace('/^.*?name="(.*?)".*?$/', '$1', $match);
			if (preg_match('/^.*?<input.*?>(.*?)<\/label>$/', $match, $m)) {
				$value = preg_replace('/&.*?;/', '', $m[1]);
				$value = str_replace('　', '', $value);
				$checkboxes[$name] = $value;
			}
		}
	}
	return $checkboxes;
}

function getMode() {
	$return = false;
	foreach ($_POST as $k=>$v) {
		if (strpos($k, 'return') !== false) {
			return 'return';
		} else if (strpos($k, 'confirm') !== false) {
			return 'confirm';
		}
	}
}

function replaceConfirm($contents, $options) {
	foreach ($_POST as $k=>$v) {
		if (@$options['textarea'] && in_array($k, $options['textarea'])) {
			$contents = str_replace("%${k}%", nl2br(htmlspecialchars($v)), $contents);
		} else {
			$contents = str_replace("%${k}%", htmlspecialchars($v), $contents);
		}
		$contents = preg_replace('/(<form name="confirm".*?>)/iu', '$1<input type="hidden" name="'.htmlspecialchars($k).'" value="'.htmlspecialchars($v).'" />', $contents);
	}
	return $contents;
}

