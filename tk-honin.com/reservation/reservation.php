<?php
/**
 * 予約フォーム
 * @date 2013-03-15
 */
require_once 'configure.php';

$formfile = 'reservation.html';
$confirmfile = 'reservation-check.html';
$thanksfile = 'reservation-thanks.html';

$formcontent = file_get_contents($formfile);
$checkboxes = getCheckboxes($formcontent);

if (@$_POST) {
	$mode = getMode();

	if ($mode == 'confirm') {
		/*
		 * TODO 終了処理
		 */
		$contents = file_get_contents($thanksfile);
		$htmlfilemtime = time();

	} else {
		$required = array('名前', 'フリガナ', '電話番号1', '電話番号2', '電話番号3', 'メールアドレス');
		$errors = array();
		foreach ($required as $name) {
			if (!@$_POST[$name]) {
				if (strpos($name, '電話番号') !== false) {
					$errors[] = '電話番号';
				} else {
					$errors[] = $name;
				}
			}
		}

		if (@$_POST['メールアドレス'] !== @$_POST['メールアドレス確認']) {
			$errors[] = 'メールアドレス確認';
		}

		$checked = array();
		$dateselected = array(
			'1' => true
			,'2' => true
			,'3' => true
			);
		foreach ($_POST as $k=>$v) {
			if (preg_match('/^checkbox\d+$/', $k)) {
				$checked[] = $k;
			} else if (preg_match('/ご相談希望.*?(\d+)$/', $k, $matches)) {
				if (!$v) $dateselected[$matches[1]] = false;
			}
		}
		if (count($checked) < 3) $errors[] = 'checkbox';
		foreach ($dateselected as $k=>$v) {
			if (!$v) $errors[] = 'ご相談希望日'.$k;
		}

		$htmlfilemtime = time();
		if ($mode == 'return' || $errors) {
			$contents = $formcontent;
			foreach ($_POST as $k=>$v) {
				if (preg_match('/^checkbox\d+$/', $k)) {
					$k = preg_quote(htmlspecialchars($k), '/');
					$contents = preg_replace('/(<input type="checkbox" name="'.$k.'")/iu', '$1 checked="checked"', $contents);
				} else if ($k == '都道府県' || $k == 'どこで知りましたか？' || strpos($k, 'ご相談希望') !== false) {
					if (preg_match('/<select name="'.htmlspecialchars($k).'">.*?<\/select>/uis', $contents, $matches)) {
						$select = $matches[0];
						$base = '<option value="'.htmlspecialchars($v).'"';
						$replace = str_replace($base, $base.' selected="selected"', $select);
						$contents = str_replace($select, $replace, $contents);
					}
				} else {
					$k = preg_quote(htmlspecialchars($k), '/');
					$base = '/<input type="text" name="'.$k.'" .*?value="/iu';
					if (preg_match($base, $contents, $matches)) {
						$contents = str_replace($matches[0], $matches[0].htmlspecialchars($v), $contents);
					} else {
						$base = '/<textarea name="'.$k.'".*?>/iu';
						if (preg_match($base, $contents, $matches)) {
							$contents = str_replace($matches[0], $matches[0].htmlspecialchars($v), $contents);
						}
					}
				}
			}
			foreach ($errors as $error) {
				$key = preg_quote(htmlspecialchars($error), '/');
				$base = '/<!-- ((?:p|span) class="hint" title="'.$key.'">.*?<\/(?:p|span)) -->/';
				$contents = preg_replace($base, '<$1>', $contents);
			}
		} else {
			$contents = file_get_contents($confirmfile);
			$contents = replaceConfirm($contents, array('textarea'=>array('ご相談内容')));
			$checkbox_values = array();
			foreach ($_POST as $k=>$v) {
				if (strpos($k, 'checkbox') !== false) {
					$checkbox_values[] = $checkboxes[$k];
				}
			}
			$contents = str_replace('%ご相談箇所%', join('<br />', $checkbox_values), $contents);
		}

	}
} else {
	$headers = apache_request_headers();
	$since = @$headers["If-Modified-Since"];
	$contents = $formcontent;
	$htmlfilemtime = filemtime($formfile);
	if ($since && strtotime($since) >= $htmlfilemtime) {
		header('Content-Type: text/html; charset=UTF-8', true, 304);
		exit;
	}
}
//ob_clean();

header('Content-Type: text/html; charset=UTF-8');
header('Content-Length: '.strlen($contents));
header('Last-Modified: '.gmdate( "D, d M Y H:i:s", $htmlfilemtime ) . " GMT");

echo $contents;


