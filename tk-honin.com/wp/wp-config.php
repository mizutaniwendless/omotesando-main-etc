<?php
/**
 * WordPress の基本設定
 *
 * このファイルは、インストール時に wp-config.php 作成ウィザードが利用します。
 * ウィザードを介さずにこのファイルを "wp-config.php" という名前でコピーして
 * 直接編集して値を入力してもかまいません。
 *
 * このファイルは、以下の設定を含みます。
 *
 * * MySQL 設定
 * * 秘密鍵
 * * データベーステーブル接頭辞
 * * ABSPATH
 *
 * @link http://wpdocs.sourceforge.jp/wp-config.php_%E3%81%AE%E7%B7%A8%E9%9B%86
 *
 * @package WordPress
 */

// 注意: 
// Windows の "メモ帳" でこのファイルを編集しないでください !
// 問題なく使えるテキストエディタ
// (http://wpdocs.sourceforge.jp/Codex:%E8%AB%87%E8%A9%B1%E5%AE%A4 参照)
// を使用し、必ず UTF-8 の BOM なし (UTF-8N) で保存してください。

// ** MySQL 設定 - この情報はホスティング先から入手してください。 ** //
/** WordPress のためのデータベース名 */
define('DB_NAME', 'sddb0040027958');

/** MySQL データベースのユーザー名 */
define('DB_USER', 'sd_dba_MzE3NTE4');

/** MySQL データベースのパスワード */
define('DB_PASSWORD', '7RBiM$8N');

/** MySQL のホスト名 */
define('DB_HOST', 'sddb0040027958.cgidb');

/** データベースのテーブルを作成する際のデータベースの文字セット */
define('DB_CHARSET', 'utf8mb4');

/** データベースの照合順序 (ほとんどの場合変更する必要はありません) */
define('DB_COLLATE', '');

/**#@+
 * 認証用ユニークキー
 *
 * それぞれを異なるユニーク (一意) な文字列に変更してください。
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org の秘密鍵サービス} で自動生成することもできます。
 * 後でいつでも変更して、既存のすべての cookie を無効にできます。これにより、すべてのユーザーを強制的に再ログインさせることになります。
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Z9KjFp>/Ia8rlzlMXc]egw*%}iAMQCFp]*,c!.0 (~yqysWKZy1`PSZG^uK2%I3~');
define('SECURE_AUTH_KEY',  '.P)xvPQ;b$xjp334ggo<R:#zi_Ya(c}W(z3<{ttruK[?tDeYMp>E>`g!$qt+?7Lh');
define('LOGGED_IN_KEY',    '+K-3@v@o:A4]6|5$eR*#$X^[h3Mu7d)@cox,yo}Y00LOl2|F![u&%1>UTub/{R+T');
define('NONCE_KEY',        '[!z5|kC_/H/U7sqC)dOOYavx*@9y%%&zUinHQ8Wv?i8tIm6cM}[UHwGbPcFJvZ=H');
define('AUTH_SALT',        'q!r>$/2}*u^J^I,:n8T{3Oa5G@FCoDlu/V&(d/`{eQmu)seWJXa#;jkPr!PKc,BB');
define('SECURE_AUTH_SALT', 'gLy5hkZE=klQE9Lw6V^]91@ven^g35t;x}#AWdeq4e:pN&04ZL+1:+?6hx#$~@<X');
define('LOGGED_IN_SALT',   '^dF:phlA5V*7vpbY1e-|~(EfIZGq1D*rf|#.>rg;;YDg+eV7zDEp:qgk~I^w;6v)');
define('NONCE_SALT',       'GHXZ;DD#YCvE%.o#ZhJywY6c dsV++La+{kzT1<%Nb2}+){,P2?^[>8[,o]C-u9[');

/**#@-*/

/**
 * WordPress データベーステーブルの接頭辞
 *
 * それぞれにユニーク (一意) な接頭辞を与えることで一つのデータベースに複数の WordPress を
 * インストールすることができます。半角英数字と下線のみを使用してください。
 */
$table_prefix  = 'wptkhonin_';

/**
 * 開発者へ: WordPress デバッグモード
 *
 * この値を true にすると、開発中に注意 (notice) を表示します。
 * テーマおよびプラグインの開発者には、その開発環境においてこの WP_DEBUG を使用することを強く推奨します。
 *
 * その他のデバッグに利用できる定数については Codex をご覧ください。
 *
 * @link http://wpdocs.osdn.jp/WordPress%E3%81%A7%E3%81%AE%E3%83%87%E3%83%90%E3%83%83%E3%82%B0
 */
define('WP_DEBUG', false);

/* 編集が必要なのはここまでです ! WordPress でブログをお楽しみください。 */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
