<?php add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' ); 
    function theme_enqueue_styles(){ 
	wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' ); 
	wp_enqueue_style( 'bxslider', get_stylesheet_directory_uri() . '/css/jquery.bxslider.css' ); 
	wp_enqueue_style( 'css-php', get_stylesheet_directory_uri() . '/css.php' ); 
	wp_enqueue_script( 'smart-script', get_stylesheet_directory_uri() . '/js/gk.js', array( 'jquery' ), '20160608', true );
	wp_enqueue_script( 'colorbox', get_stylesheet_directory_uri() . '/js/jquery.colorbox.js', array( 'jquery' ), true );
	wp_enqueue_script( 'bxslider', get_stylesheet_directory_uri() . '/js/jquery.bxslider.min.js', array( 'jquery' ), true );
	} 
	
	function load_cdn() {
		if ( !is_admin() ) {
			wp_deregister_script('jquery');
			wp_enqueue_script('jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js', array(), '2.1.3');
		}
	}
	add_action('init', 'load_cdn');

//パンくずナビ　ここから
/**
 * パンくずナビゲーションを出力します。
 *
 * @param array $args {
 *      オプションです。パンくずリストの出力を配列型の引数で変更できます。
 *
 *      @type string    $container          パンくずリスト囲むタグを指定。デフォルトは'div'。
 *      @type string    $container_class    パンくずリストを囲むタグのClassを指定。デフォルトは'breadcrumb-section'。
 *      @type string    $container_id       パンくずリストを囲むタグのIDを指定。デフォルトは無し。
 *      @type string    $crumb_tag          パンくずリスト自体のタグを指定。デフォルトは'ul'で、他に指定できるのは'ol'のみ。
 *      @type string    $crumb_class        パンくずリスト自体のタグにClassを指定。デフォルトは'crumb-lists'。
 *      @type string    $crumb_id           パンくずリスト自体のタグにIDを指定。デフォルトは無し。
 *      @type bool      $echo               パンくずリストのHTMLを変数に格納する場合は'false'を指定。デフォルトは'true'なので直接出力する。
 *      @type string    $home_class         パンくずリストのホームの階層を示すタグにClassを指定。デフォルトは'crumb-home'。
 *      @type string    $home_text          パンくずリストのホームの階層を示すタグのテキストを指定。デフォルトは'ホーム'。
 *      @type string    $delimiter          パンくずリストの区切り文字を指定。デフォルトは'<li>&nbsp;&gt;&nbsp;</li>'。
 *      @type string    $crumb_microdata    パンくずリストタグ['ul' または 'ol']に指定するリッチスニペット。デフォルトは' itemprop="breadcrumb"'。
 *      @type string    $li_microdata       パンくずリストのliタグに指定するリッチスニペット。デフォルトは' itemscope itemtype="http://data-vocabulary.org/Breadcrumb"'。
 *      @type string    $url_microdata      パンくずリストのaタグに指定するリッチスニペット。デフォルトは' itemprop="url"'。
 *      @type string    $title_microdata    パンくずリストのspanタグに指定するリッチスニペット。デフォルトは' itemprop="title"'。
 * }
 *
 * @return string 各ページに合致するパンくずナビゲーションのHTMLを吐き出します。
 *
 * @version 1.5
 */
function breadcrumb( $args = array() ) {
	$defaults = array(
		'container'         => 'div',
		'container_class'   => 'breadcrumb-section',
		'container_id'      => 'breadcrumb',
		'crumb_tag'         => 'ul',
		'crumb_class'       => 'breadcrumb-lists',
		'crumb_id'          => '',
		'echo'              => true,
		'home_class'        => 'breadcrumb-home',
		'home_text'         => 'ホーム',
		'delimiter'         => '<li>&nbsp;&gt;&nbsp;</li>',
		'crumb_microdata'   => ' itemprop="breadcrumb"',
		'li_microdata'      => ' itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb"',
		'url_microdata'     => ' itemprop="url"',
		'title_microdata'   => ' itemprop="title"'
	);
	
	$args = wp_parse_args( $args, $defaults );
	$args = (object) $args;
	$breadcrumb_html      = '';
	
	//region Rich Snippets (microdata) Setting
	$crumb_microdata    = $args->crumb_microdata ? $args->crumb_microdata : '';
	$li_microdata       = $args->li_microdata ? $args->li_microdata : '';
	$url_microdata      = $args->url_microdata ? $args->url_microdata : '';
	$title_microdata    = $args->title_microdata ? $args->title_microdata : '';
	//endregion
	
	//region Nested Function
	/**
	 * 現在のページのパンくずリスト用タグを作成します。
	 *
	 * @param $current_permalink : current crumb permalink
	 * @param string $current_text : current crumb text
	 * @param string $current_class : class name
	 * @param array $args : microdata settings
	 *
	 * @return string
	 */
	/*
	 * Nest Function [current_crumb_tag()] Argument
	 */
	$current_microdata = array(
		'li_microdata'      => $li_microdata,
		'url_microdata'     => $url_microdata,
		'title_microdata'   => $title_microdata
	);
	function current_crumb_tag( $current_permalink, $current_text = '', $args = array(), $current_class = 'current-crumb' ) {
		$defaults = array(
			'li_microdata'      => ' itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb"',
			'url_microdata'     => ' itemprop="url"',
			'title_microdata'   => ' itemprop="title"'
		);
		$args = wp_parse_args( $args, $defaults );
		$args = (object) $args;
		$current_class      = $current_class ? ' class="' . esc_attr( $current_class ) . '"' : '';
		$start_anchor_tag   = $current_permalink ? '<a href="' . $current_permalink . '"' . $args->url_microdata . '>' : '<span class="crumb-no-link">';
		$end_anchor_tag     = $current_permalink ? '</a>' : '</span>';
		$current_before     = '<li' . $current_class . $args->li_microdata . '>' . $start_anchor_tag . '<span' . $args->title_microdata . '><strong>';
		$current_crumb_tag  = $current_text;
		$current_after      = '</strong></span>' . $end_anchor_tag . '</li>';
		if ( get_query_var( 'paged' ) ) {
			if ( is_paged() || is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) {
				$current_after = ' (ページ' . get_query_var( 'paged' ) . ')' . $current_after;
			}
			
		} elseif ( ( is_page() || is_single() ) && get_query_var( 'page' ) ) {
			$current_after = ' (ページ' . get_query_var( 'page' ) . ')' . $current_after;
		}
		
		return $current_before . $current_crumb_tag . $current_after;
	}
	//endregion
	
	if (
		( ! is_home() && ! is_front_page() )
		|| ( is_home() && ! is_front_page() )
		|| is_paged()
	) {
		// Breadcrumb Container Start Tag
		if ( $args->container ) {
			$class = $args->container_class ? ' class="' . esc_attr( $args->container_class ) . '"' : ' class="' . $defaults['container_class'] . '"';
			$id = $args->container_id ? ' id="' . esc_attr( $args->container_id ) . '"' : '';
			$breadcrumb_html .= '<'. $args->container . $id . $class . '>';
		}
		
		// Breadcrumb Start Tag
		if ( $args->crumb_tag ) {
			$crumb_tag_allowed_tags = apply_filters( 'crumb_tag_allowed_tags', array( 'ul', 'ol' ) );
			if ( in_array( $args->crumb_tag, $crumb_tag_allowed_tags ) ) {
				$id = $args->crumb_id ? ' id="' . esc_attr( $args->crumb_id ) . '"' : '';
				$class = $args->crumb_class ? ' class="' . esc_attr( $args->crumb_class ) . '"' : '';
				$breadcrumb_html .= '<' . $args->crumb_tag . $id . $class . $crumb_microdata . '>';
			}
			
		} else {
			$breadcrumb_html .= '<' . $defaults['crumb_tag'] .  $crumb_microdata . '>';
		}
		
		global $post;
		
		// Home Crumb Item
		$home_class = $args->home_class ? ' class="'. esc_attr( $args->home_class ) . '"' : '';
		$breadcrumb_html .= '<li'. $home_class . $li_microdata . '><a href="' . home_url() . '"' . $url_microdata . '><span ' . $title_microdata . '>' . $args->home_text . '</span></a></li>';
		if ( is_home() && ! is_front_page() ) {
			$home_ID = get_option('page_for_posts');
			$breadcrumb_html .= current_crumb_tag( get_the_permalink( $home_ID ), get_the_title( $home_ID ), $current_microdata );
			
		} else if ( is_paged() ) {
			if ( 'post' == get_post_type() ) {
				$breadcrumb_html .= current_crumb_tag( get_pagenum_link( get_query_var( 'paged' ) ), '投稿一覧', $current_microdata );
				
			} elseif ( 'page' == get_post_type() ) {
				$breadcrumb_html .= current_crumb_tag( get_pagenum_link( get_query_var( 'paged' ) ), get_the_title(), $current_microdata );
			} elseif ( 'media' == get_post_type() ) {
				$breadcrumb_html .= current_crumb_tag( get_pagenum_link( get_query_var( 'paged' ) ), get_the_title(2077), $current_microdata );
			} elseif ( 'case_photo' == get_post_type() ) {
		
				$post_type_object = get_post_type_object( get_post_type() );
				$breadcrumb_html .= '<li' . $li_microdata . '><a href="' . get_post_type_archive_link( get_post_type() ) . '"' . $url_microdata . '><span' . $title_microdata . '>' . $post_type_object->label . '</span></a></li>';
				$taxonomies =  get_object_taxonomies( get_post_type() );
				$category_term = '';
				
				foreach ( $taxonomies as $taxonomy ) {
					$taxonomy_obj = get_taxonomy( $taxonomy );
					if ( true == $taxonomy_obj->hierarchical ) {
						$category_term = $taxonomy_obj;
						break;
					}
				}
				
				if ( $category_term ) {
					$terms = get_the_terms( $post->ID, $category_term->name );
					
					if ( $terms ) {
						if ( ! $terms || is_wp_error( $terms ) )
							$terms = array();
						
						$terms = array_values( $terms );
						$term = $terms[0];
						
						if ( $term->parent != 0 ) {
							$ancestors = array_reverse( get_ancestors( $term->term_id, $term->taxonomy ) );
							foreach ( $ancestors as $ancestor ) {
								
							}
						}
					}
				}

				$url = $_SERVER['REQUEST_URI'];
				if(isset($_GET['ope_name'])){

						$breadcrumb_html .= '<li' . $li_microdata . '><a href="' .esc_url( get_home_url() ). '/menu_category/' . get_term( $term, $term->taxonomy )->slug . '/?post_type=case_photo"' . $url_microdata . '><span' . $title_microdata . '>' . $term->name . '</span></a></li>';
						
						$tag = get_queried_object();
						$breadcrumb_html .= '<li' . $li_microdata . '><a href="' .esc_url( get_home_url() ). '/case_photo/?ope_name=' . get_term( $tag, $tag->taxonomy )->slug . '"' . $url_microdata . '><span' . $title_microdata . '>' . $tag->name . '</span></a></li>';
						
				}
				
				if(isset($_GET['post_type'])){
						$breadcrumb_html .= '<li' . $li_microdata . '><a href="' .esc_url( get_home_url() ). '/menu_category/' . get_term( $term, $term->taxonomy )->slug . '/?post_type=case_photo"' . $url_microdata . '><span' . $title_microdata . '>' . $term->name . '</span></a></li>';
						
				}
				
			} elseif ( 'questionnaire' == get_post_type() ) {
		
				$post_type_object = get_post_type_object( get_post_type() );
				$breadcrumb_html .= '<li' . $li_microdata . '><a href="' . get_post_type_archive_link( get_post_type() ) . '"' . $url_microdata . '><span' . $title_microdata . '>' . $post_type_object->label . '</span></a></li>';
				$taxonomies =  get_object_taxonomies( get_post_type() );
				$category_term = '';
				
				foreach ( $taxonomies as $taxonomy ) {
					$taxonomy_obj = get_taxonomy( $taxonomy );
					if ( true == $taxonomy_obj->hierarchical ) {
						$category_term = $taxonomy_obj;
						break;
					}
				}
				
				if ( $category_term ) {
					$terms = get_the_terms( $post->ID, $category_term->name );
					
					if ( $terms ) {
						if ( ! $terms || is_wp_error( $terms ) )
							$terms = array();
						
						$terms = array_values( $terms );
						$term = $terms[0];
						
						if ( $term->parent != 0 ) {
							$ancestors = array_reverse( get_ancestors( $term->term_id, $term->taxonomy ) );
							foreach ( $ancestors as $ancestor ) {
								
							}
						}
					}
				}

				$url = $_SERVER['REQUEST_URI'];
				if(isset($_GET['ope_name'])){

						$breadcrumb_html .= '<li' . $li_microdata . '><a href="' .esc_url( get_home_url() ). '/menu_category/' . get_term( $term, $term->taxonomy )->slug . '/?post_type=questionnaire"' . $url_microdata . '><span' . $title_microdata . '>' . $term->name . '</span></a></li>';
						
						$tag = get_queried_object();
						$breadcrumb_html .= '<li' . $li_microdata . '><a href="' .esc_url( get_home_url() ). '/questionnaire/?ope_name=' . get_term( $tag, $tag->taxonomy )->slug . '"' . $url_microdata . '><span' . $title_microdata . '>' . $tag->name . '</span></a></li>';
						
				}
				
				if(isset($_GET['post_type'])){
						$breadcrumb_html .= '<li' . $li_microdata . '><a href="' .esc_url( get_home_url() ). '/menu_category/' . get_term( $term, $term->taxonomy )->slug . '/?post_type=questionnaire"' . $url_microdata . '><span' . $title_microdata . '>' . $term->name . '</span></a></li>';
						
				}
				
				
			
			}
		} elseif ( is_category() ) {
			$cat = get_queried_object();
			
			if ( $cat->parent != 0 ) {
				$ancestors = array_reverse( get_ancestors( $cat->cat_ID, 'category' ) );
				foreach ( $ancestors as $ancestor ) {
					$breadcrumb_html .= '<li' . $li_microdata . '><a href="' . get_category_link( $ancestor ) . '"' . $url_microdata . '><span' . $title_microdata . '>' . get_cat_name( $ancestor ) . '</span></a></li>';
				}
			}
			
			$breadcrumb_html .= current_crumb_tag( get_category_link( $cat->term_id ), single_cat_title( '', false ), $current_microdata );
		
		} elseif ( is_day() ) {
			$breadcrumb_html .= '<li' . $li_microdata . '><a href="' . get_year_link( get_the_time( 'Y' ) ) . '"' . $url_microdata . '><span' . $title_microdata . '>' . get_the_time( 'Y' ) . '年</span></a></li>';
			$breadcrumb_html .= '<li' . $li_microdata . '><a href="' . get_month_link( get_the_time( 'Y' ), get_the_time( 'm' ) ) . '"' . $url_microdata . '><span' . $title_microdata . '>' . get_the_time( 'F' ) . '</span></a></li>';
			$breadcrumb_html .= current_crumb_tag( get_day_link( get_the_time( 'Y' ), get_the_time( 'm' ), get_the_time( 'd' ) ), get_the_time( 'd' ) . '日', $current_microdata );
			
		} elseif ( is_month() ) {
			$breadcrumb_html .= '<li' . $li_microdata . '><a href="' . get_year_link( get_the_time( 'Y' ) ) . '"' . $url_microdata . '><span' . $title_microdata . '>' . get_the_time( 'Y' ) . '年</span></a></li>';
			$breadcrumb_html .= current_crumb_tag( get_month_link( get_the_time( 'Y' ), get_the_time( 'm' ) ), get_the_time( 'F' ), $current_microdata );
			
		} elseif ( is_year() ) {
			$breadcrumb_html .= current_crumb_tag( get_year_link( get_the_time( 'Y' ) ), get_the_time( 'Y' ) . '年', $current_microdata );
			
		}elseif ( is_singular( 'case_photo' ) ) {

				$post_type_object = get_post_type_object( get_post_type() );
				$breadcrumb_html .= '<li' . $li_microdata . '><a href="' . get_post_type_archive_link( get_post_type() ) . '"' . $url_microdata . '><span' . $title_microdata . '>' . $post_type_object->label . '</span></a></li>';
				$taxonomies =  get_object_taxonomies( get_post_type() );
				$category_term = '';
				
				foreach ( $taxonomies as $taxonomy ) {
					$taxonomy_obj = get_taxonomy( $taxonomy );
					if ( true == $taxonomy_obj->hierarchical ) {
						$category_term = $taxonomy_obj;
						break;
					}
				}
				
				if ( $category_term ) {
					$terms = get_the_terms( $post->ID, $category_term->name );
					
					if ( $terms ) {
						if ( ! $terms || is_wp_error( $terms ) )
							$terms = array();
						
						$terms = array_values( $terms );
						$term = $terms[0];
						
						if ( $term->parent != 0 ) {
							$ancestors = array_reverse( get_ancestors( $term->term_id, $term->taxonomy ) );
							foreach ( $ancestors as $ancestor ) {
								
							}
						}
						
						$breadcrumb_html .= '<li' . $li_microdata . '><a href="' .esc_url( get_home_url() ). '/menu_category/' . get_term( $term, $term->taxonomy )->slug . '/?post_type=case_photo"' . $url_microdata . '><span' . $title_microdata . '>' . $term->name . '</span></a></li>';
					}
				}
				
				$breadcrumb_html .= current_crumb_tag( get_the_permalink( $single->ID ), get_the_title( $single->ID ), $current_microdata );
		}elseif ( is_singular( 'questionnaire' ) ) {

				$post_type_object = get_post_type_object( get_post_type() );
				$breadcrumb_html .= '<li' . $li_microdata . '><a href="' . get_post_type_archive_link( get_post_type() ) . '"' . $url_microdata . '><span' . $title_microdata . '>' . $post_type_object->label . '</span></a></li>';
				$taxonomies =  get_object_taxonomies( get_post_type() );
				$category_term = '';
				
				foreach ( $taxonomies as $taxonomy ) {
					$taxonomy_obj = get_taxonomy( $taxonomy );
					if ( true == $taxonomy_obj->hierarchical ) {
						$category_term = $taxonomy_obj;
						break;
					}
				}
				
				if ( $category_term ) {
					$terms = get_the_terms( $post->ID, $category_term->name );
					
					if ( $terms ) {
						if ( ! $terms || is_wp_error( $terms ) )
							$terms = array();
						
						$terms = array_values( $terms );
						$term = $terms[0];
						
						if ( $term->parent != 0 ) {
							$ancestors = array_reverse( get_ancestors( $term->term_id, $term->taxonomy ) );
							foreach ( $ancestors as $ancestor ) {
								
							}
						}
						
						$breadcrumb_html .= '<li' . $li_microdata . '><a href="' .esc_url( get_home_url() ). '/menu_category/' . get_term( $term, $term->taxonomy )->slug . '/?post_type=questionnaire"' . $url_microdata . '><span' . $title_microdata . '>' . $term->name . '</span></a></li>';
					}
				}
				
				$breadcrumb_html .= current_crumb_tag( get_the_permalink( $single->ID ), get_the_title( $single->ID ), $current_microdata );
		} elseif ( is_single() && ! is_attachment() ) {
			$single = get_queried_object();
			
			if ( get_post_type() == 'post' ) {
				if ( get_option( 'page_for_posts' ) ) {
					$breadcrumb_html .= '<li' . $li_microdata . '><a href="' . get_page_link( get_option( 'page_for_posts' ) ) . '"' . $url_microdata . '><span' . $title_microdata . '>' . get_the_title( get_option( 'page_for_posts' ) ) . '</span></a></li>';
				}
				
				$categories = get_the_category( $post->ID );
				$cat        = $categories[0];
				
				if ( $cat->parent != 0 ) {
					$ancestors = array_reverse( get_ancestors( $cat->cat_ID, 'category' ) );
					foreach ( $ancestors as $ancestor ) {
						$breadcrumb_html .= '<li' . $li_microdata . '><a href="' . get_category_link( $ancestor ) . '"' . $url_microdata . '><span' . $title_microdata . '>' . get_cat_name( $ancestor ) . '</span></a></li>';
					}
				}
				
				$breadcrumb_html .= '<li' . $li_microdata . '><a href="' . get_category_link( $cat->cat_ID ) . '"' . $url_microdata . '><span' . $title_microdata . '>' . get_cat_name( $cat->cat_ID ) . '</span></a></li>';
				$breadcrumb_html .= current_crumb_tag( get_the_permalink( $single->ID ), get_the_title( $single->ID ), $current_microdata );
				
			} else {
				$post_type_object = get_post_type_object( get_post_type() );
				$breadcrumb_html .= '<li' . $li_microdata . '><a href="' . get_post_type_archive_link( get_post_type() ) . '"' . $url_microdata . '><span' . $title_microdata . '>' . $post_type_object->label . '</span></a></li>';
				$taxonomies =  get_object_taxonomies( get_post_type() );
				$category_term = '';
				
				foreach ( $taxonomies as $taxonomy ) {
					$taxonomy_obj = get_taxonomy( $taxonomy );
					if ( true == $taxonomy_obj->hierarchical ) {
						$category_term = $taxonomy_obj;
						break;
					}
				}
				
				if ( $category_term ) {
					$terms = get_the_terms( $post->ID, $category_term->name );
					
					if ( $terms ) {
						if ( ! $terms || is_wp_error( $terms ) )
							$terms = array();
						
						$terms = array_values( $terms );
						$term = $terms[0];
						
						if ( $term->parent != 0 ) {
							$ancestors = array_reverse( get_ancestors( $term->term_id, $term->taxonomy ) );
							foreach ( $ancestors as $ancestor ) {
								$breadcrumb_html .= '<li' . $li_microdata . '><a href="' . get_term_link( $ancestor, $term->taxonomy ) . '"' . $url_microdata . '><span' . $title_microdata . '>' . get_term( $ancestor, $term->taxonomy )->name . '</span></a></li>';
							}
						}
						
						$breadcrumb_html .= '<li' . $li_microdata . '><a href="' . get_term_link( $term, $term->taxonomy ) . '"' . $url_microdata . '><span' . $title_microdata . '>' . $term->name . '</span></a></li>';
					}
				}
				
				$breadcrumb_html .= current_crumb_tag( get_the_permalink( $single->ID ), get_the_title( $single->ID ), $current_microdata );
			}
			
		} elseif ( is_attachment() ) {
			$attachment = get_queried_object();
			
			if ( ! empty( $post->post_parent ) ) {
				$breadcrumb_html .= '<li' . $li_microdata . '><a href="' . get_permalink( $post->post_parent ) . '"' . $url_microdata . '><span' . $title_microdata . '>' . get_the_title( $post->post_parent ) . '</span></a></li>';
			}
			
			$breadcrumb_html .= current_crumb_tag( get_the_permalink( $attachment->ID ), get_the_title( $attachment->ID ), $current_microdata );
			
		} elseif ( is_post_type_archive('case_photo') ) {
			
			
				$post_type_object = get_post_type_object( get_post_type() );
				$breadcrumb_html .= '<li' . $li_microdata . '><a href="' . get_post_type_archive_link( get_post_type() ) . '"' . $url_microdata . '><span' . $title_microdata . '>' . $post_type_object->label . '</span></a></li>';
				$taxonomies =  get_object_taxonomies( get_post_type() );
				$category_term = '';
				
				foreach ( $taxonomies as $taxonomy ) {
					$taxonomy_obj = get_taxonomy( $taxonomy );
					if ( true == $taxonomy_obj->hierarchical ) {
						$category_term = $taxonomy_obj;
						break;
					}
				}
				
				if ( $category_term ) {
					$terms = get_the_terms( $post->ID, $category_term->name );
					
					if ( $terms ) {
						if ( ! $terms || is_wp_error( $terms ) )
							$terms = array();
						
						$terms = array_values( $terms );
						$term = $terms[0];
						
						if ( $term->parent != 0 ) {
							$ancestors = array_reverse( get_ancestors( $term->term_id, $term->taxonomy ) );
							foreach ( $ancestors as $ancestor ) {
								
							}
						}
						
						
					}
				}
				
				
				$url = $_SERVER['REQUEST_URI'];
				if(isset($_GET['ope_name'])){

						$breadcrumb_html .= '<li' . $li_microdata . '><a href="' .esc_url( get_home_url() ). '/menu_category/' . get_term( $term, $term->taxonomy )->slug . '/?post_type=case_photo"' . $url_microdata . '><span' . $title_microdata . '>' . $term->name . '</span></a></li>';
						
						$tag = get_queried_object();
						$breadcrumb_html .= '<li' . $li_microdata . '><a href="' .esc_url( get_home_url() ). '/case_photo/?ope_name=' . get_term( $tag, $tag->taxonomy )->slug . '"' . $url_microdata . '><span' . $title_microdata . '><strong>' . $tag->name . '</strong></span></a></li>';
						
				}
				
				if(isset($_GET['post_type'])){
						$breadcrumb_html .= '<li' . $li_microdata . '><a href="' .esc_url( get_home_url() ). '/menu_category/' . get_term( $term, $term->taxonomy )->slug . '/?post_type=case_photo"' . $url_microdata . '><span' . $title_microdata . '><strong>' . $term->name . '</strong></span></a></li>';
						
				}
				
				
			
			
			
		} elseif ( is_post_type_archive('questionnaire') ) {
			
			
				$post_type_object = get_post_type_object( get_post_type() );
				$breadcrumb_html .= '<li' . $li_microdata . '><a href="' . get_post_type_archive_link( get_post_type() ) . '"' . $url_microdata . '><span' . $title_microdata . '>' . $post_type_object->label . '</span></a></li>';
				$taxonomies =  get_object_taxonomies( get_post_type() );
				$category_term = '';
				
				foreach ( $taxonomies as $taxonomy ) {
					$taxonomy_obj = get_taxonomy( $taxonomy );
					if ( true == $taxonomy_obj->hierarchical ) {
						$category_term = $taxonomy_obj;
						break;
					}
				}
				
				if ( $category_term ) {
					$terms = get_the_terms( $post->ID, $category_term->name );
					
					if ( $terms ) {
						if ( ! $terms || is_wp_error( $terms ) )
							$terms = array();
						
						$terms = array_values( $terms );
						$term = $terms[0];
						
						if ( $term->parent != 0 ) {
							$ancestors = array_reverse( get_ancestors( $term->term_id, $term->taxonomy ) );
							foreach ( $ancestors as $ancestor ) {
								
							}
						}
						
						
					}
				}
				
				
				$url = $_SERVER['REQUEST_URI'];
				if(isset($_GET['ope_name'])){

						$breadcrumb_html .= '<li' . $li_microdata . '><a href="' .esc_url( get_home_url() ). '/menu_category/' . get_term( $term, $term->taxonomy )->slug . '/?post_type=questionnaire"' . $url_microdata . '><span' . $title_microdata . '>' . $term->name . '</span></a></li>';
						
						$tag = get_queried_object();
						$breadcrumb_html .= '<li' . $li_microdata . '><a href="' .esc_url( get_home_url() ). '/questionnaire/?ope_name=' . get_term( $tag, $tag->taxonomy )->slug . '"' . $url_microdata . '><span' . $title_microdata . '><strong>' . $tag->name . '</strong></span></a></li>';
						
				}
				
				if(isset($_GET['post_type'])){
						$breadcrumb_html .= '<li' . $li_microdata . '><a href="' .esc_url( get_home_url() ). '/menu_category/' . get_term( $term, $term->taxonomy )->slug . '/?post_type=questionnaire"' . $url_microdata . '><span' . $title_microdata . '><strong>' . $term->name . '</strong></span></a></li>';
						
				}
				
				
			
			
			
		}elseif ( is_page() ) {
			$page = get_queried_object();
			
			if ( $post->post_parent ) {
				$ancestors = array_reverse( get_post_ancestors( $post->ID ) );
				foreach ( $ancestors as $ancestor ) {
					$breadcrumb_html .= '<li' . $li_microdata . '><a href="' . get_permalink( $ancestor ) . '"' . $url_microdata . '><span' . $title_microdata . '>' . get_the_title( $ancestor ) . '</span></a></li>';
				}
			}
			
			$breadcrumb_html .= current_crumb_tag( get_the_permalink( $page->ID ), get_the_title( $page->ID ), $current_microdata );
			
		} elseif ( is_search() ) {
			$breadcrumb_html .= current_crumb_tag( get_search_link(), get_search_query() . '" の検索結果', $current_microdata );
			
		} elseif ( is_tag() ) {
			$tag = get_queried_object();
			$breadcrumb_html .= current_crumb_tag( get_term_link( $tag->term_id, $tag->taxonomy ), single_tag_title( '', false ), $current_microdata );
			
		} elseif ( is_tax() ) {
			$taxonomy_name  = get_query_var( 'taxonomy' );
			$post_types = get_taxonomy( $taxonomy_name )->object_type;
			$breadcrumb_html .= '<li' . $li_microdata . '><a href="' . get_post_type_archive_link( $post_types[0] ) . '"' . $url_microdata . '><span' . $title_microdata . '>' . get_post_type_object( $post_types[0] )->label . '</span></a></li>';
			$tax = get_queried_object();
			
			if ( $tax->parent != 0 ) {
				$ancestors = array_reverse( get_ancestors( $tax->term_id, $tax->taxonomy ) );
				foreach ( $ancestors as $ancestor ) {
					$breadcrumb_html .= '<li' . $li_microdata . '><a href="' . get_term_link( $ancestor, $tax->taxonomy ) . '"' . $url_microdata . '><span' . $title_microdata . '>' . get_term( $ancestor, $tax->taxonomy )->name . '</span></a></li>';
				}
			}
			
			$breadcrumb_html .= current_crumb_tag( get_term_link( $tax->term_id, $tax->taxonomy ), single_tag_title( '', false ), $current_microdata );
			
		} elseif ( is_author() ) {
			$author = get_queried_object();
			$breadcrumb_html .= current_crumb_tag( get_author_posts_url( $author->ID ), get_the_author_meta( 'display_name' ), $current_microdata );
			
		} elseif ( is_404() ) {
			$breadcrumb_html .= current_crumb_tag( null, '404 Not found' );
			
		} elseif ( is_post_type_archive( get_post_type() ) ) {
			if ( false == get_post_type() ) {
				$post_type_obj = get_queried_object();
				$breadcrumb_html .= current_crumb_tag( $post_type_obj->name, $post_type_obj->label, $current_microdata );
				
			} else {
				$post_type_obj = get_post_type_object( get_post_type() );
				$breadcrumb_html .= current_crumb_tag( get_post_type_archive_link( get_post_type() ), $post_type_obj->label, $current_microdata );
			}
			
		} else {
			$breadcrumb_html .= current_crumb_tag( site_url(), wp_title( '', true ), $current_microdata );
		}
		// Breadcrumb End Tag
		if ( $args->crumb_tag ) {
			$crumb_tag_allowed_tags = apply_filters( 'crumb_tag_allowed_tags', array( 'ul', 'ol' ) );
			
			if ( in_array( $args->crumb_tag, $crumb_tag_allowed_tags ) ) {
				$breadcrumb_html .= '</' . $args->crumb_tag . '>';
			}
			
		} else {
			$breadcrumb_html .= '</' . $defaults['crumb_tag'] . '>';
		}
		
		// Breadcrumb Container End Tag
		if ( $args->container ) {
			$breadcrumb_html .= '</' . $args->container . '>';
		}
		
		if ( $args->echo ) {
			echo $breadcrumb_html;
			
		} else {
			return $breadcrumb_html;
		}
	}
}
//パンくずナビ　ここまで
	
/* カスタム投稿タイプClinicを追加 */
add_action( 'init', 'create_post_type' );
function create_post_type() {
	register_post_type( 'clinic', //カスタム投稿タイプ名を指定
		array(
			'labels' => array(
			'name' => __( 'クリニック' ),
			'singular_name' => __( 'クリニック' )
		),
		'public' => true,
		'has_archive' => true, /* アーカイブページを持つ */
		'menu_position' =>5, //管理画面のメニュー順位
		'supports' => array( 'revisions','title', 'editor', 'author', 'thumbnail', 'excerpt', 'custom-fields' ,'comments' ), 
    	)
	);
	
	
	register_post_type( 'doctor', //カスタム投稿タイプ名を指定
		array(
			'labels' => array(
			'name' => __( 'ドクター' ),
			'singular_name' => __( 'ドクター' )
		),
		'public' => true,
		'has_archive' => true, /* アーカイブページを持つ */
		'menu_position' =>5, //管理画面のメニュー順位
		'supports' => array( 'revisions','title', 'editor', 'author', 'thumbnail', 'excerpt', 'custom-fields' ,'comments' ), 
    	)
	);
	
	register_post_type( 'monitor', //カスタム投稿タイプ名を指定
		array(
			'labels' => array(
			'name' => __( 'モニター' ),
			'singular_name' => __( 'モニター' )
		),
		'public' => true,
		'has_archive' => true, /* アーカイブページを持つ */
		'menu_position' =>5, //管理画面のメニュー順位
		'supports' => array( 'revisions','title', 'editor', 'author', 'thumbnail', 'excerpt', 'custom-fields' ,'comments' ), 
    	)
	);
	
	register_post_type( 'menu', //カスタム投稿タイプ名を指定
		array(
			'labels' => array(
			'name' => __( '診療科目' ),
			'singular_name' => __( '診療科目' )
		),
		'public' => true,
		'has_archive' => true, /* アーカイブページを持つ */
		'menu_position' =>5, //管理画面のメニュー順位
		'supports' => array( 'revisions','title', 'editor', 'author', 'thumbnail', 'excerpt', 'custom-fields' ,'comments' ), 
    	)
	);
	
	register_post_type( 'case_photo', //カスタム投稿タイプ名を指定
		array(
			'labels' => array(
			'name' => __( '症例写真' ),
			'singular_name' => __( '症例写真' )
		),
		'public' => true,
		'has_archive' => true, /* アーカイブページを持つ */
		'menu_position' =>5, //管理画面のメニュー順位
		'supports' => array( 'revisions','title', 'editor', 'author', 'thumbnail', 'excerpt', 'custom-fields' ,'comments' ), 
    	)
	);	
	
	register_post_type( 'media', //カスタム投稿タイプ名を指定
		array(
			'labels' => array(
			'name' => __( 'メディア情報' ),
			'singular_name' => __( 'メディア情報' )
		),
		'public' => true,
		'has_archive' => true, /* アーカイブページを持つ */
		'menu_position' =>5, //管理画面のメニュー順位
		'supports' => array( 'revisions','title', 'editor', 'author', 'thumbnail', 'excerpt', 'custom-fields' ,'comments' ), 
    	)
	);
	
	register_post_type( 'questionnaire', //カスタム投稿タイプ名を指定
		array(
			'labels' => array(
			'name' => __( '体験談' ),
			'singular_name' => __( '体験談' )
		),
		'public' => true,
		'has_archive' => true, /* アーカイブページを持つ */
		'menu_position' =>5, //管理画面のメニュー順位
		'supports' => array( 'revisions','title', 'editor', 'author', 'thumbnail', 'excerpt', 'custom-fields' ,'comments' ), 
    	)
	);
	
	/* メディアカテゴリ */
	  register_taxonomy(
		'cat-media', /* タクソノミーの名前 */
		array('media'),
		array(
		  'hierarchical' => true,
		  'update_count_callback' => '_update_post_term_count',
		  'label' => 'カテゴリー（メディア）',
		  'singular_label' => 'カテゴリー（メディア）',
		  'public' => true,
		  'show_ui' => true
		)
	  );
	
	/* 施術カテゴリ */
	  register_taxonomy(
		'menu_category', /* タクソノミーの名前 */
		array('menu', 'monitor', 'case_photo','questionnaire'),
		array(
		  'hierarchical' => true,
		  'update_count_callback' => '_update_post_term_count',
		  'label' => '施術カテゴリー',
		  'singular_label' => '施術カテゴリー',
		  'public' => true,
		  'show_ui' => true
		)
	  );
	
	//カスタムタクソノミー、タグタイプ
	  register_taxonomy(
		'ope_name', 
		array('case_photo','questionnaire'),
		array(
		  'hierarchical' => false, 
		  'update_count_callback' => '_update_post_term_count',
		  'label' => '施術名',
		  'singular_label' => '施術名',
		  'public' => true,
		  'show_ui' => true
		)
	  );

	/* カテゴリタクソノミー(カテゴリー分け)を使えるように設定する 
	*'com_category', array('投稿タイプA', '投稿タイプB','投稿タイプC','投稿タイプD')
	*ドクターに当てる院名と、クリニックに当てる院名を共通化している。
	*/
	register_taxonomy(
		'com_category', array('clinic', 'doctor'),
		array(
		'label' => '院名',
		'hierarchical' => true,
		'update_count_callback' => '_update_post_term_count',
		'show_ui' => true,
		'public' => true,
		'query_var' => true,
		)
	);

	
	
}











/* case_photoでドクターの絞込。
*投稿オブジェクト Doctorで絞り込む
*症例写真↓
*http://192.168.11.32/tkhonin/case_photo/?case_doctor=2283
*体験談↓
*http://192.168.11.32/tkhonin/questionnaire/?case_doctor=249
*のようにできる
*/

function my_pre_get_posts( $query ) {
	
	// do not modify queries in the admin
	if( is_admin() ) {
		
		return $query;
		
	}
	
	
	// only modify queries for 'event' post type
	if( isset($query->query_vars['post_type']) && $query->query_vars['post_type'] == 'case_photo' ||isset($query->query_vars['post_type']) && $query->query_vars['post_type'] == 'questionnaire') {
		
		// allow the url to alter the query
		if( isset($_GET['case_doctor']) ) {
			
			
    		$query->set('meta_key', 'case_doctor');
			$query->set('meta_value', $_GET['case_doctor']);
			
    	} 
		
	}
	
	
	// return
	return $query;

}

add_action('pre_get_posts', 'my_pre_get_posts');


/* the_archive_title()に付いてくる「カテゴリ：」の削除 */


    function custom_archive_title( $title ){
        if ( is_tax() ) {
            $title = single_term_title( '', false );
        }
		elseif ( is_post_type_archive() ) {
                $title = post_type_archive_title( '', false );
            }
         
        return $title;
    }
    add_filter( 'get_the_archive_title', 'custom_archive_title', 10 );


/* メニューにフッター用メニュー追加 */
register_nav_menu( 'footer-menu', 'フッターメニュー' );
	
	
//カテゴリー選択をラジオボタンにする
if(strstr($_SERVER['REQUEST_URI'], 'wp-admin/post.php')) {
    ob_start('one_category_only');
}
if(strstr($_SERVER['REQUEST_URI'], 'wp-admin/post-new.php')) {
    ob_start('one_category_only');
}
function one_category_only($content) {
    $content = str_replace('type="checkbox" name="tax_input[menu_category][]', 'type="radio" name="tax_input[menu_category][]', $content);
    return $content;
}


//ショートコード作成
add_shortcode('uploads', 'shortcode_up');
function shortcode_up() {
$upload_dir = wp_upload_dir();
return $upload_dir['baseurl'];
}


//非公開：の文字削除
function remove_page_title_prefix( $title = '' ) {
if ( empty( $title )) return $title;
$search[0] = '/^' . str_replace('%s', '(.*)', preg_quote(__('Protected: %s'), '/' )) . '$/';
$search[1] = '/^' . str_replace('%s', '(.*)', preg_quote(__('Private: %s'), '/' )) . '$/';
return preg_replace( $search, '$1', $title );
}
add_filter( 'the_title', 'remove_page_title_prefix' );


//資料請求の住所自動で読み込む
function add_head_link() {
    echo '<script src="https://ajaxzip3.github.io/ajaxzip3.js" charset="UTF-8"></script>';
    echo "
    <script type=\"text/javascript\">
    jQuery(function($){
    $(\"#zip\").attr('onKeyUp', 'AjaxZip3.zip2addr(this,\'\',\'address\',\'address\');');
})";
    echo '</script>';
}
add_action('wp_head', 'add_head_link');



//Pagenation
function pagination($pages = '', $range = 2)
{
     $showitems = ($range * 2)+2;//表示するページ数（５ページを表示）

     global $paged;//現在のページ値
     if(empty($paged)) $paged = 1;//デフォルトのページ

     if($pages == '')
     {
         global $wp_query;
         $pages = $wp_query->max_num_pages;//全ページ数を取得
         if(!$pages)//全ページ数が空の場合は、１とする
         {
             $pages = 1;
         }
     }

     if(1 != $pages)//全ページが１でない場合はページネーションを表示する
     {
		 echo "<div class=\"pagenation\">\n";
		 echo "<ul>\n";
		 //Prev：現在のページ値が１より大きい場合は表示
         if($paged > 1) echo "<li class=\"prev\"><a href='".get_pagenum_link($paged - 1)."'>前へ</a></li>\n";

         for ($i=1; $i <= $pages; $i++)
         {
             if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
             {
                //三項演算子での条件分岐
                echo ($paged == $i)? "<li class=\"active\">".$i."</li>\n":"<li><a href='".get_pagenum_link($i)."'>".$i."</a></li>\n";
             }
         }
		//Next：総ページ数より現在のページ値が小さい場合は表示
		if ($paged < $pages) echo "<li class=\"next\"><a href=\"".get_pagenum_link($paged + 1)."\">次へ</a></li>\n";
		echo "</ul>\n";
		echo "</div>\n";
     }
}




?>