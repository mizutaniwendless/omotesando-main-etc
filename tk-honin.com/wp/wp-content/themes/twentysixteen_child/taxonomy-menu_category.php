<?php
/**
 * The template for displaying archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each one. For example, tag.php (Tag archives),
 * category.php (Category archives), author.php (Author archives), etc.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); 
$upload_dir = wp_upload_dir(); 
 ?>

 
 
 
 



	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<article <?php post_class("type-page category_page"); ?>>
			<header class="entry-header">
				<h1 class="page-title"> <?php the_archive_title(); ?></h1>
				<?php if (is_tax() && //カテゴリページの時
						  !is_paged() &&   //カテゴリページのトップの時
						  category_description()) : //カテゴリの説明文が空でない時 ?>
				<?php echo category_description(); ?>
				<?php endif; ?>
			</header><!-- .page-header -->

 <?php
$taxonomy_name = 'menu_category';
$get_termid = get_term_by('slug', $term, $taxonomy_name);
$termid = $get_termid->term_id;
$termchildren = get_term_children( $termid, $taxonomy_name );

if(!empty($termchildren)){
	foreach ( $termchildren as $child ) {?>

		<?php 
		$term = get_term_by( 'id', $child, $taxonomy_name );
		?>

		<h2 class="menu_cate_title"><?php echo $term->name ?></h2>
			<?php
				$post_type = "menu";
				$args = array(
						'post_type' => $post_type,
						'taxonomy'=> $taxonomy_name,
						'term'=> $term->slug,
						'posts_per_page' => -1,
					);
					$tax_posts = get_posts($args); 
					if($tax_posts): 
					?>
						<ul>
							<?php
								foreach($tax_posts as $post):
									$postId = $post->ID;
									 
									// vars	
									$menu_isit_fill = get_field('menu_isit_fill',$postId);
									// check
									if( $menu_isit_fill && in_array('yes', $menu_isit_fill) ): ?>
										
										<li class="menu_to_link">								
											<?php
											// explanation image
											 if( get_field('menu_cate_image',$postId) ): ?>
												 <?php 
												$image = get_field('menu_cate_image',$postId);
												if( !empty($image) ): 
													$url = $image['url'];
													$title = $image['title'];
													$alt = $image['alt'];
													$caption = $image['caption'];
													$size = 'full';
												 
													?>
													<div class="menu_cate_image link">
														<h3 class="menu_cate_title"><?php echo esc_html($post->post_title);?></h3>
														
														
														<a href="
															<?php $othersite_url = get_field('menu_othersite_url',$postId); if( !empty($othersite_url) ): ?>
															<?php echo $othersite_url; ?>
														<?php else: ?>
															<?php the_permalink(); ?><?php echo $othersite_url; ?>
														<?php endif; ?>
																				
														
														">
															<img src="<?php echo $url; ?>" alt="<?php echo $alt; ?>"/>
															<p><?php echo htmlspecialchars(get_field('menu_subtitle',$postId));?></p>
														</a>
													</div>
												<?php endif; ?>
											<?php else: ?>
												<div class="menu_cate_image link">
														<a href="
															<?php $othersite_url = get_field('menu_othersite_url',$postId); if( !empty($othersite_url) ): ?>
															<?php echo $othersite_url; ?>
														<?php else: ?>
															<?php the_permalink(); ?><?php echo $othersite_url; ?>
														<?php endif; ?>
																				
														
														">
														<h3 class="menu_cate_title"><?php echo esc_html($post->post_title);?></h3>
														<img src="<?php echo $upload_dir['baseurl']; ?>/img/cate_noimage.jpg" alt="<?php echo $alt; ?>"/>
														<p><?php echo htmlspecialchars(get_field('menu_subtitle',$postId));?></p>
													</a>
												</div>
											<?php endif; ?>
												
										</li>
												
									<?php else: ?>

										<li class="menu_only_price">	
											
												
											<?php if(have_rows('menu_price',$postId)): ?>
												<h4><?php the_title(); ?></h4>	
												<section class="price_box">
													<?php while(have_rows('menu_price',$postId)): the_row(); ?>
														<div class="price_inbox">
															<div class="price_name"><p><?php the_sub_field('menu_name',$postId); ?></p></div>
															<div class="price_amount">
															<?php if(have_rows('menu_amountbox')): ?>
																<?php while(have_rows('menu_amountbox')): the_row();
																	$menupes = get_sub_field('menu_amount');
																	$menupew = get_sub_field('menu_price');
																	$menufrom = get_sub_field('menu_price_from');
																	$menu_before_price = get_sub_field('menu_price_before_price');
																	$menupow = number_format($menupew);
																?>
																<ul>
																<?php if( $menupes ): ?>
																	<li><?php echo $menupes; ?></li>
																	<li><?php echo $menu_before_price; ?><?php echo $menupow; ?>円<?php if ($menufrom == 'yes'){echo '～';}?></li>
																	<?php else: ?>
																	<li class="one"><?php echo $menu_before_price; ?><?php echo $menupow; ?>円<?php if ($menufrom == 'yes'){echo '～';}?></li>
																<?php endif; ?>
																</ul>
																<?php endwhile; ?>
															<?php endif; ?>
															</div>
														</div>
													<?php endwhile; ?>
												</section>
											<?php endif; ?>
										</li>
										
										
									<?php endif; ?>
										
										
										
										

									
								<?php endforeach; ?>
						</ul>
					<?php endif; ?>
					
					
					<?php wp_reset_postdata();?>

	<?php 
	}
}else{
	
	
		
				//状況によってオプションは変える
				$term = $wp_query->queried_object;
				$post_type = "menu";
				$term_slug = $term->slug;
				$term_tax = $term->taxonomy;
				$args = array(
					'post_type' => $post_type,
					'taxonomy'=> $term_tax,
					'term'=> $term_slug,
					'posts_per_page' => -1,
				);
				$tax_posts = get_posts($args); 
				
				//ループ処理
				$outputHtml = '';
				if($tax_posts): ?>
				
					<ul>
						<?php
							foreach($tax_posts as $post):
								$postId = $post->ID;
								 
								// vars	
								$menu_isit_fill = get_field('menu_isit_fill',$postId);
								// check
								if( $menu_isit_fill && in_array('yes', $menu_isit_fill) ): ?>
									
									<li class="menu_to_link">								
										<?php
										// explanation image
										 if( get_field('menu_cate_image',$postId) ): ?>
											 <?php 
											$image = get_field('menu_cate_image',$postId);
											if( !empty($image) ): 
												$url = $image['url'];
												$title = $image['title'];
												$alt = $image['alt'];
												$caption = $image['caption'];
												$size = 'full';
											 
												?>
												<div class="menu_cate_image link">
													<h3 class="menu_cate_title"><?php echo esc_html($post->post_title);?></h3>
													
													
													<a href="
														<?php $othersite_url = get_field('menu_othersite_url',$postId); if( !empty($othersite_url) ): ?>
														<?php echo $othersite_url; ?>
													<?php else: ?>
														<?php the_permalink(); ?><?php echo $othersite_url; ?>
													<?php endif; ?>
																			
													
													">
														<img src="<?php echo $url; ?>" alt="<?php echo $alt; ?>"/>
														<p><?php echo htmlspecialchars(get_field('menu_subtitle',$postId));?></p>
													</a>
												</div>
											<?php endif; ?>
										<?php else: ?>
											<div class="menu_cate_image link">
													<a href="
														<?php $othersite_url = get_field('menu_othersite_url',$postId); if( !empty($othersite_url) ): ?>
														<?php echo $othersite_url; ?>
													<?php else: ?>
														<?php the_permalink(); ?><?php echo $othersite_url; ?>
													<?php endif; ?>
																			
													
													">
													<h3 class="menu_cate_title"><?php echo esc_html($post->post_title);?></h3>
													<img src="<?php echo $upload_dir['baseurl']; ?>/img/cate_noimage.jpg" alt="<?php echo $alt; ?>"/>
													<p><?php echo htmlspecialchars(get_field('menu_subtitle',$postId));?></p>
												</a>
											</div>
										<?php endif; ?>
											
									</li>
											
								<?php else: ?>

									<li class="menu_only_price">	
										
											
										<?php if(have_rows('menu_price',$postId)): ?>
											<h4><?php the_title(); ?></h4>	
											<section class="price_box">
												<?php while(have_rows('menu_price',$postId)): the_row(); ?>
													<div class="price_inbox">
														<div class="price_name"><p><?php the_sub_field('menu_name',$postId); ?></p></div>
														<div class="price_amount">
														<?php if(have_rows('menu_amountbox')): ?>
															<?php while(have_rows('menu_amountbox')): the_row();
																$menupes = get_sub_field('menu_amount');
																$menupew = get_sub_field('menu_price');
																$menufrom = get_sub_field('menu_price_from');
																$menu_before_price = get_sub_field('menu_price_before_price');
																$menupow = number_format($menupew);
															?>
															<ul>
															<?php if( $menupes ): ?>
																<li><?php echo $menupes; ?></li>
																<li><?php echo $menu_before_price; ?><?php echo $menupow; ?>円<?php if ($menufrom == 'yes'){echo '～';}?></li>
																<?php else: ?>
																<li class="one"><?php echo $menu_before_price; ?><?php echo $menupow; ?>円<?php if ($menufrom == 'yes'){echo '～';}?></li>
															<?php endif; ?>
															</ul>
															<?php endwhile; ?>
														<?php endif; ?>
														</div>
													</div>
												<?php endwhile; ?>
											</section>
										<?php endif; ?>
									</li>
									
									
								<?php endif; ?>
									
									
									
									

								
							<?php endforeach; ?>
					</ul>
				<?php endif; ?>
				
				
				<?php wp_reset_postdata();
	
}
?>


 
 

 
			</article>
		</main><!-- .site-main -->
		<?php get_sidebar( 'content-bottom' ); ?>
	</div><!-- .content-area -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
