<?php
	// CSSファイルからの相対パスで wp-load.php の読み込みを指定
	include_once(dirname( __FILE__ ) . '/../../../wp-load.php');
	header('Content-Type: text/css; charset=utf-8');
	$upload_dir = wp_upload_dir(); 
?>


.sub-title {
    background: rgba(0, 0, 0, 0) url("<?php echo $upload_dir['baseurl']; ?>/img/body_bg02.gif") repeat-x scroll center bottom;
	height: 5px;
}

.widget_doctor div {
	background:url("<?php echo $upload_dir['baseurl']; ?>/img/sidebar_bnr_doctor.png") no-repeat bottom right;
	background-size: contain;
	height: 132px;
}

.header-image {
    background-attachment: scroll;
   /* background-image: url("<?php echo $upload_dir['baseurl']; ?>/img/wrapper_bg011.gif");*/
    background-position: center center;
    background-repeat: repeat;
    background-size: contain;
	position: relative;
	padding:30px 0 0;
}

body.custom-background {
	background-image: url("<?php echo $upload_dir['baseurl']; ?>/2017/02/gb-1.png");
	background-position: left top !important;
	background-size: initial !important;
	background-repeat: repeat-x !important;
	background-attachment: inherit !important;
	background-color: #F1D6CF;
}

#top_news {
	background: rgba(0, 0, 0, 0) url("<?php echo $upload_dir['baseurl']; ?>/img/index_bg_01.gif") repeat-x scroll center bottom / contain ;
	padding: 0;
}

#top_pickup li {
	background: rgba(0, 0, 0, 0) url("<?php echo $upload_dir['baseurl']; ?>/img/index_bg_02.gif") repeat-x scroll center bottom / contain ;
    width: 100%;
}

#top_information,#top_mainmenu,#top_media {
	background: rgba(0, 0, 0, 0) url("<?php echo $upload_dir['baseurl']; ?>/img/index_bg_03.gif") repeat-x scroll center bottom / contain ;
	padding: 0;
	border-bottom: 1px solid #907636;
}

#menu_faq dt {
	background: rgba(0, 0, 0, 0) url("<?php echo $upload_dir['baseurl']; ?>/img/bg_qa_01.gif")  no-repeat scroll left top ;
	padding: 0 0.5em 0 1.75em;
}

#menu_faq dd {
	background: rgba(0, 0, 0, 0) url("<?php echo $upload_dir['baseurl']; ?>/img/bg_qa_02.gif")  no-repeat scroll left top ;
	padding: 0 0.5em 0.75em 1.75em;
}

.menuprice h4 a {
    background: rgba(0, 0, 0, 0) url("<?php echo $upload_dir['baseurl']; ?>/img/link_right.png") no-repeat scroll 0% 45% / 12px auto;
    content: "";
    display: block;
	padding-left: 15px !important;
	text-decoration: underline;
}


.star-ratings-sprite {
  background: url("<?php echo $upload_dir['baseurl']; ?>/img/star-rating-sprite.png") repeat-x;
}
.star-ratings-sprite-rating {
  background: url("<?php echo $upload_dir['baseurl']; ?>/img/star-rating-sprite.png") repeat-x;
  background-position: 0 100%;
  float: left;
  height: 21px;
  display: block;
}

#menu_questionnaires .female::before {
    content: "";
    background: url(<?php echo $upload_dir['baseurl']; ?>/img/female_icon.png);
	width: 64px;
	height: 64px;
	display: inline-block;
	background-size: 100%;
	background-repeat: no-repeat;
}

#menu_questionnaires .male::before {
    content: "";
    background: url(<?php echo $upload_dir['baseurl']; ?>/img/male_icon.png);
	width: 64px;
	height: 64px;
    display: inline-block;
    background-size: 100%;
	background-repeat: no-repeat;
}

#menu_questionnaires .else::before {
    content: "";
    background: url(<?php echo $upload_dir['baseurl']; ?>/img/else_icon.png);
	width: 64px;
	height: 64px;
    display: inline-block;
    background-size: 100%;
	background-repeat: no-repeat;
}
