
<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>

<script>
/*
jQuery(function(){
	jQuery('#aboutcontents section').hide();
    jQuery("a").click(function(){
        jQuery("#aboutcontents section").hide();
        jQuery(jQuery(this).attr("href")).show();
        jQuery(".current").removeClass("current");
        jQuery(this).addClass("current");
		
		return false;
    }); 
});
*/
</script>

<div id="primary" class="content-area">
	<main id="main" class="site-main access_page" role="main">

		<?php
		// Start the loop.
		while ( have_posts() ) : the_post();

			// Include the single post content template.
			get_template_part( 'template-parts/content', 'access' );

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) {
				comments_template();
			}


			// End of the loop.
		endwhile;
		?>

	

	</main><!-- .site-main -->



</div><!-- .content-area -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
