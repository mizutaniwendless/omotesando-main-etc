<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
 
$upload_dir = wp_upload_dir();
get_header(); ?>

<div id="primary" class="content-area">
	<main id="main" class="type-page site-main price_page" role="main">
	<h1><?php the_title(); ?></h1>

<article class="archive_menu archive_menu_parent page_price">	
	<section class="archive_menu_children outline">

	<?php // WordPress のタクソノミーの親・子のターム一覧とタームに紐づく記事一覧の出力
		$categories = get_terms('menu_category','parent=0');
		foreach ( $categories as $cat ) {
			$children = get_terms('menu_category','hierarchical=0&parent='.$cat->term_id);
			if($children){ // 子カテゴリの有無
			
				foreach ( $children as $child ) {
					$term_link = get_term_link( $child );
					$variable = get_field('com_category_to_other_site', $child);
					$catslug = $child->slug;
					?>
					<div>
					
					

							<?php
							// explanation image
							 if( get_field('com_category_image',$child) ): ?>
								 <?php 
								$image = get_field('com_category_image',$child);
								if( !empty($image) ): 
									$url = $image['url'];
									$title = $image['title'];
									$alt = $image['alt'];
									$caption = $image['caption'];
									$size = 'full';
								 
									?>
									<a href="#<?php echo esc_html($child->slug)?>"><img src="<?php echo $url; ?>" alt="<?php echo $alt; ?>"/></a>
								<?php endif; ?>
						<?php else: ?>
							<a href="#<?php echo esc_html($child->slug)?>"><img src="<?php echo $upload_dir['baseurl']; ?>/img/cate_noimage.jpg" alt="<?php echo $alt; ?>"/></a>
						<?php endif; ?>
						
					<h3><a href="#<?php echo esc_html($child->slug)?>"><?php echo esc_html($child->name)?></a></h3>
					</div>
	<?php wp_reset_postdata(); ?>
	<?php } //子カテゴリに紐づく記事一覧の出力終了 ?>

	<?php } ?>

	<?php } // 出力の終了 ?>
	</section>
</article>
	
	
	
	
<article>
	
	
	
<?php // WordPress のタクソノミーの親・子のターム一覧とタームに紐づく記事一覧の出力
	$categories = get_terms('menu_category','parent=0');
	foreach ( $categories as $cat ) {
		echo '<section class="menuprice">';
		echo '<h2>' . esc_html($cat->name) . '</h2>'; // 親カテゴリタイトル
		
		$children = get_terms('menu_category','hierarchical=0&parent='.$cat->term_id);
		if($children){ // 子カテゴリの有無
		
			foreach ( $children as $child ) {
				$term_link = get_term_link( $child );
				$variable = get_field('com_category_to_other_site', $child);
				$catslug = $child->slug;
				?>
				
				<h3 id="<?php echo esc_html($child->slug)?>"><span><?php echo esc_html($child->name)?></span></h3>
				<?php 
				$args = array(
					'post_type' => 'menu',
					'menu_category' => $catslug ,
					'posts_per_page' => -1
				);
				$myquery = new WP_Query( $args ); 
?>





<div class="price_bigbox">
<?php if ( $myquery->have_posts()): ?>

<?php while($myquery->have_posts()): $myquery->the_post();　?>
	<div id="<?php $slug = get_post_field( 'post_name', get_post() );echo $slug; ?>">
	<h4>							
	<?php
	/* vars	
	*シッカリページの中身ある？のチェックボックスにチェックが入っていた場合、リンクが機能する。
	*/
	$menu_isit_fill = get_field('menu_isit_fill');
	$menu_to_other_site = get_field('menu_to_other_site');
	$menu_to_other_mutli_site = get_field('menu_to_other_mutli_site');
	$post_slug = get_post_field( 'post_name', get_post() );
	
	// ページの中身があるか
	if( $menu_isit_fill && in_array('yes', $menu_isit_fill) ): ?>
	
		<?php // 他サイトへ飛ぶのか
		if( $menu_to_other_site ): ?>
		
		
			<?php // 複数サイトへ飛ぶ
			if( $menu_to_other_mutli_site): ?>
						
				<div class="inline link" data-role="#<?php echo $post_slug; ?>">
						<a href="#link">
							<?php the_title(); ?><img class="btn_target" src="<?php echo $upload_dir['baseurl']; ?>/img/btn_target.gif" alt="外部リンクへ飛びます"/>
						</a>
				</div>
				
				<!-- This contains the hidden content for inline calls -->
				
				<div style='display:none'>
					<div id='<?php echo $post_slug; ?>' style='padding:10px; background:#fff;'>
					<p><strong>院名をお選び下さい</strong><br>※外部リンクに飛びます</p>
				<?php
				 if( have_rows("menu_to_other_mutli_site_repeater") ): ?>

						<?php while ( have_rows("menu_to_other_mutli_site_repeater") ) : the_row();?>
							
							<?php $term = get_sub_field('menu_to_other_mutli_site_clinic');
							if( $term ): ?>
							<div>
								<a href="<?php the_sub_field('menu_to_other_mutli_siteurl'); ?>">
								<?php echo $term->name; ?>
								</a>
							</div>
							<?php endif; ?>

							
						<?php endwhile; ?>
					 
				<?php endif; ?>
					</div>
				</div>
								
			<?php //複数サイトへ飛ばない・単一のサイトへ飛ぶ
			else:?>
			
				<?php $othersite_url = get_field('menu_othersite_url'); if( !empty($othersite_url) ): ?>
					<a href="<?php echo $othersite_url; ?>"><?php the_title(); ?><img class="btn_target" src="<?php echo $upload_dir['baseurl']; ?>/img/btn_target.gif" alt="外部リンクへ飛びます"/></a>
				<?php endif; ?>
				
			<?php endif; ?>		

							
							
		<?php else: //他サイトへ飛ばない（サイト内のページを出す）
		?>
			<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>

		<?php endif; ?>
		

	
	
					
						
	<?php // ページの中身がない
	else: ?>
	
		<?php // 他サイトへ飛ぶのか
		if( $menu_to_other_site ): ?>
		
		
			<?php // 複数サイトへ飛ぶ
			if( $menu_to_other_mutli_site): ?>
						
						
				<div class="inline link" data-role="#<?php echo $post_slug; ?>">
						<a href="#link">
							<?php the_title(); ?><img class="btn_target" src="<?php echo $upload_dir['baseurl']; ?>/img/btn_target.gif" alt="外部リンクへ飛びます"/>
						</a>
				</div>
				
				<!-- This contains the hidden content for inline calls -->
				
				<div style='display:none'>
					<div id='<?php echo $post_slug; ?>' style='padding:10px; background:#fff;'>
					<p><strong>院名をお選び下さい</strong><br>※外部リンクに飛びます</p>
				<?php
				 if( have_rows("menu_to_other_mutli_site_repeater") ): ?>

						<?php while ( have_rows("menu_to_other_mutli_site_repeater") ) : the_row();?>
							
							<?php $term = get_sub_field('menu_to_other_mutli_site_clinic');
							if( $term ): ?>
							<div>
								<a href="<?php the_sub_field('menu_to_other_mutli_siteurl'); ?>">
								<?php echo $term->name; ?>
								</a>
							</div>
							<?php endif; ?>

							
						<?php endwhile; ?>
					 
				<?php endif; ?>
					</div>
				</div>
							
								
			<?php //複数サイトへ飛ばない・単一のサイトへ飛ぶ
			else:?>
			
				<?php $othersite_url = get_field('menu_othersite_url'); if( !empty($othersite_url) ): ?>
					<a href="<?php echo $othersite_url; ?>"><?php the_title(); ?><img class="btn_target" src="<?php echo $upload_dir['baseurl']; ?>/img/btn_target.gif" alt="外部リンクへ飛びます"/></a>
				<?php endif; ?>
				
			<?php endif; ?>		

							
							
		<?php else: //他サイトへ飛ばない（サイト内のページを出す）
		?>
	
			<?php the_title(); ?>
			
		<?php endif; ?>
		

	<?php endif; ?>

</h4>

	<div class="price_box">
		<?php if(have_rows('menu_price')): ?>
			<?php while(have_rows('menu_price')): the_row(); ?>
		<div class="price_inbox">
			<div class="price_name"><p><?php the_sub_field('menu_name'); ?></p></div>
			<div class="price_amount">
			<?php if(have_rows('menu_amountbox')): ?>
				<?php while(have_rows('menu_amountbox')): the_row();
					$menupes = get_sub_field('menu_amount');
					$menupew = get_sub_field('menu_price');
					$menufrom = get_sub_field('menu_price_from');
					$menu_before_price = get_sub_field('menu_price_before_price');
					$menupow = number_format($menupew);
				?>
				<ul>
				<?php if( $menupes ): ?>
					<li><?php echo $menupes; ?></li>
					<li><?php echo $menu_before_price; ?><?php echo $menupow; ?>円<?php if ($menufrom == 'yes'){echo '～';}?></li>
					<?php else: ?>
					<li class="one"><?php echo $menu_before_price; ?><?php echo $menupow; ?>円<?php if ($menufrom == 'yes'){echo '～';}?></li>
				<?php endif; ?>
				</ul>
				<?php endwhile; ?>
			<?php endif; ?>
			</div>
		</div>
			<?php endwhile; ?>
		<?php endif; ?>

	</div>
	<div class="menu_isit_limited">
		<?php if( get_field('menu_isit_limited') ) { ?>
		<?php 
		$terms = get_field('menu_what_clinics');
		if( $terms ): ?>
		※
			<?php foreach( $terms as $term ): ?>
				<?php echo $term->name;
				if($term !== end($terms)){
					echo "・";}
				?>
			<?php endforeach; ?>
		限定の施術です。
		<?php endif; ?>
	<?php } ?>
	<?php if( get_field('menu_price_info') ) { ?>
	<div>
		<?php the_field('menu_price_info');?>
	</div>	
	<?php } ?>
	
	</div>	
	
</div>	
<?php endwhile; ?>

<?php endif; ?>
</div>

<?php wp_reset_postdata(); ?>
<?php } //子カテゴリに紐づく記事一覧の出力終了 ?>

<?php } ?>

</section>

<?php } // 出力の終了 ?>
</article>
	

	</main><!-- .site-main -->

	<?php get_sidebar( 'content-bottom' ); ?>

</div><!-- .content-area -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
