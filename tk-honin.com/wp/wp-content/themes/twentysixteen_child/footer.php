<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
  $upload_dir = wp_upload_dir(); 
?>

		</div><!-- .site-content -->
	</div><!-- .site-inner -->
	</div><!-- .site -->
		<footer id="colophon" class="site-footer" role="contentinfo">
		
		<div class="cstm_menucate">
			<?php // WordPress のタクソノミーの親・子のターム一覧とタームに紐づく記事一覧の出力
				$categories = get_terms('menu_category','parent=0');
				foreach ( $categories as $cat ) {
					echo '<div class="cstm_menucatein">';
					echo '<ul class="sitemap_sub">'; // 親カテゴリタイトル
					echo '<li class="sitemap_catettl">' . esc_html($cat->name) . ''; // 親カテゴリタイトル
					
					$children = get_terms('menu_category','hierarchical=0&parent='.$cat->term_id);
					if($children){ // 子カテゴリの有無
						foreach ( $children as $child ) {
							echo '<li><ul><li class="page_item">' . esc_html($child->name) . ''; // 子カテゴリタイトル
							$catslug = $child->slug;
							$args = array(
								'post_type' => 'menu',
								'menu_category' => $catslug ,
								'posts_per_page' => -1
							);
							$myquery = new WP_Query( $args ); 
			?>
			
				<ul>
				<?php if ( $myquery->have_posts()): ?>
					<?php while($myquery->have_posts()): $myquery->the_post(); ?>
					<li class="sitemap_menuname"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></li>
					<?php endwhile; ?>
				<?php endif; ?>
				</ul>

			</li>
		</ul>
		</li>
	<?php wp_reset_postdata(); ?>
	<?php } //子カテゴリに紐づく記事一覧の出力終了 ?>
	<?php }  ?>
	</ul>
	</div>
	<?php } // 出力の終了 ?>
	</div><br>
		
			<?php if ( has_nav_menu( 'primary' ) ) : ?>
				<nav class="main-navigation" role="navigation" aria-label="<?php esc_attr_e( 'Footer Primary Menu', 'twentysixteen' ); ?>">
					<?php
						wp_nav_menu( array(
							'theme_location' => 'primary',
							'menu_class'     => 'primary-menu',
						 ) );
					?>
				</nav><!-- .main-navigation -->
			<?php endif; ?>

			<?php if ( has_nav_menu( 'social' ) ) : ?>
				<nav class="social-navigation" role="navigation" aria-label="<?php esc_attr_e( 'Footer Social Links Menu', 'twentysixteen' ); ?>">
					<?php
						wp_nav_menu( array(
							'theme_location' => 'social',
							'menu_class'     => 'social-links-menu',
							'depth'          => 1,
							'link_before'    => '<span class="screen-reader-text">',
							'link_after'     => '</span>',
						) );
					?>
				</nav><!-- .social-navigation -->
			<?php endif; ?>
			
			
			<div class="site-info" id="footer-info">
				<?php
					/**
					 * Fires before the twentysixteen footer text for footer customization.
					 *
					 * @since Twenty Sixteen 1.0
					 */
					do_action( 'twentysixteen_credits' );
				?>
				<span class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></span>
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"> Copyright © Omotesando Skin Clinic. All Rights Reserved.</a>
			</div><!-- .site-info -->
			
			<div class="sp_footer_menu visibleSp">
				<div class="link">
					<a href="http://tk-honin.com/lp/matome/index.php">
						<img class="" src="<?php echo $upload_dir['baseurl']; ?>/img/footer_recommend_icon.png" alt="オススメ情報"/>
						<p>オススメ情報</p>
					</a>
				</div>
				<div class="link">
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>/reservation/">
						<img class="" src="<?php echo $upload_dir['baseurl']; ?>/img/footer_web_icon.png" alt="無料カウンセリング"/>
						<p>無料カウンセリング</p>
					</a>
				</div>
				<div class="link">
					<a href="tel:0120-334-270" onclick="ga('send', 'event', 'sp', 'tel-tap', 'tel');">
						<img class="" src="<?php echo $upload_dir['baseurl']; ?>/img/footer_tel_icon.png" alt="電話予約"/>
						<p>電話予約</p>
					</a>
				</div>	
			</div><!-- .sp_footer_menu -->
			
			
		</footer><!-- .site-footer -->



<?php wp_footer(); ?>

<!-- Twitter universal website tag code -->
<script>
!function(e,t,n,s,u,a){e.twq||(s=e.twq=function(){s.exe?s.exe.apply(s,arguments):s.queue.push(arguments);
},s.version='1.1',s.queue=[],u=t.createElement(n),u.async=!0,u.src='//static.ads-twitter.com/uwt.js',
a=t.getElementsByTagName(n)[0],a.parentNode.insertBefore(u,a))}(window,document,'script');
// Insert Twitter Pixel ID and Standard Event data below
twq('init','nwbnz');
twq('track','PageView');
</script>
<!-- End Twitter universal website tag code -->
</body>
</html>
