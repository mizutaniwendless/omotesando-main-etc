<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>

<script>
var agent = navigator.userAgent;
if( agent.search(/iPhone/) != -1 || agent.search(/Android/) != -1){
jQuery(function() {
	//最初は全てのパネルを非表示に
	jQuery('div.access_inbox').hide();
	jQuery('h2.switch')
	.click(function(e){
		//選択したパネルを開く
		jQuery(this).toggleClass("open");
		jQuery('+div.access_inbox', this).slideToggle(500);
	})
});
}
jQuery(function(){
	jQuery('a[href^=#]').click(function(){
		var speed = 500;
		var href= $(this).attr("href");
		var target = $(href == "#" || href == "" ? 'html' : href);
		var position = target.offset().top;
		jQuery("html, body").animate({scrollTop:position}, speed, "swing");
		return false;
	});
});
</script>

<div id="primary" class="content-area">
	<main id="main" class="site-main recruit_page" role="main">

		<?php
		// Start the loop.
		while ( have_posts() ) : the_post();

			// Include the single post content template.
			get_template_part( 'template-parts/content', 'recruit' );

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) {
				comments_template();
			}


			// End of the loop.
		endwhile;
		?>

	

	</main><!-- .site-main -->



</div><!-- .content-area -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
