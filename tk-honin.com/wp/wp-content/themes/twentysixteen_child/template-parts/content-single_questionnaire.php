<?php
/**
 * The template part for displaying single posts
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
 $upload_dir = wp_upload_dir(); 
 
$my_ID = get_the_ID();
$ope_names = get_the_terms($my_ID, 'ope_name');
$menu_categorys = get_the_terms($my_ID, 'menu_category');
$case_doctor_id = get_field('case_doctor', false, false);
$description = get_field('questionnaire_description');
$response = get_field('questionnaire_response');
$grad = get_field('questionnaire_grad');
$hurt = get_field('questionnaire_hurt');
$result = get_field('questionnaire_result');
$doctor = get_field('questionnaire_doctor');
$hospital = get_field('questionnaire_hospital');
$questionnaire_txt = get_field('questionnaire_txt');
$gender = get_field('questionnaire_gender');
$age = get_field('questionnaire_age');
$job = get_field('questionnaire_job');

$allresult =($description+$response+$grad+$hurt+$result+$doctor+$hospital)/7*20;

?>

<article id="post-<?php the_ID(); ?>" <?php post_class("single_case_photo single_questionnaire type-page"); ?>>
	<header class="entry-header">
		<h1 class="entry-title">
			<?php 
			if( !empty($menu_categorys) ) {
			foreach($menu_categorys as $menu_category){
				$menu_category_name = $menu_category -> name;
				echo $menu_category_name;
			}}?>の体験談<br>
				<?php 
				/*
				牧さんから医師名のGOが出たらここを出す
				<small><?php echo get_the_title($case_doctor_id); ?>医師</small>
				*/
				 ?>
		</h1>
		<?php
		// subtitle
		 if( get_field("menu_subtitle") ): ?>
					   <p><?php the_field('menu_subtitle');?></p>
			
		<?php endif; ?>
	</header><!-- .entry-header -->

	<?php twentysixteen_excerpt(); /* 抜粋 */?>

	<?php twentysixteen_post_thumbnail();　/* サムネイル */ ?>
	 
	<section>
		<div class="questionnaire_img">
		   <?php 
			$image = get_field('questionnaire_image');
			if( !empty($image) ): 
				$url = $image['url'];
				$title = $image['title'];
				$alt = $image['alt'];
				$caption = $image['caption'];
				$size = 'full';
				$width = $image['sizes'][ $size . '-width' ];
				$height = $image['sizes'][ $size . '-height' ];
			 ?>
				<img src="<?php echo $url; ?>" alt="<?php echo $alt; ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>"/>
			<?php else: ?>	
				<img src="<?php echo $upload_dir['baseurl']; ?>/img/noimage_questionnaire.png" alt="noimage" width="<?php echo $width; ?>" height="<?php echo $height; ?>"/>
			
			<?php endif; ?>
		</div>

		<div class="questionnaire_txt">
			<div>
				<?php echo $questionnaire_txt;?>
			</div>
			<div>（<?php echo $job; ?>/<?php echo $age; ?>歳/<?php
			if ($gender == 'female'){
				echo '女性';
			} elseif ($gender == 'male'){
				echo '男性';
			}else{
				echo 'その他'; 
			}
			?>）
			</div>
		</div>
	</section>
	
	<section>
		<h4>施術内容</h4>
		<ul class="case_pt_link"><?php 
			$tmp=$ope_names;
			if( !empty($ope_names) ) {
			foreach($ope_names as $ope_name){
				$my_term_name = $ope_name -> name;
				$my_term_slug = $ope_name -> slug;
			?>
				<li>
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>menu/<?php echo $my_term_slug;?>"><?php echo $my_term_name;?></a>
				</li>
			<?php 	}} ?>
		</ul>
	</section>	

	
	<section>
		<h4><div class="questionnaire_stars">
			<p>総合満足度：<?php echo round($allresult); ?>％</p>
			<div class="star-ratings-sprite"><span style="width:<?php echo $allresult; ?>%" class="star-ratings-sprite-rating"></span></div>
		</div></h4>


			<div>
				<ul class="graf">
					<?php if( !empty($description) ): ?>
						<li>
							<div>施術に対する説明はわかりやすく十分でしたか？</div>
							<div class="star-ratings-sprite"><span style="width:<?php echo $description*20; ?>%" class="star-ratings-sprite-rating"></span></div>
						</li>
					<?php endif; ?>
					
					<?php if( !empty($response) ): ?>
						<li>
							<div>受付や看護師の対応はご満足いただけましたか？</div>
							<div class="star-ratings-sprite"><span style="width:<?php echo $response*20; ?>%" class="star-ratings-sprite-rating"></span></div>
						</li>
					<?php endif; ?>
					
					<?php if( !empty($grad) ): ?>
						<li>
							<div>この施術を受けて良かったと思いましたか？</div>
							<div class="star-ratings-sprite"><span style="width:<?php echo $grad*20; ?>%" class="star-ratings-sprite-rating"></span></div>
						</li>
					<?php endif; ?>
					
					<?php if( !empty($hurt) ): ?>
						<li>
							<div>術後の内出血や腫れ・痛みは気になりましたか？</div>
							<div class="star-ratings-sprite"><span style="width:<?php echo $hurt*20; ?>%" class="star-ratings-sprite-rating"></span></div>
						</li>
					<?php endif; ?>

					<?php if( !empty($result) ): ?>
						<li>
							<div>仕上がりや施術の効果は、ご満足いただけましたか？</div>
							<div class="star-ratings-sprite"><span style="width:<?php echo $result*20; ?>%" class="star-ratings-sprite-rating"></span></div>
						</li>
					<?php endif; ?>

					<?php if( !empty($doctor) ): ?>
						<li>
							<div>ドクター・スタッフの対応はいかがでしたか？</div>
							<div class="star-ratings-sprite"><span style="width:<?php echo $doctor*20; ?>%" class="star-ratings-sprite-rating"></span></div>
						</li>
					<?php endif; ?>

					<?php if( !empty($hospital) ): ?>
						<li>
							<div>院内の雰囲気はいかがでしたか？</div>
							<div class="star-ratings-sprite"><span style="width:<?php echo $hospital*20; ?>%" class="star-ratings-sprite-rating"></span></div>
						</li>
					<?php endif; ?>
					

				</ul>
			
			</div>
	</section>	
	

	<?php if($post->post_content=="") : ?>
		<!-- ここに記事本文が空だった場合に表示したいソースを記述  -->
	<?php else : ?>
	<section class="entry-content">
		<h4>クリニック・医師からのコメント</h4>
		<div class="questionnaire_txt">
		<dl class="questionnaires_doctor_comment">
			<?php if(have_rows('doctor_prof',$case_doctor_id)): ?>
			<?php while(have_rows('doctor_prof',$case_doctor_id)): the_row();?>
					
				<?php 
					$image = get_sub_field('doctor_photo',$case_doctor_id);
					if( !empty($image) ): 
						// vars
						$alt = $image['alt'];
						$url =$image['url'];
						$caption = $image['caption'];
						// thumbnail
						$size = 'full';
						$thumb = $image['sizes'][ $size ];
						$width = $image['sizes'][ $size . '-width' ];
						$height = $image['sizes'][ $size . '-height' ];
					if( $caption ): ?>
					<?php endif; ?>

				<?php endif; ?>
				<dt>
				<?php 
				/*
				牧さんから医師名のGOが出たらここを出す
				<img src="<?php echo $url; ?>">
				<span><?php echo get_the_title($case_doctor_id); ?></span>
				*/
				 ?>

				<img src="<?php echo $upload_dir['baseurl']; ?>/2017/02/img-doctor-detail-none-2.jpg">
				<span>担当医師</span>
				</dt>
				<?php endwhile; ?>
			<?php endif; ?>
		
			<dd class="arrow_box">
				<?php
					the_content();
					wp_link_pages( array(
						'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentysixteen' ) . '</span>',
						'after'       => '</div>',
						'link_before' => '<span>',
						'link_after'  => '</span>',
						'pagelink'    => '<span class="screen-reader-text">' . __( 'Page', 'twentysixteen' ) . ' </span>%',
						'separator'   => '<span class="screen-reader-text">, </span>',
					) );
					if ( '' !== get_the_author_meta( 'description' ) ) {
						get_template_part( 'template-parts/biography' );
					}
				?>
			</dd>
				<?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>

		</dl>
		</div>
		</section><!-- .entry-content -->
	<?php endif; ?>
	
	
	<section class="case_pt_link">
		<div class="case_pt_link_cate">	
		<h4>関連する体験談を見る</h4>
		<?php 
		  $terms = get_the_terms($post->ID,'menu_category'); 
		  foreach( $terms as $term ) { 
		  ?>
		<a href="<?php echo esc_url( home_url( '/' ) ); ?>menu_category/<?php echo($term->slug); ?>/?post_type=questionnaire">
				<?php echo($term->name); ?>の体験談一覧
		</a>
		<?php }?>

			<ul>
			<?php 
			  $terms = get_the_terms($post->ID,'ope_name'); 
			  foreach( $terms as $term ) { 
			  ?>
				<li>
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>questionnaire/?ope_name=<?php echo($term->slug); ?>">
							<?php echo($term->name); ?>
					</a>
				</li>
				<?php }?>
			</ul>
		</div>	
	</section>
	
	
	
	
	
	
	
	

	<?php /*関連する体験談が存在している時だけ出てくるよ*/
	  $terms = get_the_terms($post->ID,'menu_category'); 
	  foreach( $terms as $term ) { 

		$tax_posts = get_posts(array(
		  'post_type' => 'case_photo',
		  'posts_per_page' => -1,
		  'tax_query' => array(
			array(
			  'taxonomy' => 'menu_category',
			  'terms' => array($term->slug),
			  'field' =>'slug',
			  'include_children' => true,
			  'operator' => 'IN'
			),
			'relation' =>'AND'
		  )
		));
		if($tax_posts):?>
		
		<section class="case_pt_link">
			<div class="case_pt_link_cate">	
				<h4>関連する症例一覧を見る</h4>
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>menu_category/<?php echo($term->slug); ?>/?post_type=case_photo">
					<?php echo($term->name); ?>の症例一覧
				</a>
				<ul>
				<?php 
				  $terms = get_the_terms($post->ID,'ope_name'); 
				  foreach( $terms as $term ) { 
				  ?>
					<li>
						<a href="<?php echo esc_url( home_url( '/' ) ); ?>case_photo/?ope_name=<?php echo($term->slug); ?>">
								<?php echo($term->name); ?>
						</a>
					</li>
					<?php }?>
				</ul>		
			</div>	
		</section>
		<?php endif;?>

	<?php }?>
	
	
	

	

	</section>
	
	<?php $awasete = get_field('case_pt_relations'); ?>
	<?php if($awasete): ?>
	<section class="case_pt_relations">
	  <h2>関連症例</h2>
	  <?php foreach((array)$awasete as $value):?>
		  <a href="<?php echo get_the_permalink($value->ID); ?>">
			<h3><?php echo $value->post_title; ?></h3>
	 
		<?php if( have_rows('case_pt_photos',$value->ID)): // check for repeater fields ?>


					 <ul>
						  <?php
						  while ( have_rows("case_pt_photos",$value->ID) ) : the_row();
						  ?>
						  <li>
							   <?php 
								$image = get_sub_field('case_pt_photo',$value->ID);
								if( !empty($image) ): 
									$url = $image['url'];
									$title = $image['title'];
									$alt = $image['alt'];
									$caption = $image['caption'];
									$size = 'full';
								 ?>
									<img src="<?php echo $url; ?>" alt="<?php echo $alt; ?>"/>
								<?php endif; ?>
						  </li>
						  <?php endwhile; ?>
					 </ul>
		
				<?php // whatever post stuff you want goes here ?>

		<!-- End Repeater -->
		<?php endif; ?>
		   
		  </a>
		
	  <?php endforeach; ?>
	  </section>
	<?php endif; ?>



	<footer class="entry-footer">
		<?php twentysixteen_entry_meta(); ?>
		<?php
			edit_post_link(
				sprintf(
					/* translators: %s: Name of current post */
					__( 'Edit<span class="screen-reader-text"> "%s"</span>', 'twentysixteen' ),
					get_the_title()
				),
				'<span class="edit-link">',
				'</span>'
			);
		?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
