<?php
/**
 * The template part for displaying single posts
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
 $upload_dir = wp_upload_dir(); 
?>

<article id="post-<?php the_ID(); ?>" <?php post_class("single_case_photo type-page"); ?>>
	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		<?php
		// subtitle
		 if( get_field("menu_subtitle") ): ?>
					   <p><?php the_field('menu_subtitle');?></p>
			
		<?php endif; ?>
	</header><!-- .entry-header -->

	<?php twentysixteen_excerpt(); /* 抜粋 */?>

	<?php twentysixteen_post_thumbnail();　/* サムネイル */ ?>


	<!-- 症例 -->
	<?php if( have_rows('case_pt_photos')): // check for repeater fields ?>

	<section id="menu_case_photos">


				 <ul>
					  <?php
					  while ( have_rows("case_pt_photos") ) : the_row();
					  ?>
					  <li>
						   <?php 
							$image = get_sub_field('case_pt_photo');
							if( !empty($image) ): 
								$url = $image['url'];
								$title = $image['title'];
								$alt = $image['alt'];
								$caption = $image['caption'];
								$size = 'full';
							 ?>
								<img src="<?php echo $url; ?>" alt="<?php echo $alt; ?>"/>
							<?php endif; ?>
					  </li>
					  <?php endwhile; ?>
				 </ul>
	
			<?php // whatever post stuff you want goes here ?>

	</section>
	<!-- End Repeater -->
	<?php endif; ?>


	
	
	<section class="case_pt_link">
		<h4>施術内容</h4>
		<ul><?php 
			$ope_names = get_the_terms($my_ID, 'ope_name');
			$tmp=$ope_names;
			if( !empty($ope_names) ) {
			foreach($ope_names as $ope_name){
				$my_term_name = $ope_name -> name;
				$my_term_slug = $ope_name -> slug;
			?>
				<li>
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>menu/<?php echo $my_term_slug;?>"><?php echo $my_term_name;?></a>
				</li>
			<?php 	}} ?>
		</ul>
	</section>	

	
	<?php
	// explanation_txt
	 if( get_field("case_pt_txt") ): ?>
		<section id="menu_explanation">
			<h4>ドクターのコメント</h4>
				<div class="menu_explanation_txt" style="width:100%;">
					<?php the_field('case_pt_txt');?>
				</div>
		</section>
	<?php endif; ?>
	
	
	
	<?php if($post->post_content=="") : ?>
		<!-- ここに記事本文が空だった場合に表示したいソースを記述  -->
	<?php else : ?>
		<section class="entry-content">
		<?php
			the_content();

			wp_link_pages( array(
				'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentysixteen' ) . '</span>',
				'after'       => '</div>',
				'link_before' => '<span>',
				'link_after'  => '</span>',
				'pagelink'    => '<span class="screen-reader-text">' . __( 'Page', 'twentysixteen' ) . ' </span>%',
				'separator'   => '<span class="screen-reader-text">, </span>',
			) );

			if ( '' !== get_the_author_meta( 'description' ) ) {
				get_template_part( 'template-parts/biography' );
			}
		?>
		</section><!-- .entry-content -->
	<?php endif; ?>
	
	

	<?php /*関連する体験談が存在している時だけ出てくるよ*/
	  $terms = get_the_terms($post->ID,'ope_name'); 
	  foreach( $terms as $term ) { 

		$tax_posts = get_posts(array(
		  'post_type' => 'questionnaire',
		  'posts_per_page' => -1,
		  'tax_query' => array(
			array(
			  'taxonomy' => 'ope_name',
			  'terms' => array($term->slug),
			  'field' =>'slug',
			  'include_children' => true,
			  'operator' => 'IN'
			),
			'relation' =>'AND'
		  )
		));
		if($tax_posts):?>
		
		<section class="case_pt_link">
			<div class="case_pt_link_cate">	
				<h4>関連する体験談を見る</h4>
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>menu_category/<?php echo($term->slug); ?>/?post_type=questionnaire">
						<?php echo($term->name); ?>の体験談一覧
				</a>
				<ul>
				<?php 
				  $terms = get_the_terms($post->ID,'ope_name'); 
				  foreach( $terms as $term ) { 
				  ?>
					<li>
						<a href="<?php echo esc_url( home_url( '/' ) ); ?>questionnaire/?ope_name=<?php echo($term->slug); ?>">
								<?php echo($term->name); ?>
						</a>
					</li>
					<?php }?>
				</ul>		
			</div>	
		</section>
		<?php endif;?>

	<?php }?>



	<section class="case_pt_link">
		<div class="case_pt_link_cate">	
		<h4>関連する症例一覧を見る</h4>
		<?php 
		  $terms = get_the_terms($post->ID,'menu_category'); 
		  foreach( $terms as $term ) { 
		  ?>
		<a href="<?php echo esc_url( home_url( '/' ) ); ?>menu_category/<?php echo($term->slug); ?>/?post_type=case_photo">
				<?php echo($term->name); ?>の症例一覧
		</a>
		<?php }?>
			<ul>
			<?php 
			  $terms = get_the_terms($post->ID,'ope_name'); 
			  foreach( $terms as $term ) { 
			  ?>
				<li>
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>case_photo/?ope_name=<?php echo($term->slug); ?>">
							<?php echo($term->name); ?>
					</a>
				</li>
				<?php }?>
			</ul>
		</div>	
	</section>
	
	 
	<?php $awasete = get_field('case_pt_relations'); ?>
	<?php if($awasete): ?>
	<section class="case_pt_relations">
	  <h2>オススメ症例</h2>
	  <?php foreach((array)$awasete as $value):?>
		  <a href="<?php echo get_the_permalink($value->ID); ?>">
			<h3><?php echo $value->post_title; ?></h3>
			<?php if( have_rows('case_pt_photos',$value->ID)): // check for repeater fields ?>
			 <ul>
				  <?php
				  while ( have_rows("case_pt_photos",$value->ID) ) : the_row();
				  ?>
				  <li>
					   <?php 
						$image = get_sub_field('case_pt_photo',$value->ID);
						if( !empty($image) ): 
							$url = $image['url'];
							$title = $image['title'];
							$alt = $image['alt'];
							$caption = $image['caption'];
							$size = 'full';
						 ?>
							<img src="<?php echo $url; ?>" alt="<?php echo $alt; ?>"/>
						<?php endif; ?>
				  </li>
				  <?php endwhile; ?>
			 </ul>

				<?php // whatever post stuff you want goes here ?>

		<!-- End Repeater -->
		<?php endif; ?>
		   
		  </a>
		
	  <?php endforeach; ?>
	  </section>
	<?php endif; ?>



	<footer class="entry-footer">
		<?php twentysixteen_entry_meta(); ?>
		<?php
			edit_post_link(
				sprintf(
					/* translators: %s: Name of current post */
					__( 'Edit<span class="screen-reader-text"> "%s"</span>', 'twentysixteen' ),
					get_the_title()
				),
				'<span class="edit-link">',
				'</span>'
			);
		?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
