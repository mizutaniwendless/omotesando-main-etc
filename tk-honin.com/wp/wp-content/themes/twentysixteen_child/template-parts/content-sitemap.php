<?php
/**
 * The template part for displaying single posts
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
 $upload_dir = wp_upload_dir(); 
?>

<article id="post-<?php the_ID(); ?>" <?php post_class("menu_page type-page"); ?>>
	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		<?php
		// subtitle
		 if( get_field("menu_subtitle") ): ?>
		 <p><?php the_field('menu_subtitle');?></p>
		<?php endif; ?>
	</header><!-- .entry-header -->

	<?php twentysixteen_excerpt(); /* 抜粋 */?>

	<?php twentysixteen_post_thumbnail();　/* サムネイル */ ?>


	<section class="entry-content">
	
	<?php
			the_content();

			wp_link_pages( array(
				'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentysixteen' ) . '</span>',
				'after'       => '</div>',
				'link_before' => '<span>',
				'link_after'  => '</span>',
				'pagelink'    => '<span class="screen-reader-text">' . __( 'Page', 'twentysixteen' ) . ' </span>%',
				'separator'   => '<span class="screen-reader-text">, </span>',
			) );

			if ( '' !== get_the_author_meta( 'description' ) ) {
				get_template_part( 'template-parts/biography' );
			}
		?>
	
	
		<?php if (is_page('sitemap')) { ?>
			<ul class="sitemap">
			<?php wp_list_pages('title_li=&exclude=501,528,2368'); ?>
	

				<li class="page_item">
				<a href="<?php echo get_post_type_archive_link( 'menu' ); ?>"><?php echo esc_html( get_post_type_object( menu )->label ); ?></a>


			<?php 
				$parent_id = $post->post_parent; // 親ページのIDを取得
				if ($parent_id) { // 親ページがあれば実行
					$parent = get_post($parent_id);  // 親ページの情報を取得
					echo get_the_title($parent->post_parent); // 親ページの親のタイトルを取得
				}
			?>

			<?php // WordPress のタクソノミーの親・子のターム一覧とタームに紐づく記事一覧の出力
				$categories = get_terms('menu_category','parent=0');
				foreach ( $categories as $cat ) {
					echo '<ul class="sitemap_sub">'; // 親カテゴリタイトル
					echo '<li class="sitemap_catettl">' . esc_html($cat->name) . ''; // 親カテゴリタイトル
					
					$children = get_terms('menu_category','hierarchical=0&parent='.$cat->term_id);
					if($children){ // 子カテゴリの有無
						foreach ( $children as $child ) {
							echo '<li><ul><li>' . esc_html($child->name) . ''; // 子カテゴリタイトル
							$catslug = $child->slug;
							$args = array(
								'post_type' => 'menu',
								'menu_category' => $catslug ,
								'posts_per_page' => -1
							);
							$myquery = new WP_Query( $args ); 
			?>
			
				<ul>
				<?php if ( $myquery->have_posts()): ?>
					<?php while($myquery->have_posts()): $myquery->the_post(); ?>
					<li class="sitemap_menuname"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></li>
					<?php endwhile; ?>
				<?php endif; ?>
				</ul>

			</li>
		</ul>
		</li>
	<?php wp_reset_postdata(); ?>
	<?php } //子カテゴリに紐づく記事一覧の出力終了 ?>
	<?php }  ?>
	</ul>

	<?php } // 出力の終了 ?>

	</li>
	</ul>

	<?php }; ?>
		
	</section><!-- .entry-content -->



	<footer class="entry-footer">
		<?php twentysixteen_entry_meta(); ?>
		<?php
			edit_post_link(
				sprintf(
					/* translators: %s: Name of current post */
					__( 'Edit<span class="screen-reader-text"> "%s"</span>', 'twentysixteen' ),
					get_the_title()
				),
				'<span class="edit-link">',
				'</span>'
			);
		?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
