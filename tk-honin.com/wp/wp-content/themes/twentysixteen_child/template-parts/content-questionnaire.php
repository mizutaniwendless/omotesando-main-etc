<?php
/**
 * The template part for displaying single posts
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
 $upload_dir = wp_upload_dir(); 
?>	
<article id="post-<?php the_ID(); ?>" <?php post_class("case_photo questionnaire type-page"); ?>>
	<header class="entry-header">
		<h1 class="entry-title"><?php echo get_the_title(2309); ?></h1>
			 <p>
				<?php 
				$url = $_SERVER['REQUEST_URI'];
				if(isset($_GET['ope_name'])){
					 $ope_name = $_GET['ope_name'];
						echo "<strong>".get_term_by('slug',$ope_name,"ope_name")->name."</strong>の" ;
				}
				if(isset($_GET['post_type'])){
						echo "<strong>".get_term_by('slug',$term,"menu_category")->name."</strong>の" ;
				}					   
				echo "モニターになられた方の体験談を掲載しています。";
				 ?>	   
			 </p>
			 
	</header><!-- .entry-header -->
	
	<div class="wrapper">
		<form>
			<div>
				<p>診療科目</p>
				<select class="parent" name="foo">
				<option value="" selected="selected">カテゴリを選択</option>
				<?php
				$taxonomyName = "menu_category";
				//This gets top layer terms only.  This is done by setting parent to 0.  
				$parent_terms = get_terms( $taxonomyName, array( 'parent' => 0, 'orderby' => 'slug', 'hide_empty' => false ) );
				foreach ( $parent_terms as $pterm ) {
					//Get the Child terms
					$taxonomys = get_terms( $taxonomyName, array( 'parent' => $pterm->term_id, 'orderby' => 'slug', 'hide_empty' => false ) );
					foreach ( $taxonomys as $taxonomy ) {
						$url = get_term_link($taxonomy->slug, $taxonomy_name);
						$tax_posts = get_posts(array(
						  'post_type' => 'questionnaire',
						  'posts_per_page' => -1,
						  'tax_query' => array(
							array(
							  'taxonomy' => 'menu_category',
							  'terms' => array($taxonomy->slug),
							  'field' =>'slug',
							  'include_children' => true,
							  'operator' => 'IN'
							),
							'relation' =>'AND'
						  )
						));
						if($tax_posts):?>
							<option value="<?php echo($taxonomy->slug); ?>"><?php echo($taxonomy->name); ?></option>
						<?php endif;
					}
				}?>
				</select>
			</div>

			<div>
				<p>施術名</p>
				
				<select class="children" name="bar" disabled>
				<option value="" selected="selected">施術名を選択</option>

				<?php
				 $args = array(
				   'orderby' => 'id',
				   'order' =>'ASC'
				  );
				  $taxonomy_name = 'menu_category';
				  $taxonomys = get_terms($taxonomy_name,$args);
				  if(!is_wp_error($taxonomys) && count($taxonomys)):
						foreach($taxonomys as $taxonomy):
						$url = get_term_link($taxonomy->slug, $taxonomy_name);
						$tax_posts = get_posts(array(
						  'post_type' => 'questionnaire',
						  'posts_per_page' => -1,
						  'tax_query' => array(
							array(
							  'taxonomy' => 'menu_category',
							  'terms' => array($taxonomy->slug),
							  'field' =>'slug',
							  'include_children' => true,
							  'operator' => 'IN'
							),
							'relation' =>'AND'
						  )
						));
						if($tax_posts):?>

							<?php
							 $ope_args = array(
							   'orderby' => 'id',
							   'order' =>'ASC'
							  );
							  $ope_taxonomy_name = 'ope_name';
							  $ope_taxonomys = get_terms($ope_taxonomy_name,$args);
							  if(!is_wp_error($ope_taxonomys) && count($ope_taxonomys)):
									foreach($ope_taxonomys as $ope_taxonomy):
									$ope_url = get_term_link($ope_taxonomy->slug, $ope_taxonomy_name);
									$ope_tax_posts = get_posts(array(
									  'post_type' => 'questionnaire',
									  'posts_per_page' => -1,
										'tax_query' => array (
											'relation' => 'AND',
											array (
										  'taxonomy' => 'menu_category',
										  'terms' => array($taxonomy->slug),
										  'field' =>'slug',
										  'include_children' => true,
										  'operator' => 'IN'
											),
											array (
										  'taxonomy' => 'ope_name',
										  'terms' => array($ope_taxonomy->slug),
										  'field' =>'slug',
										  'include_children' => true,
										  'operator' => 'IN'
											)
										)
									));
									if($ope_tax_posts):?>
										<option value="<?php echo esc_url( home_url( '/' ) ); ?>questionnaire/?ope_name=<?php echo($ope_taxonomy->slug); ?>" data-val="<?php echo($taxonomy->slug); ?>"><?php echo($ope_taxonomy->name); ?>（<?php echo count($ope_tax_posts); ?>件）</option>

									<?php endif;
								endforeach;
							endif; ?>

						<?php endif;
					endforeach;
				endif; ?>
				</select>

			</div>

			<div>
			<input id="pt_button_questionnaire" type=button onClick="" value="検索" >
			</div>
		</form>
	</div>
	<!-- /.wrapper -->

	
	
	
	
	
	

	
	<?php
	$url = $_SERVER['REQUEST_URI'];
	if(!isset($_GET['ope_name'])): 
		if(strstr($url,'page')==false):
	?>
		<h2>新着体験談</h2>

	<?php endif;
	endif; ?>

			<?php if(have_posts()): ?>
				<?php
                	while(have_posts()): the_post();
					$my_ID = get_the_ID();
					$ope_names = get_the_terms($my_ID, 'ope_name');
					$menu_categorys = get_the_terms($my_ID, 'menu_category');
					
					$description = get_field('questionnaire_description');
					$response = get_field('questionnaire_response');
					$grad = get_field('questionnaire_grad');
					$hurt = get_field('questionnaire_hurt');
					$result = get_field('questionnaire_result');
					$doctor = get_field('questionnaire_doctor');
					$hospital = get_field('questionnaire_hospital');
					$questionnaire_txt = get_field('questionnaire_txt');
					$gender = get_field('questionnaire_gender');
					$age = get_field('questionnaire_age');
					$job = get_field('questionnaire_job');

					$allresult =($description+$response+$grad+$hurt+$result+$doctor+$hospital)/7*20;

					
				?>
				
                <section>
					<h3><span>
					<?php 
					if( !empty($menu_categorys) ) {
					foreach($menu_categorys as $menu_category){
						$menu_category_name = $menu_category -> name;
						echo "カテ：".$menu_category_name;
					}
					} ?>
					<br>
					<?php if( !empty($ope_names) ) {
						echo "施術：";
						
					foreach($ope_names as $ope_name){
						    if ($ope_name === end($ope_names)) {
								// 最後
								$ope_name = $ope_name -> name;
								echo $ope_name;
							}else{
							$ope_name = $ope_name -> name;
							echo $ope_name."・";
								
							}

					}
					}
					 ?>
					 </span>
					</h3>
                	<div>
						<div class="questionnaire_img">
						   <?php 
							$image = get_field('questionnaire_image');
							if( !empty($image) ): 
								$url = $image['url'];
								$title = $image['title'];
								$alt = $image['alt'];
								$caption = $image['caption'];
								$size = 'thumbnail';
								$thumb = $image['sizes'][ $size ];
								$width = $image['sizes'][ $size . '-width' ];
								$height = $image['sizes'][ $size . '-height' ];
							 ?>
								<a href="<?php the_permalink(); ?>"><img src="<?php echo $thumb; ?>" alt="<?php echo $alt; ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>"/></a>
							<?php else: ?>	
								<a href="<?php the_permalink(); ?>"><img src="<?php echo $upload_dir['baseurl']; ?>/img/noimage_questionnaire.png" alt="noimage" width="<?php echo $width; ?>" height="<?php echo $height; ?>"/></a>
							
							<?php endif; ?>
						</div>
						
						
						
						<div class="questionnaire_txt">
							<h4><?php echo $job; ?>/<?php echo $age; ?>歳/<?php
							if ($gender == 'female'){
								echo '女性';
							} elseif ($gender == 'male'){
								echo '男性';
							}else{
								echo 'その他'; 
							}
							?>
							
							<div class="questionnaire_stars">
						<p>満足度<?php echo round($allresult); ?>％</p>
						<div class="star-ratings-sprite"><span style="width:<?php echo $allresult; ?>%" class="star-ratings-sprite-rating"></span></div>
						</div>
							
							
							</h4>
							<div>
							<p><?php /*
								$tmp=$ope_names;
								if( !empty($ope_names) ) {
								foreach($ope_names as $ope_name){
									$my_term_name = $ope_name -> name;
									$my_term_slug = $ope_name -> slug;
									echo $my_term_name;
									if(next($tmp)){
										echo "｜"; // 最後の要素ではないとき
									}
									}
								}*/
								 ?>
								<?php
									if(mb_strlen($questionnaire_txt, 'UTF-8')>100){
										$questionnaire_txt= mb_substr($questionnaire_txt, 0, 100, 'UTF-8');
										echo $questionnaire_txt.'……';
									}else{
										echo $questionnaire_txt;
									}
								?></p>
							</div>
						</div>
						
						
						

						<?php // whatever post stuff you want goes here ?>
						<div class="case_pt_detail"><a href="<?php the_permalink(); ?>">詳細を見る >></a></div>
					</div>
                 </section>
                <?php endwhile; ?>
            <?php endif; ?>
           <!-- /.mrl30 -->

<?php
//Pagenation 
if (function_exists("pagination")) {
	pagination($additional_loop->max_num_pages);
}
?><!-- /.pagination -->
      
      
        
        

        
    </article>

