<?php
/**
 * The template part for displaying single posts
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
 $upload_dir = wp_upload_dir(); 
?>

<script>
jQuery(function(){
	jQuery('#rsvcontents section').hide();
    jQuery("a").click(function(){
        jQuery("#rsvcontents section").hide();
        jQuery(jQuery(this).attr("href")).show();
        jQuery(".current").removeClass("current");
        jQuery(this).addClass("current");
		
		return false;
    }); 
});
</script>

<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<script type="text/javascript">
// <![CDATA[
jQuery(function(a){a.datepicker.regional.ja={closeText:"閉じる",prevText:"&#x3c;前",nextText:"次&#x3e;",currentText:"今日",monthNames:["1月","2月","3月","4月","5月","6月","7月","8月","9月","10月","11月","12月"],monthNamesShort:["1月","2月","3月","4月","5月","6月","7月","8月","9月","10月","11月","12月"],dayNames:["日曜日","月曜日","火曜日","水曜日","木曜日","金曜日","土曜日"],dayNamesShort:["日","月","火","水","木","金","土"],dayNamesMin:["日","月","火","水","木","金","土"],weekHeader:"週",dateFormat:"yy/mm/dd",firstDay:0,isRTL:!1,showMonthAfterYear:!0,yearSuffix:"年"},a.datepicker.setDefaults(a.datepicker.regional.ja)});
// ]]></script>
<script type="text/javascript">
jQuery(function(){

jQuery('table#rsv_omotesando input[name="reserve1"],table#rsv_omotesando input[name="reserve2"],table#rsv_omotesando input[name="reserve3"]').datepicker({
dateFormat:'yy/mm/dd', 
minDate: 0, 
beforeShowDay:function(date){

	if(date.getDay() == 3 ){ return [false,'','']; }
	ymd = date.toISOString().substr(0,10); //ISO Timeなので９時間前
	if( ymd=='2016-12-29' || ymd=='2016-12-30' || ymd=='2016-12-31' || ymd=='2017-01-01' || ymd=='2017-01-02' || ymd=='2017-01-04' || ymd=='2017-01-05'){ return [false,'','']; }

	return [true,'',''];
}
});

jQuery('table#rsv_osaka input[name="reserve1"],table#rsv_osaka input[name="reserve2"],table#rsv_osaka input[name="reserve3"]').datepicker({
dateFormat:'yy/mm/dd', 
minDate: 0, 
beforeShowDay:function(date){

	if(date.getDay() == 1 || date.getDay() == 4 ){ return [false,'','']; }
	ymd = date.toISOString().substr(0,10); //ISO Timeなので９時間前
	if( ymd=='2016-12-29' || ymd=='2016-12-30' || ymd=='2016-12-31' || ymd=='2017-01-01' || ymd=='2017-01-02' || ymd=='2017-01-04' || ymd=='2017-01-05'){ return [false,'','']; }

	return [true,'',''];
}
});
jQuery('table#rsv_nagoya input[name="reserve1"],table#rsv_nagoya input[name="reserve2"],table#rsv_nagoya input[name="reserve3"]').datepicker({
dateFormat:'yy/mm/dd', 
minDate: 0, 
beforeShowDay:function(date){

	if(date.getDay() == 1 || date.getDay() == 2 ){ return [false,'','']; }
	ymd = date.toISOString().substr(0,10); //ISO Timeなので９時間前
	if( ymd=='2016-12-29' || ymd=='2016-12-30' || ymd=='2016-12-31' || ymd=='2017-01-01' || ymd=='2017-01-02' || ymd=='2017-01-04' || ymd=='2017-01-05'){ return [false,'','']; }

	return [true,'',''];
}
});
jQuery('table#rsv_okinawa input[name="reserve1"],table#rsv_okinawa input[name="reserve2"],table#rsv_okinawa input[name="reserve3"]').datepicker({
dateFormat:'yy/mm/dd', 
minDate: 0, 
beforeShowDay:function(date){

	if(date.getDay() == 1 ){ return [false,'','']; }
	ymd = date.toISOString().substr(0,10); //ISO Timeなので９時間前
	if( ymd=='2016-12-29' || ymd=='2016-12-30' || ymd=='2016-12-31' || ymd=='2017-01-01' || ymd=='2017-01-02' || ymd=='2017-01-04' || ymd=='2017-01-05'){ return [false,'','']; }

	return [true,'',''];
}
});
});
</script>
<style type="text/css">
<!--
/* jqui.css上書き */
.ui-state-default, .ui-widget-content .ui-state-default, .ui-widget-header .ui-state-default, .ui-button, html .ui-button.ui-state-disabled:hover, html .ui-button.ui-state-disabled:active{
	background: #f99 !important;
}
.ui-state-disabled span,
.ui-widget-content .ui-state-disabled span,
.ui-widget-header .ui-state-disabled span{
	background: #ddd !important;
}
-->
</style>


<article id="post-<?php the_ID(); ?>" <?php post_class("type-page"); ?>>
	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		<?php
		// subtitle
		 if( get_field("menu_subtitle") ): ?>
					   <p><?php the_field('menu_subtitle');?></p>
			 </ul>
		<?php endif; ?>
	</header><!-- .entry-header -->

	<section class="entry-content">
		<?php
			the_content();

			wp_link_pages( array(
				'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentysixteen' ) . '</span>',
				'after'       => '</div>',
				'link_before' => '<span>',
				'link_after'  => '</span>',
				'pagelink'    => '<span class="screen-reader-text">' . __( 'Page', 'twentysixteen' ) . ' </span>%',
				'separator'   => '<span class="screen-reader-text">, </span>',
			) );

			if ( '' !== get_the_author_meta( 'description' ) ) {
				get_template_part( 'template-parts/biography' );
			}
		?>
	</section><!-- .entry-content -->

<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
	<footer class="entry-footer">
		<?php twentysixteen_entry_meta(); ?>
		<?php
			edit_post_link(
				sprintf(
					/* translators: %s: Name of current post */
					__( 'Edit<span class="screen-reader-text"> "%s"</span>', 'twentysixteen' ),
					get_the_title()
				),
				'<span class="edit-link">',
				'</span>'
			);
		?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
