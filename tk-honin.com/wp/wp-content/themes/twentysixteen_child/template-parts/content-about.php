<?php
/**
 * The template part for displaying single posts
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
 $upload_dir = wp_upload_dir(); 
?>
<!-- 
<script>
jQuery(function(){
	jQuery('#aboutcontents section').hide();
    jQuery("a").click(function(){
        jQuery("#aboutcontents section").hide();
        jQuery(jQuery(this).attr("href")).show();
        jQuery(".current").removeClass("current");
        jQuery(this).addClass("current");
		
		return false;
    }); 
});
</script>
-->
<article id="post-<?php the_ID(); ?>" <?php post_class("about_page type-page"); ?>>
	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
	</header><!-- .entry-header -->

	<section class="entry-content">
		<?php
			the_content();

			wp_link_pages( array(
				'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentysixteen' ) . '</span>',
				'after'       => '</div>',
				'link_before' => '<span>',
				'link_after'  => '</span>',
				'pagelink'    => '<span class="screen-reader-text">' . __( 'Page', 'twentysixteen' ) . ' </span>%',
				'separator'   => '<span class="screen-reader-text">, </span>',
			) );

			if ( '' !== get_the_author_meta( 'description' ) ) {
				get_template_part( 'template-parts/biography' );
			}
		?>
	</section><!-- .entry-content -->
	

	
	
	<ul class="about_cliniclist" id="about-nav">
	<?php /*if(have_rows('clinic_about')): ?>
		<?php while(have_rows('clinic_about')): the_row(); ?>
		<?php $post_objects = get_sub_field('about_slug');?>
		<li><a href="#<?php echo $post_objects->post_name;?>"><?php echo the_sub_field('about_clinicname'); ?>はこちら</a></li>
		<?php endwhile; ?>
	<?php endif; */?>
	</ul>
	<div id="aboutcontents">
	

	
	
    <?php $args = array(
        'numberposts' => -1,                //表示（取得）する記事の数
        'post_type' => 'clinic'    //投稿タイプの指定
    );
    $customPosts = get_posts($args);
    if($customPosts) : foreach($customPosts as $post) : setup_postdata( $post ); ?>
		<section id="<?php  $terms = get_the_terms($post->ID,'com_category');  foreach( $terms as $term ) { ?><?php echo($term->slug); ?><?php }?>">
			<h3><?php the_title(); ?></h3>

			<div class="clinic_sub">
				<?php if(have_rows('clinic_about')): ?>
					<?php while(have_rows('clinic_about')): the_row();
						$clinicphoto = get_sub_field('about_clinicphoto');
						$googlemap = get_sub_field('about_googlemap');
						$postalcode = get_sub_field('about_postalcode');
						$address = get_sub_field('about_address');
						$traffics = get_sub_field('about_traffics');
						$tel = get_sub_field('about_tel');
						$open = get_sub_field('about_open');
						$close = get_sub_field('about_close');
						$time = get_sub_field('about_time');
					?>
					<div class="cphoto">
					<?php 
						$image = get_sub_field('about_clinicphoto');
						if( !empty($image) ): 
							// vars
							$alt = $image['alt'];
							$caption = $image['caption'];
							// thumbnail
							$size = 'thumbnail';
							$thumb = $image['sizes'][ $size ];
							$width = $image['sizes'][ $size . '-width' ];
							$height = $image['sizes'][ $size . '-height' ];
						if( $caption ): ?>
						<?php endif; ?>
					<img src="<?php echo $thumb; ?>" alt="<?php echo $alt; ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>" />
					<?php endif; ?>
					</div>
					<div class="">
						<table class="cdetails">
							<tbody>
							<tr>
								<th class="arrow_box">住所</th>
								<td>〒<?php echo $postalcode; ?>&nbsp;<?php echo $address; ?></td>
							</tr>
							<tr>
								<th class="arrow_box">TEL</th>
								<td>
									<span class="visiblePc">
										<?php echo $tel; ?>
									</span>
									<span class="visibleSp">
										<a href="tel:<?php echo $tel; ?>" onclick="ga('send', 'event', 'sp', 'tel-tap', 'tel');">
											<?php echo $tel; ?>
										</a>
									</span>
								</td>
							</tr>
							<tr>
								<th>診察日</th>
								<td><?php echo the_sub_field('about_open'); ?></td>
							</tr>
							<tr>
								<th>休診日</th>
								<td><?php echo the_sub_field('about_close'); ?></td>
							</tr>
							<tr>
								<th>診療時間</th>
								<td><?php echo $time; ?></td>
							</tr>
							</tbody>
						</table>
					</div>
					
					</div>
					
					
					<?php
					$postobject = get_sub_field('about_slug');
					if( !empty($postobject) ):?>	
						<div class="doctor_next">
							<?php $postobject = get_sub_field('about_slug');?>
							<a href="<?php echo $postURL = get_permalink($postobject->ID);?>">
							<?php echo $postobject->post_title;?></a>
						</div>	
					<?php endif;?>
					

					

					<div class="doctor_next"><a href="<?php echo esc_url( home_url( '/' ) ); ?>access/#<?php $terms = get_the_terms($post->ID,'com_category');  foreach( (array)$terms as $term ) { ?><?php echo($term->slug); ?><?php }?>">アクセスはこちら</a></div>
		</section>
	
					

					<?php endwhile; ?>
				<?php endif; ?>
			

	
		
    <?php endforeach; ?>
    <?php else : //記事が無い場合 ?>
       <p>登録がありません。</p>
    <?php endif;
    wp_reset_postdata(); //クエリのリセット ?>


	</div>
			
			

	


	<footer class="entry-footer">
		<?php twentysixteen_entry_meta(); ?>
		<?php
			edit_post_link(
				sprintf(
					/* translators: %s: Name of current post */
					__( 'Edit<span class="screen-reader-text"> "%s"</span>', 'twentysixteen' ),
					get_the_title()
				),
				'<span class="edit-link">',
				'</span>'
			);
		?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
