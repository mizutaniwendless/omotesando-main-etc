<?php
/**
 * The template used for displaying page content
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>
<?php $upload_dir = wp_upload_dir(); ?>
<article id="post-<?php the_ID(); ?>" <?php post_class("home"); ?>>
	<?php
	// NEWS
	 if( have_rows("top_news") ): ?>
		<section id="top_news">
			 <h3>NEWS</h3>
			 <ul>
				  <?php
				  while ( have_rows("top_news") ) : the_row();
				  ?>
				  <li>
					   <h4><?php the_sub_field('top_news_clinic_name');?></h4>
					   <p><?php the_sub_field('top_news_clinic_txt');?></p>
				  </li>
				  <?php endwhile; ?>
			 </ul>
		</section>
	<?php endif; ?>
	
	<section id="top_pickup">
	

	
	
	
	
		<?php
		// pickup
		 if( have_rows("top_pickup") ): ?>
			 <h3>Pick UP</h3>
			 <ul id="banner_bxSlider">
				  <?php
				  while ( have_rows("top_pickup") ) : the_row();
				  ?>
				  	<?php
						$top_pickup_title = get_sub_field('top_pickup_title');
						$top_pickup_multi_id = get_sub_field('top_pickup_multi_id');
						
						
					?>	
					
					<?php if( get_sub_field('top_pickup_multi') ): // 複数サイトへ飛ぶ ?>

							<li class="inline link" data-role="#<?php echo $top_pickup_multi_id; ?>">
									<div>
											<a class="inline" href="#link">
												<?php 
												$image = get_sub_field('top_pickup_sp_image');
												if( !empty($image) ): ?>
												<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
												<?php endif; ?>
											</a>
									</div>

									<!-- This contains the hidden content for inline calls -->
									
									<div style='display:none'>
										<div id='<?php echo $top_pickup_multi_id; ?>' style='padding:10px; background:#fff;'>
										<p><strong>院名をお選び下さい</strong></p>
										<?php
										// pickup
										 if( have_rows("top_pickup_urls") ): ?>
											<?php
											while ( have_rows("top_pickup_urls") ) : the_row();
												$top_pickup_repeater_url = get_sub_field('top_pickup_repeater_url');
												$top_pickup_repeater_clinicname = get_sub_field('top_pickup_repeater_clinicname');
											?>	

											<div>
												<a href="<?php echo $top_pickup_repeater_url; ?>">
												<?php echo $top_pickup_repeater_clinicname; ?>
												</a>
											</div>
											<?php endwhile; ?>
										<?php endif; ?>
									
										</div>
									</div>
								</li>						

						
						
						<?php else: // 1URLにのみ飛ぶ?>
						
							<li class="link">
								<div>
								
						
								<?php
								// pickup
								 if( have_rows("top_pickup_urls") ): ?>
									<?php
									while ( have_rows("top_pickup_urls") ) : the_row();
										$top_pickup_repeater_url = get_sub_field('top_pickup_repeater_url');
									?>	
									<a href="<?php echo $top_pickup_repeater_url; ?>">
										
									<?php endwhile; ?>
									<?php 
										$image = get_sub_field('top_pickup_sp_image');
										if( !empty($image) ): ?>
										<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
										<?php endif; ?>
									</a>
								<?php endif; ?>
	
								</div>
							</li>	
						


					<?php endif; ?>
				
				  
				   

				  <?php endwhile; ?>
			 </ul>
		<?php endif; ?>
		
		
	</section>
	
	
	<section id="top_information">
		<?php
		// information
		 if( have_rows("top_information") ): ?>
			 <h3>information</h3>
				<?php while ( have_rows("top_information") ) : the_row();?>
					
						<?php if( get_sub_field('top_information_link_to_multi') ): //複数飛び先がある場合
						$multi_link_id = get_sub_field('top_information_multi_link_id');
						?>
						<div class="inline link" data-role="#<?php echo $multi_link_id; ?>">
							<a href="#link">
							
							<?php 
							$image = get_sub_field('top_information_image');
							if( !empty($image) ): ?>
								<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
							<?php endif; ?>
							</a>

						</div>
						<div style='display:none'>
							<div id='<?php echo $multi_link_id; ?>' style='padding:10px; background:#fff;'>
							<p><strong>院名をお選び下さい</strong></p>
						<?php
						 if( have_rows("top_information_link_repater") ): ?>
								<?php while ( have_rows("top_information_link_repater") ) : the_row();?>
									<div>
										<a href="<?php the_sub_field('top_information_multi_link_url'); ?>">
										<?php the_sub_field('top_information_multi_link_title'); ?>
										</a>
									</div>
								<?php endwhile; ?>
						<?php endif; ?>
						
							</div>
						</div>

						<?php else: //複数飛び先がない場合
						?>
	
							<?php 
							$image = get_sub_field('top_information_image');
							if( !empty($image) ): ?>
								<div >
									<a href="<?php the_sub_field('top_information_link_single'); ?>">
										<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
									</a>
								</div>
							<?php endif; ?>
						<?php endif; ?>
					 
				<?php endwhile; ?>
			 
		<?php endif; ?>
	</section>
	
	
	
	<section id="top_mainmenu">
		<?php
		// NEWS
		 if( have_rows("top_mainmenu") ): ?>
			 <h3>主な診療科目</h3>
			 <ul>
				<?php while ( have_rows("top_mainmenu") ) : the_row(); ?>
						<?php if( get_sub_field('top_mainmenu_this_site_or_not') ): ?>
							<?php $term = get_sub_field('top_mainmenu_select');
							if( $term ): ?>
							<li class="link">
							<div>
								<a href="<?php echo get_term_link( $term ); ?>">
									<?php 
									$image = get_sub_field('top_mainmenu_image');
									if( !empty($image) ): ?>
										<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
									<?php endif; ?>
								</a>
							</div>
							<div>
								<h4><?php echo $term->name; ?></h4>
								<p><?php the_sub_field('top_mainmenu_txt'); ?></p>
							</div>
							</li>
							<?php endif; ?>
							
						<?php else: ?>
						
							<?php $term = get_sub_field('top_mainmenu_select');
							if( $term ): ?>
							
													
								<?php
								$link_to_single = get_field('com_category_to_other_site', $term);
								$link_to_mutli_site = get_field('com_category_to_other_mutli_site', $term);
										 ?>
										 
								<?php /* 飛び先が他サイトの場合 */
								if($link_to_single){ ?>

									<?php /* 飛び先が他サイトので複数ある場合 */
									if($link_to_mutli_site){ ?>

										<li class="blank inline" data-role="#<?php echo esc_html($term->slug)?>">
											<div>
												<a href="#link">
													<?php 
													$image = get_sub_field('top_mainmenu_image');
													if( !empty($image) ): ?>
														<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
													<?php endif; ?>
												</a>
											</div>
											<div>
												<h4><?php echo $term->name; ?><img class="btn_target" src="<?php echo $upload_dir['baseurl']; ?>/img/btn_target.gif" alt="外部リンクへ飛びます"/></h4>
												<p><?php the_sub_field('top_mainmenu_txt'); ?></p>
											</div>
												

										<!-- This contains the hidden content for inline calls -->
										
										<div style='display:none'>
											<div id='<?php echo esc_html($term->slug)?>' style='padding:10px; background:#fff;'>
											<p><strong>院名をお選び下さい</strong><br>※外部リンクに飛びます</p>
										<?php
										 if( have_rows("com_category_url_repeater",$term) ): ?>

												<?php while ( have_rows("com_category_url_repeater",$term) ) : the_row();?>
													
													<?php $com_category_clinic = get_sub_field('com_category_clinic');
													if( $com_category_clinic ): ?>
													<div>
														<a href="<?php the_sub_field('com_category_url'); ?>">
														<?php echo $com_category_clinic->name; ?>
														</a>
													</div>
													<?php endif; ?>

													
												<?php endwhile; ?>
											 
										<?php endif; ?>
											</div>
										</div>
									</li>
									<?php 
									// 飛び先が1個の場合
									} else {
									?>
									
									<li class="blank">
									<div>
										<a href="<?php if($link_to_single){ the_field('com_category_to_other_site_url', $term);}else{echo $term_link;}?>">
														<span>
														<?php
													// explanation image
													 if( get_field('com_category_image',$term) ): ?>
														 <?php 
														$image = get_field('com_category_image',$term);
														if( !empty($image) ): 
															$url = $image['url'];
															$title = $image['title'];
															$alt = $image['alt'];
															$caption = $image['caption'];
															$size = 'full';
														 
															?>
															<img src="<?php echo $url; ?>" alt="<?php echo $alt; ?>"/>
														<?php endif; ?>
												<?php else: ?>
													<img src="<?php echo $upload_dir['baseurl']; ?>/img/cate_noimage.jpg" alt="<?php echo $alt; ?>"/>
												<?php endif; ?></span>
													</a>
									</div>
									<div>
										<h4><?php echo $term->name; ?><img class="btn_target" src="<?php echo $upload_dir['baseurl']; ?>/img/btn_target.gif" alt="外部リンクへ飛びます"/></h4>
										<p><?php the_sub_field('top_mainmenu_txt'); ?></p>
									</div>
											
									</li>
	
									<?php }?>
								<?php } ?>
							
							<?php endif; ?>

						
						<?php endif; ?>
					  
					
				<?php endwhile; ?>
			 </ul>
		<?php endif; ?>
	</section>
	
	
   <section id="top_media" class="media">
		<h3>メディア情報</h3>
        <?php
			$the_query = new WP_Query('post_type=media&posts_per_page=3');
			while ( $the_query->have_posts() ) : $the_query->the_post();
				$my_ID = get_the_ID();
				$my_terms = get_the_terms($my_ID, 'cat-media');
				foreach($my_terms as $my_term){
					$my_term_name = $my_term -> name;
					$my_term_slug = $my_term -> slug;
				}
		?>
        	<section>
<h2><?php the_title(); ?></h2>
<div>
<img src="<?php the_field('img'); ?>" alt="">
</div>
<div>
<p class="media_date_cate"><?php the_time('Y.m.d'); ?><span class="<?php echo $my_term_slug; ?> media_cat"><?php echo $my_term_name; ?></span></p>
 <p><?php the_field('summary'); ?></p>
 <?php if(get_field('yn') == 'no'): ?>
                        	<div class="taR"><a href="<?php the_field('url'); ?>"<?php if(get_field('target') == 't2') echo ' target="_blank"'; ?> class="btn_type01">詳しくはこちら</a></div>
						<?php endif; ?>
</div>
</section>
		<?php
			endwhile;
			wp_reset_postdata();
		?>
		<div class="media_all after_right_arrow"><a href="<?php echo get_post_type_archive_link('media'); ?>">メディア情報一覧を見る</a></div>
    </section>
	


	
	<div class="entry-content">
		<?php
		the_content();

		wp_link_pages( array(
			'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentysixteen' ) . '</span>',
			'after'       => '</div>',
			'link_before' => '<span>',
			'link_after'  => '</span>',
			'pagelink'    => '<span class="screen-reader-text">' . __( 'Page', 'twentysixteen' ) . ' </span>%',
			'separator'   => '<span class="screen-reader-text">, </span>',
		) );
		?>
	</div><!-- .entry-content -->

	<?php
		edit_post_link(
			sprintf(
				/* translators: %s: Name of current post */
				__( 'Edit<span class="screen-reader-text"> "%s"</span>', 'twentysixteen' ),
				get_the_title()
			),
			'<footer class="entry-footer"><span class="edit-link">',
			'</span></footer><!-- .entry-footer -->'
		);
	?>

</article><!-- #post-## -->
