<?php
/**
 * The template part for displaying single posts
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
 $upload_dir = wp_upload_dir(); 
 
 // 性別毎回判定を書くの面倒だったので関数をここで書いておきます
function pntgendername() {
	$gender = get_field('questionnaire_gender');
	if ($gender == 'female'){echo '女性';} elseif ($gender == 'male'){echo '男性';}else{echo 'その他'; }
}

function pntgenderslug() {
	$gender = get_field('questionnaire_gender');
	if ($gender == 'female'){echo 'female';} elseif ($gender == 'male'){echo 'male';}else{echo 'else'; }
}


?>

<article id="post-<?php the_ID(); ?>" <?php post_class("menu_page type-page"); ?>>
	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		<?php
		// subtitle
		 if( get_field("menu_subtitle") ): ?>
					   <p><?php the_field('menu_subtitle');?></p>
			
		<?php endif; ?>
	</header><!-- .entry-header -->

	<?php twentysixteen_excerpt(); /* 抜粋 */?>

	<?php twentysixteen_post_thumbnail();　/* サムネイル */ ?>

	<?php
	// menu_solutions
	if( have_rows("menu_solutions") ): ?>
		 <section id="menu_solutions">
			 <h3>こんなお悩みをお持ちの方へ</h3>
			 <ul>
				  <?php
				  while ( have_rows("menu_solutions") ) : the_row();
				  ?>
				  <li>
					   <?php the_sub_field('menu_solution');?>
				  </li>
				  <?php endwhile; ?>
			 </ul>
		</section>
	<?php endif; ?>

	
	<?php if($post->post_content=="") : ?>
		<!-- ここに記事本文が空だった場合に表示したいソースを記述  -->
	<?php else : ?>
		<section class="entry-content">
		<h3>診療内容</h3>
		<?php
			the_content();

			wp_link_pages( array(
				'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentysixteen' ) . '</span>',
				'after'       => '</div>',
				'link_before' => '<span>',
				'link_after'  => '</span>',
				'pagelink'    => '<span class="screen-reader-text">' . __( 'Page', 'twentysixteen' ) . ' </span>%',
				'separator'   => '<span class="screen-reader-text">, </span>',
			) );

			if ( '' !== get_the_author_meta( 'description' ) ) {
				get_template_part( 'template-parts/biography' );
			}
		?>
		</section><!-- .entry-content -->

	<?php endif; ?>
	
	
	
	
	<!-- Start Repeater -->
	<?php if( have_rows('menu_case_photos')): // check for repeater fields ?>

	<section id="menu_case_photos">

		<?php while ( have_rows('menu_case_photos')) : the_row(); // loop through the repeater fields ?>

		<?php // set up post object
			$post_object = get_sub_field('menu_case_photo');
			if( $post_object ) :
			$post = $post_object;
			setup_postdata($post);
			?>

			<?php
			 if( have_rows("case_pt_photos") ): ?>
				 <ul>
					  <?php
					  while ( have_rows("case_pt_photos") ) : the_row();
					  ?>
					  <li>
						   <?php 
							$image = get_sub_field('case_pt_photo');
							if( !empty($image) ): 
								$url = $image['url'];
								$title = $image['title'];
								$alt = $image['alt'];
								$caption = $image['caption'];
								$size = 'full';
							 ?>
								<img src="<?php echo $url; ?>" alt="<?php echo $alt; ?>"/>
							<?php endif; ?>
					  </li>
					  <?php endwhile; ?>
				 </ul>
			<?php endif; ?>
			<?php // whatever post stuff you want goes here ?>


		<?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>

		<?php endif; ?> 

		<?php endwhile; ?>
		
		

	
		<?php 
		$menu_opnames = get_field('menu_select_opname');
		if( $menu_opnames ): ?>

			<ul class="menu_select_opname">
			<?php foreach( $menu_opnames as $menu_opname ): ?>
				<li>
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>case_photo/?ope_name=<?php echo($menu_opname->slug); ?>"><?php echo $menu_opname->name; ?>の症例をもっと見る</a>
				</li>
			<?php endforeach; ?>
			</ul>

		<?php endif; ?>
		

	</section>
	<!-- End Repeater -->
	<?php endif; ?>
	
	
	

	
	
	
	
	
		
	<!-- Start Repeater -->
	<?php if( have_rows('menu_questionnaires')): // check for repeater fields ?>

	<section id="menu_questionnaires">
		<dl>
			<?php while ( have_rows('menu_questionnaires')) : the_row(); // loop through the repeater fields ?>

			<?php // set up post object
				$post_object = get_sub_field('menu_questionnaire');
				if( $post_object ) :
				$post = $post_object;
				setup_postdata($post);
				$age = get_field('questionnaire_age');
				$job = get_field('questionnaire_job');
				?>
				<dt class="<?php pntgenderslug() ;?>">
					<p class="arrow_box"><?php the_title(); ?>（<?php echo $job; ?>/<?php echo $age; ?>歳/<?php pntgendername(); ?>）</p>
				</dt>
				<dd>
					
				   <?php 
					$questionnaire_txt = get_field('questionnaire_txt');
					if( !empty($questionnaire_txt) ): 

					 ?>
						<?php echo $questionnaire_txt; ?>
					<?php endif; ?>
				</dd>
				<?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
			<?php endif; ?> 
			<?php endwhile; ?>
			
		</dl>

	
		<?php 
		$menu_opnames = get_field('menu_select_opname');
		if( $menu_opnames ): ?>

			<ul class="menu_select_opname">
			<?php foreach( $menu_opnames as $menu_opname ): ?>
				<li>
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>questionnaire/?ope_name=<?php echo($menu_opname->slug); ?>"><?php echo $menu_opname->name; ?>の体験談をもっと見る</a>
				</li>
			<?php endforeach; ?>
			</ul>

		<?php endif; ?>
		

	</section>
	<!-- End Repeater -->
	<?php endif; ?>
	

	

	
	<?php
	// explanation_txt
	 if( get_field("menu_explanation_txt") ): ?>
		<section id="menu_explanation">
			<h4>施術解説</h4>
			<?php
			// explanation image
			 if( get_field("menu_explanation_image") ): ?>
				 <?php 
				$image = get_field('menu_explanation_image');
				if( !empty($image) ): 
					$url = $image['url'];
					$title = $image['title'];
					$alt = $image['alt'];
					$caption = $image['caption'];
					$size = 'full';
				 
					 ?>
					 	<div class="menu_explanation_txt">
							<?php the_field('menu_explanation_txt');?>
						</div>
						<div class="menu_explanation_image">
					
						<img src="<?php echo $url; ?>" alt="<?php echo $alt; ?>"/>
					
						</div>
				<?php endif; ?>
				
			<?php else: ?>
				<div class="menu_explanation_txt" style="width:100%;">
					<?php the_field('menu_explanation_txt');?>
				</div>
			<?php endif; ?>
			
			<?php
			// explanation image
			 if( get_field("menu_explanation_more_image") ): ?>
				 <?php 
				$more_images = get_field('menu_explanation_more_image');
				$more_image = apply_filters('the_content', $more_images);
				if( !empty($more_image) ): 
					 ?>
						<div class="menu_explanation_more_image">
					
						<?php echo $more_image; ?>
					
						</div>
				<?php endif; ?>
			<?php endif; ?>

		</section>
	<?php endif; ?>
	
	<?php if(have_rows('menu_price')): ?>	
		<section class="price_box">
		<h4>料金</h4>
			<?php while(have_rows('menu_price')): the_row(); ?>
				<div class="price_inbox">
					<div class="price_name"><p><?php the_sub_field('menu_name'); ?></p></div>
					<div class="price_amount">
					<?php if(have_rows('menu_amountbox')): ?>
						<?php while(have_rows('menu_amountbox')): the_row();
							$menupes = get_sub_field('menu_amount');
							$menupew = get_sub_field('menu_price');
							$menufrom = get_sub_field('menu_price_from');
							$menu_before_price = get_sub_field('menu_price_before_price');
							$menupow = number_format($menupew);
						?>
						<ul>
						<?php if( $menupes ): ?>
							<li><?php echo $menupes; ?></li>
							<li><?php echo $menu_before_price; ?><?php echo $menupow; ?>円<?php if ($menufrom == 'yes'){echo '～';}?></li>
							<?php else: ?>
							<li class="one"><?php echo $menu_before_price; ?><?php echo $menupow; ?>円<?php if ($menufrom == 'yes'){echo '～';}?></li>
						<?php endif; ?>
						</ul>
						<?php endwhile; ?>
					<?php endif; ?>
					</div>
				</div>
			<?php endwhile; ?>
			
		</section>
		
		<div class="menu_isit_limited">
			<?php if( get_field('menu_isit_limited') ) { ?>
			<?php 
			$terms = get_field('menu_what_clinics');
			if( $terms ): ?>
			※
				<?php foreach( $terms as $term ): ?>
					<?php echo $term->name;
					if($term !== end($terms)){
						echo "・";}
					?>
				<?php endforeach; ?>
			限定の施術です。
			<?php endif; ?>
		<?php } ?>
		<?php if( get_field('menu_price_info') ) { ?>
		<div>
			<?php the_field('menu_price_info');?>
		</div>	
		<?php } ?>
		
		</div>	
		
		
	<?php endif; ?>

		

	
	<?php
	// menu_faq
	if( have_rows("menu_faq") ): ?>
		<section id="menu_faq">
			<h3><img src="<?php echo $upload_dir['baseurl']; ?>/img/icn-qa-title.gif" alt="よくあるご質問"><span><?php the_title(); ?></span></h3>
			<dl>
				<?php
				while ( have_rows("menu_faq") ) : the_row();
				?>
					<dt>
					<?php the_sub_field('menu_questions');?>
					</dt>
					<dd>
					<?php the_sub_field('menu_answer');?>
					</dd>
				<?php endwhile; ?>
			</dl>
		</section>
	<?php endif; ?>


	<footer class="entry-footer">
		<?php twentysixteen_entry_meta(); ?>
		<?php
			edit_post_link(
				sprintf(
					/* translators: %s: Name of current post */
					__( 'Edit<span class="screen-reader-text"> "%s"</span>', 'twentysixteen' ),
					get_the_title()
				),
				'<span class="edit-link">',
				'</span>'
			);
		?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
