<?php
/**
 * The template part for displaying single posts
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
 $upload_dir = wp_upload_dir(); 
?>
<article id="post-<?php the_ID(); ?>" <?php post_class("flow_page type-page"); ?>>
	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		<?php
		// subtitle
		 if( get_field("flow_subtitle") ): ?>
					   <p><?php the_field('flow_subtitle');?></p>
		
		<?php endif; ?>
	</header><!-- .entry-header -->
	
	
	  <?php if($post->post_content=="") : ?>

	  <!-- ここに記事本文が空だった場合に表示したいソースを記述  -->

	  <?php else : ?>
		<section class="entry-content">
			<?php
				the_content();

				wp_link_pages( array(
					'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentysixteen' ) . '</span>',
					'after'       => '</div>',
					'link_before' => '<span>',
					'link_after'  => '</span>',
					'pagelink'    => '<span class="screen-reader-text">' . __( 'Page', 'twentysixteen' ) . ' </span>%',
					'separator'   => '<span class="screen-reader-text">, </span>',
				) );

				if ( '' !== get_the_author_meta( 'description' ) ) {
					get_template_part( 'template-parts/biography' );
				}
			?>
		</section><!-- .entry-content -->

	  <?php endif; ?>


	
	
	
	<?php if(have_rows('flow_steps')): ?>
		
		<?php $i=1; while(have_rows('flow_steps')): the_row(); ?>
		<section class="flow_steps">
			<?php
			// flow_step_title
			 if( get_sub_field("flow_step_title") ): ?>
						   <h2>Step<?php echo $i; ?>&nbsp;<?php the_sub_field('flow_step_title'); ?></h2>
			<?php endif; ?>
			<div>
					<?php
					// flow_step_title
					 if( get_sub_field("flow_step_txt") ): ?>
								  <?php the_sub_field('flow_step_txt'); ?>
					<?php endif; ?>
									<?php
					// flow_step_title
					 if( get_sub_field("flow_attention") ): ?>
								  <div class="flow_attention">
									<h3><img src="<?php echo $upload_dir['baseurl']; ?>/img/icon01.gif">注意点</h3>
									<p><?php the_sub_field('flow_attention'); ?></p> 
								  </div>
					<?php endif; ?>

			</div>
			<?php 
			$image = get_sub_field('flow_image');
			if( !empty($image) ): 
				// vars
				$alt = $image['alt'];
				$caption = $image['caption'];
				// thumbnail
				$size = 'thumbnail';
				$thumb = $image['sizes'][ $size ];
				$width = $image['sizes'][ $size . '-width' ];
				$height = $image['sizes'][ $size . '-height' ];
			 
			?>
		
			<img src="<?php echo $thumb; ?>" alt="<?php echo $alt; ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>" />


			<?php endif; ?>
			</section>
		<?php $i++; endwhile; ?>
	
	<?php endif; ?>

	





	


	<footer class="entry-footer">
		<?php twentysixteen_entry_meta(); ?>
		<?php
			edit_post_link(
				sprintf(
					/* translators: %s: Name of current post */
					__( 'Edit<span class="screen-reader-text"> "%s"</span>', 'twentysixteen' ),
					get_the_title()
				),
				'<span class="edit-link">',
				'</span>'
			);
		?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
