<?php
/**
 * The template part for displaying single posts
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
 $upload_dir = wp_upload_dir(); 
?>

<article id="post-<?php the_ID(); ?>" <?php post_class("type-page"); ?>>
	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
	</header><!-- .entry-header -->

	<section class="entry-content">
		<?php
			the_content();

			wp_link_pages( array(
				'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentysixteen' ) . '</span>',
				'after'       => '</div>',
				'link_before' => '<span>',
				'link_after'  => '</span>',
				'pagelink'    => '<span class="screen-reader-text">' . __( 'Page', 'twentysixteen' ) . ' </span>%',
				'separator'   => '<span class="screen-reader-text">, </span>',
			) );

			if ( '' !== get_the_author_meta( 'description' ) ) {
				get_template_part( 'template-parts/biography' );
			}
		?>
	</section><!-- .entry-content -->
	

	
	<ul class="about_cliniclist" id="about-nav">
		<?php $args = array(
			'numberposts' => -1,                //表示（取得）する記事の数
			'post_type' => 'clinic'    //投稿タイプの指定
		);
		$customPosts = get_posts($args);
		$customPostsNum = count($customPosts);
		
		if($customPostsNum > 1) : foreach($customPosts as $post) : setup_postdata( $post ); ?>

			<li>
				<a href="#<?php  $terms = get_the_terms($post->ID,'com_category');  foreach( $terms as $term ) { ?><?php echo($term->slug); ?><?php }?>">
				<?php the_title(); ?>のアクセスマップはこちら
				</a>
			</li>

			
		<?php endforeach; ?>
		<?php else : //記事が無い場合 ?>
		<?php endif;
		wp_reset_postdata(); //クエリのリセット ?>
	</ul>


	
	
	
	
	<div id="aboutcontents">
    <?php $args = array(
        'numberposts' => -1,                //表示（取得）する記事の数
        'post_type' => 'clinic'    //投稿タイプの指定
    );
    $customPosts = get_posts($args);
    if($customPosts) : foreach($customPosts as $post) : setup_postdata( $post ); ?>
		<section id="<?php  $terms = get_the_terms($post->ID,'com_category');  foreach( $terms as $term ) { ?><?php echo($term->slug); ?><?php }?>">
			<h3><?php the_title(); ?></h3>
			
			<div class="access_inbox">

				<?php if(have_rows('clinic_about')): ?>
					<?php while(have_rows('clinic_about')): the_row();
						$clinicphoto = get_sub_field('about_clinicphoto');
						$googlemap = get_sub_field('about_googlemap');
						$postalcode = get_sub_field('about_postalcode');
						$address = get_sub_field('about_address');
						$traffics = get_sub_field('about_traffics');
						$tel = get_sub_field('about_tel');
						$open = get_sub_field('about_open');
						$close = get_sub_field('about_close');
						$time = get_sub_field('about_time');
					?>
					
					<?php if( !empty($address) ):?>	
					<h4 class="">現住所</h4>
						<p>〒<?php echo $postalcode; ?>&nbsp;<?php echo $address; ?></p>
					<?php endif;?>
					
					<?php if( !empty($traffics) ):?>	
					<h4 class="">交通機関</h4>
					
						<ul class="access_transport">
							<?php while(have_rows('about_traffics')): the_row();
								$traffic = get_sub_field('about_traffic');
							?>
								<li><?php echo $traffic; ?></li>
							<?php endwhile; ?>
						</ul>
					<?php endif;?>

					<?php if( !empty($googlemap) ):?>	
						<h4 class="">マップ</h4>
						<iframe src="<?php echo $googlemap; ?>" frameborder="0" style="border:0" allowfullscreen></iframe>
					<?php endif;?>

					<?php endwhile; ?>
				<?php endif; ?>

			</div>
		</section>
	
		
    <?php endforeach; ?>
    <?php else : //記事が無い場合 ?>
       <p>登録がありません。</p>
    <?php endif;
    wp_reset_postdata(); //クエリのリセット ?>


	
	</div>
	
	
	
	
	

	<footer class="entry-footer">
		<?php twentysixteen_entry_meta(); ?>
		<?php
			edit_post_link(
				sprintf(
					/* translators: %s: Name of current post */
					__( 'Edit<span class="screen-reader-text"> "%s"</span>', 'twentysixteen' ),
					get_the_title()
				),
				'<span class="edit-link">',
				'</span>'
			);
		?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
