<?php
/**
 * The template part for displaying single posts
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
 $upload_dir = wp_upload_dir(); 
?>	
<article id="post-<?php the_ID(); ?>" <?php post_class("media type-page"); ?>>
	<header class="entry-header">
		<h1 class="entry-title"><?php echo get_the_title(2077); ?></h1>
					   <p><?php the_field('title_sub', 2077); ?></p>
	</header><!-- .entry-header -->
	

			<?php if(have_posts()): ?>
				<?php
                	while(have_posts()): the_post();
					$my_ID = get_the_ID();
					$my_terms = get_the_terms($my_ID, 'cat-media');
					foreach($my_terms as $my_term){
						$my_term_name = $my_term -> name;
						$my_term_slug = $my_term -> slug;
					}
				?>
                <section>
				 <h2><?php the_title(); ?></h2>
                	<div><img src="<?php the_field('img'); ?>" alt=""></div>
                    <div>
                    	<p class="media_date_cate"><?php the_time('Y.m.d'); ?><span class="<?php echo $my_term_slug; ?> media_cat"><?php echo $my_term_name; ?></span></p>
                       
                        <p><?php the_field('summary'); ?></p>
                        <?php if(get_field('yn') == 'no'): ?>
                        	<div class="taR"><a href="<?php the_field('url'); ?>"<?php if(get_field('target') == 't2') echo ' target="_blank"'; ?> class="btn_type01">詳しくはこちら</a></div>
						<?php endif; ?>
                    </div>
                 </section>
                <?php endwhile; ?>
            <?php endif; ?>
           <!-- /.mrl30 -->

        	
<?php
//Pagenation 
if (function_exists("pagination")) {
	pagination($additional_loop->max_num_pages);
}
?><!-- /.pagination -->
      
      
        
        

        
    </article>

