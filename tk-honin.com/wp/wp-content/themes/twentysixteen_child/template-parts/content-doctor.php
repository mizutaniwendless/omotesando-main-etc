<?php
/**
 * The template part for displaying single posts
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
 $upload_dir = wp_upload_dir(); 
?>

<article id="post-<?php the_ID(); ?>" <?php post_class("doctor_page type-page"); ?>>
	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
	</header><!-- .entry-header -->

	<section class="entry-content">
		<?php
			the_content();

			wp_link_pages( array(
				'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentysixteen' ) . '</span>',
				'after'       => '</div>',
				'link_before' => '<span>',
				'link_after'  => '</span>',
				'pagelink'    => '<span class="screen-reader-text">' . __( 'Page', 'twentysixteen' ) . ' </span>%',
				'separator'   => '<span class="screen-reader-text">, </span>',
			) );

			if ( '' !== get_the_author_meta( 'description' ) ) {
				get_template_part( 'template-parts/biography' );
			}
		?>
	</section><!-- .entry-content -->


	<?php if(have_rows('doctor_prof')): ?>
		<?php while(have_rows('doctor_prof')): the_row();?>
		<section class="drprof_box">
		<h2 class="switch">
		<?php if( $doctor_position ): ?>
			<?php the_sub_field('doctor_name'); ?>
		<?php else: ?>
			<?php the_sub_field('doctor_position'); ?>　<?php the_sub_field('doctor_name'); ?>
		<?php endif; ?>　医師
		</h2>
		<div class="drprof_inbox">
			<div class="drphoto">
			<?php 
				$image = get_sub_field('doctor_photo');
				if( !empty($image) ): 
					// vars
					$alt = $image['alt'];
					$url =$image['url'];
					$caption = $image['caption'];
					// thumbnail
					$size = 'full';
					$thumb = $image['sizes'][ $size ];
					$width = $image['sizes'][ $size . '-width' ];
					$height = $image['sizes'][ $size . '-height' ];
				if( $caption ): ?>
				<?php endif; ?>
			<img src="<?php echo $url; ?>" alt="<?php echo $alt; ?>" />
			<?php endif; ?>
			</div>
		
			<div class="drtable">
			<div class="">
				<p>経歴</p>
				<table class="cdetails">
					<tbody>
					<?php if(have_rows('doctor_career')): ?>
						<?php while(have_rows('doctor_career')): the_row();
							$doctordate = get_sub_field('doctor_date');
							$doctoreducational = get_sub_field('doctor_educational');
						?>
					<tr>
						<?php if( $doctordate ): ?>
							<th><?php echo $doctordate; ?>年</th>
						<?php endif; ?>
						<td><?php echo $doctoreducational; ?></td>
					</tr>
						<?php endwhile; ?>
					<?php endif; ?>
					</tbody>
				</table>
			</div>
			
			
			<?php if(have_rows('doctor_society')): ?>
			<div class="">
				<p>所属学会など</p>
				<table class="cdetails">
					<tbody>
				<?php while(have_rows('doctor_society')): the_row();
						$doctorsocietyname = get_sub_field('doctor_societyname');
				?>
					<tr>
						<td><?php echo $doctorsocietyname; ?></td>
					</tr>
				<?php endwhile; ?>
					</tbody>
				</table>
			</div>
			<?php endif; ?>

			
			<?php if(have_rows('doctor_title')): ?>
			<div class="">
				<p>称号・資格</p>
				<table class="cdetails">
					<tbody>
					<?php while(have_rows('doctor_title')): the_row();
						$doctortitlename = get_sub_field('doctor_titlename');
					?>
					<tr>
						<td><?php echo $doctortitlename; ?></td>
					</tr>
				<?php endwhile; ?>
					</tbody>
				</table>
			</div>
			<?php endif; ?>
			</div>
			
			<?php if (get_sub_field('doctor_voice')): ?>
				<div class="doctor_voice"><p>【ドクターから一言】<br><?php the_sub_field('doctor_voice'); ?></p></div>
			<?php endif; ?>
			
		</div>
		
</section>
<hr>
		<?php endwhile; ?>
	<?php endif; ?>

	

	<footer class="entry-footer">
		<?php twentysixteen_entry_meta(); ?>
		<?php
			edit_post_link(
				sprintf(
					/* translators: %s: Name of current post */
					__( 'Edit<span class="screen-reader-text"> "%s"</span>', 'twentysixteen' ),
					get_the_title()
				),
				'<span class="edit-link">',
				'</span>'
			);
		?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
