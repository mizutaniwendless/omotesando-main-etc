<?php
/**
 * The template part for displaying single posts
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
 $upload_dir = wp_upload_dir(); 
?>

<article id="post-<?php the_ID(); ?>" <?php post_class("archive_menu type-page"); ?>>
			<header class="entry-header">
				<?php
					the_archive_title( '<h1 class="page-title">', '</h1>' );
					the_archive_description( '<div class="taxonomy-description">', '</div>' );
				?>
			</header><!-- .page-header -->
<?php // WordPress のタクソノミーの親・子のターム一覧とタームに紐づく記事一覧の出力
	$categories = get_terms('menu_category','parent=0');
	foreach ( $categories as $cat ) {
		?>
		<section class="archive_menu_parent">
			<h2><?php echo esc_html($cat->name)?></h2>
			<div class="archive_menu_children">
		<?php
		$children = get_terms('menu_category','hierarchical=0&parent='.$cat->term_id);
		
		if($children){ // 子カテゴリの有無
		
			foreach ( $children as $child ) {
			 $term_link = get_term_link( $child );
			 $link_to_single = get_field('com_category_to_other_site', $child);
			 $link_to_mutli_site = get_field('com_category_to_other_mutli_site', $child);
			 
				 ?>
				 
				 
		
		<?php /* 飛び先が他サイトの場合 */
		if($link_to_single){ ?>


		
			<?php /* 飛び先が他サイトので複数ある場合 */
			if($link_to_mutli_site){ ?>

				<div class="inline blank" data-role="#<?php echo esc_html($child->slug)?>">
						<h3>
							<a href="#link">
								<span><?php echo esc_html($child->name)?></span><img class="btn_target" src="<?php echo $upload_dir['baseurl']; ?>/img/btn_target.gif" alt="外部リンクへ飛びます"/>
							</a>
						</h3>
						<?php
							// explanation image
							 if( get_field('com_category_image',$child) ): ?>
								 <?php 
								$image = get_field('com_category_image',$child);
								if( !empty($image) ): 
									$url = $image['url'];
									$title = $image['title'];
									$alt = $image['alt'];
									$caption = $image['caption'];
									$size = 'full';
								 
									?>
									<img src="<?php echo $url; ?>" alt="<?php echo $alt; ?>"/>
								<?php endif; ?>
						<?php else: ?>
							<img src="<?php echo $upload_dir['baseurl']; ?>/img/cate_noimage.jpg" alt="noimage"/>
						<?php endif; ?>
									
						<p>
						<?php echo esc_html($child->description)?>
						</p>
				</div>
				
				<!-- This contains the hidden content for inline calls -->
				
				<div style='display:none'>
					<div id='<?php echo esc_html($child->slug)?>' style='padding:10px; background:#fff;'>
					<p><strong>院名をお選び下さい</strong><br>※外部リンクに飛びます</p>
				<?php
				 if( have_rows("com_category_url_repeater",$child) ): ?>

						<?php while ( have_rows("com_category_url_repeater",$child) ) : the_row();?>
							
							<?php $term = get_sub_field('com_category_clinic');
							if( $term ): ?>
							<div>
								<a href="<?php the_sub_field('com_category_url'); ?>">
								<?php echo $term->name; ?>
								</a>
							</div>
							<?php endif; ?>

							
						<?php endwhile; ?>
					 
				<?php endif; ?>
					</div>
				</div>
			<?php 
			// 飛び先が1個の場合
			} else {
			?>
	
				<div class="blank">
						<h3>
							 <a href="<?php if($link_to_single){ the_field('com_category_to_other_site_url', $child);}else{echo $term_link;}?>">
								<span><?php echo esc_html($child->name)?><img src="<?php echo $upload_dir['baseurl']; ?>/img/btn_target.gif" alt="外部リンクへ飛びます"/></span>
							</a>
						</h3>
						<?php
							// explanation image
							 if( get_field('com_category_image',$child) ): ?>
								 <?php 
								$image = get_field('com_category_image',$child);
								if( !empty($image) ): 
									$url = $image['url'];
									$title = $image['title'];
									$alt = $image['alt'];
									$caption = $image['caption'];
									$size = 'full';
								 
									?>
									<img src="<?php echo $url; ?>" alt="<?php echo $alt; ?>"/>
								<?php endif; ?>
						<?php else: ?>
							<img src="<?php echo $upload_dir['baseurl']; ?>/img/cate_noimage.jpg" alt="<?php echo $alt; ?>"/>
						<?php endif; ?>
									
						<p>
						<?php echo esc_html($child->description)?>
						</p>
				</div>
			<?php }?>


			

		<?php } else { ?>
			<div class="link">
					<h3>
						<a href="<?php echo $term_link;?>">
							<span><?php echo esc_html($child->name);?></span>
						</a>
					</h3>
					<?php
						// explanation image
						 if( get_field('com_category_image',$child) ): ?>
							 <?php 
							$image = get_field('com_category_image',$child);
							if( !empty($image) ): 
								$url = $image['url'];
								$title = $image['title'];
								$alt = $image['alt'];
								$caption = $image['caption'];
								$size = 'full';
							 
								?>
								<img src="<?php echo $url; ?>" alt="<?php echo $alt; ?>"/>
							<?php endif; ?>
					<?php else: ?>
						<img src="<?php echo $upload_dir['baseurl']; ?>/img/cate_noimage.jpg" alt="<?php echo $alt; ?>"/>
					<?php endif; ?>
								
					<p>
					<?php echo esc_html($child->description)?>
					</p>
			</div>

		<?php } ?>

				 
				 
				 
				 
				 
				 
				 
				 
				 



<?php wp_reset_postdata(); ?>
<?php } //子カテゴリに紐づく記事一覧の出力終了 ?>
	</div>
</section>
<?php 
    } else { // 子カテゴリがなければ親カテゴリに紐づく記事一覧を出力
      $catslug = $cat->slug;
      $args = array(
        'post_type' => 'menu',
        'menu_category' => $catslug ,
        'posts_per_page' => -1
      );
      $myquery = new WP_Query( $args ); ?>
<?php if ( $myquery->have_posts()): ?>
<ul>
<?php while($myquery->have_posts()): $myquery->the_post(); ?>
  <li><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></li>
<?php endwhile; ?>
</ul>
<?php endif; ?>
<?php wp_reset_postdata(); ?>
<?php } // 子カテゴリの有無 if文終了 ?>
<?php } // 出力の終了 ?>


</article><!-- #post-## -->
