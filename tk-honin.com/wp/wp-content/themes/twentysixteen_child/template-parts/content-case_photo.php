<?php
/**
 * The template part for displaying single posts
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
 $upload_dir = wp_upload_dir(); 
?>	
<article id="post-<?php the_ID(); ?>" <?php post_class("case_photo type-page"); ?>>
	<header class="entry-header">
		<h1 class="entry-title"><?php echo get_the_title(2080); ?></h1>
		
				<p>
				<?php 
				$url = $_SERVER['REQUEST_URI'];
				if(isset($_GET['ope_name'])){
					 $ope_name = $_GET['ope_name'];
						echo "<strong>".get_term_by('slug',$ope_name,"ope_name")->name."</strong>の症例写真です。" ;
				}else if(isset($_GET['post_type'])){
						echo "<strong>".get_term_by('slug',$term,"menu_category")->name."</strong>の症例写真です。" ;
						if(strstr($url,'page')==false){
							echo "<br>施術名ごとの症例をご覧になりたい方は「診療科目」を選択後「施術名」からお選び下さい。" ;
						}
				}else{
					$url = $_SERVER['REQUEST_URI'];
					if(strstr($url,'page')==false){
						echo "施術名ごとの症例をご覧になりたい方は「診療科目」を選択後、「施術名」からお選び下さい。" ;
					}
				}	   
				 ?>	   
			 </p>
	</header><!-- .entry-header -->
	
	<div class="wrapper">
		<form>
			<div>
				<p>診療科目</p>
				<select class="parent" name="foo">
				<option value="" selected="selected">カテゴリを選択</option>
				<?php
				$taxonomyName = "menu_category";
				//This gets top layer terms only.  This is done by setting parent to 0.  
				$parent_terms = get_terms( $taxonomyName, array( 'parent' => 0, 'orderby' => 'slug', 'hide_empty' => false ) );
				foreach ( $parent_terms as $pterm ) {
					//Get the Child terms
					$taxonomys = get_terms( $taxonomyName, array( 'parent' => $pterm->term_id, 'orderby' => 'slug', 'hide_empty' => false ) );
					foreach ( $taxonomys as $taxonomy ) {
						$url = get_term_link($taxonomy->slug, $taxonomy_name);
						$tax_posts = get_posts(array(
						  'post_type' => 'case_photo',
						  'posts_per_page' => -1,
						  'tax_query' => array(
							array(
							  'taxonomy' => 'menu_category',
							  'terms' => array($taxonomy->slug),
							  'field' =>'slug',
							  'include_children' => true,
							  'operator' => 'IN'
							),
							'relation' =>'AND'
						  )
						));
						if($tax_posts):?>
							<option value="<?php echo($taxonomy->slug); ?>">
							<?php echo($taxonomy->name); ?></option>
						<?php endif;
					}
				}?>
				</select>
			</div>

			<div>
				<p>施術名</p>
				
				<select class="children" name="bar" disabled>
				<option value="" selected="selected">施術名を選択</option>

				<?php
				 $args = array(
				   'orderby' => 'id',
				   'order' =>'ASC'
				  );
				  $taxonomy_name = 'menu_category';
				  $taxonomys = get_terms($taxonomy_name,$args);
				  if(!is_wp_error($taxonomys) && count($taxonomys)):
						foreach($taxonomys as $taxonomy):
						$url = get_term_link($taxonomy->slug, $taxonomy_name);
						$tax_posts = get_posts(array(
						  'post_type' => 'case_photo',
						  'posts_per_page' => -1,
						  'tax_query' => array(
							array(
							  'taxonomy' => 'menu_category',
							  'terms' => array($taxonomy->slug),
							  'field' =>'slug',
							  'include_children' => true,
							  'operator' => 'IN'
							),
							'relation' =>'AND'
						  )
						));
						if($tax_posts):?>

							<?php
							 $ope_args = array(
							   'orderby' => 'id',
							   'order' =>'ASC'
							  );
							  $ope_taxonomy_name = 'ope_name';
							  $ope_taxonomys = get_terms($ope_taxonomy_name,$args);
							  if(!is_wp_error($ope_taxonomys) && count($ope_taxonomys)):
									foreach($ope_taxonomys as $ope_taxonomy):
									$ope_url = get_term_link($ope_taxonomy->slug, $ope_taxonomy_name);
									$ope_tax_posts = get_posts(array(
									  'post_type' => 'case_photo',
									  'posts_per_page' => -1,
										'tax_query' => array (
											'relation' => 'AND',
											array (
										  'taxonomy' => 'menu_category',
										  'terms' => array($taxonomy->slug),
										  'field' =>'slug',
										  'include_children' => true,
										  'operator' => 'IN'
											),
											array (
										  'taxonomy' => 'ope_name',
										  'terms' => array($ope_taxonomy->slug),
										  'field' =>'slug',
										  'include_children' => true,
										  'operator' => 'IN'
											)
										)
									));
									if($ope_tax_posts):?>
										<option value="<?php echo esc_url( home_url( '/' ) ); ?>case_photo/?ope_name=<?php echo($ope_taxonomy->slug); ?>" data-val="<?php echo($taxonomy->slug); ?>"><?php echo($ope_taxonomy->name); ?>（<?php echo count($ope_tax_posts); ?>件）</option>

									<?php endif;
								endforeach;
							endif; ?>

						<?php endif;
					endforeach;
				endif; ?>
				</select>

			</div>

			<div>
			<input id="pt_button" type=button onClick="" value="検索" >
			</div>
		</form>
	</div>
	<!-- /.wrapper -->

	
	
	
	
	
	
<?php
	$url = $_SERVER['REQUEST_URI'];
	if(!isset($_GET['ope_name'])): 
	if(!isset($_GET['post_type'])): 
		if(strstr($url,'page')==false):
?>
	<article class="case_photo_recommends">
	<h2>ピックアップ症例<span><?php the_time('Y/m/d',2080); ?>更新</span></h2>
	<?php
	 if( have_rows("case_photo_recommends", 2080) ): ?>
		 
			  <?php
			  while ( have_rows("case_photo_recommends", 2080) ) : the_row();
			  ?>
			  
				   <?php 
					$post_object = get_sub_field('case_photo_recommend', 2080);
					if( $post_object ): 
						// override $post
						$post = $post_object;
						setup_postdata( $post ); 
						?>
						<section>
							<h3><?php the_title(); ?></h3>
								<?php
								 if( have_rows("case_pt_photos") ): ?>
									 <ul>
										  <?php
										  while ( have_rows("case_pt_photos") ) : the_row();
										  ?>
										  <li>
											   <?php 
												$image = get_sub_field('case_pt_photo');
												if( !empty($image) ): 
													$url = $image['url'];
													$title = $image['title'];
													$alt = $image['alt'];
													$caption = $image['caption'];
													$size = 'full';
												 ?>
													<a href="<?php the_permalink(); ?>"><img src="<?php echo $url; ?>" alt="<?php echo $alt; ?>"/></a>
												<?php endif; ?>
										  </li>
										  <div class="case_pt_detail"><a href="<?php the_permalink(); ?>">詳細を見る >></a></div>
										  <?php endwhile; ?>
									 </ul>
								<?php endif; ?>
						</section>
						<?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
					<?php endif; ?>
			  
			  <?php endwhile; ?>
		 
	<?php endif; ?>
	</article>
 
		<?php endif;endif;endif; ?>
	

	<article>
		<h2>
		
		
				<?php 
				$url = $_SERVER['REQUEST_URI'];
				if(isset($_GET['ope_name'])){
					 $ope_name = $_GET['ope_name'];
						echo get_term_by('slug',$ope_name,"ope_name")->name."の" ;
				};
				if(isset($_GET['post_type'])){
						echo get_term_by('slug',$term,"menu_category")->name."の" ;
				};
				echo "新着症例" ;

				 ?>	 
		
		
		</h2>



			<?php if(have_posts()): ?>
				<?php
                	while(have_posts()): the_post();
					$my_ID = get_the_ID();
					$my_terms = get_the_terms($my_ID, 'menu_category');
					foreach($my_terms as $my_term){
						$my_term_name = $my_term -> name;
						$my_term_slug = $my_term -> slug;
					}
				?>
				
                <section>
				 <h3><?php the_title(); ?></h3>
                	<div>
						<?php
						 if( have_rows("case_pt_photos") ): ?>
							 <ul>
								  <?php
								  while ( have_rows("case_pt_photos") ) : the_row();
								  ?>
								  <li>
									   <?php 
										$image = get_sub_field('case_pt_photo');
										if( !empty($image) ): 
											$url = $image['url'];
											$title = $image['title'];
											$alt = $image['alt'];
											$caption = $image['caption'];
											$size = 'full';
										 ?>
											<a href="<?php the_permalink(); ?>"><img src="<?php echo $url; ?>" alt="<?php echo $alt; ?>"/></a>
										<?php endif; ?>
								  </li>
								  <?php endwhile; ?>
							 </ul>
							
						<?php endif; ?>
						<?php // whatever post stuff you want goes here ?>
						<div class="case_pt_detail"><a href="<?php the_permalink(); ?>">詳細を見る >></a></div>
					</div>
                 </section>
                <?php endwhile; ?>
            <?php endif; ?>
           <!-- /.mrl30 -->

        	
<?php
//Pagenation 
if (function_exists("pagination")) {
	pagination($additional_loop->max_num_pages);
}
?><!-- /.pagination -->
      
        
    </article>  
  
</article>

