
jQuery(function($) {

	$('article .link,footer .link').click(function() {
	location.href = $(this).find('a').attr('href');
	});
	
	
	$('article .blank').click(function() {
    window.open($(this).find('a').attr('href'), '_blank');
	});
	
	
	
	$(document).ready(function(){
		$(".inline").colorbox({inline:true, width:"100%",scrolling: false});
		
	});
	


	$(function() {
		//ページトップへのスクロール
		$('#pagetop').click(function () {
			//id名#pagetopがクリックされたら、以下の処理を実行
			$("html,body").animate({scrollTop:0},"300");
		});
		//ページトップの出現
		$('#pagetop').hide();
		$(window).scroll(function () {
			if($(window).scrollTop() > 0) {
				$('#pagetop').slideDown(800);
			} else {
				$('#pagetop').slideUp(150);
			}
		});
		
		
		
		
		//ホバーイベント
		$("#pagetop").mouseover(function(){
			$(this).animate({
				bottom:"0px"
			},300);
		});
		$("#pagetop").mouseout(function(){
			$(this).animate({
				bottom:"-60px"
			},300);
		});
	});
	
	$('a[href^=#]').click(function() {
		var speed = 800;
		var href= $(this).attr("href");
		var target = $(href == "#" || href == "" ? 'html' : href);
		var position = target.offset().top;
		$('body,html').animate({scrollTop:position}, speed, 'swing');
		return false;
	});

	
	 $('#banner_bxSlider').bxSlider({
		auto: true,
		pager: true,
		controls: false,
		pagerType: 'full',
	  });

	
	
	//case_photoのプルダウン
	var $children = $('.children');
	var original = $children.html();

	$('.parent').change(function() {

	  var val1 = $(this).val();

	  $children.html(original).find('option').each(function() {
		var val2 = $(this).data('val');
		if (val1 != val2) {
		  $(this).not(':first-child').remove();
		}
	  });

	  if ($(this).val() == "") {
		$children.attr('disabled', 'disabled');
	  } else {
		$children.removeAttr('disabled');
	  }
	});

	
  $('#pt_button').click(function() {
	  
	var bar= $('select[name=bar]').val();
	
	if ($('.parent').val() == "") {
		alert("カテゴリをお選びください\n");
		return false;
	} else{
		if ($('.children').val() == "") {
			var foo= $('select[name=foo]').val();
			window.location.href = "http://192.168.11.32/tkhonin/menu_category/"+foo+"/?post_type=case_photo";
		} else{
			window.location.href = $('select[name=bar]').val();
		}
	}
	
	 });
	  

	  
  $('#pt_button_questionnaire').click(function() {
	

	var bar= $('select[name=bar]').val();
	
	if ($('.parent').val() == "") {
		alert("カテゴリをお選びください\n");
		return false;
	} else{
		if ($('.children').val() == "") {
			var foo= $('select[name=foo]').val();
			window.location.href = "http://192.168.11.32/tkhonin/menu_category/"+foo+"/?post_type=questionnaire";
			
		} else{
			window.location.href = $('select[name=bar]').val();
		}
	}


	 });
	   


	   
	   
	  
		
	
/*
	var $pt_button = $('#pt_button');
	$('.children').change(function() {

	  if ($(this).val() == "") {
	   $pt_button.attr('disabled', 'disabled');
	  } else {
	  $pt_button.removeAttr('disabled');
	  }
	  });
		

	
	
	*/
	
	
	
	
	
	
});