jQuery(document).ready(function($) {
	var numItems = $(".media li").length;
	var numDisplay = 15;

	var selectClass = "selection";
	$(".media li").each(function(index, el) {
		var articleIdx = Math.floor(index/numDisplay);
		$(this).addClass("article-" + articleIdx);
		if(articleIdx == 0){
			$(this).addClass(selectClass);
		}
	});

	 $("#pagenation").pagination({
        items: numItems,
        itemsOnPage: numDisplay,
        cssStyle: 'light-theme',
        prevText:'<',
        nextText:">",
        onPageClick: function(currentPageNumber){
            showPage(currentPageNumber);
            $(window).scrollTop(0);
            return false;
        }
    });

	 function showPage(currentPageNumber){
	    var page=".article-" + (currentPageNumber-1);
	    $('.'+selectClass).hide();
	    $(page).show().addClass(selectClass);
	} 

});