<?php
/**
 * 登録フォーム
 * @date 2013-03-15
 */
require_once 'configure.php';

$formfile = 'entry.html';
$confirmfile = 'entry-check.html';
$thanksfile = 'entry-thanks.html';

$formcontent = file_get_contents($formfile);

if (@$_POST) {
	$mode = getMode();

	if ($mode == 'confirm') {
		/*
		 * TODO 終了処理
		 */
		$contents = file_get_contents($thanksfile);
		$htmlfilemtime = time();

	} else {
		$required = array('名前', 'フリガナ', '電話番号1', '電話番号2', '電話番号3', 'メールアドレス');
		$errors = array();
		foreach ($required as $name) {
			if (!@$_POST[$name]) {
				if (strpos($name, '電話番号') !== false) {
					$errors[] = '電話番号';
				} else {
					$errors[] = $name;
				}
			}
		}

		if (@$_POST['メールアドレス'] !== @$_POST['メールアドレス確認']) {
			$errors[] = 'メールアドレス確認';
		}

		$htmlfilemtime = time();
		if ($mode == 'return' || $errors) {
			$contents = $formcontent;
			foreach ($_POST as $k=>$v) {
				$k = preg_quote(htmlspecialchars($k), '/');
				if (preg_match('/<input type="text" name="'.$k.'" .*?value="/iu', $contents, $matches)) {
					$contents = str_replace($matches[0], $matches[0].htmlspecialchars($v), $contents);
				} else if (preg_match_all('/<input type="radio" name="'.$k.'" .*?value="(.*?)" .*?>/iu', $contents, $matches)) {
					for ($i=0; $i<count($matches[0]); $i++) {
						$m = str_replace('checked="checked"', '', $matches[0][$i]);
						if ($matches[1][$i] == $v) {
							$m = preg_replace('/(value=".*?")/', '$1 checked="checked"', $m);
						}
						$contents = str_replace($matches[0][$i], $m, $contents);
					}
				} else if (preg_match('/<textarea name="'.$k.'".*?>/iu', $contents, $matches)) {
					$contents = str_replace($matches[0], $matches[0].htmlspecialchars($v), $contents);
				}
			}
			foreach ($errors as $error) {
				$key = preg_quote(htmlspecialchars($error), '/');
				$base = '/<!-- ((?:p|span) class="hint" title="'.$key.'">.*?<\/(?:p|span)) -->/';
				$contents = preg_replace($base, '<$1>', $contents);
			}
		} else {
			$contents = file_get_contents($confirmfile);
			$contents = replaceConfirm($contents, array('textarea'=>array('志望動機', 'その他')));
		}

	}
} else {
	$headers = apache_request_headers();
	$since = @$headers["If-Modified-Since"];
	$contents = $formcontent;
	$htmlfilemtime = filemtime($formfile);
	if ($since && strtotime($since) >= $htmlfilemtime) {
		header('Content-Type: text/html; charset=UTF-8', true, 304);
		exit;
	}
}
//ob_clean();

header('Content-Type: text/html; charset=UTF-8');
header('Content-Length: '.strlen($contents));
header('Last-Modified: '.gmdate( "D, d M Y H:i:s", $htmlfilemtime ) . " GMT");

echo $contents;


