<?php
/* vim:se et ts=4 sw=4 sts=4 */

class Recruit_Index
{

    private $head = array(
        'pc' => array(
            'title'       => 'エントリーフォーム画面',
            'keywords'    => '募集要項,看護師,ビューティーアドバイザー,美容カウンセラー,エステティシャン',
            'description' => 'あなたの当クリニックで「人を美しくする仕事がしたい」という思いをお聞かせ下さい。',
        ),
        'sp' => array(
            'title'       => 'エントリーフォーム画面',
            'keywords'    => '募集要項,看護師,ビューティーアドバイザー,美容カウンセラー,エステティシャン',
            'description' => 'あなたの当クリニックで「人を美しくする仕事がしたい」という思いをお聞かせ下さい。',
        ),
    );

    // {{{ init
    public function init()
    {
        $this->e = array(
            'name' => '',
            'kana' => '',
            'tel' => '',
            'email' => '',
            'email_con' => '',
        );
        render_set(array(
            'footer_name' => $this->footername(),
            'html' => array(
                'head' => spdir() ? $this->head['sp'] : $this->head['pc'],
            ),
            'f' => lib('form')->config(array(
                'except_values' => 'action,action_x,action_y',
                'separator' => '<br />',
                'preset_values' => array(
                    'occupation' => conf('app/occupation'),
                ),
            ))
        ), FALSE);
    }
    // }}}

    // {{{ action
    public function form()
    {
        lib('form')->values(array(
            'name' => '',
            'kana' => '',
            'age' => '',
            'tel1' => '',
            'tel2' => '',
            'tel3' => '',
            'email' => '',
            'email_con' => '',
            'occupation' => 1,
            'application' => '入力なし',
            'other' => '入力なし',
        ));
        render_name('recruit/.form');
        render_set(array(
            'e' => $this->e,
        ), FALSE);
    }

    public function confirm()
    {
        $data = lib('request')->post();
        lib('form')->values($data);
        $valid = array();
        $error = '';
        if ($this->validate($data, $valid, $error)) {
            // next to confirm
            render_name('recruit/.confirm');
        } else {
            // back to form
            $e = $error;
            render_name('recruit/.form');
            render_set(array(
                'e' => $e + $this->e,
            ), FALSE);
        }
    }

    public function execute()
    {
        $data = lib('request')->post();
        if (isset($data['action']['send'])) {
            // send mail
            unset($data['action']);
            $mail = $this->prepare($data);
            $this->sendmail($mail);
            redirect_to(request_uri(spdir() . '/recruit/complete'));
        } else {
            // back to form
            render_name('recruit/.form');
            render_set(array(
                'e' => $this->e,
                'f' => lib('form')->values($data),
            ), FALSE);
        }
    }

    public function complete()
    {
        render_name('recruit/.complete');
    }
    // }}}

    // {{{ tool
    private function sendmail($mail)
    {
        $system_mail = conf('app/system_mail_from');
        mb_language('ja');
        mb_internal_encoding('UTF-8');
        foreach ($mail as $data) {
            $head = 'From: ' . $system_mail;
            if (! empty($data['header'])) {
                foreach ($data['header'] as $key => $val) {
                    $head .= "\r\n$key: " . $val;
                }
            }
            $param = '-f' . $system_mail;
            $email = $data['email'];
            $subject = mb_encode_mimeheader($data['subject'], 'JIS', 'B', "\n");
            $message = mb_convert_encoding($data['message'], 'JIS', 'UTF-8');
            mail($email, $subject, $message, $head, $param);
            //debug_mail($data);
        }
    }

    private function prepare($data)
    {
        $data['entry_date'] = date('Y/m/d H:i:s');
        $conf = $this->mailtemplate();
        $mail = array();
        $admin = array(
            'subject' => $conf['admin']['subject'],
            'message' => $conf['admin']['message'],
        );
        $user = array(
            'subject' => $conf['user']['subject'],
            'message' => $conf['user']['message'],
        );
        if (spdir()) {
            $data['tel'] = $data['tel1'];
            $data['other'] = '';
        } else {
            $data['tel'] = $data['tel1'] . '-' . $data['tel2'] . '-' . $data['tel3'];
        }
        $data['occupation'] = conf('app/occupation/' . $data['occupation']);
        foreach ($data as $key => $val) {
            $key = '{' . $key . '}';
            $admin['subject'] = str_replace($key, $val, $admin['subject']);
            $admin['message'] = str_replace($key, $val, $admin['message']);
            $user['subject'] = str_replace($key, $val, $user['subject']);
            $user['message'] = str_replace($key, $val, $user['message']);
        }
        $mail[] = array(
            'email' => $data['email'],
            'subject' => $user['subject'],
            'message' => $user['message'],
        );
        foreach (explode(',', $conf['admin']['email']) as $addr) {
            $mail[] = array(
                'email' => $addr,
                'subject' => $admin['subject'],
                'message' => $admin['message'],
                'header' => array(
                    'Reply-To' => $data['email'],
                ),
            );
        }
        return $mail;
    }

    /**
     * Get mail template
     */
    private function mailtemplate()
    {
        $conf = conf('app/recruit');
        $dir  = dirname(ZIU_MAIN_PATH) . DS . 'templates';
        $file_admin = $dir . '/mail_recruit_admin.php';
        $file_user  = $dir . '/mail_recruit_user.php';
        if (is_readable($file_admin)) {
            $conf['admin'] = include $file_admin;
        }
        if (is_readable($file_user)) {
            $conf['user'] = include $file_user;
        }
        return $conf;
    }

    /**
     * Validate data
     * @param array  $data   : validate data
     * @param array  &$valid : valid data
     * @param array  &$error : error object list
     * @return boolean
     */
    private function validate(array $data, &$valid, &$error)
    {
        $error = array();
        $label = array(
            'name' => '名前',
            'kana' => 'フリガナ',
            'tel1' => '電話番号',
            'tel2' => '電話番号',
            'tel3' => '電話番号',
            'email' => '連絡先メールアドレス',
            'email_con' => '上記と同じメールアドレス',
        );
        foreach ($data as $key => $val) {
            switch ($key) {
                case 'name' :
                case 'email' :
                case 'email_con' :
                    if (empty($data[$key])) {
                        $error[$key] = "{$label[$key]}を入力してください";
                    }
                    break;
                case 'kana' :
                    if (empty($val)) {
                        $error[$key] = "{$label[$key]}を入力してください";
                    } elseif (! preg_match('/^[ァ-ヾー　 ]+$/u', $val)) {
                        $error[$key] = "カタカナで入力してください";
                    }
                    break;
                case 'tel1' :
                case 'tel2' :
                case 'tel3' :
                    if (empty($data[$key])) {
                        $error['tel'] = "{$label[$key]}を入力してください";
                    }
                    break;
                default :
            }
        }
        if (! spdir()) {
            if ($data['email'] != $data['email_con']) {
                $error['email_con'] = "{$label['email_con']}を入力してください";
            }
        }
        $res = empty($error);
        return $res;
    }

    /**
     * Get footer_name
     */
    private function footername()
    {
        $footer_name = '';
        if (spdir()) {
            switch (basename(main_action())) {
                case 'complete' :
                    $footer_name = 'footer';
                    break;
                default :
                    $footer_name = 'footer_copy';
            }
        } else {
            $footer_name = 'form_footer';
        }
        return $footer_name;
    }
    // }}}

}

