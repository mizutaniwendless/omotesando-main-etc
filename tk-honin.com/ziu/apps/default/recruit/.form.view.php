				<div class="inner660">
					<h1 class="pageTitle"><img src="./images/entry_title.gif" alt="エントリー" /></h1>
					<div class="pageInfo">全ての女性の永遠のテーマである美しさの追求。<br />
					「人を美しくする仕事がしたい」という想いを持つ、皆様のご応募をお待ちしております。</div>

					<p class="pt25">美容のプロとして患者様のキレイを叶えるお手伝いをする、非常にやりがいのある仕事です。スタッフとして働きながら、最先端の美容医療の知識をトータルで身につけることができるので、スキルアップにも。美に対する熱い思いをもつ皆様のご応募をお待ちしております。下記のフォームに必要事項をすべて入力の上、送信してください。追って当クリニックの採用担当者よりご連絡をいたします。</p>

					<div class="formType01">
                        <?php echo $f->open('../recruit/confirm') ?>
							<table>
								<tr>
									<th>お名前【必須】</th>
									<td>
                                        <p class="mb10">
                                            <span class="adjust">お名前</span><?php echo $f->text('name')->attributes(array('class' => 'txt w315')) ?><br />
                                            <span class="hint"><?php echo $e['name'] ?></span><br />
                                        </p>
                                        <p>
                                            <span class="adjust">フリガナ</span><?php echo $f->text('kana')->attributes(array('class' => 'txt w315')) ?><br />
                                            <span class="hint"><?php echo $e['kana'] ?></span>
                                        </p>
									</td>
								</tr>
								<tr>
									<th>年齢</th>
									<td><?php echo $f->text('age')->attributes(array('class' => 'txt w50')) ?>&nbsp;歳</td>
								</tr>
								<tr>
									<th>電話番号【必須】</th>
									<td>
                                        <?php echo $f->text('tel1')->attributes(array('class' => 'txt w70')) ?>&nbsp;&nbsp;-&nbsp;
                                        <?php echo $f->text('tel2')->attributes(array('class' => 'txt w70')) ?>&nbsp;&nbsp;-&nbsp;
                                        <?php echo $f->text('tel3')->attributes(array('class' => 'txt w70')) ?><br />
                                        <span class="hint"><?php echo $e['tel'] ?></span>
									</td>
								</tr>
								<tr>
									<th>連絡先メールアドレス【必須】</th>
									<td><?php echo $f->text('email')->attributes(array('class' => 'txt w315')) ?><br />
                                    <span class="hint"><?php echo $e['email'] ?></span>
									</td>
								</tr>
								<tr>
									<th>連絡先メールアドレス<br />確認用【必須】</th>
									<td><?php echo $f->text('email_con')->attributes(array('class' => 'txt w315')) ?><br />
                                    <span class="hint"><?php echo $e['email_con'] ?></span>
									</td>
								</tr>
								<tr>
									<th>希望職種</th>
									<td><?php echo $f->radio('occupation') ?></td>
								</tr>
								<tr>
									<th>志望動機</th>
									<td><?php echo $f->textarea('application')->attributes(array('rows' => '7')) ?></td>
								</tr>
								<tr>
									<th>その他</th>
									<td><?php echo $f->textarea('other')->attributes(array('rows' => '7')) ?></td>
								</tr>
							</table>
							<p class="tac"><?php echo $f->image('action')->attributes(array('src' => './images/btn_entry_confirm.gif', 'alt' => '入力確認')) ?></p>
						<?php echo $f->close() ?>
					<!-- /formType01 --></div>


					<div class="breadCrumb">
                        <p id="pagetop"><a href="#header"><img src="../images/btn_pagetop.gif" class="rollover" alt="ページ上部へ戻る" /></a></p>
						<ul>
							<li><a href="../">TOP</a></li>
							<li>&gt;</li>
							<li><a href="./recruit.html">採用情報</a></li>
							<li>&gt;</li>
							<li>エントリーフォーム画面</li>
						</ul>
					<!-- /breadCrumb --></div>
                    <div id="backtotop">
                        <a href="../index.html"><img src="../images/btn_backtop.gif" height="25" width="130" alt="Topページに戻る" /></a>
                    </div>


				<!-- /inner660 --></div>
