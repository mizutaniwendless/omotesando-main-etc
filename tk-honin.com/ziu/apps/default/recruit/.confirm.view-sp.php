
<?php content_for('javascript') ?>
<script type="text/javascript">
function go(mode){
    if (mode == 'back') {
        $('#dummy').attr('name', 'action[back]');
    } else {
        $('#dummy').attr('name', 'action[send]');
    }
    document.f.submit();
}
</script>   
<?php content_end_for('javascript') ?>

<!--mArea-->
<div class="mArea" id="recruit_cnf">
  <div class="h1_sec">
    <h1>採用エントリー</h1>

<ul id="steplist">
<li>情報の<br>ご入力</li>
<li class="active">内容の<br>ご確認</li>
<li>送信完了</li>
</ul>
<p class="noticeTxt-s">※入力内容をご確認の上、「送信」ボタンを押して下さい。</p>

  </div>
  <!--/h1_sec-->
  
<?php echo $f->open('../recruit/execute')->attributes(array('name' => 'f')) ?>
<?php echo $f->hidden('action[send]')->attributes(array('id' => 'dummy')) ?>
<?php echo $f->hidden() ?>

  <div class="sec mt00">
    <h2><span class="icon-red">必須</span>お名前</h2>
    <div class="innerBlock">
      <p><?php echo $f->fetch('name') ?><br /><?php echo $f->fetch('kana') ?></p>
    </div>
  </div>
  <!--/sec-->
  <div class="sec mt00">
    <h2><span class="icon-blue">任意</span>年齢</h2>
    <div class="innerBlock">
      <p><?php echo $f->fetch('age') ?>&nbsp;歳</p>
    </div>
  </div>
  <!--/sec-->
  <div class="sec mt00">
    <h2><span class="icon-red">必須</span>電話番号</h2>
    <div class="innerBlock">
      <p><?php echo $f->fetch('tel1') ?></p>
    </div>
  </div>
  <!--/sec-->
  <div class="sec mt00">
    <h2><span class="icon-red">必須</span>連絡先メールアドレス</h2>
    <div class="innerBlock">
      <p><?php echo $f->fetch('email') ?></p>
    </div>
  </div>
  <!--/sec-->
  <div class="sec mt00">
    <h2><span class="icon-blue">任意</span>希望職種</h2>
    <div class="innerBlock">
      <p><?php echo conf('app/occupation/' . $f->fetch('occupation')) ?></p>
    </div>
  </div>
  <!--/sec-->
  <div class="sec mt00">
    <h2><span class="icon-blue">任意</span>志望動機</h2>
    <div class="innerBlock">
      <p><?php echo nl2br($f->fetch('application')) ?></p>
    </div>
  </div>
  <!--/sec-->
  
  <!--/backTopBlock-->
  <div class="btnGroupBlock clearfix">
    <a href="javascript:void(0);" onclick="go('back'); return false;" class="btn_pink btn_l"><span>修正する</span></a>
    <a href="javascript:void(0);" onclick="go('exec'); return false;" class="btn_pink btn_r"><span>送信</span></a>
  </div>

<?php echo $f->close() ?>

</div>
<!--/mArea--> 

