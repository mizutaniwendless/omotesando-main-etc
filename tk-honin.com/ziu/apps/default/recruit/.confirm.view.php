				<div class="inner660">
					<h1 class="pageTitle"><img src="./images/entry_title.gif" alt="エントリー" /></h1>
					<div class="pageInfo">全ての女性の永遠のテーマである美しさの追求。<br />
					「人を美しくする仕事がしたい」という想いを持つ、皆様のご応募をお待ちしております。</div>

					<p class="pt25">美容のプロとして患者様のキレイを叶えるお手伝いをする、非常にやりがいのある仕事です。スタッフとして働きながら、最先端の美容医療の知識をトータルで身につけることができるので、スキルアップにも。美に対する熱い思いをもつ皆様のご応募をお待ちしております。下記のフォームに必要事項をすべて入力の上、送信してください。追って当クリニックの採用担当者よりご連絡をいたします。</p>

					<div class="formType01">
                        <?php echo $f->open('../recruit/execute') ?>
                        <?php echo $f->hidden() ?>
							<table>
								<tr>
									<th>お名前</th>
									<td class="fb">
                                        <?php echo $f->fetch('name') ?><br />
                                        <?php echo $f->fetch('kana') ?>
                                    </td>
								</tr>
								<tr>
									<th>年齢</th>
									<td class="fb"><?php echo $f->fetch('age') ?>&nbsp;歳</td>
								</tr>
								<tr>
									<th>電話番号</th>
									<td class="fb"><?php echo $f->fetch('tel1') ?>-<?php echo $f->fetch('tel2') ?>-<?php echo $f->fetch('tel3') ?></td>
								</tr>
								<tr>
									<th>連絡先メールアドレス</th>
									<td class="fb"><?php echo $f->fetch('email') ?></td>
								</tr>
								<tr>
									<th>希望職種</th>
									<td class="fb"><?php echo conf('app/occupation/' . $f->fetch('occupation')) ?></td>
								</tr>
								<tr>
									<th>志望動機</th>
									<td><?php echo nl2br($f->fetch('application')) ?></td>
								</tr>
								<tr>
									<th>その他</th>
									<td><?php echo nl2br($f->fetch('other')) ?></td>
								</tr>
							</table>
							<p class="tac">
                                <?php echo $f->image('action[back]')->attributes(array('src' => './images/btn_entry_back.gif', 'alt' => '入力画面に戻る')) ?>
                                <?php echo $f->image('action[send]')->attributes(array('src' => './images/btn_entry_submit.gif', 'alt' => '送信', 'class' => 'ml25')) ?>
							</p>
						<?php echo $f->close() ?>
					<!-- /formType01 --></div>


					<div class="breadCrumb">
                        <p id="pagetop"><a href="#header"><img src="../images/btn_pagetop.gif" class="rollover" alt="ページ上部へ戻る" /></a></p>
						<ul>
							<li><a href="../">TOP</a></li>
							<li>&gt;</li>
							<li><a href="./recruit.html">採用情報</a></li>
							<li>&gt;</li>
							<li>リクルート入力確認</li>
						</ul>
					<!-- /breadCrumb --></div>
                    <div id="backtotop">
                        <a href="../index.html"><img src="../images/btn_backtop.gif" height="25" width="130" alt="Topページに戻る" /></a>
                    </div>


				<!-- /inner660 --></div>
