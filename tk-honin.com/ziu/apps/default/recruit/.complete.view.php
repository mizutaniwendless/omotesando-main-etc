				<div class="inner660">
					<h1 class="pageTitle"><img src="./images/entry_title.gif" alt="エントリー" /></h1>
					<div class="pageInfo">全ての女性の永遠のテーマである美しさの追求。<br />
					「人を美しくする仕事がしたい」という想いを持つ、皆様のご応募をお待ちしております。</div>

					<p class="pt25">美容のプロとして患者様のキレイを叶えるお手伝いをする、非常にやりがいのある仕事です。スタッフとして働きながら、最先端の美容医療の知識をトータルで身につけることができるので、スキルアップにも。美に対する熱い思いをもつ皆様のご応募をお待ちしております。下記のフォームに必要事項をすべて入力の上、送信してください。追って当クリニックの採用担当者よりご連絡をいたします。</p>

					<div class="thanksBox">
						<p class="title">エントリー完了</p>
						<p class="tac">当クリニックの採用募集にエントリー頂きありがとうございます。<br />
						確認後、厳正な審査の上、お電話かメールでご連絡させていただきます。</p>
					<!-- /thansksBox --></div>

					<p class="tac"><a href="../recruit/form"><img src="./images/btn_entry_back.gif" alt="入力画面に戻る" /></a></p>


					<div class="breadCrumb">
                        <p id="pagetop"><a href="#header"><img src="../images/btn_pagetop.gif" class="rollover" alt="ページ上部へ戻る" /></a></p>
						<ul>
							<li><a href="../">TOP</a></li>
							<li>&gt;</li>
							<li><a href="./recruit.html">採用情報</a></li>
							<li>&gt;</li>
							<li>リクルート入力完了</li>
						</ul>
					<!-- /breadCrumb --></div>
                    <div id="backtotop">
                        <a href="../index.html"><img src="../images/btn_backtop.gif" height="25" width="130" alt="Topページに戻る" /></a>
                    </div>


				<!-- /inner660 --></div>
