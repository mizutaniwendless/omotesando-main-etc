
<!--mArea-->
<div class="mArea" id="recruit_complete">
  <div class="h1_sec">
    <h1>採用エントリー</h1>

<ul id="steplist">
<li>情報の<br>ご入力</li>
<li>内容の<br>ご確認</li>
<li class="active">送信完了</li>
</ul>

  </div><!--/h1_sec-->
  <div class="box-bdr center">
    <p class="txt-strong">エントリー完了</p>
    <p class="txt">当社のスタッフにご応募いただき<br/>誠にありがとうございました。<br/>追って、担当者よりお電話かメールにて<br/>ご連絡をさせていただきます。<br/>恐れ入りますが、少々お待ちくださいますよう<br/>お願い申し上げます。</p>
  </div>
  <div class="buttonStepInfoBlock">
    <a href="../flow/flow.html"><span>施術の流れを見る</span></a>
  </div>
  <div class="buttonStaffInfoBlock">
    <a href="../about/doctor.html"><span>医師の紹介を見る</span></a>
  </div>
    <div class="backHomeBlock">
    <a href="../index.html"><span>Topページに戻る</span></a>
  </div><!--/backHomeBlock-->
</div>
<!--/mArea-->

