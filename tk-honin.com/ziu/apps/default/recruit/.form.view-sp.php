
<!--mArea-->
<div class="mArea" id="recruit_form">
  <div class="h1_sec">
    <h1>採用エントリー</h1>
    <p>全ての女性の永遠のテーマである美しさの追求。<br>
「人を美しくする仕事がしたい」という想いを持つ、皆様のご応募をお待ちしております。</p>
  
    <div class="bd_t">
      <p>美容のプロとして患者様のキレイを叶えるお手伝いをする、非常にやりがいのある仕事です。スタッフとして働きながら、最先端の美容医療の知識をトータルで身につけることができるので、スキルアップにも。美に対する熱い思いをもつ皆様のご応募をお待ちしております。下記のフォームに必要事項をすべて入力の上、送信してください。追って当クリニックの採用担当者よりご連絡をいたします。</p>
    </div><!--/innerBlock-->
  </div><!--/h1_sec-->
  <div class="img tac"><a href="tel:0120334270" onClick="ga('send', 'event', 'sp', 'tel-tap', 'tel');"><img src="../common/images/form_tel.jpg" alt="TEL:0120-334-270　完全予約制 受付時間 11時 － 20時" width="580" height="204"></a></div><!--/sec-->
    
<ul id="steplist">
<li class="active">様情報の<br>ご入力</li>
<li>内容の<br>ご確認</li>
<li>送信完了</li>
</ul>
<p class="noticeTxt-s">※ 大変お手数ですが必須項目はすべてご記入頂き、<br/>「確認画面へ」ボタンを押してください。</p>

<?php echo $f->open('../recruit/confirm')->attributes(array('class' => 'niceform')) ?>
  <div class="sec" id="form">
    <div class="formBlock lh1_6">
        <dl>
          <dt><span class="icon-red">必須</span>お名前</dt>
          <dd class="name">
            <div class="clearfix">
                <span class="form_tt">お名前</span>
              <div class="form_colr">
                <p><?php echo $f->text('name')->attributes(array('class' => 'mst', 'placeholder' => '例） 表参道　花子')) ?></p>
<?php if ($e['name']) :?>
                <p class="error db" title="名前"><?php echo $e['name'] ?></p>
<?php else : ?>
                <p class="error" title="名前">名前を入力してください</p>
<?php endif; ?>
              </div>
            </div>
          
            <div class="item clearfix">
                <span class="form_tt">フリガナ</span>
              <div class="form_colr">
                <p><?php echo $f->text('kana')->attributes(array('class' => 'mst', 'placeholder' => '例） オモテサンドウ　ハナコ')) ?></p>
<?php if ($e['kana']) :?>
                <p class="error db" title="フリガナ"><?php echo $e['kana'] ?></p>
<?php else : ?>
                <p class="error" title="フリガナ">フリガナを入力してください</p>
<?php endif; ?>
              </div>
            </div>
            
          </dd>
          <dt><span class="icon-blue">任意</span>年齢</dt>
          <dd class="age">
            <p><?php echo $f->number('age')->attributes(array('class' => 'num')) ?>&nbsp;歳</p>
          </dd>
          <dt><span class="icon-red">必須</span>電話番号</dt>
          <dd class="tel">
            <p><?php echo $f->text('tel1')->attributes(array('class' => 'mst num', 'placeholder' => '例）03-1234-5678', 'style' => 'ime-mode: disabled;')) ?></p>
<?php if ($e['tel']) :?>
                <p class="error db" title="電話番号"><?php echo $e['tel'] ?></p>
<?php else : ?>
                <p class="error" title="電話番号">電話番号を入力してください</p>
<?php endif; ?>
          </dd>
          <dt><span class="icon-red">必須</span>連絡先メールアドレス</dt>
          <dd class="mail">
            <p><?php echo $f->email('email')->attributes(array('class' => 'mst', 'placeholder' => '例）hanako@omotesando.co.jp')) ?></p>
            <p>例）hanako@omotesando.co.jp</p>
<?php if ($e['email']) :?>
                <p class="error db" title="連絡先メールアドレス"><?php echo $e['email'] ?></p>
<?php else : ?>
                <p class="error" title="連絡先メールアドレス">連絡先メールアドレスを入力してください</p>
<?php endif; ?>
          </dd>
          <dt><span class="icon-blue">任意</span>希望職種</dt>
          <dd class="radiochoice"><?php echo occupation_html_for_sp($f->radio('occupation')) ?></dd>
          <dt><span class="icon-blue">任意</span>志望動機</dt>
          <dd>
            <p><?php echo $f->textarea('application')->attributes(array('rows' => '7')) ?></p>
          </dd>
        </dl>

    </div>
  </div><!--/sec-->

  <div class="formBtn"><?php echo $f->submit('action', '確認画面へ')->attributes(array('id' => 'submit')) ?></div>
  
<?php echo $f->close() ?>

</div>
<!--/mArea-->

