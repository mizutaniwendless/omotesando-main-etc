<?php asset_for('stylesheet', 'catalog/css/confirm.css'); ?>

				<div class="inner660">
					<h1 class="pageTitle"><img src="./images/catalog_title.gif" alt="資料請求" /></h1>

<div class="now_status">
	<img src="images/now_status_2.gif" height="36" width="660" alt="" />
</div>


<?php echo $f->open('../catalog/execute') ?>
<?php echo $f->hidden() ?>

<table width="660px">
<tr>
	<td class="table-left">お名前</td>
	<td><?php echo $f->fetch('name') ?><br><?php echo $f->fetch('kana') ?></td>
</tr>

<tr>
	<td class="table-left">郵便番号</td>
	<td><?php echo $f->fetch('zip1') ?>-<?php echo $f->fetch('zip2') ?></td>
</tr>

<tr>
	<td class="table-left">住所</td>
	<td><?php echo conf('app/prefecture/' . $f->fetch('pref')) ?><?php echo $f->fetch('addr1') ?><?php echo $f->fetch('addr2') ?></td>
</tr>

<tr>
	<td class="table-left">電話番号</td>
	<td><?php echo $f->fetch('tel') ?></td>
</tr>

<tr>
	<td class="table-left">メールアドレス</td>
	<td><?php echo $f->fetch('email') ?></td>
</tr>

</table>


<div class="comfirm">
    <?php echo $f->image('action[back]')->attributes(array('src' => 'images/back_btn.gif', 'alt' => '戻る', 'class' => 'comfirm_back')) ?>
    <?php echo $f->image('action[send]')->attributes(array('src' => 'images/send_btn.gif', 'alt' => '送信')) ?>
</div>

<?php echo $f->close() ?>

					<div class="breadCrumb">
                        <p id="pagetop"><a href="#header"><img src="../images/btn_pagetop.gif" class="rollover" alt="ページ上部へ戻る" /></a></p>
						<ul>
							<li><a href="../">TOP</a></li>
							<li>&gt;</li>
							<li>資料請求確認</li>
						</ul>
					<!-- /breadCrumb --></div>
                    <div id="backtotop">
                        <a href="../index.html"><img src="../images/btn_backtop.gif" height="25" width="130" alt="Topページに戻る" /></a>
                    </div>


				<!-- /inner660 --></div>
