<?php
/* vim:se et ts=4 sw=4 sts=4 */

class Catalog_Index
{

    private $dirname = 'catalog';

    private $head = array(
        'pc' => array(
            'title'       => '資料請求',
            'keywords'    => 'お電話またはメールによるご予約,カウンセラーによるカウンセリング,専門のドクターによるカウンセリング,施術のお申し込み手続き,手術・治療,アフターケア',
            'description' => '当クリニックではお客様との信頼を高める為に、充分なカウンセリングを行い、不安を取り除いてから、経験に基づいた医師達による施術、そして術後のアフターフォローまで、万全の体制でサポートいたします。',
        ),
        'sp' => array(
            'title'       => '資料請求',
            'keywords'    => 'お電話またはメールによるご予約,カウンセラーによるカウンセリング,専門のドクターによるカウンセリング,施術のお申し込み手続き,手術・治療,アフターケア',
            'description' => '当クリニックではお客様との信頼を高める為に、充分なカウンセリングを行い、不安を取り除いてから、経験に基づいた医師達による施術、そして術後のアフターフォローまで、万全の体制でサポートいたします。',
        ),
    );

    // {{{ init
    public function init()
    {
        $this->e = array(
            'name' => '',
            'kana' => '',
            'zip' => '',
            'pref' => '',
            'addr1' => '',
            'addr2' => '',
            'tel' => '',
            'email' => '',
            'email_con' => '',
        );
        render_set(array(
            'footer_name' => $this->footername(),
            'html' => array(
                'head' => spdir() ? $this->head['sp'] : $this->head['pc'],
            ),
            'f' => lib('form')->config(array(
                'except_values' => 'action,action_x,action_y',
                'separator' => '',
                'preset_values' => array(
                    'pref' => array('' => '選択してください') + conf('app/prefecture'),
                ),
            ))
        ), FALSE);
    }
    // }}}

    // {{{ action
    public function form()
    {
        lib('form')->values(array(
            'name' => '',
            'kana' => '',
            'zip1' => '',
            'zip2' => '',
            'pref' => '',
            'addr1' => '',
            'addr2' => '',
            'tel' => '',
            'email' => '',
        ));
        render_name($this->dirname . '/.form');
        render_set(array(
            'e' => $this->e,
        ), FALSE);
    }

    public function confirm()
    {
        $data = lib('request')->post();
        lib('form')->values($data);
        $valid = array();
        $error = '';
        if ($this->validate($data, $valid, $error)) {
            // next to confirm
            render_name($this->dirname . '/.confirm');
        } else {
            // back to form
            $e = $error;
            render_name($this->dirname . '/.form');
            render_set(array(
                'e' => $e + $this->e,
            ), FALSE);
        }
    }

    public function execute()
    {
        $data = lib('request')->post();
        if (isset($data['action']['send'])) {
            // send mail
            unset($data['action']);
            $mail = $this->prepare($data);
            $this->sendmail($mail);
            redirect_to(request_uri(spdir() . '/' . $this->dirname . '/complete?name=' . urlencode($data['name'])));
        } else {
            // back to form
            render_name($this->dirname . '/.form');
            render_set(array(
                'e' => $this->e,
                'f' => lib('form')->values($data),
            ), FALSE);
        }
    }

    public function complete()
    {
        render_name($this->dirname . '/.complete');
        render_set(array(
            'name' => lib('request')->get('name'),
        ));
    }
    // }}}

    // {{{ tool
    private function sendmail($mail)
    {
        $system_mail = conf('app/system_mail_from');
        mb_language('ja');
        mb_internal_encoding('UTF-8');
        foreach ($mail as $data) {
            $head = 'From: ' . $system_mail;
            if (! empty($data['header'])) {
                foreach ($data['header'] as $key => $val) {
                    $head .= "\r\n$key: " . $val;
                }
            }
            $param = '-f' . $system_mail;
            $email = $data['email'];
            $subject = mb_encode_mimeheader($data['subject'], 'JIS', 'B', "\n");
            $message = mb_convert_encoding($data['message'], 'JIS', 'UTF-8');
            mail($email, $subject, $message, $head, $param);
            //debug_mail($data);
        }
    }

    private function prepare($data)
    {
        $data['entry_date'] = date('Y/m/d H:i:s');
        $conf = $this->mailtemplate();
        $mail = array();
        $admin = array(
            'subject' => $conf['admin']['subject'],
            'message' => $conf['admin']['message'],
        );
        $user = array(
            'subject' => $conf['user']['subject'],
            'message' => $conf['user']['message'],
        );
        $data['pref'] = conf('app/prefecture/' . $data['pref']);
        foreach ($data as $key => $val) {
            $key = '{' . $key . '}';
            $admin['subject'] = str_replace($key, $val, $admin['subject']);
            $admin['message'] = str_replace($key, $val, $admin['message']);
            $user['subject'] = str_replace($key, $val, $user['subject']);
            $user['message'] = str_replace($key, $val, $user['message']);
        }
        $mail[] = array(
            'email' => $data['email'],
            'subject' => $user['subject'],
            'message' => $user['message'],
        );
        foreach (explode(',', $conf['admin']['email']) as $addr) {
            $mail[] = array(
                'email' => $addr,
                'subject' => $admin['subject'],
                'message' => $admin['message'],
                'header' => array(
                    'Reply-To' => $data['email'],
                ),
            );
        }
        return $mail;
    }

    /**
     * Get mail template
     */
    private function mailtemplate()
    {
        $conf = conf('app/catalog');
        $dir  = dirname(ZIU_MAIN_PATH) . DS . 'templates';
        $file_admin = $dir . '/mail_catalog_admin.php';
        $file_user  = $dir . '/mail_catalog_user.php';
        if (is_readable($file_admin)) {
            $conf['admin'] = include $file_admin;
        }
        if (is_readable($file_user)) {
            $conf['user'] = include $file_user;
        }
        return $conf;
    }

    /**
     * Validate data
     * @param array  $data   : validate data
     * @param array  &$valid : valid data
     * @param array  &$error : error object list
     * @return boolean
     */
    private function validate(array $data, &$valid, &$error)
    {
        $error = array();
        $label = array(
            'name' => 'お名前',
            'kana' => 'フリガナ',
            'zip1' => '郵便番号',
            'zip2' => '郵便番号',
            'pref' => '都道府県',
            'addr1' => '市区町村',
            'addr2' => '丁目番地',
            'tel' => '電話番号',
            'email' => 'メールアドレス',
        );
        foreach ($data as $key => $val) {
            switch ($key) {
                case 'name' :
                    if (empty($val)) {
                        $error[$key] = "{$label[$key]}を入力してください";
                    }
                    break;
                case 'kana' :
                    if (empty($val)) {
                        $error[$key] = "{$label[$key]}を入力してください";
                    } elseif (! preg_match('/^[ァ-ヾー　 ]+$/u', $val)) {
                        $error[$key] = "カタカナで入力してください";
                    }
                    break;
                case 'zip1' :
                    if (empty($val)) {
                        $error['zip'] = "{$label[$key]}を入力してください";
                    } elseif (! preg_match('/\d{3}/', $val)) {
                        $error['zip'] = "{$label[$key]}を正しく入力してください";
                    }
                    break;
                case 'zip2' :
                    if (empty($val)) {
                        $error['zip'] = "{$label[$key]}を入力してください";
                    } elseif (! preg_match('/\d{4}/', $val)) {
                        $error['zip'] = "{$label[$key]}を正しく入力してください";
                    }
                    break;
                case 'pref' :
                case 'addr1' :
                    if (empty($val)) {
                        $error[$key] = "{$label[$key]}を入力してください";
                    }
                    break;
                case 'addr2' :
                    if (empty($val)) {
                        if (spdir()) {
                            $error[$key] = "{$label[$key]}を入力してください";
                        } else {
                            $error[$key] = "<p class=\"hint\">{$label[$key]}を入力してください</p>";
                        }
                    }
                    break;
                case 'tel' :
                    if (empty($val)) {
                        $error[$key] = "{$label[$key]}を入力してください";
                    } elseif (strlen($val) > 15 || ! preg_match('/^[\d-]+$/u', $val)) {
                        $error[$key] = "{$label[$key]}を正しく入力してください";
                    }
                    break;
                case 'email' :
                    if (empty($val)) {
                        $error[$key] = "<br>{$label[$key]}を入力してください";
                    } elseif (strpos($val, '@') === FALSE) {
                        $error[$key] = "<br>{$label[$key]}に＠を入力してください<br>「{$val}」内に＠がありません";
                    } elseif (! preg_match("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix", $val)) {
                        $error[$key] = "<br>{$label[$key]}を正しく入力してください";
                    }
                    break;
                default :
            }
        }
        $res = empty($error);
        return $res;
    }

    /**
     * Get footer_name
     */
    private function footername()
    {
        $footer_name = '';
        if (spdir()) {
            switch (basename(main_action())) {
                case 'complete' :
                    $footer_name = 'footer_reservation_complete';
                    break;
                default :
                    $footer_name = 'footer_copy';
            }
        } else {
            $footer_name = 'form_footer';
        }
        return $footer_name;
    }
    // }}}

}

