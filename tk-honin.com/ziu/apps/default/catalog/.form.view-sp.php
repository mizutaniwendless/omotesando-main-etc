
<?php content_for('javascript') ?>
<script type="text/javascript">
$(document).ready(function(){
    $('select').selectmenu();
}); 
</script>   
<?php content_end_for('javascript') ?>
<script src="https://ajaxzip3.github.io/ajaxzip3.js" charset="UTF-8"></script>

<!--mArea-->
<div class="mArea" id="catalog_form">
  <div class="h1_sec">
    <h1>資料請求</h1>
    <div class="clm_l">
      <p>表参道スキンクリニックに関する資料を無料で進呈しています。 <br>資料請求をご希望される方は <br>下記よりお申込み下さい。まずはお気軽にお申込み下さい。</p>
    </div><!--/innerBlock-->
  </div><!--/h1_sec-->
  <div class="img tac"><a href="tel:0120334270" onClick="ga('send', 'event', 'sp', 'tel-tap', 'tel');"><img src="../common/images/form_tel.jpg" alt="TEL:0120-334-270　完全予約制 受付時間 11時 － 20時" width="580" height="204"></a></div><!--/sec-->
    
<ul id="steplist">
<li class="active">お客様情報の<br>入力</li>
<li>内容の<br>ご確認</li>
<li>送信完了</li>
</ul>
<p class="noticeTxt-s">※ 大変お手数ですが必須項目はすべてご記入頂き、<br/>「確認画面へ」ボタンを押してください。</p>

<?php echo $f->open('../catalog/confirm')->attributes(array('class' => 'niceform')) ?>

  <div class="sec" id="form">
    <div class="formBlock lh1_6">
        <dl>
          <dt><span class="icon-red">必須</span>お名前</dt>
          <dd class="name">
            <div class="clearfix">
                <span class="form_tt">お名前</span>
              <div class="form_colr">
                <p><?php echo $f->text('name')->attributes(array('class' => 'mst', 'placeholder' => '例） 表参道　花子')) ?></p>
<?php if ($e['name']) :?>
                <p class="error db" title="名前"><?php echo $e['name'] ?></p>
<?php else : ?>
                <p class="error" title="名前">名前を入力してください</p>
<?php endif; ?>
              </div>
            </div>
          
            <div class="item clearfix">
                <span class="form_tt">フリガナ</span>
              <div class="form_colr">
                <p><?php echo $f->text('kana')->attributes(array('class' => 'mst', 'placeholder' => '例） オモテサンドウ　ハナコ')) ?></p>
<?php if ($e['kana']) :?>
                <p class="error db" title="ふりがな"><?php echo $e['kana'] ?></p>
<?php else : ?>
                <p class="error" title="ふりがな">ふりがなを入力してください</p>
<?php endif; ?>
              </div>
            </div>
            
          </dd>
          <dt><span class="icon-red">必須</span>郵便番号</dt>
          <dd class="tel">
            <p><?php echo $f->text('zip1')->attributes(array('class' => 'mst num', 'placeholder' => '例）150', 'style' => 'ime-mode: disabled;width: 30%;')) ?> - <?php echo $f->text('zip2')->attributes(array('class' => 'mst num', 'placeholder' => '0001', 'style' => 'ime-mode: disabled;width: 50%;', 'onkeyup' => "AjaxZip3.zip2addr('zip1','zip2','pref','addr1');", 'onchange' => "$('#pref').selectmenu('refresh');")) ?></p>
<?php if ($e['zip']) :?>
                <p class="error db" title="郵便番号"><?php echo $e['zip'] ?></p>
<?php else : ?>
                <p class="error" title="郵便番号">郵便番号を入力してください</p>
<?php endif; ?>
          </dd>
          <dt><span class="icon-red">必須</span>住所</dt>
          <dd class="address">
            <div class="clearfix">
                <span class="form_tt">都道府県</span>
              <div class="form_colr">
                <p><?php echo $f->select('pref')->attributes(array('id' => 'pref')) ?></p>
<?php if ($e['pref']) :?>
                <p class="error db" title="都道府県"><?php echo $e['pref'] ?></p>
<?php else : ?>
                <p class="error" title="都道府県">都道府県を指定してください</p>
<?php endif; ?>
              </div>
            </div>
          
            <div class="item clearfix">
                <span class="form_tt">市区町村</span>
              <div class="form_colr">
                <p><?php echo $f->text('addr1')->attributes(array('class' => 'mst', 'placeholder' => '例) 渋谷区神宮前')) ?></p>
<?php if ($e['addr1']) :?>
                <p class="error db" title="市区町村"><?php echo $e['addr1'] ?></p>
<?php else : ?>
                <p class="error" title="市区町村">市区町村を入力してください</p>
<?php endif; ?>
              </div>
            </div>
          
            <div class="item clearfix">
                <span class="form_tt">丁目番地</span>
              <div class="form_colr">
                <p><?php echo $f->text('addr2')->attributes(array('class' => 'mst', 'placeholder' => '例) 5-9-13 喜多重ビル 4階')) ?></p>
<?php if ($e['addr2']) :?>
                <p class="error db" title="丁目番地"><?php echo $e['addr2'] ?></p>
<?php else : ?>
                <p class="error" title="丁目番地">丁目番地を入力してください</p>
<?php endif; ?>
              </div>
            </div>
            
          </dd>
          <dt><span class="icon-red">必須</span>電話番号</dt>
          <dd class="tel">
            <p><?php echo $f->text('tel')->attributes(array('class' => 'mst num', 'placeholder' => '例）03-1234-5678', 'style' => 'ime-mode: disabled;')) ?></p>
<?php if ($e['tel']) :?>
                <p class="error db" title="電話番号"><?php echo $e['tel'] ?></p>
<?php else : ?>
                <p class="error" title="電話番号">電話番号を入力してください</p>
<?php endif; ?>
          </dd>
          <dt><span class="icon-red">必須</span>連絡先メールアドレス</dt>
          <dd class="mail">
            <p><?php echo $f->email('email')->attributes(array('class' => 'mst', 'placeholder' => '例）hanako@omotesando.co.jp')) ?></p>
<?php if ($e['email']) :?>
                <p class="error db" title="連絡先メールアドレス"><?php echo $e['email'] ?></p>
<?php else : ?>
                <p class="error" title="連絡先メールアドレス">連絡先メールアドレスを入力してください</p>
<?php endif; ?>
          </dd>
        </dl>

    </div>
  </div><!--/sec-->

  <div class="formBtn"><?php echo $f->submit('action', '確認画面へ') ?></div>

<?php echo $f->close() ?>
  
</div>
<!--/mArea-->

