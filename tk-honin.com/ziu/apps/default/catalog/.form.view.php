<?php asset_for('stylesheet', 'catalog/css/form.css'); ?>
<?php content_for('stylesheet') ?>
<style type="text/css">
span.hint {
    margin-left: 5px;
    color: red;
}
p.hint {
    margin-left: 64px;
    color: red;
}
</style>
<?php content_end_for('stylesheet') ?>
<script src="https://ajaxzip3.github.io/ajaxzip3.js" charset="UTF-8"></script>

				<div class="inner660">
					<h1 class="pageTitle"><img src="./images/catalog_title.gif" alt="施術の流れ" /></h1>

					<p class="sub_title">表参道スキンクリニックに関する資料を無料で進呈しています。<br>
資料請求をご希望される方は下記よりお申込み下さい。<br>
まずはお気軽にお申込み下さい。</p>

<div class="contact-denwa">
	<img src="images/contact_1.gif" height="86" width="660" alt="" />
	<img src="images/contact_img.png" height="233" width="216" alt="" class="denwa-2"/>
</div>

<div class="now_status">
	<img src="images/now_status.gif" height="36" width="660" alt="" />
</div>

<?php echo $f->open('../catalog/confirm') ?>

<table width="660px">
<tr>
<td class="table-left t-1"><img src="images/td-img.gif" height="16" width="28" alt="" />お名前</td>
<td>
<label for="name" class="name-title">お名前</label>
<?php echo $f->text('name') ?><span class="hint"><?php echo $e['name'] ?></span><br>
<p class="form_name_ex">例) 表参道　花子</p>
<label for="name_sub" class="name-title">フリガナ</label>
<?php echo $f->text('kana') ?><span class="hint"><?php echo $e['kana'] ?></span>
<p class="form_name_ex form_name_ex_last">例) オモサンドウ　ハナコ</p>

</td>
</tr>
<tr>
<td class="table-left t-2"><img src="images/td-img.gif" height="16" width="28" alt="" />郵便番号</td>
<td><?php echo $f->text('zip1')->attributes(array('id' => 'post-1')) ?> - <?php echo $f->text('zip2')->attributes(array('id' => 'post-2', 'onkeyup' => "AjaxZip3.zip2addr('zip1','zip2','pref','addr1');")) ?><span class="hint"><?php echo $e['zip'] ?></span><br><p class="form_ex">例) 150-0001</p></td>

</tr>
<tr>
<td class="table-left t-3"><img src="images/td-img.gif" height="16" width="28" alt="" />住所</td>
<td><label for="pre" class="prefe">都道府県</label>
<?php echo $f->select('pref') ?><span class="hint"><?php echo $e['pref'] ?></span>
<br>

<label for="town" class="address-title">市区町村</label><?php echo $f->text('addr1')->attributes(array('id' => 'town')) ?><span class="hint"><?php echo $e['addr1'] ?></span><br>
<p class="form_name_ex">例) 渋谷区神宮前</p>
<label for="address" class="address-title">丁目番地</label><?php echo $f->text('addr2')->attributes(array('id' => 'address')) ?><span class="hint"><?php echo $e['addr2'] ?></span><br>
<p class="form_name_ex">例) 5-9-13 喜多重ビル 4階</p>

</td>
</tr>
<tr>
<td class="table-left t-4"><img src="images/td-img.gif" height="16" width="28" alt="" />電話番号</td>
<td><?php echo $f->text('tel')->attributes(array('id' => 'tel')) ?><span class="hint"><?php echo $e['tel'] ?></span><br>
<p class="form_ex">例) 0120-334-270</p></td>
</tr>
<tr>
<td class="table-left t-5"><img src="images/td-img.gif" height="16" width="28" alt="" />メールアドレス</td>
<td><?php echo $f->text('email')->attributes(array('id' => 'mail')) ?><span class="hint"><?php echo $e['email'] ?></span><br><p class="form_ex">例) info@tk-honin.com</p></td>

</tr>
</table>


<div class="comfirm">
    <?php echo $f->image('action')->attributes(array('src' => 'images/comfirm_btn.gif', 'alt' => '入力確認')) ?>
</div>

<?php echo $f->close() ?>


					<div class="breadCrumb">
                        <p id="pagetop"><a href="#header"><img src="../images/btn_pagetop.gif" class="rollover" alt="ページ上部へ戻る" /></a></p>
						<ul>
							<li><a href="../">TOP</a></li>
							<li>&gt;</li>
							<li>資料請求</li>
						</ul>
					<!-- /breadCrumb --></div>
                    <div id="backtotop">
                        <a href="../index.html"><img src="../images/btn_backtop.gif" height="25" width="130" alt="Topページに戻る" /></a>
                    </div>


				<!-- /inner660 --></div>
