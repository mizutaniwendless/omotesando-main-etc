				<div class="inner660">
					<h1 class="pageTitle"><img src="./images/res_title.gif" alt="無料カウンセリング予約" /></h1>
					<div class="pageInfo">当クリニックへのご質問、ご相談をお受けしています。<br />
					施術に関するご相談から、お悩み相談、クリニックに関する事など、何なりとお気軽にお問い合わせ下さい。</div>

					<p class="pt25">表参道スキンクリニックでは随時、専門医による無料カウンセリングを行っております。ご希望の方は、下記のフォームにご来院希望日など必要事項を入力の上、送信をお願いいたします。予約フォームを受信しましたら、折り返し電話またはメールにて確認のご連絡をさせていただきます。<br />
					なお、ご質問やご相談は電話でも承っておりますので、お急ぎの方はこちらをご利用くださいませ。<br />
					<span class="cPink01 fb fs13">フリーダイヤル：0120‐334‐270</span></p>

					<div class="thanksBox">
						<p class="title">カウンセリング予約完了</p>
						<p class="tac">カウンセリングのご予約お申込ありがとうございます。 <br />
						予約状況を確認後、ご予約受付完了のお知らせを、お電話か<br />
						メールでご連絡させていただきます。</p>
					<!-- /thansksBox --></div>

					<p class="tac"><a href="../reservation/form"><img src="./images/btn_entry_back.gif" alt="入力画面に戻る" /></a></p>

					<div class="breadCrumb">
                        <p id="pagetop"><a href="#header"><img src="../images/btn_pagetop.gif" class="rollover" alt="ページ上部へ戻る" /></a></p>
						<ul>
							<li><a href="../">TOP</a></li>
							<li>&gt;</li>
							<li>予約フォーム入力完了</li>
						</ul>
					<!-- /breadCrumb --></div>
                    <div id="backtotop">
                        <a href="../index.html"><img src="../images/btn_backtop.gif" height="25" width="130" alt="Topページに戻る" /></a>
                    </div>


				<!-- /inner660 --></div>

<!-- {{{ 計測タグ -->
<!-- Yahoo用コンバージョンタグ -->
<!-- Yahoo Code for your Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var yahoo_conversion_id = 1000199666;
var yahoo_conversion_label = "mvTHCOGbz1oQiLG4zQM";
var yahoo_conversion_value = 0;
/* ]]> */
</script>
<script type="text/javascript" src="http://i.yimg.jp/images/listing/tool/cv/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="http://b91.yahoo.co.jp/pagead/conversion/1000199666/?value=0&amp;label=mvTHCOGbz1oQiLG4zQM&amp;guid=ON&amp;script=0&amp;disvt=true"/>
</div>
</noscript>

<!-- Google用ALLコンバージョンタグ -->
<!-- Google Code for &#28961;&#26009;&#12459;&#12454;&#12531;&#12475;&#12522;&#12531;&#12464; Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 952358852;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "w9KvCJzVk1sQxK-PxgM";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/952358852/?label=w9KvCJzVk1sQxK-PxgM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>

<!-- YDN用コンバージョンタグ -->
<script type="text/javascript" language="javascript">
  /* <![CDATA[ */
  var yahoo_ydn_conv_io = "CBcYrMcOLDV5La8LsXd4";
    var yahoo_ydn_conv_label = "7CBX7PAPKV11BTQA9G1Q33653";
    var yahoo_ydn_conv_transaction_id = "";
      var yahoo_ydn_conv_amount = "0";
      /* ]]> */
    </script>
<script type="text/javascript" language="javascript" charset="UTF-8" src="//b90.yahoo.co.jp/conv.js"></script>

<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
    document,'script','https://connect.facebook.net/en_US/fbevents.js');

fbq('init', '613338675499217');
fbq('track', "PageView");</script>
    <noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=613338675499217&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->

<!-- Kaiu Marketing -->
<script type='text/javascript'
src='https://kaiu-marketing.com/visitor/conversion/cv_script.js?site_code=cf2c10d6274fed5a734c&key=314651bab7cec8d62af6&secret=8b218e26f1c4ca3ec8f1'></script>

<!-- EBiS tag version2.11 start -->
<script type="text/javascript">
<!--
if ( location.protocol == 'http:' ){
    strServerName = 'http://ac.ebis.ne.jp';
} else {
    strServerName = 'https://ac.ebis.ne.jp';
}
cid = 'GPfVm9hG'; pid = 'reserve'; m1id=''; a1id=''; o1id='<?php echo $name ?>'; o2id=''; o3id=''; o4id=''; o5id='';
document.write("<scr" + "ipt type=\"text\/javascript\" src=\"" + strServerName + "\/ebis_tag.php?cid=" + cid + "&pid=" + pid + "&m1id=" + m1id +
"&a1id=" + a1id + "&o1id=" + o1id + "&o2id=" + o2id + "&o3id=" + o3id + "&o4id=" + o4id + "&o5id=" + o5id + "\"><\/scr" + "ipt>");
// -->
</script>
<noscript>
<img src="https://ac.ebis.ne.jp/log.php?argument=GPfVm9hG&ebisPageID=reserve&ebisMember=&ebisAmount=&ebisOther1=&ebisOther2=&ebisOther3=&ebisOther4=&ebisOther5=" width="0" height="0">
</noscript>
<!-- EBiS tag end -->

<!-- Twitter universal website tag code -->
<script>
!function(e,t,n,s,u,a){e.twq||(s=e.twq=function(){s.exe?s.exe.apply(s,arguments):s.queue.push(arguments);
},s.version='1.1',s.queue=[],u=t.createElement(n),u.async=!0,u.src='//static.ads-twitter.com/uwt.js',
a=t.getElementsByTagName(n)[0],a.parentNode.insertBefore(u,a))}(window,document,'script');
// Insert Twitter Pixel ID and Standard Event data below
twq('init','nwbnz');
twq('track','PageView');
</script>
<!-- End Twitter universal website tag code -->


<!-- }}} -->

