<?php
/* vim:se et ts=4 sw=4 sts=4 */

class Reservation_Index
{

    private $head = array(
        'pc' => array(
            'title'       => '予約フォーム',
            'keywords'    => '無料カウンセリング,施術に関するご相談,お悩み相談,クリニックに関する事',
            'description' => '施術に関するご相談から、お悩み相談、クリニックに関する事など、なんなりとお気軽にご相談下さい。',
        ),
        'sp' => array(
            'title'       => '予約フォーム',
            'keywords'    => '無料カウンセリング,施術に関するご相談,お悩み相談,クリニックに関する事',
            'description' => '施術に関するご相談から、お悩み相談、クリニックに関する事など、なんなりとお気軽にご相談下さい。',
        ),
    );

    private $default_message = '例）シミとしわが目立つようになったので、レーザー治療を考えています。肌が弱い人でもレーザー治療は可能でしょうか？';

    // {{{ init
    public function init()
    {
        $this->e = array(
            'name' => '',
            'kana' => '',
            'age' => '',
            'tel' => '',
            'email' => '',
            'email_con' => '',
            'datetime1' => '',
            'datetime2' => '',
            'datetime3' => '',
            'parts' => '',
        );
        $datetime = $this->datetime();
        render_set(array(
            'footer_name' => $this->footername(),
            'dates' => $datetime['list']['dates'],
            'times' => $datetime['list']['times'],
            'html' => array(
                'head' => spdir() ? $this->head['sp'] : $this->head['pc'],
            ),
            'f' => lib('form')->config(array(
                'except_values' => 'action,action_x,action_y',
                'separator' => '',
                'preset_values' => array(
                    'pref' => array('' => '選択してください') + conf('app/prefecture'),
                    'parts' => conf('app/parts'),
                    'reason' => array('' => '選択してください') + conf('app/reason'),
                    'date1' => array('' => '選択してください') + $datetime['date'],
                    'date2' => array('' => '選択してください') + $datetime['date'],
                    'date3' => array('' => '選択してください') + $datetime['date'],
                    'time1' => array('' => '選択してください') + $datetime['time'][1],
                    'time2' => array('' => '選択してください') + $datetime['time'][2],
                    'time3' => array('' => '選択してください') + $datetime['time'][3],
                ),
            ))
        ), FALSE);
    }
    // }}}

    // {{{ action
    public function form()
    {
        lib('form')->values(array(
            'name' => '',
            'kana' => '',
            'age' => '',
            'zip1' => '',
            'zip2' => '',
            'pref' => '',
            'addr1' => '',
            'addr2' => '',
            'tel1' => '',
            'tel2' => '',
            'tel3' => '',
            'email' => '',
            'email_con' => '',
            'reason' => '',
            'parts' => '',
            'message' => '',
            'date1' => '',
            'date2' => '',
            'date3' => '',
            'time1' => '',
            'time2' => '',
            'time3' => '',
        ));
        render_name('reservation/.form');
        render_set(array(
            'e' => $this->e,
        ), FALSE);
    }

    public function confirm()
    {
        $data = lib('request')->post();
        if (spdir()) {
            $data['message'] = str_replace($this->default_message, '', $data['message']);
        }
        lib('form')->values($data);
        $valid = array();
        $error = '';
        if ($this->validate($data, $valid, $error)) {
            // next to confirm
            render_name('reservation/.confirm');
        } else {
            // back to form
            $e = $error;
            render_name('reservation/.form');
            render_set(array(
                'e' => $e + $this->e,
            ), FALSE);
        }
    }

    public function execute()
    {
        $data = lib('request')->post();
        if (isset($data['action']['send'])) {
            // send mail
            unset($data['action']);
            $mail = $this->prepare($data);
            $this->sendmail($mail);
            redirect_to(request_uri(spdir() . '/reservation/complete?name=' . urlencode($data['name'])));
        } else {
            // back to form
            render_name('reservation/.form');
            render_set(array(
                'e' => $this->e,
                'f' => lib('form')->values($data),
            ), FALSE);
        }
    }

    public function complete()
    {
        render_name('reservation/.complete');
        render_set(array(
            'name' => lib('request')->get('name'),
        ));
    }
    // }}}

    // {{{ tool
    private function sendmail($mail)
    {
        $system_mail = conf('app/system_mail_from');
        mb_language('ja');
        mb_internal_encoding('UTF-8');
        foreach ($mail as $data) {
            $head = 'From: ' . $system_mail;
            if (! empty($data['header'])) {
                foreach ($data['header'] as $key => $val) {
                    $head .= "\r\n$key: " . $val;
                }
            }
            $param = '-f' . $system_mail;
            $email = $data['email'];
            $subject = mb_encode_mimeheader($data['subject'], 'JIS', 'B', "\n");
            $message = mb_convert_encoding($data['message'], 'JIS', 'UTF-8');
            mail($email, $subject, $message, $head, $param);
            //debug_mail($data);
        }
    }

    private function prepare($data)
    {
        $data['entry_date'] = date('Y/m/d H:i:s');
        $conf = $this->mailtemplate();
        $mail = array();
        $admin = array(
            'subject' => $conf['admin']['subject'],
            'message' => $conf['admin']['message'],
        );
        $user = array(
            'subject' => $conf['user']['subject'],
            'message' => $conf['user']['message'],
        );
        if (spdir()) {
            $data['zip'] = '';
            $data['pref'] = '';
            $data['addr1'] = '';
            $data['addr2'] = '';
            $data['tel'] = $data['tel1'];
        } else {
            $data['zip'] = $data['zip1'] . '-' . $data['zip2'];
            $data['pref'] = conf('app/prefecture/' . $data['pref']);
            $data['tel'] = $data['tel1'] . '-' . $data['tel2'] . '-' . $data['tel3'];
        }
        $data['reason'] = conf('app/reason/' . $data['reason']);
        if (! empty($data['parts'])) {
            $tmp = array();
            foreach ($data['parts'] as $val) {
                $tmp[] = conf('app/parts/' . $val);
            }
            $data['parts'] = implode(', ', $tmp);
        } else {
            $data['parts'] = '';
        }
        foreach ($data as $key => $val) {
            $key = '{' . $key . '}';
            $admin['subject'] = str_replace($key, $val, $admin['subject']);
            $admin['message'] = str_replace($key, $val, $admin['message']);
            $user['subject'] = str_replace($key, $val, $user['subject']);
            $user['message'] = str_replace($key, $val, $user['message']);
        }
        $mail[] = array(
            'email' => $data['email'],
            'subject' => $user['subject'],
            'message' => $user['message'],
        );
        foreach (explode(',', $conf['admin']['email']) as $addr) {
            $mail[] = array(
                'email' => $addr,
                'subject' => $admin['subject'],
                'message' => $admin['message'],
                'header' => array(
                    'Reply-To' => $data['email'],
                ),
            );
        }
        return $mail;
    }

    /**
     * Get mail template
     */
    private function mailtemplate()
    {
        $conf = conf('app/reservation');
        $dir  = dirname(ZIU_MAIN_PATH) . DS . 'templates';
        $file_admin = $dir . '/mail_reservation_admin.php';
        $file_user  = $dir . '/mail_reservation_user.php';
        if (is_readable($file_admin)) {
            $conf['admin'] = include $file_admin;
        }
        if (is_readable($file_user)) {
            $conf['user'] = include $file_user;
        }
        return $conf;
    }

    /**
     * Validate data
     * @param array  $data   : validate data
     * @param array  &$valid : valid data
     * @param array  &$error : error object list
     * @return boolean
     */
    private function validate(array $data, &$valid, &$error)
    {
        $error = array();
        $label = array(
            'name' => '名前',
            'kana' => 'フリガナ',
            'age'  => '年齢',
            'tel1' => '電話番号',
            'tel2' => '電話番号',
            'tel3' => '電話番号',
            'email' => '連絡先メールアドレス',
            'email_con' => '上記と同じメールアドレス',
            'date1' => '第一希望',
            'date2' => '第二希望',
            'date3' => '第三希望',
            'time1' => '第一希望',
            'time2' => '第二希望',
            'time3' => '第三希望',
            'parts' => 'ご相談箇所',
        );
        foreach ($data as $key => $val) {
            switch ($key) {
                case 'name' :
                case 'age' :
                case 'email' :
                case 'email_con' :
                    if (empty($data[$key])) {
                        $error[$key] = "{$label[$key]}を入力してください";
                    }
                    break;
                case 'kana' :
                    if (empty($val)) {
                        $error[$key] = "{$label[$key]}を入力してください";
                    } elseif (! preg_match('/^[ァ-ヾー　 ]+$/u', $val)) {
                        $error[$key] = "カタカナで入力してください";
                    }
                    break;
                case 'tel1' :
                case 'tel2' :
                case 'tel3' :
                    if (empty($data[$key])) {
                        $error['tel'] = "{$label[$key]}を入力してください";
                    }
                    break;
                default :
            }
        }
        if (empty($data['parts'])) {
            $error['parts'] = "{$label['parts']}を1つ以上選択してください";
        }
        if (! spdir()) {
            if ($data['email'] != $data['email_con']) {
                $error['email_con'] = "{$label['email_con']}を入力してください";
            }
        }
        $res = empty($error);
        return $res;
    }

    /**
     * Parse datetime selector
     */
    private function datetime()
    {
        $date = array();
        $work = conf('app/working');
        $week = array('日','月','火','水','木','金','土');
        $s = conf('app/prefered/date/start');
        $e = conf('app/prefered/date/end');
        for ($i = $s; $i <= $e; $i++) {
            $ymd = date('Ymd', strtotime("+$i days"));
            if ($ymd >= 20161230 && 20170106 >= $ymd) {
                continue;
            }
            list($d, $w) = explode(':', date('Y年n月j日:w', strtotime("+$i days")));
            if (! empty($work[$w])) {
                // 営業日のみ表示
                $d = $d . "({$week[$w]})";
                $date[$d] = $d;
                $dates[$d] = $w;
            }
        }
        foreach ($work as $key => $val) {
            if (empty($val)) {
                $times[$key] = array();
            } else {
                list($s_h, $s_m) = explode(':', $val[0]);
                list($e_h, $e_m) = explode(':', $val[1]);
                $s_m = $s_m == 30 ? '50' : '00'; // 30 to 50
                $e_m = $e_m == 30 ? '50' : '00'; // 30 to 50
                $start = (sprintf("%02d", $s_h) . sprintf("%02d", $s_m)); // + 100; // 1時間後(100)から予約開始
                $end   = (sprintf("%02d", $e_h) . sprintf("%02d", $e_m)); //  - 50; // 30分前(50)で予約終了
                while (TRUE) {
                    if ($start > $end) {
                        break(1);
                    }
                    $tmp = sscanf(sprintf("%04d", $start), "%02d%02d");
                    $tmp[1] = $tmp[1] == 50 ? '30' : '00'; // 50 to 30
                    $time = sprintf("%02d:%02d", $tmp[0], $tmp[1]);
                    $times[$key][$time] = $time;
                    $start += 50;
                }
            }
        }
        $date1 = lib('request')->post('date1');
        $date2 = lib('request')->post('date2');
        $date3 = lib('request')->post('date3');
        $time1 = isset($dates[$date1]) ? $times[$dates[$date1]] : array();
        $time2 = isset($dates[$date2]) ? $times[$dates[$date2]] : array();
        $time3 = isset($dates[$date3]) ? $times[$dates[$date3]] : array();
        return array(
            'date' => $date,
            'time' => array(
                1 => $time1,
                2 => $time2,
                3 => $time3,
            ),
            'list' => array(
                'dates' => $dates,
                'times' => $times,
            ),
        );
    }

    /**
     * Get footer_name
     */
    private function footername()
    {
        $footer_name = '';
        if (spdir()) {
            switch (basename(main_action())) {
                case 'complete' :
                    $footer_name = 'footer_reservation_complete';
                    break;
                default :
                    $footer_name = 'footer_copy';
            }
        } else {
            $footer_name = 'form_footer';
        }
        return $footer_name;
    }
    // }}}

}

