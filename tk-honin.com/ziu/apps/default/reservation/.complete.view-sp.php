
<!--mArea-->
<div class="mArea" id="reservation_complete">

  <div class="h1_sec">
    <h1>無料カウンセリング予約</h1>
  </div><!--/h1_sec-->

<ul id="steplist">
<li>お客様情報の<br>入力</li>
<li>内容の<br>ご確認</li>
<li class="active">送信完了</li>
</ul>

  <div class="box-bdr center">
    <p class="txt-strong">カウンセリング予約完了</p>
    <p class="txt">カウンセリングをご予約いただき、<br/>
誠にありがとうございました。 <br/>
<br/>
予約状況をご確認の上、<br/>
お電話またはメールにて、<br/>
折り返し日時のご連絡をさせていただきます。<br/>
少々お待ちいただけますようお願い申し上げます。<br/>
<br/>
また、お急ぎのお客様は、<br/>
大変お手数ですがお電話にて<br/>
ご連絡をいただけますようお願いいたします。</p>
  </div>

  <div class="img tac mt60"><a href="tel:0120334270" onClick="ga('send', 'event', 'sp', 'tel-tap', 'tel');"><img src="../common/images/info-tel.png" alt="電話番号　0120-334-270"></a></div>
</div>

  <div class="buttonStepInfoBlock">
    <a href="../flow/flow.html"><span>施術の流れを見る</span></a>
  </div>
  <div class="buttonStaffInfoBlock">
    <a href="../about/doctor.html"><span>医師の紹介を見る</span></a>
  </div>
  <div class="backHomeBlock">
    <a href="../index.html"><span>Topページに戻る</span></a>
  </div><!--/backHomeBlock-->
<!--/mArea-->

<!-- {{{ 計測タグ -->
<!-- Yahoo用コンバージョンタグ -->
<!-- Yahoo Code for your Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var yahoo_conversion_id = 1000199666;
var yahoo_conversion_label = "mvTHCOGbz1oQiLG4zQM";
var yahoo_conversion_value = 0;
/* ]]> */
</script>
<script type="text/javascript" src="http://i.yimg.jp/images/listing/tool/cv/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="http://b91.yahoo.co.jp/pagead/conversion/1000199666/?value=0&amp;label=mvTHCOGbz1oQiLG4zQM&amp;guid=ON&amp;script=0&amp;disvt=true"/>
</div>
</noscript>

<!-- Google用ALLコンバージョンタグ -->
<!-- Google Code for &#28961;&#26009;&#12459;&#12454;&#12531;&#12475;&#12522;&#12531;&#12464; Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 952358852;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "w9KvCJzVk1sQxK-PxgM";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/952358852/?label=w9KvCJzVk1sQxK-PxgM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>

<!-- YDN用コンバージョンタグ -->
<script type="text/javascript" language="javascript">
  /* <![CDATA[ */
  var yahoo_ydn_conv_io = "CBcYrMcOLDV5La8LsXd4";
    var yahoo_ydn_conv_label = "7CBX7PAPKV11BTQA9G1Q33653";
    var yahoo_ydn_conv_transaction_id = "";
      var yahoo_ydn_conv_amount = "0";
      /* ]]> */
    </script>
<script type="text/javascript" language="javascript" charset="UTF-8" src="//b90.yahoo.co.jp/conv.js"></script>

<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
    document,'script','https://connect.facebook.net/en_US/fbevents.js');

fbq('init', '613338675499217');
fbq('track', "PageView");</script>
    <noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=613338675499217&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->

<!-- EBiS tag version2.11 start -->
<script type="text/javascript">
<!--
if ( location.protocol == 'http:' ){
    strServerName = 'http://ac.ebis.ne.jp';
} else {
    strServerName = 'https://ac.ebis.ne.jp';
}
cid = 'GPfVm9hG'; pid = 'reserve'; m1id=''; a1id=''; o1id='<?php echo $name ?>'; o2id=''; o3id=''; o4id=''; o5id='';
document.write("<scr" + "ipt type=\"text\/javascript\" src=\"" + strServerName + "\/ebis_tag.php?cid=" + cid + "&pid=" + pid + "&m1id=" + m1id +
"&a1id=" + a1id + "&o1id=" + o1id + "&o2id=" + o2id + "&o3id=" + o3id + "&o4id=" + o4id + "&o5id=" + o5id + "\"><\/scr" + "ipt>");
// -->
</script>
<noscript>
<img src="https://ac.ebis.ne.jp/log.php?argument=GPfVm9hG&ebisPageID=reserve&ebisMember=&ebisAmount=&ebisOther1=&ebisOther2=&ebisOther3=&ebisOther4=&ebisOther5=" width="0" height="0">
</noscript>
<!-- EBiS tag end -->

<!-- Twitter universal website tag code -->
<script>
!function(e,t,n,s,u,a){e.twq||(s=e.twq=function(){s.exe?s.exe.apply(s,arguments):s.queue.push(arguments);
},s.version='1.1',s.queue=[],u=t.createElement(n),u.async=!0,u.src='//static.ads-twitter.com/uwt.js',
a=t.getElementsByTagName(n)[0],a.parentNode.insertBefore(u,a))}(window,document,'script');
// Insert Twitter Pixel ID and Standard Event data below
twq('init','nwbnz');
twq('track','PageView');
</script>
<!-- End Twitter universal website tag code -->


<!-- }}} -->

