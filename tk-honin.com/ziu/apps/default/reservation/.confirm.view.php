				<div class="inner660">
					<h1 class="pageTitle"><img src="./images/res_title.gif" alt="無料カウンセリング予約" /></h1>
					<div class="pageInfo">当クリニックへのご質問、ご相談をお受けしています。<br />
					施術に関するご相談から、お悩み相談、クリニックに関する事など、何なりとお気軽にお問い合わせ下さい。</div>

					<p class="pt25">表参道スキンクリニックでは随時、専門医による無料カウンセリングを行っております。ご希望の方は、下記のフォームにご来院希望日など必要事項を入力の上、送信をお願いいたします。予約フォームを受信しましたら、折り返し電話またはメールにて確認のご連絡をさせていただきます。<br />
					なお、ご質問やご相談は電話でも承っておりますので、お急ぎの方はこちらをご利用くださいませ。<br />
					<span class="cPink01 fb fs13">フリーダイヤル：0120‐334‐270</span></p>

					<div class="formType01">
                        <?php echo $f->open('../reservation/execute') ?>
                        <?php echo $f->hidden() ?>
							<table>
								<tr>
									<th>お名前【必須】</th>
									<td class="fb">
                                        <?php echo $f->fetch('name') ?><br />
                                        <?php echo $f->fetch('kana') ?>
                                    </td>
								</tr>
								<tr>
									<th>年齢【必須】</th>
									<td class="fb"><?php echo $f->fetch('age') ?>&nbsp;歳</td>
								</tr>
								<tr>
									<th>郵便番号</th>
									<td class="fb"><?php echo $f->fetch('zip1') ?>-<?php echo $f->fetch('zip2') ?></td>
								</tr>
								<tr>
									<th>都道府県</th>
									<td class="fb"><?php echo conf('app/prefecture/' . $f->fetch('pref')) ?></td>
								</tr>
								<tr>
									<th>市区町村</th>
									<td class="fb"><?php echo $f->fetch('addr1') ?></td>
								</tr>
								<tr>
									<th>丁目番地</th>
									<td class="fb"><?php echo $f->fetch('addr2') ?></td>
								</tr>
								<tr>
									<th>電話番号【必須】</th>
									<td class="fb"><?php echo $f->fetch('tel1') ?>-<?php echo $f->fetch('tel2') ?>-<?php echo $f->fetch('tel3') ?></td>
								</tr>
								<tr>
									<th>メールアドレス</th>
									<td class="fb"><?php echo $f->fetch('email') ?></td>
								</tr>
								<tr>
									<th>当クリニックを<br />どこで知りましたか？</th>
									<td class="fb"><?php echo conf('app/reason/' . $f->fetch('reason')) ?></td>
								</tr>
								<tr>
									<th>ご相談箇所【必須】<br />（複数選択可能）</th>
									<td class="fb"><?php echo parts($f->fetch('parts')) ?></td>
								</tr>
								<tr>
									<th>ご相談内容</th>
									<td><?php echo nl2br($f->fetch('message')) ?></td>
								</tr>
								<tr>
									<th>ご相談希望日時</th>
									<td>
										<ul class="clearfix hopeTime">
											<li class="m01">第一希望</li>
											<li class="m02 check"><span><?php echo $f->fetch('date1') ?></span></li>
											<li class="m03"><span><?php echo $f->fetch('time1') ?></span></li>
										</ul>
										<ul class="clearfix hopeTime">
											<li class="m01">第二希望</li>
											<li class="m02 check"><span><?php echo $f->fetch('date2') ?></span></li>
											<li class="m03"><span><?php echo $f->fetch('time2') ?></span></li>
										</ul>
										<ul class="clearfix hopeTime">
											<li class="m01">第三希望</li>
											<li class="m02 check"><span><?php echo $f->fetch('date3') ?></span></li>
											<li class="m03"><span><?php echo $f->fetch('time3') ?></span></li>
										</ul>
									</td>
								</tr>
							</table>
							<p class="tac">
                                <?php echo $f->image('action[back]')->attributes(array('src' => './images/btn_entry_back.gif', 'alt' => '入力画面に戻る')) ?>
                                <?php echo $f->image('action[send]')->attributes(array('src' => './images/btn_entry_submit.gif', 'alt' => '送信', 'class' => 'ml25')) ?>
							</p>
						<?php echo $f->close() ?>
					<!-- /formType01 --></div>


					<div class="breadCrumb">
                        <p id="pagetop"><a href="#header"><img src="../images/btn_pagetop.gif" class="rollover" alt="ページ上部へ戻る" /></a></p>
						<ul>
							<li><a href="../">TOP</a></li>
							<li>&gt;</li>
							<li>予約フォーム確認</li>
						</ul>
					<!-- /breadCrumb --></div>
                    <div id="backtotop">
                        <a href="../index.html"><img src="../images/btn_backtop.gif" height="25" width="130" alt="Topページに戻る" /></a>
                    </div>


				<!-- /inner660 --></div>
