
<?php content_for('javascript') ?>
<script type="text/javascript">
$(document).ready(function(){
    $('select').selectmenu();
}); 
</script>   
<script type="text/javascript">
var timecon = {
    dates: {},
    times: {},
    init: function(){
<?php foreach ($dates as $key => $val) : ?>
        this.dates['<?php echo($key); ?>'] = <?php echo($val); ?>;
<?php endforeach; ?>
<?php foreach ($times as $key => $val) : ?>
        this.times[<?php echo($key); ?>] = [];
<?php if (! empty($val)) : ?>
<?php foreach ($val as $time) : ?>
        this.times[<?php echo($key); ?>].push('<?php echo($time); ?>');
<?php endforeach; ?>
<?php endif; ?>
<?php endforeach; ?>
        this.listen();
    },
    load: function(o){
        var num = o.attr('name').replace('date', '');
        var tar = "time" + num;
        var week = timecon.dates[o.val()];
        var html = '<option value="">選択してください</option>';
        if (timecon.times[week]) {
            for (var i = 0; i < timecon.times[week].length; i++) {
                var time = timecon.times[week][i];
                html += '<option value="' + time + '">' + time + '</option>';
            }
        }
        $('select[name="' + tar + '"]').selectmenu('destroy');
        $('select[name="' + tar + '"]').html(html);
        $('select[name="' + tar + '"]').selectmenu();
    },
    listen: function(){
        $('select[name^="date"]').selectmenu({
            change: function(e, ui){
                console.log('gaga');
                timecon.load($(this));
            }
        });
    }
};
$(function(){
    timecon.init();
});
</script>
<?php content_end_for('javascript') ?>

<!--mArea-->
<div class="mArea" id="reservation_form">
  <div class="h1_sec">
    <h1>無料カウンセリング予約</h1>
    <p>当クリニックへのご質問、ご相談をお受けしています。<br>施術に関するご相談から、お悩み相談、クリニックに関する事など、何なりとお気軽にお問い合わせ下さい。</p><br />
<p class="cPink01 fb fs15"><span>※未成年の方は親権者の方の同意が必要ですので下記より書類をダウンロードして頂きご持参下さい。</span>
					<br><a href="http://tk-honin.com/agreement.pdf">未成年者施術承諾書pdfダウンロード</a></p>
  <div class="bd_t">
    <p>表参道スキンクリニックでは随時、専任のカウンセラーによる無料カウンセリングの後専門医による診察（初診料3000円／税別）を行なっております。ご希望の方は、下記のフォームにご来院希望日など必要事項を入力の上、送信をお願いいたします。予約フォームを受信しましたら、折り返し電話またはメールにて確認のご連絡をさせていただきます。<br>
なお、ご質問やご相談は電話でも承っておりますので、お急ぎの方はこちらをご利用くださいませ。
</p>
  </div><!--/innerBlock-->
  </div><!--/h1_sec-->
  <div class="img tac"><a href="tel:0120334270" onClick="ga('send', 'event', 'sp', 'tel-tap', 'tel');"><img src="../common/images/form_tel.jpg" alt="TEL:0120-334-270　完全予約制 受付時間 11時 － 20時" width="580" height="204"></a></div><!--/sec-->
    
<ul id="steplist">
<li class="active">お客様情報の<br>入力</li>
<li>内容の<br>ご確認</li>
<li>送信完了</li>
</ul>
<p class="noticeTxt-s">※ 大変お手数ですが必須項目はすべてご記入頂き、<br/>「確認画面へ」ボタンを押してください。</p>

<?php echo $f->open('../reservation/confirm')->attributes(array('class' => 'niceform')) ?>

  <div class="sec" id="form">
    <div class="formBlock lh1_6">
        <dl>
          <dt><span class="icon-red">必須</span>お名前</dt>
          <dd class="name">
            <div class="clearfix">
                <span class="form_tt">お名前</span>
              <div class="form_colr">
                <p><?php echo $f->text('name')->attributes(array('class' => 'mst', 'placeholder' => '例） 表参道　花子')) ?></p>
<?php if ($e['name']) :?>
                <p class="error db" title="名前"><?php echo $e['name'] ?></p>
<?php else : ?>
                <p class="error" title="名前">名前を入力してください</p>
<?php endif; ?>
              </div>
            </div>
          
            <div class="item clearfix">
                <span class="form_tt">フリガナ</span>
              <div class="form_colr">
                <p><?php echo $f->text('kana')->attributes(array('class' => 'mst', 'placeholder' => '例） オモテサンドウ　ハナコ')) ?></p>
<?php if ($e['kana']) :?>
                <p class="error db" title="フリガナ"><?php echo $e['kana'] ?></p>
<?php else : ?>
                <p class="error" title="フリガナ">フリガナを入力してください</p>
<?php endif; ?>
              </div>
            </div>
            
          </dd>
          <dt><span class="icon-red">必須</span>年齢</dt>
          <dd class="age">
            <p><?php echo $f->number('age')->attributes(array('class' => 'num mst', 'min' => 0)) ?>&nbsp;歳</p>
<?php if ($e['age']) :?>
                <p class="error db" title="年齢"><?php echo $e['age'] ?></p>
<?php else : ?>
                <p class="error" title="年齢">年齢を入力してください</p>
<?php endif; ?>
          </dd>
          <dt><span class="icon-red">必須</span>電話番号</dt>
          <dd class="tel">
            <p><?php echo $f->text('tel1')->attributes(array('class' => 'mst num', 'placeholder' => '例）03-1234-5678', 'style' => 'ime-mode: disabled;')) ?></p>
<?php if ($e['tel']) :?>
                <p class="error db" title="電話番号"><?php echo $e['tel'] ?></p>
<?php else : ?>
                <p class="error" title="電話番号">電話番号を入力してください</p>
<?php endif; ?>
          </dd>
          <dt><span class="icon-red">必須</span>連絡先メールアドレス</dt>
          <dd class="mail">
            <p><?php echo $f->email('email')->attributes(array('class' => 'mst', 'placeholder' => '例）hanako@omotesando.co.jp')) ?></p>
<?php if ($e['email']) :?>
                <p class="error db" title="連絡先メールアドレス"><?php echo $e['email'] ?></p>
<?php else : ?>
                <p class="error" title="連絡先メールアドレス">連絡先メールアドレスを入力してください</p>
<?php endif; ?>
          </dd>
          <dt><span class="icon-blue">任意</span>当クリニックをどこで知りましたか？</dt>
          <dd class="question small">
            <p><a href="javascript:void(0)"><?php echo $f->select('reason') ?></a></p>
          </dd>
          <dt><span class="icon-red">必須</span>ご相談箇所　（複数選択可能）</dt>
          <dd class="checkchoice">
            <p class="clearfix"><?php echo parts_html_for_sp($f->checkbox('parts')->outline(11, 2, array('class' => 'parts'))) ?></p>
<?php if ($e['parts']) :?>
                <p class="error db" title="ご相談箇所"><?php echo $e['parts'] ?></p>
<?php else : ?>
                <p class="error" title="ご相談箇所">ご相談箇所を指定してください</p>
<?php endif; ?>
          </dd>
          <dt class="content"><span class="icon-blue">任意</span>ご相談内容</dt>
          <dd>
               <?php $message = (string)$f->fetch('message'); ?>
            <p><?php echo $f->textarea('message')->attributes(array('rows' => '7', 'class' => ($message ? '' : 'sampletext'))) ?></p>
          </dd>
          <dt><span class="icon-blue">任意</span>ご相談希望日時</dt>
          <dd class="wishdate">
             <div class="wishBox">
                <p class="tit">第一希望</p>
                <div class="wishBoxInner first clearfix">
                    <label>日付</label>
                    <a href="javascript:void(0)" class="clearfix"><?php echo $f->select('date1') ?></a>
                 </div>
                 <div class="wishBoxInner clearfix">
                    <label>時間</label>
                    <a href="javascript:void(0)" class="clearfix"><?php echo $f->select('time1') ?></a>
                 </div>
            </div>
            <div class="wishBox">
                <p class="tit">第二希望</p>
                <div class="wishBoxInner first clearfix">
                    <label>日付</label>
                    <a href="javascript:void(0)" class="clearfix"><?php echo $f->select('date2') ?></a>
                 </div>
                 <div class="wishBoxInner clearfix">
                    <label>時間</label>
                    <a href="javascript:void(0)" class="clearfix"><?php echo $f->select('time2') ?></a>
                 </div>
            </div>
            <div class="wishBox last">
                <p class="tit">第三希望</p>
                <div class="wishBoxInner first clearfix">
                    <label>日付</label>
                    <a href="javascript:void(0)" class="clearfix"><?php echo $f->select('date3') ?></a>
                 </div>
                 <div class="wishBoxInner clearfix">
                    <label>時間</label>
                    <a href="javascript:void(0)" class="clearfix"><?php echo $f->select('time3') ?></a>
                 </div>
            </div>
            <p class="noticeTxt">※ご予約を承った際、改めて当クリニックからお電話かメールで、ご希望日をご確認させて頂く場合がございます。</p>
          </dd> 
        </dl>

    </div>
  </div><!--/sec-->

  <div class="formBtn"><?php echo $f->submit('action', '確認画面へ')->attributes(array('id' => 'submit')) ?></div>

<?php echo $f->close() ?>
  
</div>
<!--/mArea-->

<!-- Twitter universal website tag code -->
<script>
!function(e,t,n,s,u,a){e.twq||(s=e.twq=function(){s.exe?s.exe.apply(s,arguments):s.queue.push(arguments);
},s.version='1.1',s.queue=[],u=t.createElement(n),u.async=!0,u.src='//static.ads-twitter.com/uwt.js',
a=t.getElementsByTagName(n)[0],a.parentNode.insertBefore(u,a))}(window,document,'script');
// Insert Twitter Pixel ID and Standard Event data below
twq('init','nwbnz');
twq('track','PageView');
</script>
<!-- End Twitter universal website tag code -->