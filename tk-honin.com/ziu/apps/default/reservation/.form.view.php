<script type="text/javascript">
var timecon = {
    dates: {},
    times: {},
    init: function(){
<?php foreach ($dates as $key => $val) : ?>
        this.dates['<?php echo($key); ?>'] = <?php echo($val); ?>;
<?php endforeach; ?>
<?php foreach ($times as $key => $val) : ?>
        this.times[<?php echo($key); ?>] = [];
<?php if (! empty($val)) : ?>
<?php foreach ($val as $time) : ?>
        this.times[<?php echo($key); ?>].push('<?php echo($time); ?>');
<?php endforeach; ?>
<?php endif; ?>
<?php endforeach; ?>
        this.listen();
    },
    load: function(o){
        var num = o.attr('name').replace('date', '');
        var tar = "time" + num;
        var week = timecon.dates[o.val()];
        var html = '<option value="">選択してください</option>';
        if (timecon.times[week]) {
            for (var i = 0; i < timecon.times[week].length; i++) {
                var time = timecon.times[week][i];
                html += '<option value="' + time + '">' + time + '</option>';
            }
        }
        $('select[name="' + tar + '"]').html(html);
    },
    listen: function(){
        $('select[name^="date"]').change(function(){
            timecon.load($(this));
        });
    }
};
$(function(){
    timecon.init();
})
</script>
<script src="https://ajaxzip3.github.io/ajaxzip3.js" charset="UTF-8"></script>
				<div class="inner660">
					<h1 class="pageTitle"><img src="./images/res_title.gif" alt="無料カウンセリング予約" /></h1>
					<div class="pageInfo">当クリニックへのご質問、ご相談をお受けしています。<br />
					施術に関するご相談から、お悩み相談、クリニックに関する事など、何なりとお気軽にお問い合わせ下さい。</div>

                    <p class="pt25 mb10">表参道スキンクリニックでは随時、専門医による無料カウンセリングを行っております。ご希望の方は、下記のフォームにご来院希望日など必要事項を入力の上、送信をお願いいたします。予約フォームを受信しましたら、折り返し電話またはメールにて確認のご連絡をさせていただきます。<br />
                    なお、ご質問やご相談は電話でも承っておりますので、お急ぎの方はこちらをご利用くださいませ。</p>
                    <p class="cPink01 fb fs15"><span>フリーダイヤル：0120‐334‐270</span></p><br />
					<p class="cPink01 fb fs15"><span>※未成年の方は親権者の方の同意が必要ですので下記より書類をダウンロードして頂きご持参下さい。</span>
					<br><a href="http://tk-honin.com/agreement.pdf">未成年者施術承諾書pdfダウンロード</a></p>

					<div class="formType01">
                        <?php echo $f->open('../reservation/confirm') ?>
							<table>
								<tr>
									<th>お名前【必須】</th>
									<td>
                                        <p class="mb10">
                                            <span class="adjust">お名前</span><?php echo $f->text('name')->attributes(array('class' => 'txt w315')) ?><br />
                                            <span class="hint"><?php echo $e['name'] ?></span><br />
                                        </p>
                                        <p>
                                            <span class="adjust">フリガナ</span><?php echo $f->text('kana')->attributes(array('class' => 'txt w315')) ?><br />
                                            <span class="hint"><?php echo $e['kana'] ?></span>
                                        </p>
									</td>
								</tr>
								<tr>
									<th>年齢【必須】</th>
                                    <td><?php echo $f->text('age')->attributes(array('class' => 'txt w50')) ?>&nbsp;歳<br />
                                    <span class="hint"><?php echo $e['age'] ?></span>
                                    </td>
								</tr>
								<tr>
									<th>郵便番号</th>
									<td>
                                        <?php echo $f->text('zip1')->attributes(array('class' => 'txt w50')) ?>&nbsp;&nbsp;-&nbsp;
                                        <?php echo $f->text('zip2')->attributes(array('class' => 'txt w50', 'onkeyup' => "AjaxZip3.zip2addr('zip1','zip2','pref','addr1');")) ?>
									</td>
								</tr>
								<tr>
									<th>都道府県</th>
									<td><?php echo $f->select('pref') ?></td>
								</tr>
								<tr>
									<th>市区町村</th>
									<td><?php echo $f->text('addr1')->attributes(array('class' => 'txt w315')) ?><br />
									<span class="hint hint02">例）新宿区西新宿</span>
									</td>
								</tr>
								<tr>
									<th>丁目番地</th>
									<td><?php echo $f->text('addr2')->attributes(array('class' => 'txt w315')) ?><br />
									<span class="hint hint02">例）2-4-1　新宿NSビル3F</span>
									</td>
								</tr>
								<tr>
									<th>電話番号【必須】</th>
									<td>
                                        <?php echo $f->text('tel1')->attributes(array('class' => 'txt w70')) ?>&nbsp;&nbsp;-&nbsp;
                                        <?php echo $f->text('tel2')->attributes(array('class' => 'txt w70')) ?>&nbsp;&nbsp;-&nbsp;
                                        <?php echo $f->text('tel3')->attributes(array('class' => 'txt w70')) ?><br />
                                        <span class="hint"><?php echo $e['tel'] ?></span>
									</td>
								</tr>
								<tr>
									<th>連絡先メールアドレス【必須】</th>
									<td><?php echo $f->text('email')->attributes(array('class' => 'txt w315')) ?><br />
                                    <span class="hint"><?php echo $e['email'] ?></span>
									</td>
								</tr>
								<tr>
									<th>連絡先メールアドレス<br />確認用【必須】</th>
									<td><?php echo $f->text('email_con')->attributes(array('class' => 'txt w315')) ?><br />
                                    <span class="hint"><?php echo $e['email_con'] ?></span>
									</td>
								</tr>
								<tr>
									<th>当クリニックを<br />どこで知りましたか？</th>
									<td><?php echo $f->select('reason') ?></td>
								</tr>
								<tr>
									<th>ご相談箇所【必須】<br />（複数選択可能）</th>
                                    <td><?php echo $f->checkbox('parts')->outline(11, 2, array('class' => 'parts')) ?><br />
                                    <span class="hint"><?php echo $e['parts'] ?></span>
                                    </td>
<?php content_for('stylesheet') ?>
<style type="text/css">
table.parts {
    margin-bottom: 0;
}
table.parts tr td {
    border: 0;
    padding: 0;
    width: 145px;
}
</style>
<?php content_end_for('stylesheet') ?>
								</tr>
								<tr>
									<th>ご相談内容</th>
									<td><?php echo $f->textarea('message')->attributes(array('rows' => '7')) ?></td>
								</tr>
								<tr>
									<th>ご相談希望日時</th>
									<td>
										<ul class="clearfix hopeTime">
                                            <li>
                                                <span class="adjust">第一希望</span>
                                                <span>日付</span>
                                                <?php echo $f->select('date1') ?>&nbsp;&nbsp;
                                                <span>時間</span>
                                                <?php echo $f->select('time1') ?>
											</li>
										</ul>
                                        <p class="hint"><?php echo $e['datetime1'] ?></p>
										<ul class="clearfix hopeTime pt10">
                                            <li>
                                                <span class="adjust">第二希望</span>
                                                <span>日付</span>
                                                <?php echo $f->select('date2') ?>&nbsp;&nbsp;
                                                <span>時間</span>
                                                <?php echo $f->select('time2') ?>
											</li>
										</ul>
                                        <p class="hint"><?php echo $e['datetime2'] ?></p>
										<ul class="clearfix hopeTime pt10">
                                            <li>
                                                <span class="adjust">第三希望</span>
                                                <span>日付</span>
                                                <?php echo $f->select('date3') ?>&nbsp;&nbsp;
                                                <span>時間</span>
                                                <?php echo $f->select('time3') ?>
											</li>
										</ul>
                                        <p class="hint"><?php echo $e['datetime3'] ?></p>
                                        <p class="hint02 textIndent01 pt10">※ご予約を承った際、改めて当クリニックからお電話かメールで、ご希望日をご確認させて頂く場合がございます。</p>
									</td>
								</tr>
							</table>
							<p class="tac"><?php echo $f->image('action')->attributes(array('src' => './images/btn_entry_confirm.gif', 'alt' => '入力確認')) ?></p>
						<?php echo $f->close() ?>
					<!-- /formType01 --></div>
					
					<!-- Twitter universal website tag code -->
					<script>
					!function(e,t,n,s,u,a){e.twq||(s=e.twq=function(){s.exe?s.exe.apply(s,arguments):s.queue.push(arguments);
					},s.version='1.1',s.queue=[],u=t.createElement(n),u.async=!0,u.src='//static.ads-twitter.com/uwt.js',
					a=t.getElementsByTagName(n)[0],a.parentNode.insertBefore(u,a))}(window,document,'script');
					// Insert Twitter Pixel ID and Standard Event data below
					twq('init','nwbnz');
					twq('track','PageView');
					</script>
					<!-- End Twitter universal website tag code -->

					<div class="breadCrumb">
                        <p id="pagetop"><a href="#header"><img src="../images/btn_pagetop.gif" class="rollover" alt="ページ上部へ戻る" /></a></p>
						<ul>
							<li><a href="../">TOP</a></li>
							<li>&gt;</li>
							<li>予約フォーム</li>
						</ul>
					<!-- /breadCrumb --></div>
                    <div id="backtotop">
                        <a href="../index.html"><img src="../images/btn_backtop.gif" height="25" width="130" alt="Topページに戻る" /></a>
                    </div>


				<!-- /inner660 --></div>
