<?php

if (! function_exists('debug_mail')) {
    function debug_mail($data)
    {
        l_debug(print_r($data, TRUE));
    }
}

if (! function_exists('request_uri')) {
    function request_uri($uri)
    {
        $contents_path = str_replace($_SERVER['PATH_INFO'], '', getenv('REQUEST_URI'));
        return trim($contents_path, '/') . '/' . ltrim($uri, '/');
    }
}

function parts($parts, $device = 'pc')
{
    $parts = explode(',', (string)$parts);
    $html = array();
    foreach ($parts as $val) {
        $html[] = conf('app/parts/' . $val);
    }
    if ($device == 'sp') {
        return implode("</p><p>", $html);
    } else {
        return implode("<br />", $html);
    }
}

function parts_html_for_sp($html)
{
    $list = array(
        array('<table class="parts">', ''),
        array('<tr><td>', ''),
        array('</td></tr>', ''),
        array('</table>', ''),
        array('<label>', '<label class="clearfix">'),
        array(' />', ' /><span>'),
        array('</label>', '</span></label>'),
    );
    foreach ($list as $val) {
        $html = str_replace($val[0], $val[1], $html);
    }
    return $html;
}

function occupation_html_for_sp($html)
{
    $list = array(
        array('<label>', '<label class="clearfix">'),
        array(' />', ' /><span>'),
        array('</label>', '</span></label>'),
    );
    foreach ($list as $val) {
        $html = str_replace($val[0], $val[1], $html);
    }
    return $html;
}

