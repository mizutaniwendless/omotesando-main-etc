<?php

return array(

    'working' => array(
        // array(start, end), array() is closed
        0 => array('10:30', '18:00'), // Sun
        1 => array('11:00', '20:00'), // Mon
        2 => array('11:00', '20:00'), // Tue
        3 => array(), // Wed
        4 => array('11:00', '20:00'), // Thu
        5 => array('11:00', '20:00'), // Fri
        6 => array('11:00', '20:00'), // Sat
    ),

    // {{{ occupation
    'occupation' => array(
        1 => ' 看護師業務全般',
        2 => ' 受付業務・看護師やカウンセラーのサポート業務',
        3 => ' 美容に関するカウンセリング及びアドバイス等',
        4 => ' エステティシャン業務全般(看護師のサポート含む)',
    ),
    // }}}

    // {{{ parts
    'parts' => array(
        1 => '医療レーザー脱毛',
        2 => '金の糸',
        3 => 'シミ・ほくろ・タトゥー除去',
        4 => 'ダイエット薬・ダイエットサプリ',
        5 => 'スキンケア',
        6 => 'ヒアルロン酸注入・ボトックス注射',
        7 => '痩身',
        8 => 'ピアス',
        9 => 'メンズクリニック',
        10 => '目もと・二重まぶた',
        11 => '鼻筋・小鼻',
        12 => 'フェイスリフト',
        13 => '耳',
        14 => '口元・唇',
        15 => 'バスト・豊胸術',
        16 => '輪郭・アゴ形成',
        17 => '脂肪吸引',
        18 => 'ワキガ・多汗症',
        19 => '女性器',
        20 => '男性器',
        21 => 'その他',
    ),
    // }}}

    // {{{ reason
    'reason' => array(
        1 => 'Yahoo',
        2 => 'Google',
		3 => 'Twitter',
        4 => '友人・知人からの紹介',
        5 => 'フリーペーパーなど',
        6 => 'その他',
    ),
    // }}}

    // {{{ prefered date time
    'prefered' => array(
        'date' => array(
            'start' => 0,  // future start day
            'end'   => 20, // future end day
        ),
    ),
    // }}}

    // {{{ prefecture
    'prefecture' => array(
        1 => '北海道',
        2 => '青森県',
        3 => '岩手県',
        4 => '宮城県',
        5 => '秋田県',
        6 => '山形県',
        7 => '福島県',
        8 => '茨城県',
        9 => '栃木県',
        10 => '群馬県',
        11 => '埼玉県',
        12 => '千葉県',
        13 => '東京都',
        14 => '神奈川県',
        15 => '新潟県',
        16 => '富山県',
        17 => '石川県',
        18 => '福井県',
        19 => '山梨県',
        20 => '長野県',
        21 => '岐阜県',
        22 => '静岡県',
        23 => '愛知県',
        24 => '三重県',
        25 => '滋賀県',
        26 => '京都府',
        27 => '大阪府',
        28 => '兵庫県',
        29 => '奈良県',
        30 => '和歌山県',
        31 => '鳥取県',
        32 => '島根県',
        33 => '岡山県',
        34 => '広島県',
        35 => '山口県',
        36 => '徳島県',
        37 => '香川県',
        38 => '愛媛県',
        39 => '高知県',
        40 => '福岡県',
        41 => '佐賀県',
        42 => '長崎県',
        43 => '熊本県',
        44 => '大分県',
        45 => '宮崎県',
        46 => '鹿児島県',
        47 => '沖縄県',
    ),
    // }}}

    // {{{ system mail from
    'system_mail_from' => 'info@tk-honin.com',
    // }}}

    // {{{ mail template
    // {{{ recruit
    'recruit' => array(
        'admin' => array(
            'email' => 'info@tk-honin.com',
            'subject' => 'エントリーフォーム申込み通知',
            'message' => '
エントリーフォームより申込がありました。

申込日時 : {entry_date}

【お名前】
{name}

【フリガナ】
{kana}

【年齢】
{age} 歳

【電話番号】
{tel1}-{tel2}-{tel3}

【連絡先メールアドレス】
{email}

【希望職種】
{occupation}

【志望動機】
{application}

【その他】
{other}

-- 
表参道スキンクリニック
Tokyo Cosmetic Sergery

',
        ),
        'user' => array(
            'subject' => 'エントリーフォーム申込み承りました',
            'message' => '
{name} 様

お申し込みありがとうございます。
下記内容にてエントリーフォームのお申込みを承りました。

申込日時 : {entry_date}

【お名前】
{name}

【フリガナ】
{kana}

【年齢】
{age} 歳

【電話番号】
{tel1}-{tel2}-{tel3}

【連絡先メールアドレス】
{email}

【希望職種】
{occupation}

【志望動機】
{application}

【その他】
{other}

-- 
表参道スキンクリニック
Tokyo Cosmetic Sergery

',
        ),
    ),
    // }}}
    // {{{ reservation
    'reservation' => array(
        'admin' => array(
            'email' => 'info@tk-honin.com',
            'subject' => '無料カウンセリング予約申込み通知',
            'message' => '
無料カウンセリング予約より申込がありました。

申込日時 : {entry_date}

【お名前】
{name}

【フリガナ】
{kana}

【年齢】
{age} 歳

【郵便番号】
{zip1}-{zip2}

【都道府県】
{pref}

【市区町村】
{addr1}

【丁目番地】
{addr2}

【電話番号】
{tel1}-{tel2}-{tel3}

【メールアドレス】
{email}

【当クリニックをどこで知りましたか？】
{reason}

【ご相談箇所】
{parts}

【ご相談内容】
{message}

【ご相談希望日時】
第一希望 : {date1} {time1}
第二希望 : {date2} {time2}
第三希望 : {date3} {time3}

-- 
表参道スキンクリニック
Tokyo Cosmetic Sergery

',
        ),
        'user' => array(
            'subject' => '無料カウンセリング予約申込み承りました',
            'message' => '
{name} 様

お申し込みありがとうございます。
下記内容にて無料カウンセリング予約のお申込みを承りました。

申込日時 : {entry_date}

【お名前】
{name}

【フリガナ】
{kana}

【年齢】
{age} 歳

【郵便番号】
{zip1}-{zip2}

【都道府県】
{pref}

【市区町村】
{addr1}

【丁目番地】
{addr2}

【電話番号】
{tel1}-{tel2}-{tel3}

【メールアドレス】
{email}

【当クリニックをどこで知りましたか？】
{reason}

【ご相談箇所】
{parts}

【ご相談内容】
{message}

【ご相談希望日時】
第一希望 : {date1} {time1}
第二希望 : {date2} {time2}
第三希望 : {date3} {time3}

-- 
表参道スキンクリニック
Tokyo Cosmetic Sergery

',
        ),
    ),
    // }}}
    // {{{ inquiry
    'inquiry' => array(
        'admin' => array(
            'email' => 'info@tk-honin.com',
            'subject' => '無料メール相談申込み通知',
            'message' => '
無料メール相談より申込がありました。

申込日時 : {entry_date}

【お名前】
{name}

【ふりがな】
{kana}

【電話番号】
{tel}

【メールアドレス】
{email}

【ご相談箇所】
{parts}

【ご相談内容】
{message}

-- 
表参道スキンクリニック
Tokyo Cosmetic Sergery

',
        ),
        'user' => array(
            'subject' => '無料メール相談申込み承りました',
            'message' => '
{name} 様

お申し込みありがとうございます。
下記内容にて無料メール相談のお申込みを承りました。

申込日時 : {entry_date}

【お名前】
{name}

【ふりがな】
{kana}

【電話番号】
{tel}

【メールアドレス】
{email}

【ご相談箇所】
{parts}

【ご相談内容】
{message}

-- 
表参道スキンクリニック
Tokyo Cosmetic Sergery

',
        ),
    ),
    // }}}
    // {{{ catalog
    'catalog' => array(
        'admin' => array(
            'email' => 'info@tk-honin.com',
            'subject' => '資料請求申込み通知',
            'message' => '
資料請求より申込がありました。

申込日時 : {entry_date}

【お名前】
{name}

【ふりがな】
{kana}

【郵便番号】
{zip1}-{zip2}

【都道府県】
{pref}

【市区町村】
{addr1}

【丁目番地】
{addr2}

【電話番号】
{tel}

【メールアドレス】
{email}

-- 
表参道スキンクリニック
Tokyo Cosmetic Sergery

',
        ),
        'user' => array(
            'subject' => '資料請求申込み承りました',
            'message' => '
{name} 様

お申し込みありがとうございます。
下記内容にて資料請求のお申込みを承りました。

申込日時 : {entry_date}

【お名前】
{name}

【ふりがな】
{kana}

【郵便番号】
{zip1}-{zip2}

【都道府県】
{pref}

【市区町村】
{addr1}

【丁目番地】
{addr2}

【電話番号】
{tel}

【メールアドレス】
{email}

-- 
表参道スキンクリニック
Tokyo Cosmetic Sergery

',
        ),
    ),
    // }}}
    // }}}

);

