<?php

function spdir()
{
    if (defined('SPDIR') && SPDIR === TRUE) {
        return basename(ZIU_DISPATCH_PATH);
    }
}


return array(

    'view_layout_default'  => 'default',

    // File ident name                                                                          
    'suffix_logic' => 'logic',
    'suffix_prep'  => 'prep',
    'suffix_view'  => spdir() ? 'view-sp' : 'view',
    'suffix_join'  => 'logic,prep,' . (spdir() ? 'view-sp' : 'view'),

);

