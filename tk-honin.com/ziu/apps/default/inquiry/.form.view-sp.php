
<?php content_for('javascript') ?>
<script type="text/javascript">
$(document).ready(function(){
    $('select').selectmenu();
}); 
</script>   
<?php content_end_for('javascript') ?>

<!--mArea-->
<div class="mArea" id="inquiry_form">
  <div class="h1_sec">
    <h1>無料メール相談</h1>
    <div class="clm_l">
      <p>当クリニックへのご質問、ご相談に24時間対応しています。クリニックや施術に関する事、お悩みなど何でもご相談下さい。まずはメールでお気軽にご相談ください。</p>
    </div><!--/innerBlock-->
  </div><!--/h1_sec-->
  <div class="img tac"><a href="tel:0120334270" onClick="ga('send', 'event', 'sp', 'tel-tap', 'tel');"><img src="../common/images/form_tel.jpg" alt="TEL:0120-334-270　完全予約制 受付時間 11時 － 20時" width="580" height="204"></a></div><!--/sec-->
    
<ul id="steplist">
<li class="active">お客様情報の<br>入力</li>
<li>内容の<br>ご確認</li>
<li>送信完了</li>
</ul>
<p class="noticeTxt-s">※ 大変お手数ですが必須項目はすべてご記入頂き、<br/>「確認画面へ」ボタンを押してください。</p>

<?php echo $f->open('../inquiry/confirm')->attributes(array('class' => 'niceform')) ?>

  <div class="sec" id="form">
    <div class="formBlock lh1_6">
        <dl>
          <dt><span class="icon-red">必須</span>お名前</dt>
          <dd class="name">
            <div class="clearfix">
                <span class="form_tt">お名前</span>
              <div class="form_colr">
                <p><?php echo $f->text('name')->attributes(array('class' => 'mst', 'placeholder' => '例） 表参道　花子')) ?></p>
<?php if ($e['name']) :?>
                <p class="error db" title="名前"><?php echo $e['name'] ?></p>
<?php else : ?>
                <p class="error" title="名前">名前を入力してください</p>
<?php endif; ?>
              </div>
            </div>
          
            <div class="item clearfix">
                <span class="form_tt">フリガナ</span>
              <div class="form_colr">
                <p><?php echo $f->text('kana')->attributes(array('class' => 'mst', 'placeholder' => '例） オモテサンドウ　ハナコ')) ?></p>
<?php if ($e['kana']) :?>
                <p class="error db" title="フリガナ"><?php echo $e['kana'] ?></p>
<?php else : ?>
                <p class="error" title="フリガナ">フリガナを入力してください</p>
<?php endif; ?>
              </div>
            </div>
            
          </dd>
          <dt><span class="icon-red">必須</span>電話番号</dt>
          <dd class="tel">
            <p><?php echo $f->text('tel')->attributes(array('class' => 'mst num', 'placeholder' => '例）03-1234-5678', 'style' => 'ime-mode: disabled;')) ?></p>
<?php if ($e['tel']) :?>
                <p class="error db" title="電話番号"><?php echo $e['tel'] ?></p>
<?php else : ?>
                <p class="error" title="電話番号">電話番号を入力してください</p>
<?php endif; ?>
          </dd>
          <dt><span class="icon-red">必須</span>連絡先メールアドレス</dt>
          <dd class="mail">
            <p><?php echo $f->email('email')->attributes(array('class' => 'mst', 'placeholder' => '例）hanako@omotesando.co.jp')) ?></p>
<?php if ($e['email']) :?>
                <p class="error db" title="連絡先メールアドレス"><?php echo $e['email'] ?></p>
<?php else : ?>
                <p class="error" title="連絡先メールアドレス">連絡先メールアドレスを入力してください</p>
<?php endif; ?>
          </dd>
          <dt><span class="icon-blue">任意</span>ご相談箇所　（複数選択可能）</dt>
          <dd class="checkchoice">
            <p class="clearfix"><?php echo parts_html_for_sp($f->checkbox('parts')->outline(11, 2, array('class' => 'parts'))) ?></p>
          </dd>
          <dt class="content"><span class="icon-red">必須</span>ご相談内容</dt>
          <dd class="name">
            <p><?php echo $f->textarea('message')->attributes(array('rows' => '7', 'class' => 'mst', 'placeholder' => '例）シミとしわが目立つようになったので、レーザー治療を考えています。肌が弱い人でもレーザー治療は可能でしょうか？')) ?></p>
<?php if ($e['message']) :?>
            <p class="error db" title="ご相談内容"><?php echo $e['message'] ?></p>
<?php else : ?>
            <p class="error" title="ご相談内容">ご相談内容を入力してください</p>
<?php endif; ?>
          </dd>
        </dl>

    </div>
  </div><!--/sec-->

  <div class="formBtn"><?php echo $f->submit('action', '確認画面へ')->attributes(array('id' => 'submit')) ?></div>

<?php echo $f->close() ?>
  
</div>
<!--/mArea-->

