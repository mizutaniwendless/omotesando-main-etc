
<?php content_for('javascript') ?>
<script type="text/javascript">
function go(mode){
    if (mode == 'back') {
        $('#dummy').attr('name', 'action[back]');
    } else {
        $('#dummy').attr('name', 'action[send]');
    }
    document.f.submit();
}
</script>   
<?php content_end_for('javascript') ?>

<!--mArea-->
<div class="mArea" id="inquiry_confirm">
  <div class="h1_sec h1_block">
    <h1>無料メール相談</h1>
  </div><!--/h1_sec-->

<ul id="steplist">
<li>お客様情報の<br>入力</li>
<li class="active">内容の<br>ご確認</li>
<li>送信完了</li>
</ul>
<p class="noticeTxt-s">※入力内容をご確認の上、「送信」ボタンを押して下さい。</p>

<?php echo $f->open('../inquiry/execute')->attributes(array('name' => 'f')) ?>
<?php echo $f->hidden('action[send]')->attributes(array('id' => 'dummy')) ?>
<?php echo $f->hidden() ?>

  <div class="sec confirm mt60">
    <h2><span class="icon-red">必須</span>お名前</h2>
    <div class="innerBlock">
      <p class="p00"><?php echo $f->fetch('name') ?></p>
      <p><?php echo $f->fetch('kana') ?></p>
    </div>
  </div><!--/sec-->
  <div class="sec confirm">
    <h2><span class="icon-red">必須</span>電話番号</h2>
    <div class="innerBlock">
      <p class="p00"><?php echo $f->fetch('tel') ?></p>
    </div>
  </div><!--/sec-->
  <div class="sec confirm">
    <h2><span class="icon-red">必須</span>連絡先メールアドレス</h2>
    <div class="innerBlock">
      <p class="p00"><?php echo $f->fetch('email') ?></p>
    </div>
  </div><!--/sec-->
  <div class="sec confirm">
    <h2><span class="icon-blue">任意</span>ご相談箇所　（複数選択可能）</h2>
    <div class="innerBlock">
      <p class="p00"><?php echo parts($f->fetch('parts'), 'sp') ?></p>
    </div>
  </div><!--/sec-->
  <div class="sec confirm">
    <h2><span class="icon-red">必須</span>ご相談内容</h2>
    <div class="innerBlock">
      <p class="p00"><?php echo nl2br($f->fetch('message')) ?></p>
    </div>
  </div><!--/sec-->

  <div class="btn-wrap clearfix">
    <div class="small-backConfirmBlock clearfix">
      <a href="javascript:void(0);" onclick="go('back'); return false;"><span>修正する</span></a>
    </div>
    <div class="small-submitBlock clearfix">
      <a href="javascript:void(0);" onclick="go('exec'); return false;"><span>送信</span></a>
    </div>
  </div>

<?php echo $f->close() ?>

</div>
<!--/mArea-->

