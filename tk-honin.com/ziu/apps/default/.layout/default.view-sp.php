<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="target-densitydpi=device-dpi, width=640, user-scalable=no">
<title><?php echo $html['head']['title'] ?> | 表参道スキンクリニック</title>
<meta name="keywords" content="<?php echo $html['head']['keywords'] ?>" />
<meta name="description" content="<?php echo $html['head']['description'] ?>" />
<link rel="stylesheet" type="text/css" href="../common/css/jquery-ui.css" media="screen,print" />
<link rel="stylesheet" type="text/css" href="../common/css/niceforms.css" media="screen,print" />
<link rel="stylesheet" type="text/css" href="../common/css/common.css" media="screen,print" />
<script type="text/javascript" src="../common/js/jquery.min.js"></script>
<script type="text/javascript" src="../common/js/common.js"></script>
<script type="text/javascript" src="../common/js/form.js"></script>
<script type="text/javascript" src="../common/js/niceforms.js"></script>
<script type="text/javascript" src="../common/js/jquery-ui.js"></script>
<?php echo asset_javascript_tag('all') ?>
<?php echo asset_stylesheet_tag('all') ?>
<?php echo isset($content_for_javascript) ? $content_for_javascript : '' ?>
<?php echo isset($content_for_stylesheet) ? $content_for_stylesheet : '' ?>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-59451323-1', 'auto');
  ga('send', 'pageview');

</script>

</head>

<body>
<?php

$header = ZIU_DISPATCH_PATH . DS . 'templates/header.html';
if (is_readable($header)) {
    include $header;
}

?>

<?php echo render_content() ?>


<?php

$footer_name = empty($footer_name) ? 'footer_copy' : $footer_name;
$footer = ZIU_DISPATCH_PATH . DS . 'templates/' . $footer_name . '.html';
if (is_readable($footer)) {
    include $footer;
}

?>

</body>
</html>
