<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Language" content="ja" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<title><?php echo $html['head']['title'] ?> | 表参道スキンクリニック</title>
<meta name="keywords" content="<?php echo $html['head']['keywords'] ?>" />
<meta name="description" content="<?php echo $html['head']['description'] ?>" />
<link rel="stylesheet" type="text/css" href="../common/css/common.css" media="screen,print" />
<script type="text/javascript" src="../common/js/jquery.min.js"></script>
<script type="text/javascript" src="../common/js/common.js"></script>
<?php echo asset_javascript_tag('all') ?>
<?php echo asset_stylesheet_tag('all') ?>
<?php echo isset($content_for_javascript) ? $content_for_javascript : '' ?>
<?php echo isset($content_for_stylesheet) ? $content_for_stylesheet : '' ?>
</head>
<body>

<?php

$form_header = dirname(ZIU_MAIN_PATH) . DS . 'templates/form_header.html';
if (is_readable($form_header)) {
    include $form_header;
}

?>

<div class="wrapper">
	<div class="wrapperInner">
		<div class="inner960 clearfix" id="contents">
			<div id="contentsR">
            <?php echo render_content() ?>
			<!-- / #contentsR --></div>
			<div id="contentsL">
<?php if (strpos(main_module(), 'recruit/') !== FALSE) : ?>
				<p class="sidebarTitle"><img src="../images/sidebar_title02.gif" alt="採用情報" /></p>
                <div class="sideNaviBox">
                    <ul class="sideNaviInner">
                        <li><a href="./index.html">募集要項</a></li>
                        <li class="current"><a href="./form">エントリー</a></li>
                    </ul>
                </div>
<?php endif; ?>
				<p><img src="../images/sidebar_contact.jpg" alt="" /></p>
				<p class="sideBnr"><a href="../coupon/index.html"><img src="../images/sidebar_image01.jpg" alt="" /></a></p>
			<!-- / #contentsL --></div>
		<!-- / #contents --></div>
	<!-- /wrapperInner --></div>
<!-- /wrapper --></div>

<?php

$form_footer = dirname(ZIU_MAIN_PATH) . DS . 'templates/form_footer.html';
if (is_readable($form_footer)) {
    include $form_footer;
}

?>

</body>
</html>
