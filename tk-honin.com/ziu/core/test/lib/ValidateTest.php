<?php
/* vim:se et ts=4 sw=4 sts=4: */

chdir(dirname(__FILE__));

require_once '../../lib/validate.php';

class ValidateTest extends PHPUnit_Framework_TestCase
{

    public function setUp()
    {
        $this->obj = new Validate;
        $this->hex = array('0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f');
    }

    /**
     * インスタンスの作成テスト
     */
    public function test_init()
    {
        $obj = $this->obj->init();
        $this->assertInstanceOf('Validate', $obj);
    }

    /**
     * エラー取得のテスト
     */
    public function test_error()
    {
        $vd = $this->obj->init();
        $vd->field('item1', 'test error');
        $vd->rule('match', '/^\d+$/');
        $vd->execute(array('item1' => 'a999'));
        $er = $vd->error('item1');
        $this->assertInstanceOf('Validate_Exception', $er);
        $this->assertTrue(is_string($er->message(':label -> :state')));
        $this->assertTrue(is_string($vd->get_error('item1', ':label -> :state')));
        $this->assertTrue(is_string($vd->get_errors(':label -> :state')));
    }

    /**
     * バリデーションメソッド追加のテスト
     */
    public function test_add()
    {
        // 異常系
        $vd = $this->obj->init()->add($this);
        $vd->field('item1', 'test add', 'test[fuga]');
        $vd->execute(array('item1' => 'hoge'));
        $this->assertEquals($vd->get_error('item1', ':state'), "値(hoge)が文字[fuga]と一致しませんでした。\n");
        // 正常系
        $vd = $this->obj->init();
        $vd->field('item1', 'test add')->rule('test', 'gaga');
        $this->assertTrue($vd->execute(array('item1' => 'gaga')), FALSE, $this);
    }

    /**
     * 追加バリデーションメソッド
     * @return boolean TRUE is OK, FALSE is NG
     */
    public function _valid_test($val, $test)
    {
        Validate::message('test', '値(:value)が文字[' . $test . ']と一致しませんでした。');
        return $val === 'test';
    }

    /**
     * トリムのテスト
     */
    public function test_trim()
    {
        $vd = $this->obj->init();
        $vd->field('item1', 'test trim', 'trim');
        $vd->execute(array('item1' => ' 　 　  　hoge 　 　  　'));
        $this->assertEquals('hoge', $vd->valid('item1'));
    }

    // {{{ Validation test for character
    /**
     * バリデーション：必須[required]のテスト
     * requiredの機能
     * data値が「存在しない、空文字''、NULL、FALSE」の場合はエラーにする
     */
    public function test_required()
    {
        $vd = $this->obj->init();
        $vd->field('item', 'test field [required]', 'required');
        // 異常系
        //echo("False means Validation failure.\n");
        $data_false = array(
                            array(),                // 存在しない
                            array('item' => ''),    // 空文字
                            array('item' => NULL),  // NULL
                            array('item' => FALSE), // FALSE
                        );
        foreach ($data_false as $key => $data) {
            //echo($key . " -----------------\n");
            $result = $vd->execute($data);
            //var_dump($data, $result);
            $this->assertFalse($result);
        }
        // 正常系
        //echo("True means Validation success.\n");
        $data_true = array(
                            array('item' => 0),            // 数値 0
                            array('item' => '0'),          // 文字列 0
                            array('item' => 'テスト'),     // 文字列 テスト
                            array('item' => new StdClass), // オブジェクト
                            array('item' => array(1,2,3)), // 配列
                        );
        foreach ($data_true as $key => $data) {
            //echo($key . " -----------------\n");
            $key === 1;
            $result = $vd->execute($data);
            //var_dump($data, $result);
            $this->assertTrue($result);
        }
    }

    /**
     * バリデーション:値一致[value]のテスト
     * valueの機能
     * data値がcompareと不一致の場合はエラーにする
     * empty値「存在しない、空文字''、NULL、FALSE」はNGにならない
     */
    public function test_value()
    {
        $vd = $this->obj->init();
        // 文字列比較のテスト------------------------------------------------
        $vd->field('item1', 'test field [value]');
        $vd->rule('value', 'hoge');
        // 異常系
        $this->assertFalse($vd->execute(array('item1' => 'a')));
        $this->assertFalse($vd->execute(array('item1' => new StdClass)));
        $this->assertFalse($vd->execute(array('item1' => array('hoge'))));
        // 正常系
        $this->assertTrue($vd->execute(array()));
        $this->assertTrue($vd->execute(array('item1' => '')));
        $this->assertTrue($vd->execute(array('item1' => NULL)));
        $this->assertTrue($vd->execute(array('item1' => FALSE)));
        $this->assertTrue($vd->execute(array('item1' => 'hoge')));
        // 配列比較のテスト--------------------------------------------------
        $vd->field('item2', 'test field [value]');
        $vd->rule('value', array('fuga'));
        // 異常系
        $this->assertFalse($vd->execute(array('item2' => array('hoge'))));
        // 正常系
        $this->assertTrue($vd->execute(array('item2' => array('fuga'))));
        // オブジェクト比較のテスト------------------------------------------
        $vd->field('item3', 'test field [value]');
        $vd->rule('value', new Directory);
        // 異常系
        $this->assertFalse($vd->execute(array('item3' => new StdClass)));
        // 正常系
        $this->assertTrue($vd->execute(array('item3' => new Directory)));
        // 数値比較のテスト------------------------------------------------
        $vd->field('item4', 'test field [value]');
        $vd->rule('value', 9);
        // 異常系
        $this->assertFalse($vd->execute(array('item4' => 1)));
        // 正常系
        $this->assertTrue($vd->execute(array('item4' => 9)));
    }

    /**
     * バリデーション:パターン一致[match]のテスト
     * matchの機能
     * data値がpatternと不一致の場合はエラーにする
     * empty値「存在しない、空文字''、NULL、FALSE」はNGにならない
     */
    public function test_match()
    {
        $vd = $this->obj->init();
        $vd->field('item1', 'test field [match]');
        $vd->rule('match', '/^\d+$/');
        // 異常系
        $this->assertFalse($vd->execute(array('item1' => 'a999')));
        $this->assertFalse($vd->execute(array('item1' => new StdClass)));
        $this->assertFalse($vd->execute(array('item1' => array('hoge'))));
        // 正常系
        $this->assertTrue($vd->execute(array()));
        $this->assertTrue($vd->execute(array('item1' => '')));
        $this->assertTrue($vd->execute(array('item1' => NULL)));
        $this->assertTrue($vd->execute(array('item1' => FALSE)));
        $this->assertTrue($vd->execute(array('item1' => '9999')));
        $this->assertTrue($vd->execute(array('item1' => 9999)));
    }

    /**
     * バリデーション:最小文字数[minlen]のテスト
     * minlenの機能
     * data値が指定の文字数より小さい場合はエラーにする
     * empty値「存在しない、空文字''、NULL、FALSE」はNGにならない
     */
    public function test_minlen()
    {
        $vd = $this->obj->init();
        $vd->field('item1', 'test field [minlen]');
        $vd->rule('minlen', 5);
        // 異常系
        $this->assertFalse($vd->execute(array('item1' => '1234')));
        $this->assertFalse($vd->execute(array('item1' => new StdClass)));
        $this->assertFalse($vd->execute(array('item1' => array('hoge'))));
        // 正常系
        $this->assertTrue($vd->execute(array()));
        $this->assertTrue($vd->execute(array('item1' => '')));
        $this->assertTrue($vd->execute(array('item1' => NULL)));
        $this->assertTrue($vd->execute(array('item1' => FALSE)));
        $this->assertTrue($vd->execute(array('item1' => '12345')));
        $this->assertTrue($vd->execute(array('item1' => 12345)));
    }

    /**
     * バリデーション:最大文字数[maxlen]のテスト
     * maxlenの機能
     * data値が指定の文字数より大きい場合はエラーにする
     * empty値「存在しない、空文字''、NULL、FALSE」はNGにならない
     */
    public function test_maxlen()
    {
        $vd = $this->obj->init();
        $vd->field('item1', 'test field [maxlen]');
        $vd->rule('maxlen', 5);
        // 異常系
        $this->assertFalse($vd->execute(array('item1' => '123456')));
        $this->assertFalse($vd->execute(array('item1' => new StdClass)));
        $this->assertFalse($vd->execute(array('item1' => array('hoge'))));
        // 正常系
        $this->assertTrue($vd->execute(array()));
        $this->assertTrue($vd->execute(array('item1' => '')));
        $this->assertTrue($vd->execute(array('item1' => NULL)));
        $this->assertTrue($vd->execute(array('item1' => FALSE)));
        $this->assertTrue($vd->execute(array('item1' => '12345')));
        $this->assertTrue($vd->execute(array('item1' => 12345)));
    }

    /**
     * バリデーション:文字数一致[length]のテスト
     * lengthの機能
     * data値が指定の文字数と不一致の場合はエラーにする
     * empty値「存在しない、空文字''、NULL、FALSE」はNGにならない
     */
    public function test_length()
    {
        $vd = $this->obj->init();
        $vd->field('item1', 'test field [length]');
        $vd->rule('length', 5);
        // 異常系
        $this->assertFalse($vd->execute(array('item1' => 1234)));
        $this->assertFalse($vd->execute(array('item1' => '123456')));
        $this->assertFalse($vd->execute(array('item1' => new StdClass)));
        $this->assertFalse($vd->execute(array('item1' => array('hoge'))));
        // 正常系
        $this->assertTrue($vd->execute(array()));
        $this->assertTrue($vd->execute(array('item1' => '')));
        $this->assertTrue($vd->execute(array('item1' => NULL)));
        $this->assertTrue($vd->execute(array('item1' => FALSE)));
        $this->assertTrue($vd->execute(array('item1' => '12345')));
        $this->assertTrue($vd->execute(array('item1' => 12345)));
    }

    /**
     * バリデーション:半角英字一致[alpha]のテスト
     * alphaの機能
     * data値に半角英字以外が含まれる場合はエラーにする
     * empty値「存在しない、空文字''、NULL、FALSE」はNGにならない
     */
    public function test_alpha()
    {
        $vd = $this->obj->init();
        $vd->field('item1', 'test field [alpha]', 'alpha');
        // 異常系
        $this->assertFalse($vd->execute(array('item1' => 1234)));
        $this->assertFalse($vd->execute(array('item1' => 'abc.')));
        $this->assertFalse($vd->execute(array('item1' => new StdClass)));
        $this->assertFalse($vd->execute(array('item1' => array('hoge'))));
        // 正常系
        $this->assertTrue($vd->execute(array()));
        $this->assertTrue($vd->execute(array('item1' => '')));
        $this->assertTrue($vd->execute(array('item1' => NULL)));
        $this->assertTrue($vd->execute(array('item1' => FALSE)));
        $this->assertTrue($vd->execute(array('item1' => 'abcdefghijklmnopqrstuvwxyz')));
        $this->assertTrue($vd->execute(array('item1' => 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')));
    }

    /**
     * バリデーション:半角英数字一致[alnum]のテスト
     * alnumの機能
     * data値に半角英数字以外が含まれる場合はエラーにする
     * empty値「存在しない、空文字''、NULL、FALSE」はNGにならない
     */
    public function test_alnum()
    {
        $vd = $this->obj->init();
        $vd->field('item1', 'test field [alnum]', 'alnum');
        // 異常系
        $this->assertFalse($vd->execute(array('item1' => -1234)));
        $this->assertFalse($vd->execute(array('item1' => '-abc')));
        $this->assertFalse($vd->execute(array('item1' => new StdClass)));
        $this->assertFalse($vd->execute(array('item1' => array('hoge'))));
        // 正常系
        $this->assertTrue($vd->execute(array()));
        $this->assertTrue($vd->execute(array('item1' => '')));
        $this->assertTrue($vd->execute(array('item1' => NULL)));
        $this->assertTrue($vd->execute(array('item1' => FALSE)));
        $this->assertTrue($vd->execute(array('item1' => '0123456789abcdefghijklmnopqrstuvwxyz')));
        $this->assertTrue($vd->execute(array('item1' => '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ')));
    }

    /**
     * バリデーション:半角英数字ダッシュ一致[aldash]のテスト
     * aldashの機能
     * data値に半角英数字とアンダースコア、ハイフン以外が含まれる場合はエラーにする
     * empty値「存在しない、空文字''、NULL、FALSE」はNGにならない
     */
    public function test_aldash()
    {
        $vd = $this->obj->init();
        $vd->field('item1', 'test field [aldash]', 'aldash');
        // 異常系
        $this->assertFalse($vd->execute(array('item1' => '+1234')));
        $this->assertFalse($vd->execute(array('item1' => '+abc')));
        $this->assertFalse($vd->execute(array('item1' => new StdClass)));
        $this->assertFalse($vd->execute(array('item1' => array('hoge'))));
        // 正常系
        $this->assertTrue($vd->execute(array()));
        $this->assertTrue($vd->execute(array('item1' => '')));
        $this->assertTrue($vd->execute(array('item1' => NULL)));
        $this->assertTrue($vd->execute(array('item1' => FALSE)));
        $this->assertTrue($vd->execute(array('item1' => '-_0123456789abcdefghijklmnopqrstuvwxyz')));
        $this->assertTrue($vd->execute(array('item1' => '-_0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ')));
    }

    /**
     * バリデーション:BASE64一致[base64]のテスト
     * base64の機能
     * data値にBASE64文字列以外が含まれる場合はエラーにする
     * empty値「存在しない、空文字''、NULL、FALSE」はNGにならない
     */
    public function test_base64()
    {
        $vd = $this->obj->init();
        $vd->field('item1', 'test field [base64]', 'base64');
        // 異常系
        $this->assertFalse($vd->execute(array('item1' => '?1234')));
        $this->assertFalse($vd->execute(array('item1' => '?abc')));
        $this->assertFalse($vd->execute(array('item1' => new StdClass)));
        $this->assertFalse($vd->execute(array('item1' => array('hoge'))));
        // 正常系
        $this->assertTrue($vd->execute(array()));
        $this->assertTrue($vd->execute(array('item1' => '')));
        $this->assertTrue($vd->execute(array('item1' => NULL)));
        $this->assertTrue($vd->execute(array('item1' => FALSE)));
        $this->assertTrue($vd->execute(array('item1' => base64_encode('-_0123456789abcdefghijklmnopqrstuvwxyz'))));
    }
    // }}}

    // {{{ Validation test for internet and communication
    /**
     * バリデーション:メールアドレス形式[email]のテスト
     * emailの機能
     * data値がメールアドレス形式でない場合はエラーにする
     * empty値「存在しない、空文字''、NULL、FALSE」はNGにならない
     */
    public function test_email()
    {
        $vd = $this->obj->init();
        $vd->field('item1', 'test field [email]');
        $vd->rule('email', 5);
        // 異常系
        $this->assertFalse($vd->execute(array('item1' => 0)));
        $this->assertFalse($vd->execute(array('item1' => '0')));
        $this->assertFalse($vd->execute(array('item1' => 6)));
        $this->assertFalse($vd->execute(array('item1' => 'aaa@aaa@aaa.com')));
        $this->assertFalse($vd->execute(array('item1' => new StdClass)));
        $this->assertFalse($vd->execute(array('item1' => array('hoge'))));
        // 正常系
        $this->assertTrue($vd->execute(array()));
        $this->assertTrue($vd->execute(array('item1' => '')));
        $this->assertTrue($vd->execute(array('item1' => NULL)));
        $this->assertTrue($vd->execute(array('item1' => FALSE)));
        $this->assertTrue($vd->execute(array('item1' => '_see-saw.c+@mail.gmail.com')));
    }

    /**
     * バリデーション:IPアドレス形式[ip]のテスト
     * ipの機能
     * data値がIPアドレス形式でない場合はエラーにする
     * empty値「存在しない、空文字''、NULL、FALSE」はNGにならない
     */
    public function test_ip()
    {
        $vd = $this->obj->init();
        $vd->field('item1', 'test field [ip]')->rule('ip');
        // 異常系
        $this->assertFalse($vd->execute(array('item1' => 0)));
        $this->assertFalse($vd->execute(array('item1' => '0')));
        $this->assertFalse($vd->execute(array('item1' => '0.0.0.0')));
        $this->assertFalse($vd->execute(array('item1' => '012.0.0.0')));
        $this->assertFalse($vd->execute(array('item1' => new StdClass)));
        $this->assertFalse($vd->execute(array('item1' => array('hoge'))));
        // 正常系
        $this->assertTrue($vd->execute(array()));
        $this->assertTrue($vd->execute(array('item1' => '')));
        $this->assertTrue($vd->execute(array('item1' => NULL)));
        $this->assertTrue($vd->execute(array('item1' => FALSE)));
        $this->assertTrue($vd->execute(array('item1' => '192.168.0.255')));
        $this->assertTrue($vd->execute(array('item1' => '255.255.255.255')));
    }

    /**
     * バリデーション:ドメイン形式[domain]のテスト
     * domainの機能
     * data値がドメイン形式でない場合はエラーにする
     * empty値「存在しない、空文字''、NULL、FALSE」はNGにならない
     */
    public function test_domain()
    {
        $vd = $this->obj->init();
        $vd->field('item1', 'test field [domain]')->rule('domain');
        // 異常系
        $this->assertFalse($vd->execute(array('item1' => 0)));
        $this->assertFalse($vd->execute(array('item1' => '0')));
        $this->assertFalse($vd->execute(array('item1' => '0.0.0.0')));
        $this->assertFalse($vd->execute(array('item1' => 'test@sample.com')));
        $this->assertFalse($vd->execute(array('item1' => new StdClass)));
        $this->assertFalse($vd->execute(array('item1' => array('hoge'))));
        // 正常系
        $this->assertTrue($vd->execute(array()));
        $this->assertTrue($vd->execute(array('item1' => '')));
        $this->assertTrue($vd->execute(array('item1' => NULL)));
        $this->assertTrue($vd->execute(array('item1' => FALSE)));
        $this->assertTrue($vd->execute(array('item1' => 'google.co.jp')));
    }

    /**
     * バリデーション:URL形式[url]のテスト
     * urlの機能
     * data値がURL形式でない場合はエラーにする
     * empty値「存在しない、空文字''、NULL、FALSE」はNGにならない
     */
    public function test_url()
    {
        $vd = $this->obj->init();
        $vd->field('item1', 'test field [url]')->rule('url');
        // 異常系
        $this->assertFalse($vd->execute(array('item1' => 0)));
        $this->assertFalse($vd->execute(array('item1' => '0')));
        $this->assertFalse($vd->execute(array('item1' => '0.0.0.0')));
        $this->assertFalse($vd->execute(array('item1' => 'test.sample.com')));
        $this->assertFalse($vd->execute(array('item1' => new StdClass)));
        $this->assertFalse($vd->execute(array('item1' => array('hoge'))));
        // 正常系
        $this->assertTrue($vd->execute(array()));
        $this->assertTrue($vd->execute(array('item1' => '')));
        $this->assertTrue($vd->execute(array('item1' => NULL)));
        $this->assertTrue($vd->execute(array('item1' => FALSE)));
        $this->assertTrue($vd->execute(array('item1' => 'http://google.co.jp/search?q=abc#top')));
    }

    /**
     * バリデーション:携帯メールアドレス形式[ktaimail]のテスト
     * ktaimailの機能
     * data値が携帯メールアドレス形式でない場合はエラーにする
     * empty値「存在しない、空文字''、NULL、FALSE」はNGにならない
     */
    public function test_ktaimail()
    {
        $vd = $this->obj->init();
        $vd->field('item1', 'test field [ktaimail]')->rule('ktaimail');
        // 異常系
        $this->assertFalse($vd->execute(array('item1' => 0)));
        $this->assertFalse($vd->execute(array('item1' => '0')));
        $this->assertFalse($vd->execute(array('item1' => 'test@j-pt.ne.jp'))); // J-phone
        $this->assertFalse($vd->execute(array('item1' => new StdClass)));
        $this->assertFalse($vd->execute(array('item1' => array('hoge'))));
        // 正常系
        $this->assertTrue($vd->execute(array()));
        $this->assertTrue($vd->execute(array('item1' => '')));
        $this->assertTrue($vd->execute(array('item1' => NULL)));
        $this->assertTrue($vd->execute(array('item1' => FALSE)));
        $this->assertTrue($vd->execute(array('item1' => 'sample@docomo.ne.jp')));
        $this->assertTrue($vd->execute(array('item1' => 'sample@ezweb.ne.jp')));
        $this->assertTrue($vd->execute(array('item1' => 'sample@softbank.ne.jp')));
        $this->assertTrue($vd->execute(array('item1' => 'sample@t.vodafone.ne.jp')));
        $this->assertTrue($vd->execute(array('item1' => 'sample@willcom.com')));
        $this->assertTrue($vd->execute(array('item1' => 'sample@pdx.ne.jp')));
        $this->assertTrue($vd->execute(array('item1' => 'sample@disney.ne.jp')));
    }

    /**
     * バリデーション:郵便番号形式[zip]のテスト
     * zipの機能
     * data値が郵便番号形式でない場合はエラーにする
     * empty値「存在しない、空文字''、NULL、FALSE」はNGにならない
     */
    public function test_zip()
    {
        $vd = $this->obj->init();
        $vd->field('item1', 'test field [zip]')->rule('zip');
        // 異常系
        $this->assertFalse($vd->execute(array('item1' => 0)));
        $this->assertFalse($vd->execute(array('item1' => '0')));
        $this->assertFalse($vd->execute(array('item1' => '1000-0001')));
        $this->assertFalse($vd->execute(array('item1' => new StdClass)));
        $this->assertFalse($vd->execute(array('item1' => array('hoge'))));
        // 正常系
        $this->assertTrue($vd->execute(array()));
        $this->assertTrue($vd->execute(array('item1' => '')));
        $this->assertTrue($vd->execute(array('item1' => NULL)));
        $this->assertTrue($vd->execute(array('item1' => FALSE)));
        $this->assertTrue($vd->execute(array('item1' => '100-9999')));
    }

    /**
     * バリデーション:電話番号形式[tel]のテスト
     * telの機能
     * data値が電話番号形式(2文字以上数値-2文字以上数値-4文字以上数値)でない場合はエラーにする
     * empty値「存在しない、空文字''、NULL、FALSE」はNGにならない
     */
    public function test_tel()
    {
        $vd = $this->obj->init();
        $vd->field('item1', 'test field [tel]')->rule('tel');
        // 異常系
        $this->assertFalse($vd->execute(array('item1' => 0)));
        $this->assertFalse($vd->execute(array('item1' => '0')));
        $this->assertFalse($vd->execute(array('item1' => '9-1000-0001')));
        $this->assertFalse($vd->execute(array('item1' => '+81-03-1000-0001')));
        $this->assertFalse($vd->execute(array('item1' => new StdClass)));
        $this->assertFalse($vd->execute(array('item1' => array('hoge'))));
        // 正常系
        $this->assertTrue($vd->execute(array()));
        $this->assertTrue($vd->execute(array('item1' => '')));
        $this->assertTrue($vd->execute(array('item1' => NULL)));
        $this->assertTrue($vd->execute(array('item1' => FALSE)));
        $this->assertTrue($vd->execute(array('item1' => '090-1234-1234')));
    }
    // }}}

    // {{{ Validation test for number
    /**
     * バリデーション:最小数値[minnum]のテスト
     * minnumの機能
     * data値が指定の数値より小さい場合はエラーにする
     * empty値「存在しない、空文字''、NULL、FALSE」はNGにならない
     */
    public function test_minnum()
    {
        $vd = $this->obj->init();
        $vd->field('item1', 'test field [minnum]');
        $vd->rule('minnum', 5);
        // 異常系
        $this->assertFalse($vd->execute(array('item1' => 0)));
        $this->assertFalse($vd->execute(array('item1' => '0')));
        $this->assertFalse($vd->execute(array('item1' => 4)));
        $this->assertFalse($vd->execute(array('item1' => '4')));
        $this->assertFalse($vd->execute(array('item1' => new StdClass)));
        $this->assertFalse($vd->execute(array('item1' => array('hoge'))));
        // 正常系
        $this->assertTrue($vd->execute(array()));
        $this->assertTrue($vd->execute(array('item1' => '')));
        $this->assertTrue($vd->execute(array('item1' => NULL)));
        $this->assertTrue($vd->execute(array('item1' => FALSE)));
        $this->assertTrue($vd->execute(array('item1' => '5')));
        $this->assertTrue($vd->execute(array('item1' => 5)));
    }

    /**
     * バリデーション:最小数値[maxnum]のテスト
     * maxnumの機能
     * data値が指定の数値より大きい場合はエラーにする
     * empty値「存在しない、空文字''、NULL、FALSE」はNGにならない
     */
    public function test_maxnum()
    {
        $vd = $this->obj->init();
        $vd->field('item1', 'test field [maxnum]');
        $vd->rule('maxnum', 5);
        // 異常系
        $this->assertFalse($vd->execute(array('item1' => 6)));
        $this->assertFalse($vd->execute(array('item1' => '6')));
        $this->assertFalse($vd->execute(array('item1' => new StdClass)));
        $this->assertFalse($vd->execute(array('item1' => array('hoge'))));
        // 正常系
        $this->assertTrue($vd->execute(array()));
        $this->assertTrue($vd->execute(array('item1' => '')));
        $this->assertTrue($vd->execute(array('item1' => NULL)));
        $this->assertTrue($vd->execute(array('item1' => FALSE)));
        $this->assertTrue($vd->execute(array('item1' => 0)));
        $this->assertTrue($vd->execute(array('item1' => '0')));
        $this->assertTrue($vd->execute(array('item1' => '5')));
        $this->assertTrue($vd->execute(array('item1' => 5)));
    }

    /**
     * バリデーション:数値[numeric]のテスト
     * numericの機能
     * data値が数値(-+.0-9 float含む)以外の場合はエラーにする
     * empty値「存在しない、空文字''、NULL、FALSE」はNGにならない
     */
    public function test_numeric()
    {
        $vd = $this->obj->init();
        $vd->field('item1', 'test field [numeric]')->rule('numeric');
        // 異常系
        $this->assertFalse($vd->execute(array('item1' => '/9')));
        $this->assertFalse($vd->execute(array('item1' => '*9')));
        $this->assertFalse($vd->execute(array('item1' => '1e4')));
        $this->assertFalse($vd->execute(array('item1' => new StdClass)));
        $this->assertFalse($vd->execute(array('item1' => array('hoge'))));
        // 正常系
        $this->assertTrue($vd->execute(array()));
        $this->assertTrue($vd->execute(array('item1' => '')));
        $this->assertTrue($vd->execute(array('item1' => NULL)));
        $this->assertTrue($vd->execute(array('item1' => FALSE)));
        $this->assertTrue($vd->execute(array('item1' => 0)));
        $this->assertTrue($vd->execute(array('item1' => '0')));
        $this->assertTrue($vd->execute(array('item1' => '+5.05')));
        $this->assertTrue($vd->execute(array('item1' => -5)));
    }

    /**
     * バリデーション:数値[isnum]のテスト
     * isnumの機能
     * data値が数値(is_numeric関数の結果がTRUE)以外の場合はエラーにする
     * empty値「存在しない、空文字''、NULL、FALSE」はNGにならない
     */
    public function test_isnum()
    {
        $vd = $this->obj->init();
        $vd->field('item1', 'test field [isnum]')->rule('isnum');
        // 異常系
        $this->assertFalse($vd->execute(array('item1' => '/9')));
        $this->assertFalse($vd->execute(array('item1' => '*9')));
        $this->assertFalse($vd->execute(array('item1' => new StdClass)));
        $this->assertFalse($vd->execute(array('item1' => array('hoge'))));
        // 正常系
        $this->assertTrue($vd->execute(array()));
        $this->assertTrue($vd->execute(array('item1' => '')));
        $this->assertTrue($vd->execute(array('item1' => NULL)));
        $this->assertTrue($vd->execute(array('item1' => FALSE)));
        $this->assertTrue($vd->execute(array('item1' => 0)));
        $this->assertTrue($vd->execute(array('item1' => '0')));
        $this->assertTrue($vd->execute(array('item1' => '+5.05')));
        $this->assertTrue($vd->execute(array('item1' => -5)));
        $this->assertTrue($vd->execute(array('item1' => '1e4')));
    }

    /**
     * バリデーション:数値[integer]のテスト
     * integerの機能
     * data値が数値(+-0-9 integer)以外の場合はエラーにする
     * empty値「存在しない、空文字''、NULL、FALSE」はNGにならない
     */
    public function test_integer()
    {
        $vd = $this->obj->init();
        $vd->field('item1', 'test field [integer]')->rule('integer');
        // 異常系
        $this->assertFalse($vd->execute(array('item1' => '9.0')));
        $this->assertFalse($vd->execute(array('item1' => '1e4')));
        $this->assertFalse($vd->execute(array('item1' => new StdClass)));
        $this->assertFalse($vd->execute(array('item1' => array('hoge'))));
        // 正常系
        $this->assertTrue($vd->execute(array()));
        $this->assertTrue($vd->execute(array('item1' => '')));
        $this->assertTrue($vd->execute(array('item1' => NULL)));
        $this->assertTrue($vd->execute(array('item1' => FALSE)));
        $this->assertTrue($vd->execute(array('item1' => 0)));
        $this->assertTrue($vd->execute(array('item1' => '0')));
        $this->assertTrue($vd->execute(array('item1' => '+5')));
        $this->assertTrue($vd->execute(array('item1' => -5)));
    }

    /**
     * バリデーション:数値[isint]のテスト
     * isintの機能
     * data値が数値(is_int関数の結果がTRUE)以外の場合はエラーにする
     * empty値「存在しない、空文字''、NULL、FALSE」はNGにならない
     */
    public function test_isint()
    {
        $vd = $this->obj->init();
        $vd->field('item1', 'test field [isint]')->rule('isint');
        // 異常系
        $this->assertFalse($vd->execute(array('item1' => '-9')));
        $this->assertFalse($vd->execute(array('item1' => '+9')));
        $this->assertFalse($vd->execute(array('item1' => '9.0')));
        $this->assertFalse($vd->execute(array('item1' => 9.05)));
        $this->assertFalse($vd->execute(array('item1' => '1e4')));
        $this->assertFalse($vd->execute(array('item1' => '0')));
        $this->assertFalse($vd->execute(array('item1' => '5')));
        $this->assertFalse($vd->execute(array('item1' => new StdClass)));
        $this->assertFalse($vd->execute(array('item1' => array('hoge'))));
        // 正常系
        $this->assertTrue($vd->execute(array()));
        $this->assertTrue($vd->execute(array('item1' => '')));
        $this->assertTrue($vd->execute(array('item1' => NULL)));
        $this->assertTrue($vd->execute(array('item1' => FALSE)));
        $this->assertTrue($vd->execute(array('item1' => 0)));
        $this->assertTrue($vd->execute(array('item1' => +0)));
        $this->assertTrue($vd->execute(array('item1' => -0)));
        $this->assertTrue($vd->execute(array('item1' => 5)));
        $this->assertTrue($vd->execute(array('item1' => +5)));
        $this->assertTrue($vd->execute(array('item1' => -5)));
    }

    /**
     * バリデーション:数値[decimal]のテスト
     * decimalの機能
     * data値が数値(+- 小数点)以外の場合はエラーにする
     * empty値「存在しない、空文字''、NULL、FALSE」はNGにならない
     */
    public function test_decimal()
    {
        $vd = $this->obj->init();
        $vd->field('item1', 'test field [decimal]')->rule('decimal');
        // 異常系
        $this->assertFalse($vd->execute(array('item1' => '-9')));
        $this->assertFalse($vd->execute(array('item1' => '+9')));
        $this->assertFalse($vd->execute(array('item1' => '1e4')));
        $this->assertFalse($vd->execute(array('item1' => 0.00))); // this deal as 0
        $this->assertFalse($vd->execute(array('item1' => '0')));
        $this->assertFalse($vd->execute(array('item1' => '5')));
        $this->assertFalse($vd->execute(array('item1' => 5)));
        $this->assertFalse($vd->execute(array('item1' => +5)));
        $this->assertFalse($vd->execute(array('item1' => -5)));
        $this->assertFalse($vd->execute(array('item1' => new StdClass)));
        $this->assertFalse($vd->execute(array('item1' => array('hoge'))));
        // 正常系
        $this->assertTrue($vd->execute(array()));
        $this->assertTrue($vd->execute(array('item1' => '')));
        $this->assertTrue($vd->execute(array('item1' => NULL)));
        $this->assertTrue($vd->execute(array('item1' => FALSE)));
        $this->assertTrue($vd->execute(array('item1' => '0.00')));
        $this->assertTrue($vd->execute(array('item1' => '+0.00')));
        $this->assertTrue($vd->execute(array('item1' => '-0.00')));
        $this->assertTrue($vd->execute(array('item1' => 1.04)));
        $this->assertTrue($vd->execute(array('item1' => 0.04)));
        $this->assertTrue($vd->execute(array('item1' => '9.0')));
        $this->assertTrue($vd->execute(array('item1' => '9.05')));
    }

    /**
     * バリデーション:数値[natural]のテスト
     * naturalの機能
     * data値が数値(自然数)以外の場合はエラーにする
     * empty値「存在しない、空文字''、NULL、FALSE」はNGにならない
     */
    public function test_natural()
    {
        $vd = $this->obj->init();
        $vd->field('item1', 'test field [natural]')->rule('natural');
        // 異常系
        $this->assertFalse($vd->execute(array('item1' => '-9')));
        $this->assertFalse($vd->execute(array('item1' => '+9')));
        $this->assertFalse($vd->execute(array('item1' => '1e4')));
        $this->assertFalse($vd->execute(array('item1' => 1.04)));
        $this->assertFalse($vd->execute(array('item1' => '1.04')));
        $this->assertFalse($vd->execute(array('item1' => -5)));
        $this->assertFalse($vd->execute(array('item1' => new StdClass)));
        $this->assertFalse($vd->execute(array('item1' => array('hoge'))));
        // 正常系
        $this->assertTrue($vd->execute(array()));
        $this->assertTrue($vd->execute(array('item1' => '')));
        $this->assertTrue($vd->execute(array('item1' => NULL)));
        $this->assertTrue($vd->execute(array('item1' => FALSE)));
        $this->assertTrue($vd->execute(array('item1' => '0')));
        $this->assertTrue($vd->execute(array('item1' => 0)));
        $this->assertTrue($vd->execute(array('item1' => '5')));
        $this->assertTrue($vd->execute(array('item1' => 5)));
        $this->assertTrue($vd->execute(array('item1' => +5)));
    }

    /**
     * バリデーション:数値[nozero]のテスト
     * nozeroの機能
     * data値が数値(0を除く自然数)以外の場合はエラーにする
     * empty値「存在しない、空文字''、NULL、FALSE」はNGにならない
     */
    public function test_nozero()
    {
        $vd = $this->obj->init();
        $vd->field('item1', 'test field [nozero]')->rule('nozero');
        // 異常系
        $this->assertFalse($vd->execute(array('item1' => '-9')));
        $this->assertFalse($vd->execute(array('item1' => '+9')));
        $this->assertFalse($vd->execute(array('item1' => '1e4')));
        $this->assertFalse($vd->execute(array('item1' => 1.04)));
        $this->assertFalse($vd->execute(array('item1' => '1.04')));
        $this->assertFalse($vd->execute(array('item1' => -5)));
        $this->assertFalse($vd->execute(array('item1' => '0')));
        $this->assertFalse($vd->execute(array('item1' => 0)));
        $this->assertFalse($vd->execute(array('item1' => new StdClass)));
        $this->assertFalse($vd->execute(array('item1' => array('hoge'))));
        // 正常系
        $this->assertTrue($vd->execute(array()));
        $this->assertTrue($vd->execute(array('item1' => '')));
        $this->assertTrue($vd->execute(array('item1' => NULL)));
        $this->assertTrue($vd->execute(array('item1' => FALSE)));
        $this->assertTrue($vd->execute(array('item1' => '5')));
        $this->assertTrue($vd->execute(array('item1' => 5)));
        $this->assertTrue($vd->execute(array('item1' => +5)));
    }

    /**
     * バリデーション:数値[range]のテスト
     * rangeの機能
     * data値が指定の数値の範囲外の場合はエラーにする
     * empty値「存在しない、空文字''、NULL、FALSE」はNGにならない
     */
    public function test_range()
    {
        $vd = $this->obj->init();
        $vd->field('item1', 'test field [range]')->rule('range', -5, 5);
        // 異常系
        $this->assertFalse($vd->execute(array('item1' => '-6.1')));
        $this->assertFalse($vd->execute(array('item1' => -6.1)));
        $this->assertFalse($vd->execute(array('item1' => '+5.1')));
        $this->assertFalse($vd->execute(array('item1' => +5.1)));
        $this->assertFalse($vd->execute(array('item1' => '1e4')));
        $this->assertFalse($vd->execute(array('item1' => new StdClass)));
        $this->assertFalse($vd->execute(array('item1' => array('hoge'))));
        // 正常系
        $this->assertTrue($vd->execute(array()));
        $this->assertTrue($vd->execute(array('item1' => '')));
        $this->assertTrue($vd->execute(array('item1' => NULL)));
        $this->assertTrue($vd->execute(array('item1' => FALSE)));
        $this->assertTrue($vd->execute(array('item1' => -5.0)));
        $this->assertTrue($vd->execute(array('item1' => '-5.0')));
        $this->assertTrue($vd->execute(array('item1' => +5.0)));
        $this->assertTrue($vd->execute(array('item1' => '+5.0')));
        $this->assertTrue($vd->execute(array('item1' => '0')));
        $this->assertTrue($vd->execute(array('item1' => 0)));
    }
    // }}}

    // {{{ Validation test for date and time
    /**
     * バリデーション:日付[date]のテスト
     * dateの機能
     * data値が不正な日付の場合はエラーにする
     * empty値「存在しない、空文字''、NULL、FALSE」はNGにならない
     */
    public function test_date()
    {
        $vd = $this->obj->init();
        $vd->field('item1', 'test field [date]')->rule('date');
        // 異常系
        $this->assertFalse($vd->execute(array('item1' => '2011-2-29')));
        $this->assertFalse($vd->execute(array('item1' => '2012-4-31')));
        $this->assertFalse($vd->execute(array('item1' => '9999-99-99')));
        $this->assertFalse($vd->execute(array('item1' => '0')));
        $this->assertFalse($vd->execute(array('item1' => 0)));
        $this->assertFalse($vd->execute(array('item1' => new StdClass)));
        $this->assertFalse($vd->execute(array('item1' => array('hoge'))));
        // 正常系
        $this->assertTrue($vd->execute(array()));
        $this->assertTrue($vd->execute(array('item1' => '')));
        $this->assertTrue($vd->execute(array('item1' => NULL)));
        $this->assertTrue($vd->execute(array('item1' => FALSE)));
        $this->assertTrue($vd->execute(array('item1' => '2012-2-29')));
    }

    /**
     * バリデーション:時間[time]のテスト
     * timeの機能
     * time値が不正な日付の場合はエラーにする
     * empty値「存在しない、空文字''、NULL、FALSE」はNGにならない
     */
    public function test_time()
    {
        $vd = $this->obj->init();
        $vd->field('item1', 'test field [time]')->rule('time');
        // 異常系
        $this->assertFalse($vd->execute(array('item1' => '25')));
        $this->assertFalse($vd->execute(array('item1' => '12:64')));
        $this->assertFalse($vd->execute(array('item1' => '12:59:-1')));
        $this->assertFalse($vd->execute(array('item1' => new StdClass)));
        $this->assertFalse($vd->execute(array('item1' => array('hoge'))));
        // 正常系
        $this->assertTrue($vd->execute(array()));
        $this->assertTrue($vd->execute(array('item1' => '')));
        $this->assertTrue($vd->execute(array('item1' => NULL)));
        $this->assertTrue($vd->execute(array('item1' => FALSE)));
        $this->assertTrue($vd->execute(array('item1' => '0')));
        $this->assertTrue($vd->execute(array('item1' => 0)));
        $this->assertTrue($vd->execute(array('item1' => '23')));
        $this->assertTrue($vd->execute(array('item1' => '23:59')));
        $this->assertTrue($vd->execute(array('item1' => '23:59:59')));
    }

    /**
     * バリデーション:日付[mindate]のテスト
     * mindateの機能
     * data値が指定の日付より過去の場合はエラーにする
     * empty値「存在しない、空文字''、NULL、FALSE」はNGにならない
     */
    public function test_mindate()
    {
        $vd = $this->obj->init();
        $vd->field('item1', 'test field [mindate]')->rule('mindate', '2012-03-20');
        // 異常系
        $this->assertFalse($vd->execute(array('item1' => '2011-2-29')));
        $this->assertFalse($vd->execute(array('item1' => '2012-2-31')));
        $this->assertFalse($vd->execute(array('item1' => '1970-01-01')));
        $this->assertFalse($vd->execute(array('item1' => '0')));
        $this->assertFalse($vd->execute(array('item1' => 0)));
        $this->assertFalse($vd->execute(array('item1' => new StdClass)));
        $this->assertFalse($vd->execute(array('item1' => array('hoge'))));
        // 正常系
        $this->assertTrue($vd->execute(array()));
        $this->assertTrue($vd->execute(array('item1' => '')));
        $this->assertTrue($vd->execute(array('item1' => NULL)));
        $this->assertTrue($vd->execute(array('item1' => FALSE)));
        $this->assertTrue($vd->execute(array('item1' => '2012-3-20')));
        $this->assertTrue($vd->execute(array('item1' => '2012-3-21')));
    }

    /**
     * バリデーション:日付[maxdate]のテスト
     * maxdateの機能
     * data値が指定の日付より未来の場合はエラーにする
     * empty値「存在しない、空文字''、NULL、FALSE」はNGにならない
     */
    public function test_maxdate()
    {
        $vd = $this->obj->init();
        $vd->field('item1', 'test field [maxdate]')->rule('maxdate', '2012-03-20');
        // 異常系
        $this->assertFalse($vd->execute(array('item1' => '2011-3-29')));
        $this->assertFalse($vd->execute(array('item1' => '2012-3-31')));
        $this->assertFalse($vd->execute(array('item1' => '9999-99-99')));
        $this->assertFalse($vd->execute(array('item1' => '0')));
        $this->assertFalse($vd->execute(array('item1' => 0)));
        $this->assertFalse($vd->execute(array('item1' => new StdClass)));
        $this->assertFalse($vd->execute(array('item1' => array('hoge'))));
        // 正常系
        $this->assertTrue($vd->execute(array()));
        $this->assertTrue($vd->execute(array('item1' => '')));
        $this->assertTrue($vd->execute(array('item1' => NULL)));
        $this->assertTrue($vd->execute(array('item1' => FALSE)));
        $this->assertTrue($vd->execute(array('item1' => '2012-3-20')));
        $this->assertTrue($vd->execute(array('item1' => '2012-3-19')));
    }

    /**
     * バリデーション:日付[termdate]のテスト
     * termdateの機能
     * data値が指定の日付期間外の場合はエラーにする
     * empty値「存在しない、空文字''、NULL、FALSE」はNGにならない
     */
    public function test_termdate()
    {
        $vd = $this->obj->init();
        $vd->field('item1', 'test field [termdate]')->rule('termdate', '2011-12-30', '2012-03-20');
        // 異常系
        $this->assertFalse($vd->execute(array('item1' => '2011-3-29')));
        $this->assertFalse($vd->execute(array('item1' => '2012-3-31')));
        $this->assertFalse($vd->execute(array('item1' => '9999-99-99')));
        $this->assertFalse($vd->execute(array('item1' => '0')));
        $this->assertFalse($vd->execute(array('item1' => 0)));
        $this->assertFalse($vd->execute(array('item1' => new StdClass)));
        $this->assertFalse($vd->execute(array('item1' => array('hoge'))));
        // 正常系
        $this->assertTrue($vd->execute(array()));
        $this->assertTrue($vd->execute(array('item1' => '')));
        $this->assertTrue($vd->execute(array('item1' => NULL)));
        $this->assertTrue($vd->execute(array('item1' => FALSE)));
        $this->assertTrue($vd->execute(array('item1' => '2011-12-30')));
        $this->assertTrue($vd->execute(array('item1' => '2012-1-1')));
        $this->assertTrue($vd->execute(array('item1' => '2012-2-29')));
        $this->assertTrue($vd->execute(array('item1' => '2012-3-20')));
    }

    /**
     * バリデーション:日付[termdatetime]のテスト
     * termdatetimeの機能
     * data値が指定の日時期間外の場合はエラーにする
     * empty値「存在しない、空文字''、NULL、FALSE」はNGにならない
     */
    public function test_termdatetime()
    {
        $vd = $this->obj->init();
        $vd->field('item1', 'test field [termdatetime]')->rule('termdatetime', '2011-12-30 23:59:59', '2012-03-20 23:59:50');
        // 異常系
        $this->assertFalse($vd->execute(array('item1' => '2011-12-30 23:59:58')));
        $this->assertFalse($vd->execute(array('item1' => '2012-3-20 23:59:51')));
        $this->assertFalse($vd->execute(array('item1' => '9999-99-99')));
        $this->assertFalse($vd->execute(array('item1' => '0')));
        $this->assertFalse($vd->execute(array('item1' => 0)));
        $this->assertFalse($vd->execute(array('item1' => new StdClass)));
        $this->assertFalse($vd->execute(array('item1' => array('hoge'))));
        // 正常系
        $this->assertTrue($vd->execute(array()));
        $this->assertTrue($vd->execute(array('item1' => '')));
        $this->assertTrue($vd->execute(array('item1' => NULL)));
        $this->assertTrue($vd->execute(array('item1' => FALSE)));
        $this->assertTrue($vd->execute(array('item1' => '2011-12-30 23:59:59')));
        $this->assertTrue($vd->execute(array('item1' => '2012-1-1 00:00:00')));
        $this->assertTrue($vd->execute(array('item1' => '2012-2-29 23:59:59')));
        $this->assertTrue($vd->execute(array('item1' => '2012-3-20 23:59:50')));
    }
    // }}}

    // {{{ Validation test for Japanese string
    /**
     * バリデーション:ひらがな[hiragana]のテスト
     * hiraganaの機能
     * data値がひらがな以外が含まれる場合はエラーにする
     * ひらがなには、一部記号「・」(全角中点)、「ー」(全角長音)を含む
     * empty値「存在しない、空文字''、NULL、FALSE」はNGにならない
     */
    public function test_hiragana()
    {
        $vd = $this->obj->init();
        $vd->field('item1', 'test field [hiragana]')->rule('hiragana');
        // 異常系
        $row = array(
            'e3818*' => '0               ',
            'e3829*' => '       78       ',
            'e383b*' => '0123456789a  def',
        );
        foreach ($row as $code => $val) {
            foreach ($this->hex as $h) {
                if (strpos($val, $h) !== FALSE) {
                    $str = pack('H*', str_replace('*', $h, $code));
                    $this->assertFalse($vd->execute(array('item1' => $str)));
                }
            }
        }
        $this->assertFalse($vd->execute(array('item1' => '0')));
        $this->assertFalse($vd->execute(array('item1' => 0)));
        $this->assertFalse($vd->execute(array('item1' => new StdClass)));
        $this->assertFalse($vd->execute(array('item1' => array('hoge'))));
        // 正常系
        $this->assertTrue($vd->execute(array()));
        $this->assertTrue($vd->execute(array('item1' => '')));
        $this->assertTrue($vd->execute(array('item1' => NULL)));
        $this->assertTrue($vd->execute(array('item1' => FALSE)));
        $row = array(
            'e3818*' => ' 123456789abcdef',
            'e3819*' => '0123456789abcdef', // full range
            'e381a*' => '0123456789abcdef', // full range
            'e381b*' => '0123456789abcdef', // full range
            'e3828*' => '0123456789abcdef', // full range
            'e3829*' => '0123456  9abcdef',
            'e383b*' => '           bc   ',
        );
        $str = '';
        foreach ($row as $code => $val) {
            foreach ($this->hex as $h) {
                if (strpos($val, $h) !== FALSE) {
                    $str .= pack('H*', str_replace('*', $h, $code));
                }
            }
        }
        $this->assertTrue($vd->execute(array('item1' => $str)));
    }

    /**
     * バリデーション:全角カタカナ[fullkana]のテスト
     * fullkanaの機能
     * data値が全角カタカナ以外が含まれる場合はエラーにする
     * 全角カタカナには、一部記号「・」(全角中点)、「ー」(全角長音)を含む
     * empty値「存在しない、空文字''、NULL、FALSE」はNGにならない
     */
    public function test_fullkana()
    {
        $vd = $this->obj->init();
        $vd->field('item1', 'test field [fullkana]')->rule('fullkana');
        // 異常系
        $row = array(
            'e3828*' => '0               ',
            'e3848*' => '0123456789abcdef',
            'e387a*' => '0123456789abcdef',
            'e3888*' => '0123456789abcdef',
        );
        foreach ($row as $code => $val) {
            foreach ($this->hex as $h) {
                if (strpos($val, $h) !== FALSE) {
                    $str = pack('H*', str_replace('*', $h, $code));
                    $this->assertFalse($vd->execute(array('item1' => $str)));
                }
            }
        }
        $this->assertFalse($vd->execute(array('item1' => '0')));
        $this->assertFalse($vd->execute(array('item1' => 0)));
        $this->assertFalse($vd->execute(array('item1' => new StdClass)));
        $this->assertFalse($vd->execute(array('item1' => array('hoge'))));
        // 正常系
        $this->assertTrue($vd->execute(array()));
        $this->assertTrue($vd->execute(array('item1' => '')));
        $this->assertTrue($vd->execute(array('item1' => NULL)));
        $this->assertTrue($vd->execute(array('item1' => FALSE)));
        $row = array(
            'e382a*' => ' 123456789abcdef',
            'e382b*' => '0123456789abcdef', // full range
            'e3838*' => '0123456789abcdef', // full range
            'e3839*' => '0123456789abcdef', // full range
            'e383a*' => '0123456789abcdef', // full range
            'e383b*' => '0123456789abcdef', // full range
            'e387b*' => '0123456789abcdef', // full range
        );
        $str = '';
        foreach ($row as $code => $val) {
            foreach ($this->hex as $h) {
                if (strpos($val, $h) !== FALSE) {
                    $str .= pack('H*', str_replace('*', $h, $code));
                }
            }
        }
        $this->assertTrue($vd->execute(array('item1' => $str)));
    }

    /**
     * バリデーション:半角カタカナ[halfkana]のテスト
     * halfkanaの機能
     * data値が半角カタカナ以外が含まれる場合はエラーにする
     * 半角カタカナには、一部記号「･」(半角中点)、「ｰ」(半角長音)を含む
     * empty値「存在しない、空文字''、NULL、FALSE」はNGにならない
     */
    public function test_halfkana()
    {
        $vd = $this->obj->init();
        $vd->field('item1', 'test field [halfkana]')->rule('halfkana');
        // 異常系
        $row = array(
            'efbd9*' => '0123456789abcdef',
            'efbda*' => '01234           ',
            'efbea*' => '0123456789abcdef',
        );
        foreach ($row as $code => $val) {
            foreach ($this->hex as $h) {
                if (strpos($val, $h) !== FALSE) {
                    $str = pack('H*', str_replace('*', $h, $code));
                    $this->assertFalse($vd->execute(array('item1' => $str)));
                }
            }
        }
        $this->assertFalse($vd->execute(array('item1' => '0')));
        $this->assertFalse($vd->execute(array('item1' => 0)));
        $this->assertFalse($vd->execute(array('item1' => new StdClass)));
        $this->assertFalse($vd->execute(array('item1' => array('hoge'))));
        // 正常系
        $this->assertTrue($vd->execute(array()));
        $this->assertTrue($vd->execute(array('item1' => '')));
        $this->assertTrue($vd->execute(array('item1' => NULL)));
        $this->assertTrue($vd->execute(array('item1' => FALSE)));
        $row = array(
            'efbda*' => '     56789abcdef',
            'efbdb*' => '0123456789abcdef', // full range
            'efbe8*' => '0123456789abcdef', // full range
            'efbe9*' => '0123456789abcdef', // full range
        );
        $str = '';
        foreach ($row as $code => $val) {
            foreach ($this->hex as $h) {
                if (strpos($val, $h) !== FALSE) {
                    $str .= pack('H*', str_replace('*', $h, $code));
                }
            }
        }
        $this->assertTrue($vd->execute(array('item1' => $str)));
    }

    // }}}

}

