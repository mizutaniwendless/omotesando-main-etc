<?php

class Common
{

    private $rollback = FALSE;
    private $skip_columns = array('inserted_at', 'updated_at');

    public function args($argv)
    {
        $params = array();
        foreach ($argv as $param) {
            if (preg_match('/^--([a-z-_]+)=(.*)$/', $param, $match)) {
                $params[$match[1]] = $match[2];
            }
        }
        return $params;
    }

    public function config($params)
    {
        static $config = NULL;
        if (! is_null($config)) { return $config; }
        $pname = 'app,user,pass,db,table';
        try {
            // check arguments
            // --user=, --pass=, --db=, --table=
            foreach (explode(',', $pname) as $var) {
                if (! isset($params[$var])) {
                    throw new Exception('error');
                }
            }
            $params['host'] = isset($params['host']) ? $params['host'] : 'localhost';
            $config = $params;
            return $config;
        } catch (Exception $e) {
            list($app, $table) = array_slice($_SERVER['argv'], 2);
            $conf = array(
                conf('core/user_apps_conf_path'),
                conf('core/user_apps_path') . $app . DS . ZIU_CONF_DIR . DS,
            );
            $tmp = array();
            foreach ($conf as $path) {
                $path = str_replace('/', DS, $path);
                $file = $path . 'db.php';
                if (is_readable($file)) {
                    $data = include_once $file;
                    if (is_array($data)) {
                        $tmp = $data + $tmp;
                    }
                }
            }
            if (isset($tmp['connection'][$tmp['connection_name']])) {
                $con = $tmp['connection'][$tmp['connection_name']];
                list($sche, $info) = explode(':', $con['dsn']);
                $dsn = array();
                foreach (explode(';', $info) as $str) {
                    list($key, $val) = explode('=', $str);
                    $dsn[$key] = $val;
                }
                if (isset($dsn['dbname']) && isset($dsn['host'])) {
                    $config = array(
                        'user' => $con['user'],
                        'pass' => $con['pass'],
                        'db'   => $dsn['dbname'],
                        'host' => $dsn['host'],
                        'table' => $table,
                        'app'   => $app,
                    );
                    return $config;
                }
            }
        }
        $usage = "
Error: no database connection information.
You can use 2 method to connect database below.
[method: 1]
use db.php configuration file in apps/.conf or apps/APP_NAME/.conf.
----------------------------------------------
php -q script/gen [target] APP_NAME TABLE

[method: 2]
indicate configuration info with command.
----------------------------------------------
php -q script/gen [target] --app=APP_NAME --user=USER --pass=PASS --db=DBNAME --table=TABLE

";
        die($usage);
    }

    public function schema($p)
    {
        $host = empty($p['host']) ? 'localhost' : $p['host'];
        $conf = array(
            'dsn' => "mysql:host={$host};dbname={$p['db']}",
            'user' => $p['user'],
            'pass' => $p['pass'],
        );
        $db = lib('database', $conf);
        $sch = $db->query('show full columns from ' . $p['table'])->all();
        $res = array('table_id' => '', 'columns' => array());
        foreach ($sch as $val) {
            if ($val['Key'] == 'PRI') {
                $res['table_id'] = $val['Field'];
            }
            $res['columns'][] = array(
                'field' => $val['Field'],
                'type'  => $val['Type'],
                'null'  => $val['Null'],
                'key'   => $val['Key'],
            );
        }
        return $res;
    }

    public function filepath($params)
    {
        extract($params);
        $table = strtolower(str_replace('_', '', $table));
        $app_path = conf('core/user_apps_path') . $app . DS;
        $paths = array(
            'logic' => $app_path . $table . '.' . conf('core/suffix_logic') . '.php',
            'model' => $app_path . ZIU_MODEL_DIR . DS . $table . '.php',
            'view/index' => $app_path . $table . DS . '.index.' . conf('core/suffix_view') . '.php',
            'view/input' => $app_path . $table . DS . '.input.' . conf('core/suffix_view') . '.php',
            'view/confirm' => $app_path . $table . DS . '.confirm.' . conf('core/suffix_view') . '.php',
            'view/detail' => $app_path . $table . DS . '.detail.' . conf('core/suffix_view') . '.php',
            'view/complete' => $app_path . $table . DS . '.complete.' . conf('core/suffix_view') . '.php',
            'unit/index' => $app_path . ZIU_UNIT_DIR . DS . $table . DS . 'index.php',
            'unit/default' => $app_path . ZIU_UNIT_DIR . DS . $table . DS . '.default.php',
        );
        return $paths;
    }

    public function validate($paths)
    {
        $viewdir = dirname($paths['view/index']);
        $unitdir = dirname($paths['unit/index']);
        $appdir  = dirname($paths['logic']);
        if (is_dir($viewdir)) {
            throw new Exception("Error: [$viewdir] already exists. then exit.\n");
        }
        if (is_dir($unitdir)) {
            throw new Exception("Error: [$unitdir] already exists. then exit.\n");
        }
        foreach ($paths as $key => $file) {
            if (is_file($file)) {
                throw new Exception("Error: [$file] already exists. then exit.\n");
            }
        }
    }

    public function create($template, $paths)
    {
        $this->rollback = TRUE;
        $viewdir = dirname($paths['view/index']);
        $unitdir = dirname($paths['unit/index']);
        $appdir  = dirname($paths['logic']);
        if (! is_dir($appdir)) {
            if (! mkdir($appdir)) {
                throw new Exception("Error: fail to create directory [$appdir]\n");
            }
            echo("Create directory: $appdir\n");
        }
        if (! mkdir($viewdir)) {
            throw new Exception("Error: fail to create directory [$viewdir]\n");
        }
        echo("Create directory: $viewdir\n");
        if (! mkdir($unitdir)) {
            throw new Exception("Error: fail to create directory [$unitdir]\n");
        }
        echo("Create directory: $unitdir\n");
        // create file
        foreach ($paths as $key => $file) {
            if (($fp = fopen($file, 'w')) !== FALSE) {
                flock($fp, LOCK_EX);
                fwrite($fp, $template[$key]);
                flock($fp, LOCK_UN);
                fclose($fp);
                echo("Create file: $file\n");
            } else {
                throw new Exception("Error: fail to create file [$viewdir]\n");
            }
        }
    }

    public function rollback($paths, $force = FALSE)
    {
        echo("Rollback...\n");
        if (! $this->rollback && $force === FALSE) {
            echo("...do nothing, cause no files and directories created...\n");
            return;
        }
        $viewdir = dirname($paths['view/index']);
        $unitdir = dirname($paths['unit/index']);
        foreach ($paths as $file) {
            if (is_file($file)) {
                unlink($file);
                echo("Remove file: $file\n");
            }
        }
        if (is_dir($viewdir)) {
            rmdir($viewdir);
            echo("Remove directory: $viewdir\n");
        }
        if (is_dir($unitdir)) {
            rmdir($unitdir);
            echo("Remove directory: $unitdir\n");
        }
    }

    public function invoke($name, $params = array())
    {
        ob_start();
        invoke($name, $params);
        return ob_get_clean();
    }


    public function template($name, $map, $ext = 'php')
    {
        $dir = dirname(dirname(__FILE__));
        $name = str_replace('/', DS, $name);
        $tmpl = file_get_contents($dir . DS . '.template' . DS . $name . '.' . $ext);
        foreach ($map as $key => $val) {
            $tmpl = str_replace($key, $val, $tmpl);
        }
        return $tmpl;
    }

    public function skip_column($name)
    {
        return in_array($name, $this->skip_columns);
    }

}

