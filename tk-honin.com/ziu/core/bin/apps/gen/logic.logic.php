<?php

class Logic
{

    public function init()
    {
    }

    public function main()
    {
        try {
            $lib = lib('common');
            $params = $lib->args(func_get_args());
            $params = $lib->config($params);
            $schema = $lib->schema($params);
            $table  = str_replace(' ', '', str_replace('_', ' ', $params['table']));
            $class  = str_replace(' ', '', ucwords(str_replace('_', ' ', $params['table'])));
            $template = $lib->template('logic', array(
                '#table#' => $table,
                '#Table#' => $class,
                '#table_id#' => $schema['table_id'],
            ));
            echo($template);
        } catch (Exception $e) {
            echo($e->getMessage());
        }
    }

}

