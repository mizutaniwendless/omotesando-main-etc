<?php
/* vim:se et ts=4 sw=4 sts=4: */
/**
 * Ziu Core Database Setting.
 * 
 * LICENSE: MIT License.
 * 
 * @copyright 2012 Topazos, Inc.
 * @since File available since Release 1.0.0
 */

return array(

    // Indicate connection name there are 'development', 'staging' or 'production'
    'connection_name' => 'development',

    // Connection information for PDO
    'connection' => array(
        'development' => array(
            'dsn' => 'mysql:host=localhost;dbname=sample_dev',
            'user' => 'gaga',
            'pass' => '123456',
        ),
        'staging' => array(
            'dsn' => 'mysql:host=localhost;dbname=sample_stg',
            'user' => 'gaga',
            'pass' => '123456',
        ),
        'production' => array(
            'dsn' => 'mysql:host=localhost;dbname=sample',
            'user' => 'gaga',
            'pass' => '123456',
        ),
    ),

    // Default model super class name
    'default_model_super_class' => 'Model_Super',

);

