<?php
/* vim:se et ts=4 sw=4 sts=4: */
/**
 * Ziu Core Base Uri Engine.
 * 
 * LICENSE: MIT License.
 * 
 * @copyright 2012 Topazos, Inc.
 * @since File available since Release 1.0.0
 */

class Ziu_Uri
{

    /**
     * Constructor
     */
    function __construct()
    {
    }

    /**
     * Parse
     * @return string
     */
    public function parse()
    {
        $this->_parse_uri_string();
        return $this->uri_string;
    }

    /**
     * Parse Uri String
     * @return void
     */
    private function _parse_uri_string()
    {
        if (php_sapi_name() == 'cli' or defined('STDIN')) {
            // Is the request coming from the command line?
            $this->_set_uri_string($this->_detect_from_cli());
        } elseif ($uri = $this->_detect_from_req()) {
            // Let's try the REQUEST_URI first, this will work in most situations
            $this->_set_uri_string($uri);
        } else {
            // Is there a PATH_INFO variable?
            // Note: some servers seem to have trouble with getenv() so we'll test it two ways
            $self = pathinfo($_SERVER['SCRIPT_NAME'], PATHINFO_BASENAME);
            $path = (isset($_SERVER['PATH_INFO'])) ? $_SERVER['PATH_INFO'] : @getenv('PATH_INFO');
            if (trim($path, '/') != '' && $path != "/" . $self) {
                $this->_set_uri_string($path);
                return;
            }
            // No PATH_INFO?... What about QUERY_STRING?
            $path =  (isset($_SERVER['QUERY_STRING'])) ? $_SERVER['QUERY_STRING'] : @getenv('QUERY_STRING');
            if (trim($path, '/') != '') {
                $this->_set_uri_string($path);
                return;
            }
            // We've exhausted all our options...
            $this->_set_uri_string('');
        }
    }

    /**
     * Set Uri String
     * @param string $str : Request uri
     * @return void
     */
    private function _set_uri_string($str)
    {
        // Filter out control characters
        //$str = remove_invisible_characters($str, FALSE);
        // If the URI contains only a slash we'll kill it
        $this->uri_string = ($str == '/') ? '' : $str;
    }

    /**
     * Get Uri String from Cli
     * @return string
     */
    private function _detect_from_cli()
    {
        $args = array_slice($_SERVER['argv'], 1);
        $uri = $args ? '/' . implode('/', $args) : '';
        if ($uri == '/' || empty($uri)) {
            $result = '/';
        } else {
            $uri = parse_url($uri);
            // Do some final cleaning of the URI and return it
            $result = str_replace(array('//', '../'), '/', trim($uri['path'], '/'));
        }
        return $result;
    }

    /**
     * Get Uri String from Req
     * @return string
     */
    private function _detect_from_req()
    {
        $result = '';
        if (! isset($_SERVER['REQUEST_URI']) || ! isset($_SERVER['SCRIPT_NAME'])) {
            // no request
            return $result;
        }
        // uri
        $uri = $_SERVER['REQUEST_URI'];
        if (strpos($uri, $_SERVER['SCRIPT_NAME']) === 0) {
            // Including Script Path like '/index.php'
            // Then remove FilePath of Uri.
            $uri = substr($uri, strlen($_SERVER['SCRIPT_NAME']));
        } elseif (strpos($uri, dirname($_SERVER['SCRIPT_NAME'])) === 0) {
            // Just Dir Script Path like '/dirname'
            // Then remove DirPath of Uri.
            $uri = substr($uri, strlen(dirname($_SERVER['SCRIPT_NAME'])));
        }
        // Parts of Query String
        $parts = explode('?', $uri);
        $uri = $parts[0];
        if (isset($parts[1])) {
            $_SERVER['QUERY_STRING'] = $parts[1];
            parse_str($_SERVER['QUERY_STRING'], $_GET);
        } else {
            $_SERVER['QUERY_STRING'] = '';
            $_GET = array();
        }
        // Result
        if ($uri == '/' || empty($uri)) {
            $result = '/';
        } else {
            $uri = parse_url($uri);
            // Do some final cleaning of the URI and return it
            $result = str_replace(array('//', '../'), '/', trim($uri['path'], '/'));
        }
        return $result;
    }

}

