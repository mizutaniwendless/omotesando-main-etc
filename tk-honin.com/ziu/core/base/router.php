<?php
/* vim:se et ts=4 sw=4 sts=4: */
/**
 * Ziu Core Base Router Engine.
 * 
 * LICENSE: MIT License.
 * 
 * @copyright 2012 Topazos, Inc.
 * @since File available since Release 1.0.0
 */

class Ziu_Router
{

    /**
     * Routed Parts
     */
    private $directory, $class, $method, $args, $action;
    private $routes; // routes setting from conf
    private $_def, $_404, $_500; // static routes
    private $main_module = FALSE;
    private $main_action = FALSE;
    private $main_module_segments = array();

    /**
     * Constructor
     */
    public function __construct()
    {
    }

    /**
     * Init
     * @return void
     */
    public function init()
    {
        $this->conf = $this->loader->conf('core');
        $this->directory = NULL;
        $this->class = NULL;
        $this->method = NULL;
        $this->args = NULL;
        $this->action = NULL;
    }

    /**
     * Routing
     * @param string $uri : uri of routing
     * @return object this clone
     */
    public function routing($uri)
    {
        $this->_config();
        $this->_parse($uri);
        if ($this->main_module === FALSE) {
            $this->main_module = str_replace(DS, '/', $this->r_module());
            $this->main_action = str_replace(DS, '/', $this->r_action());
            $this->main_module_segments = $this->r_module_segments();
        }
        return clone $this;
    }

    /**
     * Configure the routes map
     * @return void
     */
    private function _config()
    {
        // Set Mapping Route
        $this->routes = $this->loader->conf('routes');
        // Set static route setting
        $this->_def = (! isset($this->routes['_def_']) || $this->routes['_def_'] == '') ? FALSE : strtolower($this->routes['_def_']);
        $this->_404 = (! isset($this->routes['_404_']) || $this->routes['_404_'] == '') ? FALSE : strtolower($this->routes['_404_']);
        $this->_500 = (! isset($this->routes['_500_']) || $this->routes['_500_'] == '') ? FALSE : strtolower($this->routes['_500_']);
    }

    /**
     * Set default controller
     * @return void
     */
    private function _set_def()
    {
        static $uri = NULL;
        if ($this->_def === FALSE) {
            l_error('No _def_ setting', __METHOD__);
            die('No _def_ setting');
        } elseif ($this->_def === '') {
            l_error('No _def_ module ' . $uri, __METHOD__);
            die('No _def_ module ' . $uri);
        } elseif (is_null($uri)) {
            // remove _def_, when use at 1st time.
            // prevent infinity loop.
            $uri = $this->_def;
            $this->_def = '';
        }
        $this->_set_route($uri);
    }

    /**
     * Show http message
     * @param string  $message : http status message
     * @return void
     */
    private function _show_http_message($message)
    {
        l_error($message, __METHOD__);
        die($message);
    }

    /**
     * Set http status controller
     * @param integer $code : http status code
     * @return void
     */
    private function _set_http_status($code)
    {
        static $uri = NULL;
        // set http header
        switch ($code) {
            case '404' :
                $status = 'Not Found';
                break;
            default :
                $status = 'Internal Server Error';
        }
        if (! headers_sent()) {
            header("HTTP/1.0 {$code} {$status}");
        }
        // set http status message
        $var = '_' . $code;
        if (! isset($this->$var) || $this->$var === FALSE) {
            $this->_show_http_message("No _{$code}_ setting");
        } elseif ($this->$var === '') {
            $this->_show_http_message("No _{$code}_ module " . $uri);
        } elseif (is_null($uri)) {
            // remove _uri_, when use at 1st time.
            // prevent infinity loop.
            $uri = $this->$var;
            $this->$var = '';
        }
        $this->_set_route($uri);
    }

    /**
     * Set route
     * @param string $uri : uri of routing
     * @return void
     */
    private function _set_route($uri = '')
    {
        $is_module_dir = FALSE;
        $segments = $this->_validate(explode('/', $uri), $is_module_dir);
        if (! is_array($segments)) {
            switch ($segments) {
                case 'def' : // _def_
                    if ($this->_is_module($this->_def)) {
                        return $this->_set_def();
                    } else {
                        $segments = array('index', 'index');
                    }
                    break;
                default :    // _400_
                    return $this->_set_http_status($segments);
            }
        }
        if ($is_module_dir) {
            $this->r_directory(array_shift($segments));
        }
        if (count($segments) == 1) {
            // adjust default method with no indication
            // ex. /hoge route Hoge::index
            // or  /hoge/fuga route Hoge_Fuga::index
            $segments = array($segments[0], 'index');
        }
        $this->r_class(array_shift($segments));
        if (is_numeric($segments[0])) {
            // ex. /hoge/edit/23 route Hoge_Edit::index(23)
            $this->r_method('index');
        } else {
            $this->r_method(array_shift($segments));
        }
        // Over Segments
        $this->r_args($segments);
    }

    /**
     * Is module?
     * @param string $path : path of module in app
     * @return boolean
     */
    public function is_module($path)
    {
        return $this->_is_module($path);
    }

    /**
     * Is module?
     * @param string $path : path of module in app
     * @return boolean
     */
    private function _is_module($path)
    {
        $result = FALSE;
        foreach (explode(',', $this->conf['suffix_join']) as $type) {
            $file = $this->conf['app_path'] . $path . '.' . $type . '.php';
            if (file_exists($file)) {
                $result = TRUE;
                break;
            }
        }
        return $result;
    }

    /**
     * Is module dir
     * @param string $path : path of module directory in app
     * @return boolean
     */
    private function _is_module_dir($path)
    {
        return is_dir($this->conf['app_path'] . $path);
    }

    /**
     * Validate segments
     * @param array   $segments       : array of segments
     * @param boolean &$is_module_dir : flag of existing module dir
     * @return array segments
     */
    private function _validate(array $segments, &$is_module_dir)
    {
        if (($cnt = count($segments)) == 0 || ($cnt == 1 && $segments[0] === '')) {
            // no segments...(ex. /)
            return 'def';
        }
        if ($this->_is_module($segments[0])) {
            // controller exists under app dir...
            if (isset($segments[1])) {
                // (ex. /hoge/fuga route Hoge::fuga)
                return $segments;
            } else {
                // (ex. /hoge route Hoge::Index)
                return array($segments[0], 'index');
            }
        }
        if ($is_module_dir = $this->_is_module_dir($segments[0])) {
            // module dir exists ...(ex. /hoge/ or /hoge/fuga)
            // if indicated controller name...
            if (count($segments) > 1) {
                if ($this->_is_module($segments[0] . DS . $segments[1])) {
                    // (ex. /hoge/fuga route Hoge_Fuga::[method])
                    return $segments;
                } elseif ($this->_is_module($segments[0] . DS . 'index')) {
                    // (ex. /hoge/fuga route Hoge_Index::fuga)
                    return array_merge(array($segments[0], 'index'), array_slice($segments, 1));
                }
            } elseif ($this->_is_module($segments[0] . DS . 'index')) {
                    // (ex. /hoge/ route Hoge_Index::index)
                    return array($segments[0], 'index', 'index');
            }
        }
        return '404';
    }

    /**
     * Parse routes
     * @param string $uri : uri of routing
     * @return void
     */
    private function _parse($uri)
    {
        if (isset($this->routes[$uri])) {
            // match string routes...
            return $this->_set_route($this->routes[$uri]);
        }
        foreach ($this->routes as $key => $val) {
            // looking for regex
            $rex = array(
                        array(':any', '.*'),
                        array(':num', '[0-9]+'),
                        array(':seg', '[^/]+'),
                    );
            foreach ($rex as $r) {
                $key = str_replace($r[0], $r[1], $key);
            }
            if (preg_match('#^'.$key.'$#', $uri)) {
                // match regex routes...
                if (strpos($val, '$') !== FALSE && strpos($key, '(') !== FALSE) {
                    // if exist $ reference, replace...
                    $val = preg_replace('#^'.$key.'$#', $val, $uri);
                }
                return $this->_set_route(trim($val, '/'));
            }
        }
        // If we got this far it means we didn't encounter a
        // matching route so we'll set the site default route
        $this->_set_route($uri);
    }

    // {{{ setter / getter
    /**
     * Set/Get the route directory name
     * @param string $dir : directory name of module
     * @return mixed void or directory name of module
     */
    public function r_directory($dir = FALSE)
    {
        if ($dir === FALSE) {
            return $this->directory;
        } else {
            $this->directory = str_replace(array(DS, '.'), '', $dir) . DS;
        }
    }

    /**
     * Set/Get the route class name
     * @param string $class : class name of module
     * @return mixed void or class name of module
     */
    public function r_class($class = FALSE)
    {
        if ($class === FALSE) {
            return $this->class;
        } else {
            $this->class = str_replace(array(DS, '.'), '', $class);
        }
    }

    /**
     * Set/Get the route method name
     * @param string $method : method name of module
     * @return mixed void or method name of module
     */
    public function r_method($method = FALSE)
    {
        if ($method === FALSE) {
            return ($this->method == $this->r_class() ? 'index' : $this->method);
        } else {
            $this->method = str_replace(array(DS, '.'), '', $method);
        }
    }

    /**
     * Set/Get the route args
     * @param array $args : args of module
     * @return mixed void or args
     */
    public function r_args($args = FALSE)
    {
        if ($args === FALSE) {
            return $this->args;
        } else {
            $this->args = $args;
        }
    }

    /**
     * Get the route action string
     * @return string action uri
     */
    public function r_action()
    {
        $action = $this->r_module();
        $args = $this->r_args();
        if ($args) {
            $action .= '/' . implode('/', $args);
        }
        return $action;
    }

    /**
     * Get the route main module name
     * @return string main module uri
     */
    public function r_main_module()
    {
        return $this->main_module;
    }

    /**
     * Get the route main action name
     * @return string main action uri
     */
    public function r_main_action()
    {
        return $this->main_action;
    }

    /**
     * Get the route current module name
     * @return string module uri
     */
    public function r_module()
    {
        $str = $this->r_directory();
        $str .= $this->r_class();
        $str .= '/' . $this->r_method();
        return trim($str, '/');
    }

    /**
     * Get the route current module segments
     * @return array module segments
     */
    public function r_module_segments()
    {
        return array(
            'directory' => str_replace(array('\\', '/'), '', $this->r_directory()),
            'class' => $this->r_class(),
            'method' => $this->r_method(),
            'args' => $this->r_args(),
        );
    }

    /**
     * Get the route main module segments
     * @return array main module segments
     */
    public function r_main_module_segments()
    {
        return $this->main_module_segments;
    }
    // }}}

}

