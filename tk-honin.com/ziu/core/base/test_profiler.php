<?php
/* vim:se et ts=4 sw=4 sts=4: */
/**
 * Ziu Core Base Profiler Engine.
 * NOTE: Use vender library of PhpQuickProfiler
 * 
 * LICENSE: MIT License.
 * 
 * @copyright 2012 Topazos, Inc.
 * @since File available since Release 1.0.0
 */

class Ziu_Profiler
{

    /**
     * PHP Quick Profiler Instance
     * @var object
     */
    private $profile = NULL;
    private $console = NULL;

    /**
     * Flag of active
     * @var boolean
     */
    private $active = FALSE;

    /**
     * Database sql time
     * @var array
     */
    private $sql = array();

    /**
     * Constructor
     */
    public function __construct()
    {
        import('vendor/phpquickprofiler/console');
        import('vendor/phpquickprofiler/phpquickprofiler');
    }

    public function init()
    {
        $active = $this->loader->conf('core/profiling');
        $this->active = ($active === TRUE);
        if ($this->active) {
            if (! is_dev() && ! is_ajax() && ! $this->profile) {
                $this->console = new Console;
                $this->profile = new PhpQuickProfiler(ZIU_START_TIME);
                $this->profile->queries = array();
                $this->profile->queryCount = 0;
            }
        }
    }

    public function time($label)
    {
        if ($this->active && $this->profile) {
            $this->console->logSpeed($label);
        }
    }

    public function memory($var = FALSE, $name = 'PHP')
    {
        if ($this->active && $this->profile) {
            $this->console->logMemory($var, $name);
        }
    }

    public function console($text)
    {
        if ($this->active && $this->profile) {
            $this->console->log($text);
        }
    }

    public function display()
    {
        if ($this->active && $this->profile) {
            return $this->profile->display($this->profile);
        }
    }

    public function sql_start($dbname, $sql)
    {
        if ($this->active && $this->profile) {
            $this->sql = array(
                'sql' => htmlspecialchars($sql, ENT_QUOTES),
                'time' => $this->profile->getMicroTime(),
            );
            return TRUE;
        }
        return FALSE;
    }

    public function sql_end($text)
    {
        if ($this->active && $this->profile) {
            $this->sql['time'] = ($this->profile->getMicroTime() - $this->sql['time']) * 1000;
            array_push($this->profile->queries, $this->sql);
            $this->profile->queryCount++;
        }
    }

    public function sql_init($text)
    {
        $this->sql = NULL;
    }

    public function app_total()
    {
        return array(
            microtime(TRUE) - FUEL_START_TIME,
            memory_get_peak_usage() - FUEL_START_MEM
        );
    }

}

