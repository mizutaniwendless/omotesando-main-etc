<?php
/* vim:se et ts=4 sw=4 sts=4: */
/**
 * Model Super.
 * 
 * LICENSE: MIT License.
 * 
 * @copyright 2012 Topazos, Inc.
 * @since File available since Release 1.0.0
 */

abstract class Model_Super
{

    /**
     * Connecting function
     */
    protected $connecting = array(
        'read'  => 'database_connect',
        'write' => 'database_connect',
    );

    /**
     * Database connection
     * @var resourse
     */
    protected $db;

    /**
     * Error Message of MySQL
     * @var string
     */
    protected $er;

    /**
     * Database execution name list
     * @var array
     */
    private $db_executing = array('fetch', 'query', 'execute');

    // {{{ constructor
    /**
     * Constructor
     */
    public function __construct()
    {
    }
    // }}}

    // {{{ undefined call
    public function __call($name, $args)
    {
        static $read = NULL, $write = NULL;
        static $last = FALSE;
        $obj = NULL;
        $chain = FALSE;
        $executing = in_array($name, $this->db_executing);
        if (is_null($read)) {
            $read  = $this->connecting['read']();
        }
        if (is_null($write)) {
            $write = $this->connecting['write']();
        }
        switch ($name) {
            case 'fetch' :
            case 'query' :
                $obj = $name == 'fetch' ? $read : $write;
                $chain = TRUE;
                break;
            case 'all' :
            case 'row' :
            case 'one' :
            case 'select' :
            case 'quote' :
                $obj = $read;
                break;
            case 'insert' :
            case 'update' :
            case 'delete' :
            case 'execute' :
            case 'begin' :
            case 'commit' :
            case 'rollback' :
            case 'transaction' :
            case 'autocommit' :
            case 'last_insert_id' :
                $obj = $write;
                break;
            case 'real_query' :
            case 'last_query' :
            case 'affected' :
                if ($last instanceof Database) {
                    $obj = $last;
                } else {
                    $message = 'Model_Super::' . $name . ' has no database class.';
                    $this->logger($message, 2);
                    throw new Exception($message);
                }
                break;
            default :
                $message = 'Model_Super::' . $name . ' does not exesit.';
                $this->logger($message, 2);
                throw new Exception($message);
        }
        try {
            $executing && $this->_before_executing($obj, $name, $args);
            $result = call_user_func_array(array($obj, $name), $args);
            $executing && $this->_after_executing();
            if ($executing) {
                $last = $obj;
            }
            if ($chain) {
                return $this;
            } else {
                return $result;
            }
        } catch (Exception $e) {
            $last = FALSE; // reset last object for error...
            $this->logger($e->getMessage(), 2);
            throw new Exception('Model_Super::' . $name . ' ' . $e->getMessage());
        }
    }
    // }}}

    // {{{ tools...
    /**
     * Before executing action
     * @return void
     */
    protected function before_action() {}

    /**
     * After executing action
     * @return void
     */
    protected function after_action() {}

    /**
     * Before executing operation
     * @param object $obj  : database object
     * @param string $name : database method
     * @param array  $args : database arguments
     * @return void
     */
    private function _before_executing($obj, $name, $args)
    {
        $this->before_action();
        if (class_exists('Ticker')) {
            Ticker::query_profile_start($obj->real_query($args[0]));
        }
    }

    /**
     * After executing operation
     * @return void
     */
    private function _after_executing()
    {
        if (class_exists('Ticker')) {
            Ticker::query_profile_stop();
        }
        $this->after_action();
    }

    /**
     * Get Error
     * @return string
     */
    public function get_error()
    {
        return $this->er;
    }

    /**
     * Log
     * @param string $message : log message
     * @param integer $back : backnumber
     * @return void
     */
    protected function logger($message, $back = 1)
    {
        $this->er = $message;
        $trace = debug_backtrace();
        $info  = $trace[$back];
        l_error($message, $info['class'] . '::' . $info['function']);
    }
    // }}}

}

