/* vim:se et ts=4 sw=4 sts=4: */
/**
 * Init script
 * 
 * You can place init script as before or after including of module in here.
 * As before including script filename must start `s**_` like "s01_foo.php".
 * As after including script filename must start `e**_` like "e01_bar.php".
 * `**` indicate number of including order.
 * 
 * For exsample as loading(executing) order.
 * 1. s10_hoge.php
 * 2. s20_fuga.php
 * 3. (execute module)
 * 4. e10_hoge.php
 * 5. e20_fuga.php
 * 
 * Otherwise above, as Ziu framework there is loading order of core, apps, app(default)
 * 
 * For execute as loading order.
 * 1.  core/init/s10_hoge.php
 * 2.  core/init/s20_fuga.php
 * 3.  apps/.init/s10_hoge.php
 * 4.  apps/.init/s20_fuga.php
 * 5.  apps/default/.init/s10_hoge.php
 * 6.  apps/default/.init/s20_fuga.php
 * 7.  (execute module)
 * 8.  apps/default/.init/e10_hoge.php
 * 9.  apps/default/.init/e20_fuga.php
 * 10. apps/.init/e10_hoge.php
 * 11. apps/.init/e20_fuga.php
 * 12. core/init/e10_hoge.php
 * 13. core/init/e20_fuga.php
 * 
 */
