<?php
global $formTool;
?>

	<div id="section11" name="section11" class="box">

		<article class="block24 cf">
			<h2><img src="img/block24_h2.png" alt="無料カウンセリングご予約フォーム" class="sp_none" style="padding-top:100px;"><img src="img/block24_h2_sp.png" alt="無料カウンセリングご予約フォーム" class="pc_none"></h2>

			<p class="flow"><img src="img/block24_flow02.png" alt="2．内容の確認" class="sp_none"/><img src="img/block24_flow02_sp.png" alt="2．内容の確認" class="pc_none"/></p>

			<div class="form">

				<p class="caution">※お手数ですが、必須項目をすべてご入力の上、確認画面へお進みください。</p>

			      <form id="form_req" method="post">
			
			        <table>
			          <tr>
			            <th class="hissu">お名前</th>
			            <td><?php echo $formTool->h($_POST['name_req']); ?></td>
			          </tr>
			          <tr>
			            <th class="hissu">電話番号</th>
			            <td><?php echo $formTool->h($_POST['tel_req']); ?></td>
			          </tr>
			          <tr>
			            <th class="hissu">メールアドレス</th>
			            <td><?php echo $formTool->h($_POST['email_req']); ?></td>
			          </tr>
			          <tr>
            <th class="hissu">年齢</th>
            <td><?php echo $formTool->h($_POST['age_req']); ?>歳</td>
          </tr>
          <!--
		  <tr>
            <th class="hissu">ご希望のクリニック</th>
            <td><?php echo $formTool->h($_POST['hope_req']); ?></td>
          </tr>
		  -->
			          <tr>
			            <th class="nini">相談日 第一希望</th>
			            <td><?php echo $formTool->h($_POST['day1']); ?> <?php echo $formTool->h($_POST['day1_period']); ?></td>
			          </tr>
			          <tr>
			            <th class="nini">相談日 第二希望</th>
			            <td><?php echo $formTool->h($_POST['day2']); ?> <?php echo $formTool->h($_POST['day2_period']); ?></td>
			          </tr>
			          <tr>
			            <th class="nini">連絡方法のご希望</th>
			            <td class="radiostyle"><?php if(is_array($_POST['type'])){ echo $formTool->h(implode(',', $_POST['type'] ) ); } ?></td>
			          </tr>
			          <tr>
			            <th class="nini last">お問い合わせ内容</th>
			            <td class="last"><?php echo nl2br($formTool->h($_POST['message'])); ?></td>
			          </tr>
			        </tbody>
			      </table>
			
			      <?php foreach($_POST as $key => $val) { ?>
			      <?php if(is_array($val)) { ?>
			      <?php foreach($val as $key2 => $val2) { ?>
			      <?php echo "<input type=\"hidden\" id=\"form_{$key}[{$key2}]\" name=\"{$key}[{$key2}]\" value=\"{$val2}\">"; ?>
			      <?php } ?>
			      <?php } else { ?>
			      <?php echo "<input type=\"hidden\" id=\"form_{$key}\" name=\"{$key}\" value=\"{$val}\">"; ?>
			      <?php } ?>
			      <?php } ?>
			      <p class="btn sp_none">
			        <input type="image" src="img/block24_btn02.png" class="btn2" alt="入力画面に戻る" />
			        <input type="image" src="img/block24_btn03.png" class="btn1" alt="送信" />
			      </p>
			      <p class="btn pc_none">
			        <input type="image" src="img/block24_btn02_sp.png" class="btn2" alt="入力画面に戻る" />
			        <input type="image" src="img/block24_btn03_sp.png" class="btn1" alt="送信" />
			      </p>
			
			      </form>
			      <script>
			      $(function() {
			      	$('.btn2').on('click', function() {
			      		$('#form_mode').val('back');
			      		$('#form_req').submit();
			      	});
			      	$('.btn1').on('click', function() {
			      		$('#form_mode').val('comp');
			      		$('#form_req').submit();
			      	});
			      });
			      </script>

			</div>

		</article>

	</div>