<?php
global $formTool;
?>

<div class="cnt_wp">
    <ul id="navi">
        <li><a href="#section01" class="on">●</a></li>
        <li><a href="#section02">●</a></li>
        <li><a href="#section03">●</a></li>
        <li><a href="#section04">●</a></li>
        <li><a href="#section05">●</a></li>
        <li><a href="#section06">●</a></li>
        <!--
        <li><a href="#section07">●</a></li>
        -->
        <li><a href="#section08">●</a></li>
        <li><a href="#section15">●</a></li>
    </ul>

<div id="section01" class="box">
    <article class="block01">
        <h2>
        <img src="img/main_img01.png" width="1600" height="514" alt="お顔のリフトアップで究極の若返りを。最新技術であなたに合ったエイジングケア" class="sp_none">
        <img src="img/main_img01_sp.jpg" width="100%" alt="お顔のリフトアップで究極の若返りを。最新技術であなたに合ったエイジングケア" class="pc_none"></h2>
    </article>
    <article class="block02">
        <h2><img src="img/block02_ttl.png" alt="顔のリフトアップは場所やお悩みによって施術方法が変わります。" class="sp_none">
        <img src="img/block02_ttl_sp.png" width="100%" alt="顔のリフトアップは場所やお悩みによって施術方法が変わります。" class="pc_none"></h2>
        <figure class="sp_none">
            <p class="copy"><img src="img/block02_copy.png" alt=" 年齢を重ねるにつれ、お肌のハリが減ってきたり、フェイスラインが崩れたりで、お顔の悩みはつきないもの。当院の最新のテクノロジーを使った、リフトアップ方法をご紹介いたします。"></p>
            <img src="img/block02_img.png" alt="リフトアップ方法のご紹介">
        </figure>
        <figure class="pc_none">
            <p class="copy mb20"><img src="img/block02_copy_sp.png" alt=" 年齢を重ねるにつれ、お肌のハリが減ってきたり、フェイスラインが崩れたりで、お顔の悩みはつきないもの。当院の最新のテクノロジーを使った、リフトアップ方法をご紹介いたします。"></p>
            <img src="img/block02_img_sp.png" alt="リフトアップ方法のご紹介">
        </figure>
    </article>
</div>

<div id="section02" class="box">
	<article class="block03">
            <h2><img src="img/block03_h2.png" alt="当院で受けられる施術方法" class="sp_none">
            <img src="img/title01_sp.png" width="100%" alt="当院で受けられる施術方法" class="pc_none"></h2>
            <div class="cf sp_none">
                <div class="box">
                    <img src="img/block03_ph01.png" alt="切らないフェイスリフト ウルトラセル">
                    <p>最新治療機器ウルトラセルを使い、高周波（RF）周波(RF)・高密度焦点式超音波(HIFU)を用いて、熱エネルギーを与えることで、お肌を傷つけることなくシワ、たるみを改善させます。</p>
                    <a href="#section03"><img src="img/block03_more.png" alt="詳しく見る"></a>
                </div>
                <div class="box rimited">
                    <img src="img/block03_rimited.png" alt="当院限定" class="ob">
                    <img src="img/block03_ph02.png" alt="極細糸で引き上げるフェイスリフト ヤングスリフト®">
                    <p>時間が経つと体内に吸収される特殊な糸を皮膚の下に通して、皮下組織を引きしめます。同時にコラーゲン生成がおこり、みずみずしい肌を保つリバイタライジング効果も期待できます。この方法は皮膚を切開する必要がなく、キズ跡もほとんど残りません。</p>
                    <a href="#section04"><img src="img/block03_more.png" alt="詳しく見る"></a>
                </div>
                <div class="box">
                    <img src="img/block03_ph03.png" alt="切るフェイスリフト フェイスリフト手術">
                    <p>フェイスリフト手術は、顔のたるみを全体的に取り除く施術で、ダウンタイムが２週間ほどあるものの、若返り手術の中で最も実績が長く効果的な方法の一つです。</p>
                    <a href="#section05"><img src="img/block03_more.png" alt="詳しく見る"></a>
                </div>
            </div>
            <div class="cf pc_none">
                <div class="box cf">
                    <img src="img/block03_ph01_sp.png" alt="" class="left">
                    <div class="right">
                        <img src="img/block03_txt01_sp.png" alt="切らないフェイスリフト ウルトラセル">
                        <a href="#section03"><img src="img/block03_more.png" alt="詳しく見る"></a>
                    </div>
                </div>

                <div class="box cf rimited">
                    <img src="img/block03_rimited.png" alt="当院限定" class="ob">
                    <img src="img/block03_ph02_sp.png" alt="" class="left">
                    <div class="right">
                        <img src="img/block03_txt02_sp.png" alt="極細糸で引き上げるフェイスリフト ヤングスリフト®">
                        <a href="#section04"><img src="img/block03_more.png" alt="詳しく見る"></a>
                    </div>
                </div>
                <div class="box cf">
                    <img src="img/block03_ph03_sp.png" alt="" class="left">
                    <div class="right">
                         <img src="img/block03_txt03_sp.png" alt="切るフェイスリフト フェイスリフト手術">
                        <a href="#section05"><img src="img/block03_more.png" alt="詳しく見る"></a>
                    </div>
                </div>
            </div>
    </article>

    <div class="btm_copy">
        <img src="img/block03_btmcopy.png" alt="当院では患者様との入念なカウンセリングを行い、経験豊富な医師が患者様個人に適した最適な施術プランをご提供いたします。" class="sp_none">
        <img src="img/block03_btmcopy_sp.png" alt="当院では患者様との入念なカウンセリングを行い、経験豊富な医師が患者様個人に適した最適な施術プランをご提供いたします。" class="pc_none">
    </div>

	<div class="btn_box cf sp_none">
        <div class="txt">
            <img src="images/tk_cv_pc1.gif" alt="0120-334-270">
        </div>
        <div class="btn">
            <a href="#section15"><img src="img/btn_box_btn.png" alt="無料カウンセリングのご予約"></a>
        </div>
    </div>
    <div class="btn_box pc_none">
    	<div class="tel_box">
            <a href="#section15"><img src="img/box_btn_sp.png" alt="無料カウンセリング予約"></a>
            <p class="tel"><a href="tel:0120334270" onclick="yahoo_report_conversion('tel:0120-334-270')" class="pc_none"><img src="images/tk_cv_sp1.gif" alt="0120-334-270 受付時間//11:00～20:00"></a></p>
<p class="tel"><a href="tel:0120931911" onclick="yahoo_report_conversion('tel:0120-931-911')" class="pc_none"><img src="images/tk_cv_sp2.gif" alt="0120-334-270 受付時間//11:00～20:00"></a></p>
            
        </div>
    </div>
</div>


<div id="section03" class="box">
    <article class="block04">
        <div class="h2_box">
            <div class="inner">
                <h2><img src="img/block04_h2.png" alt="切らないフェイスリフト ウルトラセル" class="sp_none"><img src="img/block04_h2_sp.png" alt="切らないフェイスリフト ウルトラセル" class="pc_none"></h2>
                <p class="copy"><img src="img/block04_copy01.png" alt="表皮・真皮・SMASへ同時にアプローチ。内側からしっかり引き上げ、切らずにシワ・たるみを改善！" class="sp_none"><img src="img/block04_copy01_sp.png" alt="表皮・真皮・SMASへ同時にアプローチ。内側からしっかり引き上げ、切らずにシワ・たるみを改善！" class="pc_none"></p>
                <p>今まで難しかった深層の表情筋層（SMAS）にまで熱エネルギーを届け肌を底から引き締め、持ち上げます。特に表情筋層への作用は外的治療で行うフェイスリフトと同じ層であることから、
                「切らないフェイスリフト」とも呼ばれています。</p>

                <img src="img/block04_kouka.png" alt="ウルトラセル　画像" class="ob sp_none">
                <img src="img/block04_kouka_sp.png" alt="ウルトラセル　画像" class="ob pc_none">
            </div>
        </div>

        <div class="txt">
            <h3><img src="img/block04_h3.png" alt="3つのテクノロジーを組み合わせ、年齢肌のための次世代のコンビネーション治療が可能になりました。" class="sp_none"><img src="img/block04_h3_sp.png" alt="3つのテクノロジーを組み合わせ、年齢肌のための次世代のコンビネーション治療が可能になりました。" class="pc_none"></h3>
            <div class="cf">
                <div class="box">
                    <h4><img src="img/block04_ttl01.png" alt="イントラセル（SRR）"></h4>
                    <div class="box_inner">
                        <img src="img/block04_img01.png" alt="イントラセル　画像">
                        <dl>
                            <dt>ツルツル卵肌に！</dt>
                            <dd>表皮から真皮上層かけ熱エネルギーを与える事によって、コラーゲンを再生し、ハリ・ツヤを与え、小じわや・毛穴の開きに効果的です。
また、肌への負担が少ない治療のため、従来では難しかった目元のギリギリまで安全に治療することが可能ですので、目元にシワに効果的です。</dd>
                        </dl>
                        <table class="mt100">
                            <tr>
                                <th>ターゲット</th>
                                <td>肌の浅層</td>
                            </tr>
                            <tr>
                                <th>効果</th>
                                <td>毛穴・シワ・はり</td>
                            </tr>
                            <tr>
                                <th>痛み</th>
                                <td>照射時にチクチクした痛みがあります。</td>
                            </tr>
                            <tr>
                                <th>ダウンタイム</th>
                                <td>ほとんどありません。<br />まれに赤みがでます。</td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="box">
                    <h4><img src="img/block04_ttl02.png" alt="イントラジェン（GFR）"></h4>
                    <div class="box_inner">
                        <img src="img/block04_img03.png" alt="イントラジェン　画像">
                        <dl>
                            <dt>プルプル弾力肌！</dt>
                            <dd>イントラジェンのRF高周波は、照射面を格子状にしてラジオ波の熱エネルギーを照射することで肌に照射する面積を抑え、表皮へのダメージと痛みを最小限に抑えます。<br />
                            RF(ラジオ波)を照射すると皮膚の真皮にあるコラーゲン線維が熱で収縮を起こし、これにより皮膚が引き締まり､シワ・たるみを改善していきます。この効果は速効性がありますが、一次的なもので、1～2週間ほど保たれます。</dd>
                        </dl>
                        <table class="mt40">
                            <tr>
                                <th>ターゲット</th>
                                <td>肌の中層</td>
                            </tr>
                            <tr>
                                <th>効果</th>
                                <td>ハリ・リフトアップ
ほうれい線・小顔</td>
                            </tr>
                            <tr>
                                <th>痛み</th>
                                <td>多少熱感があります。</td>
                            </tr>
                            <tr>
                                <th>ダウンタイム</th>
                                <td>ほとんどありません。</td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="box">
                    <h4><img src="img/block04_ttl03.png" alt="ハイフ（HIHU）"></h4>
                    <div class="box_inner">
                        <img src="img/block04_img02.png" alt="ハイフ　画像">
                        <dl>
                            <dt>グイッと上げてスッキリ小顔！</dt>
                            <dd>高密度焦点式調音波（HIFU）を用いた治療です｡<br />
                            今までのたるみ治療機は皮下の真皮層をターゲットにする物がほとんどでしたが、ウルトラセルのHIFUはこの技術を使用し、皮下4.5mm、3.0mmの深さに超音波を照射し、SMAS筋膜を加熱し収縮させることで、コラーゲン生成を促し緩んだ肌を整え、即自的にシワやたるみを改善させます｡</dd>
                        </dl>
                        <table class="mt90">
                            <tr>
                                <th>ターゲット</th>
                                <td>肌の一番深い層</td>
                            </tr>
                            <tr>
                                <th>効果</th>
                                <td>リフトアップ</td>
                            </tr>
                            <tr>
                                <th>痛み</th>
                                <td>照射時にチクチクした痛みがあります。</td>
                            </tr>
                            <tr>
                                <th>ダウンタイム</th>
                                <td>ほとんどありません。</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div><!--cf-->
            <div class="btm_copy mb80">
                <img src="img/block04_btmcopy.png" alt="当院オールレイヤードセラピー（SRR、GFR、HIFU）でダウンタイムの少ないお肌にやさしいエイジング治療を実現！" class="sp_none"><img src="img/block04_btmcopy_sp.png" alt="当院オールレイヤードセラピー（SRR、GFR、HIFU）でダウンタイムの少ないお肌にやさしいエイジング治療を実現！" class="pc_none">
            </div>
        </div>
        <!--txt-->
    </article>

    <article class="block05">
        <h2><img src="img/block05_h2.png" alt="症例写真" class="sp_none"><img src="img/block05_h2_sp.png" alt="症例写真" class="pc_none"></h2>
        <ul class="cf">
            <li><img src="img/block05_ex01.png" alt="症例写真1"></li>
            <li><img src="img/block05_ex02.png" alt="症例写真2"></li>
        </ul>
    </article>

    <article class="block06">
        <h3><img src="img/block06_h3.png" alt="最新鋭マシン「ウルトラセル」でできるオールレイヤードセラピーとは" class="sp_none"><img src="img/block06_h3_sp.png" alt="最新鋭マシン「ウルトラセル」でできるオールレイヤードセラピーとは" class="pc_none"></h3>

        <div class="box">
            <div class="h4_box">
                <h4><img src="img/block06_tiryo01.png" alt="SRR治療"></h4>
                <p>ツルツル卵肌！</p>
            </div>
            <p>高周波（ラジオ波）を用いて表皮から真皮上層へ熱を与えることにより』ターンオーバーを促進させ新しい肌に生まれ変わります。毛穴の引き締め、小じわの改善、肌のハリを実感できます。</p>
            <table class="sp_none">
                <tr>
                    <th>リフトアップ以外の効果</th>
                    <th>ダウンタイム</th>
                    <th>痛み</th>
                    <th>麻酔</th>
                    <th>持続効果</th>
                </tr>
                <tr>
                    <td><div class="cf"><span>ニキビ跡</span><span>毛穴</span><span>しわ</span></div></td>
                    <td>ほとんどありません。<br />まれに赤みがでます。</td>
                    <td>照射時にチクチクした痛みがあります。</td>
                    <td>なし</td>
                    <td>1.5〜3ヶ月程度</td>
                </tr>
            </table>
            <table class="pc_none">
                <tr>
                    <th>リフトアップ以外の効果</th>
                    <td><div class="cf"><span>ニキビ跡</span><span>毛穴</span><span>しわ</span></div></td>
                </tr>
                <tr>
                    <th>ダウンタイム</th>
                    <td>ほとんどありません。<br />まれに赤みがでます。</td>
                </tr>
                <tr>
                    <th>痛み</th>
                    <td>照射時にチクチクした痛みがあります。</td>
                </tr>
                <tr>
                    <th>麻酔</th>
                    <td>なし</td>
                </tr>
                <tr>
                    <th>持続効果</th>
                    <td>1.5〜3ヶ月程度</td>
                </tr>
            </table>
            <p class="att">※効果には個人差があります。</p>
        </div>
        <!---->
        <div class="box">
            <div class="h4_box">
                <h4><img src="img/block06_tiryo02.png" alt="GFR治療"></h4>
                <p>プルプル弾力肌！</p>
            </div>
            <p>真皮と脂肪層に高周波（ラジオ波）を照射することで、コラーゲン繊維が熱で収縮し、その後リモデリングが行われます。リモデリングの過程で生成された新しいコラーゲンによりお肌のハリが戻り、シワ、たるみの改善が実感できます。</p>
            <table class="sp_none">
                <tr>
                    <th>リフトアップ以外の効果</th>
                    <th>ダウンタイム</th>
                    <th>痛み</th>
                    <th>麻酔</th>
                    <th>持続効果</th>
                </tr>
                <tr>
                    <td><div class="cf"><span>しわ</span><span>小じわ</span><span>ほうれい線</span>
                    <span>小顔</span>
                    <span>毛穴の開き</span>
                    <span>ニキビ跡</span></div></td>
                    <td>なし</td>
                    <td>多少熱感があります。</td>
                    <td>なし</td>
                    <td>3～6ヶ月程度</td>
                </tr>
            </table>
            <table class="pc_none">
                <tr>
                    <th>リフトアップ以外の効果</th>
                    <td class="kouka2"><div class="cf"><span>しわ</span><span>小じわ</span><span>ほうれい線</span>
                    <span>小顔</span>
                    <span>毛穴の開き</span>
                    <span>ニキビ跡</span></div></td>
                </tr>
                <tr>
                    <th>ダウンタイム</th>
                    <td>なし</td>
                </tr>
                <tr>
                    <th>痛み</th>
                    <td>多少熱感があります。</td>
                </tr>
                <tr>
                    <th>麻酔</th>
                    <td>なし</td>
                </tr>
                <tr>
                    <th>持続効果</th>
                    <td>3～6ヶ月程度</td>
                </tr>
            </table>
            <p class="att">※効果には個人差があります。</p>
        </div>
        <!---->
        <div class="box">
            <div class="h4_box">
                <h4><img src="img/block06_tiryo03.png" alt="HIFU治療"></h4>
                <p>グイッと上げて<br class="pc_none">スッキリ小顔</p>
            </div>
            <p>ガン治療にも使用される高密度焦点式超音波を用いて、SMAS筋膜を加熱し、コラーゲンから成るSMAS筋膜を収縮、再生させることで緩んだ肌を整え即時的なリフトアップ効果を実感できます。</p>
            <table class="sp_none">
                <tr>
                    <th>リフトアップ以外の効果</th>
                    <th>ダウンタイム</th>
                    <th>痛み</th>
                    <th>麻酔</th>
                    <th>持続効果</th>
                </tr>
                <tr>
                    <td><div class="cf"><span>たるみ</span><span>二重あご</span>
                    <span>小顔</span></div></td>
                    <td>なし</td>
                    <td>照射時にチクチクした痛みがあります。</td>
                    <td>なし</td>
                    <td>6ヶ月〜1年程度</td>
                </tr>
            </table>

            <table class="pc_none">
                <tr>
                    <th>リフトアップ以外の効果</th>
                    <td><div class="cf"><span>たるみ</span><span>二重あご</span>
                    <span>小顔</span></div></td>
                </tr>
                <tr>
                    <th>ダウンタイム</th>
                    <td>なし</td>
                </tr>
                <tr>
                    <th>痛み</th>
                    <td>照射時にチクチクした痛みがあります。</td>
                </tr>
                <tr>
                    <th>麻酔</th>
                    <td>なし</td>
                </tr>
                <tr>
                    <th>持続効果</th>
                    <td>6ヶ月〜1年程度</td>
                </tr>
            </table>
            <p class="att">※効果には個人差があります。</p>
        </div>

        <p class="btm_copy">
        <img src="img/block06_btmcopy.png" alt="SRR、GFR、HIFUの3つの治療で、すべての皮膚層とSAMS筋膜に熱影響を与えることでシワ、たるみの原因を改善します。この複合的な治療がウルトラセルで全て可能になります！" class="sp_none">
            <img src="img/block06_btmcopy_sp.png" alt="SRR、GFR、HIFUの3つの治療で、すべての皮膚層とSAMS筋膜に熱影響を与えることでシワ、たるみの原因を改善します。この複合的な治療がウルトラセルで全て可能になります！" class="pc_none">
        </p>
    </article>


    <article class="block07">
        <h2><img src="img/block07_h2.png" alt="ウルトラセルのよくある質問"></h2>

        <div class="faq_wrap">
            <dl class="faq">
                <dt>1回の施術はどのくらいの時間がかかりますか？</dt>
                <dd>施術内容、照射部位により異なりますが、カウンセリングやメイクオフ等の含めて30〜120分程度です。</dd>
                <dt>痛みは有りますか？麻酔は必要ですか？</dt>
                <dd>ほとんどの方は麻酔なしで施術を受けられます。どうしても痛みに弱いという方は医師にご相談ください。</dd>
                <dt>ダウンタイムはありますか？</dt>
                <dd>浅い層への照射直後は照射部に沿ったむくみがでます。照射後の冷却することで解消します。まれに、線状痕がでますが、お化粧で完全にカバーできる程度です。深い層の照射の場合は、2週間程、軽い鈍い筋肉痛のようなものを感じる場合もございます。</dd>

                <dt>1回で効果は実感できますか？</dt>
                <dd>表皮～真皮～表情筋まですべての皮膚層に熱エネルギーを与えることができるため、１回の施術で引き締め効果を実感していただけます。これまでのレーザー照射では到達することが難しかった表情筋の浅筋腱膜にまで作用。外科的手術と同じ「切らないリフトアップ効果」が期待できます。</dd>
                <dt>注入や他のレーザー治療と一緒に受けられますか？</dt>
                <dd>ウルトラセルは、当院で導入しているほとんどの治療と組み合わせることができます。ボトックスなどの熱に弱い治療と併用する場合は別にスケジュールを立てて行いますので、お気軽にカウンセリング時に医師にご相談ください。</dd>
            </dl>
        </div>
        

    </article>

    <div class="btn_box cf sp_none">
        <div class="txt">
            <img src="images/tk_cv_pc1.gif" alt="0120-334-270">
        </div>
        <div class="btn">
            <a href="#section15"><img src="img/btn_box_btn.png" alt="無料カウンセリングのご予約"></a>
        </div>
    </div>
    <div class="btn_box pc_none">
        <div class="tel_box">
            <a href="#section15"><img src="img/box_btn_sp.png" alt="無料カウンセリング予約"></a>
            <p class="tel"><a href="tel:0120334270" onclick="yahoo_report_conversion('tel:0120-334-270')" class="pc_none"><img src="images/tk_cv_sp1.gif" alt="0120-334-270 受付時間//11:00～20:00"></a></p>
<p class="tel"><a href="tel:0120931911" onclick="yahoo_report_conversion('tel:0120-931-911')" class="pc_none"><img src="images/tk_cv_sp2.gif" alt="0120-334-270 受付時間//11:00～20:00"></a></p>
            
        </div>
    </div>

</div>

<div id="section04" class="box">
    <article class="block08">
        <div class="h2_box">
        <h2><img src="img/block08_ttl_sp.png" alt="極細糸で引き上げるフェイスリフト ヤングスリフト®" class="pc_none"></h2>
                <p class="pc_none"><img src="img/block08_txt03_sp.png" alt="日本では当院系列店のみ施術可能"></p>
            <div class="inner">
              <h2><img src="img/block08_ttl.png" alt="極細糸で引き上げるフェイスリフト ヤングスリフト®" class="sp_none"></h2>  


                <p><img src="img/block08_txt01.png" alt="特殊加工された溶ける極細糸を使用して、皮下組織を引き締めます。糸の周囲でコラーゲン生成がおこり肌にハリを与え、たるみ予防にも効果を発揮します。ヤングスリフト®はお顔のパーツごとに細かいリフトアップが可能な最先端のフェイスリフトです。" class="sp_none">

                <img src="img/block08_txt01_sp.png" alt="特殊加工された溶ける極細糸を使用して、皮下組織を引き締めます。糸の周囲でコラーゲン生成がおこり肌にハリを与え、たるみ予防にも効果を発揮します。ヤングスリフト®はお顔のパーツごとに細かいリフトアップが可能な最先端のフェイスリフトです。" class="pc_none"></p>
                
                <p><img src="img/block08_txt02.png" alt="・FDA（米国食品医療品局）・KFDA（韓国食品医薬品安全処）・CEマーク（商品がすべてのEU(欧州連合) 加盟国の基準を満たすものに付けられる基準適合マーク）各国の各国の厚生省認可を受けた安全性、信頼度の高い施術です。" class="sp_none"><img src="img/block08_txt02_sp.png" alt="・FDA（米国食品医療品局）・KFDA（韓国食品医薬品安全処）・CEマーク（商品がすべてのEU(欧州連合) 加盟国の基準を満たすものに付けられる基準適合マーク）各国の各国の厚生省認可を受けた安全性、信頼度の高い施術です。" class="pc_none"></p>
            </div>
        </div>
    </article>
    <article class="block009">
        <h2><img src="img/block09_h2.png" alt="施術内容" class="sp_none"><img src="img/block09_h2_sp.png" alt="施術内容" class="pc_none"></h2>
        <dl>
            <dt>通常よりも細く「アンカー」が付いたヤングスリフトⓇ独自の特殊糸</dt>
            <dd class="cf sp_none">
                <p>ヤングスリフトⓇで使用する糸は通常の1/3の細さでありながら引き上げる力は強いのが特徴です。<br />
                それは糸の先端についた「アンカー」で引き上げた状態をしっかりと維持することができるからです。<br />
                この「アンカー」はほかのフェイスリフトに使われる糸にはないヤングスリフト独自の技術です。<br />
                ヤングスリフト®は他の糸よりも細く、特殊な「アンカー」があることで、今まで難しいとされていた額、まぶた、チークライン、ネックラインなどの細かな部位別の引き上げも可能にします。</p>
                <img src="img/block09_img01.png" alt="ヤングスリフト　画像">
            </dd>
            <dd class="cf pc_none">
                 <img src="img/block09_img01.png" alt="ヤングスリフト　画像">
                 ヤングスリフトⓇで使用する糸は通常の1/3の細さでありながら引き上げる力は強いのが特徴です。<br />
                それは糸の先端についた「アンカー」で引き上げた状態をしっかりと維持することができるからです。<br />
                この「アンカー」はほかのフェイスリフトに使われる糸にはないヤングスリフト独自の技術です。<br />
                ヤングスリフト®は他の糸よりも細く、特殊な「アンカー」があることで、今まで難しいとされていた額、まぶた、チークライン、ネックラインなどの細かな部位別の引き上げも可能にします。
               
            </dd>
        </dl>
        <dl>
            <dt>コラーゲン、TGFβ（成長因子）の増加でお肌の若返り</dt>
            <dd>
                <p>ヤングスリフトⓇはアンカー付きの細く特殊な医療用の糸を、皮膚の内側（真皮層や皮下組織、筋膜下など）に、挿入します。<br />
                糸そのものの牽引作用によるリフトアップ効果と、糸の挿入による刺激で損傷を受けた皮膚内側の組織が修復する際に起こるコラーゲンの生成、TGFβ（成長因子）の増加により、皮膚の弾力アップ、肌つやが良くなる、引き締まるなどの効果によりシワやタルミを改善します。</p>
            </dd>
        </dl>
        <dl class="bdb">
            <dt>各国の認定を受けた高い安全性と信頼の「切らないフェイスリフト治療」</dt>
            <dd>
                <p>ヤングスリフトⓇは、高い安全性と効果が立証されており、FDA（アメリカの厚労省）、KDFA（韓国の厚労省）、CE（EU各加盟国の基準を満たした安全性の高い商品に与えられるマーク）の厳しい審査をへて認可された製品を使用しています。<br />
                また、アメリカ、韓国、日本で特許を取得している安全性、信頼性の高い施術です。（2017年1月現在40ヵ国以上で特許出願中）<br />施術時間は1か所10分程度、腫れも少なく、傷跡も小さいためダウンタイムも少なく、施術直後のポイントメイクも可能です。</p>
            </dd>
        </dl>

        <div class="cf">
            <table>
                <tr>
                    <th>施術時間</th>
                    <td>1ヶ所10分程度</td>
                </tr>
                <tr>
                    <th>ダウンタイム</th>
                    <td>ほぼありません。</td>
                </tr>
                <tr>
                    <th>傷</th>
                    <td>赤い点状の針穴が処置後数日間残りますがお化粧で隠れる程度です。</td>
                </tr>
            </table>

            <table>
                <tr>
                    <th>メイク</th>
                    <td>ポイントメイクは当日から可能です。</td>
                </tr>
                <tr>
                    <th>痛み</th>
                    <td>腫れや痛みが生じることがありますが1週間程度で落ち着いてきます。</td>
                </tr>
                <tr>
                    <th>持続効果</th>
                    <td>約1年半前後で糸は体内に吸収されていきます。</td>
                </tr>
            </table>
        </div>
    </article>

    <article class="block10">
        <img src="img/block10_txt.png" alt="注射治療、レーザー治療ではなかなか効果を実感できなかった、切る治療に抵抗がある方は是非ヤングスリフト®をお試しください。 日本では表参道スキンクリニックが認可を受けた施術です。当院の系列クリニックでのみヤングスリフト®をご提供しております。" class="sp_none">
        <img src="img/block10_txt_sp.png" alt="注射治療、レーザー治療ではなかなか効果を実感できなかった、切る治療に抵抗がある方は是非ヤングスリフト®をお試しください。 日本では表参道スキンクリニックが認可を受けた施術です。当院の系列クリニックでのみヤングスリフト®をご提供しております。" class="pc_none">
    </article>

    <div class="btn_box2">
        <p class="ttl">
            <img src="img/btn_box2_ttl.png" alt="今ならモニターキャンペーン価格" class="sp_none">
            <img src="img/btn_box2_ttl_sp.png" alt="今ならモニターキャンペーン価格" class="pc_none">
        </p>

        <p class="txt"><img src="img/btn_box2_txt.png" alt="特殊な糸でシワやたるみを引き上げる！ 最新鋭のフィエスリフト ヤングスリフト　448,000円→100,000円" class="sp_none"><img src="img/btn_box2_txt_sp.png" alt="特殊な糸でシワやたるみを引き上げる！ 最新鋭のフィエスリフト ヤングスリフト　448,000円→100,000円" class="pc_none"></p>
        <div class="cf">
            <p class="tel sp_none"><img src="images/tk_cv_pc4.png" alt="0120-334-270"></p>

            <p class="btn"><a href="#section15"><img src="img/btn_box2_btn.png" alt="無料カウンセリングのご予約" class="sp_none"><img src="img/btn_box2_btn_sp.png" alt="無料カウンセリングのご予約" class="pc_none"></a>
            </p>
            <p class="tel pc_none"><a href="tel:0120334270" onclick="yahoo_report_conversion('tel:0120-334-270')"><img src="images/tk_cv_sp3.png" alt="無料カウンセリングのご予約" class="pc_none"></a></p>

<p class="tel pc_none"><a href="tel:0120931911" onclick="yahoo_report_conversion('tel:0120-931-911')"><img src="images/tk_cv_sp4.png" alt="0120-334-270"></a></p>

        </div>
    </div>

    <article class="block11">
        <h2><img src="img/block11_h2.png" alt="症例写真" class="sp_none"><img src="img/block11_h2_sp.png" alt="症例写真" class="pc_none"></h2>

        <div class="ex cf">
        <dl>
            <dt><img src="img/block11_before2.png" alt="BEFORE" class="sp_none"><img src="img/block17_before.png" alt="BEFORE" class="pc_none"></dt>
            <dd>
                <img src="img/block11_ph13.jpg" alt="">
            </dd>
        </dl>
        <dl>
            <dt><img src="img/block11_after2.png" alt="AFTER" class="sp_none"><img src="img/block17_after.png" alt="AFTER" class="pc_none"></dt>
            <dd>
                <img src="img/block11_ph14.jpg" alt="">
            </dd>
        </dl>
    </div>

        <table class="sp_none">
            <tr>
                <th><img src="img/block11_before.png" alt="BEFORE"></th>
                <th><img src="img/block11_after01.png" alt="施術直後"></th>
                <th><img src="img/block11_after02.png" alt="術後4日後"></th>
                <th><img src="img/block11_after03.png" alt="術後1ヶ月後"></th>
            </tr>
            <tr>
                <td>
                    <img src="img/block11_ph01.jpg" alt="BEFORE写真1">
                </td>
                <td>
                    <img src="img/block11_ph02.jpg" alt="術後4日後写真1">
                    <p>施術直後、多少の腫れは有りますが、<br />顔全体が引きあがりました。</p>
                </td>
                <td>
                    <img src="img/block11_ph03.jpg" alt="術後4日後写真1">
                    <p>腫れも引いて、顔全体が引きあがりました。目が大きくなり、頬の位置も施術前に比べ大きく上がっています。</p>
                </td>
                <td>
                    <img src="img/block11_ph04.jpg" alt="術後1ヶ月後写真1">
                     <p class="mr0">腫れや傷跡も完全に消え、より自然な表情になりました。</p>
                </td>
            </tr>
            <tr>
                <td>
                    <img src="img/block11_ph05.jpg" alt="BEFORE写真2">
                </td>
                <td>
                    <img src="img/block11_ph06.jpg" alt="術後4日後写真2">
                    <p>頬のライン、アゴ下が全体的に引き上がりました。</p>
                </td>
                <td>
                    <img src="img/block11_ph07.jpg" alt="術後4日後写真2">
                    <p>まぶたやあごのたるみが解消され、スッキリした印象になりました。</p>
                </td>
                <td>
                    <img src="img/block11_ph08.jpg" alt="術後1ヶ月後写真2">
                     <p class="mr0">顔全体が引き締まった印象になりました。</p>
                </td>
            </tr>
            <tr>
                <td class="tal">
                    <img src="img/block11_ph09.jpg" alt="BEFORE写真3" class="mt20">
                </td>
                <td>
                    <img src="img/block11_ph10.jpg" alt="術後4日後写真3" class="mt20">
                    <p>傷跡も非常に小さく、テープで隠れる程度です。</p>
                </td>
                <td>
                    <img src="img/block11_ph11.jpg" alt="術後4日後写真3" class="mt20">
                    <p>あごのラインもシャープになり、施術前にあったマリオネットラインも目立たなくなりました。</p>
                </td>
                <td class="tar">
                    <img src="img/block11_ph12.jpg" alt="術後1ヶ月後写真3" class="mt20">
                    <p class="mr0">施術1ヶ月後はコラーゲンの生成がピークを迎え、顔色が良くなり、肌ツヤも出てきました。</p>
                </td>
            </tr>
        </table>

        <div class="photo_box pc_none cf">
            <div class="box_wrap">
                <div class="box">
                    <ul class="cf">
                        <li><img src="img/block11_ph01_sp.jpg" alt=""></li>
                        <li><img src="img/block11_ph02_sp.jpg" alt=""><p>施術直後、多少の腫れは有りますが、顔全体が引きあがりました。</p></li>
                        <li><img src="img/block11_ph03_sp.jpg" alt=""><p>腫れも引いて、顔全体が引きあがりました。目が大きくなり、頬の位置も施術前に比べ大きく上がっています。</p></li>
                        <li><img src="img/block11_ph04_sp.jpg" alt=""><p>腫れや傷跡も完全に消え、より自然な表情になりました。</p></li>
                    </ul>
                </div>
                <div class="box">
                    <ul class="cf">
                        <li><img src="img/block11_ph05_sp.jpg" alt=""></li>
                        <li><img src="img/block11_ph06_sp.jpg" alt=""><p>頬のライン、アゴ下が全体的に引き上がりました。</p></li>
                        <li><img src="img/block11_ph07_sp.jpg" alt=""><p>まぶたやあごのたるみが解消され、スッキリした印象になりました。</p></li>
                        <li><img src="img/block11_ph08_sp.jpg" alt=""><p>顔全体が引き締まった印象になりました。</p></li>
                    </ul>
                </div>
                <div class="box">
                    <ul class="cf">
                        <li><img src="img/block11_ph09_sp.jpg" alt=""></li>
                        <li><img src="img/block11_ph10_sp.jpg" alt=""><p>傷跡も非常に小さく、テープで隠れる程度です。</p></li>
                        <li><img src="img/block11_ph11_sp.jpg" alt=""><p>あごのラインもシャープになり、施術前にあったマリオネットラインも目立たなくなりました。</p></li>
                        <li><img src="img/block11_ph12_sp.jpg" alt=""><p>施術1ヶ月後はコラーゲンの生成がピークを迎え、顔色が良くなり、肌ツヤも出てきました。</p></li>
                    </ul>
                </div>
            </div>
        </div>

        <p class="btm_copy">
        <img src="img/block11_btmcopy.png" alt="今までの糸リフトなどでは難しかった、片方だけ下がった口角や眉毛の位置を整えたいなどの細かい部分的なリフトアップもヤングスリフト®なら可能です。" class="sp_none">
        <img src="img/block11_btmcopy_sp.png" alt="今までの糸リフトなどでは難しかった、片方だけ下がった口角や眉毛の位置を整えたいなどの細かい部分的なリフトアップもヤングスリフト®なら可能です。" class="pc_none">
    </p>
    </article>


    <article class="block12">
        <h2><img src="img/block12_h2.png" alt="ヤングスリフトのよくある質問" class="sp_none"><img src="img/block12_h2_sp.png" alt="ヤングスリフトのよくある質問" class="pc_none"></h2>

        <div class="faq_wrap">
            <dl class="faq">
                <dt>キズ跡は残りますか？</dt>
                <dd>治療直後はメイクで隠せる程度の赤い点状の針跡がありますが、数日間で目立たなくなります。強い力でのマッサージや指圧などは約1ヶ月間程お控えください。</dd>
                <dt>痛みにはかなり弱いのですが、大丈夫でしょうか？</dt>
                <dd>治療中は局所麻酔を行うのでご安心ください。施術後は痛み止めの処方も行っておりますのでぜひご相談ください。</dd>
                <dt>入れた糸はずっと残るのですか？</dt>
                <dd>挿入した糸は一年半前後で体内に吸収されます。約1ヶ月後に糸の周囲でコラーゲンと成長因子の生成がピークを迎え、徐々に肌そのものの若返り効果が期待できます。</dd>
            </dl>
        </div>
        <div class="btn_box2">
        <p class="ttl">
            <img src="img/btn_box2_ttl.png" alt="今ならモニターキャンペーン価格" class="sp_none">
            <img src="img/btn_box2_ttl_sp.png" alt="今ならモニターキャンペーン価格" class="pc_none">
        </p>

        <p class="txt"><img src="img/btn_box2_txt.png" alt="特殊な糸でシワやたるみを引き上げる！ 最新鋭のフィエスリフト ヤングスリフト　448,000円→100,000円" class="sp_none"><img src="img/btn_box2_txt_sp.png" alt="特殊な糸でシワやたるみを引き上げる！ 最新鋭のフィエスリフト ヤングスリフト　448,000円→100,000円" class="pc_none"></p>
        <div class="cf">
            <p class="tel sp_none"><img src="images/tk_cv_pc4.png" alt="0120-334-270"></p>

            <p class="btn"><a href="#section15"><img src="img/btn_box2_btn.png" alt="無料カウンセリングのご予約" class="sp_none"><img src="img/btn_box2_btn_sp.png" alt="無料カウンセリングのご予約" class="pc_none"></a>
            </p>
            <p class="tel pc_none"><a href="tel:0120334270" onclick="yahoo_report_conversion('tel:0120-334-270')"><img src="images/tk_cv_sp3.png" alt="無料カウンセリングのご予約" class="pc_none"></a></p>

<p class="tel pc_none"><a href="tel:0120931911" onclick="yahoo_report_conversion('tel:0120-931-911')"><img src="images/tk_cv_sp4.png" alt="0120-334-270"></a></p>

        </div>
    </div>

    </article>

    <div class="btn_box cf sp_none">
        <div class="txt">
            <img src="images/tk_cv_pc1.gif" alt="0120-334-270">
        </div>
        <div class="btn">
            <a href="#section15"><img src="img/btn_box_btn.png" alt="無料カウンセリングのご予約"></a>
        </div>
    </div>
    <div class="btn_box pc_none">
        <div class="tel_box">
            <a href="#section15"><img src="img/box_btn_sp.png" alt="無料カウンセリング予約"></a>
            <p class="tel"><a href="tel:0120334270" onclick="yahoo_report_conversion('tel:0120-334-270')" class="pc_none"><img src="images/tk_cv_sp1.gif" alt="0120-334-270 受付時間//11:00～20:00"></a></p>
<p class="tel"><a href="tel:0120931911" onclick="yahoo_report_conversion('tel:0120-931-911')" class="pc_none"><img src="images/tk_cv_sp2.gif" alt="0120-334-270 受付時間//11:00～20:00"></a></p>
            
        </div>
    </div>

</div>


<div id="section05" class="box">
    <article class="block13">
        <div class="inner">
            <h2><img src="img/block13_ttl.png" alt="切るフェイスリフト　フェイスリフト手術" class="sp_none"><img src="img/block13_ttl_sp.png" alt="切るフェイスリフト　フェイスリフト手術" class="pc_none"></h2>

        </div>
    </article>
    <article class="block14">
            <h2><img src="img/block14_h2.png" alt="施術内容"></h2>
            <div class="txt cf">
                <p>皮膚の下にあるSMASと呼ばれる組織(表情性筋膜)も皮膚と一緒に引き上げるため、従来の皮膚のたるみだけを引き上げるものよりも最大限のリフトアップ効果を発揮します。<br />皮膚のみリフトするよりキレイに仕上がり、傷跡や腫れが小さい施術です。<br />傷が目立たない耳の付け根（前側）付近から、皮膚の余分なたるみとSMASを引き上げます。腫れは少ししかなく、傷跡も小さく髪の毛で隠せますので１週間後の抜糸時にはほとんどわからない程度まで回復します。</p>
                <img src="img/block14_img01.png" alt="施術内容画像">
            </div>

            <div class="figure cf heightLineParent">
                <div class="box">
                    <img src="img/block14_img02.png" alt="手順1">
                    <p class="ml60">皮膚だけでなく、その下にある皮下脂肪<br class="sp_none">SMASもたるんでる状態</p>
                </div>
                <div class="box">
                    <img src="img/block14_img03.png" alt="手順2">
                    <p class="ml90">皮膚・脂肪組織とともにSMASを<br class="sp_none">引き上げ余った皮膚とSMASを切除</p>
                </div>
                <div class="box">
                    <img src="img/block14_img04.png" alt="手順3">
                    <p class="ml110">皮膚とSMASを切除した状態</p>
                </div>
                <div class="box">
                    <img src="img/block14_img05.png" alt="手順4">
                    <p class="ml75">皮膚と脂肪組織、SMASそれぞれを縫合</p>
                </div>
            </div>
    </article>

    <article class="block15">
            <h2><img src="img/block15_h2.png" alt="術後の経過" class="sp_none"><img src="img/block15_h2_sp.png" alt="術後の経過" class="pc_none"></h2>
            <img src="img/block15_img01.png" alt="たるんでくる皮膚のせいでフェイスラインがぼやけてくる。SMASからしっかり引き上げる事で-10歳の印象に！" class="sp_none">
            <img src="img/block15_img01_sp.png" alt="たるんでくる皮膚のせいでフェイスラインがぼやけてくる。SMASからしっかり引き上げる事で-10歳の印象に！" class="pc_none" style="width: 80%; margin: 0 auto;">
    </article>

    <article class="block16">
            <table class="sp_none">
                <tr>
                    <th>適応箇所</th>
                    <th>通院</th>
                    <th>施術時間</th>
                    <th>麻酔</th>
                    <th>メイク</th>
                    <th>入浴</th>
                    <th>洗髪</th>
                </tr>
                <tr>
                    <td>
                        <ul>
                        <li>・顔全体のたるみ</li>
                        <li>・ほうれい線</li>
                        <li>・目じりのシワ</li>
                        <li>・あごから首に<br />　かけてのたるみ</li>
                        <li>・肌のハリつや</li>
                        </ul>
                    </td>
                    <td>
                        <ul>
                            <li>・3日後（ガーゼ交換）</li>
                            <li>・7日後（抜糸）</li>
                        </ul>
                    </td>
                    <td>2〜3時間</td>
                    <td>局所麻（+静脈麻酔）</td>
                    <td>ポイントメイクは<br />当日から可能</td>
                    <td>3日後から可能</td>
                    <td>3日後から可能</td>
                </tr>
            </table>
            <table class="pc_none">
                <tr>
                    <th>適応箇所</th>
                    <td>
                        <ul>
                        <li>・顔全体のたるみ</li>
                        <li>・ほうれい線</li>
                        <li>・目じりのシワ</li>
                        <li>・あごから首に<br />　かけてのたるみ</li>
                        <li>・肌のハリつや</li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <th>通院</th>
                    <td>
                        <ul>
                            <li>・3日後（ガーゼ交換）</li>
                            <li>・7日後（抜糸）</li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <th>施術時間</th>
                    <td>2〜3時間</td>
                </tr>
                <tr>
                    <th>麻酔</th>
                    <td>局所麻（+静脈麻酔）</td>
                </tr>
                <tr>
                    <th>メイク</th>
                    <td>ポイントメイクは<br />当日から可能</td>
                </tr>
                <tr>
                    <th>入浴</th>
                    <td>3日後から可能</td>
                </tr>
                <tr>
                    <th>洗髪</th>
                    <td>3日後から可能</td>
                </tr>
            </table>
    </article>
    <article class="block17">
    <div class="ex cf">
        <h2><img src="img/block11_h2.png" alt="症例写真" class="sp_none"><img src="img/block11_h2_sp.png" alt="症例写真" class="pc_none"></h2>
        <dl>
            <dt><img src="img/block17_before.png" alt="BEFORE"></dt>
            <dd>
                <img src="img/block17_ph01.jpg" alt="">
                ■手術前<br />
                皮膚がたるみ、ほうれい線が目立ちます。
            </dd>
        </dl>
        <dl>
            <dt><img src="img/block17_after.png" alt="AFTER"></dt>
            <dd>
                <img src="img/block17_ph02.jpg" alt="">
                ■手術後3ヶ月<br />
                お顔全体が引きあがり、若々しい印象です。
            </dd>
        </dl>
    </div>


        <h2><img src="img/block17_h2.png" alt="フェイスリフト手術のよくある質問" class="sp_none"><img src="img/block17_h2_sp.png" alt="フェイスリフト手術のよくある質問" class="pc_none"></h2>
        <div class="faq_wrap">
            <dl class="faq">
                <dt>フェイスリフトの手術を考えていますが、痛みがとても心配です。</dt>
                <dd>手術中は麻酔が効いており無痛です。またうとうとと軽く眠った状態で手術が終了します。術後麻酔が切れますと少しの痛みを感じる場合もございますが、痛み止め等で十分押さえる事ができる程度です。</dd>
                <dt>フェイスリフトの手術は入院が必要ですか？</dt>
                <dd>入院の必要はございません。術後クリニックで数時間しっかり休んでいただいてからお帰りいただきます。</dd>
                <dt>フェイスリフトの手術は通院が必要ですか？</dt>
                <dd>手術後に消毒や抜糸のための通院がございます。その後は数カ月に一度程度の定期的な検診をさせていただきます。最終的に満足いただくまで責任を持って診させていただきます。</dd>
                <dt>腫れはどれくらい続きますか？</dt>
                <dd>2～3日がピークで1週間ほどかけて少しずつ落ち着いていきます。2週間もすればだいぶ落ち着きます。</dd>
            </dl>
        </div>
    </article>




    <article class="block18">
        <h2><img src="img/block18_h2.png" alt="お客様の声" class="sp_none"><img src="img/block18_h2_sp.png" alt="お客様の声" class="pc_none"></h2>
        
        <div class="box_wrap cf">
            <div class="box">
                <p>美容治療は今回が初めてでしたが、スタッフの皆さんが温かい雰囲気で、とてもリラックスさせてくれたので、緊張せずにすみました。不安や疑問に思ったことも相談出来る環境です。アフターケアも充実していて大満足！</p>
            </div>
            <div class="box">
                <p>結婚式前なのでクリニックを受診。院内はとても明るく清潔でキレイで居心地が良かったです。やっぱり衛生管理がきちんとされている病院は安心出来ます。レーザーでしたが施術も痛くなく、あっという間で感激！</p>
            </div>
            <div class="box">
                <p>初めての経験で分らず不安でしたが、とても丁寧に相談にのって頂き大満足。カウンセリングに時間をかけて頂いたので、安心して治療を選択することが出来ました。今まで悩んでいたたるみが改善して、化粧にかける時間も減ったのでうれしいです。</p>
            </div>
        </div>

        
    </article>

    <div class="btn_box cf sp_none">
        <div class="txt">
            <img src="images/tk_cv_pc1.gif" alt="0120-334-270">
        </div>
        <div class="btn">
            <a href="#section15"><img src="img/btn_box_btn.png" alt="無料カウンセリングのご予約"></a>
        </div>
    </div>
    <div class="btn_box pc_none">
        <div class="tel_box">
            <a href="#section15"><img src="img/box_btn_sp.png" alt="無料カウンセリング予約"></a>
            <p class="tel"><a href="tel:0120334270" onclick="yahoo_report_conversion('tel:0120-334-270')" class="pc_none"><img src="images/tk_cv_sp1.gif" alt="0120-334-270 受付時間//11:00～20:00"></a></p>
<p class="tel"><a href="tel:0120931911" onclick="yahoo_report_conversion('tel:0120-931-911')" class="pc_none"><img src="images/tk_cv_sp2.gif" alt="0120-334-270 受付時間//11:00～20:00"></a></p>
            
        </div>
    </div>
</div>







<div id="section06" class="box">
	<article class="block19 cf">
    	<h2><img src="img/block19_h2.png" alt="お問い合わせから施術までの流れ" class="sp_none">
        <img src="img/block19_h2_sp.png" alt="お問い合わせから施術までの流れ" class="pc_none"></h2>
        
        <div class="intro">
        	<p>電話、もしくはメールでご予約を承っております。施術のご予約の前に、診療や施術、<br class="sp_none">その他について疑問がありましたら、お電話またはお問い合わせフォーム、メールからお気軽にお問い合わせください。<br />
            その際には匿名でのお問合せもOKです。</p>
        </div>

        <div class="cf">
            <div class="left">
                <h3><img src="img/block19_step01.png" alt="お電話、メールにてお問い合わせ" class="sp_none"><img src="img/block19_step01_sp.png" width="100%" alt="ご来院・受付" class="pc_none"></h3>
                <div class="cf pt0">
                    <img src="img/block19_step01_ph.jpg" class="right pc_none" alt="">
                <p>電話、もしくはメールでご予約を承っております。<br />施術のご予約の前に、診療や施術、その他について疑問がありましたら、お電話またはお問い合わせフォーム、メールからお気軽にお問い合わせください。その際には匿名でのお問合せもOKです。</p>
                </div>

                <div class="b08_tel sp_none cf">
                    <img src="images/tk_cv_pc3.gif" alt="お電話でのご相談　平日//11:00～20:00" class="block07_tel01">
                    <p><a href="#section15"><img src="img/block19_btn.png" alt="無料カウンセリング予約"></a></p>
                </div>
                <div class="pc_none">
                    
                    <p style="margin:0 auto 0;"><a href="tel:0120334270" onclick="yahoo_report_conversion('tel:0120-334-270')" class="pc_none"><img src="images/tk_cv_sp1.gif" width="100%" alt="0120-334-270" class="block07_tel02"></a></p>

<p style="margin:0 auto 0;"><a href="tel:0120334270" onclick="yahoo_report_conversion('tel:0120-931-911')" class="pc_none"><img src="images/tk_cv_sp2.gif" width="100%" alt="0120-931-911" class="block07_tel02"></a></p>

                    <p><a href="#section15"><img src="img/block19_btn_sp.png" alt="無料カウンセリング予約"></a></p>
                </div>


            </div>
            <img src="img/block19_step01_ph.jpg" class="right sp_none" alt="">
        </div>

        <div class="cf">
            <div class="left">
                <h3><img src="img/block19_step02.png" alt="ご来院・受付" class="sp_none"><img src="img/block19_step02_sp.png" alt="ご来院・受付" class="pc_none"></h3>
                <img src="img/block19_step02_ph.jpg" class="right pc_none" alt="">
                <p>お電話でご予約いただいた日程でご来院ください。（日程の変更などはお気軽にご連絡ください）<br>
            ご予約された日時にお越し下さい。<br>
            表参道スキンクリニックでのカウンセリング方法など事前に何でも相談ください。</p>
            </div>
            <img src="img/block19_step02_ph.jpg" class="right sp_none" alt="">
        </div>

        <div class="cf">
            <div class="left">
                <h3><img src="img/block19_step03.png" alt="スタッフとのカウンセリング" class="sp_none"><img src="img/block19_step03_sp.png" alt="スタッフとのカウンセリング" class="pc_none"></h3>
                <img src="img/block19_step03_ph.jpg" class="right pc_none" alt="">
                <p>お客様、患者様の悩みや疑問、施術費用やお支払方法ついてなど、専門のスタッフにお気軽に相談ください。</p>
                <p class="block_box">施術へのお手続き・施術申込書の記入、術後の注意事項をお渡しします。<br>未成年の方は同意書が必要になります。又はご両親のどちらかご同伴でお願いします。<br>※認印（シャチハタ以外）をお持ち下さい。</p>
            
            </div>
            <img src="img/block19_step03_ph.jpg" class="right sp_none" alt="">
        </div>

        <div class="cf">
            <div class="left">
                <h3><img src="img/block19_step04.png" alt="担当医師とのカウンセリング・診察" class="sp_none"><img src="img/block19_step04_sp.png" alt="担当医師とのカウンセリング・診察" class="pc_none"></h3>
                <img src="img/block19_step04_ph.jpg" class="right pc_none" alt="">
                <p>実際に施術を行う担当医師が丁寧にご要望をうかがった上で、プラン、施術方法、施術内容のご説明をいたします。</p>
            <p class="small">※施術内容の詳細は担当医師がご説明いたします。ご不明な点があればお気軽にご相談ください。</p>
            </div>
            <img src="img/block19_step04_ph.jpg" class="right sp_none" alt="">
        </div>

        <div class="cf">
            <div class="left">
                <h3><img src="img/block19_step05.png" alt="施術を行います" class="sp_none"><img src="img/block19_step05_sp.png" alt="施術を行います" class="pc_none"></h3>
                <img src="img/block19_step05_ph.jpg" class="right pc_none" alt="">
                <p>お顔の施術の場合は、事前にメイクを落としていただく必要がございます。<br>
            化粧落としなどご用意しておりますので、お気軽にスタッフにお声掛けください。<br>
            準備ができましたら施術室に移動していただき、施術を行います。<br>
            リラックスして施術を受けてください。</p>
            </div>
            <img src="img/block19_step05_ph.jpg" class="right sp_none" alt="">
        </div>

        <div class="cf">
            <div class="left">
                <h3><img src="img/block19_step06.png" alt="アフターケア" class="sp_none"><img src="img/block19_step06_sp.png" alt="アフターケア" class="pc_none"></h3>
                <img src="img/block19_step06_ph.jpg" class="right pc_none" alt="">
                <p>施術後はすぐにご帰宅頂けます。<br>
            専用のメイクルームをご用意いたしておりますので、メイク直しをしながら安心して新しいご自分と対面していただけます。施術後のアフターケアの料金は手術料金に含まれております。</p>
            </div>
            <img src="img/block19_step06_ph.jpg" class="right sp_none" alt="">
        </div>


    </article>

    <div class="btn_box sp_none">
    <div class="cf">
        <div class="txt">
            <img src="images/tk_cv_pc1.gif" alt="0120-334-270">
        </div>
        <div class="btn">
            <a href="#section15"><img src="img/btn_box_btn.png" alt="無料カウンセリングのご予約"></a>
        </div>
    </div>
        
    </div>
    <div class="btn_box pc_none">
        <div class="tel_box">
            <a href="#section15"><img src="img/box_btn_sp.png" alt="無料カウンセリング予約"></a>
            <p class="tel"><a href="tel:0120334270" onclick="yahoo_report_conversion('tel:0120-334-270')" class="pc_none"><img src="images/tk_cv_sp1.gif" alt="0120-334-270 受付時間//11:00～20:00"></a></p>
<p class="tel"><a href="tel:0120931911" onclick="yahoo_report_conversion('tel:0120-931-911')" class="pc_none"><img src="images/tk_cv_sp2.gif" alt="0120-334-270 受付時間//11:00～20:00"></a></p>
            
        </div>
    </div>
</div>



<!--
<div id="section07" class="box">
	<article class="block20 cf">
        <h2><img src="img/block20_h2.png" alt="院内ご案内" class="sp_none"><img src="img/block20_h2_sp.png" alt="院内ご案内" class="pc_none"></h2>
        <p>当院では安心してご利用頂ける万全の環境を整えておりますので、安心してお越しください。<br />皆さまのご来院をお待ちしております。</p>


      <div class="sp_none">
        <ul class="bxslider cf">
          <li><a href="img/slider02_big.jpg" rel="lightbox"><img src="img/slider01_sp.jpg" width="100%" alt="受付"></a></li>
          <li><a href="img/slider03_big.jpg" rel="lightbox"><img src="img/slider02_sp.jpg" width="100%" alt="待合室"></a></li>
          <li><a href="img/slider04_big.jpg" rel="lightbox"><img src="img/slider03_sp.jpg" width="100%" alt="カウンセリングルーム"></a></li>
          <li><a href="img/slider05_big.jpg" rel="lightbox"><img src="img/slider04_sp.jpg" width="100%" alt="診察室"></a></li>
          <li><a href="img/slider06_big.jpg" rel="lightbox"><img src="img/slider05_sp.jpg" width="100%" alt="診察室"></a></li>
          <li><a href="img/slider01_big.jpg" rel="lightbox"><img src="img/slider06_sp.jpg" width="100%" alt="施術室"></a></li>
        </ul>
      </div>



      <div class="pc_none">
        <ul class="bxslider cf">
          <li><img src="img/slider01_sp.jpg" width="100%" alt="受付"></li>
          <li><img src="img/slider02_sp.jpg" width="100%" alt="待合室"></li>
          <li><img src="img/slider03_sp.jpg" width="100%" alt="カウンセリングルーム"></li>
          <li><img src="img/slider04_sp.jpg" width="100%" alt="診察室"></li>
          <li><img src="img/slider05_sp.jpg" width="100%" alt="診察室"></li>
          <li><img src="img/slider06_sp.jpg" width="100%" alt="施術室"></li>
        </ul>
      </div>
    </article>


     <div class="btn_box sp_none">
    <div class="cf">
        <div class="txt">
            <img src="images/tk_cv_pc1.gif" alt="0120-334-270">
        </div>
        <div class="btn">
            <a href="#section15"><img src="img/btn_box_btn.png" alt="無料カウンセリングのご予約"></a>
        </div>
    </div>
        
    </div>
    <div class="btn_box pc_none">
        <div class="tel_box">
            <a href="#section15"><img src="img/box_btn_sp.png" alt="無料カウンセリング予約"></a>
            <p class="tel"><a href="tel:0120334270" onclick="yahoo_report_conversion('tel:0120-334-270')" class="pc_none"><img src="images/tk_cv_sp1.gif" alt="0120-334-270 受付時間//11:00～20:00"></a></p>
<p class="tel"><a href="tel:0120931911" onclick="yahoo_report_conversion('tel:0120-931-911')" class="pc_none"><img src="images/tk_cv_sp2.gif" alt="0120-334-270 受付時間//11:00～20:00"></a></p>
            
        </div>
    </div>
</div>
-->




<div id="section08" class="box">
	<article class="block21 cf">
    	<h2><img src="img/block21_h2.png" alt="表参道スキンクリニックの理念" class="sp_none"><img src="img/block21_h2_sp.png" alt="表参道スキンクリニックの理念" class="pc_none"></h2>
        <p class="text">開業当初から当クリニックでは<br class="pc_none">痛みや不安のない施術を心がけており、<br>患者様に安心して美しくなって頂くことを<br class="pc_none">目指しております。</p>

        <div class="list cf">
            <div class="left">
                <h3><img src="img/block21_concept01.png" alt="患者様とのコミュニケーションを大切にしています" class="sp_none"><img src="img/block21_concept01_sp.png" alt="患者様とのコミュニケーションを大切にしています" class="pc_none"></h3>
                <img src="img/block21_ph01.jpg" alt="">
                <p>当クリニックは患者様の立場に立ち、今抱えている不安や悩みなどを解消するため、丁寧なカウンセリングを行なっています。</p>
            </div>
            <div class="left">
                <h3><img src="img/block21_concept02.png" alt="長年の経験と実績による確かな技術と高い信頼" class="sp_none"><img src="img/block21_concept02_sp.png" alt="長年の経験と実績による確かな技術と高い信頼" class="pc_none"></h3>
                <img src="img/block21_ph02.jpg" alt="">
                <p>当クリニックは優秀な美容皮膚科医が多数在籍しています。本当に安心して任せられるドクターがレベルの高い治療を行ないます。</p>
            </div>
            <div class="left">
                <h3><img src="img/block21_concept03.png" alt="いつもドクターがそばにいてくれるという感覚" class="sp_none"><img src="img/block21_concept03_sp.png" alt="いつもドクターがそばにいてくれるという感覚" class="pc_none"></h3>
                <img src="img/block21_ph03.jpg" alt="">
                <p>当クリニックは気軽に相談できる環境作りを心がけています。術後のケア、美肌づくりのアドバイスなどいつでもすぐにお応えいたします。</p>
            </div>
        </div>
    </article>

    <div class="btn_box pc_none">
        <div class="tel_box">
            <a href="#section15"><img src="img/box_btn_sp.png" alt="無料カウンセリング予約"></a>
            <p class="tel"><a href="tel:0120334270" onclick="yahoo_report_conversion('tel:0120-334-270')" class="pc_none"><img src="images/tk_cv_sp1.gif" alt="0120-334-270 受付時間//11:00～20:00"></a></p>
<p class="tel"><a href="tel:0120931911" onclick="yahoo_report_conversion('tel:0120-931-911')" class="pc_none"><img src="images/tk_cv_sp2.gif" alt="0120-334-270 受付時間//11:00～20:00"></a></p>
            
        </div>
    </div>
</div>


<!--
<div id="section09" class="box">
	<article class="block22 cf">
    	<h2><img src="img/block22_h2.png" alt="医師のご紹介" class="sp_none"><img src="img/block22_h2_sp.png" alt="医師のご紹介" class="pc_none"></h2>
        
        <p class="intro">当クリニックの医師たちは、お客様に安全に美しくなって頂く為に、日々最高水準の技術を日々研究しております。<br />そんな医師たちをご紹介させていただきます。</p>

        <div class="doctor_box cf">
            <p class="name"><img src="img/block22_name01.png" alt="院長　松木貴裕" class="sp_none"><img src="img/block22_name01_sp.png" alt="院長　松木貴裕" class="pc_none"></p>
            <img src="img/block22_ph01.jpg" alt="" class="left">
            <div class="right">
                <div class="fl">
                    <h3>経歴</h3>
                    <dl class="cf">
                      <dt>平成10年3月</dt>
                      <dd>藤田保健衛生大学 医学部医学研究所科 卒業</dd>
                    </dl>
                    <dl class="cf">
                      <dt>平成11年9月</dt>
                      <dd>医療法人大医会 日進おりど病院 勤務</dd>
                    </dl>
                    <dl class="cf">
                      <dt>平成14年3月</dt>
                      <dd>医療法人大医会 日進おりど病院 退職</dd>
                    </dl>
                    <dl class="cf">
                      <dt>平成14年4月</dt>
                      <dd>東京女子医科大学病院 皮膚科 勤務</dd>
                    </dl>
                    <dl class="cf">
                      <dt>平成16年3月</dt>
                      <dd>東京女子医科大学病院 皮膚科 退職</dd>
                    </dl>
                    <dl class="cf">
                      <dt>平成16年9月</dt>
                      <dd>東京美容外科 開設 勤務</dd>
                    </dl>
                </div>
                <div class="fr">
                    <h3>所属学会など</h3>
                    <ul class="cf">
                      <li>・日本皮膚科学会正会員</li>
                      <li>・レーザー医学会認定医</li>
                      <li>・日本抗加齢学会会員</li>
                      <li>・美容外科学会会員</li>
                    </ul>
                </div>
                <h3 class="clear">松木先生から一言</h3>
            <p class="bottom_text">かつては、大がかりな美容整形手術など、ハイリスクなイメージがあった美容医療の世界。しかし近年では、手軽にトライできる治療も増え、とても身近な存在になりました。当クリニックでは、高い技術を持つ経験豊富な美容皮膚科医が、患者様の希望や理想を大切にしながら、最新の美容医療を提供いたします。</p>
            </div>
        </div>


        <div class="doctor_box cf">
            <p class="name"><img src="img/block22_name02.png" alt="友利新" class="sp_none"><img src="img/block22_name02_sp.png" alt="友利新" class="pc_none"></p>
            <img src="img/block22_ph02.jpg" alt="" class="left">
            <div class="right">
                <div class="fl">
                    <h3>経歴</h3>
                    <p>
            東京女子医科大学卒業。<br>
            同大学病院の内科勤務を経て皮膚科へ転科。<br>
            現在、都内2か所のクリニックに勤務の傍ら、医師という立場から美容と健康を医療として追求し、美しく生きる為の啓蒙活動を雑誌・TV などで展開中。
            </p>
            <p>
            2004年第36回準ミス日本という経歴をもつ、美貌の新進医師。<br>
            美と健康に関する著書も多数あり、近著に『女性のキレイと健康をつくる 美肌タイミングジュース』（保健同人社より2011年6月10日発刊）がある。
            </p>
                </div>
                <div class="fr">
                    <h3>所属学会など</h3>
                    <ul class="cf">
                <li>・日本内科学会会員</li>
                <li>・日本糖尿病学会会員</li>
                <li>・日本皮膚科学会会員</li>
                <li>・抗加齢学会会員</li>
            </ul>
                </div>
                <h3 class="clear">友利先生から一言</h3>
            <p class="bottom_text">女性が綺麗になりたいという思いは変わらないものです。そんな願いを手軽に、そして確実に医療の技術で叶えたい。そう思いながら日々の診療をさせてもらっています。 一人でも多くの女性の笑顔に自信を持っていただけるように、皆さんの美の主治医としていろいろな悩みにお応えしていきます。</p>
            </div>
        </div>


    </article>
    <div class="btn_box sp_none">
    <div class="cf">
        <div class="txt">
            <img src="images/tk_cv_pc1.gif" alt="0120-334-270">
        </div>
        <div class="btn">
            <a href="#section15"><img src="img/btn_box_btn.png" alt="無料カウンセリングのご予約"></a>
        </div>
    </div>
        
    </div>
    <div class="btn_box pc_none">
        <div class="tel_box">
            <a href="#section15"><img src="img/box_btn_sp.png" alt="無料カウンセリング予約"></a>
            <p class="tel"><a href="tel:0120334270" onclick="yahoo_report_conversion('tel:0120-334-270')" class="pc_none"><img src="images/tk_cv_sp1.gif" alt="0120-334-270 受付時間//11:00～20:00"></a></p>
<p class="tel"><a href="tel:0120931911" onclick="yahoo_report_conversion('tel:0120-931-911')" class="pc_none"><img src="images/tk_cv_sp2.gif" alt="0120-334-270 受付時間//11:00～20:00"></a></p>
            
        </div>
    </div>
</div>
-->

<!--
<div id="section10" class="box">

	<article class="block23 cf">
    	<h2><img src="img/block23_h2.png" alt="アクセス" class="sp_none"><img src="img/block23_h2_sp.png" alt="アクセス" class="pc_none"></h2>
        
	 <iframe class="accecc" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3241.39689876405!2d139.707413!3d35.667228!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x60188ca3e16206fd%3A0x92146d294d00108b!2z6KGo5Y-C6YGT44K544Kt44Oz44Kv44Oq44OL44OD44Kv!5e0!3m2!1sja!2sjp!4v1433309200061" frameborder="0" style="border:0"></iframe>

     <div class="cf">
         <div class="left">
            <h3 class="sp_none"><img src="img/block23_icon01.png" alt="電車でお越しの方"></h3>
            <h3 class="pc_none"><img src="img/block23_icon01_sp.png" alt="電車でお越しの方"></h3>
            <ul>
                <li class="no01">JR山手線「原宿駅」表参道口より徒歩約5分</li>
                <li class="no02">東京メトロ各線「表参道駅」A1出口より徒歩約3分</li>
                <li class="no03">東京メトロ各線「明治神宮前駅」4番出口より徒歩約3分</li>
            </ul>
        </div>
        <div class="left">
            <h3 class="sp_none"><img src="img/block23_icon02.png" alt="お車でお越しの方"></h3>
            <h3 class="pc_none"><img src="img/block23_icon02_sp.png" alt="お車でお越しの方"></h3>
            <p class="mb0">当クリニック専用の駐車はございません。<br />お近くの有料駐車場のご利用をお願いいたします。</p>
        </div>
        <div class="left">
            <h3 class="sp_none"><img src="img/block23_icon03.png" alt="治療時間・休診日"></h3>
            <h3 class="pc_none"><img src="img/block23_icon03_sp.png" alt="治療時間・休診日"></h3>
            <img src="img/block23_data.png" alt="月・火・木・金・土　11:00～20:00　日・祝　10:30～18:00　休診日　水曜日" class="time">
        </div>
     </div>
        
    </article>
    <div class="btn_box sp_none">
    <div class="cf">
        <div class="txt">
            <img src="images/tk_cv_pc1.gif" alt="0120-334-270">
        </div>
        <div class="btn">
            <a href="#section15"><img src="img/btn_box_btn.png" alt="無料カウンセリングのご予約"></a>
        </div>
    </div>
        
    </div>
    <div class="btn_box pc_none">
        <div class="tel_box">
            <a href="#section15"><img src="img/box_btn_sp.png" alt="無料カウンセリング予約"></a>
            <p class="tel"><a href="tel:0120334270" onclick="yahoo_report_conversion('tel:0120-334-270')" class="pc_none"><img src="img/box_tel_sp.png" alt="0120-334-270 受付時間//11:00～20:00"></a>
            <img src="img/box_tel_txt_sp.png" alt="月・火・木・金・土　11:00～20:00　日・祝　10:30～18:00　休診日　水曜日"></p>
            
        </div>
    </div>

</div>
-->

<?php include 'tabs/index.html'; ?>

<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/smoothness/jquery-ui.css" >
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1/i18n/jquery.ui.datepicker-ja.min.js"></script>
<script>
$(function() {
	$("#datepicker_1").datepicker({
		dateFormat: 'yy年mm月dd日 (DD)',
		dayNames: ['日', '月', '火', '水', '木', '金', '土']
	});
	$("#datepicker_2").datepicker({
		dateFormat: 'yy年mm月dd日 (DD)',
		dayNames: ['日', '月', '火', '水', '木', '金', '土']
	});
});
</script>

	<div id="section15" class="box">

		<article class="block24 cf">
			<h2><img src="img/block24_h2.png" alt="無料カウンセリングご予約フォーム" class="sp_none"><img src="img/block24_h2_sp.png" alt="無料カウンセリングご予約フォーム" class="pc_none"></h2>

			<p class="flow"><img src="img/block24_flow.png" alt="1．情報の入力" class="sp_none"/><img src="img/block24_flow_sp.png" alt="1．情報の入力" class="pc_none"/></p>

				<div class="form">

					<p>※お手数ですが、必須項目をすべてご入力の上、確認画面へお進みください。</p>

        <form method="post">

          <table>
            <tr>
              <th class="hissu">お名前</th>
              <td class="name">
                <input type="text" name="name_req" value="<?php echo $formTool->h($_POST['name_req']); ?>" placeholder="例）表参道花子" />　
                <?php if( !empty($formTool->messages['name_req']) ) : ?>
                  <p class="error"><?php echo $formTool->h($formTool->messages['name_req']); ?></p>
                <?php endif; ?>
              </td>
            </tr>
            <tr>
              <th class="hissu">電話番号</th>
              <td>
                <input type="tel" name="tel_req" value="<?php echo $formTool->h($_POST['tel_req']); ?>" placeholder="例）01-2345-6789" />　
                <?php if( !empty($formTool->messages['tel_req']) ) : ?>
                  <p class="error"><?php echo $formTool->h($formTool->messages['tel_req']); ?></p>
                <?php endif; ?>
              </td>
            </tr>
            <tr>
              <th class="hissu">メールアドレス</th>
              <td>
                <input type="email" name="email_req" value="<?php echo $formTool->h($_POST['email_req']); ?>" placeholder="例）info@abc.com" />　
                <?php if( !empty($formTool->messages['email_req']) ) : ?>
                  <p class="error"><?php echo $formTool->h($formTool->messages['email_req']); ?></p>
                <?php endif; ?>
              </td>
            </tr>
            <tr>
              <th class="hissu">年齢</th>
              <td>
                <span><input type="text" name="age_req" value="<?php echo $formTool->h($_POST['age_req']); ?>" class="number" /> 歳</span>
                <?php if( !empty($formTool->messages['age_req']) ) : ?>
                  <p class="error"><?php echo $formTool->h($formTool->messages['age_req']); ?></p>
                <?php endif; ?>
              </td>
            </tr>
            <!--
			<tr>
              <th class="hissu">ご希望のクリニック</th>
              <td>
                <?php echo $formTool->makeRadioHtml($formTool->hopeArr, 'array', $_POST['hope_req'], 'hope_req'); ?>
                <?php if( !empty($formTool->messages['hope_req']) ) : ?>
                  <p class="error"><?php echo $formTool->h($formTool->messages['hope_req']); ?></p>
                <?php endif; ?>
              </td>
            </tr>
			-->
            <tr>
              <th class="nini">相談日 第一希望</th>
              <td>
                <input type="text" name="day1" value="<?php echo $formTool->h($_POST['day1']); ?>" placeholder="YYYY年MM月DD日" class="calendar" id="datepicker_1" readonly/>
                <select name="day1_period">
                  <option value="">時間帯を選択</option>
                  <?php echo $formTool->makeOptionHtml($formTool->timeArr, 'array', $_POST['day1_period']); ?>
                </select>
              </td>
            </tr>
            <tr>
              <th class="nini">相談日 第二希望</th>
              <td>
                <input type="text" name="day2" value="<?php echo $formTool->h($_POST['day2']); ?>" placeholder="YYYY年MM月DD日" class="calendar" id="datepicker_2" readonly/>
                <select name="day2_period">
                  <option value="">時間帯を選択</option>
                  <?php echo $formTool->makeOptionHtml($formTool->timeArr, 'array', $_POST['day2_period']); ?>
                </select>
              </td>
            </tr>
            <tr>
              <th class="nini">連絡方法のご希望</th>
              <td class="radiostyle">
                <?php echo $formTool->makeCheckBoxHtml($formTool->typeArr, 'array', $_POST['type'], 'type'); ?>
              </td>
            </tr>
            <tr>
              <th class="nini last">お問い合わせ内容</th>
              <td class="last">
                <textarea name="message" placeholder="お問い合わせ内容をご記入ください。" cols="20" rows="5"><?php echo $formTool->h($_POST['message']); ?></textarea>
              </td>
            </tr>
          </tbody>
        </table>

        <input type="hidden" name="mode" value="conf" />
        <p class="btn"><input type="image" src="img/block24_btn.png" alt="確認画面へ" class="sp_none" />
        <input type="image" src="img/block24_btn_sp.png" alt="確認画面へ" class="pc_none" /></p>

      </form>

			</div>

		</article>

		<p class="bottom_logosp"><img src="img/block24_btmcopy.png" alt="表参道スキンクリニックは、どこまでも綺麗への想いにお応えし続けます" class="sp_none"><img src="img/block24_btmcopy_sp.png" alt="表参道スキンクリニックは、どこまでも綺麗への想いにお応えし続けます" class="pc_none"></p>

	</div>

	<p id="page-top"><a href="#wrapper"><img src="img/top.png" width="45" height="45" alt="TOP"></a></p>

</div>