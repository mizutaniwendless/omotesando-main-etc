<?php
class formClass {
	function __construct() {
		$this->forms = array(
			'name_req'=> array(
				'name' => 'お名前（ひらがな）',
				'require' => true,
			),
			'tel_req' => array(
				'name' => '電話番号',
				'require' => true,
				'zip' => true,
			),
			'email_req' => array(
				'name' => 'メールアドレス',
				'require' => true,
				'email' => true,
			),
		);
		$this->messages = array();

		$this->prefectureArr = array(
			'北海道',
			'青森県',
			'岩手県',
			'宮城県',
			'秋田県',
			'山形県',
			'福島県',
			'茨城県',
			'栃木県',
			'群馬県',
			'埼玉県',
			'千葉県',
			'東京都',
			'神奈川県',
			'新潟県',
			'富山県',
			'石川県',
			'福井県',
			'山梨県',
			'長野県',
			'岐阜県',
			'静岡県',
			'愛知県',
			'三重県',
			'滋賀県',
			'京都府',
			'大阪府',
			'兵庫県',
			'奈良県',
			'和歌山県',
			'鳥取県',
			'島根県',
			'岡山県',
			'広島県',
			'山口県',
			'徳島県',
			'香川県',
			'愛媛県',
			'高知県',
			'福岡県',
			'佐賀県',
			'長崎県',
			'熊本県',
			'大分県',
			'宮崎県',
			'鹿児島県',
			'沖縄県',);
			$this->hostname = '(?:[_a-z0-9][-_a-z0-9]*\.)*(?:[a-z0-9][-a-z0-9]{0,62})\.(?:(?:[a-z]{2}\.)?[a-z]{2,})';

			$this->typeArr = array(
				'電話',
				'メール',
			);

			$this->timeArr = array(
				"10:30",
				"11:00",
				"11:30",
				"12:00",
				"12:30",
				"13:00",
				"13:30",
				"14:00",
				"14:30",
				"15:00",
				"15:30",
				"16:00",
				"16:30",
				"17:00",
				"17:30",
				"18:00",
				"18:30",
				"19:00",
			);

		}

		function h($text, $double = true, $charset = null) {
			if (is_array($text)) {
				$texts = array();
				foreach ($text as $k => $t) {
					$texts[$k] = h($t, $double, $charset);
				}
				return $texts;
			} elseif (is_object($text)) {
				if (method_exists($text, '__toString')) {
					$text = (string) $text;
				} else {
					$text = '(object)' . get_class($text);
				}
			} elseif (is_bool($text)) {
				return $text;
			}

			static $defaultCharset = false;
			if ($defaultCharset === false) {
				$defaultCharset = 'UTF-8';
				if ($defaultCharset === null) {
					$defaultCharset = 'UTF-8';
				}
			}
			if (is_string($double)) {
				$charset = $double;
			}
			return htmlspecialchars($text, ENT_QUOTES, ($charset) ? $charset : $defaultCharset, $double);
		}
		function is_inputerror() {
			$ret = false;
			if($_POST['mode'] == 'conf') {
				foreach($this->forms as $key => $form) {
					if($form['require']) {
						if (empty($_POST[$key]) && '0' != $_POST[$key]) {
							$this->messages[$key]= "「{$form['name']}」を入力してください。{$_POST[$key]}";
							$ret = true;
						}
					}
					if($form['require2']) {
						if (empty($_POST[$key]) && '0' != $_POST[$key]) {
							$this->messages[$key]= "「{$form['name']}」にチェックを入れてください。";
							$ret = true;
						}
					}
					if($form['sentaku']) {
						if ((empty($_POST[$key]) && '0' == $_POST[$key]) or ('' == $_POST[$key])) {
							$this->messages[$key]= "「{$form['name']}」を選択してください。";
							$ret = true;
						}
					}
					if($form['policy']) {
						if (empty($_POST[$key])) {
							$this->messages[$key]= "同意される場合はチェックを入れてください。";
							$ret = true;
						}
					}
					if($form['number'] && empty($this->messages[$key])) {
						if(!empty($_POST[$key])) {
							if(!preg_match("/^[0-9]+$/", $_POST[$key])){
								$this->messages[$key]= "「{$form['name']}」は半角数値で入力してください。";
								$ret = true;
							}
						}
					}
					if($form['zip'] && empty($this->messages[$key])) {
						if(!empty($_POST[$key])) {
							if(!preg_match("/^[0-9-]+$/", $_POST[$key])){
								$this->messages[$key]= "「{$form['name']}」は半角数値、ハイフンで入力してください。";
								$ret = true;
							}
						}
					}
					if($form['hankaku'] && empty($this->messages[$key])) {
						if(!empty($_POST[$key])) {
							if(!preg_match("/^[!-~]+$/", $_POST[$key])){
								$this->messages[$key]= "「{$form['name']}」は半角英数で入力してください。";
								$ret = true;
							}
						}
					}
					if($form['zenkaku'] && empty($this->messages[$key])) {
						if(!empty($_POST[$key])) {
							if($_POST[$key] !== mb_convert_kana($_POST[$key], 'ASKH', 'UTF-8')){
								$this->messages[$key]= "「{$form['name']}」は全角で入力してください。";
								$ret = true;
							}
						}
					}
					if($form['email'] && empty($this->messages[$key])) {
						if(!preg_match('/^[a-z0-9!#$%&\'*+\/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&\'*+\/=?^_`{|}~-]+)*@' . $this->hostname . '$/i', $_POST[$key])){
							$this->messages[$key]= "「{$form['name']}」を正しく入力してください。";
							$ret = true;
						}
					}
					if($form['same'] && empty($this->messages[$key])) {
						if($_POST[$key] != $_POST[$form['same']]) {
							$this->messages[$key]= "「{$form['name']}」は確認用と同じ内容を入力してください。";
							$ret = true;
						}
					}
					if($form['other_flg'] && empty($this->messages[$key])) {
						if (empty($_POST['order_content_txt']) && 'その他' == $_POST[$key]) {
							$this->messages[$key]= "「その他」を選択した場合は内容を入力してください。";
							$ret = true;
						}
					}
				}
			}
			return $ret;
		}

		function send_return_mail() {
			$messages = '';
			$subjects = '[表参道スキンクリニック]無料カウンセリングのご予約を受け付けました';
			$messages .= "{$_POST['name_req']}様\r\n";
			$messages .= "\r\n";
			$messages .= "表参道スキンクリニックです。\r\n";
			$messages .= "このたびは無料カウンセリングにお申込みいただき、ありがとうございます。\r\n";
			$messages .= "\r\n";
			$messages .= "当院スタッフより、折り返し確認のご連絡をさせていただきます。\r\n";
			$messages .= "*ご相談の日時を確認後、予約が確定となります。\r\n";
			$messages .= "\r\n";
			$messages .= "～ご注意～\r\n";
			$messages .= "本メールを受信された段階では、予約は確定しておりません。\r\n";
			$messages .= "本メールは自動で送信しておりますため、返信されましても、対応いたしかねますのでご了承ください。\r\n";
			$messages .= "\r\n";
			$messages .= "■アクセス\r\n";
			$messages .= "地図：https://omotesando-skin.jp/clinic/omotesando/\r\n";
			$messages .= "電話番号：0120-334-270\r\n";
			$messages .= "※当日場所がわからない場合はお電話でお問い合わせください\r\n";
			$messages .= "\r\n";
			$messages .= $this->get_mail_text_input_part();

			mb_language('japanese');
			mb_internal_encoding('UTF-8');
			$mail = "{$_POST['email_req']}";
			$headers = "MIME-Version: 1.0 \r\n";
			$headers = "Content-type: text/plain; charset=ISO-2022-JP\r\n";
			$headers .= "From: ".mb_encode_mimeheader('表参道スキンクリニック')."<info@tk-honin.com> \r\n";//★★★送信元のメールアドレス★★★
			mb_send_mail($mail, $subjects ,$messages, $headers);
		}

		function send_admin_mail() {
			// カウントアップは不要
			// $wk_cnt = $this->cnt_up();
			// $wk_cnt = str_pad($wk_cnt, 4, 0, STR_PAD_LEFT);
			// $user_type = $this->user_agent();
			// if($user_type == "others"){
			// 	$user_type = "PC";
			// }else{
			// 	$user_type = "スマホ";
			// }

			$messages = '';
			$subjects = '[タトゥー除去]無料カウンセリング予約申込み通知';
			$messages .= "無料カウンセリング予約より申込がありました。\r\n";
			$messages .= "タトゥー除去\r\n";
			$messages .= "\r\n";
			$messages .= $this->get_mail_text_input_part();

			mb_language('japanese');
			mb_internal_encoding('UTF-8');
			// $mail = "y-hanami@cn-door.com,k-tanaka@cn-door.com";//★★★問い合わせ受付先のメールアドレス★★★
			$mail = "info@tk-honin.com,tk-honin@mrfusion.co.jp";
			// $mail = "info@tk-honin.com";
			//$mail = "saito@mrfusion.co.jp";
			$headers = "MIME-Version: 1.0 \r\n";
			$headers = "Content-type: text/plain; charset=ISO-2022-JP\r\n";
			$headers .= "From: ".mb_encode_mimeheader($_POST['name_req'].'様')."<{$_POST['email_req']}> \r\n";//★★★送信元のメールアドレス★★★
			$headers .= "Reply-To: ".mb_encode_mimeheader($_POST['name_req'].'様')."<{$_POST['email_req']}> \r\n";
			mb_send_mail($mail, $subjects ,$messages, $headers);
		}

		function get_mail_text_input_part() {
			$messages = '';
			$messages .= "【お名前（ひらがな）】\r\n";
			$messages .= "{$_POST['name_req']}\r\n";
			$messages .= "\r\n";
			$messages .= "【電話番号】\r\n";
			$messages .= "{$_POST['tel_req']}\r\n";
			$messages .= "\r\n";
			$messages .= "【メールアドレス】\r\n";
			$messages .= "{$_POST['email_req']}\r\n";
			$messages .= "\r\n";
			$messages .= "【相談日 第一希望】\r\n";
			$messages .= "{$_POST['day1']} {$_POST['day1_period']}\r\n";
			$messages .= "\r\n";
			$messages .= "【相談日 第二希望】\r\n";
			$messages .= "{$_POST['day2']} {$_POST['day2_period']}\r\n";
			$messages .= "\r\n";
			$messages .= "【連絡方法のご希望】\r\n";
			$type_value = $_POST['type'];
			if(is_array($type_value)){
				$messages .= implode(',', $_POST['type'])."\r\n";
			}else{
				$messages .= $_POST['type']."\r\n";
			}
			$messages .= "\r\n";
			$messages .= "【お問い合わせ内容】\r\n";
			$messages .= "{$_POST['message']}\r\n";
			$messages .= "\r\n";
			return $messages;
		}

		/*
		* @param array $options <option>を生成する配列
		* @param string $type $optionsが配列ならarray,連想配列ならhash
		* @param string $selected <option>にselectedを入れる値
		* @return string
		*/
		function makeOptionHtml($options=array(), $type='array', $selected) {
			$optionHtml = '';
			foreach ($options as $key => $val) {
				if ($type = 'array') {
					$key = $val;
				}
				if ($key == $selected){
					$attr = 'selected="selected"';
				}else {
					$attr = '';
				}
				$optionHtml .= "<option value=\"{$key}\" {$attr}>{$val}</option>";
			}
			return $optionHtml;
		}
		function makeCheckBoxHtml($options=array(), $type='array', $selected, $name, $attr='') {
			$optionHtml = '';
			$attr_org = $attr;
			$cnt = 0;
			foreach ($options as $key => $val) {
				$attr = $attr_org;
				if ($type == 'array') {
					$key = $val;
				}
				if (is_array($selected)) {
					if (in_array($key, $selected)) {
						$attr .= 'checked="checked"';
					}
				} else {
					if ($key == $selected) {
						$attr .= 'checked="checked"';
					}
				}
				$optionHtml .= "<label for=\"{$name}{$cnt}\"><input type=\"checkbox\" name =\"{$name}[]\" id=\"{$name}{$cnt}\" value=\"{$key}\" {$attr}>{$val}</label>";
				$cnt++;
			}
			return $optionHtml;
		}

		function makeRadioHtml($options=array(), $type='array', $selected, $name, $attr='') {
			$optionHtml = '';
			$attr_org = $attr;
			$cnt = 0;
			foreach ($options as $key => $val) {
				$attr = $attr_org;
				if ($type = 'array') {
					$key = $val;
				}
				if ($key == $selected){
					$attr .= 'checked="checked"';
				}
				$optionHtml .= "<label for=\"{$name}{$cnt}\"><input type=\"radio\" name =\"{$name}\" id=\"{$name}{$cnt}\" value=\"{$key}\" {$attr}>{$val}</label>";
				$cnt++;
			}
			return $optionHtml;
		}
		function cnt_up(){
			$in_cnt = "";
			$cnt_file = "./data/cnt.txt";
			if( !file_exists($cnt_file) ){
				touch( $cnt_file );    		// ファイル作成
				chmod( $cnt_file, 0666 );	// ファイルのパーティションの変更
			}
			$fp = fopen($cnt_file, 'r+');
			if ($fp){
				if (flock($fp, LOCK_EX)){
					$in_cnt = fgets($fp);
					if($in_cnt){
						$in_cnt++;
					}else{
						$in_cnt=1;
					}
					rewind($fp);
					if (fwrite($fp,  $in_cnt) === FALSE){
						print('ファイル書き込みに失敗しました');
					}
					flock($fp, LOCK_UN);
				}
			}
			fclose($fp);
			return  $in_cnt;
		}
		function user_agent(){
			$ua = mb_strtolower($_SERVER['HTTP_USER_AGENT']);
			$return = "";
			if(strpos($ua,'iphone') !== false){
				$return = 'mobile(iphone)';
			}elseif(strpos($ua,'ipod') !== false){
				$return = 'mobile(ipod)';
			}elseif((strpos($ua,'android') !== false) && (strpos($ua, 'mobile') !== false)){
				$return = 'mobile(android)';
			}elseif((strpos($ua,'windows') !== false) && (strpos($ua, 'phone') !== false)){
				$return = 'mobile(windows)';
			}elseif((strpos($ua,'firefox') !== false) && (strpos($ua, 'mobile') !== false)){
				$return = 'mobile(firefox)';
			}elseif(strpos($ua,'blackberry') !== false){
				$return = 'mobile(blackberry)';
			}elseif(strpos($ua,'ipad') !== false){
				$return = 'tablet(ipad)';
			}elseif((strpos($ua,'windows') !== false) && (strpos($ua, 'touch') !== false)){
				$return = 'tablet(windows)';
			}elseif((strpos($ua,'android') !== false) && (strpos($ua, 'mobile') === false)){
				$return = 'tablet(android)';
			}elseif((strpos($ua,'firefox') !== false) && (strpos($ua, 'tablet') !== false)){
				$return = 'tablet(firefox)';
			}elseif((strpos($ua,'kindle') !== false) || (strpos($ua, 'silk') !== false)){
				$return = 'tablet(kindle)';
			}elseif((strpos($ua,'playbook') !== false)){
				$return = 'tablet(playbook)';
			}else{
				$return = 'others';
			}
			return $return;
		}
		function save_csv() {

			$list = array (
			array(
				date('c'),
				$_SERVER['HTTP_USER_AGENT'],
				$_SERVER["REMOTE_ADDR"],
				$_POST['q1'],
				$_POST['q2'],
				$_POST['q3'],
				$_POST['q4'],
				$_POST['q5'],
				$_POST['q6'],
				$_POST['sei'],
				$_POST['mei'],
				$_POST['sei_kana'],
				$_POST['mei_kana'],
				$_POST['sex'],
				$_POST['email'],
				$_POST['zip'],
				$_POST['prefecture'],
				$_POST['address1'],
				$_POST['address2'],
				$_POST['tel'],
			),
		);

		foreach($list as $key => $value ){
			for ($i = 0; $i < count($list[$key]); $i++) {
				if ($i < count($list[$key])-1) {
					$csv_data .= '"'.$value[$i]. '",';
				} else {
					$csv_data .= '"'.$value[$i]. '"';
				}
			}
			$csv_data .= "\n";
		}
		$csv_data = mb_convert_encoding($csv_data, "SJIS-win","UTF-8");
		$csv_file = "./data/file.csv";
		if( !file_exists($csv_file) ){
			touch( $csv_file );    		// ファイル作成
			chmod( $csv_file, 0666 );	// ファイルのパーティションの変更
		}
		$fp = fopen($csv_file, 'a');
		fwrite($fp, $csv_data);
		fclose($fp);
	}

}
