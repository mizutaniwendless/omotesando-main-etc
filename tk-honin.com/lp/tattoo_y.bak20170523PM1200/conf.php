<?php
global $formTool;
?>

<div id="section09" class="box">

  <article class="block13 cf">
    <h2><img src="img/title10.gif" width="1200" height="60" alt="無料カウンセリングご予約フォーム" class="sp_none mt100"><img src="img/title10_sp.gif" width="100%" alt="無料カウンセリングご予約フォーム" class="pc_none mt20"></h2>
    <p class="flow"><img src="img/flow_02.gif" alt="2．内容の確認" width="1000" height="80" /></p>
    <div class="form">
      <p class="caution">※入力内容をご確認の上、送信ボタンを押してください。</p>

      <form id="form_req" method="post">

        <table>
          <tr>
            <th class="hissu">お名前（ひらがな）</th>
            <td><?php echo $formTool->h($_POST['name_req']); ?></td>
          </tr>
          <tr>
            <th class="hissu">電話番号</th>
            <td><?php echo $formTool->h($_POST['tel_req']); ?></td>
          </tr>
          <tr>
            <th class="hissu">メールアドレス</th>
            <td><?php echo $formTool->h($_POST['email_req']); ?></td>
          </tr>
          <tr>
            <th class="nini">相談日 第一希望</th>
            <td><?php echo $formTool->h($_POST['day1']); ?> <?php echo $formTool->h($_POST['day1_period']); ?></td>
          </tr>
          <tr>
            <th class="nini">相談日 第二希望</th>
            <td><?php echo $formTool->h($_POST['day2']); ?> <?php echo $formTool->h($_POST['day2_period']); ?></td>
          </tr>
          <tr>
            <th class="nini">連絡方法のご希望</th>
            <td class="radiostyle"><?php if(is_array($_POST['type'])){ echo $formTool->h(implode(',', $_POST['type'] ) ); } ?></td>
          </tr>
          <tr>
            <th class="nini last">お問い合わせ内容</th>
            <td class="last"><?php echo nl2br($formTool->h($_POST['message'])); ?></td>
          </tr>
        </tbody>
      </table>

      <?php foreach($_POST as $key => $val) { ?>
      <?php if(is_array($val)) { ?>
      <?php foreach($val as $key2 => $val2) { ?>
      <?php echo "<input type=\"hidden\" id=\"form_{$key}[{$key2}]\" name=\"{$key}[{$key2}]\" value=\"{$val2}\">"; ?>
      <?php } ?>
      <?php } else { ?>
      <?php echo "<input type=\"hidden\" id=\"form_{$key}\" name=\"{$key}\" value=\"{$val}\">"; ?>
      <?php } ?>
      <?php } ?>
      <p class="btn">
        <input type="image" src="img/formbtn_02.gif" class="btn2" width="420" height="70" alt="入力画面に戻る" />
        <input type="image" src="img/formbtn_03.gif" class="btn1" width="420" height="70" alt="送信" />
      </p>

      </form>
      <script>
      $(function() {
      	$('.btn2').on('click', function() {
      		$('#form_mode').val('back');
      		$('#form_req').submit();
      	});
      	$('.btn1').on('click', function() {
      		$('#form_mode').val('comp');
      		$('#form_req').submit();
      	});
      });
      </script>

    </div>
  </article>

</div>
