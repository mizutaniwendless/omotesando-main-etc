<?php
global $formTool;
?>

	<div id="main_bg">
		<div id="main">
			<h1><img src="images/hyaluronan_title.png" width="297" height="191" alt="表参道スキンクリニックのヒアルロン酸注入は 丁寧な施術で張りのある自然な肌へ" class="sp_none"><img src="images/sp/hyaluronic_main.jpg" alt="表参道スキンクリニックのヒアルロン酸注入は 丁寧な施術で張りのある自然な肌へ" class="pc_none"></h1>
		</div>
	</div><!-- main !-->

	<div id="wrapper">
		<div id="contents01" class="inner">
			<div class="title_icon"><img src="images/hyaluronan_icon01.gif" width="20" height="20" alt=""></div>
			<h2 id="title01"><img src="images/hyaluronan_contents01_title.gif" width="664" height="138" alt="Quality 表参道スキンクリニックのヒアルロン酸注入の ～自然な肌の秘密とは～" class="sp_none"><img src="images/sp/hyaluronic_contents01_title.gif" alt="Quality 表参道スキンクリニックのヒアルロン酸注入の ～自然な肌の秘密とは～" class="pc_none"></h2>
			<div class="contents01_box cf">
				<h3><img src="images/hyaluronan_contents01_text01.gif" width="465" height="64" alt="POINT1.表参道スキンクリニックでは100%ピュア・ヒアルロン酸使用" class="sp_none"><img src="images/sp/hyaluronic_contents01_text01.gif" alt="POINT1.表参道スキンクリニックでは品質に不安のあるヒアルロン酸を使用しません" class="pc_none"></h3>
				<div class="left_textbox">
					<p><img src="images/sp/hyaluronic_contents01_img01.jpg" alt="" class="pc_none">ヒアルロン酸には粒子の大きさや濃度などによって、数多くの種類があります。<br>価格に違いが出るのは、当然たくさんあるヒアルロン酸の効果・成分の違いからです。</p>
					<p>「低価格」を売りにしているヒアルロン酸はどうでしょう？</p>
					<p>当然治療費も安くなりますが、そういった「低価格」を売りにしているヒアルロン酸は効果・成分共に、医療知識のある私どもでも《？》が付くものも少なくありません。</p>
					<p>当院では、お客様の安全・安心を最重視し、ピュア・ヒアルロン酸非動物性（バイオテクノロジーによって精製されたもの）しか使用しません。このヒアルロン酸は、狂牛病や鳥インフルエンザなど感染症の心配がありません。</p>
				</div>
				<div class="right_image"><img src="images/hyaluronan_contents01_img01.jpg" width="231" height="231" alt=""></div>
			</div>
			<div class="contents01_box cf">
				<h3><img src="images/hyaluronan_contents01_text02.gif" width="535" height="59" alt="POINT2.当院だけの丁寧な施術力" class="sp_none"><img src="images/sp/hyaluronic_contents01_text02.gif" alt="POINT2.お一人おひとりに合わせた丁寧な施術" class="pc_none" ></h3>
				<div class="right_textbox">
					<p><img src="images/sp/hyaluronic_contents01_img02.jpg" alt="" class="pc_none">当院は、お一人お一人のカウンセリングに時間をかけ、美容形成専門ドクターが施術を担当します。表参道スキンクリニックには日本形成学会認定専門医、及び美容皮膚科医が多数在籍しております。</p>
					<div class="detail_box_inner">
						<div class="detail_box cf">
							<div class="title"><img src="images/hyaluronan_contents01_text03.gif" width="201" height="43" alt="表参道スキンクリニック所属学会" class="sp_none"><img src="images/sp/hyaluronic_contents01_text03.gif" width="100%" alt="表参道スキンクリニック所属学会" class="pc_none"></div>
							<div class="text_list">
								<ul class="cf">
									<li class="left"><span>・</span>日本皮膚科学会正会員</li>
									<li><span>・</span>日本抗加齢学会会員</li>
									<li class="left"><span>・</span>レーザー医学会認定医</li>
									<li><span>・</span>美容外科学会会員</li>

								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="left_image"><img src="images/hyaluronan_contents01_img02.jpg" width="231" height="231" alt=""></div>
			</div>
			<div class="contents01_box">
				<h3><img src="images/hyaluronan_contents01_text04.gif" width="960" height="31" alt="気になるシワに、肌の状態に合わせた丁寧な施術でヒアルロン酸を注入" class="sp_none">
<!--<img src="images/sp/hyaluronic_contents01_text04.gif" alt="シワを改善する施術例" class="pc_none">-->
<img src="images/sp/hyaluronic_contents01_text05.gif" alt="気になるシワに、肌の状態に合わせた丁寧な施術でヒアルロン酸を注入" class="pc_none"></h3>
				<div class="contents_image"><img src="images/hyaluronan_contents01_img03.jpg" width="960" height="189" alt=""></div>
				<ul class="contents01_list cf">
					<li class="pr20">年齢とともにできてしまった刻まれたシワやお顔のくぼみに対し、ヒアルロン酸を注入します。</li>
					<li class="pr20">注入するヒアルロン酸はもともと人の体の中にある物質です。安全で肌組織の水分、潤滑性、柔軟性を保つ働きが期待できます。</li>
					<li>注入したヒアルロン酸はご自身のヒアルロン酸と融合して、ふっくらとしたハリとツヤのあるお肌へ導きます。</li>
				</ul>
			</div>
<?php
  $url = $_SERVER['REQUEST_URI'];
  if(strstr($url,'index_w')==false):
?>
			<div class="contents01_link_inner">
				<div class="contents01_link cf">
					<div class="left_image"><img src="images/hyaluronan_contents01_text05.gif" width="243" height="63" alt="刻まれたシワ向け ヒアルロン酸" class="sp_none"><img src="images/sp/hyaluronic_contents01_text06.gif" alt="刻まれたシワ向け ヒアルロン酸" class="pc_none"></div>
					<div class="btn">
						<a href="../botox/" class="sp_none"><img src="images/hyaluronan_contents01_btn_off.png" width="206" height="63" alt="表情シワ向け ボトックス"></a><a href="../botox/" class="pc_none"><img src="images/sp/hyaluronic_contents01_text07.gif" alt="表情シワ向け ボトックス"></a>
					</div>
				</div>
			</div>
<?php endif; ?>
		</div><!-- contents !-->

		<div class="conversion">
			<div class="sp_none"><a href="#section15"><img src="images/hyaluronan_cv_btn01_off.png" width="609" height="163" alt="無料カウンセリングのご予約はこちらから"></a></div>
			<div class="pc_none"><a href="#section15"><img src="images/sp/hyaluronic_cv_btn.jpg" alt="無料カウンセリングのご予約はこちらから"></a></div>
			<p class="tel"><img src="images/tk_cv_pc1.gif" alt="0120-334-270 受付時間 水・木・金・土　12：00～19:00　日・祝　10：00～17:30" height="87" width="609" class="sp_none"></p>
			<p class="tel pc_none"><a href="tel:0120334270" onclick="javascript:goog_report_conversion('tel:0120-334-270');yahoo_report_conversion(undefined);return false;"><img src="images/tk_cv_sp1.gif" alt="0120-334-270 受付時間 水・木・金・土　12：00～19:00　日・祝　10：00～17:30"></a></p>
			<!--<p class="tel pc_none"><a href="tel:0120931911" onclick="yahoo_report_conversion('tel:0120-931-911')"><img src="images/tk_cv_sp2.gif" alt="0120-334-270 受付時間 水・木・金・土　12：00～19:00　日・祝　10：00～17:30"></a></p>-->
		</div>

		<div id="contents02" class="contents02">
			<div class="title_icon"><img src="images/hyaluronan_icon02.gif" width="20" height="20" alt=""></div>
			<h2 id="title02"><img src="images/hyaluronan_contents02_title.gif" width="432" height="134" alt="Beauty ～ヒアルロン酸は女性の美しさを輝かせます～" class="sp_none"><img src="images/sp/hyaluronic_contents02_title.gif" alt="Beauty ～ヒアルロン酸は女性の美しさを輝かせます～" class="pc_none"></h2>
			<h3><img src="images/hyaluronan_contents02_text01.gif" width="1200" height="47" alt="ヒアルロン酸で期待できる効果" class="sp_none"><img src="images/sp/hyaluronic_contents02_text01.gif" alt="ヒアルロン酸で期待できる効果" class="pc_none"></h3>
			<ul class="contents02_list cf">
				<li class="pr2">
					<div><img src="images/hyaluronan_contents02_img01.jpg" width="398" height="325" alt="シルエットを整える 気になる部分をヒアルロン酸で理想的なシルエットに整えます。" class="sp_none"><img src="images/sp/hyaluronic_contents02_img01.jpg" width="100%" alt="シルエットを整える 気になる部分をヒアルロン酸で理想的なシルエットに整えます。" class="pc_none"></div>
				</li>
				<li class="pr2">
					<div><img src="images/hyaluronan_contents02_img02.jpg" width="398" height="325" alt="シワの改善 加齢変化によってできたシワや凹みに注入し、若々しい肌を取り戻します" class="sp_none"><img src="images/sp/hyaluronic_contents02_img02.jpg" width="100%" alt="シワの改善 加齢変化によってできたシワや凹みに注入し、若々しい肌を取り戻します" class="pc_none"></div>
				</li>
				<li>
					<div><img src="images/hyaluronan_contents02_img03.jpg" width="400" height="325" alt="女性らしさの形成 女性らしいボディラインやお顔のパーツをヒアルロン酸で自然な形で再現します。" class="sp_none"><img src="images/sp/hyaluronic_contents02_img03.jpg" width="100%" alt="女性らしさの形成 女性らしいボディラインやお顔のパーツをヒアルロン酸で自然な形で再現します。" class="pc_none"></div>
				</li>
			</ul>
			<div><img src="images/hyaluronan_contents02_img04.jpg" width="1174" height="564" alt="誰にも気づかれずに若返りたい 理想的なラインを手に入れたい 目立ち始めたほうれい線を気軽に解消したい" class="sp_none"><img src="images/sp/hyaluronic_contents02_img04.jpg" width="100%" alt="誰にも気づかれずに若返りたい 理想的なラインを手に入れたい 目立ち始めたほうれい線を気軽に解消したい" class="pc_none"></div>
			<h4><img src="images/hyaluronan_contents02_text02.gif" width="1039" height="23" alt="ヒアルロン酸は人間の皮膚や関節、眼球などにゼリー状の成分としてもともと存在しています。" class="sp_none"><img src="images/sp/hyaluronic_contents02_text02.gif" width="100%" alt="ヒアルロン酸は人間の皮膚や関節、眼球などにゼリー状の成分としてもともと存在しています。" class="pc_none"></h4>
			<p class="sp_none"><img src="images/hyaluronan_contents02_text03.gif" width="963" height="255" alt="ヒアルロン酸には水分が沢山含まれていて、肌のハリや弾力を取り戻すための役目や、潤いを補うという役割を持っています。しかし、年齢とともにだんだん体で作られる量が減ってしまいます。そうすると、水分が不足してしまい肌は乾燥してしまいます。肌が乾燥すると、しわやしみの原因にもなります。ヒアルロン酸をシワの凹部分の下に注入することにより、自然な形にふっくらと盛り上がります。ボットクス注射が良いかヒアルロン酸が最適かはカウンセリング後に最適なご提案をしていますのでご安心ください。"></p>
			<p class="pc_none"><img src="images/sp/hyaluronic_contents02_text03.gif" width="100%" alt="ヒアルロン酸には水分が沢山含まれていて、肌のハリや弾力を取り戻すための役目や、潤いを補うという役割を持っています。"><img src="images/sp/hyaluronic_contents02_text04.gif" width="100%" alt="しかし、年齢とともにだんだ体で作られる量が減ってしまいます。そうすると、水分が不足してしまい肌は乾燥してしまいます。"><img src="images/sp/hyaluronic_contents02_text05.gif" width="100%" alt="肌が乾燥すると、しわやしみの原因にもなります。ヒアルロン酸をシワの凹部分の下に注入することにより、自然な形にふっくらと盛り上がります。ボットクス注射が良いかヒアルロン酸が最適かはカウンセリング後に最適なご提案をしていますのでご安心ください。"></p>
		</div>

		<div class="conversion">
			<div class="sp_none"><a href="#section15"><img src="images/hyaluronan_cv_btn01_off.png" width="609" height="163" alt="無料カウンセリングのご予約はこちらから"></a></div>
			<div class="pc_none"><a href="#section15"><img src="images/sp/hyaluronic_cv_btn.jpg" alt="無料カウンセリングのご予約はこちらから"></a></div>
			<p class="tel"><img src="images/tk_cv_pc1.gif" alt="0120-334-270 受付時間 水・木・金・土　12：00～19:00　日・祝　10：00～17:30" height="87" width="609" class="sp_none"></p>
			<p class="tel pc_none"><a href="tel:0120334270" onclick="javascript:goog_report_conversion('tel:0120-334-270');yahoo_report_conversion(undefined);return false;"><img src="images/tk_cv_sp1.gif" alt="0120-334-270 受付時間 水・木・金・土　12：00～19:00　日・祝　10：00～17:30"></a></p>
			<!--<p class="tel pc_none"><a href="tel:0120931911" onclick="yahoo_report_conversion('tel:0120-931-911')"><img src="images/tk_cv_sp2.gif" alt="0120-334-270 受付時間 水・木・金・土　12：00～19:00　日・祝　10：00～17:30"></a></p>-->
		</div>

		<div id="priceArea">
      <div class="title_icon"><img src="images/hyaluronan_icon02.gif" width="20" height="20" alt=""></div>
      <h2 class="title06"><img src="images/hyaluronan_contents06_title.gif" width="252" height="121" alt="Price ～ヒアルロン酸注入料金～" class="sp_none"><img src="images/sp/hyaluronic_contents03_title.gif" width="100%" alt="Price ～ヒアルロン酸注入料金～" class="pc_none"></h2>
      <ul class="price_list cf">
        <li class="pb55"><img src="images/hyaluronan_contents06_img01.jpg" width="360" height="160" alt="眼瞼周囲" class="sp_none"><img src="images/sp/hyaluronic_contents03_img01.jpg" width="100%" alt="眼瞼周囲" class="pc_none"></li>
        <li class="pb55"><img src="images/hyaluronan_contents06_img02.jpg" width="360" height="160" alt="法令線・口唇" class="sp_none"><img src="images/sp/hyaluronic_contents03_img02.jpg" width="100%" alt="法令線・口唇" class="pc_none"></li>
        <li class="last pb55"><img src="images/hyaluronan_contents06_img03.jpg" width="360" height="160" alt="顎・隆鼻" class="sp_none"><img src="images/sp/hyaluronic_contents03_img03.jpg" width="100%" alt="顎・隆鼻" class="pc_none"></li>
        <li><img src="images/hyaluronan_contents06_img04.jpg" width="360" height="160" alt="バスト・ヒップ" class="sp_none"><img src="images/sp/hyaluronic_contents03_img04.jpg" width="100%" alt="バスト・ヒップ" class="pc_none"></li>
        <li><img src="images/hyaluronan_contents06_img05.jpg" width="360" height="160" alt="涙袋形成" class="sp_none"><img src="images/sp/hyaluronic_contents03_img05.jpg" width="100%" alt="涙袋形成" class="pc_none"></li>
        <li class="last"><img src="images/hyaluronan_contents06_img06.jpg" width="360" height="160" alt="鼻筋・眉間" class="sp_none"><img src="images/sp/hyaluronic_contents03_img06.jpg" width="100%" alt="鼻筋・眉間" class="pc_none"></li>
      </ul>
      <div class="campaign_price"><img src="images/hyaluronan_contents06_text01_wy.gif" width="689" height="84" alt="初回キャンペーン価格 0.1cc 4,800（+税）～" class="sp_none"><img src="images/sp/hyaluronic_contents03_text01.gif" width="100%" alt="初回キャンペーン価格 0.1cc 4,800（+税）～" class="pc_none"></div>
      <div class="campaign_price02">
      <!--<img src="images/hyaluronan_contents06_text02.jpg" width="840" height="84" alt="ほうれい線、目の下、隆鼻、顎形成 1cc 24,000（+税）～" class="sp_none">
      <img src="images/sp/hyaluronic_contents03_text02.jpg" width="100%" alt="ほうれい線、目の下、隆鼻、顎形成 1cc 24,000（+税）～" class="pc_none">-->
      </div>

      <p class="price_text">※ご希望の仕上がり、施術範囲、シワの深さなどによりヒアルロン酸の注入量が異なります。<br class="sp_none">その際はあらかじめ担当医師よりご説明をさせていただきます。<br>※2回目以降の施術を半額でお受けいただけるプランもございます。</p>
		</div>

		<div class="conversion">
			<div class="sp_none"><a href="#section15"><img src="images/hyaluronan_cv_btn01_off.png" width="609" height="163" alt="無料カウンセリングのご予約はこちらから"></a></div>
			<div class="pc_none"><a href="#section15"><img src="images/sp/hyaluronic_cv_btn.jpg" alt="無料カウンセリングのご予約はこちらから"></a></div>
			<p class="tel"><img src="images/tk_cv_pc1.gif" alt="0120-334-270 受付時間 水・木・金・土　12：00～19:00　日・祝　10：00～17:30" height="87" width="609" class="sp_none"></p>
			<p class="tel pc_none"><a href="tel:0120334270" onclick="javascript:goog_report_conversion('tel:0120-334-270');yahoo_report_conversion(undefined);return false;"><img src="images/tk_cv_sp1.gif" alt="0120-334-270 受付時間 水・木・金・土　12：00～19:00　日・祝　10：00～17:30"></a></p>
			<!--<p class="tel pc_none"><a href="tel:0120931911" onclick="yahoo_report_conversion('tel:0120-931-911')"><img src="images/tk_cv_sp2.gif" alt="0120-334-270 受付時間 水・木・金・土　12：00～19:00　日・祝　10：00～17:30"></a></p>-->
		</div>

		<div id="contents03" class="inner">
			<div class="contents03">
				<div class="title_icon"><img src="images/hyaluronan_icon01.gif" width="20" height="20" alt=""></div>
				<h2 id="title03"><img src="images/hyaluronan_contents03_title.gif" width="366" height="133" alt="Operation ～お問合わせから施術までの流れ～" class="sp_none"><img src="images/sp/hyaluronic_contents04_title.gif" width="100%" alt="Operation ～お問合わせから施術までの流れ～" class="pc_none"></h2>
				<div class="contents03_box cf">
					<div class="left_image">
						<img src="images/hyaluronan_contents03_img01.jpg" width="334" height="205" alt="STEP1" class="sp_none"><img src="images/sp/hyaluronic_contents04_step01.gif" width="18%" alt="STEP1" class="pc_none">
					</div>
					<div class="right_text">
						<h3><img src="images/hyaluronan_contents03_text01.gif" width="596" height="40" alt="お電話、メールにてお問い合わせ" class="sp_none"><img src="images/sp/hyaluronic_contents04_text01.gif" width="100%" alt="お電話、メールにてお問い合わせ" class="pc_none"></h3>
						<p><img src="images/sp/hyaluronic_contents04_img01.jpg" width="40%" alt="" class="image_left pc_none">電話、もしくはメールでご予約を承っております。<br class="pc_none">施術のご予約の前に、診療や施術、その他について疑問がありましたら、お電話またはお問い合わせフォーム、メールからお気軽にお問い合わせください。その際には匿名でのお問合せもOKです。</p>
						<div class="contact cf">
							<div class="tel"><img src="images/tk_cv_pc2.gif" width="208" height="106" alt="0120-334-270 受付時間 水・木・金・土　12：00～19:00　日　10：00～17:30" class="sp_none"><a href="tel:0120334270" onclick="javascript:goog_report_conversion('tel:0120-334-270');yahoo_report_conversion(undefined);return false;"><img src="images/tk_cv_sp1.gif" width="100%" alt="0120-334-270 受付時間 水・木・金・土　12：00～19:00　日　10：00～17:30" class="pc_none"></a>
<!--<a href="tel:0120931911" onclick="yahoo_report_conversion('tel:0120-931-911')"><img src="images/tk_cv_sp2.gif" width="100%" alt="0120-334-270 受付時間 水・木・金・土　12：00～19:00　日　10：00～17:30" class="pc_none"></a>--></div>
							<div class="btn"><a href="#section15" class="sp_none"><img src="images/hyaluronan_contents03_btn_off.png" width="325" height="39" alt="無料カウンセリングのご予約はこちらから"></a><a href="#section15" class="pc_none"><img src="images/sp/hyaluronic_contents04_btn.gif" width="100%" alt="無料カウンセリングのご予約はこちらから"></a></div>
						</div>
					</div>
				</div>
				<div class="contents03_box cf">
					<div class="left_image">
						<img src="images/hyaluronan_contents03_img02.jpg" width="334" height="205" alt="STEP2" class="sp_none"><img src="images/sp/hyaluronic_contents04_step02.gif" width="18%" alt="STEP2" class="pc_none">
					</div>
					<div class="right_text">
						<h3><img src="images/hyaluronan_contents03_text02.gif" width="596" height="40" alt="ご来院・受付" class="sp_none"><img src="images/sp/hyaluronic_contents04_text02.gif" width="100%" alt="ご来院・受付" class="pc_none"></h3>
						<p><img src="images/sp/hyaluronic_contents04_img02.jpg" width="40%" alt="" class="image_right pc_none">お電話でご予約いただいた日程でご来院ください。<br class="sp_none">
						（日程の変更などはお気軽にご連絡ください）<br class="sp_none">
						ご予約された日時にお越し下さい。<br>
						表参道スキンクリニックでのカウンセリング方法など事前に何でも相談ください。</p>

					</div>
				</div>
				<div class="contents03_box cf">
					<div class="left_image">
						<img src="images/hyaluronan_contents03_img03.jpg" width="334" height="205" alt="STEP3" class="sp_none"><img src="images/sp/hyaluronic_contents04_step03.gif" width="18%" alt="STEP3" class="pc_none">
					</div>
					<div class="right_text">
						<h3><img src="images/hyaluronan_contents03_text03.gif" width="596" height="40" alt="スタッフとのカウンセリング" class="sp_none"><img src="images/sp/hyaluronic_contents04_text03.gif" width="100%" alt="スタッフとのカウンセリング" class="pc_none"></h3>
						<p><img src="images/sp/hyaluronic_contents04_img03.jpg" width="40%" alt="" class="image_left pc_none">お客様、患者様の悩みや疑問、施術費用やお支払方法ついてなど、専門のスタッフにお気軽に相談ください。</p>
						<div class="textbox">
							<div class="text">
								<p>施術へのお手続き・施術申込書の記入、術後の注意事項をお渡しします。<br>未成年の方は同意書が必要になります。又はご両親のどちらかご同伴でお願いします。<br>※認印（シャチハタ以外）をお持ち下さい。</p>
							</div>
						</div>
					</div>
				</div>
				<div class="contents03_box cf">
					<div class="left_image">
						<img src="images/hyaluronan_contents03_img04.jpg" width="334" height="205" alt="STEP4" class="sp_none"><img src="images/sp/hyaluronic_contents04_step04.gif" width="18%" alt="STEP4" class="pc_none">
					</div>
					<div class="right_text">
						<h3><img src="images/hyaluronan_contents03_text04.gif" width="596" height="40" alt="担当医師とのカウンセリング・診察" class="sp_none"><img src="images/sp/hyaluronic_contents04_text04.gif" width="100%" alt="担当医師とのカウンセリング・診察 " class="pc_none"></h3>
						<p><img src="images/sp/hyaluronic_contents04_img04.jpg" width="40%" alt="" class="image_right pc_none">実際に施術を行う担当医師が丁寧にご要望をうかがった上で、プラン、施術方法、施術内容のご説明をいたします。</p>
						<p class="detail">※施術内容の詳細は担当医師がご説明いたします。ご不明な点があればお気軽にご相談ください。</p>
					</div>
				</div>
				<div class="contents03_box cf">
					<div class="left_image">
						<img src="images/hyaluronan_contents03_img05.jpg" width="334" height="205" alt="STEP5" class="sp_none"><img src="images/sp/hyaluronic_contents04_step05.gif" width="18%" alt="STEP5" class="pc_none">
					</div>
					<div class="right_text">
						<h3><img src="images/hyaluronan_contents03_text05.gif" width="596" height="40" alt="施術を行います" class="sp_none"><img src="images/sp/hyaluronic_contents04_text05.gif" width="100%" alt="施術を行います" class="pc_none"></h3>
						<p><img src="images/sp/hyaluronic_contents04_img05.jpg" width="40%" alt="" class="image_left pc_none">お顔の施術の場合は、事前にメイクを落としていただく必要がございます。<br class="sp_none">
						化粧落としなどご用意しておりますので、お気軽にスタッフにお声掛けください。<br class="sp_none">
						準備ができましたら施術室に移動していただき、施術を行います。<br class="sp_none">リラックスして施術を受けてください。</p>
					</div>
				</div>
				<div class="contents03_box cf">
					<div class="left_image">
						<img src="images/hyaluronan_contents03_img06.jpg" width="334" height="205" alt="STEP6" class="sp_none"><img src="images/sp/hyaluronic_contents04_step06.gif" width="18%" alt="STEP6" class="pc_none">
					</div>
					<div class="right_text">
						<h3><img src="images/hyaluronan_contents03_text06.gif" width="596" height="40" alt="アフターケア" class="sp_none"><img src="images/sp/hyaluronic_contents04_text06.gif" width="100%" alt="アフターケア" class="pc_none"></h3>
						<p><img src="images/sp/hyaluronic_contents04_img06.jpg" width="40%" alt="" class="image_right pc_none">ヒアルロン酸注入の場合ほとんどは終了後すぐにご帰宅いただけます。<br class="sp_none">
						専用のメイクルームをご用意いたしておりますので、メイク直しをしながら安心して新しいご自分と対面していただけます。<br>

						施術後のアフターケアの料金は手術料金に含まれております。 <br>
						また、施術後しばらく経ってご不安な点や不明瞭な点などがございましたら、お気軽に電話、またはメールにて直接ご相談ください。</p>
						<p class="detail">ヒアルロン酸の溶解（ヒロラーゼ、ヒアルロニターゼ）についてもご相談を承っております。<br>※別途費用</p>
					</div>
				</div>
				<div class="contents03_text"><img src="images/hyaluronan_contents03_text07.gif" width="752" height="125" alt=" 表参道スキンクリニックは、どこまでも綺麗への想いにお応えし続けます" class="sp_none" ><img src="images/sp/hyaluronic_contents04_text07.gif" width="100%" alt="表参道スキンクリニックは、どこまでも綺麗への想いにお応えし続けます" class="pc_none" ></div>
			</div><!-- contents03 !-->
		</div><!-- inner !-->

		<div class="conversion02">
			<div class="sp_none"><a href="#section15"><img src="images/hyaluronan_cv_btn01_off.png" width="609" height="163" alt="無料カウンセリングのご予約はこちらから"></a></div>
			<div class="pc_none"><a href="#section15"><img src="images/sp/hyaluronic_cv_btn.jpg" alt="無料カウンセリングのご予約はこちらから"></a></div>
			<p class="tel"><img src="images/tk_cv_pc1.gif" alt="0120-334-270 受付時間 水・木・金・土　12：00～19:00　日・祝　10：00～17:30" height="87" width="609" class="sp_none"></p>
			<p class="tel pc_none"><a href="tel:0120334270" onclick="javascript:goog_report_conversion('tel:0120-334-270');yahoo_report_conversion(undefined);return false;"><img src="images/tk_cv_sp1.gif" alt="0120-334-270 受付時間 水・木・金・土　12：00～19:00　日・祝　10：00～17:30"></a></p>
			<!--<p class="tel pc_none"><a href="tel:0120931911" onclick="yahoo_report_conversion('tel:0120-931-911')"><img src="images/tk_cv_sp2.gif" alt="0120-334-270 受付時間 水・木・金・土　12：00～19:00　日・祝　10：00～17:30"></a></p>-->
		</div>


		<!--
		<div id="sliderArea">
      <div class="title_icon"><img src="images/hyaluronan_icon02.gif" width="20" height="20" alt=""></div>
      <h2 id="title04"><img src="images/hyaluronan_contents04_title.gif" width="481" height="133" alt="Luxury space ～当院を選ばれた女性に大変好評な院内空間～" class="sp_none"><img src="images/sp/hyaluronic_contents05_title.gif" width="100%" alt="Luxury space ～当院を選ばれた女性に大変好評な院内空間～" class="pc_none"></h2>
      <div class="bg_img">
        <ul id="slider" class="slider">
          <li class="slide1"><a href="images/hyaluronan_contents04_img07_big.jpg" rel="lightbox" class="sp_none"><img src="images/hyaluronan_contents04_img07.jpg" width="600" height="426" alt="診察室"></a><img src="images/sp/hyaluronic_contents05_main07.jpg" alt="診察室" width="100%" class="pc_none"></li>
          <li class="slide2"><a href="images/hyaluronan_contents04_img08_big.jpg" rel="lightbox" class="sp_none"><img src="images/hyaluronan_contents04_img08.jpg" width="600" height="426" alt="施術室"></a><img src="images/sp/hyaluronic_contents05_main08.jpg" alt="施術室" width="100%" class="pc_none"></li>
          <li class="slide3"><a href="images/hyaluronan_contents04_img09_big.jpg" rel="lightbox" class="sp_none"><img src="images/hyaluronan_contents04_img09.jpg" width="600" height="426" alt="受付"></a><img src="images/sp/hyaluronic_contents05_main09.jpg" alt="受付" width="100%" class="pc_none"></li>
          <li class="slide4"><a href="images/hyaluronan_contents04_img10_big.jpg" rel="lightbox" class="sp_none"><img src="images/hyaluronan_contents04_img10.jpg" width="600" height="426" alt="待合室"></a><img src="images/sp/hyaluronic_contents05_main10.jpg" alt="待合室" width="100%" class="pc_none"></li>
          <li class="slide5"><a href="images/hyaluronan_contents04_img11.jpg" rel="lightbox" class="sp_none"><img src="images/hyaluronan_contents04_img11.jpg" width="600" height="426" alt="化粧室" ></a><img src="images/sp/hyaluronic_contents05_main11.jpg" alt="化粧室" width="100%" class="pc_none"></li>
          <li class="slide6"><a href="images/hyaluronan_contents04_img12_big.jpg" rel="lightbox"><img src="images/hyaluronan_contents04_img12.jpg" width="600" height="426" alt="エントランス"></a><img src="images/sp/hyaluronic_contents05_main12.jpg" alt="エントランス" width="100%" class="pc_none"></li>
        </ul>
      </div>
      <div class="contents04_text"><img src="images/hyaluronan_contents04_text01.gif" width="1200" height="61" alt="表参道スキンクリニックでは皆様のプライベートを大切に考え、院内はいつも安心して来院していただけますように清潔で明るいクリニックを保っております。" class="sp_none"><img src="images/sp/hyaluronic_contents05_text01.gif" width="100%" alt="表参道スキンクリニックでは皆様のプライベートを大切に考え、院内はいつも安心して来院していただけますように清潔で明るいクリニックを保っております。" class="pc_none"></div>
		</div>

		<div class="conversion">
			<div class="sp_none"><a href="#section15"><img src="images/hyaluronan_cv_btn01_off.png" width="609" height="163" alt="無料カウンセリングのご予約はこちらから"></a></div>
			<div class="pc_none"><a href="#section15"><img src="images/sp/hyaluronic_cv_btn.jpg" alt="無料カウンセリングのご予約はこちらから"></a></div>
			<p class="tel"><img src="images/tk_cv_pc1.gif" alt="0120-334-270 受付時間 水・木・金・土　12：00～19:00　日・祝　10：00～17:30" height="87" width="609" class="sp_none"><a href="tel:0120-334-270" onclick="yahoo_report_conversion(undefined); goog_report_conversion('tel:0120-334-270');return false;" class="pc_none"><img src="images/sp/hyaluronic_cv_tel.gif" alt="0120-334-270 受付時間 水・木・金・土　12：00～19:00　日・祝　10：00～17:30"></a></p>
		</div>
		-->
		<div id="contents05" class="inner">
			<div class="contents05">
				<div class="title_icon"><img src="images/hyaluronan_icon01.gif" width="20" height="20" alt=""></div>
				<h2 id="title05"><img src="images/hyaluronan_contents05_title.gif" width="271" height="130" alt="FAQ ～よくいただくご質問～" class="sp_none" ><img src="images/sp/hyaluronic_contents06_title.gif" width="100%" alt="FAQ ～よくいただくご質問～" class="pc_none" ></h2>
				<div class="faq_contents">
					<h3><img src="images/hyaluronan_contents05_text01.gif" width="368" height="42" alt="Q1「ヒアルロン酸は安全ですか？」" class="sp_none"><span class="pc_none"><img src="images/sp/hyaluronic_contents06_q01.gif" width="100%" alt="Q1"><img src="images/sp/hyaluronic_contents06_text01.gif" width="100%" alt"「ヒアルロン酸は安全ですか？」"=""></span></h3>
					<div class="answer_box cf">
						<div class="answer"><img src="images/hyaluronan_contents05_answer.gif" width="28" height="28" alt="A" class="sp_none"><img src="images/sp/hyaluronic_contents06_answer.gif" width="100%" alt="A" class="pc_none"></div>
						<div class="answer_text">
							<p>ヒアルロン酸はもともと体内にある物質のため、副作用の心配もほとんどありません。</p>
						</div>
					</div>
				</div>
				<div class="faq_contents">
					<h3><img src="images/hyaluronan_contents05_text02.gif" width="524" height="42" alt="Q2「ヒアルロン酸でどのような治療ができますか？」" class="sp_none"><span class="pc_none"><img src="images/sp/hyaluronic_contents06_q02.gif" width="100%" alt="Q2"><img src="images/sp/hyaluronic_contents06_text02.gif" width="100%" alt"「ヒアルロン酸でどのような治療ができますか？」"=""></span></h3>
					<div class="answer_box cf">
						<div class="answer"><img src="images/hyaluronan_contents05_answer.gif" width="28" height="28" alt="A" class="sp_none"><img src="images/sp/hyaluronic_contents06_answer.gif" width="100%" alt="A" class="pc_none"></div>
						<div class="answer_text">
							<p>ヒアルロン酸では以下のような治療ができます。<br>
							目下のクマ・タルミ、ほうれい線、マリオネットライン（口元からアゴにかけてのしわ）、額・目尻・眉間の深いしわ、<br>
							こめかみや頬の凹み、鼻を高くする、唇をふっくらさせる、アゴを出す、豊胸など。<br>
							この他にもヒアルロン酸治療適応箇所がございますので詳しくはご相談ください。</p>
						</div>
					</div>
				</div>
				<div class="faq_contents">
					<h3><img src="images/hyaluronan_contents05_text03.gif" width="464" height="42" alt="Q3「治療にかかる時間はどれくらいですか？」" class="sp_none"><span class="pc_none"><img src="images/sp/hyaluronic_contents06_q03.gif" width="100%" alt="Q3"><img src="images/sp/hyaluronic_contents06_text03.gif" width="100%" alt"「治療にかかる時間はどれくらいですか？」"=""></span></h3>
					<div class="answer_box cf">
						<div class="answer"><img src="images/hyaluronan_contents05_answer.gif" width="28" height="28" alt="A" class="sp_none"><img src="images/sp/hyaluronic_contents06_answer.gif" width="100%" alt="A" class="pc_none"></div>
						<div class="answer_text">
							<p>注入の範囲にもよりますが5～30分ほどです。</p>
						</div>
					</div>
				</div>
				<div class="faq_contents">
					<h3><img src="images/hyaluronan_contents05_text04.gif" width="385" height="42" alt="Q4「すぐに治療を受けられますか？」" class="sp_none"><span class="pc_none"><img src="images/sp/hyaluronic_contents06_q04.gif" width="100%" alt="Q4"><img src="images/sp/hyaluronic_contents06_text04.gif" width="100%" alt"「すぐに治療を受けられますか？」"=""></span></h3>
					<div class="answer_box cf">
						<div class="answer"><img src="images/hyaluronan_contents05_answer.gif" width="28" height="28" alt="A" class="sp_none"><img src="images/sp/hyaluronic_contents06_answer.gif" width="100%" alt="A" class="pc_none"></div>
						<div class="answer_text">
							<p>人間の持っているヒアルロン酸と同じ天然物質から作られたヒアルロン酸を使用しますので、事前検査は必要ありません。</p>
						</div>
					</div>
				</div>
				<div class="faq_contents">
					<h3><img src="images/hyaluronan_contents05_text05.gif" width="351" height="42" alt="Q5「施術時の痛みが心配です。」" class="sp_none"><span class="pc_none"><img src="images/sp/hyaluronic_contents06_q05.gif" width="100%" alt="Q5"><img src="images/sp/hyaluronic_contents06_text05.gif" width="100%" alt"「施術時の痛みが心配です。」"=""></span></h3>
					<div class="answer_box cf">
						<div class="answer"><img src="images/hyaluronan_contents05_answer.gif" width="28" height="28" alt="A" class="sp_none"><img src="images/sp/hyaluronic_contents06_answer.gif" width="100%" alt="A" class="pc_none"></div>
						<div class="answer_text">
							<p>注射による施術のため、ある程度の痛みはあります。<br>
							当美容皮膚科ではなるべく痛みを抑えるために、局所麻酔を塗って、極細の針で注射します。<br>
							痛みに弱い方にも安心して治療をお受け頂けます。</p>
						</div>
					</div>
				</div>
				<div class="faq_contents">
					<h3><img src="images/hyaluronan_contents05_text06.gif" width="484" height="42" alt="Q6「ヒアルロン酸の効果はどれ位もちますか？」" class="sp_none"><span class="pc_none"><img src="images/sp/hyaluronic_contents06_q06.gif" width="100%" alt="Q6"><img src="images/sp/hyaluronic_contents06_text06.gif" width="100%" alt"「ヒアルロン酸の効果はどれ位もちますか？」"=""></span></h3>
					<div class="answer_box cf">
						<div class="answer"><img src="images/hyaluronan_contents05_answer.gif" width="28" height="28" alt="A" class="sp_none"><img src="images/sp/hyaluronic_contents06_answer.gif" width="100%" alt="A" class="pc_none"></div>
						<div class="answer_text">
							<p>注射用ヒアルロン酸は約1年間、効果の持続が期待できます。（製剤の種類や注入技術、個体差で差があります）<br>
							ヒアルロン酸は完全に分解される物質ですので、体内に残るということはありません。<br>
							ヒアルロン酸の吸収度合いにより、年に１回程度の注入が可能です。</p>
						</div>
					</div>
				</div>
				<div class="faq_contents">
					<h3><img src="images/hyaluronan_contents05_text07.gif" width="392" height="42" alt="Q7「施術後の副作用はありますか？」" class="sp_none"><span class="pc_none"><img src="images/sp/hyaluronic_contents06_q07.gif" width="100%" alt="Q7"><img src="images/sp/hyaluronic_contents06_text07.gif" width="100%" alt"「施術後の副作用はありますか？」"=""></span></h3>
					<div class="answer_box cf">
						<div class="answer"><img src="images/hyaluronan_contents05_answer.gif" width="28" height="28" alt="A" class="sp_none"><img src="images/sp/hyaluronic_contents06_answer.gif" width="100%" alt="A" class="pc_none"></div>
						<div class="answer_text">
							<p>ヒアルロン酸は針を使用するために内出血を起こす可能性があります。<br>
							内出血が起きやすい体質の方もいらっしゃいますので、念のために1回目は1～2週間程度、<br>
							大切な御用のない時を選んでお越し下さい。</p>
						</div>
					</div>
				</div>
				<div class="faq_contents">
					<h3><img src="images/hyaluronan_contents05_text08.gif" width="465" height="42" alt="Q8「治療後に気を付けることはありますか？」" class="sp_none"><span class="pc_none"><img src="images/sp/hyaluronic_contents06_q08.gif" width="100%" alt="Q8"><img src="images/sp/hyaluronic_contents06_text08.gif" width="100%" alt"「治療後に気を付けることはありますか？」"=""></span></h3>
					<div class="answer_box cf">
						<div class="answer"><img src="images/hyaluronan_contents05_answer.gif" width="28" height="28" alt="A" class="sp_none"><img src="images/sp/hyaluronic_contents06_answer.gif" width="100%" alt="A" class="pc_none"></div>
						<div class="answer_text">
							<p>ヒアルロン酸を注射した後は、通常の日常生活を送ることが出来ます。<br>
							お化粧も治療後すぐにでき、入浴、運動、飲酒も問題ありません。<br>
							ただし、感染予防のため、注入部位を必要以上に触らないように気を付けて下さい。<br>
							初めての方は、違和感を感じることがありますが、数日でなじみます。</p>
						</div>
					</div>
				</div>
				<div class="faq_contents">
					<h3><img src="images/hyaluronan_contents05_text09.gif" width="655" height="42" alt="Q9「ヒアルロン酸を入れ過ぎてしまったらどうしたらいいですか？」" class="sp_none"><span class="pc_none"><img src="images/sp/hyaluronic_contents06_q09.gif" width="100%" alt="Q9"><img src="images/sp/hyaluronic_contents06_text09.gif" width="100%" alt"「ヒアルロン酸を入れ過ぎてしまったらどうしたらいいですか？」"=""></span></h3>
					<div class="answer_box cf">
						<div class="answer"><img src="images/hyaluronan_contents05_answer.gif" width="28" height="28" alt="A" class="sp_none"><img src="images/sp/hyaluronic_contents06_answer.gif" width="100%" alt="A" class="pc_none"></div>
						<div class="answer_text">
							<p>入り過ぎてしまったり凸凹が残ってしまったヒアルロン酸は分解することができます。<br>
							他院でヒアルロン酸を入れ過ぎてしまった方、デコボコ感が気になる方、ぜひお気軽にご相談下さい。</p>
						</div>
					</div>
				</div>
				<div class="faq_contents">
					<h3><img src="images/hyaluronan_contents05_text10.gif" width="489" height="42" alt="Q10「持病があっても大丈夫ですか？禁忌は？」" class="sp_none"><span class="pc_none"><img src="images/sp/hyaluronic_contents06_q10.gif" width="100%" alt="Q10"><img src="images/sp/hyaluronic_contents06_text10.gif" width="100%" alt"「持病があっても大丈夫ですか？禁忌は？」"=""></span></h3>
					<div class="answer_box cf">
						<div class="answer"><img src="images/hyaluronan_contents05_answer.gif" width="28" height="28" alt="A" class="sp_none"><img src="images/sp/hyaluronic_contents06_answer.gif" width="100%" alt="A" class="pc_none"></div>
						<div class="answer_text">
							<p><span>・</span>糖尿病・膠原病の方　 <span>・</span>妊娠されている方・授乳中の方　 <span>・</span>注入箇所周辺が化膿している方　 <span>・</span>ケロイド体質の方<br>などはお控えください。</p>
						</div>
					</div>
				</div>
			</div><!-- contents05 !-->
		</div><!-- inner !-->

		<div class="conversion">
			<div class="sp_none"><a href="#section15"><img src="images/hyaluronan_cv_btn01_off.png" width="609" height="163" alt="無料カウンセリングのご予約はこちらから"></a></div>
			<div class="pc_none"><a href="#section15"><img src="images/sp/hyaluronic_cv_btn.jpg" alt="無料カウンセリングのご予約はこちらから"></a></div>
			<p class="tel"><img src="images/tk_cv_pc1.gif" alt="0120-334-270 受付時間 水・木・金・土　12：00～19:00　日・祝　10：00～17:30" height="87" width="609" class="sp_none"></p>
			<p class="tel pc_none"><a href="tel:0120334270" onclick="javascript:goog_report_conversion('tel:0120-334-270');yahoo_report_conversion(undefined);return false;"><img src="images/tk_cv_sp1.gif" alt="0120-334-270 受付時間 水・木・金・土　12：00～19:00　日・祝　10：00～17:30"></a></p>
			<!--<p class="tel pc_none"><a href="tel:0120931911" onclick="yahoo_report_conversion('tel:0120-931-911')"><img src="images/tk_cv_sp2.gif" alt="0120-334-270 受付時間 水・木・金・土　12：00～19:00　日・祝　10：00～17:30"></a></p>-->
		</div>

		<div id="priceArea02">
      <div class="title_icon"><img src="images/hyaluronan_icon02.gif" width="20" height="20" alt=""></div>
      <h2 class="title06"><img src="images/hyaluronan_contents06_title.gif" width="252" height="121" alt="Price ～ヒアルロン酸注入料金～" class="sp_none"><img src="images/sp/hyaluronic_contents03_title.gif" width="100%" alt="Price ～ヒアルロン酸注入料金～" class="pc_none"></h2>
      <ul class="price_list cf">
        <li class="pb55"><img src="images/hyaluronan_contents06_img01.jpg" width="360" height="160" alt="眼瞼周囲" class="sp_none"><img src="images/sp/hyaluronic_contents03_img01.jpg" width="100%" alt="眼瞼周囲" class="pc_none"></li>
        <li class="pb55"><img src="images/hyaluronan_contents06_img02.jpg" width="360" height="160" alt="法令線・口唇" class="sp_none"><img src="images/sp/hyaluronic_contents03_img02.jpg" width="100%" alt="法令線・口唇" class="pc_none"></li>
        <li class="last pb55"><img src="images/hyaluronan_contents06_img03.jpg" width="360" height="160" alt="顎・隆鼻" class="sp_none"><img src="images/sp/hyaluronic_contents03_img03.jpg" width="100%" alt="顎・隆鼻" class="pc_none"></li>
        <li><img src="images/hyaluronan_contents06_img04.jpg" width="360" height="160" alt="バスト・ヒップ" class="sp_none"><img src="images/sp/hyaluronic_contents03_img04.jpg" width="100%" alt="バスト・ヒップ" class="pc_none"></li>
        <li><img src="images/hyaluronan_contents06_img05.jpg" width="360" height="160" alt="涙袋形成" class="sp_none"><img src="images/sp/hyaluronic_contents03_img05.jpg" width="100%" alt="涙袋形成" class="pc_none"></li>
        <li class="last"><img src="images/hyaluronan_contents06_img06.jpg" width="360" height="160" alt="鼻筋・眉間" class="sp_none"><img src="images/sp/hyaluronic_contents03_img06.jpg" width="100%" alt="鼻筋・眉間" class="pc_none"></li>
      </ul>
      <div class="campaign_price"><img src="images/hyaluronan_contents06_text01_wy.gif" width="689" height="84" alt="初回キャンペーン価格 0.1cc 4,800（+税）～" class="sp_none"><img src="images/sp/hyaluronic_contents03_text01.gif" width="100%" alt="初回キャンペーン価格 0.1cc 4,800（+税）～" class="pc_none"></div>
      <div class="campaign_price02">
      <!--<img src="images/hyaluronan_contents06_text02.jpg" width="840" height="84" alt="ほうれい線、目の下、隆鼻、顎形成 1cc 24,000（+税）～" class="sp_none">
      <img src="images/sp/hyaluronic_contents03_text02.jpg" width="100%" alt="ほうれい線、目の下、隆鼻、顎形成 1cc 24,000（+税）～" class="pc_none">-->
      </div>
      <p class="price_text">※ご希望の仕上がり、施術範囲、シワの深さなどによりヒアルロン酸の注入量が異なります。<br class="sp_none">その際はあらかじめ担当医師よりご説明をさせていただきます。<br>※2回目以降の施術を半額でお受けいただけるプランもございます。</p>
		</div>

		<div class="conversion">
			<div class="sp_none"><a href="#section15"><img src="images/hyaluronan_cv_btn01_off.png" width="609" height="163" alt="無料カウンセリングのご予約はこちらから"></a></div>
			<div class="pc_none"><a href="#section15"><img src="images/sp/hyaluronic_cv_btn.jpg" alt="無料カウンセリングのご予約はこちらから"></a></div>
			<p class="tel"><img src="images/tk_cv_pc1.gif" alt="0120-334-270 受付時間 水・木・金・土　12：00～19:00　日・祝　10：00～17:30" height="87" width="609" class="sp_none"></p>
			<p class="tel pc_none"><a href="tel:0120334270" onclick="javascript:goog_report_conversion('tel:0120-334-270');yahoo_report_conversion(undefined);return false;"><img src="images/tk_cv_sp1.gif" alt="0120-334-270 受付時間 水・木・金・土　12：00～19:00　日・祝　10：00～17:30"></a></p>
			<!--<p class="tel pc_none"><a href="tel:0120931911" onclick="yahoo_report_conversion('tel:0120-931-911')"><img src="images/tk_cv_sp2.gif" alt="0120-334-270 受付時間 水・木・金・土　12：00～19:00　日・祝　10：00～17:30"></a></p>-->
		</div>


		<!--
		<div id="contents07" class="inner">
			<div class="contents07">
            <a name="access" id="access"></a>
				<div class="title_icon"><img src="images/hyaluronan_icon01.gif" width="20" height="20" alt=""></div>
				<h2 id="title07"><img src="images/hyaluronan_contents07_title.gif" width="243" height="112" alt="ACCESS ～クリニックについて～" class="sp_none"><img src="images/sp/hyaluronic_contents07_title.gif" width="100%" alt="ACCESS ～クリニックについて～" class="pc_none"></h2>
				<div class="cf mb45">
					<div class="left_text">
						<h3><img src="images/hyaluronan_contents07_text01.gif" width="269" height="53" alt="表参道スキンクリニック" class="sp_none"><img src="images/sp/hyaluronic_contents07_text01.gif" width="100%" alt="表参道スキンクリニック" class="pc_none"></h3>
					</div>
					<div class="right_text">
						<p>〒812-0011<br />福岡県福岡市博多区博多駅前3丁目26-5 Ｍビル1号館8Ｆ</p>
					</div>
				</div>
				<div class="cf">
					<div class="map">
						<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3323.619403838426!2d130.4165377!3d33.5892298!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x354191b8cf40dcd3%3A0xf0dd7430beaba2b5!2z44CSODEyLTAwMTEg56aP5bKh55yM56aP5bKh5biC5Y2a5aSa5Yy65Y2a5aSa6aeF5YmN77yT5LiB55uu77yS77yW4oiS77yVIO-8reODk-ODq--8keWPt-mkqA!5e0!3m2!1sja!2sjp!4v1431411106073" width="480" height="415" frameborder="0" style="border:0"></iframe>
					</div>
					<div class="left_text02">
						<h4><img src="images/hyaluronan_contents07_text02.gif" width="201" height="45" alt="電車でお越しの方" class="sp_none"><img src="images/sp/hyaluronic_contents07_text02.gif" width="100%" alt="電車でお越しの方" class="pc_none"></h4>
						<table cellpadding="0" cellspacing="0" border="0">
							<tr>
								<th><img src="images/hyaluronan_contents07_icon01.gif" width="17" height="17" alt="1" /></th>
								<td>JR鹿児島本線 博多駅 博多口出口(1番出口) 徒歩6分</td>
							</tr>
							<tr>
								<th><img src="images/hyaluronan_contents07_icon02.gif" width="17" height="17" alt="2" /></th>
								<td>西鉄バス 博多駅前駅 徒歩5分</td>
							</tr>
						</table>
					  <h4><img src="images/hyaluronan_contents07_text03.gif" width="199" height="45" alt="お車でお越しの方" class="sp_none"><img src="images/sp/hyaluronic_contents07_text03.gif" width="100%" alt="電車でお越しの方" class="pc_none"></h4>
						<p>当クリニック専用の駐車はございません。<br>お近くの有料駐車場のご利用をお願いいたします。</p>
						<div class="cf mt40">
							<div class="left_text03">
								<h4><img src="images/hyaluronan_contents07_text04.gif" width="126" height="45" alt="診療時間" class="sp_none"><img src="images/sp/hyaluronic_contents07_text04.gif" width="100%" alt="診療時間" class="pc_none"></h4>
								<p>AM11:00～PM8:00（完全予約制）</p>
							</div>
							<div class="right_text02">
								<h4><img src="images/hyaluronan_contents07_text05.gif" width="103" height="45" alt="休診日" class="sp_none"><img src="images/sp/hyaluronic_contents07_text05.gif" width="100%" alt="休診日" class="pc_none"></h4>
								<p>月曜日・火曜日</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="conversion">
			<div class="sp_none"><a href="#section15"><img src="images/hyaluronan_cv_btn01_off.png" width="609" height="163" alt="無料カウンセリングのご予約はこちらから"></a></div>
			<div class="pc_none"><a href="#section15"><img src="images/sp/hyaluronic_cv_btn.jpg" alt="無料カウンセリングのご予約はこちらから"></a></div>
			<p class="tel"><img src="images/tk_cv_pc1.gif" alt="0120-334-270 受付時間 水・木・金・土　12：00～19:00　日・祝　10：00～17:30" height="87" width="609" class="sp_none"><a href="tel:0120-334-270" onclick="yahoo_report_conversion(undefined); goog_report_conversion('tel:0120-334-270');return false;" class="pc_none"><img src="images/sp/hyaluronic_cv_tel.gif" alt="0120-334-270 受付時間 水・木・金・土　12：00～19:00　日・祝　10：00～17:30"></a></p>
		</div>
		-->
		<div id="ideaArea">
      <div class="title_icon"><img src="images/hyaluronan_icon02.gif" width="20" height="20" alt=""></div>
      <h2 id="title08"><img src="images/hyaluronan_contents08_title.gif" width="200" height="122" alt="Idea ～クリニックの理念～" class="sp_none"><img src="images/sp/hyaluronic_contents08_title.gif" width="100%" alt="Idea ～クリニックの理念～" class="pc_none"></h2>
      <p class="contents08_text">開業当初から当クリニックでは痛みや不安のない施術を心がけており、患者様に安心して美しくなって頂くことを目指しております。</p>
      <h3><img src="images/hyaluronan_contents08_text01.gif" width="1200" height="55" alt="表参道スキンクリニック" class="sp_none"><img src="images/sp/hyaluronic_contents08_text01.gif" width="100%" alt="表参道スキンクリニック" class="pc_none"></h3>
      <ul class="contents08_list cf">
        <li>
          <div class="concept"><img src="images/hyaluronan_contents08_text02.gif" width="107" height="103" alt="CONCEPT1"></div>
          <h4><img src="images/hyaluronan_contents08_text03.gif" width="361" height="75" alt="患者様とのコミュニケーションを大切にしています" class="sp_none"><img src="images/sp/hyaluronic_contents08_text02.gif" width="100%" alt="CONCEPT1 患者様とのコミュニケーションを大切にしています" class="pc_none"></h4>
          <div><img src="images/hyaluronan_contents08_img01.jpg" width="361" height="150" alt="" class="sp_none"><img src="images/sp/hyaluronic_contents08_img01.jpg" width="100%" alt="" class="pc_none"></div>
          <p>当クリニックは患者様の立場に立ち、今抱えている不安や悩みなどを解消するため、丁寧なカウンセリングを行なっています。</p>
        </li>
        <li>
          <div class="concept"><img src="images/hyaluronan_contents08_text04.gif" width="107" height="103" alt="CONCEPT2"></div>
          <h4><img src="images/hyaluronan_contents08_text05_wy.gif" width="361" height="75" alt="長年の経験と実績による確かな技術と高い信頼" class="sp_none"><img src="images/sp/hyaluronic_contents08_text03.gif" width="100%" alt="CONCEPT2 長年の経験と実績による確かな技術と高い信頼" class="pc_none"></h4>
          <div><img src="images/hyaluronan_contents08_img02.jpg" width="361" height="150" alt="" class="sp_none"><img src="images/sp/hyaluronic_contents08_img02.jpg" width="100%" alt="" class="pc_none"></div>
          <?php
  $url = $_SERVER['REQUEST_URI'];
  if(strstr($url,'index_wy')==true):
?>
<p>当クリニックは美容皮膚科医が多数在籍しています。専門医がお客さまとのカウンセリングを基に、最適な施術ご提案いたします。</p>
<?php else :?>
          <p>当クリニックは優秀な美容皮膚科医が多数在籍しています。本当に安心して任せられるドクターがレベルの高い治療を行ないます。</p><?php endif;?>
        </li>
        <li class="last">
          <div class="concept"><img src="images/hyaluronan_contents08_text06.gif" width="107" height="103" alt="CONCEPT3"></div>
          <h4><img src="images/hyaluronan_contents08_text07.gif" width="361" height="75" alt="いつもドクターがそばにいてくれるという感覚" class="sp_none"><img src="images/sp/hyaluronic_contents08_text04.gif" width="100%" alt="CONCEPT3 いつもドクターがそばにいてくれるという感覚" class="pc_none"></h4>
          <div><img src="images/hyaluronan_contents08_img03.jpg" width="361" height="150" alt="" class="sp_none"><img src="images/sp/hyaluronic_contents08_img03.jpg" width="100%" alt="" class="pc_none"></div>
          <p>当クリニックは気軽に相談できる環境作りを心がけています。術後のケア、美肌づくりのアドバイスなどいつでもすぐにお応えいたします。</p>
        </li>
      </ul>
		</div>


		<!--
		<div id="doctorArea" class="inner">
			<div class="title_icon"><img src="images/hyaluronan_icon01.gif" width="20" height="20" alt=""></div>
			<h2 id="title09"><img src="images/hyaluronan_contents09_title.gif" width="260" height="107" alt="Doctor ～医師のご紹介～" class="sp_none"><img src="images/sp/hyaluronic_contents09_title.gif" width="100%" alt="Doctor ～医師のご紹介～" class="pc_none"></h2>
			<p class="contents09_text">当クリニックの医師たちは、お客様に安全に美しくなって頂く為に、日々最高水準の技術を日々研究しております。<br>そんな医師たちを御紹介させていただきます。</p>
			<h3><img src="images/hyaluronan_contents09_text01.gif" width="960" height="40" alt="院長 小原 直樹" class="sp_none"><img src="images/sp/hyaluronic_contents09_text01.gif" width="100%" alt="院長 小原 直樹" class="pc_none"></h3>
			<ul class="contents09_detail cf">
				<li class="image"><img src="images/hyaluronan_contents09_img01.jpg" width="156" height="191" alt="" class="sp_none"><img src="images/sp/hyaluronic_contents09_img01.jpg" width="100%" alt="" class="pc_none"></li>
				<li class="w288">
					<h4>経歴</h4>
					<p><span>平成16年3月</span><br />岡山大学医学部医学科　卒業</p>
<p><span>平成16年4月</span><br />川崎医科大学附属病院　初期臨床研修医</p>
<p><span>平成18年3月</span><br />川崎医科大学附属病院　初期臨床研修医修了</p>
<p><span>平成18年4月</span><br />川崎医科大学付属部病院形成外科学教室　入局</p>
<p><span>平成18年8月</span><br />福山市民病院形成外科　勤務</p>
<p><span>平成24年3月</span><br />福山市民病院形成外科　退職</p>
<p><span>平成24年4月</span><br />国立病院機構福山医療センター形成外科　勤務</p>
<p><span>平成25年3月</span><br />国立病院機構福山医療センター形成外科　退職</p>
<p><span>平成25年4月</span><br />大手美容外科　勤務</p>
<p><span>平成26年7月</span><br />表参道スキンクリニック　勤務</p>
<p><span>平成27年1月</span><br />NAOクリニック開院</p>
				</li>
				<li class="w464">
					<h4 class="title01">所属学会など</h4>
					<p><span>・</span>日本形成外科学会認定専門医　<span>・</span>日本美容学会（JSAPS)正会員<br /><span>・</span>日本抗加齢学会認定専門医</p>
					<h4 class="title02">小原先生からひと言</h4>
					<p>どんなささいな悩みでも、悩んでいる本人にとっては大きなコンプレックス。<br />
自信をもって前向きに過ごせるお手伝いができるよう一人一人丁寧な診察を心掛けています。<br />
私自身も美容の治療は大好きでいろんな治療を試していますので、ぜひ色々質問してください。<br />
					当クリニックでは、高い技術を持つ経験豊富な美容皮膚科医が、患者様の希望や理想を大切にしながら、最新の美容医療を提供いたします。<br />
					「愛されツルスベ美肌を手に入れたい…」多くの女性が持つ、そんな願いにお応えします。</p>
				</li>
			</ul>
		</div>

		<div class="conversion">
			<div class="sp_none"><a href="#section15"><img src="images/hyaluronan_cv_btn01_off.png" width="609" height="163" alt="無料カウンセリングのご予約はこちらから"></a></div>
			<div class="pc_none"><a href="#section15"><img src="images/sp/hyaluronic_cv_btn.jpg" alt="無料カウンセリングのご予約はこちらから"></a></div>
			<p class="tel"><img src="images/tk_cv_pc1.gif" alt="0120-334-270 受付時間 水・木・金・土　12：00～19:00　日・祝　10：00～17:30" height="87" width="609" class="sp_none"><a href="tel:0120-334-270" onclick="yahoo_report_conversion(undefined); goog_report_conversion('tel:0120-334-270');return false;" class="pc_none"><img src="images/sp/hyaluronic_cv_tel.gif" alt="0120-334-270 受付時間 水・木・金・土　12：00～19:00　日・祝　10：00～17:30"></a></p>
		</div>
inner !-->

          <?php
  $url = $_SERVER['REQUEST_URI'];
  if(strstr($url,'index_wy')==true):
?>
<?php include 'tabs/index_wy.html'; ?>
<?php else :?>

<?php include 'tabs/index.html'; ?>
<?php endif;?>

<!--フォーム導入-->
  <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/smoothness/jquery-ui.css" >
  <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js"></script>
  <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1/i18n/jquery.ui.datepicker-ja.min.js"></script>
  <script>
  $(function() {
    $("#datepicker_1").datepicker({
      dateFormat: 'yy年mm月dd日 (DD)',
      dayNames: ['日', '月', '火', '水', '木', '金', '土']
    });
    $("#datepicker_2").datepicker({
      dateFormat: 'yy年mm月dd日 (DD)',
      dayNames: ['日', '月', '火', '水', '木', '金', '土']
    });
  });
  </script>
  <div id="section15" name="section15" class="box">

    <div class="block13 cf">
      <h2 class="mt20"><img src="images/form/title10.gif" width="1200" height="60" alt="無料カウンセリングご予約フォーム" class="sp_none"><img src="images/form/title10_sp.gif" width="100%" alt="無料カウンセリングご予約フォーム" class="pc_none"></h2>
      <p class="flow"><img src="images/form/flow_01.gif" alt="1．情報の入力" width="1000" height="80" /></p>
      <div class="form">
        <p class="caution">※お手数ですが、必須項目をすべてご入力の上、確認画面へお進みください。</p>

        <form method="post">

          <table>
            <tr>
              <th class="hissu">お名前（ひらがな）</th>
              <td>
                <input type="text" name="name_req" value="<?php echo $formTool->h($_POST['name_req']); ?>" />　例）おもてさんどうはなこ
                <?php if( !empty($formTool->messages['name_req']) ) : ?>
                  <p class="error"><?php echo $formTool->h($formTool->messages['name_req']); ?></p>
                <?php endif; ?>
              </td>
            </tr>
            <tr>
              <th class="hissu">電話番号</th>
              <td>
                <input type="tel" name="tel_req" value="<?php echo $formTool->h($_POST['tel_req']); ?>" />　例）01-2345-6789
                <?php if( !empty($formTool->messages['tel_req']) ) : ?>
                  <p class="error"><?php echo $formTool->h($formTool->messages['tel_req']); ?></p>
                <?php endif; ?>
              </td>
            </tr>
            <tr>
              <th class="hissu">メールアドレス</th>
              <td>
                <input type="email" name="email_req" value="<?php echo $formTool->h($_POST['email_req']); ?>" />　例）info@abc.com
                <?php if( !empty($formTool->messages['email_req']) ) : ?>
                  <p class="error"><?php echo $formTool->h($formTool->messages['email_req']); ?></p>
                <?php endif; ?>
              </td>
            </tr>
            <tr>
              <th class="hissu">年齢</th>
              <td>
                <span><input type="text" name="age_req" value="<?php echo $formTool->h($_POST['age_req']); ?>" class="number" /> 歳</span>　例）30歳
                <?php if( !empty($formTool->messages['age_req']) ) : ?>
                  <p class="error"><?php echo $formTool->h($formTool->messages['age_req']); ?></p>
                <?php endif; ?>
              </td>
            </tr>
            <!--
			<tr>
              <th class="hissu">ご希望のクリニック</th>
              <td>
                <?php echo $formTool->makeRadioHtml($formTool->hopeArr, 'array', $_POST['hope_req'], 'hope_req'); ?>
                <?php if( !empty($formTool->messages['hope_req']) ) : ?>
                  <p class="error"><?php echo $formTool->h($formTool->messages['hope_req']); ?></p>
                <?php endif; ?>
              </td>
            </tr>
			-->
            <tr>
              <th class="nini">相談日 第一希望</th>
              <td>
                <input type="text" name="day1" value="<?php echo $formTool->h($_POST['day1']); ?>" placeholder="YYYY年MM月DD日" class="calendar" id="datepicker_1" readonly/>
                <select name="day1_period">
                  <option value="">時間帯を選択</option>
                  <?php echo $formTool->makeOptionHtml($formTool->timeArr, 'array', $_POST['day1_period']); ?>
                </select>
              </td>
            </tr>
            <tr>
              <th class="nini">相談日 第二希望</th>
              <td>
                <input type="text" name="day2" value="<?php echo $formTool->h($_POST['day2']); ?>" placeholder="YYYY年MM月DD日" class="calendar" id="datepicker_2" readonly/>
                <select name="day2_period">
                  <option value="">時間帯を選択</option>
                  <?php echo $formTool->makeOptionHtml($formTool->timeArr, 'array', $_POST['day2_period']); ?>
                </select>
              </td>
            </tr>
            <tr>
              <th class="nini">連絡方法のご希望</th>
              <td class="radiostyle">
                <?php echo $formTool->makeCheckBoxHtml($formTool->typeArr, 'array', $_POST['type'], 'type'); ?>
              </td>
            </tr>
            <tr>
              <th class="nini last">お問い合わせ内容</th>
              <td class="last">
                <textarea name="message" placeholder="例）ヒアルロン酸でしわを目立たなくさせたいです。" cols="20" rows="5"><?php echo $formTool->h($_POST['message']); ?></textarea>
              </td>
            </tr>
          </tbody>
        </table>

        <input type="hidden" name="mode" value="conf" />
        <p class="btn"><input type="image" src="images/form/formbtn_01.gif" alt="確認画面へ" width="420" height="70" /></p>

      </form>

    </div>
  </div>
</div>
<!--フォーム導入-->

		<p id="page-top"><a href="#wrap"><img src="images/hyaluronan_pagetop.png" width="45" height="45" alt="pagetop"></a></p>
	</div><!-- wrapper !-->
