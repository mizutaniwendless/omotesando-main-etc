<?php
global $formTool;
?>
<div class="cnt_wp">




  <ul id="navi">
    <li><a href="#section01" class="on">●</a></li>
    <li><a href="#section02">●</a></li>
    <li><a href="#section03">●</a></li>
    <li><a href="#section04">●</a></li>
    <li><a href="#section05">●</a></li>
    <li><a href="#section06">●</a></li>
    <li><a href="#section07">●</a></li>
    <li><a href="#section08">●</a></li>
    <li><a href="#section09">●</a></li>
    <li><a href="#section10">●</a></li>
    <li><a href="#section11">●</a></li>
    <li><a href="#section12">●</a></li>
  </ul>




<div id="section01" class="box">

	<article class="block01">
		<h2><img src="img/main-img_01.jpg" alt="二の腕・太もも・お腹・おしり 落ちにくい脂肪を最新技術で除去 - 理想のボディラインを手に入れよう -" width="1500" height="642" class="sp_none"><img src="img/main-img_01_sp.jpg" alt="二の腕・太もも・お腹・おしり 落ちにくい脂肪を最新技術で除去 - 理想のボディラインを手に入れよう -" width="100%" class="pc_none"></h2>
		<p><img src="img/main-img_02.png" alt="肌見せの季節までに何とかしたい！ 脂肪が落ちにくい気になる部位…。" width="1500" height="158" class="sp_none"><img src="img/main-img_02_sp.png" alt="肌見せの季節までに何とかしたい！ 脂肪が落ちにくい気になる部位…。" width="100%" class="pc_none"></p>
	</article>

	<article class="block02">
		<h2><img src="img/main-txt.png" alt="気になる部位の脂肪を運動やダイエットでピンポイントに落とすのは大変…。" width="1200" height="60" class="sp_none"><img src="img/main-txt_sp.png" alt="気になる部位の脂肪を運動やダイエットでピンポイントに落とすのは大変…。" width="100%" class="pc_none"></h2>
		<p>理想のボディラインを手に入れるために脂肪吸引のニーズが高まっています。<br>
		実際に施術を受けるにあたり重要なことは“理想のボディをデザインする”という事です。<br>
		脂肪吸引のし過ぎにより全体の形、バランスが崩れてしまってはボディデザインとは言えません。<br>
		当クリニックの医師は豊富な経験と実績で患者様のご希望とボディデザインを両立させるご提案をいたします。</p>
	</article>

	<div class="campaign">
		<p><a href="#section12" class="sp_none"><img src="img/campaign-big.png" alt="初回限定 脂肪吸引モニター価格 各料金50％OFF! モニターの内容はホームページ・雑誌・メディアへ掲載させていただきます。" width="877" height="91"></a><a href="#section12" class="pc_none"><img src="img/campaign-sp.png" alt="初回限定 脂肪吸引モニター価格 各料金50％OFF! モニターの内容はホームページ・雑誌・メディアへ掲載させていただきます。"></a></p>
		<p>※モニター掲載は、ホームページやDMなどの限られた媒体で、施術箇所のみです。顔などは一切掲載されません。</p>
	</div>

	<article class="block03 cf">
		<div class="block_l bg1">
			<div class="point cf">
				<div class="image"><img src="img/block03-img_01.png" alt="二の腕" width="194" height="194"></div>
				<div class="read">
					<h3><img src="img/block03-txt_01.png" alt="二の腕" width="100" height="30" class="sp_none"><img src="img/block03-txt_01_sp.png" alt="二の腕" class="pc_none"></h3>
					<p class="txt">二の腕の付け根は、年齢とともに脂肪が付きやすい部位。細くすっきりした二の腕へ。</p>
					<p class="sp_none"><a href="#block09_01"><img src="img/block03-btn_off.png" alt="詳しく見る" width="162" height="32" /></a></p>
					<p class="pc_none"><a href="#block09_01"><img src="img/block03-btn_sp.png" alt="詳しく見る" width="200" height="50" /></a></p>
				</div>
			</div>
		</div>
		<div class="block_r bg2">
			<div class="point cf">
				<div class="image"><img src="img/block03-img_02.png" alt="太もも" width="194" height="194"></div>
				<div class="read">
					<h3><img src="img/block03-txt_02.png" alt="太もも" width="100" height="30" class="sp_none"><img src="img/block03-txt_02_sp.png" alt="太もも" class="pc_none"></h3>
					<p class="txt">体の中でも脂肪量が多い太もも。細く足が長く見えるモデルのような太ももに。</p>
					<p class="sp_none"><a href="#block09_02"><img src="img/block03-btn_off.png" alt="詳しく見る" width="162" height="32" /></a></p>
					<p class="pc_none"><a href="#block09_02"><img src="img/block03-btn_sp.png" alt="詳しく見る" width="200" height="50" /></a></p>
				</div>
			</div>
		</div>
	</article>
	<article class="block03 cf">
		<div class="block_l bg2">
			<div class="point cf">
				<div class="image"><img src="img/block03-img_03.png" alt="お腹" width="194" height="194"></div>
				<div class="read">
					<h3><img src="img/block03-txt_03.png" alt="お腹" width="100" height="30" class="sp_none"><img src="img/block03-txt_03_sp.png" alt="お腹" class="pc_none"></h3>
					<p class="txt">脂肪がつきやすく、シェイプアップしにくいお腹まわり。スッキリしたくびれのあるウエストラインを目指して。</p>
					<p class="sp_none"><a href="#block09_03"><img src="img/block03-btn_off.png" alt="詳しく見る" width="162" height="32" /></a></p>
					<p class="pc_none"><a href="#block09_03"><img src="img/block03-btn_sp.png" alt="詳しく見る" width="200" height="50" /></a></p>
				</div>
			</div>
		</div>
		<div class="block_r bg1">
			<div class="point cf">
				<div class="image"><img src="img/block03-img_04.png" alt="おしり" width="194" height="194"></div>
				<div class="read">
					<h3><img src="img/block03-txt_04.png" alt="おしり" width="100" height="30" class="sp_none"><img src="img/block03-txt_04_sp.png" alt="おしり" class="pc_none"></h3>
					<p class="txt">ボリュームのあるお尻、垂れ下がったお尻をサイズダウンして丸みのある女性らしいシルエットに。</p>
					<p class="sp_none"><a href="#block09_04"><img src="img/block03-btn_off.png" alt="詳しく見る" width="162" height="32" /></a></p>
					<p class="pc_none"><a href="#block09_04"><img src="img/block03-btn_sp.png" alt="詳しく見る" width="200" height="50" /></a></p>
				</div>
			</div>
		</div>
	</article>

	<article class="block04 cf">
		<p><img src="img/block04-txt.png" alt="当院では患者様との入念なカウンセリングを行い、経験豊富な医師が患者様個人に適した最適な施術プランをご提供いたします。" width="910" height="64" class="sp_none"><img src="img/block04-txt_sp.png" alt="当院では患者様との入念なカウンセリングを行い、経験豊富な医師が患者様個人に適した最適な施術プランをご提供いたします。" width="100%" class="pc_none"></p>
	</article>

	<div class="btn_box cf">
		<div class="btn_camp">
			<p><a href="#section12" class="sp_none"><img src="img/campaign-small.png" alt="初回限定 脂肪吸引モニター価格 各料金50％OFF! モニターの内容はホームページ・雑誌・メディアへ掲載させていただきます。" width="554" height="91"></a><a href="#section12" class="pc_none"><img src="img/campaign-sp.png" alt="初回限定 脂肪吸引モニター価格 各料金50％OFF! モニターの内容はホームページ・雑誌・メディアへ掲載させていただきます。"></a></p>
			<p>※モニター掲載は、ホームページやDMなどの限られた媒体で、施術箇所のみです。顔などは一切掲載されません。</p>
		</div>
		<dl class="cf">
			<dt><img src="img/tel_01.png" alt="098-860-9980 受付時間 // 火・水・木・金・土 12：00～20:00　日・祝 10：00～19:00" width="260" height="70" class="sp_none"><a href="tel:0988609980" onclick="yahoo_report_conversion('tel:098-860-9980')" class="pc_none"><img src="img/tel_01_sp_01.png" alt="098-860-9980" width="500" height="103" class="pc_none"></a><img src="img/tel_01_sp_02.png" alt="受付時間 // 火・水・木・金・土 12：00～20:00　日・祝 10：00～19:00" class="pc_none"></dt>
			<dd><a href="#section12" class="sp_none"><img src="img/btn_01_off.png" alt="無料カウンセリングのご予約" width="300" height="70" /></a><a href="#section12" class="pc_none"><img src="img/btn_01_sp.png" alt="無料カウンセリングのご予約"></a></dd>
		</dl>
	</div>

</div>




<div id="section02" class="box">

	<article class="block05 cf">
		<div class="block">
			<div class="read">
				<h2><img src="img/block05-txt.png" alt="脂肪吸引が有効な理由" width="510" height="100" class="sp_none"><img src="img/block05-txt_sp.png" alt="脂肪吸引が有効な理由" class="pc_none"></h2>
				<p>脂肪細胞の数は思春期までに決定されてしまい、それ以降はほとんど変わることがないと言われています。太るということはその脂肪細胞が膨らむことが原因です。<br>
				脂肪吸引は脂肪細胞の数を減らすことで、その部位のリバウンドが生じにくさせ、確実な効果が望めるメソッドです。</p>
			</div>
			<div class="image"><img src="img/block05-img_01.jpg" alt=""></div>
		</div>
	</article>

	<article class="block06 cf">
		<div class="read">
			<h2><img src="img/block06-txt.png" alt="最新式ボディジェット脂肪吸引法（FDA認可）" width="530" height="240" class="sp_none"><img src="img/block06-txt_sp.png" alt="最新式ボディジェット脂肪吸引法（FDA認可）" class="pc_none"></h2>
			<p>ボディジェットとは、ドイツで開発された最新の脂肪吸引専用の医療機器で、日本国内に僅か数台しかありません。従来の脂肪吸引と比較して、圧倒的に手術時の出血が少なく、手術後の痛みも大幅に軽減しています。周辺組織や体への負担が軽度で痛み、腫れ、むくみ、ダウンタイムが短いのが特徴です。これは、ボディジェットが水を勢いよく噴射することで、血管や神経などは傷つけず脂肪だけを溶かして吸引します。<br>
			弱い吸引力でも脂肪細胞を吸引できるため、取りムラを抑えられ、吸引部分が拘縮して皮膚の表面が凸凹を防ぐことができます。</p>
		</div>
		<div class="image cf">
			<ul class="cf">
				<li><img src="img/block06-img_01.png" alt="1.ジェット水流を噴射"></li>
				<li><img src="img/block06-img_02.png" alt="2.脂肪だけをバラバラに分解"></li>
				<li><img src="img/block06-img_03.png" alt="3.脂肪と水を吸引"></li>
				<li><img src="img/block06-img_04.png" alt="4.脂肪だけを取り除く"></li>
			</ul>
			<p><img src="img/block06-img_05.png" alt="皮膚の構造"></p>
		</div>
	</article>

	<article class="block07">
		<div class="block_l cf">
			<div class="point">
				<h3><img src="img/block07-txt_01.png" alt="従来の脂肪吸引" width="460" height="50" class="sp_none"><img src="img/block07-txt_01_sp.png" alt="従来の脂肪吸引" class="pc_none"></h3>
				<div class="read">
					<p>従来の脂肪吸引法では、カニューレ（吸引に使用する管）の操作により、脂肪以外の周囲の組織に機械的ダメージを与えてしまいます。特に脂肪層の直下にある筋肉層は血管や神経が豊富で、ここにダメージを与えると内出血や知覚鈍磨などを引き起こしやすく、ダウンタイムが長くなります。</p>
					<div class="image"><img src="img/block07-img_01.png" alt="従来の脂肪吸引"></div>
				</div>
			</div>
			<div class="ar"><img src="img/block07-ar.png" alt="" width="25" height="45" class="sp_none"><img src="img/block07-ar_sp.png" alt="" class="pc_none"></div>
		</div>
		<div class="block_r cf">
			<div class="point">
				<h3><img src="img/block07-txt_02.png" alt="ボディジェットの脂肪吸引" width="460" height="50" class="sp_none"><img src="img/block07-txt_02_sp.png" alt="ボディジェットの脂肪吸引" class="pc_none"></h3>
				<div class="read">
					<p>ボディジェットは、脂肪の除去に先立ち、麻酔液と止血剤入りのジェット水流で、脂肪層と筋肉層の間に水の隙間（ウォーターポケット）を作ります。そうするとカニューレ操作での筋肉層へのダメージを防ぎ、痛みや内出血もほとんどなく、ダウンタイムが少ない施術が行えます。</p>
					<div class="image"><img src="img/block07-img_02.png" alt="ボディジェットの脂肪吸引"></div>
				</div>
			</div>
		</div>
	</article>

	<article class="block08">
		<h2><img src="img/block08-txt_01.png" alt="ボディジェットによる脂肪吸引は…" width="830" height="55" class="sp_none"><img src="img/block08-txt_01_sp.png" alt="ボディジェットによる脂肪吸引は…" class="pc_none"></h2>
		<p>(1) 施術中の体への負担が少ない。<br>
		(2) 血管や神経を傷つけにくく、術後のダウンタイムも短い<br>
		(3) 確実な脂肪吸引力と繊細なボディデザインが可能</p>
		<div class="table-box">
			<table cellpadding="0" cellspacing="0" border="0">
				<tr class="first">
					<th width="12%" class="none"></th>
					<th width="22%">脂肪溶解注射</th>
					<th width="22%">従来の脂肪吸引法</th>
					<th width="22%">ベイザー脂肪吸引法</th>
					<th width="22%" class="ch">ボディジェット脂肪吸引法<br>（当院）</th>
				</tr>
				<tr>
					<th>原理</th>
					<td>薬剤で溶かし、吸収を待つ。</td>
					<td>陰圧により脂肪組織を吸引する。</td>
					<td>ベイザー波を用いて皮下脂肪を液体状に溶かし吸引する。</td>
					<td class="ch">ベイザー波を用いて皮下脂肪を液体状に溶かし吸引する。</td>
				</tr>
				<tr>
					<th>脂肪除去効果</th>
					<td>脂肪吸引と比べると低い。</td>
					<td>吸引量は多いが、周辺組織も一緒に吸引してしまう。</td>
					<td>吸引量は多いが、周辺組織も一緒に吸引してしまう。</td>
					<td class="ch">脂肪細胞だけを吸引するため、十分な量の脂肪を除去できる。</td>
				</tr>
				<tr class="tc">
					<th>生活の制限</th>
					<td>ほとんど無し</td>
					<td>多い</td>
					<td>少ない</td>
					<td class="ch">少ない</td>
				</tr>
				<tr class="tc">
					<th>内出血</th>
					<td>少ない</td>
					<td>多い</td>
					<td>少ない</td>
					<td class="ch">非常に少ない</td>
				</tr>
				<tr class="tc">
					<th>痛み</th>
					<td>少ない</td>
					<td>多い</td>
					<td>少ない</td>
					<td class="ch">非常に少ない</td>
				</tr>
				<tr class="tc">
					<th>ダウンタイム</th>
					<td>ほとんど無し</td>
					<td>長い</td>
					<td>短い</td>
					<td class="ch">非常に短い</td>
				</tr>
				<tr class="tc">
					<th>皮膚のたるみ</th>
					<td>ほとんど無し</td>
					<td>多い</td>
					<td>多い</td>
					<td class="ch">少ない</td>
				</tr>
				<tr class="tc">
					<th>回数</th>
					<td>最低3回から5回以上</td>
					<td>1回</td>
					<td>1回</td>
					<td class="ch">1回</td>
				</tr>
				<tr class="last vt">
					<th>評価</th>
					<td>マイルドな効果の為、回数が必要。セルライトや深層の脂肪は溶解しにくい。脂肪量が多いと効果が出にくい。<br>
					<span>比較的脂肪量の少ない方は、効果的なことが多い。</span></td>
					<td>周辺組織を傷付けるため、安全性が低い。</td>
					<td>従来の施術では困難だった部位や体型に関わらず、施術可能。吸引量は多い。術後の痛みや内出血、腫れは比較的少ない。<br>
					<span>周囲組織の損傷などの安全性を考慮し、当院では使用していません。</span></td>
					<td class="ch">従来の施術では困難だった部位や体型に関わらず、施術可能。<br>
					<span>ジェット水流により脂肪組織以外の血管や神経を傷つける心配が少ない為、安全が高く、術後の負担も少ない。</span></td>
				</tr>
			</table>
		</div>
	</article>

	<div class="btn_box_out">
		<div class="btn_box cf">
			<div class="btn_camp">
				<p><a href="#section12" class="sp_none"><img src="img/campaign-small.png" alt="初回限定 脂肪吸引モニター価格 各料金50％OFF! モニターの内容はホームページ・雑誌・メディアへ掲載させていただきます。" width="554" height="91"></a><a href="#section12" class="pc_none"><img src="img/campaign-sp.png" alt="初回限定 脂肪吸引モニター価格 各料金50％OFF! モニターの内容はホームページ・雑誌・メディアへ掲載させていただきます。"></a></p>
				<p>※モニター掲載は、ホームページやDMなどの限られた媒体で、施術箇所のみです。顔などは一切掲載されません。</p>
			</div>
			<dl class="cf">
				<dt><img src="img/tel_01.png" alt="098-860-9980 受付時間 // 火・水・木・金・土 12：00～20:00　日・祝 10：00～19:00" width="260" height="70" class="sp_none"><a href="tel:0988609980" onclick="yahoo_report_conversion('tel:098-860-9980')" class="pc_none"><img src="img/tel_01_sp_01.png" alt="098-860-9980" width="500" height="103" class="pc_none"></a><img src="img/tel_01_sp_02.png" alt="受付時間 // 火・水・木・金・土 12：00～20:00　日・祝 10：00～19:00" class="pc_none"></dt>
				<dd><a href="#section12" class="sp_none"><img src="img/btn_01_off.png" alt="無料カウンセリングのご予約" width="300" height="70" /></a><a href="#section12" class="pc_none"><img src="img/btn_01_sp.png" alt="無料カウンセリングのご予約"></a></dd>
			</dl>
		</div>
	</div>

</div>




<div id="section03" class="box">

	<article class="block09">
		<div class="block cf">
			<div id="block09_01" name="block09_01" class="read fl">
				<h2><img src="img/block09-txt_01.png" alt="二の腕の脂肪吸引" width="460" height="78" class="sp_none"><img src="img/block09-txt_sp_01.png" alt="二の腕の脂肪吸引" class="pc_none"></h2>
				<p>二の腕の付け根は、年齢とともに脂肪が付きやすい部位です。<br>
				筋肉の割合が多く、脂肪層の薄い二の腕は通常太りにくい部分ですが、いったん二の腕に脂肪が付いてしまうと痩せにくいため、二の腕ダイエットをしても簡単には細くなりません。<br>
				二の腕の脂肪は、ファッションを楽しむためにも、ぜひとも取り除き、理想の形を目指します。</p>
				<table cellpadding="0" cellspacing="0" border="0">
					<tr>
						<th>・上腕部（半周）</th>
						<td>300,000円～</td>
					</tr>
					<tr>
						<th>・上腕部（全周）</th>
						<td>500,000円～</td>
					</tr>
				</table>
			</div>
			<div id="block09_02" name="block09_02" class="read fr">
				<h2><img src="img/block09-txt_02.png" alt="太ももの脂肪吸引" width="460" height="78" class="sp_none"><img src="img/block09-txt_sp_02.png" alt="太ももの脂肪吸引" class="pc_none"></h2>
				<p>体の中でも脂肪量が多く、ダイエットや筋トレではなかなか細くならない太もも。<br>
				モデルさんのように細い足になって、細身のスキニーパンツやミニスカートを履きこなせるようになりたい。<br>
				そんな理想の太ももをボディデザインします。</p>
				<table cellpadding="0" cellspacing="0" border="0">
					<tr>
						<th>・大腿部ポイント法</th>
						<td>400,000円～</td>
					</tr>
					<tr>
						<th>・大腿部全周法</th>
						<td>700,000円～</td>
					</tr>
					<tr>
						<th>・大腿部全周法+移行部</th>
						<td>1,000,000円～</td>
					</tr>
				</table>
			</div>
		</div>
	</article>

	<div class="btn_box_out">
		<div class="btn_box cf">
			<div class="btn_camp">
				<p><a href="#section12" class="sp_none"><img src="img/campaign-small.png" alt="初回限定 脂肪吸引モニター価格 各料金50％OFF! モニターの内容はホームページ・雑誌・メディアへ掲載させていただきます。" width="554" height="91"></a><a href="#section12" class="pc_none"><img src="img/campaign-sp.png" alt="初回限定 脂肪吸引モニター価格 各料金50％OFF! モニターの内容はホームページ・雑誌・メディアへ掲載させていただきます。"></a></p>
			</div>
			<dl class="cf">
				<dt><img src="img/tel_01.png" alt="098-860-9980 受付時間 // 火・水・木・金・土 12：00～20:00　日・祝 10：00～19:00" width="260" height="70" class="sp_none"><a href="tel:0988609980" onclick="yahoo_report_conversion('tel:098-860-9980')" class="pc_none"><img src="img/tel_01_sp_01.png" alt="098-860-9980" width="500" height="103" class="pc_none"></a><img src="img/tel_01_sp_02.png" alt="受付時間 // 火・水・木・金・土 12：00～20:00　日・祝 10：00～19:00" class="pc_none"></dt>
				<dd><a href="#section12" class="sp_none"><img src="img/btn_01_off.png" alt="無料カウンセリングのご予約" width="300" height="70" /></a><a href="#section12" class="pc_none"><img src="img/btn_01_sp.png" alt="無料カウンセリングのご予約"></a></dd>
			</dl>
		</div>
	</div>

</div>




<div id="section04" class="box">

	<article class="block09">
		<div class="block cf">
			<div id="block09_03" name="block09_03" class="read fl">
				<h2><img src="img/block09-txt_03.png" alt="お腹の脂肪吸引" width="460" height="78" class="sp_none"><img src="img/block09-txt_sp_03.png" alt="お腹の脂肪吸引" class="pc_none"></h2>
				<p>スカートの上に乗ってしまった脂肪、水着になるのが恥ずかしい、年齢のせいかおなか回りがたるんできたなど、お腹周りの脂肪が気になる方は多いと思います。</p>
				<table cellpadding="0" cellspacing="0" border="0">
					<tr>
						<th>・ウエスト部</th>
						<td>300,000円～</td>
					</tr>
					<tr>
						<th>・上腹部</th>
						<td>300,000円～</td>
					</tr>
					<tr>
						<th>・下腹部</th>
						<td>400,000円～</td>
					</tr>
					<tr>
						<th>・全腹部</th>
						<td>750,000円～</td>
					</tr>
				</table>
			</div>
			<div id="block09_04" name="block09_04" class="read fr">
				<h2><img src="img/block09-txt_04.png" alt="おしりの脂肪吸引" width="460" height="78" class="sp_none"><img src="img/block09-txt_sp_04.png" alt="おしりの脂肪吸引" class="pc_none"></h2>
				<p>大きすぎるお尻、垂れ下がったお尻を何とかしたい。<br>
				女性らしい丸みは残しつつ、引き締まって、キュッと上がったお尻は脚長効果ももたらします。<br>
				理想のヒップラインでパンツスタイルにも断然差がつきます。</p>
				<table cellpadding="0" cellspacing="0" border="0">
					<tr>
						<th>・臀部または臀部移行部</th>
						<td>400,000円～</td>
					</tr>
				</table>
			</div>
		</div>
	</article>

	<div class="btn_box_out">
		<div class="btn_box cf">
			<div class="btn_camp">
				<p><a href="#section12" class="sp_none"><img src="img/campaign-small.png" alt="初回限定 脂肪吸引モニター価格 各料金50％OFF! モニターの内容はホームページ・雑誌・メディアへ掲載させていただきます。" width="554" height="91"></a><a href="#section12" class="pc_none"><img src="img/campaign-sp.png" alt="初回限定 脂肪吸引モニター価格 各料金50％OFF! モニターの内容はホームページ・雑誌・メディアへ掲載させていただきます。"></a></p>
			</div>
			<dl class="cf">
				<dt><img src="img/tel_01.png" alt="098-860-9980 受付時間 // 火・水・木・金・土 12：00～20:00　日・祝 10：00～19:00" width="260" height="70" class="sp_none"><a href="tel:0988609980" onclick="yahoo_report_conversion('tel:098-860-9980')" class="pc_none"><img src="img/tel_01_sp_01.png" alt="098-860-9980" width="500" height="103" class="pc_none"></a><img src="img/tel_01_sp_02.png" alt="受付時間 // 火・水・木・金・土 12：00～20:00　日・祝 10：00～19:00" class="pc_none"></dt>
				<dd><a href="#section12" class="sp_none"><img src="img/btn_01_off.png" alt="無料カウンセリングのご予約" width="300" height="70" /></a><a href="#section12" class="pc_none"><img src="img/btn_01_sp.png" alt="無料カウンセリングのご予約"></a></dd>
			</dl>
		</div>
	</div>

</div>




<div id="section05" class="box">

	<article class="block10">
		<h2><img src="img/block10-txt.png" alt="よくある質問" width="1200" height="70" class="sp_none"><img src="img/block10-txt_sp.png" alt="よくある質問" class="pc_none"></h2>
		<div class="faq cf">
			<div class="fl">
				<dl>
					<dt><span>痛みはどのくらいありますか？</span></dt>
					<dd>麻酔を行いますので、手術中の痛みはほとんどありませんが、術後に少々痛みが出る場合がございます。</dd>
				</dl>
				<dl class="bg">
					<dt><span>皮膚はどのくらい切開するのですか？</span></dt>
					<dd>カニューレという脂肪吸引専用の非常に細い管を使いますので、切開部分はわずか5ミリほどです。目立たない場所で切開をするので、傷も目立ちません。切開部は脂肪吸引後に縫合され、約1週間後に抜糸をします。</dd>
				</dl>
				<dl>
					<dt><span>いつごろから効果は現れますか？</span></dt>
					<dd>皮下脂肪そのものを物理的に取り除いていく手術なので、その場で痩せていくのがご実感できるというのが脂肪吸引の特徴です。</dd>
				</dl>
			</div>
			<div class="fr">
				<dl class="bg">
					<dt><span>脂肪を吸引することで、皮膚がたるんだりしませんか？</span></dt>
					<dd>脂肪の取りすぎは皮膚のたるみの原因となります。ワイエススキンクリニックでは、表面にたるみや凸凹が出ないよう十分なデザイニングをしたうえで、おひとりおひとりに合った適切な量の脂肪を吸引いたします。</dd>
				</dl>
				<dl>
					<dt><span>ダウンタイムについて教えて下さい</span></dt>
					<dd>個人差はありますが、術後1～2週間ほどは痛みや腫れが続くことがございます。</dd>
				</dl>
				<dl class="bg">
					<dt><span>体にメスを入れたくないのですが、<br>他に脂肪を減らす方法はありますか？</span></dt>
					<dd>注射をするだけで脂肪を溶解して体外に排出させることができる、「脂肪溶解注射」という施術がございます。脂肪吸引のように1回の施術で著しい変化は見られませんが、繰り返すことで着実にダイエット効果が現れます。</dd>
				</dl>
			</div>
		</div>
	</article>

</div>




<div id="section06" class="box">

	<article class="block11 cf">
		<h2><img src="img/block11-txt.png" alt="お客さまの声" width="1200" height="70" class="sp_none"><img src="img/block11-txt_sp.png" alt="お客さまの声" class="pc_none"></h2>
		<div class="voice-box">
			<div class="block_l bg1">
				<div class="voice">
					<p class="voice1">美容治療は今回が初めてでしたが、スタッフの皆さんが温かい雰囲気で、とてもリラックスさせてくれたので、緊張せずにすみました。不安や疑問に思ったことも相談出来る環境です。アフターケアも充実していて大満足！</p>
				</div>
			</div>
			<div class="block_r bg2">
				<div class="voice">
					<p class="voice2">結婚式前なのでクリニックを受診。院内はとても明るく清潔でキレイで居心地が良かったです。やっぱり衛生管理がきちんとされている病院は安心出来ます。施術も痛みは少なく、あっという間で感激！</p>
				</div>
			</div>
		</div>
		<div class="voice-box">
			<div class="block_l bg2">
				<div class="voice">
					<p class="voice3">初めての経験で分らず不安でしたが、とても丁寧に相談にのって頂き大満足。カウンセリングに時間をかけて頂いたので、安心して治療を選択することが出来ました。今まで悩んでいたしみが改善して、化粧にかける時間も減ったのでうれしいです。</p>
				</div>
			</div>
			<div class="block_r bg1">
				<div class="voice">
					<p class="voice4">美容整形をするのは初めてだったのでとても不安だったのですが、納得がいくまでカウンセリングで説明していただいたので安心して施術に望むことができました。術後の相談も気軽にでき、頼れるクリニックです。</p>
				</div>
			</div>
		</div>
	</article>

	<div class="btn_box_out">
		<div class="btn_box cf">
			<div class="btn_camp">
				<p><a href="#section12" class="sp_none"><img src="img/campaign-small.png" alt="初回限定 脂肪吸引モニター価格 各料金50％OFF! モニターの内容はホームページ・雑誌・メディアへ掲載させていただきます。" width="554" height="91"></a><a href="#section12" class="pc_none"><img src="img/campaign-sp.png" alt="初回限定 脂肪吸引モニター価格 各料金50％OFF! モニターの内容はホームページ・雑誌・メディアへ掲載させていただきます。"></a></p>
			</div>
			<dl class="cf">
				<dt><img src="img/tel_01.png" alt="098-860-9980 受付時間 // 火・水・木・金・土 12：00～20:00　日・祝 10：00～19:00" width="260" height="70" class="sp_none"><a href="tel:0988609980" onclick="yahoo_report_conversion('tel:098-860-9980')" class="pc_none"><img src="img/tel_01_sp_01.png" alt="098-860-9980" width="500" height="103" class="pc_none"></a><img src="img/tel_01_sp_02.png" alt="受付時間 // 火・水・木・金・土 12：00～20:00　日・祝 10：00～19:00" class="pc_none"></dt>
				<dd><a href="#section12" class="sp_none"><img src="img/btn_01_off.png" alt="無料カウンセリングのご予約" width="300" height="70" /></a><a href="#section12" class="pc_none"><img src="img/btn_01_sp.png" alt="無料カウンセリングのご予約"></a></dd>
			</dl>
		</div>
	</div>

</div>




<div id="section07" class="box">

	<article class="block12">
		<h2><img src="img/block12-txt.png" alt="お問い合わせから施術までの流れ" width="1200" height="70" class="sp_none"><img src="img/block12-txt_sp.png" alt="お問い合わせから施術までの流れ" class="pc_none"></h2>
		<p class="text">電話、もしくはメールでご予約を承っております。<br class="pc_none">施術のご予約の前に、診療や施術、<br>
		その他について疑問がありましたら、<br class="pc_none">お電話またはお問い合わせフォーム、<br class="pc_none">メールからお気軽にお問い合わせください。<br>
		その際には匿名でのお問合せもOKです。</p>
		<div class="flow-box cf">
			<div class="read">
				<h3><img src="img/block12-txt_01.png" alt="STEP1 お電話、メールにてお問い合わせ" width="790" height="60" class="sp_none"><img src="img/block12-txt_01_sp.png" alt="STEP1 お電話、メールにてお問い合わせ" class="pc_none"></h3>
				<div class="subimg"><img src="img/block12-img_01_sp.jpg" alt=""></div>
				<p>電話、もしくはメールでご予約を承っております。<br>
				施術のご予約の前に、診療や施術、その他について疑問がありましたら、お電話またはお問い合わせフォーム、メールからお気軽にお問い合わせください。その際には匿名でのお問合せもOKです。</p>
				<p class="tel sp_none"><img src="img/block12-tel.png" alt="098-860-9980 受付時間 // 火・水・木・金・土 12：00～20:00　日・祝 10：00～19:00" width="400" height="50"><a href="#section12"><img src="img/block12-btn_off.png" alt="無料カウンセリングのご予約" width="305" height="50"></a></p>
				<p class="tel pc_none"><img src="img/block12-tel_sp.png" alt="098-860-9980 受付時間 // 火・水・木・金・土 12：00～20:00　日・祝 10：00～19:00"><a href="#section12"><img src="img/block12-btn_sp.png" alt="無料カウンセリングのご予約" width="430" height="95" /></a></p>
			</div>
			<div class="image"><img src="img/block12-img_01.jpg" alt="お電話、メールにてお問い合わせ" width="310" height="195"></div>
		</div>
		<div class="flow-box cf">
			<div class="read">
				<h3><img src="img/block12-txt_02.png" alt="STEP2 ご来院・受付" width="790" height="60" class="sp_none"><img src="img/block12-txt_02_sp.png" alt="STEP2 ご来院・受付" class="pc_none"></h3>
				<div class="subimg"><img src="img/block12-img_02_sp.jpg" alt=""></div>
				<p>お電話でご予約いただいた日程でご来院ください。（日程の変更などはお気軽にご連絡ください）<br>
				ご予約された日時にお越し下さい。<br>
				ワイエススキンクリニックでのカウンセリング方法など事前に何でも相談ください。</p>
			</div>
			<div class="image"><img src="img/block12-img_02.jpg" alt="ご来院・受付" width="310" height="195"></div>
		</div>
		<div class="flow-box cf">
			<div class="read">
				<h3><img src="img/block12-txt_03.png" alt="STEP3 スタッフとのカウンセリング" width="790" height="60" class="sp_none"><img src="img/block12-txt_03_sp.png" alt="STEP3 スタッフとのカウンセリング" class="pc_none"></h3>
				<div class="subimg"><img src="img/block12-img_03_sp.jpg" alt=""></div>
				<p>客様、患者様の悩みや疑問、施術費用やお支払方法ついてなど、専門のスタッフにお気軽に相談ください。<br>
				施術へのお手続き・施術申込書の記入、術後の注意事項をお渡しします。<br>
				未成年の方は同意書が必要になります。又はご両親のどちらかご同伴でお願いします。<br>
				※認印（シャチハタ以外）をお持ち下さい。</p>
			</div>
			<div class="image"><img src="img/block12-img_03.jpg" alt="スタッフとのカウンセリング" width="310" height="195"></div>
		</div>
		<div class="flow-box cf">
			<div class="read">
				<h3><img src="img/block12-txt_04.png" alt="STEP4 担当医師とのカウンセリング・診察" width="790" height="60" class="sp_none"><img src="img/block12-txt_04_sp.png" alt="STEP4 担当医師とのカウンセリング・診察" class="pc_none"></h3>
				<div class="subimg"><img src="img/block12-img_04_sp.jpg" alt=""></div>
				<p>実際に施術を行う担当医師が丁寧にご要望をうかがった上で、プラン、施術方法、施術内容のご説明をいたします。<br>
				※施術内容の詳細は担当医師がご説明いたします。ご不明な点があればお気軽にご相談ください。</p>
			</div>
			<div class="image"><img src="img/block12-img_04.jpg" alt="担当医師とのカウンセリング・診察" width="310" height="195"></div>
		</div>
		<div class="flow-box cf">
			<div class="read">
				<h3><img src="img/block12-txt_05.png" alt="STEP5 施術を行います" width="790" height="60" class="sp_none"><img src="img/block12-txt_05_sp.png" alt="STEP5 施術を行います" class="pc_none"></h3>
				<div class="subimg"><img src="img/block12-img_05_sp.jpg" alt=""></div>
				<p>お顔の施術の場合は、事前にメイクを落としていただく必要がございます。<br>
				化粧落としなどご用意しておりますので、お気軽にスタッフにお声掛けください。<br>
				準備ができましたら施術室に移動していただき、施術を行います。<br>
				リラックスして施術を受けてください。</p>
			</div>
			<div class="image"><img src="img/block12-img_05.jpg" alt="施術を行います" width="310" height="195"></div>
		</div>
		<div class="flow-box cf">
			<div class="read">
				<h3><img src="img/block12-txt_06.png" alt="STEP6 アフターケア" width="790" height="60" class="sp_none"><img src="img/block12-txt_06_sp.png" alt="STEP6 アフターケア" class="pc_none"></h3>
				<div class="subimg"><img src="img/block12-img_06_sp.jpg" alt=""></div>
				<p>施術後はすぐにご帰宅頂けます。<br>
				専用のメイクルームをご用意いたしておりますので、メイク直しをしながら安心して新しいご自分と対面していただけます。施術後のアフターケアの料金は手術料金に含まれております。<br>
				また、施術後しばらく経ってご不安な点や不明瞭な点などがございましたら、お気軽に電話、またはメールにて直接ご相談ください。</p>
			</div>
			<div class="image"><img src="img/block12-img_06.jpg" alt="アフターケア" width="310" height="195"></div>
		</div>
	</article>

	<div class="btn_box_out">
		<div class="btn_box cf">
			<div class="btn_camp">
				<p><a href="#section12" class="sp_none"><img src="img/campaign-small.png" alt="初回限定 脂肪吸引モニター価格 各料金50％OFF! モニターの内容はホームページ・雑誌・メディアへ掲載させていただきます。" width="554" height="91"></a><a href="#section12" class="pc_none"><img src="img/campaign-sp.png" alt="初回限定 脂肪吸引モニター価格 各料金50％OFF! モニターの内容はホームページ・雑誌・メディアへ掲載させていただきます。"></a></p>
			</div>
			<dl class="cf">
				<dt><img src="img/tel_01.png" alt="098-860-9980 受付時間 // 火・水・木・金・土 12：00～20:00　日・祝 10：00～19:00" width="260" height="70" class="sp_none"><a href="tel:0988609980" onclick="yahoo_report_conversion('tel:098-860-9980')" class="pc_none"><img src="img/tel_01_sp_01.png" alt="098-860-9980" width="500" height="103" class="pc_none"></a><img src="img/tel_01_sp_02.png" alt="受付時間 // 火・水・木・金・土 12：00～20:00　日・祝 10：00～19:00" class="pc_none"></dt>
				<dd><a href="#section12" class="sp_none"><img src="img/btn_01_off.png" alt="無料カウンセリングのご予約" width="300" height="70" /></a><a href="#section12" class="pc_none"><img src="img/btn_01_sp.png" alt="無料カウンセリングのご予約"></a></dd>
			</dl>
		</div>
	</div>

</div>




<div id="section08" class="box">

	<article class="block13 cf">
		<h2><img src="img/block13-txt.png" alt="院内ご案内" width="1200" height="70" class="sp_none"><img src="img/block13-txt_sp.png" alt="院内ご案内" class="pc_none"></h2>
		<p class="text">当院では安心してご利用頂ける万全の環境を整えておりますので、安心してお越しください。<br>
		皆さまのご来院をお待ちしております。</p>
		<div class="slider">
	        <div class="sp_none">
	            <ul class="bxslider cf">
	              <li><a href="img/slider01_big.jpg" rel="lightbox"><img src="img/slider01_sp.jpg" width="100%" alt="エントランス"></a></li>
	              <li><a href="img/slider02_big.jpg" rel="lightbox"><img src="img/slider02_sp.jpg" width="100%" alt="施術室"></a></li>
	              <li><a href="img/slider03_big.jpg" rel="lightbox"><img src="img/slider03_sp.jpg" width="100%" alt="受付"></a></li>
	              <li><a href="img/slider04_big.jpg" rel="lightbox"><img src="img/slider04_sp.jpg" width="100%" alt="対合室"></a></li>
	            </ul>
	        </div>
	        <div class="pc_none">
	            <ul class="bxslider cf">
	              <li><img src="img/slider01_sp.jpg" width="100%" alt="エントランス"></li>
	              <li><img src="img/slider02_sp.jpg" width="100%" alt="施術室"></li>
	              <li><img src="img/slider03_sp.jpg" width="100%" alt="受付"></li>
	              <li><img src="img/slider04_sp.jpg" width="100%" alt="対合室"></li>
	            </ul>
	        </div>
        </div>
	</article>

	<div class="btn_box_out">
		<div class="btn_box cf">
			<div class="btn_camp">
				<p><a href="#section12" class="sp_none"><img src="img/campaign-small.png" alt="初回限定 脂肪吸引モニター価格 各料金50％OFF! モニターの内容はホームページ・雑誌・メディアへ掲載させていただきます。" width="554" height="91"></a><a href="#section12" class="pc_none"><img src="img/campaign-sp.png" alt="初回限定 脂肪吸引モニター価格 各料金50％OFF! モニターの内容はホームページ・雑誌・メディアへ掲載させていただきます。"></a></p>
			</div>
			<dl class="cf">
				<dt><img src="img/tel_01.png" alt="098-860-9980 受付時間 // 火・水・木・金・土 12：00～20:00　日・祝 10：00～19:00" width="260" height="70" class="sp_none"><a href="tel:0988609980" onclick="yahoo_report_conversion('tel:098-860-9980')" class="pc_none"><img src="img/tel_01_sp_01.png" alt="098-860-9980" width="500" height="103" class="pc_none"></a><img src="img/tel_01_sp_02.png" alt="受付時間 // 火・水・木・金・土 12：00～20:00　日・祝 10：00～19:00" class="pc_none"></dt>
				<dd><a href="#section12" class="sp_none"><img src="img/btn_01_off.png" alt="無料カウンセリングのご予約" width="300" height="70" /></a><a href="#section12" class="pc_none"><img src="img/btn_01_sp.png" alt="無料カウンセリングのご予約"></a></dd>
			</dl>
		</div>
	</div>

</div>




<div id="section09" class="box">

	<article class="block14">
		<h2><img src="img/block14-txt.png" alt="ワイエススキンクリニックの理念" width="1200" height="70" class="sp_none"><img src="img/block14-txt_sp.png" alt="ワイエススキンクリニックの理念" class="pc_none"></h2>
		<p class="text">開業当初から当クリニックでは痛みや不安のない施術を心がけており、<br>
		患者様に安心して美しくなって頂くことを目指しております。</p>
		<div class="concept-box cf">
			<div class="read">
				<h3><img src="img/block14-img_01.png" alt="Concept1 患者様とのコミュニケーションを大切にしています。"></h3>
				<p>当クリニックは患者様の立場に立ち、今抱えている不安や悩みなどを解消するため、丁寧なカウンセリングを行なっています。</p>
			</div>
			<div class="read">
				<h3><img src="img/block14-img_02.png" alt="Concept2 長年の経験と実績による確かな技術と高い信頼"></h3>
				<p>当クリニックは優秀な美容皮膚科医が多数在籍しています。本当に安心して任せられるドクターがレベルの高い治療を行ないます。</p>
			</div>
			<div class="read rp0">
				<h3><img src="img/block14-img_03.png" alt="Concept3 いつもドクターがそばにいてくれるという感覚"></h3>
				<p>当クリニックは気軽に相談できる環境作りを心がけています。術後のケア、美肌づくりのアドバイスなどいつでもすぐにお応えいたします。</p>
			</div>
		</div>
	</article>

</div>




<div id="section10" class="box">
	<article class="block15 cf">
		<h2><img src="img/block15-txt.png" alt="医師のご紹介" width="1200" height="70" class="sp_none"><img src="img/block15-txt_sp.png" alt="医師のご紹介" class="pc_none"></h2>
		<p class="text">当クリニックの医師たちは、お客様に安全に美しくなって頂く為に、日々最高水準の技術を日々研究しております。<br>
		そんな医師たちを御紹介させていただきます。</p>
		<div class="doctor-box first cf">
			<h3><img src="img/block15-img_01.jpg" width="154" height="221" alt="総括医療部長 中西雄二"></h3>
			<div class="career">
				<h4>経歴</h4>
				<dl class="cf">
					<dt>1983年</dt>
					<dd>藤田保健衛生大学医学部卒</dd>
					<dt>1989年</dt>
					<dd>トヨタ記念病院形成外科部長</dd>
					<dt>1993年</dt>
					<dd>藤田保健衛生大学講師(形成外科) </dd>
					<dt>2000年</dt>
					<dd>慶應義塾大学助教授 (伊勢慶應病院形成外科) </dd>
					<dt>2002年</dt>
					<dd>第一なるみ病院形成外科部長大手美容外科院長・医療部長を経て</dd>
					<dt>2004年</dt>
					<dd>ヴェリテクリニック総院長 就任</dd>
					<dt>2015年</dt>
					<dd>各院の統括医療部長に就任</dd>
					<dt>現在</dt>
					<dd>藤田保健衛生大学形成外科　客員准教授</dd>
				</dl>
			</div>
			<div class="meeting">
				<h4>所属学会など</h4>
				<ul class="cf">
					<li class="w45">・日本形成外科学会評議員</li>
					<li class="w55">・日本美容外科学会(JSAPS)評議員</li>
					<li>・日本美容外科学会(JSAS)専門医</li>
					<li>・日本頭蓋顎顔面外科学会会員</li>
				</ul>
			</div>
			<div class="word">
				<h4>中西先生から一言</h4>
				<p>美容外科的手術だけではなく、アンチエイジング機器や薬剤、美肌に対するあらゆる施術について親切、丁寧に細かい部分のご質問にもご理解いただけるまでご説明するよう心がけています。形成外科学と美容外科学の最先端医療を取り入れ、インフォームドコンセントには十分に時間を取り、決して無理のない手術を行う事をモットーとしています。傷跡を含め専門的な知識のない施設で行われた手術に対しての修正術もお気軽にご相談ください。</p>
			</div>
		</div>
		<div class="doctor-box cf">
			<h3><img src="img/block15-img_02.jpg" width="154" height="203" alt="院長 佐藤美博"></h3>
			<div class="career">
				<h4>経歴</h4>
				<dl class="cf">
					<dt>平成15年3月</dt>
					<dd>杏林大学医学部医学科　卒業</dd>
					<dt>平成15年4月</dt>
					<dd>自治医科大学付属病院一般内科勤務</dd>
					<dt>平成18年10月</dt>
					<dd>ワイエス美容外科クリニック勤務</dd>
					<dt>平成19年1月</dt>
					<dd>横浜ワイエスクリニック開設</dd>
					<dt>平成20年1月</dt>
					<dd>メディクスクリニック溝の口勤務</dd>
					<dt>平成21年2月</dt>
					<dd>ファミリークリニック蒲田勤務</dd>
					<dt>平成21年11月</dt>
					<dd>表参道スキンクリニック勤務</dd>
					<dt>平成23年10月</dt>
					<dd>横浜銀座クリニック勤務</dd>
					<dt>平成25年4月</dt>
					<dd>大手美容外科　勤務</dd>
					<dt>平成27年1月</dt>
					<dd>ワイエススキンクリニック開設（沖縄県）</dd>
				</dl>
			</div>
			<div class="meeting">
				<h4>所属学会など</h4>
				<ul class="cf">
					<li>・日本皮膚科学会</li>
					<li>・美容皮膚科学会</li>
					<li>・日本抗加齢医学会</li>
				</ul>
			</div>
			<div class="word">
				<h4>佐藤先生から一言</h4>
				<p>どんなささいな悩みでも、悩んでいる本人にとっては大きなコンプレックス。自信をもって前向きに過ごせるお手伝いができるよう一人一人丁寧な診察を心掛けています。私自身も美容の治療は大好きでいろんな治療を試していますので、ぜひ色々質問してください。</p>
			</div>
		</div>
	</article>

	<div class="btn_box_out">
		<div class="btn_box cf">
			<div class="btn_camp">
				<p><a href="#section12" class="sp_none"><img src="img/campaign-small.png" alt="初回限定 脂肪吸引モニター価格 各料金50％OFF! モニターの内容はホームページ・雑誌・メディアへ掲載させていただきます。" width="554" height="91"></a><a href="#section12" class="pc_none"><img src="img/campaign-sp.png" alt="初回限定 脂肪吸引モニター価格 各料金50％OFF! モニターの内容はホームページ・雑誌・メディアへ掲載させていただきます。"></a></p>
				<p>※モニター掲載は、ホームページやDMなどの限られた媒体で、施術箇所のみです。顔などは一切掲載されません。</p>
			</div>
			<dl class="cf">
				<dt><img src="img/tel_01.png" alt="098-860-9980 受付時間 // 火・水・木・金・土 12：00～20:00　日・祝 10：00～19:00" width="260" height="70" class="sp_none"><a href="tel:0988609980" onclick="yahoo_report_conversion('tel:098-860-9980')" class="pc_none"><img src="img/tel_01_sp_01.png" alt="098-860-9980" width="500" height="103" class="pc_none"></a><img src="img/tel_01_sp_02.png" alt="受付時間 // 火・水・木・金・土 12：00～20:00　日・祝 10：00～19:00" class="pc_none"></dt>
				<dd><a href="#section12" class="sp_none"><img src="img/btn_01_off.png" alt="無料カウンセリングのご予約" width="300" height="70" /></a><a href="#section12" class="pc_none"><img src="img/btn_01_sp.png" alt="無料カウンセリングのご予約"></a></dd>
			</dl>
		</div>
	</div>

</div>




<div id="section11" class="box">

	<article class="block16 cf">
		<a name="access" id="access"></a><h2><img src="img/block16-txt.png" alt="アクセス" width="1200" height="70" class="sp_none"><img src="img/block16-txt_sp.png" alt="アクセス" class="pc_none"></h2>
		<p class="text">〒900-0014  沖縄県那覇市松尾2-8-19 <br class="pc_none">ドンキホーテ国際通り店ビル5F</p>
		<div class="access-box"><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d447.4256814349776!2d127.68779722883606!3d26.216012317045966!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x34e5697a0d58da6b%3A0x7e2a95600db65086!2z44CSOTAwLTAwMTQg5rKW57iE55yM6YKj6KaH5biC5p2-5bC-77yS5LiB55uu77yY4oiS77yR77yZ!5e0!3m2!1sja!2sjp!4v1431422219925" width="100%" height="350" frameborder="0" style="border:0"></iframe></div>
		<div class="access-box cf">
			<div class="read">
				<h3><img src="img/block16-icon_01.png" alt="電車でお越しの方"></h3>
				<p>・牧志駅から徒歩7分<br>
				・美栄橋駅から徒歩7分</p>
			</div>
			<div class="read">
				<h3><img src="img/block16-icon_02.png" alt="お車でお越しの方"></h3>
				<p>提携駐車場は<br>
				・カナン駐車場<br>
				・てんぶす地下駐車場<br>
				の2箇所です(一時間分無料)。</p>
			</div>
			<div class="read">
				<h3><img src="img/block16-icon_03.png" alt="治療時間・休診日"></h3>
				<dl class="cf">
					<dt>診療時間</dt>
					<dd>平日:12:00～20:00(完全予約制)<br>
					日祝:11:00～19:00(完全予約制)</dd>
					<dt>休診日</dt>
					<dd>月曜日</dd>
				</dl>
			</div>
		</div>
	</article>

	<div class="btn_box_out">
		<div class="btn_box cf">
			<div class="btn_camp">
				<p><a href="#section12" class="sp_none"><img src="img/campaign-small.png" alt="初回限定 脂肪吸引モニター価格 各料金50％OFF! モニターの内容はホームページ・雑誌・メディアへ掲載させていただきます。" width="554" height="91"></a><a href="#section12" class="pc_none"><img src="img/campaign-sp.png" alt="初回限定 脂肪吸引モニター価格 各料金50％OFF! モニターの内容はホームページ・雑誌・メディアへ掲載させていただきます。"></a></p>
				<p>※モニター掲載は、ホームページやDMなどの限られた媒体で、施術箇所のみです。顔などは一切掲載されません。</p>
			</div>
			<dl class="cf">
				<dt><img src="img/tel_01.png" alt="098-860-9980 受付時間 // 火・水・木・金・土 12：00～20:00　日・祝 10：00～19:00" width="260" height="70" class="sp_none"><a href="tel:0988609980" onclick="yahoo_report_conversion('tel:098-860-9980')" class="pc_none"><img src="img/tel_01_sp_01.png" alt="098-860-9980" width="500" height="103" class="pc_none"></a><img src="img/tel_01_sp_02.png" alt="受付時間 // 火・水・木・金・土 12：00～20:00　日・祝 10：00～19:00" class="pc_none"></dt>
				<dd><a href="#section12" class="sp_none"><img src="img/btn_01_off.png" alt="無料カウンセリングのご予約" width="300" height="70" /></a><a href="#section12" class="pc_none"><img src="img/btn_01_sp.png" alt="無料カウンセリングのご予約"></a></dd>
			</dl>
		</div>
	</div>

</div>




<!--フォーム導入-->
<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/smoothness/jquery-ui.css" >
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1/i18n/jquery.ui.datepicker-ja.min.js"></script>
<script>
  $(function() {
    $("#datepicker_1").datepicker({
      dateFormat: 'yy年mm月dd日 (DD)',
      dayNames: ['日', '月', '火', '水', '木', '金', '土']
    });
    $("#datepicker_2").datepicker({
      dateFormat: 'yy年mm月dd日 (DD)',
      dayNames: ['日', '月', '火', '水', '木', '金', '土']
    });
  });
</script>

<div id="section12" name="section12" class="box">

	<article class="block17 cf">
		<h2><img src="img/block17-txt.png" alt="無料カウンセリングご予約フォーム" width="1200" height="70" class="sp_none"><img src="img/block17-txt_sp.png" alt="無料カウンセリングご予約フォーム" class="pc_none"></h2>
		<p class="flow"><img src="img/block17-flow_01.png" alt="1．情報の入力" width="1000" height="63" class="sp_none"><img src="img/block17-flow_sp_01.png" alt="1．情報の入力" class="pc_none"></p>

		<div class="form">

			<p class="caution">※お手数ですが、必須項目をすべてご入力の上、確認画面へお進みください。</p>

			<form method="post">

          <table>
            <tr>
              <th class="hissu">お名前</th>
              <td>
                <input type="text" name="name_req" value="<?php echo $formTool->h($_POST['name_req']); ?>" placeholder="例）表参道花子">
                <?php if( !empty($formTool->messages['name_req']) ) : ?><p class="error"><?php echo $formTool->h($formTool->messages['name_req']); ?></p><?php endif; ?>
              </td>
            </tr>
            <tr>
              <th class="hissu">電話番号</th>
              <td>
                <input type="tel" name="tel_req" value="<?php echo $formTool->h($_POST['tel_req']); ?>" placeholder="例）01-2345-6789">
                <?php if( !empty($formTool->messages['tel_req']) ) : ?><p class="error"><?php echo $formTool->h($formTool->messages['tel_req']); ?></p><?php endif; ?>
              </td>
            </tr>
            <tr>
              <th class="hissu">メールアドレス</th>
              <td>
                <input type="email" name="email_req" value="<?php echo $formTool->h($_POST['email_req']); ?>" placeholder="例）info@abc.com">
                <?php if( !empty($formTool->messages['email_req']) ) : ?>
                  <p class="error"><?php echo $formTool->h($formTool->messages['email_req']); ?></p>
                <?php endif; ?>
              </td>
            </tr>
            <tr>
              <th class="nini">相談日 第一希望</th>
              <td>
                <input type="text" name="day1" value="<?php echo $formTool->h($_POST['day1']); ?>" placeholder="YYYY年MM月DD日" class="calendar" id="datepicker_1" readonly="readonly"/>
                <select name="day1_period">
                  <option value="">時間帯を選択</option>
                  <?php echo $formTool->makeOptionHtml($formTool->timeArr, 'array', $_POST['day1_period']); ?>
                </select>
              </td>
            </tr>
            <tr>
              <th class="nini">相談日 第二希望</th>
              <td>
                <input type="text" name="day2" value="<?php echo $formTool->h($_POST['day2']); ?>" placeholder="YYYY年MM月DD日" class="calendar" id="datepicker_2" readonly="readonly"/>
                <select name="day2_period">
                  <option value="">時間帯を選択</option>
                  <?php echo $formTool->makeOptionHtml($formTool->timeArr, 'array', $_POST['day2_period']); ?>
                </select>
              </td>
            </tr>
            <tr>
              <th class="nini">連絡方法のご希望</th>
              <td class="radiostyle">
                <?php echo $formTool->makeCheckBoxHtml($formTool->typeArr, 'array', $_POST['type'], 'type'); ?>
              </td>
            </tr>
            <tr>
              <th class="nini">お問い合わせ内容</th>
              <td class="last">
                <textarea name="message" placeholder="お問い合わせ内容をご記入ください。" cols="20" rows="5"><?php echo $formTool->h($_POST['message']); ?></textarea>
              </td>
            </tr>
          </tbody>
        </table>

        <input type="hidden" name="mode" value="conf" />
        <p class="btn"><input type="image" src="img/block17-btn_off.png" alt="確認画面へ" class="sp_none"><input type="image" src="img/block17-btn_sp.png" alt="確認画面へ" class="pc_none"></p>

		</form>
		</div>

	</article>

</div>
<!--フォーム導入-->




</div>



</div>