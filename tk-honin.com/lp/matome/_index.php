<?php
require('formClass.php');
$formTool = new formClass();
$mode = (empty($_POST['mode'])) ? "" : $_POST['mode'] ;
$error = false;
if($mode == 'comp') {
  $error = $formTool->is_inputerror();
  //完了画面のリロードで再送させないようにリダイレクト
  if(!$error){
    $formTool->send_return_mail();
    $formTool->send_admin_mail();
    header('Location: ./index_end.html');
  }
} else if ($mode == 'conf') {
  $error = $formTool->is_inputerror();
}
?>
<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="UTF-8">
  <title>表参道スキンクリニック</title>
  <meta name="keywords" content="表参道スキンクリニック">
  <meta name="description" content="表参道スキンクリニック">
  <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0">

  <link rel="stylesheet" type="text/css" href="./css/common.css" media="screen,print">
  <link rel="stylesheet" type="text/css" href="./css/style.css" media="screen,print">
  <link rel="stylesheet" type="text/css" href="./css/style_sp.css" media="screen,print">
  <link rel="stylesheet" type="text/css" href="./css/jquery.bxslider.css" media="screen,print">
  <link rel="stylesheet" type="text/css" href="./css/lightbox.css" media="screen,print">
  <link rel="stylesheet" type="text/css" href="./css/slider-pro.css" media="all" />


  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
  <script src="./js/smooth.pack.js"></script>
  <script src="./js/rollover.js"></script>
  <script src="./js/jquery.bxslider.min.js"></script>
  <script type="text/javascript" src="./js/lightbox.js"></script>
  <script>
  $(document).ready(function(){
    $('.bxslider').bxSlider({
      auto: true,
      pause: 5000,
      speed: 1000,
      pager: false,
      moveSlideQty: 1,
      minSlides: 1
    });
  });
  </script>
<?php /*
<script type="text/javascript" src="js/jquery.sliderPro.js"></script>
	<script>
	$(document).ready(function(){
		var sW = window.innerWidth;
		if(sW > 799) {
			$('.slider1').sliderPro({
				width: "40%",
				aspectRatio: 2,
				arrows: true,
				buttons: true,
				autoplay: true,
				loop: true,
				visibleSize: '100%',
				//forceSize: 'fullWidth',
				autoHeight:true,
				keyboardOnlyOnFocus:true
			});
		} else {
			$('.slider1').sliderPro({
				width: "100%",
				autoHeight:true,
				loop: true
			});
		}
		$( '.sp-image' ).parent( 'a' ).on( 'click', function( event ) {
			window.open().location.href = event.target.parentNode.getAttribute("href");

		});
	});
	</script>
 */ ?>
  <!--[if (gte IE 6)&(lte IE 8)]>
  <script src="/js/selectivizr-min.js"></script>
  <![endif]-->
  <!-- HTML5 IEハック IE7までOK -->
  <!--[if lt IE 9]>
  <script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE9.js"></script>
  <script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/ie7-squish.js"></script>
  <![endif]-->


<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-59451323-1', 'auto');
  ga('send', 'pageview');

</script>

</head>
<body>




<div id="wrapper">












    <div id="h_wrapper">
      <header class="sp_none" id="PAGE_TOP">
        <h1><a href="http://tk-honin.com/"><img src="img/header_logo.png" width="249" height="45" alt="表参道スキンクリニック　Omotesando Skin Clinic"></a></h1>
        <p><img src="img/header_tel.png" width="430" height="40" alt="0120-334-270 受付時間11:00～20:00・日祝10:30～18:00/水曜休診" class="sp_none"><a href="tel:0120334270" onclick="javascript:goog_report_conversion('tel:0120-334-270');yahoo_report_conversion(undefined);ga('send','event','sp','tel-tap','tel');return false;" class="pc_none"><img src="img/header_tel_sp.png" width="100%" alt="0120-334-270 受付時間11:00～20:00・日祝10:30～18:00/水曜休診"></a></p>
        <ul class="cf">
          <li class="sp_right"><a href="#section09"><img src="img/header_btn_01.gif" width="249" height="40" alt="無料カウンセリングのご予約" class="sp_none"><img src="img/header_btn01_sp.gif" width="100%" alt="無料カウンセリングのご予約" class="pc_none"></a></li>
          <li class="sp_ab"><a href="#section08"><img src="img/header_btn02_off.gif" width="112" height="40" alt="アクセス" class="sp_none"><img src="img/header_btn02_sp.gif" width="100%" alt="アクセス" class="pc_none"></a></li>
        </ul>
      </header>
		<div class="pc_none" id="sp_header">
			<div id="logo" class="cf">
			<a href="http://tk-honin.com/"><img src="img/sp_header_logo.png" width="100%" alt="表参道スキンクリニック" style="width:47%;float:left;"></a><a href="tel:0120334270" onClick="yahoo_report_conversion('tel:0120-334-270')"><img src="img/sp_header_tel.png" width="100%" alt="表参道スキンクリニック0120-334-270 受付時間11:00～20:00・日祝10:30～18:00/水曜休診" style="width:53%;float:right;"></a>
			</div>
			<div class="bdbm cf">
				<div class="header_tel">
					<a href="#section09"><img src="img/header_tel_sp.gif" width="100%" alt="無料カウンセリングのご予約" ></a>
				</div>
				<div class="btn">
					<a href="#section08"><img src="img/header_btn_sp.gif" width="100%" alt="アクセス" ></a>
				</div>
			</div>
		</div>
    </div>




    <?php if(basename($_SERVER['REQUEST_URI']) == 'comp.html') : ?>
      <?php include('comp.php'); ?>
    <?php elseif($_POST['mode'] == 'conf' && !$error) : ?>
      <?php include('conf.php'); ?>
    <?php else : ?>
      <?php include('edit.php'); ?>
      <?php if($error) : ?>
        <script type="text/javascript">
        window.location.hash = 'section12';
        </script>
      <?php endif; ?>
    <?php endif; ?>





<div id="f_wrapper">
	<p class="tc sp_none text">表参道スキンクリニックは、<br>どこまでも綺麗への想いにお応えし続けます</p>
	<p class="tc pc_none text">表参道スキンクリニックは、<br>どこまでも綺麗への想いに<br>お応えし続けます</p>
	<footer>
		<p>Copyright &copy; TOKYO COSMETIC SERGERY . All Rights Reserved.</p>
	</footer>
</div>
<p id="page-top"><a href="#wrapper"><img src="img/top.png" width="50" height="50" alt="TOP"></a></p>






</div>




<script src="./js/custom.js"></script>

<!-- Yahoo Code for your Conversion Page
In your html page, add the snippet and call
yahoo_report_conversion when someone clicks on the
phone number link or button. -->
<script type="text/javascript">
  /* <![CDATA[ */
  yahoo_snippet_vars = function() {
    var w = window;
    w.yahoo_conversion_id = 1000234916;
    w.yahoo_conversion_label = "vqUdCPGril8QvuiIwwM";
    w.yahoo_conversion_value = 0;
    w.yahoo_remarketing_only = false;
  }
  // IF YOU CHANGE THE CODE BELOW, THIS CONVERSION TAG MAY NOT WORK.
  yahoo_report_conversion = function(url) {
    yahoo_snippet_vars();
    window.yahoo_conversion_format = "3";
    window.yahoo_is_call = true;
    var opt = new Object();
    opt.onload_callback = function() {
      if (typeof(url) != 'undefined') {
        window.location = url;
      }
    }
    var conv_handler = window['yahoo_trackConversion'];
    if (typeof(conv_handler) == 'function') {
      conv_handler(opt);
    }
  }
/* ]]> */
</script>
<script type="text/javascript"
  src="http://i.yimg.jp/images/listing/tool/cv/conversion_async.js">
</script>




</body>
</html>