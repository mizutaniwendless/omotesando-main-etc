<?php
global $formTool;
?>

<div class="cnt_wp">
    <ul id="navi">
        <li><a href="#section01" class="on">●</a></li>
        <li><a href="#block06">●</a></li>
        <li><a href="#section03">●</a></li>
        <li><a href="#section04">●</a></li>
        <li><a href="#section05">●</a></li>
        <li><a href="#section06">●</a></li>
        <li><a href="#section07">●</a></li>
        <li><a href="#section08">●</a></li>
        <li><a href="#section09">●</a></li>
    </ul>

<div id="section01" class="box">
    <article class="block01">
        <h2><img src="img/main_img01.jpg" width="1200" height="549" alt="ヒアルロン酸豊胸・シリコンバッグ豊胸　理想的なバストを手に入れてコンプレックスを一掃　思い通りのサイズ、形を手に入れて、理想のバストへ" class="sp_none"><img src="img/main_img01_sp.jpg" width="100%" alt="ヒアルロン酸豊胸・シリコンバッグ豊胸　理想的なバストを手に入れてコンプレックスを一掃　思い通りのサイズ、形を手に入れて、理想のバストへ" class="pc_none"></h2>
    </article>


    <article class="block02">
        <h2><img src="img/main_img02.jpg" width="1200" height="551" alt="バストをボリュームアップさせたい　乳頭、乳輪を小さくしたい　左右のバストの大きさが違う　乳頭の黒ずみに悩んでいる" class="sp_none"><img src="img/main_img02_sp.jpg" width="100%" alt="バストをボリュームアップさせたい　乳頭、乳輪を小さくしたい　左右のバストの大きさが違う　乳頭の黒ずみに悩んでいる" class="pc_none"></h2>
    </article>
</div>




<div id="section02" class="box">
	<article class="block05">
    	<div class="cf">
            <h2><img src="img/title01.gif" width="1112" height="105" alt="当院では人気のある２パターンの施術方法をご紹介します。" class="sp_none"><img src="img/title01_sp.gif" width="100%" alt="当院では人気のある２パターンの施術方法をご紹介します。" class="pc_none"></h2>
            <p class="b05text">各施術方法にそれぞれ特徴がありますので、ご希望にあった施術をお選びください。
</p>
            <div>
                <div class="left">
                    <p><a href="#block06"><img src="img/block5_pic01_off.jpg" width="574" height="220" alt="ヒアルロン酸豊胸施術"></a></p>
                    <p class="text"><span><img src="img/merit.gif" width="64" height="22" alt="メリット"></span>身体に安全<br class="pc_none"><span><img src="img/demerit.gif" width="64" height="22" alt="デメリット"></span>永久的に持続しない</p>
                </div>
                <div class="right">
                    <p><a href="#section03"><img src="img/block5_pic02_off.jpg" width="574" height="220" alt="シリコンバッグ豊胸施術"></a></p>
                    <p class="text"><span><img src="img/merit.gif" width="64" height="22" alt="メリット"></span>半永久的に持続<br class="pc_none"><span><img src="img/demerit.gif" width="64" height="22" alt="デメリット"></span>体にメスを入れため傷跡つく</p>
                </div>
            </div>
    	</div>
    </article>
<a id="block06"></a>
	<article class="block06 cf">
    	<h2><img src="img/title03.gif" width="387" height="104" alt="ヒアルロン酸豊胸施術" class="sp_none"><img src="img/title03_sp.gif" width="100%" alt="ヒアルロン酸豊胸施術" class="pc_none"></h2>

        <div class="cf b6_box01">
	        <h3><img src="img/block06_midasi01.gif" width="1038" height="53" alt="注射器を使ってヒアルロン酸を注入。傷跡を残したくない方向け" class="sp_none"><img src="img/block06_midasi01_sp.gif" width="100%" alt="注射器を使ってヒアルロン酸を注入。傷跡を残したくない方向け" class="pc_none"></h3>
            <img src="img/block06_pic01.jpg" width="365" height="170" alt="" class="left">
            <p>「バストアップをしたいけれど、体にメスを入れるのはちょっと……」という方や、「週末の結婚パーティでドレスアップしたい」といった急なご要望にもお応えできる豊胸術が、ヒアルロン酸注入です。</p>
            <p>注射器を使ってバスト部にヒアルロン酸を注入するため、皮膚に傷跡が残らないというのが本施術の最大のメリットです。</p>
            <p>部分注入も可能なため、「左右の乳房のバランスを整えたい」「谷間だけがほしい」といった方にも最適です。</p>
            <h3><img src="img/block06_midasi02.gif" width="1039" height="55" alt="ヒアルロン酸は世界トップメーカーが開発したマクロレイン・サブＱを使用" class="sp_none"><img src="img/block06_midasi02_sp.gif" width="100%" alt="ヒアルロン酸は世界トップメーカーが開発したマクロレイン・サブＱを使用" class="pc_none"></h3>
            <img src="img/block06_pic02.png" width="358" height="125" alt="スウェーデン・Ｑ－ＭＥＤ社の「マクロレイン－サブＱ」" class="right sp_none"><img src="img/block06_pic02_sp.gif" width="100%" alt="スウェーデン・Ｑ－ＭＥＤ社の「マクロレイン－サブＱ」" class="pc_none right_sp">
            <p>当院が使用するヒアルロン酸は世界のトップメーカー、スウェーデン・Q‐MED社の「マクロレイン‐サブQ」。</p>
            <p>サブQはこれまでに鼻やアゴなど、おもに顔の輪郭形成に用いられてきましたが、本品は豊胸用に新たに開発されたもの。<br>従来のヒアルロン酸よりも粒子が大きく濃度も3倍になったことで、ハリ感や持続時間が大幅に向上しました。</p>
            <p>感触も非常に軟らかく、普通のバストと比べても違和感がありません。一度の注入で、約2年間効果が続きます。</p>
        </div>
        <h4 class="sp_mb"><img src="img/block06_midasi04.gif" width="1199" height="28" alt="よくある質問" class="sp_none"><img src="img/block06_midasi04_sp.gif" width="100%" alt="よくある質問" class="pc_none"></h4>
        <div class="situmon cf">
            <div class="s_box">
                <p class="situmon_q"><img src="img/block06_q01.gif" width="438" height="21" alt="ヒアルロン酸とはどのようなものですか？"></p>
                <p class="situmon_a ml15">人間の体にも存在するムコ多糖類の一種で、それを精製した物質がシワの解消や涙袋形成といったプチ整形に多く用いられています。<br>
        ヒアルロン酸はきわめて安全性が高く、人体に注入しても違和感やアレルギーの心配はほぼありません。お顔に使用する場合は粒子の細かいものを使用しますが、豊胸の場合は、ハリや持続時間を考えると、粒子が粗いものの方が適しているといえます。</p>
            </div>

<hr style="border-top:1px dotted #c5c5c5; margin-bottom:15px;" size="1" class="pc_none">

            <div class="s_box">
                <p class="situmon_q ml30"><img src="img/block06_q02.gif" width="561" height="21" alt="一回にどのくらいの量を注入することができますか？" class="sp_none"><img src="img/block06_q02_sp.gif" width="100%" alt="一回にどのくらいの量を注入することができますか？" class="pc_none"></p>
                <p class="situmon_a ml45">片側で40～60ccくらいが目安となります。100cc以上の量を希望される方は、数回に分けて注入する場合もあります。<br>
        ただし、ヒアルロン酸注入による豊胸術の場合、注入のしすぎはバスト下垂の原因となりますので、極端なバストアップは難しいとお考えください。
    </p>
            </div>
<hr style="border-top:1px dotted #c5c5c5; margin-bottom:15px;" size="1" class="pc_none">
            <div class="s_box">
                <p class="situmon_q ml30"><img src="img/block06_q03.gif" width="410" height="22" alt="妊娠や出産（授乳）に影響はありますか？"></p>
                <p class="situmon_a ml45">注入によって乳腺を傷つけることはないので、とくにこれらへの支障はございません。</p>
            </div>
<hr style="border-top:1px dotted #c5c5c5; margin-bottom:15px;" size="1" class="pc_none">

            <div class="s_box">
                <p class="situmon_q"><img src="img/block06_q04.gif" width="373" height="22" alt="施術後のマッサージは必要ですか？"></p>
                <p class="situmon_a ml15">基本的にアフターケアの必要はなく、マッサージも行わなくて大丈夫です。<br>施術後の圧迫や固定なども不要となっております。</p>
            </div>
        </div>
        <h4><img src="img/block06_midasi004.gif" width="1199" height="29" alt="ヒアルロン酸豊胸料金" class="sp_none"><img src="img/block06_midasi0402_sp.gif" width="100%" alt="ヒアルロン酸豊胸料金" class="pc_none b6_h401"></h4>
    </article>





<div class="btn_box2 cf sp_none">
    	<div class="right">
            <h2><img src="img/box_title.png" width="606" height="110" alt="「傷跡の残らない」ヒアルロン酸豊胸"></h2>
            <p><img src="img/box_text_r.gif" width="479" height="58" alt="キャンペーン価格　通常価格1cc 5,250円 1cc　2,100～円"></p>
            <dl class="box cf">
                <dt><img src="img/box_tel.gif" width="284" height="74" alt="0120-334-270 受付時間//11:00～20:00" class="sp_none"></dt>
                <dd><a href="#section09"><img src="img/box_btn_on.jpg" style="position: absolute; opacity: 0;"><img src="img/box_btn_off.jpg" width="423" height="69" alt="無料カウンセリング予約"></a></dd>
            </dl>
        </div>
        <div class="left">
        	<img src="img/box_logo.gif" width="193" height="35" alt="表参道スキンクリニック">
            <img src="img/box_img.png" width="175" height="211" alt="[ドクター]友利　新">
        </div>
    </div>
<div class="btn_box2 pc_none">
    	<p class="logo"><img src="img/box_logo_sp.gif" width="100%" alt="「傷跡の残らない」ヒアルロン酸豊胸"></p>
    	<p class="title"><img src="img/box_title_sp.gif" width="100%" alt="表参道スキンクリニック"></p>
        <ul class="cf">
    		<li class="left"><img src="img/box_img_sp.jpg" width="100%" alt="[ドクター]友利　新"></li>
    		<li class="right"><img src="img/box_text_sp_r.gif" width="100%" alt="キャンペーン価格　通常価格1cc 5,250円 1cc　2,100～円" class="pt_15"></li>
        </ul>
    	<div class="tel_box">
            <p class="tel"><a href="tel:0120334270" onclick="yahoo_report_conversion('tel:0120-334-270')" class="pc_none"><img src="img/box_tel_sp.gif" width="100%" alt="0120-334-270 受付時間//11:00～20:00"></a></p>
            <a href="#section09"><img src="img/box_btn_sp.gif" width="100%" alt="無料カウンセリング予約"></a>
        </div>
    </div>


</div>

<div id="section03" class="box">
	<article class="block07 cf">
    	<h2><img src="img/title04.gif" width="410" height="105" alt="シリコンバッグ豊胸施術" class="sp_none"><img src="img/title04_sp.gif" width="100%" alt="シリコンバッグ豊胸施術" class="pc_none"></h2>
        <div class="cf">
            <img src="img/block07_pic01.jpg" width="365" height="170" alt="" class="right">
            <p>シリコン製のバッグを体内に埋入することによって、バストを豊かにする手術です。
    シリコンバッグによる豊胸手術の一番の課題は、バッグ挿入のための「切開」を行う場所です。</p>
            <p>従来は乳輪切開、乳房下切開が主流でしたが、どうしても傷跡が目立ちやすくなるため、当院では脇の下で切開を行う「腋下切開」を採用しております。
    </p>
            <p>さらに、傷跡自体を目立たなくさせる技術の確立により、時間とともに切開部はほとんど分からなくなります。 </p>
        </div>
        <div class="cf">
       	  <div class="left_box sp_none">
            <p class="text07_01">従来の術法では大胸筋の内側にバックを挿入していました。</p>
            <p class="text07_02">最新の術法では、大胸筋の外側にバックを挿入することにより腫れを抑えて自然な仕上がりにします。</p>
                <p class="text07_03">大胸筋膜内に入れることにより、より自然な仕上がりができます。また、型崩れが出来にくく人気のある施術です。</p>
            </div>
          <img src="img/block07_pic02_sp.gif" width="100%" alt="大胸筋下法の場合　乳腺下法の場合" class="pc_none">
            <p>局所麻酔と静脈麻酔を併用し、ほとんど痛みを感じることのない状態で施術を行います。<br>まず脇の下を最小限に切開し、シリコンバッグを挿入したら、切開部を丁寧に縫合します。</p>
            <p>当院では、バッグを挿入する位置にもこだわっており、従来の大胸筋下ではなく、大胸筋の外側にある筋膜部に挿入する「筋膜下法」を採用。</p>
            <p>これは筋肉の伸縮によるバッグの型崩れを防ぐとともに、乳腺損傷の心配もクリアにした画期的な挿入方法です。</p>
        </div>
        <div class="cf b7_box01">
            <h4><img src="img/block07_midasi01_sp.gif" width="886" height="54" alt="当院はEUROSILICON社認定院" class="pc_none"></h4>
            <img src="img/block07_pic03.jpg" width="116" height="168" alt="" class="left">
            <h4><img src="img/block07_midasi01.gif" width="886" height="54" alt="当院はEUROSILICON社認定院" class="sp_none"></h4>
            <p>EUROSILICON社製のバッグは、アメリカ厚生省（FDA）の認可を受けている安全性の高いバッグです。</p>
            <p>安全性と徹底研究の結果、9層にも及ぶ被膜精製とバリアーコートにより、従来のバックに比べて抜群の耐久性が特徴です。<br>また新技術によるシリコンはその柔軟特性により、感触はもとより外観の仕上がりも良好です。
</p>
        </div>
        <h3><img src="img/block07_midasi03.gif" width="1199" height="28" alt="よくあるご質問" class="sp_none"><img src="img/block07_midasi03_sp.gif" width="100%" alt="よくあるご質問" class="pc_none"></h3>
        <div class="situmon cf">
            <div class="s_box">
                <p class="situmon_q ml45"><img src="img/block07_q01.gif" width="551" height="23" alt="挿入するシリコンバッグは、どのようなものですか？" class="sp_none"><img src="img/block07_q01_sp.gif" width="100%" alt="挿入するシリコンバッグは、どのようなものですか？" class="pc_none"></p>
                <p class="situmon_a ml30">シリコンとはケイ素を元につくられた人口化合物で、医薬品や化粧品などに多く使われています。表参道スキンクリニックでは、FDA（米国食品医薬品局。日本の厚生労働省にあたる機関）に認可された「バイオセル」「メモリージェル」をはじめ、国内で使用することのできる安全性の高いシリコンバッグをほぼ全種類揃えており、おひとりおひとりに合った最適な方法を提案させていただいております。</p>
            </div>
<hr style="border-top:1px dotted #c5c5c5; margin-bottom:15px;" size="1" class="pc_none">
            <div class="s_box">
                <p class="situmon_q ml15"><img src="img/block07_q02.gif" width="419" height="21" alt="感触や見た目が不自然になりませんか？" class="sp_none"><img src="img/block07_q02_sp.gif" width="100%" alt="感触や見た目が不自然になりませんか？" class="pc_none"></p>
                <p class="situmon_a">安全性と仕上がりにこだわる表参道スキンクリニックでは、特別オーダーのシリコンバッグを採用しております。
手で触れると温かく、非常に軟らかいため、見た目も感触もナチュラルなバストを形成することが可能となります。
ぜひ、カウンセリング時に感触を実感してください。</p>
            </div>
<hr style="border-top:1px dotted #c5c5c5; margin-bottom:15px;" size="1" class="pc_none">
            <div class="s_box">
                <p class="situmon_q ml15"><img src="img/block07_q03.gif" width="543" height="21" alt="施術にかかる時間と、抜糸について教えてください" class="sp_none"><img src="img/block07_q03_sp.gif" width="100%" alt="施術にかかる時間と、抜糸について教えてください" class="pc_none situmon_img"></p>
                <p class="situmon_a">施術時間はおよそ40?60分、術後７日以降に抜糸を行います。</p>
            </div>
<hr style="border-top:1px dotted #c5c5c5; margin-bottom:15px;" size="1" class="pc_none">
            <div class="s_box">
                <p class="situmon_q ml45"><img src="img/block07_q04.gif" width="372" height="21" alt="入浴や運動はいつからできますか？" class="sp_none"><img src="img/block07_q04_sp.gif" width="100%" alt="入浴や運動はいつからできますか？" class="pc_none"></p>
                <p class="situmon_a ml30">シャワーはバスト部が濡れなければ当日より可能。激しい運動は4週間後より可能です。</p>
            </div>
<hr style="border-top:1px dotted #c5c5c5; margin-bottom:15px;" size="1" class="pc_none">
            <div class="s_box">
                <p class="situmon_q ml60"><img src="img/block07_q05.gif" width="467" height="21" alt="傷跡はどのくらいで目立たなくなりますか？" class="sp_none"><img src="img/block07_q05_sp.gif" width="100%" alt="傷跡はどのくらいで目立たなくなりますか？" class="pc_none"></p>
        <p class="situmon_a ml45">脇の下のシワに沿って切開をするため、そもそも傷は目立ちにくいのですが、術後1ヵ月ほどでほとんど傷跡は分からなくなります。</p>
            </div>
        </div>

        <h3><img src="img/block07_midasi04.gif" width="1202" height="29" alt="シリコンバッグ豊胸料金" class="sp_none"><img src="img/block06_midasi00402_sp.gif" width="100%" alt="ヒアルロン酸豊胸料金" class="pc_none b7_h401"></h3>







		<div class="btn_box2 cf sp_none">
            <div class="right">
                <h2><img src="img/box_title02.png" width="574" height="123" alt="シリコンバッグ豊胸"></h2>
                <p><img src="img/box_text02_r.gif" width="467" height="58" alt="キャンペーン価格　通常価格630,000円 210,000円"></p>
                <dl class="box cf">
                    <dt><img src="img/box_tel.gif" width="284" height="74" alt="0120-334-270 受付時間//11:00～20:00" class="sp_none"></dt>
                    <dd><a href="#section09"><img src="img/box_btn_on.jpg" style="position: absolute; opacity: 0;"><img src="img/box_btn_off.jpg" width="423" height="69" alt="無料カウンセリング予約"></a></dd>
                </dl>
            </div>
            <div class="left">
                <img src="img/box_logo.gif" width="193" height="35" alt="表参道スキンクリニック">
                <img src="img/box_img.png" width="175" height="211" alt="[ドクター]友利　新">
            </div>
        </div>
		<div class="btn_box2 pc_none sp_btnarea">
            <p class="logo"><img src="img/box_logo_sp02.gif" width="100%" class="b7_p01" alt="シリコンバッグ豊胸"></p>
            <p class="title"><img src="img/box_title_sp.gif" width="100%" class="b7_p02" alt="表参道スキンクリニック"></p>
            <ul class="cf">
                <li class="left"><img src="img/box_img_sp.jpg" width="100%" alt="[ドクター]友利　新"></li>
                <li class="right"><img src="img/box_text02_sp_r.gif" width="100%" alt="キャンペーン価格　通常価格630,000円 210,000円" class="pt_15"></li>
            </ul>
            <div class="tel_box">
                <p class="tel"><a href="tel:0120334270" onclick="yahoo_report_conversion('tel:0120-334-270')" class="pc_none"><img src="img/box_tel_sp.gif" width="100%" alt="0120-334-270 受付時間//11:00～20:00"></a></p>
                <a href="#section09"><img src="img/box_btn_sp.gif" width="100%" alt="無料カウンセリング予約"></a>
            </div>
        </div>




    </article>
</div>
<div id="section04" class="box">
	<article class="block08 cf">
    	<h2><img src="img/title05.gif" width="516" height="104" alt="お問い合わせから施術までの流れ" class="sp_none"><img src="img/title05_sp.gif" width="100%" alt="お問い合わせから施術までの流れ" class="pc_none"></h2>
        <div class="cf">
        	<h3><img src="img/block07_tit01.gif" width="675" height="53" alt="お電話、メールにてお問い合わせ" class="sp_none"><img src="img/block07_tit01_sp.gif" width="100%" alt="お電話、メールにてお問い合わせ" class="pc_none"></h3>
            <img src="img/pic02.jpg" width="325" height="245" class="right" alt="">
            <p>電話、もしくはメールでご予約を承っております。<br>
			施術のご予約の前に、診療や施術、その他について疑問がありましたら、お電話またはお問い合わせフォーム、メールからお気軽にお問い合わせください。その際には匿名でのお問合せもOKです。</p>
            <div class="b08_tel sp_none">
                <img src="img/block07_tel01.gif" width="452" height="17" alt="お電話でのご相談　平日//11:00～20:00" class="block07_tel01">
                <img src="img/block07_tel02.gif" width="231" height="23" alt="0120-334-270" class="block07_tel02">
                <p><a href="#section09"><img src="img/block07_tel_off.gif" width="339" height="49" alt="無料カウンセリング予約"></a></p>
            </div>
            <div class="b08_telsp pc_none">
                <img src="img/block07_tel01_sp.gif" width="100%" alt="お電話でのご相談　平日//11:00～20:00" class="block07_tel01">
                <a href="tel:0120334270" onclick="yahoo_report_conversion('tel:0120-334-270')" class="pc_none"><img src="img/block07_tel02_sp.gif" width="100%" alt="0120-334-270" class="block07_tel02"></a>
                <p><a href="#section09"><img src="img/block07_tel_off.gif" width="100%" alt="無料カウンセリング予約"></a></p>
            </div>
        </div>



        <div class="cf">
        	<h3><img src="img/block07_tit02.gif" width="676" height="52" alt="ご来院・受付" class="sp_none"><img src="img/block07_tit02_sp.gif" width="100%" alt="ご来院・受付" class="pc_none"></h3>
            <img src="img/pic03.jpg" width="325" height="200" class="right" alt="">
            <p>お電話でご予約いただいた日程でご来院ください。（日程の変更などはお気軽にご連絡ください）<br>
            ご予約された日時にお越し下さい。<br>
            表参道スキンクリニックでのカウンセリング方法など事前に何でも相談ください。</p>
        </div>



        <div class="cf">
        	<h3><img src="img/block07_tit03.gif" width="675" height="52" alt="スタッフとのカウンセリング" class="sp_none"><img src="img/block07_tit03_sp.gif" width="100%" alt="スタッフとのカウンセリング" class="pc_none"></h3>
            <img src="img/pic04.jpg" width="325" height="200" class="right" alt="">
            <p>お客様、患者様の悩みや疑問、施術費用やお支払方法ついてなど、専門のスタッフにお気軽に相談ください。</p>
            <p class="block_box">施術へのお手続き・施術申込書の記入、術後の注意事項をお渡しします。<br>未成年の方は同意書が必要になります。又はご両親のどちらかご同伴でお願いします。<br>※認印（シャチハタ以外）をお持ち下さい。</p>
        </div>


        <div class="cf">
        	<h3><img src="img/block07_tit04.gif" width="676" height="52" alt="担当医師とのカウンセリング・診察" class="sp_none"><img src="img/block07_tit04_sp.gif" width="100%" alt="担当医師とのカウンセリング・診察" class="pc_none"></h3>
            <img src="img/pic05.jpg" width="325" height="200" class="right" alt="">
            <p>実際に施術を行う担当医師が丁寧にご要望をうかがった上で、プラン、施術方法、施術内容のご説明をいたします。</p>
            <p class="small">※施術内容の詳細は担当医師がご説明いたします。ご不明な点があればお気軽にご相談ください。</p>
        </div>



        <div class="cf">
        	<h3><img src="img/block07_tit05.gif" width="676" height="52" alt="施術を行います" class="sp_none"><img src="img/block07_tit05_sp.gif" width="100%" alt="施術を行います" class="pc_none"></h3>
            <img src="img/pic06.jpg" width="325" height="200" class="right" alt="">
            <p>お顔の施術の場合は、事前にメイクを落としていただく必要がございます。<br>
            化粧落としなどご用意しておりますので、お気軽にスタッフにお声掛けください。<br>
            準備ができましたら施術室に移動していただき、施術を行います。<br>
            リラックスして施術を受けてください。</p>
        </div>



        <div class="cf">
        	<h3><img src="img/block07_tit06.gif" width="675" height="52" alt="アフターケア" class="sp_none"><img src="img/block07_tit06_sp.gif" width="100%" alt="アフターケア" class="pc_none"></h3>
            <img src="img/pic07.jpg" width="325" height="200" alt="" class="right">
            <p>施術後はすぐにご帰宅頂けます。<br>
            専用のメイクルームをご用意いたしておりますので、メイク直しをしながら安心して新しいご自分と対面していただけます。施術後のアフターケアの料金は手術料金に含まれております。<br>
            また、施術後しばらく経ってご不安な点や不明瞭な点などがございましたら、お気軽に電話、またはメールにて直接ご相談ください。</p>
        </div>
    </article>

    <div class="btn_box cf">
        <dl class="cf">
        	<dt><img src="img/tel01.gif" width="365" height="54" alt="0120-334-270 受付時間//11:00～20:00" class="sp_none"><a href="tel:0120334270" onclick="yahoo_report_conversion('tel:0120-334-270')" class="pc_none"><img src="img/tel01_sp.gif" width="258" height="60" alt="0120-334-270 受付時間//11:00～20:00"></a></dt>
            <dd><a href="#section09"><img class="sp_none" src="img/btn01_on.gif" style="position: absolute; opacity: 0;"><img src="img/btn01_off.gif" width="300" height="61" alt="無料カウンセリング予約"></a></dd>
        </dl>
    </div>
</div>
<div id="section05" class="box">
	<article class="block09 cf">
        <h2><img src="img/title06.gif" width="193" height="104" alt="院内ご案内" class="sp_none"><img src="img/title06_sp.gif" width="100%" alt="院内ご案内" class="pc_none"></h2>


      <div class="sp_none">
        <ul class="bxslider cf">
          <li><a href="img/slider02_big.jpg" rel="lightbox"><img src="img/slider01_sp.jpg" width="100%" alt="受付"></a></li>
          <li><a href="img/slider03_big.jpg" rel="lightbox"><img src="img/slider02_sp.jpg" width="100%" alt="待合室"></a></li>
          <li><a href="img/slider04_big.jpg" rel="lightbox"><img src="img/slider03_sp.jpg" width="100%" alt="カウンセリングルーム"></a></li>
          <li><a href="img/slider05_big.jpg" rel="lightbox"><img src="img/slider04_sp.jpg" width="100%" alt="診察室"></a></li>
          <li><a href="img/slider06_big.jpg" rel="lightbox"><img src="img/slider05_sp.jpg" width="100%" alt="診察室"></a></li>
          <li><a href="img/slider01_big.jpg" rel="lightbox"><img src="img/slider06_sp.jpg" width="100%" alt="施術室"></a></li>
        </ul>
      </div>



      <div class="pc_none">
        <ul class="bxslider cf">
          <li><img src="img/slider01_sp.jpg" width="100%" alt="受付"></li>
          <li><img src="img/slider02_sp.jpg" width="100%" alt="待合室"></li>
          <li><img src="img/slider03_sp.jpg" width="100%" alt="カウンセリングルーム"></li>
          <li><img src="img/slider04_sp.jpg" width="100%" alt="診察室"></li>
          <li><img src="img/slider05_sp.jpg" width="100%" alt="診察室"></li>
          <li><img src="img/slider06_sp.jpg" width="100%" alt="施術室"></li>
        </ul>
      </div>




        <p class="tc"><img src="img/text01.gif" width="803" height="50" alt="当院では安心してご利用頂ける万全の環境を整えておりますので、安心してお越しください。皆さまのご来院をお待ちしております。" class="sp_none"><img src="img/text01_sp.gif" width="100%" alt="当院では安心してご利用頂ける万全の環境を整えておりますので、安心してお越しください。皆さまのご来院をお待ちしております。" class="pc_none"></p>
    </article>

    <div class="btn_box cf">
        <dl class="cf">
        	<dt><img src="img/tel01.gif" width="365" height="54" alt="0120-334-270 受付時間//11:00～20:00" class="sp_none"><a href="tel:0120334270" onclick="yahoo_report_conversion('tel:0120-334-270')" class="pc_none"><img src="img/tel01_sp.gif" width="258" height="60" alt="0120-334-270 受付時間//11:00～20:00"></a></dt>
            <dd><a href="#section09"><img class="sp_none" src="img/btn01_on.gif" style="position: absolute; opacity: 0;"><img src="img/btn01_off.gif" width="300" height="61" alt="無料カウンセリング予約"></a></dd>
        </dl>
    </div>
</div>





<div id="section06" class="box">
	<article class="block10 cf">
    	<h2><img src="img/title07.gif" width="517" height="104" alt="表参道スキンクリニックの理念" class="sp_none"><img src="img/title07_sp.gif" width="100%" alt="表参道スキンクリニックの理念" class="pc_none"></h2>
        <p class="text">開業当初から当クリニックでは<br class="pc_none">痛みや不安のない施術を心がけており、<br class="pc_none">患者様に安心して美しくなって頂くことを<br class="pc_none">目指しております。</p>
        <div class="list cf sp_none">
            <div class="left">
                <h3><img src="img/list_01.png" width="360" height="389" alt="患者様とのコミュニケーションを大切にしています" class="sp_none"></h3>
                <p>当クリニックは患者様の立場に立ち、今抱えている不安や悩みなどを解消するため、丁寧なカウンセリングを行なっています。</p>
            </div>
            <div class="center">
                <h3><img src="img/list_02.png" width="360" height="389" alt="長年の経験と実績による確かな技術と高い信頼" class="sp_none"></h3>
                <p>当クリニックは優秀な美容皮膚科医が多数在籍しています。本当に安心して任せられるドクターがレベルの高い治療を行ないます。</p>
            </div>
            <div class="right">
                <h3><img src="img/list_03.png" width="360" height="389" alt="いつもドクターがそばにいてくれるという感覚" class="sp_none"></h3>
                <p>当クリニックは気軽に相談できる環境作りを心がけています。術後のケア、美肌づくりのアドバイスなどいつでもすぐにお応えいたします。</p>
            </div>
        </div>
        <div class="listsp cf pc_none">
            <div class="left">
                <h3><img src="img/list_01_sp.png" width="100%" alt="患者様とのコミュニケーションを大切にしています"></h3>
                <p>当クリニックは患者様の立場に立ち、今抱えている不安や悩みなどを解消するため、丁寧なカウンセリングを行なっています。</p>
            </div>
            <div class="center">
                <h3><img src="img/list_02_sp.png" width="100%" alt="長年の経験と実績による確かな技術と高い信頼"></h3>
                <p>当クリニックは優秀な美容皮膚科医が多数在籍しています。本当に安心して任せられるドクターがレベルの高い治療を行ないます。</p>
            </div>
            <div class="right">
                <h3><img src="img/list_03_sp.png" width="100%" alt="いつもドクターがそばにいてくれるという感覚"></h3>
                <p>当クリニックは気軽に相談できる環境作りを心がけています。術後のケア、美肌づくりのアドバイスなどいつでもすぐにお応えいたします。</p>
            </div>
        </div>
    </article>
</div>

<div id="section07" class="box">
	<article class="block11 cf">
    	<h2><img src="img/title08.gif" width="236" height="104" alt="医師のご紹介" class="sp_none"><img src="img/title08_sp.gif" width="100%" alt="医師のご紹介" class="pc_none"></h2>
        

        <p class="name"><img src="img/name_01.gif" width="1041" height="43" alt="院長　松木貴裕" class="sp_none"><img src="img/name_01_sp.gif" width="100%" alt="院長　松木貴裕" class="pc_none"></p>
        <img src="img/docter_01.jpg" width="154" height="173" alt="" class="right">
        <div class="cf sp_right">
            <h3>経歴<span>CAREER</span></h3>
		        <dl class="cf">
		          <dt>平成10年3月</dt>
		          <dd>藤田保健衛生大学 医学部医学研究所科 卒業</dd>
		        </dl>
		        <dl class="cf">
		          <dt>平成11年9月</dt>
		          <dd>医療法人大医会 日進おりど病院 勤務</dd>
		        </dl>
		        <dl class="cf">
		          <dt>平成14年3月</dt>
		          <dd>医療法人大医会 日進おりど病院 退職</dd>
		        </dl>
		        <dl class="cf">
		          <dt>平成14年4月</dt>
		          <dd>東京女子医科大学病院 皮膚科 勤務</dd>
		        </dl>
		        <dl class="cf">
		          <dt>平成16年3月</dt>
		          <dd>東京女子医科大学病院 皮膚科 退職</dd>
		        </dl>
		        <dl class="cf">
		          <dt>平成16年9月</dt>
		          <dd>東京美容外科 開設 勤務</dd>
		        </dl>
            </div>
        <div class="cf sp_clear">
            <h3>所属学会など<span>A BELONGING ACADEMIC MEETING</span></h3>
            <ul class="cf">
	          <li>・日本皮膚科学会正会員</li>
	          <li>・レーザー医学会認定医</li>
	          <li>・日本抗加齢学会会員</li>
	          <li>・美容外科学会会員</li>
            </ul>
        </div>
        <h3 class="clear">松木先生から一言<span>FROM DOCTOR WORD</span></h3>
        <p class="bottom_text">かつては、大がかりな美容整形手術など、ハイリスクなイメージがあった美容医療の世界。しかし近年では、手軽にトライできる治療も増え、とても身近な存在になりました。当クリニックでは、高い技術を持つ経験豊富な美容皮膚科医が、患者様の希望や理想を大切にしながら、最新の美容医療を提供いたします。</p>





        <p class="name"><img src="img/name_05.png" width="1041" height="43" alt="友利新" class="sp_none"><img src="img/name_02_sp.gif" width="100%" alt="友利新" class="pc_none"></p>
        <img src="img/docter_02.jpg" width="154" height="173" alt="" class="right">
        <div class="cf sp_right">
            <h3>経歴<span>CAREER</span></h3>
			<p>
			東京女子医科大学卒業。<br>
			同大学病院の内科勤務を経て皮膚科へ転科。<br>
			現在、都内2か所のクリニックに勤務の傍ら、医師という立場から美容と健康を医療として追求し、美しく生きる為の啓蒙活動を雑誌・TV などで展開中。
			</p>
			<p>
			2004年第36回準ミス日本という経歴をもつ、美貌の新進医師。<br>
			美と健康に関する著書も多数あり、近著に『女性のキレイと健康をつくる 美肌タイミングジュース』（保健同人社より2011年6月10日発刊）がある。
			</p>
       </div>
        <div class="cf sp_clear">
            <h3>所属学会など<span>A BELONGING ACADEMIC MEETING</span></h3>
            <ul class="cf">
	            <li>・日本内科学会会員</li>
	            <li>・日本糖尿病学会会員</li>
	            <li>・日本皮膚科学会会員</li>
	            <li>・抗加齢学会会員</li>
            </ul>
        </div>
        <h3 class="clear">友利先生から一言<span>FROM DOCTOR WORD</span></h3>
        <p class="bottom_text">女性が綺麗になりたいという思いは変わらないものです。そんな願いを手軽に、そして確実に医療の技術で叶えたい。そう思いながら日々の診療をさせてもらっています。 一人でも多くの女性の笑顔に自信を持っていただけるように、皆さんの美の主治医としていろいろな悩みにお応えしていきます。</p>

    </article>
    <div class="btn_box cf">
        <dl class="cf">
        	<dt><img src="img/tel01.gif" width="365" height="54" alt="0120-334-270 受付時間//11:00～20:00" class="sp_none"><a href="tel:0120334270" onclick="yahoo_report_conversion('tel:0120-334-270')" class="pc_none"><img src="img/tel01_sp.gif" width="258" height="60" alt="0120-334-270 受付時間//11:00～20:00"></a></dt>
            <dd><a href="#section09"><img class="sp_none" src="img/btn01_on.gif" width="300" height="61" style="position: absolute; opacity: 0;"><img src="img/btn01_on.gif" style="position: absolute; opacity: 0;"><img src="img/btn01_off.gif" width="300" height="61" alt="無料カウンセリング予約"></a></dd>
        </dl>
    </div>
</div>

<div id="section08" class="box">

	<article class="block12 cf">
    	<h2><img src="img/title09.gif" width="149" height="104" alt="アクセス" class="sp_none"><img src="img/title09_sp.gif" width="100%" alt="アクセス" class="pc_none"></h2>
        <p class="pc_none sp_logo"><img src="img/logo_sp.gif" width="100%" alt="表参道スキンクリニック"><br>〒460-0003 名古屋市中区錦3-17-15栄ナナイロ8F</p>
		<iframe class="accecc" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3241.39689876405!2d139.707413!3d35.667228!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x60188ca3e16206fd%3A0x92146d294d00108b!2z6KGo5Y-C6YGT44K544Kt44Oz44Kv44Oq44OL44OD44Kv!5e0!3m2!1sja!2sjp!4v1433309200061" width="585" height="470" frameborder="0" style="border:0"></iframe>
        <div class="cf">
            <p class="text sp_none">〒150-0001 東京都渋谷区神宮前5-9-13喜多重ビル4F（1Fオリエンタルバザー）</p>
        </div>
        <div class="cf">
        	<h3 class="sp_none"><img src="img/tit_img01.jpg" width="204" height="44" alt="電車でお越しの方"></h3>
            <h3 class="pc_none"><img src="img/tit_img01_sp.jpg" width="100%" alt="電車でお越しの方"></h3>
            <ul>
            	<li class="no01">JR山手線「原宿駅」表参道口より徒歩約5分</li>
            	<li class="no02">東京メトロ各線「表参道駅」A1出口より徒歩約3分</li>
            	<li class="no03">東京メトロ各線「明治神宮前駅」4番出口より徒歩約3分</li>
            </ul>
        </div>
        <div class="cf center">
			<h3 class="sp_none"><img src="img/tit_img02.jpg" width="204" height="44" alt="お車でお越しの方"></h3>
            <h3 class="pc_none"><img src="img/tit_img02_sp.jpg" width="100%" alt="お車でお越しの方"></h3>
            <p class="mb0">当クリニック専用の駐車はございません。<br class="pc_none">お近くの有料駐車場のご利用をお願いいたします。</p>
        </div>
        <div class="cf bottom">
			<h3 class="sp_none"><img src="img/tit_img03.jpg" width="193" height="44" alt="治療時間・休診日"></h3>
            <h3 class="pc_none"><img src="img/tit_img03_sp.jpg" width="100%" alt="治療時間・休診日"></h3>
            <dl class="dl01 cf">
            	<dt><img src="img/sinnroyu.jpg" width="66" height="21" alt="診療時間"></dt>
                <dd>AM11:00～PM8:00(完全予約制)</dd>
            </dl>
            <dl class="dl02 cf">
            	<dt><img src="img/kyuusinn.jpg" width="66" height="21" alt="休診日"></dt>
                <dd>水曜日</dd>
            </dl>
        </div>
    </article>

</div>


<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/smoothness/jquery-ui.css" >
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1/i18n/jquery.ui.datepicker-ja.min.js"></script>
<script>
$(function() {
	$("#datepicker_1").datepicker({
		dateFormat: 'yy年mm月dd日 (DD)',
		dayNames: ['日', '月', '火', '水', '木', '金', '土']
	});
	$("#datepicker_2").datepicker({
		dateFormat: 'yy年mm月dd日 (DD)',
		dayNames: ['日', '月', '火', '水', '木', '金', '土']
	});
});
</script>

	<div id="section09" class="box">

		<article class="block13 cf">
			<h2><img src="img/title10.gif" width="600" height="104" alt="無料カウンセリングご予約フォーム" class="sp_none"><img src="img/title10_sp.gif" width="100%" alt="無料カウンセリングご予約フォーム" class="pc_none"></h2>

			<p class="flow"><img src="img/flow_01.gif" alt="1．情報の入力" width="1000" height="80" /></p>

				<div class="form">

					<p class="caution">※お手数ですが、必須項目をすべてご入力の上、確認画面へお進みください。</p>

        <form method="post">

          <table>
            <tr>
              <th class="hissu">お名前</th>
              <td>
                <input type="text" name="name_req" value="<?php echo $formTool->h($_POST['name_req']); ?>" />　例）表参道花子
                <?php if( !empty($formTool->messages['name_req']) ) : ?>
                  <p class="error"><?php echo $formTool->h($formTool->messages['name_req']); ?></p>
                <?php endif; ?>
              </td>
            </tr>
            <tr>
              <th class="hissu">電話番号</th>
              <td>
                <input type="tel" name="tel_req" value="<?php echo $formTool->h($_POST['tel_req']); ?>" />　例）01-2345-6789
                <?php if( !empty($formTool->messages['tel_req']) ) : ?>
                  <p class="error"><?php echo $formTool->h($formTool->messages['tel_req']); ?></p>
                <?php endif; ?>
              </td>
            </tr>
            <tr>
              <th class="hissu">メールアドレス</th>
              <td>
                <input type="email" name="email_req" value="<?php echo $formTool->h($_POST['email_req']); ?>" />　例）info@abc.com
                <?php if( !empty($formTool->messages['email_req']) ) : ?>
                  <p class="error"><?php echo $formTool->h($formTool->messages['email_req']); ?></p>
                <?php endif; ?>
              </td>
            </tr>
            <tr>
              <th class="nini">相談日 第一希望</th>
              <td>
                <input type="text" name="day1" value="<?php echo $formTool->h($_POST['day1']); ?>" placeholder="YYYY年MM月DD日" class="calendar" id="datepicker_1" readonly/>
                <select name="day1_period">
                  <option value="">時間帯を選択</option>
                  <?php echo $formTool->makeOptionHtml($formTool->timeArr, 'array', $_POST['day1_period']); ?>
                </select>
              </td>
            </tr>
            <tr>
              <th class="nini">相談日 第二希望</th>
              <td>
                <input type="text" name="day2" value="<?php echo $formTool->h($_POST['day2']); ?>" placeholder="YYYY年MM月DD日" class="calendar" id="datepicker_2" readonly/>
                <select name="day2_period">
                  <option value="">時間帯を選択</option>
                  <?php echo $formTool->makeOptionHtml($formTool->timeArr, 'array', $_POST['day2_period']); ?>
                </select>
              </td>
            </tr>
            <tr>
              <th class="nini">連絡方法のご希望</th>
              <td class="radiostyle">
                <?php echo $formTool->makeCheckBoxHtml($formTool->typeArr, 'array', $_POST['type'], 'type'); ?>
              </td>
            </tr>
            <tr>
              <th class="nini last">お問い合わせ内容</th>
              <td class="last">
                <textarea name="message" placeholder="例）バストをボリュームアップさせたいです。" cols="20" rows="5"><?php echo $formTool->h($_POST['message']); ?></textarea>
              </td>
            </tr>
          </tbody>
        </table>

        <input type="hidden" name="mode" value="conf" />
        <p class="btn"><input type="image" src="img/formbtn_01.gif" alt="確認画面へ" width="420" height="70" /></p>

      </form>

			</div>

		</article>

		<p class="tc mt60 mb110 bottom_logosp"><img src="img/bottom_image.jpg" width="689" height="159" alt="表参道スキンクリニックは、どこまでも綺麗への想いにお応えし続けます" class="sp_none"><img src="img/bottom_image_sp.gif" width="100%" alt="表参道スキンクリニックは、どこまでも綺麗への想いにお応えし続けます" class="pc_none"></p>










		<div class="btn_box2 cf sp_none bottom_btnarea">
            <div class="right">
                <h2><img src="img/box_title03.png" width="402" height="94" alt="キャンペーン価格"></h2>
                <p><img src="img/box_text03_r.gif" width="773" height="93" alt="ヒアルロン酸豊胸　通常価格　1cc　5,250円　1cc　2,100円～　シリコンバッグ豊胸　通常価格　630,000円　210,000円～"></p>
                <dl class="box cf">
                    <dt><img src="img/box_tel.gif" width="284" height="74" alt="0120-334-270 受付時間//11:00～20:00" class="sp_none"></dt>
                    <dd><a href="#section09"><img src="img/box_btn_on.jpg" style="position: absolute; opacity: 0;"><img src="img/box_btn_on.jpg" style="position: absolute; opacity: 0;"><img src="img/box_btn_off.jpg" width="423" height="69" alt="無料カウンセリング予約"></a></dd>
                </dl>
            </div>
            <div class="left">
                <img src="img/box_logo.gif" width="193" height="35" alt="表参道スキンクリニック">
                <img src="img/box_img.png" width="175" height="211" alt="[ドクター]友利　新">
            </div>
        </div>
		<div class="btn_box2 pc_none">
            <p class="logo"><img src="img/box_logo_sp03.gif" width="100%" alt="キャンペーン価格"></p>
            <p class="title"><img src="img/box_title_sp.gif" width="100%" class="b11_p02" alt="表参道スキンクリニック"></p>
            <ul class="cf">
                <li class="left"><img src="img/box_img_sp.jpg" width="100%" alt="[ドクター]友利　新"></li>
                <li class="right"><img src="img/box_text_sp02_r.gif" width="100%" alt="ヒアルロン酸豊胸　通常価格　1cc　5,250円　1cc　2,100円～ シリコンバッグ豊胸　通常価格　630,000円　210,000円～"></li>
            </ul>
            <div class="tel_box">
                <p class="tel"><a href="tel:0120334270" onclick="yahoo_report_conversion('tel:0120-334-270')" class="pc_none"><img src="img/box_tel_sp.gif" width="100%" alt="0120-334-270 受付時間//11:00～20:00"></a></p>
                <a href="#section09"><img src="img/box_btn_sp.gif" width="100%" alt="無料カウンセリング予約"></a>
            </div>
        </div>





	</div>

	<p id="page-top"><a href="#wrapper"><img src="img/top.png" width="45" height="45" alt="TOP"></a></p>

</div>