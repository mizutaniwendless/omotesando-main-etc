
$(function(){
	$("body").on("click", ".tabMenu li a", function (){
		var inName = '_ins';
		var offName = '_offs';
		
		//一度全てクリア
		$(this).parent().parents().parent().children(".tabMenu").children("li").children("a").children("img").each(function(i) {
			$(this).attr('src',$(this).attr('src').replace(inName,offName));
		});
		//クリックした画像をoffからinに変更
		$(this).children('img').attr('src',$(this).children('img').attr('src').replace(offName,inName));
		
		$(this).parent().parents().parent().children("div.tabBoxes").children().css('height','0').css('overflow','hidden').css('position','absolute').css('top','0');
		$($(this).attr("href")).css('height','auto').css('overflow','inherit').css('position','');
		return false;
	});
	return false;
});


$(function(){
	$("body").on("click", ".tabMenu2 li a", function (){
		var inName = '_ins';
		var offName = '_offs';
		
		//一度全てクリア
		$(this).parent().parents().parent().children(".tabMenu2").children("li").children("a").children("img").each(function(i) {
			$(this).attr('src',$(this).attr('src').replace(inName,offName));
		});
		//クリックした画像をoffからinに変更
		$(this).children('img').attr('src',$(this).children('img').attr('src').replace(offName,inName));
		
		$(this).parent().parents().parent().children("div.tabBoxes2").children().hide();
		$($(this).attr("href")).fadeToggle();
		return false;
	});
	return false;
});


$(function(){
	$("body").on("click", ".tabMenu3 li a", function (){
		var inName = '_ins';
		var offName = '_offs';
		
		//一度全てクリア
		$(this).parent().parents().parent().children(".tabMenu3").children("li").children("a").children("img").each(function(i) {
			$(this).attr('src',$(this).attr('src').replace(inName,offName));
		});
		//クリックした画像をoffからinに変更
		$(this).children('img').attr('src',$(this).children('img').attr('src').replace(offName,inName));
		
		$(this).parent().parents().parent().children("div.tabBoxes3").children().hide();
		$($(this).attr("href")).fadeToggle();
		return false;
	});
	return false;
});


