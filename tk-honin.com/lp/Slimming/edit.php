<?php
global $formTool;
?>
<div class="cnt_wp">
  <ul id="navi">
    <li><a href="#section01" class="on">●</a></li>
    <li><a href="#section02">●</a></li>
    <li><a href="#section03">●</a></li>
    <li><a href="#section04">●</a></li>
    <li><a href="#section05">●</a></li>
    <li><a href="#section06">●</a></li>
    <li><a href="#section07">●</a></li>
    <li><a href="#section08">●</a></li>
    <li><a href="#section09">●</a></li>
  </ul>

  <div id="section01" class="box">

<article class="block01">
      <h2><img src="img/main_img01.jpg" width="1500" height="604" alt="「切らない痩身美容で部分痩せ」安全にきになるパーツを集中ダイエット、脂肪溶解注射、BNLS、ダイエット処方薬" class="sp_none"><img src="img/main_img01_sp.jpg" width="100%" alt="傷跡を残さない「美しい肌」に諦めかけていたシミやタトゥーも最新のレーザーですっきりキレイに除去" class="pc_none"></h2>
</article>

<article class="block02">



<h2 class="sp_none"><img src="img/b02_tit.jpg" width="1076" height="181" alt="外科手術不要！切らない痩身美容"></h2>
<h2 class="pc_none"><img src="img/b02_tit_sp.jpg" width="100%" alt="外科手術不要！切らない痩身美容"></h2>
<p class="sp_none b02_p01">運動やダイエットをしても中々ピンポイントで脂肪を落とすのが難しい…。<br />
でもメスや大掛かりな外科手術を行うのも大変…。<br />
手術不要のお手軽痩身美容で理想の部分痩せを手に入れましょう。</p>


<p class="pc_none b02_p01">運動やダイエットをしても<br />中々ピンポイントで<br />
脂肪を落とすのが難しい…。<br />
でもメスや大掛かりな外科手術を<br />
行うのも大変…。<br />
手術不要のお手軽痩身美容で<br />
理想の部分痩せを<br />手に入れましょう。</p>


<div class="b02box_back cf">
      	<div class="b02box01">
        <div class="cf">
            	<img src="img/b02_01.jpg" width="272" height="147" alt="脂肪溶解注射" class="sp_none" />
				 <div class="sp_block01 pc_none">
					<p class="tc">脂肪溶解注射</p>
                     <img src="img/b02_01.jpg" width="272" height="147" alt="脂肪溶解注射" />
					<a href="#section02">詳しく見る</a>
          </div>
                <div class="sp_none">
                  <dl class="b02dl01">
                        <dt>脂肪溶解注射<dt>
                        <dd>脂肪細胞を破壊し、体外に排出。<br />気になる部位に直接的なアプローチが可能な注射でできるメソセラピー。</dd>
                  </dl>
                  <dl class="b02dl02">
                        <dt>施術可能箇所</dt>
                        <dd>二の腕、お腹回り、ももの裏等</dd>
                  </dl>
                  <a href="#section02">詳しく見る</a>
				</div>
          </div>
  </div>
      	<div class="b02box01 b02box02">
  <div class="cf">
            	<img src="img/b02_02.jpg" width="272" height="147" alt="BNLS®" class="sp_none" />
                <div class="sp_block01 pc_none">
					<p class="tc">BNLS®</p>
            	<img src="img/b02_02.jpg" width="272" height="147" alt="BNLS®" />
					<a href="#section03">詳しく見る</a>
                </div>
                <div class="sp_none">
                  <dl class="b02dl01">
                        <dt>BNLS®<dt>
                        <dd>今話題の注射でできる部分痩せメソッド。ダウンタイムが少なく人気の施術です。</dd>
                  </dl>
                  <dl class="b02dl02">
                        <dt>施術可能箇所</dt>
                        <dd>顔、二の腕、お腹周り、太もも等</dd>
                  </dl>
                  <a href="#section03">詳しく見る</a>
				</div>
		  </div>
  </div>
<div class="b02box01 b02box03">
        <div class="cf">
        <img src="img/b02_03.jpg" width="272" height="147" alt="ダイエット処方薬（服用）" class="sp_none"/>
                <div class="sp_block01 pc_none">
					<p class="tc">ダイエット処方薬<br /><span>（服用）</span></p>
                    <img src="img/b02_03.jpg" width="272" height="147" alt="ダイエット処方薬（服用）" />
					<a href="#section04">詳しく見る</a>
                </div>
               <div class="sp_none">
            <dl class="b02dl01">
                        <dt>ダイエット処方薬<span>（服用）</span><dt>
                        <dd>自然に着実に痩せたい、リバウンド防止したいというお客様に向いています。</dd>
                 </dl>
                  <dl class="b02dl02">
                        <dt>施術可能箇所</dt>
                        <dd>食欲抑制、脂肪吸収コントロール、カロリー制限、炭水化物吸収抑制等</dd>
                  </dl>
                  <a href="#section04">詳しく見る</a>
		  </div>
	</div>
  </div>
</div>
	<p class="b02_p02">食事制限や運動などのダイエットでは実現が難しい部分痩せ。<br />でも大掛かりな手術や入院はしたくない。<br />そのような要望をかなえるためのメソッドが脂肪溶解注射、BNLS®、ダイエット処方薬です。<br />基本的に入院、通院不要、メスを使わずに部分痩せ、ダイエットが可能です。（※効果には個人差があります。）<br />当クリニックの医師は豊富な経験を持っており、徹底的にカウンセリングを行ったうえで、<br />患者様のご希望に合わせた最適な施術、ダイエットプランをご提案をさせていただいております。</p>

      <div class="sp_none b02band">
      	<p>当院では患者様との入念なカウンセリングを行い、<br />経験豊富な医師が患者様個人に適した最適な施術プランをご提供いたします。</p>
      </div>

      <div class="pc_none b02band">
      	<p>当院では患者様との<br />入念なカウンセリングを行い、<br />経験豊富な医師が患者様個人に<br />適した最適な施術プランを<br />ご提供いたします。</p>
      </div>


      <div class="sp_none b02info cf">
      	<img class="info01" src="img/info01.jpg" width="533" height="104" alt="脂肪溶解注射、BNLS®、ダイエット処方薬、お試しキャンペーン価格" />
            <img class="info02" src="img/info02.png" width="288" height="70" alt="052-962-5155　平日11:00〜19:00/日祝10:00〜17:30" />
            <a href="#section09" class="info03"><img src="img/info03.jpg" width="298" height="61" alt="無料カウンセリングのご予約" /></a>
      </div>

	<div class="pc_none b02info cf">
		<img src="img/new02sp.jpg" width="100%" alt="脂肪溶解注射、BNLS®、ダイエット処方薬、お試しキャンペーン価格" class="mb15"/>
		<a href="tel:0120334270"><img src="img/new03sp.jpg" width="100%" alt="052-962-5155　平日11:00～19:00/日祝10:00～17:30" /></a>
		<a href="#section09" class="info03"><img src="img/new04sp.jpg" width="100%" alt="無料カウンセリングのご予約" /></a>
	</div>





<div id="section02" class="box">

      <div class="sp_none b03_band">
      	<div class="b03_band_in">
           		<div class="b03head">
                  	<img src="img/b03en.jpg" width="245" height="15" alt="脂肪溶解注射" />
                        <h2>脂肪溶解注射</h2>
                  </div>
           		<p>メソセラピーとも呼ばれる脂肪溶解注射は、仏・ミシュエル・ピストール医師によって開発された治療法です。<br />メスを用いる脂肪吸引とは異なり、気になる部分に薬剤を注入することによって脂肪を分解・溶解<br />して、体外に排出させます。施術時間はわずか５～10分（施術箇所により異なります。）ほど。<br />リバウンドの心配がなく、部分痩せも可能なことから、手軽で画期的なダイエット法として注目を集めています。</p>
            </div>
      </div>

		<div class="pc_none">
			<img src="img/new05sp.jpg" width="100%" alt="脂肪溶解注射"/>
			<div class="spblock002">
				<p>メソセラピーとも呼ばれる脂肪溶解注射は、仏・ミシュエル・ピストール医師によって開発された治療法です。<br />
メスを用いる脂肪吸引とは異なり、気になる部分に薬剤を注入することによって脂肪を分解・溶解して、体外に排出させます。施術時間はわずか５～10分（施術箇所により異なります。）ほど。<br />
リバウンドの心配がなく、部分痩せも可能なことから、手軽で画期的なダイエット法として注目を集めています。</p>
            </div>
        </div>



	<div class="b03_graph">
		<diV class="b03_graph_in">
			<div class="b03_g cf">
				<div class="b03gra01">
					<p>薬剤を注射<br />（主成分レシチン）</p>
					<img src="img/b03_img02.png" width="228" height="165" alt="薬剤を注射（主成分レシチン）" />
				</div>
				<img src="img/b03_arr.png" width="20" height="36" alt="" class="b03arr sp_none"/>
				<img src="img/new17sp.jpg" width="20" height="36" alt="" class="b03arr pc_none"/>
				<div class="b03gra01 b03gra02">
					<p>薬剤が脂肪細胞を<br />分解・溶解</p>
					<img src="img/b03_img03.png" width="228" height="165" alt="薬剤が脂肪細胞を分解・溶解" />
				</div>
				<img src="img/b03_arr.png" width="20" height="36" alt="" class="b03arr sp_none"/>
				<img src="img/new17sp.jpg" width="20" height="36" alt="" class="b03arr pc_none"/>
				<div class="b03gra01 b03gra03">
					<p>溶解された脂肪細胞は<br />体外に排出</p>
					<img src="img/b03_img04.png" width="228" height="165" alt="溶解された脂肪細胞は体外に排出" />
				</div>
			</div>
            <p class="sptxt01">● 気になる部分に直接注射することによりピンポイントに効果を発揮！</p>
			<p class="sptxt01">● お腹周り、お尻、二の腕、ももの裏などの脂肪がつきやすい部分を直接ケア</p>
		</div>
	</div>






      <div class="b04">
      	<div class="b04_01">
			<div class="pc_none">
            	<div><img src="img/new10sp.jpg" width="100%" alt="お腹周り" /></div>
				<p class="">下腹部や背中からついてしまった浮輪のような脂肪を何とかしたい。<br />脂肪溶解注射でスッキリしたお腹を目指します。</p>
				<img src="img/new18sp.jpg" width="100%" alt="BEFORE AFTER" class="mt15" />
				<p class="tr">※効果には個人差があります。</p>
            </div>

        		<div class="sp_none cf">
            	<dl class="b04_01head cf">
                  	<dt>お腹周り</dt>
                        <dd><img src="img/b03_head04.jpg" width="320" height="29" alt="下腹部のたるんだぜい肉もスッキリ！" /></dd>
                  </dl>
                  <p class="b04_01head_p">下腹部や背中からついてしまった浮輪のような脂肪を何とかしたい。<br />脂肪溶解注射でスッキリしたお腹を目指します。</p>
                  <div class="b04_01bottom cf">
                  	<img src="img/b04_ba01a.jpg" width="182" height="176" alt="BEFORE" />
                        <img src="img/b04_arr.png" width="84" height="55" alt="施術後" class="b04_01arr" />
                        <img src="img/b04_ba01b.jpg" width="182" height="176" alt="AFTER" />
                  </div>
                  <p class="b04_01bottom_p">※効果には個人差があります。</p>

				</div>
            </div>

      	<div class="b04_01 b04_02">
			<div class="pc_none">
            	<div><img src="img/new11sp.jpg" width="100%" alt="お尻" /></div>
				<p>脂肪がついて垂れ下がってしまったお尻、全体的にセルライトが気になるなどの悩みに効果的です。サイズダウン、キュッと引き締まった形の良いお尻になります。</p>
				<img src="img/new19sp.jpg" width="100%" alt="BEFORE AFTER" class="mt15" />
				<p class="tr">※効果には個人差があります。</p>
            </div>


        	<div class="sp_none">
            	<dl class="b04_01head cf">
                  	<dt>お尻</dt>
                        <dd><img src="img/b03_head05.jpg" width="389" height="29" alt="お尻の下の落ちにくい脂肪をしっかりと溶解" /></dd>
                  </dl>
                  <p class="b04_01head_p">脂肪がついて垂れ下がってしまったお尻、全体的にセルライトが気になるなどの悩みに効果的です。サイズダウン、キュッと引き締まった形の良いお尻になります。</p>
                  <div class="b04_01bottom">
                  	<img src="img/b04_ba02a.jpg" width="182" height="176" alt="BEFORE" />
                        <img src="img/b04_arr.png" width="84" height="55" alt="施術後" class="b04_01arr" />
                        <img src="img/b04_ba02b.jpg" width="182" height="176" alt="AFTER" />
                  </div>
                  <p class="b04_01bottom_p">※効果には個人差があります。</p>
				</div>
            </div>

      	<div class="b04_01 b04_03">

			<div class="pc_none">
            	<div><img src="img/new12sp.jpg" width="100%" alt="二の腕" /></div>
				<p>二の腕の付け根は、年齢とともに脂肪が付きやすい部位。余分な脂肪細胞自体を直接取り除くので、もとの状態に戻りにくくなります。細くすっきりした二の腕へ。</p>
				<img src="img/new20sp.jpg" width="100%" alt="BEFORE AFTER" class="mt15" />
				<p class="tr">※効果には個人差があります。</p>
            </div>

        	<div class="sp_none">
            	<dl class="b04_01head cf">
                  	<dt>二の腕</dt>
                        <dd><img src="img/b03_head06.jpg" width="296" height="29" alt="たるんだ二の腕も細くスッキリ！" /></dd>
                  </dl>
                  <p class="b04_01head_p">二の腕の付け根は、年齢とともに脂肪が付きやすい部位。余分な脂肪細胞自体を直接取り除くので、もとの状態に戻りにくくなります。細くすっきりした二の腕へ。</p>
                  <div class="b04_01bottom">
                  	<img src="img/b04_ba03a.jpg" width="182" height="176" alt="BEFORE" />
                        <img src="img/b04_arr.png" width="84" height="55" alt="施術後" class="b04_01arr" />
                        <img src="img/b04_ba03b.jpg" width="182" height="176" alt="AFTER" />
                  </div>
                  <p class="b04_01bottom_p">※効果には個人差があります。</p>
                  </div>
            </div>
      </div>





      <div class="b05 cf">
      	<table class="b05_l">
            	<tr>
                  	<th>麻酔</th>
                        <td>不要</td>
                  </tr>
                 <tr>
                  	<th>傷跡</th>
                        <td>注射の針跡<br />（時間の経過と共に消えます）</td>
                  </tr>
            	<tr>
                  	<th>洗顔・入浴</th>
                        <td>施術翌日から可能</td>
                  </tr>
            	<tr>
                  	<th>ダウンタイム</th>
                        <td>アザ、腫れ等の在り</td>
                  </tr>
            </table>
      	<table class="b05_r">
            	<tr>
                  	<th>施術時間</th>
                        <td>5〜10分</td>
                  </tr>
                 <tr>
                  	<th>通院</th>
                        <td>不要</td>
                  </tr>
            	<tr>
                  	<th>通院</th>
                        <td>施術翌日から可能</td>
                  </tr>
            	<tr>
                  	<th>リバウンド</th>
                        <td>しにくい</td>
                  </tr>

            </table>
      </div>

	<div class="b06">
		<h2 class="b06_head">脂肪溶解注射の優れている特徴</h2>
        <div class="sp_none">
            <div class="b06_li">
            	<div class="b06_00">
                  	<img src="img/b06n_01.png" width="66" height="76" alt="no1" />
                        <dl>
                        	<dt>ダイレクトに<br />効果を発揮</dt>
                              <dd>気になる部分に直接注射することにより直接的な効果を発揮します。</dd>
                        </dl>
                  </div>
            	<div class="b06_00 b06_02">
                  	<img src="img/b06n_02.png" width="76" height="76" alt="no2" />
                        <dl>
                        	<dt>施術時間が短い</dt>
                              <dd>施術時間が短時間で済み、注射に使う針は非常に細い特殊針で痛みが少ないです。</dd>
                        </dl>
                  </div>
            	<div class="b06_00 b06_03">
                  	<img src="img/b06n_03.png" width="68" height="77" alt="no3" />
                        <dl>
                        	<dt>リバウンド防止</dt>
                              <dd>脂肪細胞を壊し、体外に排出します。また、脂肪細胞そのものを減らし、痩身効果が続くためリバウンドしにくいです。</dd>
                        </dl>
                  </div>
            	<div class="b06_00 b06_04">
                  	<img src="img/b06n_04.png" width="74" height="76" alt="no4" />
                        <dl>
                        	<dt>セルライトに効果的</dt>
                              <dd>落ちにくいセルライト除去にも効果があります。<br />新陳代謝を促して細胞の活性化を助け、中性脂肪をたまりにくくします。</dd>
                        </dl>
                  </div>
            </div>
		</div>
      </div>

	<div class="pc_none">
    	<ul>
        	<li><img src="img/new13sp.jpg" width="100%" alt="no1 ダイレクトに効果を発揮" /></li>
        	<li><img src="img/new14sp.jpg" width="100%" alt="no2 施術時間が短い" /></li>
        	<li><img src="img/new15sp.jpg" width="100%" alt="no3 リバウンド防止" /></li>
        	<li><img src="img/new16sp.jpg" width="100%" alt="no4 セルライトに効果的" /></li>
        </ul>
    </div>



	<div class="b07">
            <h2 class="b06_head b07_head">脂肪溶解注射に関するFAQ</h2>
      	<div class="b07faq00">
                  <p class="b07_q">腫れや赤み、痛みはどのぐらい出ますか？</p>
                  <p class="b07_a">多少の腫れや赤みは出ますが、３～４日程度で引いていきます。痛みについては、注射の当日から鈍痛や筋肉痛のような痛みが２～３日続くことがあります。</p>
            </div>
      	<div class="b07faq00 b07faq02">
                  <p class="b07_q">リバウンドはありませんか？</p>
                  <p class="b07_a">注入された薬剤は脂肪細胞の核に作用し、脂肪細胞の数自体を減少させるため、リバウンドの心配がありません。</p>
            </div>
      	<div class="b07faq00 b07faq03">
                  <p class="b07_q">溶け出した脂肪はどこに行くのですか？</p>
                  <p class="b07_a">体内で分解・溶解された脂肪組織は、血中から腸管を経て、尿や便として体外に排出されます。</p>
            </div>
            <img src="img/b07_bnr.jpg" width="824" height="253" alt="初回限定お試し価格　脂肪溶解注射キャンペーン価格　1本9,500円　別途初診料が必要です" class="sp_none"/>
            <img src="img/new21sp.jpg" width="100%" alt="初回限定お試し価格　脂肪溶解注射キャンペーン価格　1本9,500円　別途初診料が必要です" class="pc_none"/>
      </div>

      <div class="b08"></div>

      <div class="sp_none b02info b08info cf">
      	<img class="info01" src="img/info01.jpg" width="533" height="104" alt="" />
            <img class="info02" src="img/info02.png" width="288" height="70" alt="052-962-5155　平日11:00〜19:00/日祝10:00〜17:30" />
            <a href="#section09" class="info03"><img src="img/info03.jpg" width="298" height="61" alt="" /></a>
      </div>

	<div class="pc_none b02info cf">
		<img src="img/new02sp.jpg" width="100%" alt="脂肪溶解注射、BNLS®、ダイエット処方薬、お試しキャンペーン価格" class="mb15"/>
		<a href="tel:0120334270"><img src="img/new03sp.jpg" width="100%" alt="052-962-5155　平日11:00～19:00/日祝10:00～17:30" /></a>
		<a href="#section09" class="info03"><img src="img/new04sp.jpg" width="100%" alt="無料カウンセリングのご予約" /></a>
	</div>


</div>



<div id="section03" class="box">

      <div class="sp_none b03_band b09_band">
      	<div class="b03_band_in b09_band_in">
           		<div class="b03head b09head">
                  	<img src="img/b09en.jpg" width="56" height="13" alt="BNLS&reg;" />
                        <h2>BNLS&reg;</h2>
                  </div>
           		<p>BNLS&reg;は、植物から抽出した成分を主成分とし、顔の輪郭形成や部分的痩せのための新しい脂肪溶解注射です。
脂肪溶作用・肌の引き締め作用、リンパ循環作用により迅速な効果を実現しました。
注射で注入するだけで、メスを使用しないので、安心して施術を受けていただけます。</p>
            </div>
      </div>


		<div class="pc_none">
			<img src="img/new22sp.jpg" width="100%" alt="BNLS"/>
			<div class="spblock003">
				<p>BNLS&reg;は、植物から抽出した成分を主成分とし、顔の輪郭形成や部分的痩せのための新しい脂肪溶解注射です。
脂肪溶作用・肌の引き締め作用、リンパ循環作用により迅速な効果を実現しました。
注射で注入するだけで、メスを使用しないので、安心して施術を受けていただけます。</p>
            </div>
        </div>





      <div class="b03_graph b10_graph">
      <diV class="b10_graph_in">
      	<h2>BNLS&reg;脂肪細胞反応実験</h2>
            <div class="b10_grp">
                  <div class="b10_l">
                  	<img src="img/b10_img01.jpg" width="268" height="175" alt="脂肪細胞" />
                        <p>赤色は脂肪細胞</p>
                  </div>
                  <img src="img/b10_arr.png" width="115" height="55" alt="24時間後" class="sp_none b10arr" />
                  <img src="img/new24sp.jpg" width="100%" alt="24時間後" class="pc_none" />
                  <div class="b10_l">
                  	<img src="img/b10_img02.jpg" width="268" height="175" alt="脂肪細胞" />
                        <p>BNLSⓇ添加後</p>
                  </div>
            </div>
            <img src="img/b10_text.png" width="305" height="39" alt="脂肪細胞の30%が減少" class="b10text" />
      </div>
      </div>

	<img src="img/b11_back.png" width="1201" height="498" alt="フェイス　ボディ" class="sp_none d11" />
	<img src="img/new25sp.jpg" width="100%" alt="フェイス　ボディ" class="pc_none" />

      <div class="b12 b05 cf">
      	<table width="440px" class="b05_l">
            	<tr>
                  	<th>麻酔</th>
                        <td>不要</td>
                  </tr>
                 <tr>
                  	<th>傷跡</th>
                        <td>注射の針跡<br />（時間の経過と共に消えます）</td>
                  </tr>
            	<tr>
                  	<th>洗顔・入浴</th>
                        <td>施術翌日から可能</td>
                  </tr>
            	<tr>
                  	<th>ダウンタイム</th>
                        <td>アザ、腫れ等は少ない</td>
                  </tr>
            </table>
      	<table width="440px" class="b05_r">
            	<tr>
                  	<th>施術時間</th>
                        <td>10～30分</td>
                  </tr>
                 <tr>
                  	<th>通院</th>
                        <td>不要</td>
                  </tr>
            	<tr>
                  	<th>通院</th>
                        <td>施術翌日から可能</td>
                  </tr>
            	<tr>
                  	<th>リバウンド</th>
                        <td>しにくい</td>
                  </tr>
            </table>
      </div>





      <div class="b13 b06">
      	<h2 class="b06_head">BNLS&reg;の優れている特徴</h2>
		<div class="sp_none">
            <div class="b06_li">
            	<div class="b06_00">
                  	<img src="img/b06n_01.png" width="66" height="76" alt="no1" />
                        <dl>
                        	<dt>ダウンタイムが少ない</dt>
                              <dd>BNLS®による顔の輪郭形成では、注入部位に痛み、腫れ、熱感がほとんど発生しないため、ダウンタイムの少ない治療が可能です。</dd>
                        </dl>
                  </div>
            	<div class="b06_00 b06_02">
                  	<img src="img/b06n_02.png" width="76" height="76" alt="no2" />
                        <dl>
                        	<dt>短期で効果を実感</dt>
                              <dd>最短で注射後約3日（72時間）で効果を実感できます。<br /><span>※患者様の状態により効果は異なります。</span></dd>
                        </dl>
                  </div>
            	<div class="b06_00 b06_03">
                  	<img src="img/b06n_03.png" width="68" height="77" alt="no3" />
                        <dl>
                        	<dt>短期間で再施術可能</dt>
                              <dd>1～2週間の間隔で治療を行うことができます。<br /><span>※1回の施術でも効果は期待できますが、患者様の希望により2、3回程度の施術が推奨されます。</span></dd>
                        </dl>
                  </div>
            	<div class="b06_00 b06_04">
                  	<img src="img/b06n_04.png" width="74" height="76" alt="no4" />
                        <dl>
                        	<dt>痛み、アザ、腫れが<br />少ない</dt>
                              <dd>注射の痛みも少なく、アザになりにくく、腫れもほとんどないので、すぐに日常生活に復帰することができます。</dd>
                        </dl>
                  </div>
            </div>
         </div>
      </div>

	<div class="pc_none">
    	<ul>
        	<li><img src="img/new26sp.jpg" width="100%" alt="1 ダウンタイムが少ない" /></li>
        	<li><img src="img/new27sp.jpg" width="100%" alt="2 短期で効果を実感" /></li>
        	<li><img src="img/new28sp.jpg" width="100%" alt="3 短期間で再施術可能" /></li>
        	<li><img src="img/new29sp.jpg" width="100%" alt="4 痛み、アザ、腫れが少ない" /></li>
        </ul>
    </div>



      <div class="b14 cf">
      	<div class="b14_01">
            	<img src="img/b14_img01.jpg" width="200" height="208" alt="BEFORE" />
                <!--<p>コメントがはいります。コメントが入ります。コメントが入ります。</p>-->
            </div>
            <img src="img/b14_arr.png" width="84" height="55" alt="施術後" class="b14_03" />
            <div class="b14_01 b14_02">
            	<img src="img/b14_img02.jpg" width="200" height="208" alt="AFTER" />
                  <p>※効果には個人差があります。</p>
            </div>
      </div>

      <div class="b15 b07">
            <h2 class="b06_head b07_head">BNLS&reg;に関するFAQ</h2>
      	<div class="b07faq00">
                  <p class="b07_q">腫れや赤み、痛みはどのぐらい出ますか？</p>
                  <p class="b07_a">多少の腫れや赤みは出ますが、３～４日程度で引いていきます。痛みについては、注射の当日から鈍痛や筋肉痛のような痛みが２～３日続くことがあります。</p>
            </div>
      	<div class="b07faq00 b07faq02">
                  <p class="b07_q">リバウンドはありませんか？</p>
                  <p class="b07_a">注入された薬剤は脂肪細胞の核に作用し、脂肪細胞の数自体を減少させるため、リバウンドの心配がありません。</p>
            </div>
      	<div class="b07faq00 b07faq03">
                  <p class="b07_q">溶け出した脂肪はどこに行くのですか？</p>
                  <p class="b07_a">体内で分解・溶解された脂肪組織は、血中から腸管を経て、尿や便として体外に排出されます。</p>
            </div>
            <img src="img/b08_bnr.jpg" width="824" height="253" alt="初回限定お試し価格　BNLSキャンペーン価格　1本9,500円　別途初診料が必要です" class="sp_none"/>
            <img src="img/new69sp.jpg" width="100%" alt="初回限定お試し価格　BNLSキャンペーン価格　1本9,500円　別途初診料が必要です" class="pc_none"/>
      </div>

      <div class="b08"></div>

      <div class="sp_none b02info b08info cf">
      	<img class="info01" src="img/info01.jpg" width="533" height="104" alt="" />
            <img class="info02" src="img/info02.png" width="288" height="70" alt="052-962-5155　平日11:00〜19:00/日祝10:00〜17:30" />
            <a href="#section09" class="info03"><img src="img/info03.jpg" width="298" height="61" alt="" /></a>
      </div>

	<div class="pc_none b02info cf">
		<img src="img/new02sp.jpg" width="100%" alt="脂肪溶解注射、BNLS®、ダイエット処方薬、お試しキャンペーン価格" class="mb15"/>
		<a href="tel:0120334270"><img src="img/new03sp.jpg" width="100%" alt="052-962-5155　平日11:00～19:00/日祝10:00～17:30" /></a>
		<a href="#section09" class="info03"><img src="img/new04sp.jpg" width="100%" alt="無料カウンセリングのご予約" /></a>
	</div>
</div>




<div id="section04" class="box">



      <div class="sp_none b16_band b03_band">
      	<div class="b16_band_in b03_band_in">
           		<div class="b16head b03head">
                  	<img src="img/b16en.jpg" width="240" height="13" alt="ダイエット処方薬" />
                        <h2>ダイエット処方薬<span>（服用）</span></h2>
                  </div>
           		<p>ダイエット処方薬とは、体重の減少や、体脂肪の低下を促すお薬です。<br />食欲を抑えるタイプのダイエット薬から、体脂肪をつきにくくするお薬まで幅広くご用意しています。患者様のご希望と症状をお伺いし、最適なお薬を処方しています。</p>
            </div>
      </div>

		<div class="pc_none">
			<img src="img/new30sp.jpg" width="100%" alt="ダイエット処方薬"/>
			<div class="spblock004">
				<p>ダイエット処方薬とは、体重の減少や、体脂肪の低下を促すお薬です。<br />食欲を抑えるタイプのダイエット薬から、体脂肪をつきにくくするお薬まで幅広くご用意しています。患者様のご希望と症状をお伺いし、最適なお薬を処方しています。</p>
            </div>
        </div>




      <div class="b17 b03_graph b10_graph">
      <diV class="b17_in b10_graph_in">
      	<h2>このような方におすすめです。</h2>
            <img src="img/b18_img.jpg" width="751" height="115" alt="自然に痩せたい、すぐにリバウンドしてしまう、深夜のドカ食いがやめられない、着実に体重を減らしたい" class="sp_none"/>
            <img src="img/new32sp.jpg" width="100%" alt="自然に痩せたい、すぐにリバウンドしてしまう、深夜のドカ食いがやめられない、着実に体重を減らしたい" class="pc_none"/>
      </div>
      </div>






      <div class="b18 b04">
      	<div class="sp_none b18_01 b04_01">
            	<dl class="b18_01head b04_01head cf">
                  	<dt>サノレックス</dt>
                  </dl>
                  <p>食欲の抑制、空腹感の解消といった働きをします。副作用として、口渇、便秘、悪心などが考えられますが、重大な問題になる可能性は極めて低く、医師から正しい服用の仕方と適切な指導を受ければ心配はありません。</p>
                  <dl class="b18_01bottom cf">
                  	<dt>サノレックス（1錠）</dt>
                        <dd>キャンペーン価格　540円（税込）～</dd>
                  </dl>
            </div>

		<div class="spblock005 pc_none">
        	<div class="ttl003">◆サノレックス</div>
			<div class="cf">
				<div class="spleft01"><img src="img/new33sp.jpg" width="100%" alt="サノレックス" /></div>
				<div class="spright01">食欲の抑制、空腹感の解消といった働きをします。副作用として、口渇、便秘、悪心などが考えられますが、重大な問題になる可能性は極めて低く、医師から正しい服用の仕方と適切な指導を受ければ心配はありません。</div>

            </div>
			<dl class="b18_01bottom cf">
				<dt>サノレックス（1錠）</dt>
				<dd>キャンペーン価格<br />540円（税込）～</dd>
			</dl>
        </div>






      	<div class="sp_none b18_02 b18_01 b04_01">
            	<dl class="b18_01head b04_01head cf">
                  	<dt>ゼニカル</dt>
                  </dl>
                  <p>ゼニカルは脂肪分解酵素リパーゼの働きを抑えて食べた脂肪の吸収を抑制します。脂肪は便に混じって排泄されます。約30％カットしてくれます。<br />サノレックスと違い、食欲を抑えるのではなく脂肪の吸収をしにくくする働きがあり、カロリーを制限できます。<br />無理な食事制限をしなくていいことから、リバウンドしにくくなります。</p>
                  <dl class="b18_01bottom cf">
                  	<dt>ゼニカル（1錠）</dt>
                        <dd>キャンペーン価格　540円（税込）～</dd>
                  </dl>
            </div>

		<div class="spblock005 pc_none">
        	<div class="ttl003">◆ゼニカル</div>
			<div class="cf">
				<div class="spleft01"><img src="img/new34sp.jpg" width="100%" alt="ゼニカル" /></div>
				<div class="spright01">ゼニカルは脂肪分解酵素リパーゼの働きを抑えて食べた脂肪の吸収を抑制します。脂肪は便に混じって排泄されます。約30％カットしてくれます。<br />サノレックスと違い、食欲を抑えるのではなく脂肪の吸収をしにくくする働きがあり、カロリーを制限できます。<br />無理な食事制限をしなくていいことから、リバウンドしにくくなります。</div>
            </div>
			<dl class="b18_01bottom cf">
				<dt>ゼニカル（1錠）</dt>
				<dd>キャンペーン価格<br />540円（税込）～</dd>
			</dl>
        </div>








      	<div class="sp_none b18_03 b18_01 b04_01">
            	<dl class="b18_01head b04_01head cf">
                  	<dt>カーブトリム<span class="b18_min01">（当院オリジナルサプリ）</span></dt>
                  </dl>
                  <p>カーブトリムは当院のオリジナルサプリメントとして、炭水化物の吸収を抑制します。ご飯やパスタなどから摂取した炭水化物は、体内でαアミラーゼという酵素と結合してグルコースとなり、エネルギーとして燃焼されますが、余った分は脂肪として体内に蓄積されます。カーブトリムに含まれるファセオラミン（白いんげん豆エキス）は、αアミラーゼをブロックする作用があるため、炭水化物の吸収を抑えて脂肪の蓄積を防いでくれるのです。</p>
                  <dl class="b18_01bottom cf">
                  	<dt>カーブトリム（1箱 20錠）</dt>
                        <dd>キャンペーン価格　6000円＋税</dd>
                  </dl>
            </div>
		<div class="spblock005 pc_none">
        	<div class="ttl003">◆カーブトリム<span>（当院オリジナルサプリ）</span></div>
			<div class="cf">
				<div class="spleft01"><img src="img/new35sp.jpg" width="100%" alt="カーブトリム" /></div>
				<div class="spright01">カーブトリムは当院のオリジナルサプリメントとして、炭水化物の吸収を抑制します。ご飯やパスタなどから摂取した炭水化物は、体内でαアミラーゼという酵素と結合してグルコースとなり、エネルギーとして燃焼されますが、余った分は脂肪として体内に蓄積されます。カーブトリムに含まれるファセオラミン（白いんげん豆エキス）は、αアミラーゼをブロックする作用があるため、炭水化物の吸収を抑えて脂肪の蓄積を防いでくれるのです。</div>
            </div>
			<dl class="b18_01bottom cf">
				<dt>カーブトリム（1箱 20錠）</dt>
				<dd>キャンペーン価格<br />6000円＋税</dd>
			</dl>
        </div>









            <div class="sp_none b18_04 b18_01 b04_01">
            	<dl class="b18_01head b04_01head cf">
                  	<dt>BBX</dt>
                  </dl>
                  <p>BBXは、天然原料を用いた食欲抑制サプリです。<br />科学的検証に基づき構成・配合された天然原料が食欲抑制に加え、脂肪の吸収を抑制する効果もあるので食欲が抑えれない方や、代謝が悪い方にお勧めです。<br /><span class="b18_min02">※処方には事前に血液検査が必要な場合があります。（血液検査代別）</span></p>
                  <dl class="b18_01bottom cf">
                  	<dt>BBX（1シート 15錠）</dt>
                        <dd>キャンペーン価格　7500円＋税</dd>
                  </dl>
            </div>

		<div class="spblock005 pc_none">
        	<div class="ttl003">◆BBX</div>
			<div class="cf">
				<div class="spleft01"><img src="img/new36sp.jpg" width="100%" alt="BBX" /></div>
				<div class="spright01">BBXは、天然原料を用いた食欲抑制サプリです。<br />科学的検証に基づき構成・配合された天然原料が食欲抑制に加え、脂肪の吸収を抑制する効果もあるので食欲が抑えれない方や、代謝が悪い方にお勧めです。<br /><span class="b18_min02">※処方には事前に血液検査が必要な場合があります。（血液検査代別）</span></div>
            </div>
			<dl class="b18_01bottom cf">
				<dt>BBX（1シート 15錠）</dt>
				<dd>キャンペーン価格<br />7500円＋税</dd>
			</dl>
        </div>


      </div>




      <div class="b15 b07">
            <h2 class="b06_head b07_head">ダイエット処方薬（服用）に関するFAQ</h2>
      	<div class="b07faq00">
                  <p class="b07_q">サノレックスの働きと、服用のタイミングを教えてください。</p>
                  <p class="b07_a">サノレックスは、食欲中枢に作用して食欲を抑えるお薬です。少ない量の食事でも満腹感を得ることができ、空腹も感じにくくなります。ダイエット中はもちろんのこと、食欲にムラがあり、つい間食や深夜のドカ食いをしてしまうときなどにも服用していただくことができます。１日１回、朝に服用します。</p>
            </div>
      	<div class="b07faq00 b07faq02">
                  <p class="b07_q">ゼニカルの働きと、服用のタイミングを教えてください。</p>
                  <p class="b07_a">ゼニカルは、脂肪の吸収を30％カットしてくれるお薬です。脂っこい食事をしたときや、ダイエット中に友人と焼肉を食べに行ったときなどに服用してください。吸収されなかった脂肪は便として排泄されます。食中または食後１時間以内に服用します。</p>
            </div>
      	<div class="b07faq00 b07faq03">
                  <p class="b07_q">カーブトリムの働きと、服用のタイミングを教えてください。</p>
                  <p class="b07_a">カーブトリムは、炭水化物の吸収を抑えるサプリメントです。ご飯やパスタなどの炭水化物を摂るときや、甘いものを食べるときに服用してください。</p>
            </div>
      </div>

      <div class="b08"></div>




      <div class="sp_none b02info b08info cf">
      	<img class="info01" src="img/info01.jpg" width="533" height="104" alt="" />
            <img class="info02" src="img/info02.png" width="288" height="70" alt="052-962-5155　平日11:00〜19:00/日祝10:00〜17:30" />
            <a href="#section09" class="info03"><img src="img/info03.jpg" width="298" height="61" alt="" /></a>
      </div>

	<div class="pc_none b02info cf">
		<img src="img/new02sp.jpg" width="100%" alt="脂肪溶解注射、BNLS®、ダイエット処方薬、お試しキャンペーン価格" class="mb15"/>
		<a href="tel:0120334270"><img src="img/new03sp.jpg" width="100%" alt="052-962-5155　平日11:00～19:00/日祝10:00～17:30" /></a>
		<a href="#section09" class="info03"><img src="img/new04sp.jpg" width="100%" alt="無料カウンセリングのご予約" /></a>
	</div>




</div>


<div id="section05" class="box">
	<div class="b19">
      	<img src="img/b19_en.jpg" width="191" height="12" alt="" class="sp_none"/>
      	<img src="img/new37sp.jpg" width="100%"  alt="" class="pc_none"/>
      	<h2>お客様の声</h2>

      		<div class="sp_none b19_li cf">
            	<div class="b19_00">
                  	<p>顔に脂肪がつきやすく、体型の割に顔が大きいのが悩みでした。<br>
集合写真などでもいつも後ろの方に並んで顔の大きさをごまかしていました…。BNLSの施術を行ったのですが、どこの箇所に何本しようするか、悩みに合わせて、先生にアドバイスいただけたのでバランスよく仕上がり、大変満足です！術後の腫れもなく、翌日から仕事に行くことができました。</p>
                  </div>
            	<div class="b19_02 b19_00">
                  	<p>結婚式前に、どうしてもキレイになりたくて受診しました。院内はとても明るく清潔な印象で 「衛生管理がちゃんとできてそうな病院だな」と感じました。 施術も思っていたより痛みもなく、あっという間に終わって驚きました。 また何かあるときはよろしくお願いします！</p>
                  </div>
            	<div class="b19_03 b19_00">
                  	<p>何もかも初めてでよくわからず不安でしたが、とても丁寧に相談にのっていただきました。 長いカウンセリングになってしまいましたが、笑顔で応対していただき、納得して施術方法を選ぶことができました。 信頼できるドクターと明るく出迎えてくれるスタッフの皆さんに感謝です！</p>
                  </div>
            </div>


			<div class="pc_none">
				<div class="spblock006 cf">
					<div class="spleft02"><img src="img/new38sp.jpg" width="100%"  alt="お客様の声1" /></div>
					<div class="spright02"><p>顔に脂肪がつきやすく、体型の割に顔が大きいのが悩みでした。<br>
集合写真などでもいつも後ろの方に並んで顔の大きさをごまかしていました…。BNLSの施術を行ったのですが、どこの箇所に何本しようするか、悩みに合わせて、先生にアドバイスいただけたのでバランスよく仕上がり、大変満足です！術後の腫れもなく、翌日から仕事に行くことができました。</p></div>
                </div>
            </div>


			<div class="pc_none">
				<div class="spblock006 cf">
					<div class="spleft03"><p>結婚式前なのでクリニックを受診。院内はとても明るく清潔でキレイで居心地が良かったです。やっぱり衛生管理がきちんとされている病院は安心出来ます。レーザーでしたが施術も痛くなく、あっという間で感激！</p></div>
					<div class="spright03"><img src="img/new39sp.jpg" width="100%"  alt="お客様の声2" /></div>
                </div>
            </div>

			<div class="pc_none">
				<div class="spblock006 cf">
					<div class="spleft02"><img src="img/new40sp.jpg" width="100%"  alt="お客様の声3" /></div>
					<div class="spright02"><p>初めての経験で分らず不安でしたが、とても丁寧に相談にのって頂き大満足。カウンセリングに時間をかけて頂いたので、安心して治療を選択することが出来ました。今まで悩んでいたしみが改善して、化粧にかける時間も減ったのでうれしいです。</p></div>
                </div>
            </div>



      </div>
</div>



      <div class="b08"></div>

      <div class="sp_none b02info b08info cf">
      	<img class="info01" src="img/info01.jpg" width="533" height="104" alt="" />
            <img class="info02" src="img/info02.png" width="288" height="70" alt="052-962-5155　平日11:00〜19:00/日祝10:00〜17:30" />
            <a href="#section09" class="info03"><img src="img/info03.jpg" width="298" height="61" alt="" /></a>
      </div>

	<div class="pc_none b02info cf">
		<img src="img/new02sp.jpg" width="100%" alt="脂肪溶解注射、BNLS®、ダイエット処方薬、お試しキャンペーン価格" class="mb15"/>
		<a href="tel:0120334270"><img src="img/new03sp.jpg" width="100%" alt="052-962-5155　平日11:00～19:00/日祝10:00～17:30" /></a>
		<a href="#section09" class="info03"><img src="img/new04sp.jpg" width="100%" alt="無料カウンセリングのご予約" /></a>
	</div>





<div id="section06" class="box">
      <div class="b20">
      	<img src="img/b20_en.png" width="243" height="12" alt="" class="sp_none"/>
      	<img src="img/new41sp.jpg" width="100%"  alt="" class="pc_none"/>
      	<h2 class="sp_none">お問い合わせから施術までの流れ</h2>
      	<h2 class="pc_none">お問い合わせから<br />施術までの流れ</h2>
            <p class="b20_p">電話、もしくはメールでご予約を承っております。施術のご予約の前に、診療や施術、<br />その他について疑問がありましたら、お電話またはお問い合わせフォーム、メールからお気軽にお問い合わせください。<br />その際には匿名でのお問合せもOKです。</p>





<div class="sp_none b20li">
            	<div class="b20li00">
                  	<img src="img/b20_no1.png" width="89" height="89" alt="step1" class="b20step" />
                  	<dl>
                        	<dt>お電話、メールにてお問い合わせ</dt>
                              <dd>電話、もしくはメールでご予約を承っております。<br />施術のご予約の前に、診療や施術、その他について疑問がありましたら、お電話またはお問い合わせフォーム、メールからお気軽にお問い合わせください。その際には匿名でのお問合せもOKです。</dd>
                        </dl>
                        <div class="b20info">
                        	<img src="img/b20_info01.png" width="94" height="49" alt="お電話でのご相談" class="b20_in01" />
                              <img src="img/b20_info04.png" width="211" height="60" alt="" class="b20_in02" />
                              <a href="#section09" class="b20_in03"><img src="img/b20_info03.png" width="301" height="49" alt="" /></a>
                        </div>
                  </div>
            	<div class="b20li02 b20li00">
                  	<img src="img/b20_no2.png" width="89" height="89" alt="step2" class="b20step" />
                  	<dl>
                        	<dt>ご来院・受付</dt>
                              <dd>お電話でご予約いただいた日程でご来院ください。（日程の変更などはお気軽にご連絡ください）<br />ご予約された日時にお越し下さい。<br />当クリニックでのカウンセリング方法など事前に何でも相談ください。</dd>
                        </dl>
                  </div>
            	<div class="b20li03 b20li00">
                  	<img src="img/b20_no3.png" width="89" height="89" alt="step3" class="b20step" />
                  	<dl>
                        	<dt>スタッフとのカウンセリング</dt>
                              <dd>お客様、患者様の悩みや疑問、施術費用やお支払方法ついてなど、専門のスタッフにお気軽に相談ください。<br />施術へのお手続き・施術申込書の記入、術後の注意事項をお渡しします。<br />
未成年の方は同意書が必要になります。又はご両親のどちらかご同伴でお願いします。</dd>
                        </dl>
                  </div>
            	<div class="b20li04 b20li00">
                  	<img src="img/b20_no4.png" width="89" height="89" alt="step4" class="b20step" />
                  	<dl>
                        	<dt>担当医師とのカウンセリング・診察</dt>
                              <dd>実際に施術を行う担当医師が丁寧にご要望をうかがった上で、プラン、施術方法、施術内容のご説明をいたします。<br />※施術内容の詳細は担当医師がご説明いたします。ご不明な点があればお気軽にご相談ください。</dd>
                        </dl>
                  </div>
            	<div class="b20li05 b20li00">
                  	<img src="img/b20_no5.png" width="89" height="89" alt="step5" class="b20step" />
                  	<dl>
                        	<dt>施術を行います</dt>
                              <dd>お顔の施術の場合は、事前にメイクを落としていただく必要がございます。<br />化粧落としなどご用意しておりますので、お気軽にスタッフにお声掛けください。<br />準備ができましたら施術室に移動していただき、施術を行います。<br />リラックスして施術を受けてください。</dd>
                        </dl>
                  </div>
            	<div class="b20li06 b20li00">
                  	<img src="img/b20_no6.png" width="89" height="89" alt="step6" class="b20step" />
                  	<dl>
                        	<dt>アフターケア</dt>
                              <dd>施術後はすぐにご帰宅頂けます。専用のメイクルームをご用意いたしておりますので、メイク直しをしながら安心して新しいご自分と対面していただけます。施術後のアフターケアの料金は手術料金に含まれております。</dd>
                        </dl>
                  </div>
            </div>




		<div class="pc_none cf sp_block007">
            <div><img src="img/new42sp.jpg" width="100%"  alt="STEP1 お電話、メールにてお問い合わせ"/></div>
			<div class="spleft004"><img src="img/new48sp.jpg" alt="STEP1" width="100%" /></div>
			<div class="spright004">お電話でご予約いただいた日程でご来院ください。（日程の変更などはお気軽にご連絡ください）<br />ご予約された日時にお越し下さい。<br />当クリニックでのカウンセリング方法など事前に何でも相談ください。</div>
        </div>
			<div class="pc_none mt15"><a href="tel:0120334270"><img src="img/new54sp.jpg" alt="お電話でのご相談　052-962-5155" width="100%" /></a></div>
		<div class="pc_none"><a href="#section09"><img src="img/new55sp.jpg" alt="無料カウンセリングのご予約" width="100%" /></a></div>

		<div class="pc_none cf sp_block007">
            <div><img src="img/new43sp.jpg" width="100%"  alt="STEP2 ご来院・受付"/></div>
			<div class="spleft004"><img src="img/new49sp.jpg" alt="STEP2" width="100%" /></div>
			<div class="spright004">お電話でご予約いただいた日程でご来院ください。（日程の変更などはお気軽にご連絡ください）<br />ご予約された日時にお越し下さい。<br />当クリニックでのカウンセリング方法など事前に何でも相談ください。</div>
        </div>


		<div class="pc_none cf sp_block007">
            <div><img src="img/new44sp.jpg" width="100%"  alt="STEP3　スタッフとのカウンセリング"/></div>
			<div class="spleft004"><img src="img/new50sp.jpg" alt="STEP3" width="100%" /></div>
			<div class="spright004">お客様、患者様の悩みや疑問、施術費用やお支払方法ついてなど、専門のスタッフにお気軽に相談ください。<br />施術へのお手続き・施術申込書の記入、術後の注意事項をお渡しします。<br />
未成年の方は同意書が必要になります。又はご両親のどちらかご同伴でお願いします。</div>
        </div>


		<div class="pc_none cf sp_block007">
            <div><img src="img/new45sp.jpg" width="100%"  alt="STEP4　担当医師とのカウンセリング・診察"/></div>
			<div class="spleft004"><img src="img/new51sp.jpg" alt="STEP4" width="100%" /></div>
			<div class="spright004">実際に施術を行う担当医師が丁寧にご要望をうかがった上で、プラン、施術方法、施術内容のご説明をいたします。<br />※施術内容の詳細は担当医師がご説明いたします。ご不明な点があればお気軽にご相談ください。</div>
        </div>


		<div class="pc_none cf sp_block007">
            <div><img src="img/new46sp.jpg" width="100%"  alt="STEP5　施術を行います"/></div>
			<div class="spleft004"><img src="img/new52sp.jpg" alt="STEP5" width="100%" /></div>
			<div class="spright004">お顔の施術の場合は、事前にメイクを落としていただく必要がございます。<br />化粧落としなどご用意しておりますので、お気軽にスタッフにお声掛けください。<br />準備ができましたら施術室に移動していただき、施術を行います。<br />リラックスして施術を受けてください。</div>
        </div>

		<div class="pc_none cf sp_block007">
            <div><img src="img/new47sp.jpg" width="100%"  alt="STEP6　アフターケア"/></div>
			<div class="spleft004"><img src="img/new53sp.jpg" alt="STEP6" width="100%" /></div>
			<div class="spright004">施術後はすぐにご帰宅頂けます。専用のメイクルームをご用意いたしておりますので、メイク直しをしながら安心して新しいご自分と対面していただけます。施術後のアフターケアの料金は手術料金に含まれております。</div>
        </div>



      </div>
      <div class="b08"></div>

      <div class="sp_none b02info b08info cf">
      	<img class="info01" src="img/info01.jpg" width="533" height="104" alt="" />
            <img class="info02" src="img/info02.png" width="288" height="70" alt="052-962-5155　平日11:00〜19:00/日祝10:00〜17:30" />
            <a href="#section09" class="info03"><img src="img/info03.jpg" width="298" height="61" alt="" /></a>
      </div>

	<div class="pc_none b02info cf">
		<img src="img/new02sp.jpg" width="100%" alt="脂肪溶解注射、BNLS®、ダイエット処方薬、お試しキャンペーン価格" class="mb15"/>
		<a href="tel:0120334270"><img src="img/new03sp.jpg" width="100%" alt="052-962-5155　平日11:00～19:00/日祝10:00～17:30" /></a>
		<a href="#section09" class="info03"><img src="img/new04sp.jpg" width="100%" alt="無料カウンセリングのご予約" /></a>
	</div>

</div>








<div id="section07" class="box">
      <div class="b20">
      	<img src="img/b21_en.png" width="247" height="12" alt="" class="sp_none"/>
      	<img src="img/new41sp.jpg" width="100%"  alt="" class="pc_none"/>
      	<h2>院内ご案内</h2>
            <p class="b20_p">当院では安心してご利用頂ける万全の環境を整えておりますので、安心してお越しください。<br />皆さまのご来院をお待ちしております。</p>
	</div>

	<article class="block09 cf">
	<div class="sp_none">
		<ul class="bxslider cf">
                  <li><a href="img/sli_01_big.jpg" rel="lightbox"><img src="img/sli_01.jpg" width="399" height="399" alt="受付" /></a></li>
                  <li><a href="img/sli_02_big.jpg" rel="lightbox"><img src="img/sli_02.jpg" width="399" height="399" alt="待合室" /></a></li>
                  <li><a href="img/sli_03_big.jpg" rel="lightbox"><img src="img/sli_03.jpg" width="399" height="399" alt="カウンセリングルーム" /></a></li>
                  <li><a href="img/sli_04_big.jpg" rel="lightbox"><img src="img/sli_04.jpg" width="399" height="399" alt="診察室" /></a></li>
                  <li><a href="img/sli_05_big.jpg" rel="lightbox"><img src="img/sli_05.jpg" width="399" height="399" alt="診察室" /></a></li>
                  <li><a href="img/sli_06_big.jpg" rel="lightbox"><img src="img/sli_06.jpg" width="399" height="399" alt="施術室" /></a></li>
            </ul>
      </div>
      <div class="pc_none">
            <ul class="bxslider cf">
                  <li><img src="img/sli_01_sp.jpg" width="100%" alt="受付"></li>
                  <li><img src="img/sli_02_sp.jpg" width="100%" alt="待合室"></li>
                  <li><img src="img/sli_03_sp.jpg" width="100%" alt="カウンセリングルーム"></li>
                  <li><img src="img/sli_04_sp.jpg" width="100%" alt="診察室"></li>
                  <li><img src="img/sli_05_sp.jpg" width="100%" alt="診察室"></li>
                  <li><img src="img/sli_06_sp.jpg" width="100%" alt="施術室"></li>
            </ul>
      </div>
      </article>

      <div class="b08"></div>

      <div class="sp_none b02info b08info cf">
      	<img class="info01" src="img/info01.jpg" width="533" height="104" alt="" />
            <img class="info02" src="img/info02.png" width="288" height="70" alt="052-962-5155　平日11:00〜19:00/日祝10:00〜17:30" />
            <a href="#section09" class="info03"><img src="img/info03.jpg" width="298" height="61" alt="" /></a>
      </div>

	<div class="pc_none b02info cf">
		<img src="img/new02sp.jpg" width="100%" alt="脂肪溶解注射、BNLS®、ダイエット処方薬、お試しキャンペーン価格" class="mb15"/>
		<a href="tel:0120334270"><img src="img/new03sp.jpg" width="100%" alt="052-962-5155　平日11:00～19:00/日祝10:00～17:30" /></a>
		<a href="#section09" class="info03"><img src="img/new04sp.jpg" width="100%" alt="無料カウンセリングのご予約" /></a>
	</div>





      <div class="b20 cf">
      	<img src="img/b22_en.jpg" width="267" height="12" alt="" class="sp_none"/>
		<img src="img/new60sp.jpg" width="100%"  alt="" class="pc_none"/>
      	<h2>表参道クニックの理念</h2>
            <p class="b20_p">開業当初から当クリニックでは痛みや不安のない施術を心がけており、<br />患者様に安心して美しくなって頂くことを目指しております。</p>
	</div>


      <div class="b22_li cf">
            <div class="b22_00 cf">
                  <dl class="cf">
                        <dt><img src="img/b22_no1.jpg" width="113" height="35" alt="concept1" /></dt>
                        <dd>患者様とのコミュニケーション<br />を大切にしています。</dd>
                  </dl>
                  <img src="img/b22_01.jpg" width="360" height="150" alt="concept1" />
                  <p>当クリニックは患者様の立場に立ち、今抱えている不安や悩みなどを解消するため、丁寧なカウンセリングを行なっています。</p>
            </div>
            <div class="b22_02 b22_00">
                  <dl class="cf">
                        <dt><img src="img/b22_no2.jpg" width="113" height="35" alt="concept2" /></dt>
                        <dd>長年の経験と実績による<br />確かな技術と高い信頼</dd>
                  </dl>
                  <img src="img/b22_02.jpg" width="360" height="150" alt="concept2" />
                  <p>当クリニックは優秀な美容皮膚科医が多数在籍しています。本当に安心して任せられるドクターがレベルの高い治療を行ないます。</p>
            </div>
            <div class="b22_03 b22_00">
                  <dl class="cf">
                        <dt><img src="img/b22_no3.jpg" width="113" height="35" alt="concept3" /></dt>
                        <dd>いつもドクターが<br />そばにいてくれるという感覚</dd>
                  </dl>
                  <img src="img/b22_03.jpg" width="360" height="150" alt="concept3" />
                  <p>当クリニックは気軽に相談できる環境作りを心がけています。術後のケア、美肌づくりのアドバイスなどいつでもすぐにお応えいたします。</p>
            </div>
      </div>







      <div class="b20 cf">
      	<img src="img/b23_en.jpg" width="265" height="13" alt="doctors introduction" class="sp_none"/>
		<img src="img/new61sp.jpg" width="100%"  alt="doctors introduction" class="pc_none"/>
      	<h2>医師のご紹介</h2>
            <p class="b20_p">当クリニックの医師たちは、お客様に安全に美しくなって頂く為に、日々最高水準の技術を日々研究しております。<br />そんな医師たちをご紹介させていただきます。</p>
	</div>
<div class="b23_box00">
      	<h3><span>統括医療部長</span>　中西 雄二</h3>
        <div class="pc_none tc"><img src="img/b23_img03.jpg" alt="中西 雄二" /></div>
            <div class="b23_in002 b23_in cf">
                  <div class="cf">
                        <div class="b23_in00">
                              <p class="b23_p">経歴</p>
                              <dl class="cf">
                                    <dt>1983年</dt>
                                    <dd>藤田保健衛生大学医学部卒</dd>
                              </dl>
                              <dl class="cf">
                                    <dt>1989年</dt>
                                    <dd>トヨタ記念病院形成外科部長</dd>
                              </dl>
                              <dl class="cf">
                                    <dt>1993年</dt>
                                    <dd>藤田保健衛生大学講師(形成外科)</dd>
                              </dl>
                              <dl class="cf">
                                    <dt>2000年</dt>
                                    <dd>慶應義塾大学助教授 (伊勢慶應病院形成外科)</dd>
                              </dl>
                              <dl class="cf">
                                    <dt>2002年</dt>
                                    <dd>第一なるみ病院形成外科部長</dd>
                                    <dd>大手美容外科院長・医療部長を経て</dd>
                              </dl>
                              <dl class="cf">
                                    <dt>2004年</dt>
                                    <dd>ヴェリテクリニック総院長 就任</dd>
                              </dl>
                              <dl class="cf">
                                    <dt>2015年</dt>
                                    <dd>ワイエススキングループの統括医療部長に就任</dd>
                              </dl>
                              <dl class="cf">
                                    <dt>現　在</dt>
                                    <dd>藤田保健衛生大学形成外科　客員准教授</dd>
                              </dl>

                        </div>
                        <div class="b23_in02 b23_in00 cf">
                              <p class="b23_p">所属学会など</p>
                              <ul class="b23_ul01">
                                    <li>・日本形成外科学会評議員</li>
                                    <li>・日本美容外科学会(JSAS)専門医</li>
                              </ul>
                              <ul class="b23_ul02">
                                    <li>・日本美容外科学会(JSAPS)評議員</li>
                                    <li>・日本頭蓋顎顔面外科学会会員</li>
                              </ul>
                        </div>
                  </div>
                  <div class="b23_in03 b23_in00">
                              <p class="b23_p02 b23_p">中西先生から一言</p>
                              <p>美容外科的手術だけではなく、アンチエイジング機器や薬剤、美肌に対するあらゆる施術について親切、 丁寧に細かい部分のご質問にもご理解いただけるまでご説明するよう心がけています。形成外科学と美容外科学の最先端医療を取り入れ、 インフォームドコンセントには十分に時間を取り、決して無理のない手術を行う事をモットーとしています。
傷跡を含め専門的な知識のない施設で行われた手術に対しての修正術もお気軽にご相談ください。</p>
                  </div>
		</div>
  </div>
	<div class="b23_box00">
      	<h3><span>院長</span>　松木 貴裕</h3>
        <div class="pc_none tc"><img src="img/new62sp.jpg" alt="松木 貴裕" /></div>
            <div class="b23_in cf">
                  <div class="cf">
                        <div class="b23_in00">
                              <p class="b23_p">経歴</p>
                              <dl class="cf">
                                    <dt>平成10年3月</dt>
                                    <dd>藤田保健衛生大学 医学部医学研究所科 卒業</dd>
                              </dl>
                              <dl class="cf">
                                    <dt>平成11年9月</dt>
                                    <dd>医療法人大医会 日進おりど病院 勤務</dd>
                              </dl>
                              <dl class="cf">
                                    <dt>平成14年3月</dt>
                                    <dd>医療法人大医会 日進おりど病院 退職</dd>
                              </dl>
                              <dl class="cf">
                                    <dt>平成14年4月</dt>
                                    <dd>東京女子医科大学病院 皮膚科 勤務</dd>
                              </dl>
                              <dl class="cf">
                                    <dt>平成16年3月</dt>
                                    <dd>東京女子医科大学病院 皮膚科 退職</dd>
                              </dl>
                              <dl class="cf">
                                    <dt>平成16年9月</dt>
                                    <dd>東京美容外科 開設 勤務</dd>
                              </dl>
                        </div>
                        <div class="b23_in02 b23_in00 cf">
                              <p class="b23_p">所属学会など</p>
                              <ul class="b23_ul01">
                                    <li>・日本皮膚科学会正会員</li>
                                    <li>・日本抗加齢学会会員</li>
                              </ul>
                              <ul class="b23_ul02">
                                    <li>・レーザー医学会認定医</li>
                                    <li>・美容外科学会会員</li>
                              </ul>
                        </div>
                  </div>
                  <div class="b23_in03 b23_in00">
                              <p class="b23_p02 b23_p">松木先生から一言</p>
                              <p>かつては、大がかりな美容整形手術など、ハイリスクなイメージがあった美容医療の世界。しかし近年では、手軽にトライできる治療も増え、とても身近な存在になりました。当クリニックでは、高い技術を持つ経験豊富な美容皮膚科医が、患者様の希望や理想を大切にしながら、最新の美容医療を提供いたします。</p>
                  </div>
		</div>
  </div>
	<div class="b23_box00">
      	<h3>有利 新</h3>
        <div class="pc_none tc"><img src="img/new63sp.jpg" alt="有利 新" /></div>
            <div class="b23_in000 b23_in cf">
                  <div class="cf">
                        <div class="b23_in00">
                              <p class="b23_p">経歴</p>
                              <p>東京女子医科大学卒業。<br />同大学病院の内科勤務を経て皮膚科へ転科。<br />現在、都内2か所のクリニックに勤務の傍ら、医師という立場から美容と健康を医療として追求し、美しく生きる為の啓蒙活動を雑誌・TV などで展開中。<br />2004年第36回準ミス日本という経歴をもつ、美貌の新進医師。<br />美と健康に関する著書も多数あり、近著に『女性のキレイと健康をつくる 美肌タイミングジュース』（保健同人社より2011年6月10日発刊）がある。</p>
                        </div>
                        <div class="b23_in02 b23_in00 cf">
                              <p class="b23_p">所属学会など</p>
                              <ul class="b23_ul01">
                                    <li>・日本内科学会会員</li>
                                    <li>・日本皮膚科学会会員</li>
                              </ul>
                              <ul class="b23_ul02">
                                    <li>・日本糖尿病学会会員</li>
                                    <li>・抗加齢学会会員</li>
                              </ul>
                        </div>
                  </div>
                  <div class="b23_in03 b23_in00">
                              <p class="b23_p02 b23_p">友利先生から一言</p>
                              <p>女性が綺麗になりたいという思いは変わらないものです。そんな願いを手軽に、そして確実に医療の技術で叶えたい。そう思いながら日々の診療をさせてもらっています。 一人でも多くの女性の笑顔に自信を持っていただけるように、皆さんの美の主治医としていろいろな悩みにお応えしていきます。
</p>
                  </div>
		</div>
      </div>

      <div class="b08"></div>


      <div class="sp_none b02info b08info cf">
      	<img class="info01" src="img/info01.jpg" width="533" height="104" alt="" />
            <img class="info02" src="img/info02.png" width="288" height="70" alt="052-962-5155　平日11:00〜19:00/日祝10:00〜17:30" />
            <a href="#section09" class="info03"><img src="img/info03.jpg" width="298" height="61" alt="" /></a>
      </div>

	<div class="pc_none b02info cf">
		<img src="img/new02sp.jpg" width="100%" alt="脂肪溶解注射、BNLS®、ダイエット処方薬、お試しキャンペーン価格" class="mb15"/>
		<a href="tel:0120334270"><img src="img/new03sp.jpg" width="100%" alt="052-962-5155　平日11:00～19:00/日祝10:00～17:30" /></a>
		<a href="#section09" class="info03"><img src="img/new04sp.jpg" width="100%" alt="無料カウンセリングのご予約" /></a>
	</div>




</div>






<div id="section08" class="box">

      <div class="b24 b20 cf">
      	<img src="img/b24_en.jpg" width="70" height="12" alt="access" class="sp_none"/>
		<img src="img/new64sp.jpg" width="100%"  alt="access" class="pc_none"/>
      	<h2>アクセス</h2>
	</div>
	<article class="block12 cf">
      <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3241.39689876405!2d139.707413!3d35.667228!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x60188ca3e16206fd%3A0x92146d294d00108b!2z6KGo5Y-C6YGT44K544Kt44Oz44Kv44Oq44OL44OD44Kv!5e0!3m2!1sja!2sjp!4v1433309200061" width="1200" height="350" frameborder="0" style="border:0"></iframe>
      </article>
      <div class="b25_00 cf">
            <div class="b25_01 cf">
                  <h3><img src="img/b24_01.jpg" width="373" height="46" alt="電車でお越しの方" /></h3>
                  <ul>
                        <li>・JR山手線「原宿駅」表参道口より徒歩約5分</li>
                        <li>・東京メトロ各線「表参道駅」A1出口より徒歩約3分</li>
                        <li>・東京メトロ各線「明治神宮前駅」4番出口より徒歩約3分</li>
                  </ul>
            </div>

            <div class="b25_02 cf center">
                  <h3><img src="img/b24_02.jpg" width="373" height="46" alt="お車でお越しの方" /></h3>
                  <p>当クリニック専用の駐車はございません。</p>
                  <p>お近くの有料駐車場のご利用をお願いいたします。</p>
            </div>

            <div class="b25_03 cf">
                  <h3><img src="img/b24_03.jpg" width="373" height="46" alt="診療時間・休診日" /></h3>
                  <div class="sp_none">
                  <dl class="cf">
                        <dt class="mb10"><img src="img/b24_icon01.jpg" width="86" height="22" alt="診療時間" /></dt>
                        <dd>AM11:00～PM8:00(完全予約制)</dd>
                  </dl>
                  <dl class="cf">
                        <dt><img src="img/b24_icon02.jpg" width="86" height="22" alt="休診日" /></dt>
                        <dd>水曜日</dd>
                  </dl>
                  </div>
                  <div class="pc_none mt15"><img src="img/new65sp.jpg" width="100%"  alt="診察時間" /></div>
            </div>
      </div>

      <div class="b08"></div>

      <div class="sp_none b02info b08info cf">
      	<img class="info01" src="img/info01.jpg" width="533" height="104" alt="" />
            <img class="info02" src="img/info02.png" width="288" height="70" alt="052-962-5155　平日11:00〜19:00/日祝10:00〜17:30" />
            <a href="#section09" class="info03"><img src="img/info03.jpg" width="298" height="61" alt="" /></a>
      </div>

	<div class="pc_none b02info cf">
		<img src="img/new02sp.jpg" width="100%" alt="脂肪溶解注射、BNLS®、ダイエット処方薬、お試しキャンペーン価格" class="mb15"/>
		<a href="tel:0120334270"><img src="img/new03sp.jpg" width="100%" alt="052-962-5155　平日11:00～19:00/日祝10:00～17:30" /></a>
		<a href="#section09" class="info03"><img src="img/new04sp.jpg" width="100%" alt="無料カウンセリングのご予約" /></a>
	</div>







<!--フォーム導入-->

  <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/smoothness/jquery-ui.css" >
  <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js"></script>
  <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1/i18n/jquery.ui.datepicker-ja.min.js"></script>
  <script>
  $(function() {
    $("#datepicker_1").datepicker({
      dateFormat: 'yy年mm月dd日 (DD)',
      dayNames: ['日', '月', '火', '水', '木', '金', '土']
    });
    $("#datepicker_2").datepicker({
      dateFormat: 'yy年mm月dd日 (DD)',
      dayNames: ['日', '月', '火', '水', '木', '金', '土']
    });
  });
  </script>
  <div id="section09" name="section09" class="box">

    <article class="block13 cf">
      <img src="img/form_h2.png" width="447" height="60" alt="無料カウンセリングご予約フォーム" class="sp_none" />
      <img src="img/new66sp.jpg" width="100%" alt="無料カウンセリングご予約フォーム" class="pc_none" />
      <p class="pc_none flow"><img src="img/new67sp.jpg" width="100%" alt="流れ" /></p>
		<p class="sp_none flow"><img src="img/form_img01.png" width="1000" height="63" alt="流れ" /></p>
      <div class="form">
        <p class="caution">※お手数ですが、必須項目をすべてご入力の上、確認画面へお進みください。</p>

        <form method="post">

          <table>
            <tr>
              <th class="hissu">お名前</th>
              <td>
                <input type="text" name="name_req" value="<?php echo $formTool->h($_POST['name_req']); ?>"  placeholder="例）山田花子"/>
                <?php if( !empty($formTool->messages['name_req']) ) : ?>
                  <p class="error"><?php echo $formTool->h($formTool->messages['name_req']); ?></p>
                <?php endif; ?>
              </td>
            </tr>
            <tr>
              <th class="hissu">電話番号</th>
              <td>
                <input type="tel" name="tel_req" value="<?php echo $formTool->h($_POST['tel_req']); ?>" placeholder="例）01-2345-6789"/>
                <?php if( !empty($formTool->messages['tel_req']) ) : ?>
                  <p class="error"><?php echo $formTool->h($formTool->messages['tel_req']); ?></p>
                <?php endif; ?>
              </td>
            </tr>
            <tr>
              <th class="hissu">メールアドレス</th>
              <td>
                <input type="email" name="email_req" value="<?php echo $formTool->h($_POST['email_req']); ?>" placeholder="例）info@abc.com"/>
                <?php if( !empty($formTool->messages['email_req']) ) : ?>
                  <p class="error"><?php echo $formTool->h($formTool->messages['email_req']); ?></p>
                <?php endif; ?>
              </td>
            </tr>
            <tr>
              <th class="nini">相談日 第一希望</th>
              <td>
                <input type="text" name="day1" value="<?php echo $formTool->h($_POST['day1']); ?>" placeholder="YYYY年MM月DD日" class="calendar" id="datepicker_1" readonly="readonly"/>
                <select name="day1_period">
                  <option value="">時間帯を選択</option>
                  <?php echo $formTool->makeOptionHtml($formTool->timeArr, 'array', $_POST['day1_period']); ?>
                </select>
              </td>
            </tr>
            <tr>
              <th class="nini">相談日 第二希望</th>
              <td>
                <input type="text" name="day2" value="<?php echo $formTool->h($_POST['day2']); ?>" placeholder="YYYY年MM月DD日" class="calendar" id="datepicker_2" readonly="readonly"/>
                <select name="day2_period">
                  <option value="">時間帯を選択</option>
                  <?php echo $formTool->makeOptionHtml($formTool->timeArr, 'array', $_POST['day2_period']); ?>
                </select>
              </td>
            </tr>
            <tr>
              <th class="nini">連絡方法のご希望</th>
              <td class="radiostyle">
                <?php echo $formTool->makeCheckBoxHtml($formTool->typeArr, 'array', $_POST['type'], 'type'); ?>
              </td>
            </tr>
            <tr>
              <th class="nini last">お問い合わせ内容</th>
              <td class="last">
                <textarea name="message" placeholder="お問い合わせ内容をご記入ください。" cols="20" rows="5"><?php echo $formTool->h($_POST['message']); ?></textarea>
              </td>
            </tr>
          </tbody>
        </table>

        <input type="hidden" name="mode" value="conf" />
        <p class="btn"><input type="image" src="img/form_btn.png" width="378" height="71" alt="確認画面へ" /></p>

      </form>

    </div>
  </article>

<!--フォーム導入-->

<div class="b26">
	<img src="img/b26_text.png" width="479" height="63" alt="ワイエススキンクリニックは、どこまでも綺麗への想いにお応えし続けます" class="sp_none" />
	<img src="img/new68sp.jpg" width="100%" alt="ワイエススキンクリニックは、どこまでも綺麗への想いにお応えし続けます" class="pc_none" />
</div>


<p id="page-top"><a href="#wrapper"><img src="img/top.png" width="48" height="49" alt="TOP"></a></p>
</div>
