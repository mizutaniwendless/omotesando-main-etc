<?php
global $formTool;
?>
<section id="main-img">
  <h2><img src="img/main-img.jpg" alt="アンケートに答えてプレゼントを当てよう。毎月抽選で5名様にみかん5kgをプレゼント　アンケートにお答えいただいた方全員に小芝風花の壁紙プレゼント！"></h2>
</section>

<section id="read-area">
  <h3><img src="img/head_01.png" alt="β-クリプトキサンチンって知ってますか？"></h3>
  <h4><img src="img/subhead_01.png" alt="みかんの健康パワー β-クリプトキサンチン"></h4>
  <img src="img/img.png" alt="" class="right-img">
  <p>みかんに含まれるβ-クリプトキサンチン、トマトのリコペン、ニンジンのβ-カロテンなどカロテノイドという栄養素を人間は合成できず、野菜や果物から摂って、体の中に貯めておかねばなりません。<br>
  みかんの時期に、一日平均2・3個ずつ食べておくと、血中濃度が食べない人の10～20倍になって、私たちの体を守ってくれます。<br>
  守り方には二つあり、(1)活性酸素を減らし、(2)病気になる様々の原因を取り除いてくれます。<br>
  今年の秋、冬は美味しいみかんを楽しみ、一緒にβ-クリプトキサンチンを血中に貯める健康作りに挑戦してみませんか。</p>
  <h4><img src="img/subhead_02.png" alt="β-クリプトキサンチンの健康効果"></h4>
  <p>β-クリプトキサンチンを血中に貯めている人と貯めていない人が患っている病気を比較する大がかりな調査が静岡のみかん産地（浜松市三ケ日町）で行われました。<br>
  その結果によれば、β-クリプトキサンチンの血中濃度が高い人は生活習慣病が明らかに少ない結果でした。<br>
  少なかったのは (1)飲酒や高血糖による肝機能障害、(2)動脈硬化、(3)インスリンの働きが悪くなるインスリン抵抗性、(4)閉経女性の骨密度低下、(5)メタボリックシンドローム、(6)喫煙・飲酒で生ずる活性酸素の害 などでした。<br>
  更に調査開始時の検診で異常がなかった人達のその後を調べると、β-クリプトキサンチン血中濃度の高い人に比べ、低い人では骨密度の低下など異常を生じやすいという結果も得られました。<br>
  みかんを良く食べ、β-クリプトキサンチンを貯めると健康に過ごせそうですね。</p>
  <p>10～20倍になっている人はいろいろな病気が半分以下であることが分かっていますし、健康な人でも観察をつづけると病気になりにくいことが分かってきました。</p>
</section>

<section id="form-area">
  <h3><img src="img/head_02.png" alt="キャンペーン応募入力フォーム"></h3>
  <p><span class="cp">※</span>の項目は必須項目となります。</p>
  <form action="check.html" method="post">
    <table cellpadding="0" cellspacing="0" border="0">
      <tr>
        <th><div>問1</div><span>※</span></th>
        <td>
          みかんは好きですか？<br>
          <?php echo $formTool->makeRadioHtml($formTool->q1Arr, 'array', $_POST['q1'], 'q1'); ?>
          <div class="message"><?php echo $formTool->messages['q1']; ?></div>
        </td>
      </tr>
      <tr>
        <th><div>問2</div><span>※</span></th>
        <td>
          みかんはどのくらいの頻度で食べていますか？<br>
          <?php echo $formTool->makeRadioHtml($formTool->q2Arr, 'array', $_POST['q2'], 'q2'); ?>
          <div class="message"><?php echo $formTool->messages['q2']; ?></div>
        </td>
      </tr>
      <tr>
        <th><div>問3</div><span>※</span></th>
        <td>
          β-クリプトキサンチンを知っていましたか？<br>
          <?php echo $formTool->makeRadioHtml($formTool->q3Arr, 'array', $_POST['q3'], 'q3'); ?>
          <div class="message"><?php echo $formTool->messages['q3']; ?></div>
        </td>
      </tr>
      <tr>
        <th><div>問4</div><span>※</span></th>
        <td>
          みかんの健康効果を知って、みかんの食べる頻度は？<br>
        <?php echo $formTool->makeRadioHtml($formTool->q4Arr, 'array', $_POST['q4'], 'q4'); ?>
        <div class="message"><?php echo $formTool->messages['q4']; ?></div>
      </td>
      </tr>
      <tr>
        <th><div>問5</div><span>※</span></th>
        <td>
          4月より機能性表示制度が変わりました、みかんに機能性表示があったら買いますか？<br>
          <?php echo $formTool->makeRadioHtml($formTool->q5Arr, 'array', $_POST['q5'], 'q5'); ?>
          <div class="message"><?php echo $formTool->messages['q5']; ?></div>
        </td>
      </tr>
      <tr>
        <th><div>問6</div><span>※</span></th>
        <td>
          「毎日くだもの200g運動」がありますが、毎日何グラムくだものを食べていますか？<br>
          <?php echo $formTool->makeRadioHtml($formTool->q6Arr, 'array', $_POST['q6'], 'q6'); ?>
          <br><span class="r-txt">※200gの目安：みかん2個・りんご1個</span>
          <div class="message"><?php echo $formTool->messages['q6']; ?></div>
        </td>
      </tr>
    </table>
    <table cellpadding="0" cellspacing="0" border="0">
      <tr>
        <th><div>お名前(漢字)</div><span>※</span></th>
        <td>
          <span class="l-txt">性</span><input type="text" name="sei" size="20" class="w120" value="<?php echo $formTool->h($_POST['sei']); ?>">
          <span class="l-txt">名</span><input type="text" name="mei" size="20" class="w120" value="<?php echo $formTool->h($_POST['mei']); ?>">
          <span class="r-txt">全角 (例: 山田　太郎)</span>
          <div class="message"><?php echo $formTool->messages['sei']; ?></div>
          <div class="message"><?php echo $formTool->messages['mei']; ?></div>
        </td>
      </tr>
      <tr>
        <th><div>フリガナ</div><span>※</span></th>
        <td>
          <span class="l-txt">セイ</span><input type="text" name="sei_kana" size="20" class="w120" value="<?php echo $formTool->h($_POST['sei_kana']); ?>">
          <span class="l-txt">メイ</span><input type="text" name="mei_kana" size="20" class="w120" value="<?php echo $formTool->h($_POST['mei_kana']); ?>">
          <span class="r-txt">全角 (例: ヤマダ　タロウ)</span>
          <div class="message"><?php echo $formTool->messages['sei_kana']; ?></div>
          <div class="message"><?php echo $formTool->messages['mei_kana']; ?></div>
        </td>
      </tr>
      <tr>
        <th><div>性別</div></th>
        <td>
          <?php echo $formTool->makeRadioHtml($formTool->sexArr, 'array', $_POST['sex'], 'sex'); ?>
          <div class="message"><?php echo $formTool->messages['sex']; ?></div>
        </td>
      </tr>
      <tr>
        <th><div>メールアドレス</div><span>※</span></th>
        <td>
          <input type="text" name="email" size="20" class="w340" value="<?php echo $formTool->h($_POST['email']); ?>"><span class="r-txt">半角</span>
          <div class="message"><?php echo $formTool->messages['email']; ?></div>
        </td>
      </tr>
      <tr>
        <th><div>郵便番号</div><span>※</span></th>
        <td>
          <input name="zip" type="text" maxlength="8" class="w150 text_style02" value="<?php echo $formTool->h($_POST['zip']); ?>" onKeyUp="AjaxZip3.zip2addr(this, '', 'prefecture', 'address1');"><span class="r-txt">半角 (例: 123-4567)</span>
          <div class="message"><?php echo $formTool->messages['zip']; ?></div>
        </td>
      </tr>
      <tr>
        <th><div>都道府県</div><span>※</span></th>
        <td>
          <select name="prefecture" class="w120">
            <option value="">都道府県の選択</option>
            <?php echo $formTool->makeOptionHtml($formTool->prefectureArr, 'array', $_POST['prefecture']); ?>
          </select>
          <div class="message"><?php echo $formTool->messages['prefecture']; ?></div>
        </td>
      </tr>
      <tr>
        <th><div>住所1</div><span>※</span></th>
        <td>
          <input type="text" name="address1" size="20" class="w340" value="<?php echo $formTool->h($_POST['address1']); ?>"><span class="r-txt">全角</span>
          <div class="message"><?php echo $formTool->messages['address1']; ?></div>
        </td>
      </tr>
      <tr>
        <th><div>住所2</div></th>
        <td>
          <input type="text" name="address2" size="20" class="w340" value="<?php echo $formTool->h($_POST['address2']); ?>"><span class="r-txt">全角 (マンション・ビル名)</span>
          <div class="message"><?php echo $formTool->messages['address2']; ?></div>
        </td>
      </tr>
      <tr>
        <th><div>電話番号</div><span>※</span></th>
        <td>
          <input type="text" name="tel" size="20" class="w180" value="<?php echo $formTool->h($_POST['tel']); ?>">
          <span class="r-txt">半角 (例: 03-1234-5678)</span>
          <div class="message"><?php echo $formTool->messages['tel']; ?></div>
        </td>
      </tr>
    </table>
    <div class="btn-area fcb">
      <p class="btn1"><input type="image" src="img/btn_01.png" alt="内容を確認する"></p>
    </div>
    <input type="hidden" name="mode" value="conf" />
  </form>
</section>
