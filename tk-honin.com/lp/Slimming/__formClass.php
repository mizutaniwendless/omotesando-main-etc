<?php
class formClass {
	function __construct() {
		$this->forms = array(
			'q1'=> array(
				'name' => '問1',
				'require' => true,
				'sentaku' =>  true,
			),
			'q2'=> array(
				'name' => '問2',
				'require' => true,
				'sentaku' =>  true,
			),
			'q3'=> array(
				'name' => '問3',
				'require' => true,
				'sentaku' =>  true,
			),
			'q4'=> array(
				'name' => '問4',
				'require' => true,
				'sentaku' =>  true,
			),
			'q5'=> array(
				'name' => '問5',
				'require' => true,
				'sentaku' =>  true,
			),
			'q6'=> array(
				'name' => '問6',
				'require' => true,
				'sentaku' =>  true,
			),
			'sei'=> array(
				'name' => '姓',
				'require' => true,
			),
			'mei'=> array(
				'name' => '名',
				'require' => true,
			),
			'sei_kana'=> array(
				'name' => 'セイ',
				'require' => true,
			),
			'mei_kana'=> array(
				'name' => 'メイ',
				'require' => true,
			),
			'sex' => array(
				'name' => '性別',
			),
			'email' => array(
				'name' => 'メールアドレス',
				'require' => true,
				'email' => true,
			),
			'zip' => array(
				'name' => '郵便番号',
				'require' => true,
				'zip' => true,
			),
			'prefecture' => array(
				'name' => '都道府県',
				'require' => true,
				'sentaku' => true,
			),
			'address1' => array(
				'name' => '住所1',
				'require' => true,
				'zenkaku' => true,
			),
			'address2' => array(
				'name' => '住所2',
				'zenkaku' => true,
			),
			'tel' => array(
				'name' => '電話番号',
				'require' => true,
				'tel' => true,
			),
		);
		$this->messages = array();

		$this->prefectureArr = array(
			'北海道',
			'青森県',
			'岩手県',
			'宮城県',
			'秋田県',
			'山形県',
			'福島県',
			'茨城県',
			'栃木県',
			'群馬県',
			'埼玉県',
			'千葉県',
			'東京都',
			'神奈川県',
			'新潟県',
			'富山県',
			'石川県',
			'福井県',
			'山梨県',
			'長野県',
			'岐阜県',
			'静岡県',
			'愛知県',
			'三重県',
			'滋賀県',
			'京都府',
			'大阪府',
			'兵庫県',
			'奈良県',
			'和歌山県',
			'鳥取県',
			'島根県',
			'岡山県',
			'広島県',
			'山口県',
			'徳島県',
			'香川県',
			'愛媛県',
			'高知県',
			'福岡県',
			'佐賀県',
			'長崎県',
			'熊本県',
			'大分県',
			'宮崎県',
			'鹿児島県',
			'沖縄県',);
			$this->hostname = '(?:[_a-z0-9][-_a-z0-9]*\.)*(?:[a-z0-9][-a-z0-9]{0,62})\.(?:(?:[a-z]{2}\.)?[a-z]{2,})';

			$this->q1Arr = array(
				'好き',
				'きらい',
				'ふつう',
			);
			$this->q2Arr = array(
				'1日2個以上',
				'1日1個程度',
				'1週間に2・3個',
				'1週間に1個程度',
				'全く食べない',
			);

			$this->q3Arr = array(
				'知っていた',
				'知らなかった',
			);

			$this->q4Arr = array(
				'増える',
				'減る',
				'変わらない',
			);

			$this->q5Arr = array(
				'今まで以上に買う',
				'変わらない',
				'買わない',
			);

			$this->q6Arr = array(
				'200g以上',
				'200～100g',
				'100g以下',
				'全く食べない',
			);

			$this->sexArr = array(
				'男性',
				'女性',
			);

		}

		function h($text, $double = true, $charset = null) {
			if (is_array($text)) {
				$texts = array();
				foreach ($text as $k => $t) {
					$texts[$k] = h($t, $double, $charset);
				}
				return $texts;
			} elseif (is_object($text)) {
				if (method_exists($text, '__toString')) {
					$text = (string) $text;
				} else {
					$text = '(object)' . get_class($text);
				}
			} elseif (is_bool($text)) {
				return $text;
			}

			static $defaultCharset = false;
			if ($defaultCharset === false) {
				$defaultCharset = 'UTF-8';
				if ($defaultCharset === null) {
					$defaultCharset = 'UTF-8';
				}
			}
			if (is_string($double)) {
				$charset = $double;
			}
			return htmlspecialchars($text, ENT_QUOTES, ($charset) ? $charset : $defaultCharset, $double);
		}
		function is_inputerror() {
			$ret = false;
			if($_POST['mode'] == 'conf') {
				foreach($this->forms as $key => $form) {
					if($form['require']) {
						if (empty($_POST[$key]) && '0' != $_POST[$key]) {
							$this->messages[$key]= "「{$form['name']}」を入力してください。{$_POST[$key]}";
							$ret = true;
						}
					}
					if($form['require2']) {
						if (empty($_POST[$key]) && '0' != $_POST[$key]) {
							$this->messages[$key]= "「{$form['name']}」にチェックを入れてください。";
							$ret = true;
						}
					}
					if($form['sentaku']) {
						if ((empty($_POST[$key]) && '0' == $_POST[$key]) or ('' == $_POST[$key])) {
							$this->messages[$key]= "「{$form['name']}」を選択してください。";
							$ret = true;
						}
					}
					if($form['policy']) {
						if (empty($_POST[$key])) {
							$this->messages[$key]= "同意される場合はチェックを入れてください。";
							$ret = true;
						}
					}
					if($form['number'] && empty($this->messages[$key])) {
						if(!empty($_POST[$key])) {
							if(!preg_match("/^[0-9]+$/", $_POST[$key])){
								$this->messages[$key]= "「{$form['name']}」は半角数値で入力してください。";
								$ret = true;
							}
						}
					}
					if($form['zip'] && empty($this->messages[$key])) {
						if(!empty($_POST[$key])) {
							if(!preg_match("/^[0-9-]+$/", $_POST[$key])){
								$this->messages[$key]= "「{$form['name']}」は半角数値、ハイフンで入力してください。";
								$ret = true;
							}
						}
					}
					if($form['hankaku'] && empty($this->messages[$key])) {
						if(!empty($_POST[$key])) {
							if(!preg_match("/^[!-~]+$/", $_POST[$key])){
								$this->messages[$key]= "「{$form['name']}」は半角英数で入力してください。";
								$ret = true;
							}
						}
					}
					if($form['zenkaku'] && empty($this->messages[$key])) {
						if(!empty($_POST[$key])) {
							if($_POST[$key] !== mb_convert_kana($_POST[$key], 'ASKH', 'UTF-8')){
								$this->messages[$key]= "「{$form['name']}」は全角で入力してください。";
								$ret = true;
							}
						}
					}
					if($form['email'] && empty($this->messages[$key])) {
						if(!preg_match('/^[a-z0-9!#$%&\'*+\/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&\'*+\/=?^_`{|}~-]+)*@' . $this->hostname . '$/i', $_POST[$key])){
							$this->messages[$key]= "「{$form['name']}」を正しく入力してください。";
							$ret = true;
						}
					}
					if($form['same'] && empty($this->messages[$key])) {
						if($_POST[$key] != $_POST[$form['same']]) {
							$this->messages[$key]= "「{$form['name']}」は確認用と同じ内容を入力してください。";
							$ret = true;
						}
					}
					if($form['other_flg'] && empty($this->messages[$key])) {
						if (empty($_POST['order_content_txt']) && 'その他' == $_POST[$key]) {
							$this->messages[$key]= "「その他」を選択した場合は内容を入力してください。";
							$ret = true;
						}
					}
				}
			}
			return $ret;
		}

		function send_return_mail() {
			$messages = '';
			$subjects = '【日本くだもの農協】ご応募を受け付けました。';
			$messages .= "※このメールは自動返信メールになります。\n";
			$messages .= "この度は日本くだもの農協のアンケートにお答え頂き、\n";
			$messages .= "誠にありがとうございました。\n";
			$messages .= "\n";
			$messages .= "また、みかんプレゼントの当選につきましては、\n";
			$messages .= "商品の発送をもって発表とさせていただいております。\n";
			$messages .= "\n";
			$messages .= $this->get_mail_text_input_part();
			$messages .= "\n";

			mb_language('japanese');
			mb_internal_encoding('UTF-8');
			$mail = "{$_POST['email']}";
			$headers = "MIME-Version: 1.0 \n";
			$headers = "Content-type: text/plain; charset=ISO-2022-JP\n";
			$headers .= "From: ".mb_encode_mimeheader('日本くだもの農協')."<no-reply@kudamono-noukyo.com> \n";//★★★送信元のメールアドレス★★★
			$param   = "t-kosaka@cn-door.com";//★★★送信エラー時のエラーメール送信先★★★
			mb_send_mail($mail, $subjects ,$messages, $headers, $param);
		}

		function send_admin_mail() {
			$wk_cnt = $this->cnt_up();
			$wk_cnt = str_pad($wk_cnt, 4, 0, STR_PAD_LEFT);
			$user_type = $this->user_agent();
			if($user_type == "others"){
				$user_type = "PC";
			}else{
				$user_type = "スマホ";
			}

			$messages = '';
			$subjects = '['.$wk_cnt.']【日本くだもの農協】ご応募を受け付けました。';
			$messages .= "みかんキャンペーンより応募がありました。\n";
			$messages .= "近日中にご対応下さい。\n";
			$messages .= "\n";
			$messages .= $this->get_mail_text_input_part();
			$messages .= "\n";

			mb_language('japanese');
			mb_internal_encoding('UTF-8');
			$mail = "t-kosaka@cn-door.com";//★★★問い合わせ受付先のメールアドレス★★★
			$headers = "MIME-Version: 1.0 \n";
			$headers = "Content-type: text/plain; charset=ISO-2022-JP\n";
			$headers .= "From: ".mb_encode_mimeheader('日本くだもの農協')."<no-reply@kudamono-noukyo.com> \n";//★★★送信元のメールアドレス★★★
			$param   = "t-kosaka@cn-door.com";//★★★送信エラー時のエラーメール送信先★★★
			mb_send_mail($mail, $subjects ,$messages, $headers, $param);
		}

		function get_mail_text_input_part() {
			$messages = '';
			$messages .= "アンケート\n";
			$messages .= "=======================\n";
			$messages .= "問1：{$_POST['q1']}\n";
			$messages .= "問2：{$_POST['q2']}\n";
			$messages .= "問3：{$_POST['q3']}\n";
			$messages .= "問4：{$_POST['q4']}\n";
			$messages .= "問5：{$_POST['q5']}\n";
			$messages .= "問6：{$_POST['q6']}\n";
			$messages .= "=======================\n";
			$messages .= "\n";
			$messages .= "お客様情報\n";
			$messages .= "=======================\n";
			$messages .= "お名前：{$_POST['sei']} {$_POST['mei']}\n";
			$messages .= "フリガナ：{$_POST['sei_kana']} {$_POST['mei_kana']}\n";
			if(!empty($_POST['sex'])){
				$messages .= "性別：{$_POST['sex']}\n";
			}else{
				$messages .= "性別：未入力\n";
			}
			$messages .= "郵便番号：{$_POST['zip']}\n";
			$messages .= "都道府県：{$_POST['prefecture']}\n";
			$messages .= "住所1：{$_POST['address1']}\n";
			if(!empty($_POST['address2'])){
				$messages .= "住所2：{$_POST['address2']}\n";
			}else{
				$messages .= "住所2：未入力\n";
			}
			$messages .= "電話番号：{$_POST['tel']}\n";
			$messages .= "=======================\n";
			$messages .= "\n";


			return $messages;
		}

		/*
		* @param array $options <option>を生成する配列
		* @param string $type $optionsが配列ならarray,連想配列ならhash
		* @param string $selected <option>にselectedを入れる値
		* @return string
		*/
		function makeOptionHtml($options=array(), $type='array', $selected) {
			$optionHtml = '';
			foreach ($options as $key => $val) {
				if ($type = 'array') {
					$key = $val;
				}
				if ($key == $selected){
					$attr = 'selected="selected"';
				}else {
					$attr = '';
				}
				$optionHtml .= "<option value=\"{$key}\" {$attr}>{$val}</option>";
			}
			return $optionHtml;
		}
		function makeCheckBoxHtml($options=array(), $type='array', $selected, $name, $attr='') {
			$optionHtml = '';
			$attr_org = $attr;
			foreach ($options as $key => $val) {
				$attr = $attr_org;
				if ($type == 'array') {
					$key = $val;
				}
				if (in_array($key, $selected)) {
					$attr .= 'checked="checked"';
				}
				$optionHtml .= "<input type=\"checkbox\" name =\"{$name}\" id=\"final_check\" value=\"{$key}\" {$attr}>{$val}";
			}
			return $optionHtml;
		}

		function makeRadioHtml($options=array(), $type='array', $selected, $name, $attr='') {
			$optionHtml = '';
			$attr_org = $attr;
			$cnt = 0;
			foreach ($options as $key => $val) {
				$attr = $attr_org;
				if ($type = 'array') {
					$key = $val;
				}
				if ($key == $selected){
					$attr .= 'checked="checked"';
				}
				$optionHtml .= "<label for=\"{$name}{$cnt}\"><input type=\"radio\" name =\"{$name}\" id=\"{$name}{$cnt}\" value=\"{$key}\" {$attr}>{$val}</label>";
				$cnt++;
			}
			return $optionHtml;
		}
		function cnt_up(){
			$in_cnt = "";
			$cnt_file = "./data/cnt.txt";
			if( !file_exists($cnt_file) ){
				touch( $cnt_file );    		// ファイル作成
				chmod( $cnt_file, 0666 );	// ファイルのパーティションの変更
			}
			$fp = fopen($cnt_file, 'r+');
			if ($fp){
				if (flock($fp, LOCK_EX)){
					$in_cnt = fgets($fp);
					if($in_cnt){
						$in_cnt++;
					}else{
						$in_cnt=1;
					}
					rewind($fp);
					if (fwrite($fp,  $in_cnt) === FALSE){
						print('ファイル書き込みに失敗しました');
					}
					flock($fp, LOCK_UN);
				}
			}
			fclose($fp);
			return  $in_cnt;
		}
		function user_agent(){
			$ua = mb_strtolower($_SERVER['HTTP_USER_AGENT']);
			$return = "";
			if(strpos($ua,'iphone') !== false){
				$return = 'mobile(iphone)';
			}elseif(strpos($ua,'ipod') !== false){
				$return = 'mobile(ipod)';
			}elseif((strpos($ua,'android') !== false) && (strpos($ua, 'mobile') !== false)){
				$return = 'mobile(android)';
			}elseif((strpos($ua,'windows') !== false) && (strpos($ua, 'phone') !== false)){
				$return = 'mobile(windows)';
			}elseif((strpos($ua,'firefox') !== false) && (strpos($ua, 'mobile') !== false)){
				$return = 'mobile(firefox)';
			}elseif(strpos($ua,'blackberry') !== false){
				$return = 'mobile(blackberry)';
			}elseif(strpos($ua,'ipad') !== false){
				$return = 'tablet(ipad)';
			}elseif((strpos($ua,'windows') !== false) && (strpos($ua, 'touch') !== false)){
				$return = 'tablet(windows)';
			}elseif((strpos($ua,'android') !== false) && (strpos($ua, 'mobile') === false)){
				$return = 'tablet(android)';
			}elseif((strpos($ua,'firefox') !== false) && (strpos($ua, 'tablet') !== false)){
				$return = 'tablet(firefox)';
			}elseif((strpos($ua,'kindle') !== false) || (strpos($ua, 'silk') !== false)){
				$return = 'tablet(kindle)';
			}elseif((strpos($ua,'playbook') !== false)){
				$return = 'tablet(playbook)';
			}else{
				$return = 'others';
			}
			return $return;
		}
		function save_csv() {

			$list = array (
			array(
				date('c'),
				$_SERVER['HTTP_USER_AGENT'],
				$_SERVER["REMOTE_ADDR"],
				$_POST['q1'],
				$_POST['q2'],
				$_POST['q3'],
				$_POST['q4'],
				$_POST['q5'],
				$_POST['q6'],
				$_POST['sei'],
				$_POST['mei'],
				$_POST['sei_kana'],
				$_POST['mei_kana'],
				$_POST['sex'],
				$_POST['email'],
				$_POST['zip'],
				$_POST['prefecture'],
				$_POST['address1'],
				$_POST['address2'],
				$_POST['tel'],
			),
		);

		foreach($list as $key => $value ){
			for ($i = 0; $i < count($list[$key]); $i++) {
				if ($i < count($list[$key])-1) {
					$csv_data .= '"'.$value[$i]. '",';
				} else {
					$csv_data .= '"'.$value[$i]. '"';
				}
			}
			$csv_data .= "\n";
		}
		$csv_data = mb_convert_encoding($csv_data, "SJIS-win","UTF-8");
		$csv_file = "./data/file.csv";
		if( !file_exists($csv_file) ){
			touch( $csv_file );    		// ファイル作成
			chmod( $csv_file, 0666 );	// ファイルのパーティションの変更
		}
		$fp = fopen($csv_file, 'a');
		fwrite($fp, $csv_data);
		fclose($fp);
	}

}
