<?php
global $formTool;
?>

<section id="form-area">
  <h3 class="campaign"><img src="img/head_03.png" alt="キャンペーン応募入力確認"></h3>
  <table cellpadding="0" cellspacing="0" border="0">
    <tr>
      <th><div>問1</div><span>※</span></th>
      <td>
        みかんは好きですか？<br>
        <?php echo $formTool->h($_POST['q1']); ?>
      </td>
    </tr>
    <tr>
      <th><div>問2</div><span>※</span></th>
      <td>
        みかんはどのくらいの頻度で食べていますか？<br>
        <?php echo $formTool->h($_POST['q2']); ?>
      </td>
    </tr>
    <tr>
      <th><div>問3</div><span>※</span></th>
      <td>
        β-クリプトキサンチンを知っていましたか？<br>
        <?php echo $formTool->h($_POST['q3']); ?>
      </td>
    </tr>
    <tr>
      <th><div>問4</div><span>※</span></th>
      <td>
        みかんの健康効果を知って、みかんの食べる頻度は？<br>
        <?php echo $formTool->h($_POST['q4']); ?>
      </td>
    </tr>
    <tr>
      <th><div>問5</div><span>※</span></th>
      <td>
        4月より機能性表示制度が変わりました、みかんに機能性表示があったら買いますか？<br>
        <?php echo $formTool->h($_POST['q5']); ?>
      </td>
    </tr>
    <tr>
      <th><div>問6</div><span>※</span></th>
      <td>
        「毎日くだもの200g運動」がありますが、毎日何グラムくだものを食べていますか？<br>
        <?php echo $formTool->h($_POST['q6']); ?>
      </td>
    </tr>
  </table>
  <table cellpadding="0" cellspacing="0" border="0">
    <tr>
      <th><div>お名前(漢字)</div><span>※</span></th>
      <td><?php echo $formTool->h($_POST['sei']); ?> <?php echo $formTool->h($_POST['mei']); ?></td>
    </tr>
    <tr>
      <th><div>フリガナ</div><span>※</span></th>
      <td><?php echo $formTool->h($_POST['sei_kana']); ?> <?php echo $formTool->h($_POST['mei_kana']); ?></td>
    </tr>
    <tr>
      <th><div>性別</div></th>
      <td><?php echo empty($_POST['sex'])? '未入力' : $formTool->h($_POST['sex']); ?></td>
    </tr>
    <tr>
      <th><div>メールアドレス</div><span>※</span></th>
      <td><?php echo $formTool->h($_POST['email']); ?></td>
    </tr>
    <tr>
      <th><div>郵便番号</div><span>※</span></th>
      <td><?php echo $formTool->h($_POST['zip']); ?></td>
    </tr>
    <tr>
      <th><div>都道府県</div><span>※</span></th>
      <td><?php echo $formTool->h($_POST['prefecture']); ?></td>
    </tr>
    <tr>
      <th><div>住所1</div><span>※</span></th>
      <td><?php echo $formTool->h($_POST['address1']); ?></td>
    </tr>
    <tr>
      <th><div>住所2</div></th>
      <td><?php echo empty($_POST['address2'])? '未入力' : $formTool->h($_POST['address2']); ?></td>
    </tr>
    <tr>
      <th><div>電話番号</div><span>※</span></th>
      <td><?php echo $formTool->h($_POST['tel']); ?></td>
    </tr>
  </table>
  <form id="form" method="post" action="mikan2015.html">
    <?php foreach($_POST as $key => $val) { ?>
    <?php if(is_array($val)) { ?>
    <?php foreach($val as $key2 => $val2) { ?>
    <?php echo "<input type=\"hidden\" id=\"form_{$key}[{$key2}]\" name=\"{$key}[{$key2}]\" value=\"{$val2}\">"; ?>
    <?php } ?>
    <?php } else { ?>
    <?php echo "<input type=\"hidden\" id=\"form_{$key}\" name=\"{$key}\" value=\"{$val}\">"; ?>
    <?php } ?>
    <?php } ?>
    <div class="btn-area check fcb">
      <p class="btn2"><img src="img/btn_02.png" alt="戻る"></p>
      <p class="btn1"><img src="img/btn_03.png" alt="内容を送信する"></p>
    </div>
  </form>
  <script>
  $(function() {
  	$('.btn2').on('click', function() {
  		$('#form_mode').val('back');
  		$('#form').submit();
  	});
  	$('.btn1').on('click', function() {
  		$('#form_mode').val('comp');
  		$('#form').submit();
  	});
  });
  </script>
</section>
