<?php
global $formTool;
?>

<script>
//アコーディオン
$(function() {
	$("body").on("click", ".acd", function (){
		$(this).addClass("acd_open");
		$(this).next(".acd_menu").slideToggle();
	});
	$("body").on("click", ".acd_open", function (){
		$(this).removeClass("acd_open");
	});
});
</script>

<div class="cnt_wp">
<!--
<ul id="navi">
	<li><a href="#section01" class="on">●</a></li>
	<li><a href="#section02">●</a></li>
	<li><a href="#section03">●</a></li>
	<li><a href="#section04">●</a></li>
	<li><a href="#section05">●</a></li>
	<li><a href="#section15">●</a></li>
</ul>
-->
<ul id="side_navi">
	<li>
		<img src="img/sidenavi01.jpg" width="68" height="92" alt="ぽってり" />
		<p>
			<span><a href="#ptr_01">BNLS®</a></span><br />
			<span><a href="#ptr_02">ヤングスリフト</a></span>
		</p>
	</li>
	<li>
		<img src="img/sidenavi02.jpg" width="68" height="92" alt="しっかり" />
		<p>
			<span><a href="#skr_01">エラボトックス</a></span>
		</p>
	</li>
	<li>
		<img src="img/sidenavi03.jpg" width="68" height="92" alt="がっしり" />
		<p>
			<span><a href="#gsr_01">ホホの骨削り</a></span><br />
			<span><a href="#gsr_02">エラの骨削り</a></span><br />
			<span><a href="#gsr_03">アゴの骨削り</a></span>
		</p>
	</li>
</ul>

<div id="section01" class="box">
	<article class="block01">
		<h2>
		<img class="sp_none" src="img/main_img.jpg" width="1600" height="725" alt="あなたはどのタイプ？お悩みタイプ別にマッチした施術で憧れの小顔に変身" />
		<img class="pc_none" src="img/sp/main_img.jpg" width="100%" alt="あなたはどのタイプ？お悩みタイプ別にマッチした施術で憧れの小顔に変身" />
		</h2>
	</article>
	<article class="block02">
		<h2>
		<img class="sp_none" src="img/b01_text01.png" width="697" height="37" alt=" 小顔になりたい！タイプ別お顔のお悩み" />
		<img class="pc_none" src="img/sp/b01_text01.gif" width="100%" alt=" 小顔になりたい！タイプ別お顔のお悩み" />
		</h2>
		<div class="top_box inner">
			<ul class="cf">
				<li class="type01">
					<img class="sp_none" src="img/b01_type01.png" width="215" height="290" alt="脂肪やたるみが気になる ぽってりタイプ" />
					<img class="pc_none face" src="img/sp/b01_type01_img.png" width="100%" />
					<dl>
						<img class="pc_none" src="img/sp/b01_type01_text.png" width="100%" alt="脂肪やたるみが気になる ぽってりタイプ" />
						<dt>主な施術方法</dt>
						<dd>
						・BNLS®<br />・ヤングスリフト
						<a href="#section02">
						<img class="sp_none" src="img/linkbtn01_off.gif" width="230" height="44" alt="more" />
						<img class="pc_none" src="img/sp/linkbtn01.gif" width="100%" alt="more" />
						</a>
						</dd>
					</dl>
				</li>
				<li class="type02">
					<img class="sp_none" src="img/b01_type02.png" width="207" height="290" alt="筋肉の張りが気になる しっかりタイプ" />
					<img class="pc_none face" src="img/sp/b01_type02_img.png" width="100%" />
					<dl>
						<img class="pc_none" src="img/sp/b01_type02_text.png" width="100%" alt="筋肉の張りが気になる しっかりタイプ" />
						<dt>主な施術方法</dt>
						<dd>
						・エラボトックス
						<a href="#section03">
						<img class="sp_none" src="img/linkbtn01_off.gif" width="230" height="44" alt="more" />
						<img class="pc_none" src="img/sp/linkbtn01.gif" width="100%" alt="more" />
						</a>
						</dd>
					</dl>
				</li>
				<li class="type03">
					<img class="sp_none" src="img/b01_type03.png" width="212" height="290" alt="骨格が気になる がっしりタイプ" />
					<img class="pc_none face" src="img/sp/b01_type03_img.png" width="100%" />
					<dl>
						<img class="pc_none" src="img/sp/b01_type03_text.png" width="100%" alt="骨格が気になる がっしりタイプ" />
						<dt>主な施術方法</dt>
						<dd>
						・ホホの骨削り<br />・エラの骨削り<br />・アゴの骨削り
						<a href="#section04">
						<img class="sp_none" src="img/linkbtn01_off.gif" width="230" height="44" alt="more" />
						<img class="pc_none" src="img/sp/linkbtn01.gif" width="100%" alt="more" />
						</a>
						</dd>
					</dl>
				</li>
			</ul>
		</div>
		<div class="bottom_box inner">
			<img class="sp_none" src="img/b01_text02.png" width="848" height="56" alt="タイプによって施術方法は異なります。 医師とのカウンセリングでしっかり把握し、お悩みに応じた適正な施術をご提案いたします。" />
			<img class="pc_none" src="img/sp/b01_text02.png" width="100%" alt="タイプによって施術方法は異なります。 医師とのカウンセリングでしっかり把握し、お悩みに応じた適正な施術をご提案いたします。" />
		</div>
	</article>
	<article class="block03 info_box info_box01">
		<ul class="sp_none">
			<li class="tel01"><img src="img/tel_info01.png" width="209" height="44" alt="表参道院 水曜休診 0120-334-270" /></li>
			<li class="tel02"><img src="img/tel_info02.png" width="209" height="44" alt="福岡院 月・火曜休診 0120-931-911" /></li>
			<li class="linkbtn"><a href="#section15"><img src="img/counselingbtn_off.png" width="371" height="85" alt="無料カウンセリングのご予約" /></a></li>
		</ul>
		<ul class="pc_none">
			<li class="linkbtn"><a href="#section15"><img src="img/sp/counselingbtn.png" width="100%" alt="無料カウンセリングのご予約" /></a></li>
			<li class="tel01"><a href="tel:0120334270"><img src="img/tel_info01.png" width="100%" alt="表参道院 水曜休診 0120-334-270" /></a></li>
			<li class="tel02"><a href="tel:0120931911"><img src="img/tel_info02.png" width="100%" alt="福岡院 月・火曜休診 0120-931-911" /></a></li>
		</ul>
	</article>
</div><!--/section01-->


<div id="section02" class="box">
	<article class="block04 ptr">
		<div class="sp_none cate_area">
			<div class="inner">
				<h2><img src="img/ptr_title01.png" width="423" height="110" alt="脂肪やたるみが気になるぽってりタイプ" /></h2>
				<p>お顔の気になる脂肪をピンポイントで除去。<br />
				たるんだ部分を引き上げてお肌にハリとツヤを。<br />
				注射や切らないお顔のリフトアップでスッキリ小顔に。
				</p>
			</div>
		</div>
		<div class="sp_none obi_area">
			<div class="inner">
				<div class="obi_left">
					<h3><img src="img/ptr_title02.png" width="386" height="87" alt="注射で気になる部分の脂肪を溶かすBNLS®" /></h3>
					<p class="cf"><img src="img/ptr_img02.png" width="224" height="195" alt="BNLS®" />BNLS®は、植物から抽出した成分を主成分とし、顔の輪郭形成や部分的痩せのための新しい脂肪溶解注射です。脂肪溶作用・肌の引き締め作用、リンパ循環作用により迅速な効果を実現しました。注射で注入するだけで、メスを使用しないので、安心して施術を受けていただけます。</p>
				</div>
				<div class="obi_right">
					<img src="img/ptr_img01.png" width="477" height="332" alt="施術箇所" />
				</div>
			</div>
		</div>
		<div class="pc_none title_area">
			<h2><img src="img/sp/ptr_vmain01.jpg" width="100%" alt="脂肪やたるみが気になるぽってりタイプ" /></h2>
			<h3><img src="img/sp/ptr_title01.jpg" width="100%" alt="注射で気になる部分の脂肪を溶かすBNLS®" /></h3>
			<img src="img/sp/ptr_text01.jpg" width="100%" alt="BNLS®は、植物から抽出した成分を主成分とし、顔の輪郭形成や部分的痩せのための新しい脂肪溶解注射です。脂肪溶作用・肌の引き締め作用、リンパ循環作用により迅速な効果を実現しました。注射で注入するだけで、メスを使用しないので、安心して施術を受けていただけます。" />
			<img src="img/sp/ptr_img01.jpg" width="100%" alt="施術箇所" />
		</div>
		<div class="boxmain inner" id="ptr_01">
			<div class="box box01">
				<h4>BNLS®の特徴</h4>
				<dl>
					<dt><span>1</span>ダウンタイムが少ない</dt>
					<dd>BNLS®による顔の輪郭形成では、注入部位に痛み、腫れ、熱感がほとんど発生しないため、ダウンタイムの少ない治療が可能です。</dd>
					<dt><span>2</span>短期で効果を実感</dt>
					<dd>最短で注射後約3日（72時間）で効果を実感できます。<br />※患者様の状態により効果は異なります。</dd>
					<dt><span>3</span>短期間で再施術可能</dt>
					<dd>1～2週間の間隔で治療を行うことができます。<br />※1回の施術でも効果は期待できますが、患者様の希望により2、3回程度の施術を推奨しています。</dd>
					<dt><span>4</span>痛み、アザ、腫れが少ない</dt>
					<dd>注射の痛みも少なく、アザになりにくく、腫れもほとんどないので、すぐに日常生活に復帰できます。</dd>
				</dl>
				<div class="cf">
					<table class="tbl_left">
						<tr>
							<th>施術時間</th>
							<td>10～30分</td>
						</tr>
						<tr>
							<th>通院</th>
							<td>不要</td>
						</tr>
						<tr>
							<th>メイク</th>
							<td>施術翌日から可能</td>
						</tr>
						<tr>
							<th>リバウンド</th>
							<td>しにくい</td>
						</tr>
					</table>
					<table class="tbl_right">
						<tr>
							<th>麻酔</th>
							<td>不要</td>
						</tr>
						<tr>
							<th>傷跡</th>
							<td>注射の針跡<br />（時間の経過と共に消えます） </td>
						</tr>
						<tr>
							<th>洗顔・入浴</th>
							<td>施術翌日から可能</td>
						</tr>
						<tr>
							<th>ダウンタイム</th>
							<td>アザ、腫れ等は少ない</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="box box03">
				<h4>BNLS®に関するFAQ</h4>
				<div class="menu">
					<dl>
						<dt class="acd">腫れや赤み、痛みはどのぐらい出ますか？</dt>
						<dd class="acd_menu">多少の腫れや赤みは出ますが、３～４日程度で引いていきます。痛みについては、注射の当日から鈍痛や筋肉痛のような痛みが２～３日続くことがあります。</dd>
					</dl>
					<dl>
						<dt class="acd">リバウンドはありませんか？</dt>
						<dd class="acd_menu">注入された薬剤は脂肪細胞の核に作用し、脂肪細胞の数自体を減少させるため、リバウンドの心配がありません。</dd>
					</dl>
					<dl>
						<dt class="acd">溶け出した脂肪はどこに行くのですか？</dt>
						<dd class="acd_menu">体内で分解・溶解された脂肪組織は、血中から腸管を経て、尿や便として体外に排出されます。</dd>
					</dl>
				</div>
			</div>
		</div><!--/boxmain-->
	</article>
	<article class="block05 inner info_box info_box02 cf">
		<a href="#section15">
		<img class="sp_none" src="img/banner01.png" width="626" height="207" alt="初回限定お試しキャンペーン価格 1dayスリムフェイス 3,950円" />
		<img class="pc_none" src="img/sp/banner01.png" width="100%" alt="初回限定お試しキャンペーン価格 1dayスリムフェイス 3,950円" />
		</a>
		<ul class="sp_none info_right">
			<li class="tel01"><img src="img/tel_info01.png" width="209" height="44" alt="表参道院 水曜休診 0120-334-270" /></li>
			<li class="tel02"><img src="img/tel_info02.png" width="209" height="44" alt="福岡院 月・火曜休診 0120-931-911" /></li>
			<li class="linkbtn"><a href="#section15"><img src="img/counselingbtn_off.png" width="371" height="85" alt="無料カウンセリングのご予約" /></a></li>
		</ul>
		<ul class="pc_none info_right">
			<li class="linkbtn"><a href="#section15"><img src="img/sp/counselingbtn.png" width="100%" alt="無料カウンセリングのご予約" /></a></li>
			<li class="tel01"><a href="tel:0120334270"><img src="img/tel_info01.png" width="100%" alt="表参道院 水曜休診 0120-334-270" /></a></li>
			<li class="tel02"><a href="tel:0120931911"><img src="img/tel_info02.png" width="100%" alt="福岡院 月・火曜休診 0120-931-911" /></a></li>
		</ul>
	</article>
	<article class="block06 ptr">
		<div class="sp_none obi_area">
			<div class="inner">
				<div class="obi_left">
					<h3><img src="img/ptr_title03.png" width="621" height="142" alt="日本では当院系列店のみ施術可能 たるんだ皮膚を引き上げて若々しいお顔に ヤングスリフト®" /></h3>
					<p class="cf">
					お肌のたるみによって輪郭が崩れたり、大きく見える、お肌のツヤやハリがないなどのお悩みにはヤングスリフト®がおすすめです。<br />
					特殊加工された溶ける極細糸を使用して、皮下組織を引き締めます。<br />
					糸の周囲でコラーゲン生成がおこりお肌にハリを与え、引き締めや、たるみ予防にも効果を発揮します。<br />
					ヤングスリフト®はお顔のパーツごとに細かいリフトアップが可能な最先端のフェイスリフトです。<br />
					ヤングスリフトは表参道スキンクリニックだけが認可を受けた施術です。					
					</p>
				</div>
				<div class="obi_right">
					<img src="img/ptr_img03.png" width="395" height="325" alt="施術箇所" />
					<img src="img/ptr_img04.png" width="466" height="146" alt="アメリカ、韓国、日本では特許取得済み40カ国以上で特許申請中の施術です。 （2017年1月現在）" />
				</div>
			</div>
		</div>
		<div class="pc_none title_area">
			<h3><img src="img/sp/ptr_title02.jpg" width="100%" alt="日本では当院系列店のみ施術可能 たるんだ皮膚を引き上げて若々しいお顔に ヤングスリフト®" /></h3>
			<img src="img/sp/ptr_text02.jpg" width="100%" alt="お肌のたるみによって輪郭が崩れたり、大きく見える、お肌のツヤやハリがないなどのお悩みにはヤングスリフト®がおすすめです。" />
			<img src="img/sp/ptr_img02.jpg" width="100%" alt="施術箇所" />
			<img src="img/sp/ptr_img03.jpg" width="100%" alt="アメリカ、韓国、日本では特許取得済み40カ国以上で特許申請中の施術です。 （2017年1月現在）" />
		</div>
		<div class=" boxmain inner" id="ptr_02">
			<div class="box box01">
				<h4>ヤングスリフト®の特徴</h4>
				<dl>
					<dt><span>1</span>通常よりも細く「アンカー」が付いたヤングスリフト®独自の特殊糸</dt>
					<dd>ヤングスリフト®で使用する糸は通常の1/3の細さでありながら引き上げる力は強く、糸の先端についた「アンカー」で引き上げた状態をしっかりと維持します。<br />この「アンカー」はほかのフェイスリフトに使われる糸にはないヤングスリフト独自の技術です。今まで難しいとされていた額、アゴ、チークライン、ネックラインなどの細かな部位別の引き上げも可能です。</dd>
					<dt><span>2</span>コラーゲン、TGFβ（成長因子）の増加でお肌の若返り</dt>
					<dd>ヤングスリフト®は皮膚の内側（真皮層や皮下組織、筋膜下など）に糸を挿入します。糸そのものの牽引作用によるリフトアップ効果と、糸の挿入による刺激で損傷を受けた皮膚内側の組織が修復する際に起こるコラーゲンの生成、TGFβ（成長因子）の増加により、皮膚の弾力アップ、肌つやが良くなる、引き締まるなどの作用によりシワやタルミを改善し小顔効果を発揮します。</dd>
					<dt><span>3</span>各国の認定を受けた高い安全性と信頼の<em>「切らないフェイスリフト治療」</em></dt>
					<dd>ヤングスリフト®は、高い安全性と効果が立証されており、FDA（アメリカの厚労省）、KDFA（韓国の厚労省）、CE（EU各加盟国の基準を満たした安全性の高い商品に与えられるマーク）の厳しい審査をへて認可された製品を使用しています。
また、アメリカ、韓国、日本で特許を取得している安全性、信頼性の高い施術です。（2017年1月現在40ヵ国以上で特許出願中）施術時間は1か所10分程度、腫れも少なく、傷跡も小さいためダウンタイムも少なく、施術直後のポイントメイクも可能です。</dd>
				</dl>
				<div class="cf">
					<table class="tbl_left">
						<tr>
							<th>施術時間</th>
							<td>1ヶ所10分程度</td>
						</tr>
						<tr>
							<th>通院</th>
							<td>不要</td>
						</tr>
						<tr>
							<th>メイク</th>
							<td>ポイントメイクは当日から可能</td>
						</tr>
						<tr>
							<th>持続効果</th>
							<td>約一年半前後で糸は<br />体内に吸収されます</td>
						</tr>
					</table>
					<table class="tbl_right">
						<tr>
							<th>麻酔</th>
							<td>部分麻酔をします</td>
						</tr>
						<tr>
							<th>傷跡</th>
							<td>注射の針跡 <br />（時間の経過と共に消えます）</td>
						</tr>
						<tr>
							<th>洗顔・入浴</th>
							<td>施術翌日から可能</td>
						</tr>
						<tr>
							<th>ダウンタイム</th>
							<td>ほぼありません</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="box box03">
				<h4>ヤングスリフト®に関するFAQ</h4>
				<div class="menu">
					<dl>
						<dt class="acd">キズ跡は残りますか？</dt>
						<dd class="acd_menu">治療直後はメイクで隠せる程度の赤い点状の針跡がありますが、数日間で目立たなくなります。強い力でのマッサージや指圧などは約1ヶ月間程お控えください。</dd>
					</dl>
					<dl>
						<dt class="acd">痛みにはかなり弱いのですが、大丈夫でしょうか？</dt>
						<dd class="acd_menu">治療中は局所麻酔を行うのでご安心ください。施術後は痛み止めの処方も行っておりますのでぜひご相談ください。</dd>
					</dl>
					<dl>
						<dt class="acd">入れた糸はずっと残るのですか？</dt>
						<dd class="acd_menu">挿入した糸は一年半前後で体内に吸収されます。約１ヶ月後に糸の周囲でコラーゲンと成長因子の生成がピークを迎え、徐々に肌そのものの若返り効果が期待できます。</dd>
					</dl>
				</div>
			</div>
		</div><!--/boxmain-->
	</article>

	<article class="block07 inner info_box info_box02 cf">
		<a href="#section15">
		<img class="sp_none" src="img/banner02.png" width="626" height="233" alt="日本では当院系列店のみ施術可能 モニターキャンペーン価格 スタンダード（一部分）10,000円" />
		<img class="pc_none" src="img/sp/banner02.png" width="100%" alt="日本では当院系列店のみ施術可能 モニターキャンペーン価格 スタンダード（一部分）10,000円" />
		</a>
		<ul class="sp_none info_right">
			<li class="tel01"><img src="img/tel_info01.png" width="209" height="44" alt="表参道院 水曜休診 0120-334-270" /></li>
			<li class="tel02"><img src="img/tel_info02.png" width="209" height="44" alt="福岡院 月・火曜休診 0120-931-911" /></li>
			<li class="linkbtn"><a href="#section15"><img src="img/counselingbtn_off.png" width="371" height="85" alt="無料カウンセリングのご予約" /></a></li>
		</ul>
		<ul class="pc_none info_right">
			<li class="linkbtn"><a href="#section15"><img src="img/sp/counselingbtn.png" width="100%" alt="無料カウンセリングのご予約" /></a></li>
			<li class="tel01"><a href="tel:0120334270"><img src="img/tel_info01.png" width="100%" alt="表参道院 水曜休診 0120-334-270" /></a></li>
			<li class="tel02"><a href="tel:0120931911"><img src="img/tel_info02.png" width="100%" alt="福岡院 月・火曜休診 0120-931-911" /></a></li>
		</ul>
	</article>
</div><!--/section02-->


<div id="section03" class="box">
	<article class="block14 skr">
		<div class="sp_none cate_area">
			<div class="inner">
				<h2><img src="img/skr_title01.png" width="420" height="111" alt="筋肉の発達が気になるしっかりタイプ" /></h2>
				<p>筋肉の発達によるエラ張りの改善。<br />
				発達した筋肉の張りを解消し<br />
				スッキリしたフェイスラインで小顔に。
				</p>
			</div>
		</div>
		<div class="sp_none obi_area">
			<div class="inner">
				<div class="obi_left">
					<h3><img src="img/skr_title02.png" width="504" height="91" alt="発達しすぎた筋肉を緩めスッキリフェイスラインエラボトックス注射" /></h3>
					<p class="cf"><img src="img/skr_img01.png" width="150" height="210" alt="ボトックス注射" />えら部分の筋肉は発達していると顔を大きく見せ、「エラ張り顔」になります。ボトックス注射ではエラの筋肉を小さくしたり、張りを解消することで小顔効果が期待できます。「顔が大きくて悩んでいる方」や「脂肪がついていないのに、エラ張り顔で悩んでいる方」にはエラボトックス注射がおすすめです。</p>
				</div>
				<div class="obi_right">
					<img src="img/skr_img02.png" width="504" height="352" alt="BeforeAfter" />
				</div>
			</div>
		</div>
		<div class="pc_none title_area">
			<h2><img src="img/sp/skr_vmain01.jpg" width="100%" alt="筋肉の発達が気になるしっかりタイプ" /></h2>
			<h3><img src="img/sp/skr_title01.jpg" width="100%" alt="発達しすぎた筋肉を緩めスッキリフェイスラインエラボトックス注射" /></h3>
			<img src="img/sp/skr_text01.jpg" width="100%" alt="えら部分の筋肉は発達していると顔を大きく見せ、「エラ張り顔」になります。ボトックス注射ではエラの筋肉を小さくしたり、張りを解消することで小顔効果が期待できます。「顔が大きくて悩んでいる方」や「脂肪がついていないのに、エラ張り顔で悩んでいる方」にはエラボトックス注射がおすすめです。" />
			<img src="img/sp/skr_img01.jpg" width="100%" alt="BeforeAfter" />
		</div>
		<div class="boxmain inner" id="skr_01">
			<div class="box box01">
				<h4>エラボトックスの特徴</h4>
				<dl>
					<dt><span>1</span>発達しすぎた筋肉を緩め、筋肉自体を小さくする</dt>
					<dd>エラの筋肉は噛む動作や口を動かすなど日常的によく使う筋肉です。そのため、自分では中々その筋肉の発達をコントロールすることが難しいです。<br />人によって発達の程度にも差があるため、発達しやすい方にはエラボトックスで筋肉の発達を抑え、筋肉自体を小さくできることは小顔に有効な方法です。</dd>
					<dt><span>2</span>施術時間が短く、副作用が起きにくい</dt>
					<dd>ボトックスは、A型ボツリヌス毒素から抽出した、毒性のない製剤（毒性のないA型ボツリヌストキシン）の事で、筋肉を弛緩させる作用があります。筋肉に注射することにより、弛緩し、小さくします。施術にかかる時間も5～10分と短く、アレルギーなどの副作用もほとんどないため、人気の高い治療です。</dd>
					<dt><span>3</span>認可を受けや信頼性、安全性の高い薬剤を使用</dt>
					<dd>当院では米国アラガン社の日本法人である「アラガンジャパン」が取り扱っている「ボトックスビスタ」を使用しています。2009年に厚生労働大臣によってシワ等の美容領域での使用が承認された、日本国で唯一お墨付きをつけた信頼性と安全性の高い薬剤です。</dd>
				</dl>
				<div class="cf">
					<table class="tbl_left">
						<tr>
							<th>施術時間</th>
							<td>5〜10分</td>
						</tr>
						<tr>
							<th>通院</th>
							<td>不要</td>
						</tr>
						<tr>
							<th>メイク</th>
							<td>ポイントメイクは当日から可能</td>
						</tr>
						<tr>
							<th>リバウンド</th>
							<td>3～7日後から効果が表れ、<br />その後6～8ヶ月持続します</td>
						</tr>
					</table>
					<table class="tbl_right">
						<tr>
							<th>麻酔</th>
							<td>不要</td>
						</tr>
						<tr>
							<th>傷跡</th>
							<td>注射の針跡<br />（時間の経過と共に消えます） </td>
						</tr>
						<tr>
							<th>洗顔・入浴</th>
							<td>施術翌日から可能</td>
						</tr>
						<tr>
							<th>ダウンタイム</th>
							<td>ほぼありません</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="box box03">
				<h4>エラボトックスに関するFAQ</h4>
				<div class="menu">
					<dl>
						<dt class="acd">副作用は本当にないのでしょうか？</dt>
						<dd class="acd_menu">今のところ副作用は報告されていません。<br />ただし、当院では念のため妊娠中や授乳中の方に関しては当院では注射しないようにしております。</dd>
					</dl>
					<dl>
						<dt class="acd">具体的にどこの部分に注射をして、どのように効果があるのでしょうか。</dt>
						<dd class="acd_menu">エラの部分に咬筋という筋肉があります。奥歯を噛み締めるとぽっこり盛り上がる筋肉です。エラが張っている方は、この咬筋が大きく発達している可能性が高いといえます。そこでボトックスという薬剤をその咬筋に直接注射します。すると筋肉が萎縮して小さくなる為、エラがスッキリしてフェイスラインがシャープになり、小顔効果が期待できます。</dd>
					</dl>
					<dl>
						<dt class="acd">小顔治療にBNLS®もありますが、ボトックスとどちらがいいのでしょうか？</dt>
						<dd class="acd_menu">ABNLS®は脂肪溶解注射と呼ばれるものであり、その名の通り脂肪に対する治療です。一方、ボトックス注射は筋肉を萎縮させる目的で行います。<br />小顔の治療を行う際、ご自身が脂肪が多いのか筋肉が多いのかを、まずはカウンセリングします。その上で、筋肉の発達による問題である場合、エラボトックスをご提案いたします。</dd>
					</dl>
				</div>
			</div>
		</div><!--/boxmain-->
	</article>
	<article class="block15 inner info_box info_box02 cf">
		<a href="#section15">
		<img class="sp_none" src="img/banner03.png" width="626" height="207" alt="今ならモニターキャンペーン価格 エラボトックス" />
		<img class="pc_none" src="img/sp/banner03.png" width="100%" alt="今ならモニターキャンペーン価格 エラボトックス" />
		</a>
		<ul class="sp_none info_right">
			<li class="tel01"><img src="img/tel_info01.png" width="209" height="44" alt="表参道院 水曜休診 0120-334-270" /></li>
			<li class="tel02"><img src="img/tel_info02.png" width="209" height="44" alt="福岡院 月・火曜休診 0120-931-911" /></li>
			<li class="linkbtn"><a href="#section15"><img src="img/counselingbtn_off.png" width="371" height="85" alt="無料カウンセリングのご予約" /></a></li>
		</ul>
		<ul class="pc_none info_right">
			<li class="linkbtn"><a href="#section15"><img src="img/sp/counselingbtn.png" width="100%" alt="無料カウンセリングのご予約" /></a></li>
			<li class="tel01"><a href="tel:0120334270"><img src="img/tel_info01.png" width="100%" alt="表参道院 水曜休診 0120-334-270" /></a></li>
			<li class="tel02"><a href="tel:0120931911"><img src="img/tel_info02.png" width="100%" alt="福岡院 月・火曜休診 0120-931-911" /></a></li>
		</ul>
	</article>
</div><!--/section03-->


<div id="section04" class="box">
	<article class="block08 gsr">
		<div class="sp_none cate_area">
			<div class="inner">
				<h2><img src="img/gsr_title01.png" width="424" height="111" alt="骨格が気になるがっしりタイプ" /></h2>
				<p>ホホやエラ、アゴの骨格のせいで顔が大きく見える。<br />骨削りで気になる骨格を整形し理想の小顔に。</p>
			</div>
		</div>
		<div class="sp_none obi_area">
			<div class="inner">
				<div class="obi_left">
					<h3><img src="img/gsr_title02.png" width="338" height="91" alt="骨格を整形し理想の小顔に変身 骨削り" /></h3>
					<p class="cf">顔のかたち（輪郭・フェイスライン）は、骨格および軟部組織（脂肪や筋肉）で構成されています。BN注射などで行う美容施術は浅いところにある軟部組織にアプローチするものですが、骨削り手術では輪郭の土台である骨格そのものを削り、整えます。出っ歯（上顎前突）、ガミースマイル、受け口（下顎前突）、エラ、アゴ（オトガイ）、頬骨、おでこ（前額）のかたちに悩みがあり、骨格レベルでの矯正をお望みのお客様は骨削りがおすすめです。</p>
				</div>
				<div class="obi_right">
					<img src="img/gsr_img01.png" width="478" height="355" alt="施術箇所" />
				</div>
			</div>
		</div>
		<div class="pc_none title_area">
			<h2><img src="img/sp/gsr_vmain01.jpg" width="100%" alt="骨格が気になるがっしりタイプ" /></h2>
			<h3><img src="img/sp/gsr_title01.jpg" width="100%" alt="骨格を整形し理想の小顔に変身 骨削り" /></h3>
			<img src="img/sp/gsr_text01.jpg" width="100%" alt="顔のかたち（輪郭・フェイスライン）は、骨格および軟部組織（脂肪や筋肉）で構成されています。BN注射などで行う美容施術は浅いところにある軟部組織にアプローチするものですが、骨削り手術では輪郭の土台である骨格そのものを削り、整えます。出っ歯（上顎前突）、ガミースマイル、受け口（下顎前突）、エラ、アゴ（オトガイ）、頬骨、おでこ（前額）のかたちに悩みがあり、骨格レベルでの矯正をお望みのお客様は骨削りがおすすめです。" />
			<img src="img/sp/gsr_img01.jpg" width="100%" alt="施術箇所" />
		</div>
		<div class="boxmain inner" id="gsr_01">
			<div class="box box01">
				<h4>頬骨の骨削り<span>（頬骨整形術）</span>の特徴</h4>
				<p class="text cf"><img class="sp_none" src="img/gsr_main_img01.png" width="250" height="230" alt="頬（ホホ）骨" />骨張った頬は顔全体をゴツく男っぽく見せ、また頬が作る影によって老けた印象を与えてしまいがちです。頬骨整形術は、必要以上に突き出た頬骨を削ったりカットすることで、卵形のやさしい面立ちを形成する手術です。<br />「角張った顔を女性らしくしたい」「突き出た頬骨を小さくしたい」「頬骨が出ているのでやつれて見える」といったお悩みをお持ちの方は、ぜひ一度ご相談ください。張り出した頬を削ることで小顔効果が期待できます。</p>
				<img class="pc_none sejutu" src="img/gsr_main_img01.png" width="100%" alt="頬（ホホ）骨" />
				<div class="cf">
					<table class="tbl_left">
						<tr>
							<th>施術時間</th>
							<td>2～3時間</td>
						</tr>
						<tr>
							<th>通院</th>
							<td>翌日・1週間後・1か月後・3か月後</td>
						</tr>
						<tr>
							<th>メイク</th>
							<td class="smole">ポイントメイクは当日から可能ですが1カ月はフェイスバンドを装着する必要があります</td>
						</tr>
						<tr>
							<th>持続効果</th>
							<td>永久</td>
						</tr>
					</table>
					<table class="tbl_right">
						<tr>
							<th>麻酔</th>
							<td>全身麻酔</td>
						</tr>
						<tr>
							<th>傷跡</th>
							<td>目立たない箇所につきます </td>
						</tr>
						<tr>
							<th>洗顔・入浴</th>
							<td>洗顔：当日より<br />入浴：抜糸後より</td>
						</tr>
						<tr>
							<th>ダウンタイム</th>
							<td>3～4週間</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="box box03">
				<h4>頬骨の骨削り<span>（頬骨整形術）</span>に関するFAQ</h4>
				<div class="menu">
					<dl>
						<dt class="acd">頬削りとエラ削りを同時に行うことはできますか？</dt>
						<dd class="acd_menu">はい、可能です。これらを同時に行うことで、ほとんど骨格の目立たないやわらかなフェイスラインとなり、小顔効果も大幅にアップします。</dd>
					</dl> 
					<dl>
						<dt class="acd">手術にかかる時間と痛みについて教えてください。</dt>
						<dd class="acd_menu">施術時間は約２～３時間。全身麻酔を行いますので、手術中の痛みはございません。</dd>
					</dl> 
					<dl>
						<dt class="acd">頬の下にできる影も消えますか？</dt>
						<dd class="acd_menu">頬骨自体が縮小されるため、影はできにくくなります。<br />頬の脂肪が少ないために頬骨が目立ってしまうという場合には、ヒアルロン酸の注入によって頬をふっくらさせるという方法もございます。</dd>
					</dl> 
				</div>
			</div>
		</div><!--/boxmain-->
	</article>
	<article class="block09 inner info_box info_box02 cf">
		<a href="#section15">
		<img class="sp_none" src="img/banner04.png" width="626" height="207" alt="今ならモニターキャンペーン価格 頬骨整形術" />
		<img class="pc_none" src="img/sp/banner04.png" width="100%" alt="今ならモニターキャンペーン価格 頬骨整形術" />
		</a>
		<ul class="sp_none info_right">
			<li class="tel01"><img src="img/tel_info01.png" width="209" height="44" alt="表参道院 水曜休診 0120-334-270" /></li>
			<li class="tel02"><img src="img/tel_info02.png" width="209" height="44" alt="福岡院 月・火曜休診 0120-931-911" /></li>
			<li class="linkbtn"><a href="#section15"><img src="img/counselingbtn_off.png" width="371" height="85" alt="無料カウンセリングのご予約" /></a></li>
		</ul>
		<ul class="pc_none info_right">
			<li class="linkbtn"><a href="#section15"><img src="img/sp/counselingbtn.png" width="100%" alt="無料カウンセリングのご予約" /></a></li>
			<li class="tel01"><a href="tel:0120334270"><img src="img/tel_info01.png" width="100%" alt="表参道院 水曜休診 0120-334-270" /></a></li>
			<li class="tel02"><a href="tel:0120931911"><img src="img/tel_info02.png" width="100%" alt="福岡院 月・火曜休診 0120-931-911" /></a></li>
		</ul>
	</article>
	<article class="block10 gsr" id="gsr_02">
		<div class="boxmain inner">
			<div class="box box01">
				<h4>エラ削りの特徴</h4>
				<p class="text cf"><img class="sp_none" src="img/gsr_main_img03.png" width="247" height="230" alt="エラ骨" />エラの骨を削り取ることによって、角張ったフェイスラインをやわらかなイメージの卵型へと整えていきます。正面からよりも横から見たときにエラの突起が目立つ方は、この方法が適しているといえるでしょう。張り出したエラを削ることによって得られる小顔効果でも、大変ご好評をいただいている施術です。</p>
				<img class="pc_none sejutu" src="img/gsr_main_img03.png" width="100%" alt="エラ骨" />
			</div>
			<div class="box box03">
					<h4>エラ削りに関するFAQ</h4>
				<div class="menu">
					<dl>
						<dt class="acd">エラを削る際の痛みや腫れについて教えてください。</dt>
						<dd class="acd_menu">全身麻酔を行うので施術中の痛みはありませんが、術後２～４週間ほどは痛みや腫れが続く場合がございます。また、術後一時的に会話や食事がしづらくなるケースもありますが、腫れが引くとともに自然に解消されていきます。</dd>
					</dl> 
					<dl>
						<dt class="acd">ボトックス注射によるエラの改善と、エラ削りはどう違うのですか？</dt>
						<dd class="acd_menu">発達した筋肉によってエラが横に張り出している場合は、ボトックス注射による筋肉へのアプローチが効果的です。一方、横から見たときにエラのラインがくっきりと出ている場合は、骨削りによってエラのラインをなだらかにする方法が向いています。また、ボトックス注射の場合は定期的な注入が必要ですが、エラ削りは半永久的に効果が持続します。</dd>
					</dl> 
				</div>
			</div>
		</div><!--/boxmain-->
	</article>
	<article class="block11 inner info_box info_box02 cf">
		<a href="#section15">
		<img class="sp_none" src="img/banner05.png" width="626" height="207" alt="今ならモニターキャンペーン価格 エラ削り" />
		<img class="pc_none" src="img/sp/banner05.png" width="100%" alt="今ならモニターキャンペーン価格 エラ削り" />
		</a>
		<ul class="sp_none info_right">
			<li class="tel01"><img src="img/tel_info01.png" width="209" height="44" alt="表参道院 水曜休診 0120-334-270" /></li>
			<li class="tel02"><img src="img/tel_info02.png" width="209" height="44" alt="福岡院 月・火曜休診 0120-931-911" /></li>
			<li class="linkbtn"><a href="#section15"><img src="img/counselingbtn_off.png" width="371" height="85" alt="無料カウンセリングのご予約" /></a></li>
		</ul>
		<ul class="pc_none info_right">
			<li class="linkbtn"><a href="#section15"><img src="img/sp/counselingbtn.png" width="100%" alt="無料カウンセリングのご予約" /></a></li>
			<li class="tel01"><a href="tel:0120334270"><img src="img/tel_info01.png" width="100%" alt="表参道院 水曜休診 0120-334-270" /></a></li>
			<li class="tel02"><a href="tel:0120931911"><img src="img/tel_info02.png" width="100%" alt="福岡院 月・火曜休診 0120-931-911" /></a></li>
		</ul>
	</article>
	<article class="block12 gsr" id="gsr_03">
		<div class="boxmain inner">
			<div class="box box01">
				<h4>アゴ削りの特徴</h4>
				<p class="text cf"><img class="sp_none" src="img/gsr_main_img05.png" width="251" height="230" alt="顎（アゴ）骨" />頬骨やエラと並んで、顔の輪郭形成に大きな影響を及ぼすのがアゴです。ただし、ひとくちにアゴのお悩みといっても、その内容はさまざま。なかでも、「長いアゴを短くしたい」「面長で悩んでいる」「前に出たアゴを何とかしたい」といった方に有効なのが、下アゴの先端の骨を削る手術です。<br />現在、アゴを短くするためには、アゴの骨を削るしか手段はありません。アゴ削りの手術は傷を目立たせないよう、口の中から行うことが可能です。また長さを整えるだけではなく、前に突き出したアゴを削ることもできます。</p>
				<img class="pc_none sejutu" src="img/gsr_main_img05.png" width="100%" alt="顎（アゴ）骨" />
			</div>
			<div class="box box03">
				<h4>アゴ削りに関するFAQ</h4>
				<div class="menu">
					<dl>
						<dt class="acd">アゴ削りの手術にかかる時間を教えてください。</dt>
						<dd class="acd_menu">施術時間は90～120分ほどです。全身麻酔を行いますので、手術中の痛みはありません。</dd>
					</dl> 
					<dl>
						<dt class="acd">しゃくれたアゴを引っ込めることもできますか？</dt>
						<dd class="acd_menu">アゴ削りは長さの短縮だけでなく、前に出たアゴの先を削ることもできる手術です。ご要望やお悩みがございましたら、カウンセリング時にお気軽にお問い合わせください。</dd>
					</dl> 
				</div>
			</div>
		</div><!--/boxmain-->
	</article>
	<article class="block21 inner info_box info_box02 cf">
		<a href="#section15">
		<img class="sp_none" src="img/banner06.png" width="626" height="207" alt="今ならモニターキャンペーン価格 アゴ削り" />
		<img class="pc_none" src="img/sp/banner06.png" width="100%" alt="今ならモニターキャンペーン価格 アゴ削り" />
		</a>
		<ul class="sp_none info_right">
			<li class="tel01"><img src="img/tel_info01.png" width="209" height="44" alt="表参道院 水曜休診 0120-334-270" /></li>
			<li class="tel02"><img src="img/tel_info02.png" width="209" height="44" alt="福岡院 月・火曜休診 0120-931-911" /></li>
			<li class="linkbtn"><a href="#section15"><img src="img/counselingbtn_off.png" width="371" height="85" alt="無料カウンセリングのご予約" /></a></li>
		</ul>
		<ul class="pc_none info_right">
			<li class="linkbtn"><a href="#section15"><img src="img/sp/counselingbtn.png" width="100%" alt="無料カウンセリングのご予約" /></a></li>
			<li class="tel01"><a href="tel:0120334270"><img src="img/tel_info01.png" width="100%" alt="表参道院 水曜休診 0120-334-270" /></a></li>
			<li class="tel02"><a href="tel:0120931911"><img src="img/tel_info02.png" width="100%" alt="福岡院 月・火曜休診 0120-931-911" /></a></li>
		</ul>
	</article>
</div><!--/section04-->


<div id="section05" class="box">
	<article class="block16">
		<p>
		<img class="sp_none" src="img/yoyaku_text.png" width="692" height="105" alt="美容外科手術の前に入念な医師とのカウンセリングを行って、施術について納得いくまでご相談ください。まずはお気軽に無料カウンセリングのご予約からどうぞ。" />
		<img class="pc_none" src="img/sp/yoyaku_text.png" width="100%" alt="美容外科手術の前に入念な医師とのカウンセリングを行って、施術について納得いくまでご相談ください。まずはお気軽に無料カウンセリングのご予約からどうぞ。" />
		</p>
	</article>
	<article class="block17">
		<div class="inner">
			<h2>
			<img class="sp_none" src="img/koe_title.png" width="153" height="30" alt="お客様の声" />
			<img class="pc_none" src="img/sp/koe_title.png" width="100%" alt="お客様の声" />
			</h2>
			<ul class="cf">
				<li>
					<img class="sp_none" src="img/koe_img01.png" width="81" height="121" alt="human" /> 
					<img class="pc_none human" src="img/sp/koe_img01.png" width="100%" alt="human" /> 
					<p>美容治療は今回が初めてでしたが、スタッフの皆さんが温かい雰囲気で、とてもリラックスさせてくれたので、緊張せずにすみました。不安や疑問に思ったことも相談出来る環境です。アフターケアも充実していて大満足！</p>
				</li>
				<li class="koe02">
					<img class="sp_none" src="img/koe_img02.png" width="78" height="122" alt="human" />
					<p>結婚式前なのでクリニックを受診。院内はとても明るく清潔でキレイで居心地が良かったです。やっぱり衛生管理がきちんとされている病院は安心出来ます。施術も痛くなく、あっという間で感激！</p>
					<img class="pc_none human" src="img/sp/koe_img02.png" width="100%" alt="human" /> 
				</li>
				<li>
					<img class="sp_none" src="img/koe_img03.png" width="74" height="120" alt="human" />
					<img class="pc_none human" src="img/sp/koe_img03.png" width="100%" alt="human" /> 
					<p>初めての経験で分らず不安でしたが、とても丁寧に相談にのって頂き大満足。カウンセリングに時間をかけて頂いたので、安心して治療を選択することが出来ました。今まで悩んでいたたるみが改善して、化粧にかける時間も減ったのでうれしいです。</p>
				</li>
			</ul>
		</div>
	</article>
	<article class="block18 info_box info_box01">
		<ul class="sp_none">
			<li class="tel01"><img src="img/tel_info01.png" width="209" height="44" alt="表参道院 水曜休診 0120-334-270" /></li>
			<li class="tel02"><img src="img/tel_info02.png" width="209" height="44" alt="福岡院 月・火曜休診 0120-931-911" /></li>
			<li class="linkbtn"><a href="#section15"><img src="img/counselingbtn_off.png" width="371" height="85" alt="無料カウンセリングのご予約" /></a></li>
		</ul>
		<ul class="pc_none">
			<li class="linkbtn"><a href="#section15"><img src="img/sp/counselingbtn.png" width="100%" alt="無料カウンセリングのご予約" /></a></li>
			<li class="tel01"><a href="tel:0120334270"><img src="img/tel_info01.png" width="100%" alt="表参道院 水曜休診 0120-334-270" /></a></li>
			<li class="tel02"><a href="tel:0120931911"><img src="img/tel_info02.png" width="100%" alt="福岡院 月・火曜休診 0120-931-911" /></a></li>
		</ul>
	</article>
	<article class="block19">
		<div class="inner">
			<h2>
			<img class="sp_none" src="img/flow_title.png" width="466" height="31" alt="お問い合わせから施術までの流れ" />
			<img class="pc_none" src="img/sp/flow_title.png" width="100%" alt="お問い合わせから施術までの流れ" />
			</h2>
			<p class="text01">電話、もしくはメールでご予約を承っております。施術のご予約の前に、診療や施術、<br />その他について疑問がありましたら、お電話またはお問い合わせフォーム、メールからお気軽にお問い合わせください。<br />その際には匿名でのお問合せもOKです。</p>
			<div class="flow_box fb01">
				<h3>
				<img class="sp_none" src="img/flow_step01.png" width="881" height="87" alt="step01 お電話、メールにてお問い合わせ" />
				<img class="pc_none" src="img/sp/flow_step01.png" width="100%" alt="step01 お電話、メールにてお問い合わせ" />
				</h3>
				<img class="photo" src="img/flow_img01.png" width="221" height="195" />
				<p>電話、もしくはメールでご予約を承っております。<br />施術のご予約の前に、診療や施術、その他について疑問がありましたら、お電話またはお問い合わせフォーム、メールからお気軽にお問い合わせください。その際には匿名でのお問合せもOKです。</p>
				<div class="link">
					<img class="sp_none flow_tel" src="img/flow_tel.png" width="320" height="49" alt="お電話でのご相談 0120-334-270" />
					<a class="sp_none" href="#section15"><img src="img/counselingbtn_off.png" width="290" height="auto" alt="無料カウンセリングのご予約" /></a>
					<a class="pc_none" href="tel:0120334270"><img class="flow_tel" src="img/sp/flow_tel.png" width="100%" alt="お電話でのご相談 0120-334-270" /></a>
					<a class="pc_none" href="#section15"><img src="img/sp/counselingbtn.png" width="100%" alt="無料カウンセリングのご予約" /></a>
				</div>
			</div>
			<div class="flow_box fb02">
				<h3>
				<img class="sp_none" src="img/flow_step02.png" width="881" height="87" alt="step02 ご来院・受付" />
				<img class="pc_none" src="img/sp/flow_step02.png" width="100%" alt="step02 ご来院・受付" />
				</h3>
				<img class="photo" src="img/flow_img02.png" width="221" height="165" />
				<p>お電話でご予約いただいた日程でご来院ください。（日程の変更などはお気軽にご連絡ください）<br />ご予約された日時にお越し下さい。<br />当クリニックでのカウンセリング方法など事前に何でも相談ください。</p>
			</div>
			<div class="flow_box fb03">
				<h3>
				<img class="sp_none" src="img/flow_step03.png" width="881" height="87" alt="step03 スタッフとのカウンセリング" />
				<img class="pc_none" src="img/sp/flow_step03.png" width="100%" alt="step03 スタッフとのカウンセリング" />
				</h3>
				<img class="photo" src="img/flow_img03.png" width="221" height="165" />
				<p>客様、患者様の悩みや疑問、施術費用やお支払方法ついてなど、専門のスタッフにお気軽に相談ください。<br />施術へのお手続き・施術申込書の記入、術後の注意事項をお渡しします。<br />未成年の方は同意書が必要になります。又はご両親のどちらかご同伴でお願いします。<br />※認印（シャチハタ以外）をお持ち下さい。</p>
			</div>
			<div class="flow_box fb04">
				<h3>
				<img class="sp_none" src="img/flow_step04.png" width="881" height="87" alt="step04 担当医師とのカウンセリング・診察" />
				<img class="pc_none" src="img/sp/flow_step04.png" width="100%" alt="step04 担当医師とのカウンセリング・診察" />
				</h3>
				<img class="photo" src="img/flow_img04.png" width="221" height="165" />
				<p>実際に施術を行う担当医師が丁寧にご要望をうかがった上で、プラン、施術方法、施術内容のご説明をいたします。<br />※施術内容の詳細は担当医師がご説明いたします。ご不明な点があればお気軽にご相談ください。</p>
			</div>
			<div class="flow_box fb05">
				<h3>
				<img class="sp_none" src="img/flow_step05.png" width="881" height="87" alt="step05 施術を行います" />
				<img class="pc_none" src="img/sp/flow_step05.png" width="100%" alt="step05 施術を行います" />
				</h3>
				<img class="photo" src="img/flow_img05.png" width="221" height="165" />
				<p>お顔の施術の場合は、事前にメイクを落としていただく必要がございます。<br />化粧落としなどご用意しておりますので、お気軽にスタッフにお声掛けください。<br />準備ができましたら施術室に移動していただき、施術を行います。<br />リラックスして施術を受けてください。</p>
			</div>
			<div class="flow_box fb06">
				<h3>
				<img class="sp_none" src="img/flow_step06.png" width="881" height="87" alt="step06 アフターケア" />
				<img class="pc_none" src="img/sp/flow_step06.png" width="100%" alt="step06 アフターケア" />
				</h3>
				<img class="photo" src="img/flow_img06.png" width="221" height="165" />
				<p>施術後はすぐにご帰宅頂けます。<br />専用のメイクルームをご用意いたしておりますので、メイク直しをしながら安心して新しいご自分と対面していただけます。施術後のアフターケアの料金は手術料金に含まれております。<br />また、施術後しばらく経ってご不安な点や不明瞭な点などがございましたら、お気軽に電話、またはメールにて直接ご相談ください。</p>
			</div>
		</div>
	</article>
	<article class="block20 info_box info_box01">
		<ul class="sp_none">
			<li class="tel01"><img src="img/tel_info01.png" width="209" height="44" alt="表参道院 水曜休診 0120-334-270" /></li>
			<li class="tel02"><img src="img/tel_info02.png" width="209" height="44" alt="福岡院 月・火曜休診 0120-931-911" /></li>
			<li class="linkbtn"><a href="#section15"><img src="img/counselingbtn_off.png" width="371" height="85" alt="無料カウンセリングのご予約" /></a></li>
		</ul>
		<ul class="pc_none">
			<li class="linkbtn"><a href="#section15"><img src="img/sp/counselingbtn.png" width="100%" alt="無料カウンセリングのご予約" /></a></li>
			<li class="tel01"><a href="tel:0120334270"><img src="img/tel_info01.png" width="100%" alt="表参道院 水曜休診 0120-334-270" /></a></li>
			<li class="tel02"><a href="tel:0120931911"><img src="img/tel_info02.png" width="100%" alt="福岡院 月・火曜休診 0120-931-911" /></a></li>
		</ul>
	</article>
	<article class="block22">
		<div class="inner">
			<h2>
			<img class="sp_none" src="img/rinen_title.png" width="401" height="31" alt="表参道スキンクリニックの理念" />
			<img class="pc_none" src="img/sp/rinen_title.png" width="100%" alt="表参道スキンクリニックの理念" />
			</h2>
			<p class="text">開業当初から当クリニックでは痛みや不安のない施術を心がけており、<br />患者様に安心して美しくなって頂くことを目指しております。</p>
			<ul class="cf">
				<li class="Concept01">
					<h3>
					<img class="sp_none" src="img/rinen_con01.png" width="335" height="39" alt="Concept01患者様とのコミュニケーションを大切にしています。" />
					<img class="pc_none" src="img/sp/rinen_con01.png" width="100%" alt="Concept01患者様とのコミュニケーションを大切にしています。" />
					</h3>
					<img src="img/rinen_img01.png" width="331" height="137" alt="Concept01" />
					<p>当クリニックは患者様の立場に立ち、今抱えている不安や悩みなどを解消するため、丁寧なカウンセリングを行なっています。</p>
				</li>
				<li class="Concept02">
					<h3>
					<img class="sp_none" src="img/rinen_con02.png" width="308" height="39" alt="Concept02長年の経験と実績による確かな技術と高い信頼" />
					<img class="pc_none" src="img/sp/rinen_con02.png" width="100%" alt="Concept02長年の経験と実績による確かな技術と高い信頼" />
					</h3>
					<img src="img/rinen_img02.png" width="331" height="137" alt="Concept02" />
					<p>当クリニックは優秀な美容皮膚科医が多数在籍しています。本当に安心して任せられるドクターがレベルの高い治療を行ないます。</p>
				</li>
				<li class="Concept03">
					<h3>
					<img class="sp_none" src="img/rinen_con03.png" width="326" height="39" alt="Concept03いつもドクターがそばにいてくれるという感覚" />
					<img class="pc_none" src="img/sp/rinen_con03.png" width="100%" alt="Concept03いつもドクターがそばにいてくれるという感覚" />
					</h3>
					<img src="img/rinen_img03.png" width="331" height="137" alt="Concept03" />
					<p>当クリニックは気軽に相談できる環境作りを心がけています。術後のケア、美肌づくりのアドバイスなどいつでもすぐにお応えいたします。</p>
				</li>
			</ul>
		</div>
	</article>
</div><!--/section05-->



<?php include '../tabs/index.html'; ?>



<!--フォーム導入-->
  <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/smoothness/jquery-ui.css" >
  <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js"></script>
  <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1/i18n/jquery.ui.datepicker-ja.min.js"></script>
  <script>
  $(function() {
    $("#datepicker_1").datepicker({
      dateFormat: 'yy年mm月dd日 (DD)',
      dayNames: ['日', '月', '火', '水', '木', '金', '土']
    });
    $("#datepicker_2").datepicker({
      dateFormat: 'yy年mm月dd日 (DD)',
      dayNames: ['日', '月', '火', '水', '木', '金', '土']
    });
  });
  </script>
  <div id="section15" name="section15" class="box">

    <div class="block13 cf">
      <h2 class="mt20"><img src="images/form/title10.gif" width="1200" height="60" alt="無料カウンセリングご予約フォーム" class="sp_none"><img src="images/form/title10_sp.gif" width="100%" alt="無料カウンセリングご予約フォーム" class="pc_none"></h2>
      <p class="flow"><img src="images/form/flow_01.gif" alt="1．情報の入力" width="1000" height="80" /></p>
      <div class="form">
        <p class="caution">※お手数ですが、必須項目をすべてご入力の上、確認画面へお進みください。</p>

        <form method="post">

          <table>
            <tr>
              <th class="hissu">お名前（ひらがな）</th>
              <td>
                <input type="text" name="name_req" value="<?php echo $formTool->h($_POST['name_req']); ?>" />　例）おもてさんどうはなこ
                <?php if( !empty($formTool->messages['name_req']) ) : ?>
                  <p class="error"><?php echo $formTool->h($formTool->messages['name_req']); ?></p>
                <?php endif; ?>
              </td>
            </tr>
            <tr>
              <th class="hissu">電話番号</th>
              <td>
                <input type="tel" name="tel_req" value="<?php echo $formTool->h($_POST['tel_req']); ?>" />　例）01-2345-6789
                <?php if( !empty($formTool->messages['tel_req']) ) : ?>
                  <p class="error"><?php echo $formTool->h($formTool->messages['tel_req']); ?></p>
                <?php endif; ?>
              </td>
            </tr>
            <tr>
              <th class="hissu">メールアドレス</th>
              <td>
                <input type="email" name="email_req" value="<?php echo $formTool->h($_POST['email_req']); ?>" />　例）info@abc.com
                <?php if( !empty($formTool->messages['email_req']) ) : ?>
                  <p class="error"><?php echo $formTool->h($formTool->messages['email_req']); ?></p>
                <?php endif; ?>
              </td>
            </tr>
            <tr>
              <th class="hissu">年齢</th>
              <td>
                <span><input type="text" name="age_req" value="<?php echo $formTool->h($_POST['age_req']); ?>" class="number" /> 歳</span>　例）30歳
                <?php if( !empty($formTool->messages['age_req']) ) : ?>
                  <p class="error"><?php echo $formTool->h($formTool->messages['age_req']); ?></p>
                <?php endif; ?>
              </td>
            </tr>
            <tr>
              <th class="hissu">ご希望のクリニック</th>
              <td>
                <?php echo $formTool->makeRadioHtml($formTool->hopeArr, 'array', $_POST['hope_req'], 'hope_req'); ?>
                <?php if( !empty($formTool->messages['hope_req']) ) : ?>
                  <p class="error"><?php echo $formTool->h($formTool->messages['hope_req']); ?></p>
                <?php endif; ?>
              </td>
            </tr>
            <tr>
              <th class="nini">相談日 第一希望</th>
              <td>
                <input type="text" name="day1" value="<?php echo $formTool->h($_POST['day1']); ?>" placeholder="YYYY年MM月DD日" class="calendar" id="datepicker_1" readonly/>
                <select name="day1_period">
                  <option value="">時間帯を選択</option>
                  <?php echo $formTool->makeOptionHtml($formTool->timeArr, 'array', $_POST['day1_period']); ?>
                </select>
              </td>
            </tr>
            <tr>
              <th class="nini">相談日 第二希望</th>
              <td>
                <input type="text" name="day2" value="<?php echo $formTool->h($_POST['day2']); ?>" placeholder="YYYY年MM月DD日" class="calendar" id="datepicker_2" readonly/>
                <select name="day2_period">
                  <option value="">時間帯を選択</option>
                  <?php echo $formTool->makeOptionHtml($formTool->timeArr, 'array', $_POST['day2_period']); ?>
                </select>
              </td>
            </tr>
            <tr>
              <th class="nini">連絡方法のご希望</th>
              <td class="radiostyle">
                <?php echo $formTool->makeCheckBoxHtml($formTool->typeArr, 'array', $_POST['type'], 'type'); ?>
              </td>
            </tr>
            <tr>
              <th class="nini last">お問い合わせ内容</th>
              <td class="last">
                <textarea name="message" placeholder="例）ボトックスでしわを目立たなくさせたいです。" cols="20" rows="5"><?php echo $formTool->h($_POST['message']); ?></textarea>
              </td>
            </tr>
          </tbody>
        </table>

        <input type="hidden" name="mode" value="conf" />
        <p class="btn"><input type="image" src="images/form/formbtn_01.gif" alt="確認画面へ" width="420" height="70" /></p>

      </form>

    </div>
  </div>
</div>
<!--フォーム導入-->



<article class="block23">
	<img class="sp_none" src="img/bottom_text.png" width="479" height="65" alt="表参道スキンクリニックは、 どこまでも綺麗への想いにお応えし続けます" />
	<img class="pc_none" src="img/sp/bottom_text.png" width="100%" alt="表参道スキンクリニックは、 どこまでも綺麗への想いにお応えし続けます" />
</article>
<p id="page-top"><a href="#wrapper"><img src="img/top.gif" width="48" height="46" alt="TOP"></a></p>

</div><!--/cnt_wp-->

