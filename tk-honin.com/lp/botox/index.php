<?php
require('formClass.php');
$formTool = new formClass();
$mode = (empty($_POST['mode'])) ? "" : $_POST['mode'] ;
$error = false;
if($mode == 'comp') {
  $error = $formTool->is_inputerror();
  //完了画面のリロードで再送させないようにリダイレクト
  if(!$error){
    $formTool->send_return_mail();
    $formTool->send_admin_mail();
    header('Location: ./index_end.html');
  }
} else if ($mode == 'conf') {
  $error = $formTool->is_inputerror();
}
?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<title>表参道スキンクリニック | ボトックス注入で小じわに悩まない若々しい肌へ</title>
<meta name="Keywords" content="表参道スキンクリニック,ボトックス,小じわ,シワ,若返り" /> 
<meta name="Description" content="表参道スキンクリニックでは、BOTOX VISTAを使用し、お顔のシワ取り、エラ張り、美脚、ワキや手足の多汗治療を丁寧に行っております。おでこ＆目まわりのシワ、鼻＆鼻まわりのシワ、顎＆首まわりのシワの改善は表参道スキンクリニックへお任せください。" /> 
<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0">

<link rel="stylesheet" type="text/css" href="css/common.css" media="screen,print" />
<link rel="stylesheet" type="text/css" href="css/style.css" media="screen,print" />
<link rel="stylesheet" type="text/css" href="css/style_sp.css" media="screen,print" />
<link rel="stylesheet" type="text/css" href="css/lightbox.css" media="screen,print" />
<link rel="stylesheet" type="text/css" href="css/jquery.bxslider.css" media="screen,print">


<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>

<script src="js/jquery.page-scroller-309.js"></script>
<script src="js/rollover.js"></script>


<script type="text/javascript" src="js/jquery.bxslider.js"></script>
<script type="text/javascript" src="js/lightbox.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $('#slider').bxSlider({
        auto: true,
        pause: 5000,
        speed: 1000,
        pager: false,
        displaySlideQty: 3, //一画面に表示する数
        moveSlideQty: 1, //移動時にずれる数
        prevText: '<',
        nextText: '>'
        });
  });
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-59451323-1', 'auto');
  ga('send', 'pageview');

</script>
</head>
<body id="wrap">
	<div id="h_wrapper">
      <header id="PAGE_TOP">
        <h1><img src="images/header_logo.png" width="249" height="45" alt="表参道スキンクリニック　Omotesando Skin Clinic"></h1>
        <p><img src="images/header_tel2.jpg" width="450" height="59" alt="0120-334-270 受付時間11:00～20:00・日祝10:30～18:00/水曜休診" class="sp_none"><a href="tel:0120334270" onclick="javascript:goog_report_conversion('tel:0120-334-270');yahoo_report_conversion(undefined);ga('send','event','sp','tel-tap','tel');return false;" class="pc_none"><!--<img src="images/header_tel_sp.png" width="100%" alt="0120-334-270 受付時間11:00～20:00・日祝10:30～18:00/水曜休診" style="z-index:9999;">--></a></p>
        <ul class="cf">
          <li class="sp_right"><a href="#section15"><img src="images/header_btn01_off.gif" width="249" height="40" alt="無料カウンセリングのご予約" class="sp_none"><img src="images/header_btn01_sp2.gif" width="100%" alt="無料カウンセリングのご予約" class="pc_none"></a></li>
          <li class="sp_ab"><a href="#access_box"><img src="images/header_btn02_off.gif" width="112" height="40" alt="アクセス" class="sp_none"><img src="images/header_btn02_sp.gif" width="100%" alt="アクセス" class="pc_none"></a></li>

<li class="sp_ab1"><a href="tel:0120334270"><img src="images/header_btn02_sp1.gif" width="100%" class="pc_none" alt="表参道院"></a></li>

<li class="sp_ab2"><a href="tel:0120931911"><img src="images/header_btn02_sp2.gif" width="100%" class="pc_none" alt="福岡院"></a></li>
        </ul>
      </header>
    </div>



    <?php if(basename($_SERVER['REQUEST_URI']) == 'comp.html') : ?>
      <?php include('comp.php'); ?>
    <?php elseif($_POST['mode'] == 'conf' && !$error) : ?>
      <?php include('conf.php'); ?>
    <?php else : ?>
      <?php include('edit.php'); ?>
      <?php if($error) : ?>
        <script type="text/javascript">
        window.location.hash = 'section15';
        </script>
      <?php endif; ?>
    <?php endif; ?>


<!-- YDN用リマーケティングタグ -->
<script type="text/javascript" language="javascript">
/* <![CDATA[ */
var yahoo_retargeting_id = 'A15O0FYNPU';
var yahoo_retargeting_label = '';
/* ]]> */
</script>
<script type="text/javascript" language="javascript" src="//b92.yahoo.co.jp/js/s_retargeting.js"></script>
</body>
</html>
