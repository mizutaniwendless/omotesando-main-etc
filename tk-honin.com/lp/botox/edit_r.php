<?php
global $formTool;
?>
	<div id="main">
		<h1><img src="images/botox_main_title.gif" width="355" height="222" alt="表参道スキンクリニックのボトックスはデザインセンスでシワを作らせない綺麗な肌へ" class="sp_none"><img src="images/sp/botox_main.jpg" alt="表参道スキンクリニックのボトックスはデザインセンスでシワを作らせない綺麗な肌へ" width="100%" class="pc_none"></h1>
	</div><!-- main !-->
	
	<div id="contents01">
		<div class="wrapper">
			<h2 id="title01"><img src="images/botox_contents01_title.gif" width="570" height="104" alt="表参道スキンクリニックのボトックスはデザインセンスでシワを作らせない綺麗な肌へ" class="sp_none" /><img src="images/sp/botox_common_title_icon.gif" alt="" width="5%" height="1" class="imgbdr pc_none"><img src="images/sp/botox_contents01_title.gif" alt="表参道スキンクリニックのボトックスはデザインセンスでシワを作らせない綺麗な肌へ" width="100%" class="pc_none"></h2>
		</div>
		<div class="contents01_box">
			<div class="contents01_bg01">
				<div class="pc_none"><img src="images/sp/botox_contents01_img01.jpg" alt="" width="100%"></div>
				<div class="text">
					<p>美容外科の治療では、<br class="pc_none">デザインセンスがとても大切です。<br />美しいお顔への施術となると<br />デザインには特に敏感でいたいものです。</p>
					<p>表情シワに効果がある<br class="pc_none">ボトックス注入の場合も同じです。</p>
					<p>シワが消えた後の美しい肌のデザインは<br class="pc_none">その量の加減で決まります。</p>
				</div>
			</div>
			<div class="contents01_bg02">
				<div class="wrapper">
					<div class="sub_title pc_none">
						<img src="images/sp/botox_common_title_icon.gif" alt="" width="5%" height="1">
					</div>
					<div class="pc_none"><img src="images/sp/botox_contents01_img02.jpg" alt="" width="100%"></div>
					<div class="text_inner">
						<p>当院の医師は豊富な経験はもとより日頃より数多くの施術例を研究し施術のクオリティを<br />高めることに余念がありません。</p>
						<p>カウンセリング力にも定評があり、ボトックス注入においてはお一人お一人の状態に合わせて、<br class="sp_none" />美しいデザインをご提案させて頂いております。</p>
						<p>大切なお顔の事は表参道スキンクリニックにお任せ下さい。</p>
						<div class="textbox">
							<div class="inner">
								<div class="title"><img src="images/botox_contents01_text01.gif" width="227" height="12" alt="表参道スキンクリニック［所属学会］" class="sp_none" /><img src="images/sp/botox_contents01_text01.gif" alt="表参道スキンクリニック［所属学会］" width="100%" class="pc_none"></div>
								<ul class="cf">
									<li>・日本皮膚科学会正会員</li>
									<li>・日本抗加齢学会会員</li>
									<li>・レーザー医学会認定医</li>
									<li>・美容外科学会会員</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>

	<div class="conversion cf">
		<div class="label pc_none"><img src="images/sp/botox_cv_01.gif" alt="お電話でのご相談" width="100%"></div>
		<div class="tel">
			<img src="images/botox_cv_tel.gif" width="212" height="92" alt="お電話でのご相談 0120-334-270 OPEN / 11:00-20:00" class="sp_none" />
			<a href="tel:0120334270" onclick="javascript:goog_report_conversion('tel:0120-334-270');yahoo_report_conversion(undefined);ga('send','event','sp','tel-tap','tel');return false;" class="pc_none"><img src="images/sp/botox_cv_02.gif" alt="0120-334-270" width="100%"></a>
		</div>
		<div class="last pc_none"><img src="images/sp/botox_cv_03.gif" alt="OPEN / 11:00-20:00" width="100%"></div>
		<div class="btn"><a href="#section09" class="sp_none"><img src="images/botox_cv_btn_off.png" width="309" height="66" alt="無料カウンセリングのご予約は こちらから" /></a><a href="#section09" class="pc_none"><img src="images/sp/botox_cv_btn.gif" alt="無料カウンセリングのご予約はこちらから" width="100%"></a></div>
	</div>
	
<div class="wrapper">
	<div id="content02" class="contents02">
		<div class="cf">
			<div class="contents02_box">
				<h2 id="title02"><img src="images/botox_contents02_title.gif" width="660" height="149" alt="当院のボトックス施術では、BOTOX VISTAを使用し、お顔のシワ取り、エラ張り、美脚、ワキや手足の多汗治療を丁寧におこなっております。" class="sp_none" /><img src="images/sp/botox_common_title_icon.gif" alt="" width="5%" height="1" class="imgbdr pc_none"><img src="images/sp/botox_contents02_title.gif" alt="当院のボトックス施術では、BOTOX VISTAを使用し、お顔のシワ取り、エラ張り、美脚、ワキや手足の多汗治療を丁寧におこなっております。" width="100%" class="pc_none"></h2>
				<div class="contenst02_image pc_none"><img src="images/sp/botox_contents02_img01.jpg" width="100%" alt="BOTOX VISTA" /></div>
				<p>一般的なボトックスの注入個所はお顔のシワですが、<br />切らない小顔術として凝り固まったエラや痩せにくいふくらはぎにも注入が可能です。</p>
				<p>筋肉の働きを抑制するのと同じ作用で、汗腺の働きも抑え、匂いの元となる汗を出させません。<br />
				個人差はありますが、約6ヶ月効果が持続するため、毎年薄着になり始める夏前に施術を受ければ<br />ひと夏サラサラのワキを持続できます。</p>
			</div>
			<div class="contenst02_itemimage"><img src="images/botox_contents02_img01.jpg" width="399" height="390" alt="BOTOX VISTA" /></div>
		</div>
		<h3 id="subtitle01"><img src="images/botox_contents02_title02.gif" width="1200" height="30" alt="OPERATION 施術について" class="sp_none" /><img src="images/sp/botox_contents02_text01.gif" alt="OPERATION 施術について" width="100%" class="pc_none"></h3>
		<div class="cf">
			<div class="contents02_detail">
				<div><img src="images/botox_contents02_text01.gif" width="395" height="43" alt="施術概要" class="sp_none" /><img src="images/sp/botox_contents02_text02.gif" alt="施術概要" width="100%" class="pc_none"></div>
				<table cellpadding="0" cellspacing="0" border="0">
					<tr>
						<th class="box01">施術時間</th>
						<td class="box01">
							<ul>
								<li>シワ改善・エラはり改善<br />15～30分</li>
								<li>わきが、多汗治療・ふくらはぎ痩身<br />40～50分</li>
							</ul>
						</td>
					</tr>
					<tr>
						<th class="box02">痛み</th>
						<td class="box02">チクッとした軽い痛み</td>
					</tr>
					<tr>
						<th class="box02">ダウンタイム</th>
						<td class="box02">ほとんどありません</td>
					</tr>
					<tr>
						<th class="box02">メイク</th>
						<td class="box02">施術直後から可能</td>
					</tr>
					<tr>
						<th class="box02">接続時間</th>
						<td class="box02">約6ヶ月</td>
					</tr>
					<tr>
						<th class="box02">即効性</th>
						<td class="box02">3日～1週間後より徐々に効果が表れる</td>
					</tr>
				</table>
			</div>
			<div class="contenst02_detailitem_inner">
				<div class="contenst02_detailitem">
					<div><img src="images/botox_contents02_img02.jpg" width="719" height="282" alt="A：皮膚 B：皮下組織" class="sp_none" /><img src="images/sp/botox_contents02_img02.jpg" alt="A：皮膚 B：皮下組織" width="100%" class="pc_none"></div>
					<p>ボトックスは、筋肉の緊張をほぐすことで、シワを解消するメカニズムのシワ治療です。</p>
				</div>
			</div>
		</div>
		<h3 id="subtitle02"><img src="images/botox_contents02_title03.gif" width="246" height="51" alt="BEFORE - AFTER" class="sp_none" /><img src="images/sp/botox_common_title_icon.gif" alt="" width="5%" height="1" class="imgbdr pc_none"><img src="images/sp/botox_contents02_text03.gif" alt="BEFORE - AFTER" width="100%" class="pc_none"></h3>
		<ul class="contents02_beforeafter cf">
			<li class="w583">
				<h4><img src="images/botox_contents02_text02.gif" width="583" height="28" alt="眉間のシワとり" class="sp_none" /><img src="images/sp/botox_contents02_text04.gif" alt="眉間のシワとり" width="100%" class="pc_none"></h4>
				<div><img src="images/botox_contents02_img03.jpg" width="583" height="154" alt="" class="sp_none" /><img src="images/sp/botox_contents02_img03.jpg" alt="" width="100%" class="pc_none"><img src="images/sp/botox_contents02_img04.jpg" alt="" width="100%" class="pc_none"></div>
			</li>
			<li class="w274">
				<h4><img src="images/botox_contents02_text03.gif" width="274" height="28" alt="額のシワとり" class="sp_none" /><img src="images/sp/botox_contents02_text05.gif" alt="額のシワとり" width="100%" class="pc_none"></h4>
				<div><img src="images/botox_contents02_img04.jpg" width="274" height="154" alt="" class="sp_none" /><img src="images/sp/botox_contents02_img05.jpg" alt="" width="100%" class="pc_none"></div>
			</li>
			<li class="w273">
				<h4><img src="images/botox_contents02_text04.gif" widh="273" height="28" alt="目尻のシワとり" class="sp_none" /><img src="images/sp/botox_contents02_text06.gif" alt="目尻のシワとり" width="100%" class="pc_none"></h4>
				<div><img src="images/botox_contents02_img05.jpg" width="273" height="154" alt="" class="sp_none" /><img src="images/sp/botox_contents02_img06.jpg" alt="" width="100%" class="pc_none"></div>
			</li>
			<li>
				<h4><img src="images/botox_contents02_text05.gif" width="1200" height="28" alt="ふくらはぎ痩せ" class="sp_none" /><img src="images/sp/botox_contents02_text07.gif" alt="ふくらはぎ痩せ" width="100%" class="pc_none"></h4>
				<div><img src="images/botox_contents02_img06.jpg" width="583" height="184" alt="" class="sp_none" /><img src="images/sp/botox_contents02_img07.jpg" alt="" width="100%" class="pc_none"><img src="images/sp/botox_contents02_img08.jpg" alt="" width="100%" class="pc_none"></div>
			</li>
		</ul>
	</div>

	<div class="conversion02 cf">
		<div class="label pc_none"><img src="images/sp/botox_cv_01.gif" alt="お電話でのご相談" width="100%"></div>
		<div class="tel">
			<img src="images/botox_cv_tel.gif" width="212" height="92" alt="お電話でのご相談 0120-334-270 OPEN / 11:00-20:00" class="sp_none" />
			<a href="tel:0120334270" onclick="javascript:goog_report_conversion('tel:0120-334-270');yahoo_report_conversion(undefined);ga('send','event','sp','tel-tap','tel');return false;" class="pc_none"><img src="images/sp/botox_cv_02.gif" alt="0120-334-270" width="100%"></a>
		</div>
		<div class="last pc_none"><img src="images/sp/botox_cv_03.gif" alt="OPEN / 11:00-20:00" width="100%"></div>
		<div class="btn"><a href="#section09" class="sp_none"><img src="images/botox_cv_btn_off.png" width="309" height="66" alt="無料カウンセリングのご予約は こちらから" /></a><a href="#section09" class="pc_none"><img src="images/sp/botox_cv_btn.gif" alt="無料カウンセリングのご予約はこちらから" width="100%"></a></div>
	</div>
	
	<div id="content03">
		<div id="contents03_icon"><img src="images/botox_title_icon.gif" width="22" height="1" alt="" class="sp_none" /><img src="images/sp/botox_common_title_icon.gif" alt="" width="5%" height="1" class="imgbdr pc_none"></div>
		<ul class="contents03_box cf">
			<li class="left">
				<h3><img src="images/botox_contents03_text01.gif" width="399" height="43" alt="CASE1 おでこ＆目周りのシワを改善" class="sp_none" /><img src="images/sp/botox_contents03_text01.gif" alt="CASE1 おでこ＆目周りのシワを改善" width="100%" class="pc_none"></h3>
				<div><img src="images/botox_contents03_img01.jpg" width="399" height="132" alt="Brow Eyes" class="sp_none" /><img src="images/sp/botox_contents03_img01.jpg" alt="Brow Eyes" width="100%" class="pc_none"></div>
				<h4><img src="images/botox_contents03_text02.gif" width="144" height="20" alt="額・眉間のシワ取り" class="sp_none" /><span class="pc_none">額・眉間のシワ取り</span></h4>
				<p>笑ったり、顔に力を入れると現れる額の横ジワも、ツルっとした滑らかな肌に。</p>
				<h4><img src="images/botox_contents03_text03.gif" width="116" height="20" alt="目尻のシワ取り" class="sp_none" /><span class="pc_none">目尻のシワ取り</span></h4>
				<p>皮膚を寄せ集めるようにして現れる目尻の細かいシワを改善し、笑顔が素敵な目元に。</p>
				<h4><img src="images/botox_contents03_text04.gif" width="127" height="20" alt="ひとみをぱっちり" class="sp_none" /><span class="pc_none">ひとみをぱっちり</span></h4>
				<p>下にひっぱる力が強いために垂れ下がってしまった眉や上まぶたの引っ張る力を和らげて、すっきりとした目元をつくりあげます。</p>
			</li>
			<li class="center">
				<h3><img src="images/botox_contents03_text05.gif" width="399" height="43" alt="CASE2 鼻＆鼻周りのシワを改善" class="sp_none" /><img src="images/sp/botox_contents03_text02.gif" alt="CASE2 鼻＆鼻周りのシワを改善" width="100%" class="pc_none"></h3>
				<div><img src="images/botox_contents03_img02.jpg" width="399" height="132" alt="Nose" class="sp_none" /><img src="images/sp/botox_contents03_img02.jpg" alt="Nose" width="100%" class="pc_none"></div>
				<h4><img src="images/botox_contents03_text06.gif" width="118" height="20" alt="鼻の横ジワ取り" class="sp_none" /><span class="pc_none">鼻の横ジワ取り</span></h4>
				<p>目をぎゅっと強く閉じたときなどに現われる鼻の横ジワを解消。</p>
				<h4><img src="images/botox_contents03_text07.gif" width="100" height="20" alt="小鼻を小さく" class="sp_none" /><span class="pc_none">小鼻を小さく</span></h4>
				<p>広がりやすい鼻の穴の開く力を和らげて、横に広がった小鼻を小さくします。</p>
			</li>
			<li class="right">
				<h3><img src="images/botox_contents03_text08.gif" width="398" height="43" alt="CASE3 顎＆首のシワ改善" class="sp_none" /><img src="images/sp/botox_contents03_text03.gif" alt="CASE3 顎＆首のシワ改善" width="100%" class="pc_none"></h3>
				<div><img src="images/botox_contents03_img03.jpg" width="398" height="132" alt="Jaw" class="sp_none" /><img src="images/sp/botox_contents03_img03.jpg" alt="Jaw" width="100%" class="pc_none"></div>
				<h4><img src="images/botox_contents03_text09.gif" width="102" height="20" alt="顎のシワ取り" class="sp_none" /><span class="pc_none">顎のシワ取り</span></h4>
				<p>「梅干ジワ」とも呼ばれる顎のシワも、滑らかな肌へと導きます。</p>
			</li>
		</ul>
		<ul class="contents03_box cf">
			<li class="left_bottom">
				<h3><img src="images/botox_contents03_text11.gif" width="399" height="43" alt="CASE4 &quot;エラはり&quot;を解消して憧れの小顔に！" class="sp_none" /><img src="images/sp/botox_contents03_text04.gif" alt="CASE4 &quot;エラはり&quot;を解消して憧れの小顔に！" width="100%" class="pc_none"></h3>
				<div><img src="images/botox_contents03_img04.jpg" width="399" height="132" alt="Ella" class="sp_none" /><img src="images/sp/botox_contents03_img04.jpg" alt="Ella" width="100%" class="pc_none"></div>
				<p class="mt10">「ベース型」と言われる&quot;エラ&quot;がはった顔立ち。<br />&quot;エラはり&quot;のほとんどは咬筋（咀嚼筋のひとつ）のコリに原因があります。<br />咬筋を『ボトックス』で和らげることで"エラ"を解消して小顔効果が期待できます。</p>
			</li>
			<li class="center_bottom">
				<h3><img src="images/botox_contents03_text12.gif" width="399" height="43" alt="CASE5 ふくらはぎをほっそりさせて美脚に！" class="sp_none" /><img src="images/sp/botox_contents03_text05.gif" alt="CASE5 ふくらはぎをほっそりさせて美脚に！" width="100%" class="pc_none"></h3>
				<div><img src="images/botox_contents03_img05.jpg" width="399" height="132" alt="Calf" class="sp_none" /><img src="images/sp/botox_contents03_img05.jpg" alt="Calf" width="100%" class="pc_none"></div>
				<p class="mt10">ふくらはぎは脂肪より筋肉によって太く見えてしまっていることが多く、その場合、脂肪を減らすダイエットでやせることは難しいです。<br />
				ふくらはぎだけが太く悩んでいる方は、『ボトックス』で筋肉を少しづつ萎縮させることで、ほっそりとした美脚ラインをつくります。</p>
			</li>
			<li class="right_bottom">
				<h3><img src="images/botox_contents03_text13.gif" width="398" height="43" alt="CASE6 わきが・多汗症のお悩みも解消！" class="sp_none" /><img src="images/sp/botox_contents03_text06.gif" alt="CASE6 わきが・多汗症のお悩みも解消！" width="100%" class="pc_none"></h3>
				<div><img src="images/botox_contents03_img06.jpg" width="398" height="132" alt="Side" class="sp_none" /><img src="images/sp/botox_contents03_img06.jpg" alt="Side" width="100%" class="pc_none"></div>
				<p class="mt10">汗を分泌する2種類の汗腺（アポクリン腺・エクリン腺）を支配している神経を、『ボトックス』で麻痺させることで汗の分泌量を減らす治療です。広い範囲でしっかりと効果を出すため、できるだけ細かく点状に打つのがポイントです。<br />
				ワキに少量の注射をするだけなので、ダウンタイムもなく、当日からシャワーが可能と、手軽さと安全性の高さが人気です。</p>
			</li>
		</ul>
	</div>
	
	<div id="priceArea" class="priceArea">
		<h2 class="title04"><img src="images/botox_contents04_title.gif" width="228" height="87" alt="ボトックス注入料金※全て税抜き価格です。" class="sp_none" /><img src="images/sp/botox_common_title_icon.gif" alt="" width="5%" height="1" class="imgbdr pc_none"><img src="images/sp/botox_contents04_title.gif" alt="ボトックス注入料金 ※全て税抜き価格です。" width="100%" class="pc_none"></h2>
		<ul class="price_list cf">
			<li class="pr2">
				<div class="menu"><img src="images/botox_contents04_img01.jpg" width="399" height="133" alt="眉間 目尻・額" class="sp_none" /><img src="images/sp/botox_contents04_img01.jpg" alt="眉間 目尻・額" width="100%" class="pc_none"></div>
				<div><img src="images/botox_contents04_text01.gif" width="369" height="34" alt="初回キャンペーン価格 &yen;15,000（+税）" class="sp_none" /><img src="images/sp/botox_contents04_text01.gif" alt="初回キャンペーン価格  ¥15,000（+税）" width="100%" class="pc_none"></div>
			</li>
			<li class="pr2">
				<div class="menu"><img src="images/botox_contents04_img02.jpg" width="399" height="133" alt="エラ" class="sp_none" /><img src="images/sp/botox_contents04_img02.jpg" alt="エラ" width="100%" class="pc_none"></div>
				<div><img src="images/botox_contents04_text02.gif" width="369" height="34" alt="初回キャンペーン価格 &yen;39,800（+税）" class="sp_none" /><img src="images/sp/botox_contents04_text02.gif" width="100%" alt="初回キャンペーン価格  ¥39,800（+税）" class="pc_none"></div>
			</li>
			<li>
				<div class="menu"><img src="images/botox_contents04_img03.jpg" width="398" height="133" alt="両脇" class="sp_none" /><img src="images/sp/botox_contents04_img03.jpg" width="100%" alt="両脇" class="pc_none"></div>
				<div><img src="images/botox_contents04_text02.gif" width="368" height="34" alt="初回キャンペーン価格 &yen;39,800（+税）" class="sp_none" /><img src="images/sp/botox_contents04_text02.gif" width="100%" alt="初回キャンペーン価格  ¥39,800（+税）" class="pc_none"></div>
			</li>
			<li class="pr2">
				<div class="menu"><img src="images/botox_contents04_img04.jpg" width="399" height="133" alt="手のひら" class="sp_none" /><img src="images/sp/botox_contents04_img04.jpg" width="100%" alt="手のひら" class="pc_none"></div>
				<div><img src="images/botox_contents04_text02.gif" width="369" height="34" alt="初回キャンペーン価格 &yen;39,800（+税）" class="sp_none" /><img src="images/sp/botox_contents04_text02.gif" width="100%" alt="初回キャンペーン価格  ¥39,800（+税）" class="pc_none"></div>
			</li>
			<li class="pr2">
				<div class="menu"><img src="images/botox_contents04_img05.jpg" width="399" height="133" alt="足の裏" class="sp_none" /><img src="images/sp/botox_contents04_img05.jpg" width="100%" alt="足の裏" class="pc_none"></div>
				<div><img src="images/botox_contents04_text02.gif" width="369" height="34" alt="初回キャンペーン価格 &yen;39,800（+税）" class="sp_none" /><img src="images/sp/botox_contents04_text02.gif" width="100%" alt="初回キャンペーン価格  ¥39,800（+税）" class="pc_none"></div>
			</li>
			<li>
				<div class="menu"><img src="images/botox_contents04_img06.jpg" width="398" height="133" alt="下腿" class="sp_none" /><img src="images/sp/botox_contents04_img06.jpg" width="100%" alt="下腿 " class="pc_none"></div>
				<div><img src="images/botox_contents04_text03.gif" width="368" height="34" alt="初回キャンペーン価格 &yen;59,800（+税）" class="sp_none" /><img src="images/sp/botox_contents04_text03.gif" width="100%" alt="初回キャンペーン価格  ¥59,800（+税）" class="pc_none"></div>
			</li>
		</ul>
		<p>※2回目以降の施術を半額でお受けいただけるプランもございます。</p>
	</div>
	
	<div id="faqArea" class="faqArea">
		<h2 id="title05"><img src="images/botox_contents05_title.gif" width="463" height="58" alt="よくある質問" class="sp_none" /><img src="images/sp/botox_common_title_icon.gif" alt="" width="5%" height="1" class="imgbdr pc_none"><img src="images/sp/botox_contents05_title.gif" width="100%" alt="よくある質問" class="pc_none"></h2>
		<ul class="content05_detail cf">
			<li class="w399">
				<div><img src="images/botox_contents05_img01.jpg" width="399" height="200" alt="" class="sp_none" /><img src="images/sp/botox_contents05_img01.jpg" width="100%" alt="" class="pc_none"></div>
				<h3><img src="images/botox_contents05_text01.gif" width="399" height="54" alt="表情シワと自然シワの違いって？" class="sp_none" /><img src="images/sp/botox_contents05_text01.gif" width="100%" alt="表情シワと自然シワの違いって？" class="pc_none"></h3>
				<p>ボトックスがお勧めなのは、表情シワです。<br />
				自然シワとの大きな違いは眉間の縦ジワ、額の横ジワ、目の周りなど、表情のクセで、出来てしまうシワです。<br />
				当院では、お悩みのシワの状況をしっかりカウンセリングさせていただいてから、適切な施術をご提案しています。</p>
			</li>
			<li class="w397">
				<div><img src="images/botox_contents05_img02.jpg" width="397" height="200" alt="" class="sp_none" /><img src="images/sp/botox_contents05_img02.jpg" width="100%" alt="" class="pc_none"></div>
				<h3><img src="images/botox_contents05_text02.gif" width="397" height="54" alt="シワを予防することはできますか？" class="sp_none" /><img src="images/sp/botox_contents05_text02.gif" width="100%" alt="シワを予防することはできますか？" class="pc_none"></h3>
				<p>ボトックスを定期的に少量づつ注入しておくことで、すでにあるしわを隠すことは可能です。また、シワの原因によりますが表情シワの場合はなるべくクセのある表情を改善することをお話しています。また筋肉老化によるたるみシワの場合は、表情筋を鍛えるマッサージなどが有効です。</p>
			</li>
			<li class="w400">
				<div><img src="images/botox_contents05_img03.jpg" width="400" height="200" alt="" class="sp_none" /><img src="images/sp/botox_contents05_img03.jpg" width="100%" alt="" class="pc_none"></div>
				<h3><img src="images/botox_contents05_text03.gif" width="400" height="54" alt="ボトックス注射でワキの臭いや多汗症に効果があるって本当ですか？" class="sp_none" /><img src="images/sp/botox_contents05_text03.gif" width="100%" alt="ボトックス注射でワキの臭いや多汗症に効果があるって本当ですか？" class="pc_none"></h3>
				<p>ボトックス注射は筋肉の働きを抑制するのと同じ作用で、汗腺の働きも抑え、匂いの元となる汗を出させません。メスを使わない多汗症治療として、気になる汗の臭いを半年～1年程（個人差あり）抑えてくれます。症状によりますが、軽度のわきがなら、ボトックス注射だけでも十分だと思います。</p>
			</li>
			<li class="w399">
				<div><img src="images/botox_contents05_img04.jpg" width="399" height="200" alt="" class="sp_none" /><img src="images/sp/botox_contents05_img04.jpg" width="100%" alt="" class="pc_none"></div>
				<h3><img src="images/botox_contents05_text04.gif" width="399" height="54" alt="どうして、ボトックス注射で脚が細くなるんですか？" class="sp_none" /><img src="images/sp/botox_contents05_text04.gif" width="100%" alt="どうして、ボトックス注射で脚が細くなるんですか？" class="pc_none"></h3>
				<p>それは、ボトックス注射に筋肉の動きを和らげる効果があるからです。身体のメカニズムに働きかけ一時的に足の筋肉を使わなくさせ、自然と美脚にしていくのがボトックス注射の特徴です。</p>
			</li>
			<li class="w397">
				<div><img src="images/botox_contents05_img05.jpg" width="397" height="200" alt="" class="sp_none" /><img src="images/sp/botox_contents05_img05.jpg" width="100%" alt="" class="pc_none"></div>
				<h3><img src="images/botox_contents05_text05.gif" width="397" height="54" alt="ボトックス注射に副作用はないんですか？" class="sp_none" /><img src="images/sp/botox_contents05_text05.gif" width="100%" alt="ボトックス注射に副作用はないんですか？" class="pc_none"></h3>
				<p>今のところ副作用は報告されていません。<br />ただし、当院では念のため妊娠中や授乳中の方に関しては当院では注射しないようにしております。</p>
			</li>
		</ul>
	</div>

	<div class="conversion03 cf">
		<div class="label pc_none"><img src="images/sp/botox_cv_01.gif" alt="お電話でのご相談" width="100%"></div>
		<div class="tel">
			<img src="images/botox_cv_tel.gif" width="212" height="92" alt="お電話でのご相談 0120-334-270 OPEN / 11:00-20:00" class="sp_none" />
			<a href="tel:0120334270" onclick="javascript:goog_report_conversion('tel:0120-334-270');yahoo_report_conversion(undefined);ga('send','event','sp','tel-tap','tel');return false;" class="pc_none"><img src="images/sp/botox_cv_02.gif" alt="0120-334-270" width="100%"></a>
		</div>
		<div class="last pc_none"><img src="images/sp/botox_cv_03.gif" alt="OPEN / 11:00-20:00" width="100%"></div>
		<div class="btn"><a href="#section09" class="sp_none"><img src="images/botox_cv_btn_off.png" width="309" height="66" alt="無料カウンセリングのご予約は こちらから" /></a><a href="#section09" class="pc_none"><img src="images/sp/botox_cv_btn.gif" alt="無料カウンセリングのご予約はこちらから" width="100%"></a></div>
	</div>
</div><!-- wrapper !-->
	
	<div id="treatmentFlow">
		<h2 id="title06"><img src="images/botox_contents06_title.gif" width="141" height="59" alt="施術の流れ" class="sp_none" /><img src="images/sp/botox_common_title_icon.gif" alt="" width="5%" height="1" class="imgbdr pc_none"><img src="images/sp/botox_contents06_title.gif" width="100%" alt="施術の流れ" class="pc_none"></h2>
		<ul class="contents06_box cf">
			<li class="left_box cf">
				<div class="detail_left">
					<h3><img src="images/botox_contents06_text01.gif" width="340" height="42" alt="STEP1 お電話、メールにてお問い合わせ" class="sp_none" /><img src="images/sp/botox_contents06_text01.gif" width="100%" alt="STEP1 お電話、メールにてお問い合わせ" class="pc_none"></h3>
					<p><img src="images/sp/botox_contents06_img01.jpg" width="40%" alt="" class="image_right pc_none">お電話、もしくはメールでご予約を承っております。<br />
					施術のご予約の前に、診療や施術、その他について疑問がありましたら、お電話またはお問い合わせフォーム、メールからお気軽にお問い合わせください。<br />
					その際には匿名でのお問合せも可能です。</p>
					<div class="contact">
						<div class="inner">
							<div><img src="images/botox_contents06_tel.gif" width="314" height="39" alt="お電話でのご相談 06-6341-5037 OPEN / 火・水・金・土　12：00～19:00　日・祝　10：00～17:30" class="sp_none" /><div class="tel01 pc_none"><img src="images/sp/botox_contents06_tel01.gif" width="100%" alt="お電話でのご相談"></div><div class="tel02 pc_none"><a href="tel:0663415037" onclick="javascript:goog_report_conversion('tel:0663415037');yahoo_report_conversion(undefined);ga('send','event','sp','tel-tap','tel');return false;"><img src="images/sp/botox_contents06_tel02.gif" width="100%" alt="06-6341-5037 OPEN / 火・水・金・土　12：00～19:00　日・祝　10：00～17:30"></a></div></div>
							<div class="btn">
								<a href="#section09" class="sp_none"><img src="images/botox_contents06_btn_off.png" width="314" height="37" alt="無料カウンセリングのご予約はこちらから" /></a>
								<a href="#section09" class="pc_none"><img src="images/sp/botox_contents06_tel03.gif" width="100%" alt="無料カウンセリングのご予約はこちらから"></a>
							</div>
						</div>
					</div>
				</div>
				<div class="detail_right"><img src="images/botox_contents06_img01.jpg" width="199" height="301" alt="" class="sp_none" /></div>
			</li>
			<li class="cf">
				<div class="detail_left">
					<h3><img src="images/botox_contents06_text02.gif" width="340" height="42" alt="STEP2 ご来院／受付" class="sp_none" /><img src="images/sp/botox_contents06_text02.gif" width="100%" alt="STEP2 ご来院／受付" class="pc_none"></h3>
					<p><img src="images/sp/botox_contents06_img02.jpg" width="40%" alt="" class="image_left pc_none">お電話でご予約いただいた日程でご来院ください。<br />（日程の変更などはお気軽にご連絡ください）<br />ご予約された日時にお越し下さい。</p>
					<p>表参道スキンクリニックでのカウンセリング方法など事前に何でも相談ください。</p>
				</div>
				<div class="detail_right"><img src="images/botox_contents06_img02.jpg" width="199" height="301" alt="" class="sp_none" /></div>
			</li>
		</ul>
		<div class="contents06_bg">
			<ul class="contents06_box cf">
				<li class="left_box cf">
					<div class="detail_right02">
						<h3><img src="images/botox_contents06_text03.gif" width="340" height="42" alt="STEP3 スタッフとのカウンセリング" class="sp_none" /><img src="images/sp/botox_contents06_text03.gif" width="100%" alt="STEP3 スタッフとのカウンセリング" class="pc_none"></h3>
						<p><img src="images/sp/botox_contents06_img03.jpg" width="40%" alt="" class="image_right pc_none">お客様、患者様の悩みや疑問、施術費用やお支払方法ついてなど、専門のスタッフにお気軽に相談ください。</p>
						<p>施術へのお手続き・施術申込書の記入、術後の注意事項をお渡しします。<br />未成年の方は同意書が必要になります。<br />又はご両親のどちらかご同伴でお願いします。<br />※認印（シャチハタ以外）をお持ち下さい。</p>
					</div>
					<div class="detail_left02"><img src="images/botox_contents06_img03.jpg" width="199" height="280" alt="お電話でのご相談 06-6341-5037 OPEN / 火・水・金・土　12：00～19:00　日・祝　10：00～17:30" class="sp_none" /></div>
				</li>
				<li class="cf">
					<div class="detail_right02">
						<h3><img src="images/botox_contents06_text04.gif" width="340" height="43" alt="STEP4 担当医師とのカウンセリング／診察" class="sp_none" /><img src="images/sp/botox_contents06_text04.gif" width="100%" alt="STEP4 担当医師とのカウンセリング／診察" class="pc_none"></h3>
						<p><img src="images/sp/botox_contents06_img04.jpg" width="40%" alt="" class="image_left pc_none">お電話でご予約いただいた日程でご来院ください。<br />（日程の変更などはお気軽にご連絡ください）<br /></p>
						<p>表参道スキンクリニックでのカウンセリング方法など事前に何でも相談ください。</p>
					</div>
					<div class="detail_left02"><img src="images/botox_contents06_img04.jpg" width="199" height="280" alt="" class="sp_none" /></div>
				</li>
			</ul>
		</div>
		<ul class="contents06_box cf">
			<li class="left_box cf">
				<div class="detail_left">
					<h3><img src="images/botox_contents06_text05.gif" width="340" height="42" alt="STEP5 施術を行います" class="sp_none" /><img src="images/sp/botox_contents06_text05.gif" width="100%" alt="STEP5 施術を行います" class="pc_none"></h3>
					<p><img src="images/sp/botox_contents06_img05.jpg" width="40%" alt="" class="image_right pc_none">お顔の施術の場合は、事前にメイクを落としていただく必要がございます。<br />化粧落としなどご用意しておりますので、お気軽にスタッフにお声掛けください。</p>
					<p>準備ができましたら施術室に移動していただき、施術を行います。<br />リラックスして施術を受けてください。</p>
				</div>
				<div class="detail_right"><img src="images/botox_contents06_img05.jpg" width="199" height="325" alt="" class="sp_none" /></div>
			</li>
			<li class="cf">
				<div class="detail_left">
					<h3><img src="images/botox_contents06_text06.gif" width="340" height="42" alt="STEP6 アフターケア" class="sp_none" /><img src="images/sp/botox_contents06_text06.gif" width="100%" alt="STEP6 アフターケア" class="pc_none"></h3>
					<p><img src="images/sp/botox_contents06_img06.jpg" width="40%" alt="" class="image_left pc_none">施術終了後すぐにご帰宅いただけます。<br />
					専用のメイクルームをご用意いたしておりますので、メイク直しをしながら安心して新しいご自分と対面していただけます。施術後のアフターケアの料金は手術料金に含まれております。<br />
					また、施術後しばらく経ってご不安な点や不明瞭な点などがございましたら、お気軽にお電話、またはメールにて直接ご相談ください。</p>
				</div>
				<div class="detail_right"><img src="images/botox_contents06_img06.jpg" width="199" height="325" alt="" class="sp_none" /></div>
			</li>
		</ul>
	</div>

	<div class="conversion cf">
		<div class="label pc_none"><img src="images/sp/botox_cv_01.gif" alt="お電話でのご相談" width="100%"></div>
		<div class="tel">
			<img src="images/botox_cv_tel.gif" width="212" height="92" alt="お電話でのご相談 0120-334-270 OPEN / 11:00-20:00" class="sp_none" />
			<a href="tel:0120334270" onclick="javascript:goog_report_conversion('tel:0120-334-270');yahoo_report_conversion(undefined);ga('send','event','sp','tel-tap','tel');return false;" class="pc_none"><img src="images/sp/botox_cv_02.gif" alt="0120-334-270" width="100%"></a>
		</div>
		<div class="last pc_none"><img src="images/sp/botox_cv_03.gif" alt="OPEN / 11:00-20:00" width="100%"></div>
		<div class="btn"><a href="#section09" class="sp_none"><img src="images/botox_cv_btn_off.png" width="309" height="66" alt="無料カウンセリングのご予約は こちらから" /></a><a href="#section09" class="pc_none"><img src="images/sp/botox_cv_btn.gif" alt="無料カウンセリングのご予約はこちらから" width="100%"></a></div>
	</div>
</div><!-- wrapper !-->

<div class="wrapper">

	<div id="slideArea">
		<h2 id="title07"><img src="images/botox_contents07_title.gif" width="548" height="59" alt="当院を選ばれた女性に大変好評な院内空間" class="sp_none" /><img src="images/sp/botox_common_title_icon.gif" alt="" width="5%" height="1" class="imgbdr pc_none"><img src="images/sp/botox_contents07_title.gif" width="100%" alt="当院を選ばれた女性に大変好評な院内空間" class="pc_none"></h2>
		<div class="contents07_bg">
			<div class="bg_img">
				<ul id="slider" class="slider">
					<li class="slide1"><a href="images/botox_contents04_img01_big.jpg" rel="lightbox" class="sp_none"><img src="images/botox_contents07_img01.jpg" width="400" height="361" alt="受付" /></a><img src="images/sp/botox_contents07_main01.jpg" alt="受付" width="100%" class="pc_none"></li>
					<li class="slide2"><a href="images/botox_contents04_img02_big.jpg" rel="lightbox" class="sp_none"><img src="images/botox_contents07_img02.jpg" width="400" height="361" alt="待合室" /></a><img src="images/sp/botox_contents07_main02.jpg" alt="待合室" width="100%" class="pc_none"></li>
					<li class="slide3"><a href="images/botox_contents04_img03_big.jpg" rel="lightbox" class="sp_none"><img src="images/botox_contents07_img03.jpg" width="400" height="361" alt="カウンセリングルーム" /></a><img src="images/sp/botox_contents07_main03.jpg" alt="カウンセリングルーム" width="100%" class="pc_none"></li>
					<li class="slide4"><a href="images/botox_contents04_img04_big.jpg" rel="lightbox" class="sp_none"><img src="images/botox_contents07_img04.jpg" width="400" height="361" alt="診察室" /></a><img src="images/sp/botox_contents07_main04.jpg" alt="診察室" width="100%" class="pc_none"></li>
					<li class="slide5"><a href="images/botox_contents04_img05_big.jpg" rel="lightbox" class="sp_none"><img src="images/botox_contents07_img05.jpg" width="400" height="361" alt="診察室"  /></a><img src="images/sp/botox_contents07_main05.jpg" alt="診察室" width="100%" class="pc_none"></li>
					<li class="slide6"><a href="images/botox_contents04_img06_big.jpg" rel="lightbox" class="sp_none"><img src="images/botox_contents07_img06.jpg" width="400" height="361" alt="施術室" /></a><img src="images/sp/botox_contents07_main06.jpg" alt="施術室" width="100%" class="pc_none"></li>
				</ul>
			</div>
		</div>
		<div class="contents07_text"><img src="images/botox_contents07_text01.gif" width="557" height="42" alt="表参道スキンクリニックでは皆様のプライベートを大切に考えています。院内は、いつも安心して来院していただけます。" class="sp_none" /><img src="images/sp/botox_contents07_text01.gif" width="100%" alt="表参道スキンクリニックでは皆様のプライベートを 大切に考えています。 院内は、いつも安心して来院していただけます。" class="pc_none"></div>
	</div>

	<div class="conversion cf">
		<div class="label pc_none"><img src="images/sp/botox_cv_01.gif" alt="お電話でのご相談" width="100%"></div>
		<div class="tel">
			<img src="images/botox_cv_tel.gif" width="212" height="92" alt="お電話でのご相談 0120-334-270 OPEN / 11:00-20:00" class="sp_none" />
			<a href="tel:0120334270" onclick="javascript:goog_report_conversion('tel:0120-334-270');yahoo_report_conversion(undefined);ga('send','event','sp','tel-tap','tel');return false;" class="pc_none"><img src="images/sp/botox_cv_02.gif" alt="0120-334-270" width="100%"></a>
		</div>
		<div class="last pc_none"><img src="images/sp/botox_cv_03.gif" alt="OPEN / 11:00-20:00" width="100%"></div>
		<div class="btn"><a href="#section09" class="sp_none"><img src="images/botox_cv_btn_off.png" width="309" height="66" alt="無料カウンセリングのご予約は こちらから" /></a><a href="#section09" class="pc_none"><img src="images/sp/botox_cv_btn.gif" alt="無料カウンセリングのご予約はこちらから" width="100%"></a></div>
	</div>
</div><!-- wrapper !-->
	
	<div id="faqArea">
		<h2 id="title08"><img src="images/botox_contents08_title.gif" width="66" height="59" alt="FAQ" class="sp_none" /><img src="images/sp/botox_common_title_icon.gif" alt="" width="5%" height="1" class="imgbdr pc_none"><img src="images/sp/botox_contents08_title.gif" width="100%" alt="FAQ" class="pc_none"></h2>
		<div class="contents08_bg">
			<div class="wrapper">
				<ul class="cf">
					<li class="pr60 box01">
						<h3><img src="images/botox_contents08_text01.gif" width="570" height="29" alt="Q1.ボトックスでどんな治療ができますか？" class="sp_none" /><img src="images/sp/botox_contents08_text01.gif" width="100%" alt="Q1.ボトックスでどんな治療ができますか？" class="pc_none"></h3>
						<p>ボトックスは表情しわに効果的です。<br />
						表情しわには、眉間の縦しわ、額の横しわ、目尻のしわ、アゴの梅干状のしわ、口元のしわなどがあります。<br />
						また、額をリフトする効果もあります。<br />ボトックスはしわ以外に小顔治療、脚を細くする、多汗症治療にも効果があります。</p>
					</li>
					<li class="box01">
						<h3><img src="images/botox_contents08_text02.gif" width="570" height="29" alt="Q2.ヒアルロン酸とボトックスとは何が違うのですか？" class="sp_none" /><img src="images/sp/botox_contents08_text02.gif" width="100%" alt="Q2.ヒアルロン酸とボトックスとは何が違うのですか？" class="pc_none"></h3>
						<p>効果のあるしわの種類が異なります。<br />ヒアルロン酸は細胞内でのヒアルロン酸やコラーゲン、エラスチンの生成力が落ちる事によって生じた、いわゆる溝的なほうれい線などのしわに注入する事で、溝が埋まりしわを目立たなくさせる治療です。<br />
						ボトックスは、目尻や額などの筋肉が作用する事によって起こるしわに注入する事で、筋肉の働きを抑えしわを出にくくします。つまりしわを寄らなくするわけです。</p>
					</li>
					<li class="pr60 box02">
						<h3><img src="images/botox_contents08_text03.gif" width="570" height="29" alt="Q3.治療にどれくらい時間がかかりますか？" class="sp_none" /><img src="images/sp/botox_contents08_text03.gif" width="100%" alt="Q3.治療にどれくらい時間がかかりますか？" class="pc_none"></h3>
						<p>治療は5分～10分程度で終わります。<br />麻酔に別途、30分程度お時間を頂きます。</p>
					</li>
					<li class="box02">
						<h3><img src="images/botox_contents08_text04.gif" width="570" height="29" alt="Q4.効果はどれ位持ちますか？" class="sp_none" /><img src="images/sp/botox_contents08_text04.gif" width="100%" alt="Q4.効果はどれ位持ちますか？" class="pc_none"></h3>
						<p>しわ治療はボトックス注射後48時間後以降で効果が現れ、その効果は3～6ヶ月ほど続きます。<br />
						一般的に、2回目以降の治療効果は初回より長く持続すると言われています。<br />
						これは、しわを作る原因筋が、ボトックスが効いている一定期間、あまり動かされないので、少しずつ萎縮して、しわが作られにくくなるためと考えられます。<br />
						小顔治療や足を細くする治療は1回目の効果が現れるまでに1～2ヶ月かかり、効果は3～6ヶ月程度続きます。2回目以降は効果がより早く現れ（1～2週間）、1回目に比べて効果もしっかり出ます。<br />小顔治療は4～6回程度繰り返すと半永久的な効果が得られることが多々あります。</p>
					</li>
					<li class="pr60 box03">
						<h3><img src="images/botox_contents08_text05.gif" width="570" height="29" alt="Q5.施術時の痛みが心配です。" class="sp_none" /><img src="images/sp/botox_contents08_text05.gif" width="100%" alt="Q5.施術時の痛みが心配です。" class="pc_none"></h3>
						<p>注射による施術のため、ある程度の痛みはあります。<br />
						当美容皮膚科ではなるべく痛みを抑えるために使用し、極細の針で注射します。<br />
						痛みに弱い方にも安心して治療をお受け頂けます。</p>
					</li>
					<li class="box03">
						<h3><img src="images/botox_contents08_text06.gif" width="570" height="29" alt="Q6.ボトックスでボツリヌス中毒になることはありますか？"  class="sp_none"/><img src="images/sp/botox_contents08_text06.gif" width="100%" alt="Q6.ボトックスでボツリヌス中毒になることはありますか？" class="pc_none"></h3>
						<p>ボツリヌス菌は食中毒を起こす菌として知られていますが、ボトックスはボツリヌス菌そのものを注射するわけではありません。<br />
						ボツリヌス菌の毒素であるボツリヌス毒素をごく少量だけ注射します。<br />
						食中毒を起こすボツリヌス毒素の量は3万単位程度なのに比べ、美容に使用される量は、2.5～100単位と少量であり、適切な治療を行っている限り、中毒をおこす心配はまずありません。</p>
					</li>
					<li class="pr60 box04">
						<h3><img src="images/botox_contents08_text07.gif" width="570" height="29" alt="Q7.ボトックスはどんな副反応がありますか？" class="sp_none" /><img src="images/sp/botox_contents08_text07.gif" width="100%" alt="Q7.ボトックスはどんな副反応がありますか？" class="pc_none"></h3>
						<p>針の跡が点状に残ったり、内出血が起こることがありますが、これらの反応は一時的で、お化粧で隠せる程度です。ボトックスのデザインによっては仮面のような不自然な表情になったり、目が下がってしまうという症状も見られます。<br />
						当クリニックでは熟練した医師の治療いたしますので、ご安心ください。</p>
					</li>
					<li class="box04">
						<h3><img src="images/botox_contents08_text08.gif" width="570" height="29" alt="Q8.施術後当日にメイクはできますか？" class="sp_none" /><img src="images/sp/botox_contents08_text08.gif" width="100%" alt="Q8.施術後当日にメイクはできますか？" class="pc_none"></h3>
						<p>当日のメイクは可能です。</p>
					</li>
					<li class="pr60">
						<h3><img src="images/botox_contents08_text09.gif" width="570" height="29" alt="Q9.妊娠中でも施術できますか？" class="sp_none" /><img src="images/sp/botox_contents08_text09.gif" width="100%" alt="Q9.妊娠中でも施術できますか？" class="pc_none"></h3>
						<p>当院ではすでに妊娠が分かっている方には治療を行っていません。<br />また注入後も念のため、最低3ヶ月間は妊娠や授乳を避けていただくようお願いしています。</p>
					</li>
					<li>
						<h3><img src="images/botox_contents08_text10.gif" width="570" height="29" alt="Q10.ボトックスの薬剤はどのようなものを使いますか？" class="sp_none" /><img src="images/sp/botox_contents08_text10.gif" width="100%" alt="Q10.ボトックスの薬剤はどのようなものを使いますか？" class="pc_none"></h3>
						<p>米国FDAの認可を受けている、アラガン社製のボトックスを使用しています。<br />アラガン社のボトックスは、全世界で信頼されている製品です。</p>
					</li>
				</ul>
		
			</div><!-- wrapper !-->
		</div>
	</div>

	<div class="conversion cf">
		<div class="label pc_none"><img src="images/sp/botox_cv_01.gif" alt="お電話でのご相談" width="100%"></div>
		<div class="tel">
			<img src="images/botox_cv_tel.gif" width="212" height="92" alt="お電話でのご相談 0120-334-270 OPEN / 11:00-20:00" class="sp_none" />
			<a href="tel:0120334270" onclick="javascript:goog_report_conversion('tel:0120-334-270');yahoo_report_conversion(undefined);ga('send','event','sp','tel-tap','tel');return false;" class="pc_none"><img src="images/sp/botox_cv_02.gif" alt="0120-334-270" width="100%"></a>
		</div>
		<div class="last pc_none"><img src="images/sp/botox_cv_03.gif" alt="OPEN / 11:00-20:00" width="100%"></div>
		<div class="btn"><a href="#section09" class="sp_none"><img src="images/botox_cv_btn_off.png" width="309" height="66" alt="無料カウンセリングのご予約は こちらから" /></a><a href="#section09" class="pc_none"><img src="images/sp/botox_cv_btn.gif" alt="無料カウンセリングのご予約はこちらから" width="100%"></a></div>
	</div>

<div class="wrapper">
	
	<div id="priceArea02" class="priceArea">
		<h2 class="title04"><img src="images/botox_contents04_title.gif" width="228" height="87" alt="ボトックス注入料金※全て税抜き価格です。" class="sp_none" /><img src="images/sp/botox_common_title_icon.gif" alt="" width="5%" height="1" class="imgbdr pc_none"><img src="images/sp/botox_contents04_title.gif" alt="ボトックス注入料金 ※全て税抜き価格です。" width="100%" class="pc_none"></h2>
		<ul class="price_list cf">
			<li class="pr2">
				<div class="menu"><img src="images/botox_contents04_img01.jpg" width="399" height="133" alt="眉間 目尻・額" class="sp_none" /><img src="images/sp/botox_contents04_img01.jpg" alt="眉間 目尻・額" width="100%" class="pc_none"></div>
				<div><img src="images/botox_contents04_text01.gif" width="369" height="34" alt="初回キャンペーン価格 &yen;15,000（+税）" class="sp_none" /><img src="images/sp/botox_contents04_text01.gif" alt="初回キャンペーン価格  ¥15,000（+税）" width="100%" class="pc_none"></div>
			</li>
			<li class="pr2">
				<div class="menu"><img src="images/botox_contents04_img02.jpg" width="399" height="133" alt="エラ" class="sp_none" /><img src="images/sp/botox_contents04_img02.jpg" alt="エラ" width="100%" class="pc_none"></div>
				<div><img src="images/botox_contents04_text02.gif" width="369" height="34" alt="初回キャンペーン価格 &yen;39,800（+税）" class="sp_none" /><img src="images/sp/botox_contents04_text02.gif" width="100%" alt="初回キャンペーン価格  ¥39,800（+税）" class="pc_none"></div>
			</li>
			<li>
				<div class="menu"><img src="images/botox_contents04_img03.jpg" width="398" height="133" alt="両脇" class="sp_none" /><img src="images/sp/botox_contents04_img03.jpg" width="100%" alt="両脇" class="pc_none"></div>
				<div><img src="images/botox_contents04_text02.gif" width="368" height="34" alt="初回キャンペーン価格 &yen;39,800（+税）" class="sp_none" /><img src="images/sp/botox_contents04_text02.gif" width="100%" alt="初回キャンペーン価格  ¥39,800（+税）" class="pc_none"></div>
			</li>
			<li class="pr2">
				<div class="menu"><img src="images/botox_contents04_img04.jpg" width="399" height="133" alt="手のひら" class="sp_none" /><img src="images/sp/botox_contents04_img04.jpg" width="100%" alt="手のひら" class="pc_none"></div>
				<div><img src="images/botox_contents04_text02.gif" width="369" height="34" alt="初回キャンペーン価格 &yen;39,800（+税）" class="sp_none" /><img src="images/sp/botox_contents04_text02.gif" width="100%" alt="初回キャンペーン価格  ¥39,800（+税）" class="pc_none"></div>
			</li>
			<li class="pr2">
				<div class="menu"><img src="images/botox_contents04_img05.jpg" width="399" height="133" alt="足の裏" class="sp_none" /><img src="images/sp/botox_contents04_img05.jpg" width="100%" alt="足の裏" class="pc_none"></div>
				<div><img src="images/botox_contents04_text02.gif" width="369" height="34" alt="初回キャンペーン価格 &yen;39,800（+税）" class="sp_none" /><img src="images/sp/botox_contents04_text02.gif" width="100%" alt="初回キャンペーン価格  ¥39,800（+税）" class="pc_none"></div>
			</li>
			<li>
				<div class="menu"><img src="images/botox_contents04_img06.jpg" width="398" height="133" alt="下腿" class="sp_none" /><img src="images/sp/botox_contents04_img06.jpg" width="100%" alt="下腿 " class="pc_none"></div>
				<div><img src="images/botox_contents04_text03.gif" width="368" height="34" alt="初回キャンペーン価格 &yen;59,800（+税）" class="sp_none" /><img src="images/sp/botox_contents04_text03.gif" width="100%" alt="初回キャンペーン価格  ¥59,800（+税）" class="pc_none"></div>
			</li>
		</ul>
		<p>※2回目以降の施術を半額でお受けいただけるプランもございます。</p>
	</div>

	<div id="accessArea">
		<h2 id="title10"><a name="access" id="access"></a><img src="images/botox_contents09_title.gif" width="225" height="57" alt="クリニックについて" class="sp_none" /><img src="images/sp/botox_common_title_icon.gif" alt="" width="5%" height="1" class="imgbdr pc_none"><img src="images/sp/botox_contents09_title.gif" width="100%" alt="クリニックについて" class="pc_none"></h2>
		<div class="cf">
			<div class="pc_none">
				<h3><img src="images/sp/botox_contents09_text01.gif" width="100%" alt="表参道スキンクリニック"></h3>
				<div class="icon">
					<img src="images/sp/botox_common_title_icon.gif" alt="" width="5%" height="1">
				</div>
				<p class="place">〒150-0001<br>東京都渋谷区神宮前5-9-13喜多重ビル4F<br>(1Fオリエンタルバザー)</p>
			</div>
			<div class="contenst10_map">
				<iframe width="400" height="389" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=ja&amp;geocode=&amp;q=%E6%9D%B1%E4%BA%AC%E9%83%BD%E6%B8%8B%E8%B0%B7%E5%8C%BA%E7%A5%9E%E5%AE%AE%E5%89%8D5-9-13%E5%96%9C%E5%A4%9A%E9%87%8D%E3%83%93%E3%83%AB4F+%E8%A1%A8%E5%8F%82%E9%81%93%E3%82%B9%E3%82%AD%E3%83%B3%E3%82%AF%E3%83%AA%E3%83%8B%E3%83%83%E3%82%AF&amp;aq=&amp;sll=35.667178,139.707193&amp;sspn=0.006546,0.011748&amp;g=%E6%9D%B1%E4%BA%AC%E9%83%BD%E6%B8%8B%E8%B0%B7%E5%8C%BA%E7%A5%9E%E5%AE%AE%E5%89%8D5-9-13%E5%96%9C%E5%A4%9A%E9%87%8D%E3%83%93%E3%83%AB4F&amp;ie=UTF8&amp;hq=%E8%A1%A8%E5%8F%82%E9%81%93%E3%82%B9%E3%82%AD%E3%83%B3%E3%82%AF%E3%83%AA%E3%83%8B%E3%83%83%E3%82%AF&amp;hnear=%E6%97%A5%E6%9C%AC,+%E6%9D%B1%E4%BA%AC%E9%83%BD%E6%B8%8B%E8%B0%B7%E5%8C%BA%E7%A5%9E%E5%AE%AE%E5%89%8D%EF%BC%95%E4%B8%81%E7%9B%AE%EF%BC%99%E2%88%92%EF%BC%91%EF%BC%93+%E5%96%9C%E5%A4%9A%E9%87%8D%E3%83%93%E3%83%AB%E5%88%A5%E9%A4%A8&amp;ll=35.667228,139.707413&amp;spn=0.006546,0.011748&amp;t=m&amp;z=14&amp;iwloc=A&amp;cid=10526158253224628363&amp;output=embed"></iframe>
			</div>
			<div class="contenst10_detail">
				<h3 class="sp_none"><img src="images/botox_contents09_text01.gif" width="564" height="61" alt="表参道スキンクリニック 〒150-0001 東京都渋谷区神宮前5-9-13喜多重ビル4F(1Fオリエンタルバザー)" /></h3>
				<p>JR山手線「原宿駅」表参道口より徒歩約5分<br />東京メトロ各線「表参道駅」A1出口より徒歩約3分<br />東京メトロ各線「明治神宮前駅」4番出口より徒歩約3分</p>
				<table cellpadding="0" cellspacing="0" border="0">
					<tr>
						<th>診療時間</th>
						<td>AM11:00～PM8:00（完全予約制）</td>
					</tr>
					<tr>
						<th>休診日</th>
						<td>水曜日</td>
					</tr>
				</table>
			</div>
		</div>
	</div>

	<div class="conversion cf">
		<div class="label pc_none"><img src="images/sp/botox_cv_01.gif" alt="お電話でのご相談" width="100%"></div>
		<div class="tel">
			<img src="images/botox_cv_tel.gif" width="212" height="92" alt="お電話でのご相談 0120-334-270 OPEN / 11:00-20:00" class="sp_none" />
			<a href="tel:0120334270" onclick="javascript:goog_report_conversion('tel:0120-334-270');yahoo_report_conversion(undefined);ga('send','event','sp','tel-tap','tel');return false;" class="pc_none"><img src="images/sp/botox_cv_02.gif" alt="0120-334-270" width="100%"></a>
		</div>
		<div class="last pc_none"><img src="images/sp/botox_cv_03.gif" alt="OPEN / 11:00-20:00" width="100%"></div>
		<div class="btn"><a href="#section09" class="sp_none"><img src="images/botox_cv_btn_off.png" width="309" height="66" alt="無料カウンセリングのご予約は こちらから" /></a><a href="#section09" class="pc_none"><img src="images/sp/botox_cv_btn.gif" alt="無料カウンセリングのご予約はこちらから" width="100%"></a></div>
	</div>
</div><!-- wrapper !-->

	<div id="philosophyArea">
		<div class="wrapper">
			<h2 id="title11"><img src="images/botox_contents10_title.gif" width="199" height="58" alt="クリニックの理念" class="sp_none" /><img src="images/sp/botox_common_title_icon.gif" alt="" width="5%" height="1" class="imgbdr pc_none"><img src="images/sp/botox_contents10_title.gif" width="100%" alt="クリニックの理念" class="pc_none"></h2>
			<p>開業当初から当クリニックでは痛みや不安のない施術を心がけており、患者様に安心して美しくなって頂くことを目指しております。</p>
			<h3 id="contents10_subtitle"><img src="images/botox_contents10_text01.gif" width="1200" height="30" alt="表参道スキンクリニック" class="sp_none" /><img src="images/sp/botox_contents09_text01.gif" width="100%" alt="表参道スキンクリニック" class="pc_none"></h3>
		</div><!-- wrapper !-->
		<div class="contents10_bg">
			<div class="wrapper">
				<ul class="cf">
					<li class="pr60">
						<h4><img src="images/botox_contents10_text02.gif" width="342" height="51" alt="concept1 患者様とのコミュニケーションを大切にしています" class="sp_none" /><img src="images/sp/botox_contents10_concept1.gif" width="100%" alt="CONCEPT1" class="pc_none"><img src="images/sp/botox_contents10_text01.gif" width="100%" alt="患者様とのコミュニケーションを大切にしています" class="pc_none"></h4>
						<div><img src="images/botox_contents10_img01.jpg" width="360" height="150" alt="" class="sp_none" /><img src="images/sp/botox_contents10_img01.jpg" width="100%" alt="" class="pc_none"></div>
						<p>当クリニックは患者様の立場に立ち、今抱えている不安や悩みなどを解消するため、丁寧なカウンセリングを行なっています。</p>
					</li>
					<li class="pr60">
						<h4><img src="images/botox_contents10_text03.gif" width="327" height="51" alt="concept2 長年の経験と実績による確かな技術と高い信頼" class="sp_none" /><img src="images/sp/botox_contents10_concept2.gif" width="100%" alt="CONCEPT2" class="pc_none"><img src="images/sp/botox_contents10_text02.gif" width="100%" alt="長年の経験と実績による確かな技術と高い信頼" class="pc_none"></h4>
						<div><img src="images/botox_contents10_img02.jpg" width="360" height="150" alt="" class="sp_none" /><img src="images/sp/botox_contents10_img02.jpg" width="100%" alt="" class="pc_none"></div>
						<p>当クリニックは優秀な美容皮膚科医が多数在籍しています。本当に安心して任せられるドクターがレベルの高い治療を行ないます。</p>
					</li>
					<li>
						<h4><img src="images/botox_contents10_text04.gif" width="311" height="51" alt="concept3 いつもドクターがそばにいてくれるという感覚" class="sp_none" /><img src="images/sp/botox_contents10_concept3.gif" width="100%" alt="CONCEPT3" class="pc_none"><img src="images/sp/botox_contents10_text03.gif" width="100%" alt="いつもドクターがそばにいてくれるという感覚" class="pc_none"></h4>
						<div><img src="images/botox_contents10_img03.jpg" width="360" height="150" alt="" class="sp_none" /><img src="images/sp/botox_contents10_img03.jpg" width="100%" alt="" class="pc_none"></div>
						<p>当クリニックは気軽に相談できる環境作りを心がけています。術後のケア、美肌づくりのアドバイスなどいつでもすぐにお応えいたします。</p>
					</li>
				</ul>
			</div><!-- wrapper !-->
		</div>
	</div>
	
	<div id="doctor" class="wrapper">
		<h2 id="title12"><img src="images/botox_contents11_title.gif" width="172" height="58" alt="医師のご紹介" class="sp_none" /><img src="images/sp/botox_common_title_icon.gif" alt="" width="5%" height="1" class="imgbdr pc_none"><img src="images/sp/botox_contents11_title.gif" width="100%" alt="医師のご紹介" class="pc_none"></h2>
		<p>当クリニックの医師たちは、お客様に安全に美しくなって頂く為に、日々最高水準の技術を日々研究しております。そんな医師たちを御紹介させていただきます。</p>
<ul class="contents12_detail cf">
				<li class="w156">
					<div class="sp_none"><img src="images/botox_contents11_img01.jpg" width="156" height="191" alt=""></div>
					<div class="doctor"><img src="images/botox_contents11_text01.gif" width="156" height="32" alt="院長 松木貴裕" class="sp_none"><img src="images/sp/botox_contents11_text01.gif" width="100%" alt="院長 松木貴裕" class="pc_none"><img src="images/botox_contents11_img01.jpg" width="100%" alt="松木貴裕" class="pc_none"></div>
				</li>
				<li class="w399">
					<h3 class="title01">経歴</h3>
					<p>平成10年3月&nbsp;&nbsp;藤田保健衛生大学 医学部医学研究所科 卒業<br />
					平成11年9月&nbsp;&nbsp;医療法人大医会 日進おりど病院 勤務<br />
					平成14年3月&nbsp;&nbsp;医療法人大医会 日進おりど病院 退職<br />
					平成14年4月&nbsp;&nbsp;東京女子医科大学病院 皮膚科 勤務<br />
					平成16年3月&nbsp;&nbsp;東京女子医科大学病院 皮膚科 退職<br />
					平成16年9月&nbsp;&nbsp;東京美容外科 開設 勤務<br />
					平成26年10月&nbsp;表参道スキンクリニック 開設</p>
					<h3 class="title02">所属学会など</h3>
					<p>・日本皮膚科学会正会員　 ・レーザー医学会認定医<br />・日本抗加齢学会会員　 ・美容外科学会会員</p>
				</li>
				<li class="w581">
					<h3 class="title03">松木先生からひと言</h3>
					<p>かつては、大がかりな美容整形手術など、ハイリスクなイメージがあった美容医療の世界。<br />
					しかし近年では、手軽にトライできる治療も増え、女性にとって、とても身近な存在になりました。当クリニックでは、高い技術を持つ経験豊富な美容皮膚科医が、患者様の希望や理想を大切にしながら、最新の美容医療を提供いたします。<br />
					「愛されツルスベ美肌を手に入れたい…」多くの女性が持つ、そんな願いにお応えします。</p>
				</li>
			</ul>
	</div><!-- wrapper !-->

	<div class="conversion cf">
		<div class="label pc_none"><img src="images/sp/botox_cv_01.gif" alt="お電話でのご相談" width="100%"></div>
		<div class="tel">
			<img src="images/botox_cv_tel.gif" width="212" height="92" alt="お電話でのご相談 0120-334-270 OPEN / 11:00-20:00" class="sp_none" />
			<a href="tel:0120334270" onclick="javascript:goog_report_conversion('tel:0120-334-270');yahoo_report_conversion(undefined);ga('send','event','sp','tel-tap','tel');return false;" class="pc_none"><img src="images/sp/botox_cv_02.gif" alt="0120-334-270" width="100%"></a>
		</div>
		<div class="last pc_none"><img src="images/sp/botox_cv_03.gif" alt="OPEN / 11:00-20:00" width="100%"></div>
		<div class="btn"><a href="#section09" class="sp_none"><img src="images/botox_cv_btn_off.png" width="309" height="66" alt="無料カウンセリングのご予約は こちらから" /></a><a href="#section09" class="pc_none"><img src="images/sp/botox_cv_btn.gif" alt="無料カウンセリングのご予約はこちらから" width="100%"></a></div>
	</div>


<!--フォーム導入-->
  <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/smoothness/jquery-ui.css" >
  <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js"></script>
  <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1/i18n/jquery.ui.datepicker-ja.min.js"></script>
  <script>
  $(function() {
    $("#datepicker_1").datepicker({
      dateFormat: 'yy年mm月dd日 (DD)',
      dayNames: ['日', '月', '火', '水', '木', '金', '土']
    });
    $("#datepicker_2").datepicker({
      dateFormat: 'yy年mm月dd日 (DD)',
      dayNames: ['日', '月', '火', '水', '木', '金', '土']
    });
  });
  </script>
  <div id="section09" name="section09" class="box">

    <div class="block13 cf">
      <h2 class="mt20"><img src="images/form/title10.gif" width="1200" height="60" alt="無料カウンセリングご予約フォーム" class="sp_none"><img src="images/form/title10_sp.gif" width="100%" alt="無料カウンセリングご予約フォーム" class="pc_none"></h2>
      <p class="flow"><img src="images/form/flow_01.gif" alt="1．情報の入力" width="1000" height="80" /></p>
      <div class="form">
        <p class="caution">※お手数ですが、必須項目をすべてご入力の上、確認画面へお進みください。</p>

        <form method="post">

          <table>
            <tr>
              <th class="hissu">お名前</th>
              <td>
                <input type="text" name="name_req" value="<?php echo $formTool->h($_POST['name_req']); ?>" />　例）表参道花子
                <?php if( !empty($formTool->messages['name_req']) ) : ?>
                  <p class="error"><?php echo $formTool->h($formTool->messages['name_req']); ?></p>
                <?php endif; ?>
              </td>
            </tr>
            <tr>
              <th class="hissu">電話番号</th>
              <td>
                <input type="tel" name="tel_req" value="<?php echo $formTool->h($_POST['tel_req']); ?>" />　例）01-2345-6789
                <?php if( !empty($formTool->messages['tel_req']) ) : ?>
                  <p class="error"><?php echo $formTool->h($formTool->messages['tel_req']); ?></p>
                <?php endif; ?>
              </td>
            </tr>
            <tr>
              <th class="hissu">メールアドレス</th>
              <td>
                <input type="email" name="email_req" value="<?php echo $formTool->h($_POST['email_req']); ?>" />　例）info@abc.com
                <?php if( !empty($formTool->messages['email_req']) ) : ?>
                  <p class="error"><?php echo $formTool->h($formTool->messages['email_req']); ?></p>
                <?php endif; ?>
              </td>
            </tr>
            <tr>
              <th class="nini">相談日 第一希望</th>
              <td>
                <input type="text" name="day1" value="<?php echo $formTool->h($_POST['day1']); ?>" placeholder="YYYY年MM月DD日" class="calendar" id="datepicker_1" readonly="readonly"/>
                <select name="day1_period">
                  <option value="">時間帯を選択</option>
                  <?php echo $formTool->makeOptionHtml($formTool->timeArr, 'array', $_POST['day1_period']); ?>
                </select>
              </td>
            </tr>
            <tr>
              <th class="nini">相談日 第二希望</th>
              <td>
                <input type="text" name="day2" value="<?php echo $formTool->h($_POST['day2']); ?>" placeholder="YYYY年MM月DD日" class="calendar" id="datepicker_2" readonly="readonly"/>
                <select name="day2_period">
                  <option value="">時間帯を選択</option>
                  <?php echo $formTool->makeOptionHtml($formTool->timeArr, 'array', $_POST['day2_period']); ?>
                </select>
              </td>
            </tr>
            <tr>
              <th class="nini">連絡方法のご希望</th>
              <td class="radiostyle">
                <?php echo $formTool->makeCheckBoxHtml($formTool->typeArr, 'array', $_POST['type'], 'type'); ?>
              </td>
            </tr>
            <tr>
              <th class="nini last">お問い合わせ内容</th>
              <td class="last">
                <textarea name="message" placeholder="例）ボトックスでしわを目立たなくさせたいです。" cols="20" rows="5"><?php echo $formTool->h($_POST['message']); ?></textarea>
              </td>
            </tr>
          </tbody>
        </table>

        <input type="hidden" name="mode" value="conf" />
        <p class="btn"><input type="image" src="images/form/formbtn_01.gif" alt="確認画面へ" width="420" height="70" /></p>

      </form>

    </div>
  </div>
</div>
<!--フォーム導入-->

		<p id="page-top"><a href="#wrap"><img src="images/botox_pagetop.png" width="48" height="46" alt="pagetop" class="sp_none"><img src="images/sp/botox_pagetop.png" alt="pagetop" width="35" height="35" class="pc_none"></a></p>