<?php
require('formClass.php');
$formTool = new formClass();
$mode = (empty($_POST['mode'])) ? "" : $_POST['mode'] ;
$error = false;
if($mode == 'comp') {
  $error = $formTool->is_inputerror();
  //完了画面のリロードで再送させないようにリダイレクト
  if(!$error){
    $formTool->send_return_mail();
    $formTool->send_admin_mail();
    header('Location: ./index_end.html');
  }
} else if ($mode == 'conf') {
  $error = $formTool->is_inputerror();
}
?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<title>表参道スキンクリニック | ボトックス注入で小じわに悩まない若々しい肌へ</title>
<meta name="Keywords" content="表参道スキンクリニック,ボトックス,小じわ,シワ,若返り" /> 
<meta name="Description" content="表参道クリニックでは、BOTOX VISTAを使用し、お顔のシワ取り、エラ張り、美脚、ワキや手足の多汗治療を丁寧に行っております。おでこ＆目まわりのシワ、鼻＆鼻まわりのシワ、顎＆首まわりのシワの改善は表参道クリニックへお任せください。" /> 
<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0">

<link rel="stylesheet" type="text/css" href="css/common.css" media="screen,print" />
<link rel="stylesheet" type="text/css" href="css/style.css" media="screen,print" />
<link rel="stylesheet" type="text/css" href="css/lightbox.css" media="screen,print" />

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script type="text/javascript" src="js/rollover.js"></script>
<script type="text/javascript" src="js/pagetop.js"></script>
<script type="text/javascript" src="js/jquery.bxslider.js"></script>
<script type="text/javascript" src="js/lightbox.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $('#slider').bxSlider({
        auto: true,
        pause: 5000,
        speed: 1000,
        pager: false,
        displaySlideQty: 3, //一画面に表示する数
        moveSlideQty: 1, //移動時にずれる数
        prevText: '<',
        nextText: '>'
        });
  });
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-59451323-1', 'auto');
  ga('send', 'pageview');

</script>

<script type='text/javascript' src='https://kaiu-marketing.com/visitor/conversion/cv_script.js?site_code=cf2c10d6274fed5a734c&key=314651bab7cec8d62af6&secret=8b218e26f1c4ca3ec8f1'></script>

</head>
<body id="wrap">
	<div id="header_bg">
		<div id="header" class="cf">
			<div class="logo"><img src="images/botox_logo.jpg" width="265" height="45" alt="表参道スキンクリニック" class="sp_none"><img src="images/sp/botox_logo.gif" alt="表参道スキンクリニック" width="50%" class="pc_none"></div>
			<div class="header_right cf">
				<div class="tel"><img src="images/botox_tel.gif" width="208" height="40" alt="0120-334-270 受付時間 11:00～20:00" class="sp_none"><a href="tel:0120334270" onclick="javascript:goog_report_conversion('tel:0120-334-270');yahoo_report_conversion(undefined);ga('send','event','sp','tel-tap','tel');return false;" class="pc_none"><img src="images/sp/botox_header_tel.gif" alt="0120-334-270 受付時間 11:00～20:00" width="100%"></a></div>
                <div class="btn"><a href="#section09" class="sp_none"><img src="images/botox_btn_off.png" width="249" height="40" alt="無料カウンセリングのご予約"></a><a href="#section09" class="pc_none"><img src="images/sp/botox_header_btn.gif" alt="無料カウンセリングのご予約" width="100%"></a></div>
			</div>
		</div>
	</div><!-- header !-->


    <?php if(basename($_SERVER['REQUEST_URI']) == 'comp.html') : ?>
      <?php include('comp.php'); ?>
    <?php elseif($_POST['mode'] == 'conf' && !$error) : ?>
      <?php include('conf.php'); ?>
    <?php else : ?>
      <?php include('edit_r.php'); ?>
      <?php if($error) : ?>
        <script type="text/javascript">
        window.location.hash = 'section09';
        </script>
      <?php endif; ?>
    <?php endif; ?>


<!-- YDN用リマーケティングタグ -->
<script type="text/javascript" language="javascript">
/* <![CDATA[ */
var yahoo_retargeting_id = 'A15O0FYNPU';
var yahoo_retargeting_label = '';
/* ]]> */
</script>
<script type="text/javascript" language="javascript" src="//b92.yahoo.co.jp/js/s_retargeting.js"></script>
</body>
</html>
