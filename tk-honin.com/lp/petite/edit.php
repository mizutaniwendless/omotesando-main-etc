<?php
global $formTool;
?>

<div class="cnt_wp">


    <ul id="navi">
        <li><a href="#section01" class="on">●</a></li>
        <li><a href="#section02">●</a></li>
        <li><a href="#section03">●</a></li>
        <li><a href="#section04">●</a></li>
        <li><a href="#section05">●</a></li>
        <li><a href="#section06">●</a></li>
        <li><a href="#section07">●</a></li>
        <!--
        <li><a href="#section08">●</a></li>
        -->
        <li><a href="#section09">●</a></li>
        <!--
        <li><a href="#section10">●</a></li>
        <li><a href="#section11">●</a></li>
        -->
        <li><a href="#section15">●</a></li>
    </ul>


<article class="block01">
    <h1>
    <img class="sp_none" src="img/main_v.jpg" width="1500" height="514" alt="埋没法 ヒアルロン酸 ボトックス/BNLS メスを入れずにフチ整形で“なりたい顔”に憧れのモデル顔も夢じゃない!?">
    <img class="pc_none" src="img/sp_main_v.jpg" width="100%" alt="埋没法 ヒアルロン酸 ボトックス/BNLS メスを入れずにフチ整形で“なりたい顔”に憧れのモデル顔も夢じゃない!?">
    </h1>
</article>
<div id="section01" class="box">
    <article class="block02">
        <div class="block02_in">
            <h2>
            <img class="sp_none" src="img/block02_title.png" width="912" height="34" alt="憧れのモデル顔になりたい！">
            <img class="pc_none" src="img/sp_block02_title01.png" width="100%" alt="憧れのモデル顔になりたい！">
            </h2>
            <div class="sp_area">
                <p>
                「ちょっと気になるパーツがある…<br class="pc_none">
                でも、メスは入れたくない！」<br>
                そんなあなたに、メス要らず、かつ、日帰りでできるプチ整形方法をご紹介します。<br>
                痛みや体への負担も少なく、あまりの手軽さにびっくりしてしまうかもしれません。
                </p>
                <div class="sp_none main_box">
                    <img class="woman" src="img/block02_img01.png" width="395" height="464" alt="img">
                    <div class="box box01">
                        <h3>埋没法</h3>
                        <img class="point" src="img/block02_point01.png" width="76" height="55" alt="Point1">
                        <dl>
                            <dt>二重まぶたの形成</dt>
                            <dd>特殊な極細糸を使用した、自然な二重づくり。</dd>
                        </dl>
                        <a href="#section02">詳しく見る</a>
                    </div>
                    <div class="box box02">
                        <h3>ヒアルロン酸注入</h3>
                        <img class="point" src="img/block02_point02.png" width="76" height="55" alt="Point2">
                        <dl>
                            <dt>涙袋形成</dt>
                            <dd>ほんの少量のヒアルロン酸で目力アップ、ふっくら涙袋づくり。</dd>
                        </dl>
                        <a href="#section03">詳しく見る</a>
                        <hr>
                        <dl>
                            <dt>鼻を高くする隆鼻</dt>
                            <dd>ヒアルロン酸で鼻筋にハリ、高くキレイな鼻筋づくり。</dd>
                        </dl>
                        <a href="#section03">詳しく見る</a>
                        <hr>
                        <dl>
                            <dt>アゴラインの形成</dt>
                            <dd>ヒアルロン酸でアゴをボリュームアップ、丸く整ったアゴづくり。</dd>
                        </dl>
                        <a href="#section03">詳しく見る</a>
                    </div>
                    <div class="box box03">
                        <h3>ボトックス注射</h3>
                        <img class="point" src="img/block02_point03.png" width="76" height="55" alt="Point3">
                        <dl>
                            <dt>筋肉の張りを改善しシャープな小顔に</dt>
                            <dd>注射でできるエラ張り改善、太い筋肉をスマートにする「ボトックス注射」</dd>
                        </dl>
                        <a href="#section04">詳しく見る</a>
                    </div>
                    <div class="box box04">
                        <h3>BNLS注射</h3>
                        <img class="point" src="img/block02_point04.png" width="76" height="55" alt="Point3">
                        <dl>
                            <dt>脂肪を溶かしてスッキリした小顔に</dt>
                            <dd>注射でできる小顔づくり、脂肪を溶かす「BNLS注射」</dd>
                        </dl>
                        <a href="#section05">詳しく見る</a>
                    </div>
                </div>
                <!-- SP_START -->
                <div class="pc_none link_box">
                    <img class="sp_block02_img01" src="img/sp_block02_img01.png" width="100%" alt="埋没法 ヒアルロン酸注入 ボトックス注射/BNLS注射">
                    <p class="link01"><a href="#section02"><img src="img/sp_block02_link01.png" width="100%" alt="詳しく見る"></a></p>
                    <p class="link02"><a href="#section03"><img src="img/sp_block02_link02.png" width="100%" alt="詳しく見る"></a></p>
                    <p class="link03"><a href="#section03"><img src="img/sp_block02_link02.png" width="100%" alt="詳しく見る"></a></p>
                    <p class="link04"><a href="#section03"><img src="img/sp_block02_link02.png" width="100%" alt="詳しく見る"></a></p>
                    <p class="link05"><a href="#section04"><img src="img/sp_block02_link03.png" width="100%" alt="ボトックス注射を詳しく見る"></a></p>
                    <p class="link06"><a href="#section05"><img src="img/sp_block02_link04.png" width="100%" alt="BNLS注射を詳しく見る"></a></p>
                </div>
                <!-- SP_END -->
                <p class="color_text">
                当院では患者様と向き合う<br class="pc_none">丁寧なカウンセリングを<br class="pc_none">大切にしています。<br>
                経験豊富な医師が患者様<br class="pc_none">一人ひとりに合った施術プランを<br class="pc_none">
                ご提案・ご提供いたしますので、<br>なんでもお気軽にご相談ください。
                </p>
            </div>
        </div>
    </article>
	<div class="btn_box btn_box01 cf">
        <dl class="cf sp_none">
        	<dt><img src="images/tk_cv_pc1.gif" width="480" height="75" alt="0120-334-270 受付時間11:00～20:00・日祝10:30～18:00/水曜休診"></dt>
            <dd><a href="#section15"><img src="img/linkarea_btn.png" width="299" height="59" alt="無料カウンセリングのご予約"></a></dd>
        </dl>
        <dl class="cf pc_none">
        	<dt>

<a href="tel:0120334270" onclick="javascript:goog_report_conversion('tel:0120-334-270');yahoo_report_conversion(undefined);ga('send','event','sp','tel-tap','tel');return false;"><img src="images/tk_cv_sp1.gif" width="100%" alt="0120-334-270"></a>

<a href="tel:0120931911" onclick="javascript:goog_report_conversion('tel:0120-931-911');yahoo_report_conversion(undefined);ga('send','event','sp','tel-tap','tel');return false;"><img src="images/tk_cv_sp2.gif" width="100%" alt="0120-334-270"></a>

</dt>
            <dd><a href="#section15"><img src="img/sp_linkarea_btn.png" width="100%" alt="無料カウンセリングのご予約"></a></dd>
        </dl>
    </div>
</div>    
<div id="section02" class="box">
	<article class="block03">
        <h2 class="sp_none"><img src="img/block03_point01.png" width="125" height="91" alt="Method1">埋没法：二重まぶたの形成</h2>
        <h2 class="pc_none"><img src="img/sp_block03_title01.png" width="100%" alt="Method1 埋没法：二重まぶたの形成"></h2>
        <div class="sp_area">
            <div class="block03_in">
                <div class="con_top cf">
                    <div class="left_area box">
                        <dl>
                            <dt><p>特殊な糸を使って、<br>自然な二重をつくることができます</p></dt>
                            <dd>
                            埋没法は、特殊な極細糸を使い、患者様ご希望の二重ラインに合わせてまぶたの裏側から縫い止める方法です。<br>
                            5～10分程度という短い手術時間でできること、痛みや腫れも比較的少ないことから、
                            当院の中でも人気の高い施術となっています。
                            </dd>
                        </dl>
                    </div>
                    <div class="right_area box cf">
                        <img class="sp_none" src="img/block03_img01.png" width="532" height="291" alt="before after">
                        <p class="text01 sp_none">左右の目の大きさの違い、二重の幅の差、一重で腫れぼったく見えるなどのお悩みを解消したい。</p>
                        <p class="text02 sp_none">埋没法なら自由に二重の幅や大きさをデザイン出来ます。パッチリとした印象的な目元になります。糸で縫い留めているので、もしイメージと合わない場合も変更可能です。</p>
                        <div class="img_text cf pc_none">
                            <img src="img/sp_block03_img01.png" width="100%" alt="before after">
                            <p class="text01">左右の目の大きさの違い、二重の幅の差、一重で腫れぼったく見えるなどのお悩みを解消したい。</p>
                            <p class="text02">埋没法なら自由に二重の幅や大きさをデザイン出来ます。パッチリとした印象的な目元になります。糸で縫い留めているので、もしイメージと合わない場合も変更可能です。</p>
                        </div>
                    </div>
                </div>
                <div class="con_center cf">
                    <h3>施術内容</h3>
                    <div class="sp_none left_area box cf">
                        <img src="img/block03_img02.jpg" width="200" height="150" alt="埋没法１点止め">
                        <dl>
                            <dt><h4>埋没法１点止め</h4></dt>
                            <dd>
                            二重をつくりたいラインの真ん中1点を縫い止める方法です。
                            埋没法の中では体への負担がもっとも少なく、まぶたの皮膚が薄い方や、
                            術後にも二重ラインの微調整を考えている方に好まれる埋没法です。
                            </dd>
                        </dl>
                    </div>
                    <table class="pc_none">
                        <tr>
                            <th><img src="img/sp_block03_img02.png" width="100%" alt="埋没法１点止め"></th>
                            <td>
                            <span>埋没法１点止め</span>
                            二重をつくりたいラインの真ん中1点を縫い止める方法です。
                            埋没法の中では体への負担がもっとも少なく、まぶたの皮膚が薄い方や、
                            術後にも二重ラインの微調整を考えている方に好まれる埋没法です。
                            </td>
                        </tr>
                    </table>
                    <div class="sp_none right_area box cf">
                        <img src="img/block03_img03.jpg" width="200" height="150" alt="埋没法2点止め">
                        <dl>
                            <dt><h4>埋没法2点止め</h4></dt>
                            <dd>
                            左記の1点止めよりさらに強固で、二重を長く維持しやすい方法です。
                            1点止めが向かないまぶたをお持ちの方、術後そのままの状態でより長持ちさせたい方におススメしています。
                            </dd>
                        </dl>
                    </div>
                    <table class="pc_none">
                        <tr>
                            <th><img src="img/sp_block03_img03.png" width="100%" alt="埋没法１点止め"></th>
                            <td>
                            <span>埋没法2点止め</span>
                            左記の1点止めよりさらに強固で、二重を長く維持しやすい方法です。
                            1点止めが向かないまぶたをお持ちの方、術後そのままの状態でより長持ちさせたい方におススメしています。
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="faq_area cf">
                    <h3>
                    <img class="sp_none" src="img/block03_title01.png" width="1200" height="50" alt="FAQ 埋没法：二重まぶたの形成に関するよくある質問">
                    <img class="pc_none" src="img/sp_block03_title03.png" width="100%" alt="FAQ 埋没法：二重まぶたの形成に関するよくある質問">
                    </h3>
                    <div class="left_area box cf">
                        <ul class="accordion_ul">
                            <li>
                                <p>痛みや腫れについて教えてください</p>
                                <ul>
                                    <li>
                                    局所麻酔を行うので、手術中の痛みはほとんどありません。
                                    また、術後も著しい痛みはなく、鈍痛も通常１?２日でおさまります。
                                    腫れについては、術後３?４日程度をピークに落ち着いてきます。
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <p>まぶたに傷跡は残りますか？</p>
                                <ul>
                                    <li>
                                    手術後、一時的に赤みが生じる場合がありますが、傷跡はほとんど残りません。
                                    また、時間とともに糸は自然にまぶたに埋もれていくので、他人から気づかれる心配もまずありません。
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <p>当日からシャワーを浴びることはできますか？</p>
                                <ul>
                                    <li>
                                    シャワーのみでしたら手術の翌日から、入浴は２～３日後から可能です。
                                    洗顔は当日からしていただくことができます。
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <p>すぐにメイクをしたいのですが</p>
                                <ul>
                                    <li>
                                    メイクは手術の翌日から可能です。ただし、アイメイクに関しては、
                                    術後1週間ほど経ってから行っていただく方が安心です。
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="right_area box cf">
                        <ul class="accordion_ul">
                            <li>
                                <p>コンタクトレンズを着けても大丈夫ですか？</p>
                                <ul>
                                    <li>ワンデータイプのものは、基本的に手術の翌日より使用可能です。</li>
                                </ul>
                            </li>
                            <li>
                                <p>元の状態に戻すことは可能ですか？</p>
                                <ul>
                                    <li>仕上がりがイメージと違った場合などは元に戻すことができ、また二重のラインを変更することも可能です。</li>
                                </ul>
                            </li>
                            <li>
                                <p>止めた糸は、まぶたに残るのでしょうか</p>
                                <ul>
                                    <li>糸は残って二重まぶたをキープします。</li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        	<div class="con_bottom">
                <h3>
                <img class="sp_none" src="img/block03_title02.png" width="1200" height="52" alt="PRICE 埋没法：二重まぶた　施術料金">
                <img class="pc_none" src="img/sp_block03_title04.png" width="100%" alt="PRICE 埋没法：二重まぶた　施術料金">
                </h3>
                <dl class="price_box cf">
                    <dt>スタンダードクイック（2点留め）両目</dt>
                    <dd>18,000円<span>（+税）</span></dd>
                    <dt>スタンダードクイック（3点留め）両目</dt>
                    <dd>30,000円<span>（+税）</span></dd>
                </dl>
            </div>
        </div>
    </article>
	<div class="btn_box btn_box02 cf">
        <dl class="cf sp_none">
        	<dt><img src="images/tk_cv_pc1.gif" width="480" height="75" alt="0120-334-270 受付時間11:00～20:00・日祝10:30～18:00/水曜休診"></dt>
            <dd><a href="#section15"><img src="img/linkarea_btn.png" width="299" height="59" alt="無料カウンセリングのご予約"></a></dd>
        </dl>
        <dl class="cf pc_none">
        	<dt>

<a href="tel:0120334270" onclick="javascript:goog_report_conversion('tel:0120-334-270');yahoo_report_conversion(undefined);ga('send','event','sp','tel-tap','tel');return false;"><img src="images/tk_cv_sp1.gif" width="100%" alt="0120-334-270"></a>

<a href="tel:0120931911" onclick="javascript:goog_report_conversion('tel:0120-931-911');yahoo_report_conversion(undefined);ga('send','event','sp','tel-tap','tel');return false;"><img src="images/tk_cv_sp2.gif" width="100%" alt="0120-334-270"></a>

</dt>
            <dd><a href="#section15"><img src="img/sp_linkarea_btn.png" width="100%" alt="無料カウンセリングのご予約"></a></dd>
        </dl>
    </div>
</div>
<div id="section03" class="box">
	<article class="block04">
        <h2 class="sp_none"><img src="img/block04_point01.png" width="114" height="91" alt="Point2">ヒアルロン酸注入による施術</h2>
        <h2 class="pc_none"><img src="img/sp_block04_title01.png" width="100%" alt="ヒアルロン酸注入による施術"></h2>
        <div class="block04_in">
            <div class="sp_area">
                <div class="con_top cf">
                    <div class="left_area box">
                        <dl>
                            <dt><p>当院は100％ピュア・ヒアルロン酸を使用</p></dt>
                            <dd>
                            「ヒアルロン酸って、クリニックによってずいぶん値段が違うけど、どうして？」…　
                            一口にヒアルロン酸と言ってもその種類はさまざまで、効果や成分の質も実に幅広くなっています。<br><br>
                            当院で使用しているのは、100％ピュア・ヒアルロン酸。非動物性（バイオテクノロジーによって精製されたもの）のため、
                            従来の動物性ヒアルロン酸に比べ、鳥インフルエンザなど感染症の心配がなく、安心してご利用いただけることが特徴です。<br><br>
                            注入したヒアルロン酸は、効果を発揮しながら徐々に体内に吸収されていきます。体に負担がかからないうえ、その効果は1回の注入で、
                            平均的には1年ほど持続します。これらのメリットから、ヒアルロン酸注入は当院でも高い人気をもつ施術のひとつとなっています。
                            「高品質で効果の高いヒアルロン酸のみを厳選し、患者様の安心・安全を守りたい」当院ではそう考えています。
                            </dd>
                        </dl>
                    </div>
                    <div class="right_area box">
                        <img src="img/block04_img01.png" width="544" height="354" alt="img">
                    </div>
                </div>
                <!--
                <div class="sp_none con_link cf">
                    <a href="#area01"><img src="img/block04_link01.png" width="318" height="111" alt="涙袋を作りたい"></a>
                    <a href="#area02"><img src="img/block04_link02.png" width="318" height="111" alt="鼻を高くしたい"></a>
                    <a href="#area03"><img class="none_mr" src="img/block04_link03.png" width="318" height="111" alt="アゴのラインを形成したい"></a>
                </div>
                <table class="pc_none con_link">
                    <tr>
                        <td><a href="#area01"><img src="img/sp_block04_link01.png" width="100%" alt="涙袋を作りたい"></a></td>
                        <td><a href="#area02"><img src="img/sp_block04_link02.png" width="100%" alt="鼻を高くしたい"></a></td>
                        <td><a href="#area03"><img src="img/sp_block04_link03.png" width="100%" alt="アゴのラインを形成したい"></a></td>
                    </tr>
                </table>
                -->
                <div class="con_middle con_middle01" id="area01">
                    <h3 class="sp_none">
                    <img src="img/block04_title01.png" width="550" height="46" alt="ヒアルロン酸注入：涙袋形成">
                    <span>ふっくら涙袋に憧れる！という方に</span>
                    </h3>
                    <h3 class="pc_none">
                    <img src="img/sp_block04_title02.png" width="100%" alt="ヒアルロン酸注入：涙袋形成 ふっくら涙袋に憧れる！という方に">
                    </h3>
                    <div class="text_area cf">
                        <div class="text_area_right sp_none">
                            <p>女性らしさを演出するふっくらとした涙袋は、モデルや女優にもよく見られますね。<br>
                            魅力的な涙袋は、ほんの少量のヒアルロン酸を注入するだけでつくることができます。
                            </p>
                            <img src="img/block04_img05.png" width="200" height="50" alt="イメージ写真">
                        </div>
                        <div class="text_area_left sp_none">
                            <img src="img/block04_img02.png" width="513" height="230" alt="before after">
                            <p class="text01">涙袋で”女性らしさ”や”セクシーで色っぽい”目元の印象を作りたい。目力をアップさせたい。</p>
                            <p class="text02">目の周りに明るさがプラスされるため、優しげで女性らしいシルエットに。
                            目が大きく見え、顎から下の部分の面積が小さくなり、小顔効果も。
                            </p>
                        </div>
                        <p class="pc_none">女性らしさを演出するふっくらとした涙袋は、モデルや女優にもよく見られますね。<br>
                        魅力的な涙袋は、ほんの少量のヒアルロン酸を注入するだけでつくることができます。
                        </p>
                        <div class="img_text cf pc_none">
                            <img class="pc_none" src="img/sp_block04_img02.png" width="100%" alt="before after">
                            <p class="text01">涙袋で”女性らしさ”や”セクシーで色っぽい”目元の印象を作りたい。目力をアップさせたい。</p>
                            <p class="text02">目の周りに明るさがプラスされるため、優しげで女性らしいシルエットに。
                            目が大きく見え、顎から下の部分の面積が小さくなり、小顔効果も。</p>
                        </div>
                    </div>
                </div>
                <div class="con_middle con_middle02" id="area02">
                    <h3 class="sp_none">
                    <img src="img/block04_title02.png" width="550" height="46" alt="ヒアルロン酸注入：鼻を高くする"/>
                    <span>鼻を高くキレイなラインにしたい！という方に</span>
                    </h3>
                    <h3 class="pc_none">
                    <img src="img/sp_block04_title03.png" width="100%" alt="ヒアルロン酸注入：鼻を高くする 鼻を高くキレイなラインにしたい！という方に">
                    </h3>
                    <div class="text_area cf">
                        <p class="text_area_right">「ハリウッド女優やハーフモデルのようなスッと通った鼻筋に憧れる」
                        そんな方に最適なのがヒアルロン酸注入による隆鼻術です。<br>
                        鼻の上部分・鼻筋となる箇所にヒアルロン酸がボリュームとハリをもたせることで、高くキレイな鼻筋づくりができます。
                        </p>
                        <div class="text_area_left sp_none">
                            <img class="sp_none" src="img/block04_img03.png" width="512" height="240" alt="before after">
                            <p class="text01">「鼻すじをキレイに高くしたいけれど、メスを入れるのは抵抗が…」そんな方にお勧めです。</p>
                            <p class="text02">理想の鼻筋に整えます。施術時間も短く、痛みや腫れもほとんどありません。
                            メイクも当日から可能です。
                            </p>
                        </div>
                        <div class="img_text cf pc_none">
                            <img class="pc_none" src="img/sp_block04_img03.png" width="100%" alt="before after">
                            <p class="text01">「鼻すじをキレイに高くしたいけれど、メスを入れるのは抵抗が…」そんな方にお勧めです。</p>
                            <p class="text02">理想の鼻筋に整えます。施術時間も短く、痛みや腫れもほとんどありません。
                            メイクも当日から可能です。</p>
                        </div>
                    </div>
                </div>
                <div class="con_middle con_middle03" id="area03">
                    <h3 class="sp_none">
                    <img src="img/block04_title03.png" width="550" height="46" alt="ヒアルロン酸注入：アゴのラインを形成"/>
                    <span>アゴが引っ込んでいる…というお悩みに</span>
                    </h3>
                    <h3 class="pc_none">
                    <img src="img/sp_block04_title04.png" width="100%" alt="ヒアルロン酸注入：アゴのラインを形成 アゴが引っ込んでいる…というお悩みに">
                    </h3>
                    <div class="text_area cf">
                        <p class="text_area_right">美しいアゴのラインは横顔美人の必須条件ですね。
                        ヒアルロン酸はアゴに注入することも可能です。<br>
                        ヒアルロン酸が皮膚をボリュームアップさせ、アゴのラインを整えます。<br>
                        注入する量はカウンセリングにて、患者様に合った量を相談していきましょう。
                        </p>
                        <div class="text_area_left sp_none">
                            <img class="sp_none" src="img/block04_img04.png" width="511" height="239" alt="before after">
                            <p class="text01">理想的な顔のバランスである鼻の頭→唇→あごを結ぶ線が直線でつながる
                            “エステティックライン（Eライン）をつくりたい。</p>
                            <p class="text02">約10分程の施術で、あごの高さを整え、引き締まったフェイスラインに。
                            顔全体も引き締まって見えるので、小顔効果もばっちりです。</p>
                        </div>
                        <div class="img_text cf pc_none">
                            <img class="pc_none" src="img/sp_block04_img04.png" width="100%" alt="before after">
                            <p class="text01">想的な顔のバランスである鼻の頭→唇→あごを結ぶ線が直線でつながる
                            “エステティックライン（Eライン）をつくりたい。</p>
                            <p class="text02">約10分程の施術で、あごの高さを整え、引き締まったフェイスラインに。
                            顔全体も引き締まって見えるので、小顔効果もばっちりです。</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </article>
	<div class="btn_box btn_box03 cf">
        <dl class="cf sp_none">
        	<dt><img src="images/tk_cv_pc1.gif" width="480" height="75" alt="0120-334-270 受付時間11:00～20:00・日祝10:30～18:00/水曜休診"></dt>
            <dd><a href="#section15"><img src="img/linkarea_btn.png" width="299" height="59" alt="無料カウンセリングのご予約"></a></dd>
        </dl>
        <dl class="cf pc_none">
        	<dt>

<a href="tel:0120334270" onclick="javascript:goog_report_conversion('tel:0120-334-270');yahoo_report_conversion(undefined);ga('send','event','sp','tel-tap','tel');return false;"><img src="images/tk_cv_sp1.gif" width="100%" alt="0120-334-270"></a>

<a href="tel:0120931911" onclick="javascript:goog_report_conversion('tel:0120-931-911');yahoo_report_conversion(undefined);ga('send','event','sp','tel-tap','tel');return false;"><img src="images/tk_cv_sp2.gif" width="100%" alt="0120-334-270"></a>

</dt>
            <dd><a href="#section15"><img src="img/sp_linkarea_btn.png" width="100%" alt="無料カウンセリングのご予約"></a></dd>
        </dl>
    </div>
	<article class="block05">
        <div class="block05_in">
            <div class="sp_area">
                <div class="faq_area cf">
                    <h3>
                    <img class="sp_none" src="img/block04_title04.png" width="1200" height="52" alt="FAQ ヒアルロン酸注入に関するよくある質問">
                    <img class="pc_none" src="img/sp_block05_title01.png" width="100%" alt="FAQ ヒアルロン酸注入に関するよくある質問">
                    </h3>
                    <div class="left_area box cf">
                        <ul class="accordion_ul">
                            <li>
                                <p>ヒアルロン酸は安全ですか？</p>
                                <ul>
                                    <li>ヒアルロン酸はもともと体内にある物質のため、副作用の心配もほとんどありません。</li>
                                </ul>
                            </li>
                            <li>
                                <p>ヒアルロン酸でどのような治療ができますか？</p>
                                <ul>
                                    <li>
                                    ヒアルロン酸では以下のような治療ができます。
                                    目下のクマ・タルミ、ほうれい線、マリオネットライン（口元からアゴにかけてのしわ）、額・目尻・眉間の深いしわ、
                                    こめかみや頬の凹み、鼻を高くする、唇をふっくらさせる、アゴを出す、豊胸など。
                                    この他にもヒアルロン酸治療適応箇所がございますので詳しくはご相談ください。
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <p>治療にかかる時間はどれくらいですか？</p>
                                <ul>
                                    <li>注入の範囲にもよりますが5～30分ほどです。</li>
                                </ul>
                            </li>
                            <li>
                                <p>すぐに治療を受けられますか？</p>
                                <ul>
                                    <li>人間の持っているヒアルロン酸と同じ天然物質から作られたヒアルロン酸を使用しますので、事前検査は必要ありません。</li>
                                </ul>
                            </li>
                            <li>
                                <p>施術時の痛みが心配です。</p>
                                <ul>
                                    <li>
                                    注射による施術のため、ある程度の痛みはあります。
                                    当美容皮膚科ではなるべく痛みを抑えるために、局所麻酔を塗って、極細の針で注射します。
                                    痛みに弱い方にも安心して治療をお受け頂けます。
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="right_area box cf">
                        <ul class="accordion_ul">
                            <li>
                                <p>ヒアルロン酸の効果はどれ位もちますか？</p>
                                <ul>
                                    <li>
                                    注射用ヒアルロン酸は約1年間効果が持続します。（製剤の種類や注入技術、個体差で差があります）
                                    ヒアルロン酸は完全に分解される物質ですので、体内に残るということはありません。
                                    ヒアルロン酸の吸収度合いにより、年に１回程度の注入が可能です。
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <p>施術後の副作用はありますか？</p>
                                <ul>
                                    <li>
                                    ヒアルロン酸は針を使用するために内出血を起こす可能性があります。
                                    内出血が起きやすい体質の方もいらっしゃいますので、念のために1回目は1～2週間程度、
                                    大切な御用のない時を選んでお越し下さい。
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <p>治療後に気を付けることはありますか？</p>
                                <ul>
                                    <li>
                                    ヒアルロン酸を注射した後は、通常の日常生活を送ることが出来ます。
                                    お化粧も治療後すぐにでき、入浴、運動、飲酒も問題ありません。
                                    ただし、感染予防のため、注入部位を必要以上に触らないように気を付けて下さい。
                                    初めての方は、違和感を感じることがありますが、数日でなじみます。
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <p>ヒアルロン酸を入れ過ぎてしまったらどうしたらいいですか？</p>
                                <ul>
                                    <li>
                                    入り過ぎてしまったり凸凹が残ってしまったヒアルロン酸は分解することができます。
                                    他院でヒアルロン酸を入れ過ぎてしまった方、デコボコ感が気になる方、ぜひお気軽にご相談下さい。
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <p>持病があっても大丈夫ですか？禁忌は？</p>
                                <ul>
                                    <li>
                                    ・糖尿病・膠原病の方　 ・妊娠されている方・授乳中の方　 ・注入箇所周辺が化膿している方　 ・ケロイド体質の方
                                    などはお控えください。
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="con_bottom">
                    <h3>
                    <img class="sp_none" src="img/block04_title05.png" width="1200" height="53" alt="PRICE ヒアルロン酸注入 施術料金">
                    <img class="pc_none" src="img/sp_block05_title02.png" width="100%" alt="PRICE ヒアルロン酸注入 施術料金">
                    </h3>
                    <dl class="price_box cf">
                        <dt>ヒアルロン酸　0.1cc</dt>
                        <dd>4,800円<span>（+税）</span>～</dd>
                    </dl>
                </div>
            </div>
        </div>
    </article>
	<div class="btn_box btn_box04 cf">
        <dl class="cf sp_none">
        	<dt><img src="images/tk_cv_pc1.gif" width="480" height="75" alt="0120-334-270 受付時間11:00～20:00・日祝10:30～18:00/水曜休診"></dt>
            <dd><a href="#section15"><img src="img/linkarea_btn.png" width="299" height="59" alt="無料カウンセリングのご予約"></a></dd>
        </dl>
        <dl class="cf pc_none">
        	<dt>

<a href="tel:0120334270" onclick="javascript:goog_report_conversion('tel:0120-334-270');yahoo_report_conversion(undefined);ga('send','event','sp','tel-tap','tel');return false;"><img src="images/tk_cv_sp1.gif" width="100%" alt="0120-334-270"></a>

<a href="tel:0120931911" onclick="javascript:goog_report_conversion('tel:0120-931-911');yahoo_report_conversion(undefined);ga('send','event','sp','tel-tap','tel');return false;"><img src="images/tk_cv_sp2.gif" width="100%" alt="0120-334-270"></a>

</dt>
            <dd><a href="#section15"><img src="img/sp_linkarea_btn.png" width="100%" alt="無料カウンセリングのご予約"></a></dd>
        </dl>
    </div>
</div>
<div id="section04" class="box">
	<article class="block06">
        <h2 class="sp_none"><img src="img/block06_point03.png" width="114" height="91" alt="Point3">ボトックス注射：エラの筋肉の張りを改善しシャープな小顔に</h2>
        <h2 class="pc_none"><img src="img/sp_block06_title01.png" width="100%" alt="Point3 ボトックス注射：エラの筋肉の張りを改善しシャープな小顔に"></h2>
        <div class="block06_in">
            <div class="sp_area">
                <div class="con_top cf">
                    <div class="left_area box cf">
                        <dl>
                            <dt><p>メスを使わず小顔づくり</p></dt>
                            <dd>
                                <table>
                                    <tr>
                                        <td>
                                        顔のシワ対策に使われることで有名なボトックス注射ですが、発達しすぎたエラの筋肉に使うことも効果的です。<br><br>
                                        筋肉の働きを抑制し、角ばったエラの筋肉をシャープに整えていきます。じわじわと効果が表れるため、
                                        「あれ、痩せた？」と言われるような確実でありながらも自然な変身ができることが特徴です。<br><br>
                                        個人差はありますが、平均的には1回の注入で約6ヵ月程度の効果が持続します。
                                        </td>
                                        <th>
                                        <img src="img/block06_img01.png" width="174" height="222" alt="img">
                                        </th>
                                    </tr>
                                </table>
                            </dd>
                        </dl>
                    </div>
                    <div class="right_area box cf">
                        <img class="sp_none" src="img/block06_img02.png" width="532" height="272" alt="before after">
                        <p class="text01 sp_none">筋肉の張りよって大きく見えるエラを解消してシャープな輪郭に見せたい。</p>
                        <p class="text02 sp_none">注射のみの簡単プチ小顔術で、フェイスラインを整えます。筋肉を緩ませることでシャープな小顔に。
                        </p>
                        <div class="img_text cf pc_none">
                            <img class="pc_none" src="img/sp_block06_img02.png" width="100%" alt="before after">
                            <p class="text01">筋肉の張りよって大きく見えるエラを解消してシャープな輪郭に見せたい。</p>
                            <p class="text02">注射のみの簡単プチ小顔術で、フェイスラインを整えます。筋肉を緩ませることでシャープな小顔に。</p>
                        </div>
                    </div>
                </div>
                <div class="con_center">
                    <h3>施術内容</h3>
                    <div class="cf">
                        <img class="sp_none" src="img/block06_img03.png" width="400" height="280" alt="グラフ">
                        <img class="sp_none none_mr" src="img/block06_img04.png" width="636" height="273" alt="イメージ図">
                        <div class="text_area pc_none">
                        	<img src="img/sp_block06_img03.png" width="100%" alt="グラフ">
                        	<img src="img/sp_block06_img04.png" width="100%" alt="イメージ図">
                            <p>ボトックスは、筋肉の緊張をほぐすことで、シワを解消するメカニズムのシワ治療です。</p>
                        </div>
                    </div>
                </div>
                <div class="faq_area cf">
                    <h3>
                    <img class="sp_none" src="img/block06_title01.png" width="1200" height="51" alt="FAQ ボトックス注射に関するよくある質問">
                    <img class="pc_none" src="img/sp_block06_title02.png" width="100%" alt="FAQ ボトックス注射に関するよくある質問">
                    </h3>
                    <div class="left_area box cf">
                        <ul class="accordion_ul">
                            <li>
                                <p>ボトックスでどんな治療ができますか？</p>
                                <ul>
                                    <li>
                                    ボトックスは表情しわに効果的です。
                                    表情しわには、眉間の縦しわ、額の横しわ、目尻のしわ、アゴの梅干状のしわ、口元のしわなどがあります。
                                    また、額をリフトする効果もあります。
                                    ボトックスはしわ以外に小顔治療、脚を細くする、多汗症治療にも効果があります。
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <p>ヒアルロン酸とボトックスとは何が違うのですか？</p>
                                <ul>
                                    <li>
                                    効果のあるしわの種類が異なります。
									ヒアルロン酸は細胞内でのヒアルロン酸やコラーゲン、エラスチンの生成力が落ちる事によって生じた、いわゆる溝的なほうれい線などのしわに注入する事で、溝が埋まりしわを目立たなくさせる治療です。
									ボトックスは、目尻や額などの筋肉が作用する事によって起こるしわに注入する事で、筋肉の働きを抑えしわを出にくくします。つまりしわを寄らなくするわけです。
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <p>治療にどれくらい時間がかかりますか？</p>
                                <ul>
                                    <li>
                                    治療は5分～10分程度で終わります。
									麻酔に別途、30分程度お時間を頂きます。
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <p>効果はどれ位持ちますか？</p>
                                <ul>
                                    <li>
                                    しわ治療はボトックス注射後48時間後以降で効果が現れ、その効果は3～6ヶ月ほど続きます。
									一般的に、2回目以降の治療効果は初回より長く持続すると言われています。
									これは、しわを作る原因筋が、ボトックスが効いている一定期間、あまり動かされないので、少しずつ萎縮して、しわが作られにくくなるためと考えられます。
									小顔治療や足を細くする治療は1回目の効果が現れるまでに1～2ヶ月かかり、効果は3～6ヶ月程度続きます。2回目以降は効果がより早く現れ（1～2週間）、1回目に比べて効果もしっかり出ます。
									小顔治療は4～6回程度繰り返すと半永久的な効果が得られることが多々あります。
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <p>施術時の痛みが心配です。</p>
                                <ul>
                                    <li>
                                    注射による施術のため、ある程度の痛みはあります。
									当美容皮膚科ではなるべく痛みを抑えるために使用し、極細の針で注射します。
									痛みに弱い方にも安心して治療をお受け頂けます。
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="right_area box cf">
                        <ul class="accordion_ul">
                            <li>
                                <p>ボトックスでボツリヌス中毒になることはありますか？</p>
                                <ul>
                                    <li>
                                    ボツリヌス菌は食中毒を起こす菌として知られていますが、ボトックスはボツリヌス菌そのものを注射するわけではありません。
									ボツリヌス菌の毒素であるボツリヌス毒素をごく少量だけ注射します。
									食中毒を起こすボツリヌス毒素の量は3万単位程度なのに比べ、美容に使用される量は、2.5～100単位と少量であり、適切な治療を行っている限り、中毒をおこす心配はまずありません。
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <p>ボトックスはどんな副反応がありますか？</p>
                                <ul>
                                    <li>
                                    針の跡が点状に残ったり、内出血が起こることがありますが、これらの反応は一時的で、お化粧で隠せる程度です。ボトックスのデザインによっては仮面のような不自然な表情になったり、目が下がってしまうという症状も見られます。
									当クリニックでは熟練した医師の治療いたしますので、ご安心ください。
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <p>施術後当日にメイクはできますか？</p>
                                <ul>
                                    <li>
                                    当日のメイクは可能です。
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <p>妊娠中でも施術できますか？</p>
                                <ul>
                                    <li>
                                    当院ではすでに妊娠が分かっている方には治療を行っていません。
									また注入後も念のため、最低3ヶ月間は妊娠や授乳を避けていただくようお願いしています。
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <p>ボトックスの薬剤はどのようなものを使いますか？</p>
                                <ul>
                                    <li>
                                    米国FDAの認可を受けている、アラガン社製のボトックスを使用しています。
									アラガン社のボトックスは、全世界で信頼されている製品です。
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="con_bottom">
                    <h3>
                    <img class="sp_none" src="img/block06_title02.png" width="1200" height="50" alt="PRICE ボトックス注射　施術料金">
                    <img class="pc_none" src="img/sp_block06_title03.png" width="100%" alt="PRICE ボトックス注射　施術料金">
                    </h3>
                    <dl class="price_box cf">
                        <dt>エラ</dt>
                        <dd>39,800円<span>（+税）</span>～</dd>
                    </dl>
                </div>
            </div>
        </div>
    </article>
	<div class="btn_box btn_box05 cf">
        <dl class="cf sp_none">
        	<dt><img src="images/tk_cv_pc1.gif" width="480" height="75" alt="0120-334-270 受付時間11:00～20:00・日祝10:30～18:00/水曜休診"></dt>
            <dd><a href="#section15"><img src="img/linkarea_btn.png" width="299" height="59" alt="無料カウンセリングのご予約"></a></dd>
        </dl>
        <dl class="cf pc_none">
        	<dt>

<a href="tel:0120334270" onclick="javascript:goog_report_conversion('tel:0120-334-270');yahoo_report_conversion(undefined);ga('send','event','sp','tel-tap','tel');return false;"><img src="images/tk_cv_sp1.gif" width="100%" alt="0120-334-270"></a>

<a href="tel:0120931911" onclick="javascript:goog_report_conversion('tel:0120-931-911');yahoo_report_conversion(undefined);ga('send','event','sp','tel-tap','tel');return false;"><img src="images/tk_cv_sp2.gif" width="100%" alt="0120-334-270"></a>

</dt>
            <dd><a href="#section15"><img src="img/sp_linkarea_btn.png" width="100%" alt="無料カウンセリングのご予約"></a></dd>
        </dl>
    </div>
</div>
<div id="section05" class="box">
	<article class="block07">
        <h2 class="sp_none"><img src="img/block07_point04.png" width="114" height="91" alt="Method4">BNLS注射：脂肪を溶かしてスッキリした小顔に</h2>
        <h2 class="pc_none"><img src="img/sp_block07_title01.png" width="100%" alt="Method4 BNLS注射：脂肪を溶かしてスッキリした小顔に"></h2>
        <div class="block07_in">
            <div class="sp_area">
                <div class="con_top cf">
                    <div class="left_area box">
                        <dl>
                            <dt><p>部分痩せを実現するメソセラピー</p></dt>
                            <dd>
                                <table>
                                    <tr>
                                        <td>
                                        フランスのミッシェル・ピストール医師によって開発された美容治療法、メソセラピー。
                                        脂肪細胞を分解・溶解する薬剤を注射する新しい美容治療方法です。<br>
                                        1回の施術自体にかかる時間はわずか5～10分ほどで、
                                        気になる部分のみを狙った効果を得ることができます。<br>
                                        溶けた脂肪細胞はそのまま体外に排出されます。
                                        まるごと脂肪細胞を排出できるので、リバウンドの心配もありません。
                                        </td>
                                        <th>
                                        <img src="img/block07_img03.png" width="251" height="173" alt="img">
                                        </th>
                                    </tr>
                                </table>
                            </dd>
                        </dl>
                    </div>
                    <div class="right_area box cf">
                        <img class="sp_none" src="img/block07_img01.png" width="532" height="272" alt="before after">
                        <p class="text01 sp_none">部分的についてしまった脂肪で顔が大きく見える</p>
                        <p class="text02 sp_none">注射のみの簡単プチ小顔術で、部分的についてしまった脂肪をピンポイントで溶かし、スッキリした小顔に</p>
                        <div class="img_text cf pc_none">
                            <img class="pc_none" src="img/sp_block07_img01.png" width="100%" alt="before after">
                            <p class="text01">部分的についてしまった脂肪で顔が大きく見える</p>
                            <p class="text02">注射のみの簡単プチ小顔術で、部分的についてしまった脂肪をピンポイントで溶かし、スッキリした小顔に</p>
                        </div>
                    </div>
                </div>
                <div class="con_center">
                    <h3>施術内容</h3>
                    <div class="cf sp_none">
                        <p class="number no01">薬剤を注射<br>（主成分レシチン）</p>
                        <p class="number no02">薬剤が脂肪細胞を<br>分解・溶解</p>
                        <p class="number no03">溶解された脂肪細胞は<br>体外に排出</p>
                        <img src="img/block07_img02.png" width="977" height="205" alt="表">
                        <p>
                        レシチンは大豆にも含まれている成分です。<br>
                        また、使用する薬剤は、もともと高コレステロールに対応する薬剤として実績があり、
                        安心・安全にご利用いただけるものとなっております。
                        </p>
                    </div>
                    <div class="sp_text_area pc_none">
                        <img class="pc_none" src="img/sp_block07_img02.png" width="100%" alt="before after">
                        <p>
                        レシチンは大豆にも含まれている成分です。また、使用する薬剤は、
                        もともと高コレステロールに対応する薬剤として実績があり、
                        安心・安全にご利用いただけるものとなっております。
                        </p>
                    </div>
                </div>
                <div class="faq_area cf">
                    <h3>
                    <img class="sp_none" src="img/block07_title01.png" width="1200" height="49" alt="FAQ BNLS注射に関するよくある質問">
                    <img class="pc_none" src="img/sp_block07_title02.png" width="100%" alt="FAQ BNLS注射に関するよくある質問">
                    </h3>
                    <div class="left_area box cf">
                        <ul class="accordion_ul">
                            <li>
                                <p>腫れや赤み、痛みはどのぐらい出ますか？</p>
                                <ul style="display: none;">
                                    <li>
                                    多少の腫れや赤みは出ますが、３?４日程度で引いていきます。
                                    痛みについては、注射の当日から鈍痛や筋肉痛のような痛みが２?３日続くことがあります。
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <p>リバウンドはありませんか？</p>
                                <ul style="display: none;">
                                    <li>
                                    注入された薬剤は脂肪細胞の核に作用し、脂肪細胞の数自体を減少させるため、リバウンドの心配がありません。
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="right_area box cf">
                        <ul class="accordion_ul">
                            <li>
                                <p>溶け出した脂肪はどこに行くのですか？</p>
                                <ul style="display: none;">
                                    <li>
                                    体内で分解・溶解された脂肪組織は、血中から腸管を経て、尿や便として体外に排出されます。
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="con_bottom">
                    <h3>
                    <img class="sp_none" src="img/block07_title02.png" width="1200" height="49" alt="PRICE BNLS注射　施術料金">
                    <img class="pc_none" src="img/sp_block07_title03.png" width="100%" alt="PRICE BNLS注射　施術料金">
                    </h3>
                    <dl class="price_box cf">
                        <dt>小顔輪郭注射　1本</dt>
                        <dd>3,950円<span>（+税）</span>～</dd>
                    </dl>
                </div>
            </div>
        </div>
    </article>
	<div class="btn_box btn_box06 cf">
        <dl class="cf sp_none">
        	<dt><img src="images/tk_cv_pc1.gif" width="480" height="75" alt="0120-334-270 受付時間11:00～20:00・日祝10:30～18:00/水曜休診"></dt>
            <dd><a href="#section15"><img src="img/linkarea_btn.png" width="299" height="59" alt="無料カウンセリングのご予約"></a></dd>
        </dl>
        <dl class="cf pc_none">
        	<dt>

<a href="tel:0120334270" onclick="javascript:goog_report_conversion('tel:0120-334-270');yahoo_report_conversion(undefined);ga('send','event','sp','tel-tap','tel');return false;"><img src="images/tk_cv_sp1.gif" width="100%" alt="0120-334-270"></a>

<a href="tel:0120931911" onclick="javascript:goog_report_conversion('tel:0120-931-911');yahoo_report_conversion(undefined);ga('send','event','sp','tel-tap','tel');return false;"><img src="images/tk_cv_sp2.gif" width="100%" alt="0120-334-270"></a>


</dt>
            <dd><a href="#section15"><img src="img/sp_linkarea_btn.png" width="100%" alt="無料カウンセリングのご予約"></a></dd>
        </dl>
    </div>
</div>
<div id="section06" class="box">
	<article class="block08">
        <div class="block08_in">
            <h2>
            <img class="sp_none" src="img/block08_title01.png" width="581" height="57" alt="お客様の声">
            <img class="pc_none" src="img/sp_block08_title01.png" width="100%" alt="お客様の声">
            </h2>
            <div class="huki huki01 cf sp_none">
                <img src="img/block08_img01.png" width="86" height="129" alt="女性01">
                <p>
                美容治療は初めてでとても緊張していたのですが、スタッフの皆さんが温かい雰囲気で迎えてくれて、リラックスできました。
                こちらの質問にもわかりやすく答えてくれたので、不安や疑問など相談しやすかったです。アフターケアも細やかでした。
                他のクリニックのことはわかりませんが、今回は大満足といえると思います。
                </p>
            </div>
            <div class="huki huki02 cf sp_none">
                <p>
結婚式前に、どうしてもキレイになりたくて受診しました。院内はとても明るく清潔な印象で 「衛生管理がちゃんとできてそうな病院だな」と感じました。 施術も思っていたより痛みもなく、あっという間に終わって驚きました。 また何かあるときはよろしくお願いします！ </p>
                <img src="img/block08_img02.png" width="83" height="130" alt="女性02">
            </div>
            <div class="huki huki03 cf sp_none">
                <img src="img/block08_img03.png" width="79" height="128" alt="女性03">
                <p>
何もかも初めてでよくわからず不安でしたが、とても丁寧に相談にのっていただきました。 長いカウンセリングになってしまいましたが、笑顔で応対していただき、納得して施術方法を選ぶことができました。 信頼できるドクターと明るく出迎えてくれるスタッフの皆さんに感謝です！
                </p>
            </div>
            <div class="sp_area">
                <table class="pc_none tbl01">
                    <tr>
                        <th><img class="pc_none" src="img/sp_block08_img01.png" width="100%" alt="女性01"></th>
                        <td>
                        美容治療は初めてでとても緊張していたのですが、スタッフの皆さんが温かい雰囲気で迎えてくれて、リラックスできました。
                        こちらの質問にもわかりやすく答えてくれたので、不安や疑問など相談しやすかったです。アフターケアも細やかでした。
                        他のクリニックのことはわかりませんが、今回は大満足といえると思います。
                        </td>
                    </tr>
                </table>
                <table class="pc_none tbl02">
                    <tr>
                        <td>
結婚式前に、どうしてもキレイになりたくて受診しました。院内はとても明るく清潔な印象で 「衛生管理がちゃんとできてそうな病院だな」と感じました。 施術も思っていたより痛みもなく、あっという間に終わって驚きました。 また何かあるときはよろしくお願いします！</td>
                        <th><img class="pc_none" src="img/sp_block08_img02.png" width="100%" alt="女性02"></th>
                    </tr>
                </table>
                <table class="pc_none tbl03">
                    <tr>
                        <th><img class="pc_none" src="img/sp_block08_img03.png" width="100%" alt="女性03"></th>
                        <td>
何もかも初めてでよくわからず不安でしたが、とても丁寧に相談にのっていただきました。 長いカウンセリングになってしまいましたが、笑顔で応対していただき、納得して施術方法を選ぶことができました。 信頼できるドクターと明るく出迎えてくれるスタッフの皆さんに感謝です！</td>
                    </tr>
                </table>
            </div>
            <div class="btn_box cf">
                <dl class="cf sp_none">
                    <dt><img src="images/tk_cv_pc1.gif" width="480" height="75" alt="0120-334-270 受付時間11:00～20:00・日祝10:30～18:00/水曜休診"></dt>
                    <dd><a href="#section15"><img src="img/linkarea_btn.png" width="299" height="59" alt="無料カウンセリングのご予約"></a></dd>
                </dl>
                <dl class="cf pc_none">
                    <dt>

<a href="tel:0120334270" onclick="javascript:goog_report_conversion('tel:0120-334-270');yahoo_report_conversion(undefined);ga('send','event','sp','tel-tap','tel');return false;"><img src="images/tk_cv_sp1.gif" width="100%" alt="0120-334-270"></a>

<a href="tel:0120931911" onclick="javascript:goog_report_conversion('tel:0120-931-911');yahoo_report_conversion(undefined);ga('send','event','sp','tel-tap','tel');return false;"><img src="images/tk_cv_sp2.gif" width="100%" alt="0120-334-270"></a>
</dt>
                    <dd><a href="#section15"><img src="img/sp_linkarea_btn.png" width="100%" alt="無料カウンセリングのご予約"></a></dd>
                </dl>
            </div>
        </div>
    </article>
</div>
<div id="section07" class="box">
	<article class="block09">
        <div class="block09_in">
            <h2>
            <img class="sp_none" src="img/block09_title01.png" width="902" height="58" alt="お問い合わせから施術までの流れ">
            <img class="pc_none" src="img/sp_block09_title01.png" width="100%" alt="お問い合わせから施術までの流れ">
            </h2>
            <div class="sp_area">
                <p class="top_text">
                電話、もしくはメールで<br class="pc_none">ご予約を承っております。<br class="pc_none">施術のご予約の前に、診療や施術、<br>
                その他について疑問がありましたら、<br class="pc_none">お電話またはお問い合わせフォーム、<br class="pc_none">メールからお気軽にお問い合わせください。<br>
                その際には匿名でのお問合せもOKです。
                </p>
                <div class="sp_none">
                    <div class="flow_area">
                        <div class="flow flow01">
                            <dl>
                                <dt>お電話、メールにてお問い合わせ</dt>
                                <dd>
                                電話、もしくはメールでご予約を承っております。<br>
                                施術のご予約の前に、診療や施術、その他について疑問がありましたら、お電話またはお問い合わせフォーム、
                                メールからお気軽にお問い合わせください。その際には匿名でのお問合せもOKです。
                                </dd>
                            </dl>
                            <div class="link cf">
                                <dl>
                                    <dt>お電話での<br>ご相談 </dt>
                                    <dd><img src="images/tk_cv_pc3.gif" width="285" height="69" alt="0120-334-270 受付時間11:00～20:00・日祝10:30～18:00/水曜休診"></dd>
                                </dl>
                                <a href="#section15"><img src="img/block09_linkbtn.png" width="301" height="49" alt="無料カウンセリングのご予約"></a>
                            </div>
                        </div>
                    </div>
                    <div class="flow_area">
                        <div class="flow flow02">
                            <dl>
                                <dt>ご来院・受付</dt>
                                <dd>
                                お電話でご予約いただいた日程でご来院ください。（日程の変更などはお気軽にご連絡ください）<br>
                                ご予約された日時にお越し下さい。<br>
                                当クリニックでのカウンセリング方法など事前に何でも相談ください。
                                </dd>
                            </dl>
                        </div>
                    </div>
                    <div class="flow_area">
                        <div class="flow flow03">
                            <dl>
                                <dt>スタッフとのカウンセリング</dt>
                                <dd>
                                患者様の悩みや疑問、施術費用やお支払方法ついてなど、専門のスタッフにお気軽に相談ください。<br>
                                施術へのお手続き・施術申込書の記入、術後の注意事項をお渡しします。<br>
                                未成年の方は同意書が必要になります。又はご両親のどちらかご同伴でお願いします。<br>
                                ※認印（シャチハタ以外）をお持ち下さい。
                                </dd>
                            </dl>
                        </div>
                    </div>
                    <div class="flow_area">
                        <div class="flow flow04">
                            <dl>
                                <dt>担当医師とのカウンセリング・診察</dt>
                                <dd>
                                実際に施術を行う担当医師が丁寧にご要望をうかがった上で、プラン、施術方法、施術内容のご説明をいたします。<br>
                                ※施術内容の詳細は担当医師がご説明いたします。ご不明な点があればお気軽にご相談ください。
                                </dd>
                            </dl>
                        </div>
                    </div>
                    <div class="flow_area">
                        <div class="flow flow05">
                            <dl>
                                <dt>施術を行います</dt>
                                <dd>
                                お顔の施術の場合は、事前にメイクを落としていただく必要がございます。<br>
                                化粧落としなどご用意しておりますので、お気軽にスタッフにお声掛けください。<br>
                                準備ができましたら施術室に移動していただき、施術を行います。
                                リラックスして施術を受けてください。
                                </dd>
                            </dl>
                        </div>
                    </div>
                    <div class="flow_area">
                        <div class="flow flow06">
                            <dl>
                                <dt>アフターケア</dt>
                                <dd>
                                施術後はすぐにご帰宅頂けます。<br>
                                専用のメイクルームをご用意いたしておりますので、メイク直しをしながら安心して新しいご自分と対面していただけます。
                                施術後のアフターケアの料金は手術料金に含まれております。<br>
                                また、施術後しばらく経ってご不安な点や不明瞭な点などがございましたら、お気軽に電話、またはメールにて直接ご相談ください。
                                </dd>
                            </dl>
                        </div>
                    </div>
                </div>
                <div class="pc_none tbl_area">
                    <p><img src="img/sp_block09_title02.png" width="100%" alt="お電話、メールにてお問い合わせ"></p>
                    <table>
                        <tr>
                            <th><img src="img/sp_block09_img01.png" width="100%" alt="お電話、メールにてお問い合わせ"></th>
                            <td>
                            電話、もしくはメールでご予約を承っております。<br>
                            施術のご予約の前に、診療や施術、その他について疑問がありましたら、お電話またはお問い合わせフォーム、
                            メールからお気軽にお問い合わせください。その際には匿名でのお問合せもOKです。
                            <dl class="link">
                                <dt class="cf">
                                <a href="tel:0120334270" onclick="javascript:goog_report_conversion('tel:0120-334-270');yahoo_report_conversion(undefined);ga('send','event','sp','tel-tap','tel');return false;"><img src="images/tk_cv_sp1.gif" width="100%" alt="0120-334-270 受付時間11:00～20:00・日祝10:30～18:00/水曜休診"></a>

<a href="tel:0120931911" onclick="javascript:goog_report_conversion('tel:0120-931-911');yahoo_report_conversion(undefined);ga('send','event','sp','tel-tap','tel');return false;"><img src="images/tk_cv_sp2.gif" width="100%" alt="0120-334-270 受付時間11:00～20:00・日祝10:30～18:00/水曜休診"></a>

                                </dt>
                                <dd><a href="#section15"><img src="img/sp_block09_link.png" width="100%" alt="無料カウンセリングのご予約"></a></dd>
                            </dl>
                            </td>
                        </tr>
                    </table>
                    <p><img src="img/sp_block09_title03.png" width="100%" alt="ご来院・受付"></p>
                    <table>
                        <tr>
                            <th><img src="img/sp_block09_img02.png" width="100%" alt="ご来院・受付"></th>
                            <td>
                            お電話でご予約いただいた日程でご来院ください。（日程の変更などはお気軽にご連絡ください）<br>
                            ご予約された日時にお越し下さい。<br>
                            当クリニックでのカウンセリング方法など事前に何でも相談ください。
                            </td>
                        </tr>
                    </table>
                    <p><img src="img/sp_block09_title04.png" width="100%" alt="スタッフとのカウンセリング"></p>
                    <table>
                        <tr>
                            <th><img src="img/sp_block09_img03.png" width="100%" alt="スタッフとのカウンセリング"></th>
                            <td>
                            患者様の悩みや疑問、施術費用やお支払方法ついてなど、専門のスタッフにお気軽に相談ください。<br>
                            施術へのお手続き・施術申込書の記入、術後の注意事項をお渡しします。<br>
                            未成年の方は同意書が必要になります。又はご両親のどちらかご同伴でお願いします。<br>
                            ※認印（シャチハタ以外）をお持ち下さい。
                            </td>
                        </tr>
                    </table>
                    <p><img src="img/sp_block09_title05.png" width="100%" alt="担当医師とのカウンセリング・診察"></p>
                    <table>
                        <tr>
                            <th><img src="img/sp_block09_img04.png" width="100%" alt="担当医師とのカウンセリング・診察"></th>
                            <td>
                            実際に施術を行う担当医師が丁寧にご要望をうかがった上で、プラン、施術方法、施術内容のご説明をいたします。<br>
                            ※施術内容の詳細は担当医師がご説明いたします。ご不明な点があればお気軽にご相談ください。
                            </td>
                        </tr>
                    </table>
                    <p><img src="img/sp_block09_title06.png" width="100%" alt="施術を行います"></p>
                    <table>
                        <tr>
                            <th><img src="img/sp_block09_img05.png" width="100%" alt="施術を行います"></th>
                            <td>
                            お顔の施術の場合は、事前にメイクを落としていただく必要がございます。<br>
                            化粧落としなどご用意しておりますので、お気軽にスタッフにお声掛けください。<br>
                            準備ができましたら施術室に移動していただき、施術を行います。
                            リラックスして施術を受けてください。
                            </td>
                        </tr>
                    </table>
                    <p><img src="img/sp_block09_title07.png" width="100%" alt="アフターケア"></p>
                    <table>
                        <tr>
                            <th><img src="img/sp_block09_img06.png" width="100%" alt="アフターケア"></th>
                            <td>
                            施術後はすぐにご帰宅頂けます。<br>
                            専用のメイクルームをご用意いたしておりますので、メイク直しをしながら安心して新しいご自分と対面していただけます。
                            施術後のアフターケアの料金は手術料金に含まれております。<br>
                            また、施術後しばらく経ってご不安な点や不明瞭な点などがございましたら、お気軽に電話、またはメールにて直接ご相談ください。
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </article>
    <div class="btn_box btn_box07 cf">
        <dl class="cf sp_none">
            <dt><img src="images/tk_cv_pc1.gif" width="480" height="75" alt="0120-334-270 受付時間11:00～20:00・日祝10:30～18:00/水曜休診"></dt>
            <dd><a href="#section15"><img src="img/linkarea_btn.png" width="299" height="59" alt="無料カウンセリングのご予約"></a></dd>
        </dl>
        <dl class="cf pc_none">
            <dt>

<a href="tel:0120334270" onclick="javascript:goog_report_conversion('tel:0120-334-270');yahoo_report_conversion(undefined);ga('send','event','sp','tel-tap','tel');return false;"><img src="images/tk_cv_sp1.gif" width="100%" alt="0120-334-270"></a>

<a href="tel:0120931911" onclick="javascript:goog_report_conversion('tel:0120-931-911');yahoo_report_conversion(undefined);ga('send','event','sp','tel-tap','tel');return false;"><img src="images/tk_cv_sp2.gif" width="100%" alt="0120-334-270"></a>

</dt>
            <dd><a href="#section15"><img src="img/sp_linkarea_btn.png" width="100%" alt="無料カウンセリングのご予約"></a></dd>
        </dl>
    </div>
</div>



<!--
<div id="section08" class="box">
	<article class="block10">
        <div class="block10_in">
            <h2>
            <img class="sp_none" src="img/block10_title01.png" width="574" height="58" alt="院内ご案内">
            <img class="pc_none" src="img/sp_block10_title01.png" width="100%" alt="院内ご案内">
            </h2>
            <div class="sp_area">
                <p class="top_text">
                当院では安心してご利用頂ける万全の環境を<br class="pc_none">整えておりますので、安心してお越しください。<br>
                皆さまのご来院をお待ちしております。
                </p>
                <div class="slider_area">
                    <div class="sp_none">
                        <ul class="bxslider cf">
                            <li><a href="img/slider01_big.jpg" rel="lightbox"><img src="img/block10_slider01.png" width="400" height="400" alt="施術室"></a></li>
                            <li><a href="img/slider02_big.jpg" rel="lightbox"><img src="img/block10_slider02.png" width="400" height="400" alt="受付"></a></li>
                            <li><a href="img/slider03_big.jpg" rel="lightbox"><img src="img/block10_slider03.png" width="400" height="400" alt="待合室"></a></li>
                            <li><a href="img/slider04_big.jpg" rel="lightbox"><img src="img/block10_slider04.png" width="400" height="400" alt="カウンセリングルーム"></a></li>
                            <li><a href="img/slider05_big.jpg" rel="lightbox"><img src="img/block10_slider05.png" width="400" height="400" alt="診察室"></a></li>
                            <li><a href="img/slider06_big.jpg" rel="lightbox"><img src="img/block10_slider06.png" width="400" height="400" alt="診察室"></a></li>
                        </ul>
                    </div>
                    <div class="pc_none">
                        <ul class="bxslider cf">
                            <li><img src="img/slider01_sp.jpg" width="100%" alt="施術室"></li>
                            <li><img src="img/slider02_sp.jpg" width="100%" alt="受付"></li>
                            <li><img src="img/slider03_sp.jpg" width="100%" alt="待合室"></li>
                            <li><img src="img/slider04_sp.jpg" width="100%" alt="カウンセリングルーム"></li>
                            <li><img src="img/slider05_sp.jpg" width="100%" alt="診察室"></li>
                            <li><img src="img/slider06_sp.jpg" width="100%" alt="診察室"></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="btn_box cf">
                <dl class="cf sp_none">
                    <dt><img src="images/tk_cv_pc1.gif" width="480" height="75" alt="0120-334-270 受付時間11:00～20:00・日祝10:30～18:00/水曜休診"></dt>
                    <dd><a href="#section15"><img src="img/linkarea_btn.png" width="299" height="59" alt="無料カウンセリングのご予約"></a></dd>
                </dl>
                <dl class="cf pc_none">
                    <dt>

<a href="tel:0120334270" onclick="javascript:goog_report_conversion('tel:0120-334-270');yahoo_report_conversion(undefined);ga('send','event','sp','tel-tap','tel');return false;"><img src="images/tk_cv_sp1.gif" width="100%" alt="0120-334-270"></a>

<a href="tel:0120931911" onclick="javascript:goog_report_conversion('tel:0120-931-911');yahoo_report_conversion(undefined);ga('send','event','sp','tel-tap','tel');return false;"><img src="images/tk_cv_sp2.gif" width="100%" alt="0120-334-270"></a>

</dt>
                    <dd><a href="#section15"><img src="img/sp_linkarea_btn.png" width="100%" alt="無料カウンセリングのご予約"></a></dd>
                </dl>
            </div>
        </div>
    </article>
</div>
-->


<div id="section09" class="box">
	<article class="block11">
        <div class="block11_in">
            <h2>
            <img class="sp_none" src="img/block11_title01.png" width="926" height="62" alt="表参道スキンクリニックの理念">
            <img class="pc_none" src="img/sp_block11_title01.png" width="100%" alt="表参道スキンクリニックの理念">
            </h2>
            <p class="top_text">
            開業当初から当クリニックでは痛みや不安のない<br class="pc_none">施術を心がけており、<br>
            患者様に安心して美しくなって頂くことを<br class="pc_none">目指しております。
            </p>
            <div class="sp_area">
                <div class="rinen_area cf sp_none" style="width:auto;margin:0 0 50px 0;">
                    <dl class="rinen01">
                        <dt>患者様とのコミュニケーションを大切にしています。</dt>
                        <dd>
                            <img src="img/block11_img01.png" width="373" height="150" alt="Concept1イメージ">
                            当クリニックは患者様の立場に立ち、今抱えている不安や悩みなどを解消するため、
                            丁寧なカウンセリングを行なっています。
                        </dd>
                    </dl>
                    <dl class="rinen02">
                        <dt>長年の経験と実績による<br>確かな技術と高い信頼</dt>
                        <dd>
                            <img src="img/block11_img02.png" width="373" height="150" alt="Concept2イメージ">
                            当クリニックは優秀な美容皮膚科医が多数在籍しています。
                            本当に安心して任せられるドクターがレベルの高い治療を行ないます。
                        </dd>
                    </dl>
                    <dl class="rinen03">
                        <dt>いつもドクターが<br>そばにいてくれるという感覚</dt>
                        <dd>
                            <img src="img/block11_img03.png" width="373" height="150" alt="Concept3イメージ">
                            当クリニックは気軽に相談できる環境作りを心がけています。
                            術後のケア、美肌づくりのアドバイスなどいつでもすぐにお応えいたします。
                        </dd>
                    </dl>
                </div>
                <div class="rinen_area cf pc_none">
                    <dl class="rinen01">
                        <dt><img src="img/sp_block11_title02.png" width="100%" alt="患者様とのコミュニケーションを大切にしています。"></dt>
                        <dd>
                            <img src="img/sp_block11_img01.png" width="100%" alt="Concept1イメージ">
                            当クリニックは患者様の立場に立ち、今抱えている不安や悩みなどを解消するため、
                            丁寧なカウンセリングを行なっています。
                        </dd>
                    </dl>
                    <dl class="rinen02">
                        <dt><img src="img/sp_block11_title03.png" width="100%" alt="長年の経験と実績による<br>確かな技術と高い信頼"></dt>
                        <dd>
                            <img src="img/sp_block11_img02.png" width="100%" alt="Concept2イメージ">
                            当クリニックは優秀な美容皮膚科医が多数在籍しています。
                            本当に安心して任せられるドクターがレベルの高い治療を行ないます。
                        </dd>
                    </dl>
                    <dl class="rinen03">
                        <dt><img src="img/sp_block11_title04.png" width="100%" alt="いつもドクターが<br>そばにいてくれるという感覚"></dt>
                        <dd>
                            <img src="img/sp_block11_img03.png" width="100%" alt="Concept3イメージ">
                            当クリニックは気軽に相談できる環境作りを心がけています。
                            術後のケア、美肌づくりのアドバイスなどいつでもすぐにお応えいたします。
                        </dd>
                    </dl>
                </div>
            </div>
        </div>
    </article>
</div>

<!--
<div id="section10" class="box">
	<article class="block12">
        <div class="block12_in">
            <h2>
            <img class="sp_none" src="img/block12_title01.png" width="628" height="59" alt="医師のご紹介">
            <img class="pc_none" src="img/sp_block12_title01.png" width="100%" alt="医師のご紹介">
            </h2>
            <div class="sp_area">
                <p class="top_text">
                当クリニックの医師たちは、<br class="pc_none">お客様に安全に美しくなって頂く為に、<br>
                日々最高水準の技術を日々研究しております。
                </p>
                <div class="docter_area">
                    <div class="docter docter01 cf">
						<h3 class="photo"><img src="img/pic08.jpg" alt="院長 松木貴裕" width="154" height="203"></h3>
                        <div class="short cf">
                            <dl class="left_box">
                                <dt class="title">経歴</dt>
                                <dd>
									<dl class="cf">
										<dt>平成10年3月</dt>
										<dd>藤田保健衛生大学 医学部医学研究所科 卒業</dd>
										<dt>平成11年9月</dt>
										<dd>医療法人大医会 日進おりど病院 勤務</dd>
										<dt>平成14年3月</dt>
										<dd>医療法人大医会 日進おりど病院 退職</dd>
										<dt>平成14年4月</dt>
										<dd>東京女子医科大学病院 皮膚科 勤務</dd>
										<dt>平成16年3月</dt>
										<dd>東京女子医科大学病院 皮膚科 退職</dd>
										<dt>平成16年9月</dt>
										<dd>東京美容外科 開設 勤務</dd>
									</dl>
                                </dd>
                            </dl>
                            <dl class="right_box">
                                <dt class="title">所属学会など</dt>
                                <dd>
                                    <ul class="cf">
                                        <li>・日本皮膚科学会正会員</li>
                                        <li>・レーザー医学会認定医</li>
                                        <li>・日本抗加齢学会会員</li>
                                        <li>・美容外科学会会員</li>
                                    </ul>
                                </dd>
                            </dl>
                        </div>
                        <dl class="long">
                            <dt class="title">松木先生から一言</dt>
                            <dd>かつては、大がかりな美容整形手術など、ハイリスクなイメージがあった美容医療の世界。しかし近年では、手軽にトライできる治療も増え、とても身近な存在になりました。当クリニックでは、高い技術を持つ経験豊富な美容皮膚科医が、患者様の希望や理想を大切にしながら、最新の美容医療を提供いたします。</dd>
                        </dl>
                    </div>
                    <div class="docter docter02 cf">
						<h3 class="photo"><img src="img/pic09.jpg" alt="友利新" width="154" height="203"></h3>
                        <div class="short cf">
                            <dl class="left_box">
                                <dt class="title">経歴</dt>
                                <dd> 東京女子医科大学卒業。<br>
                                同大学病院の内科勤務を経て皮膚科へ転科。<br>
                                現在、都内2か所のクリニックに勤務の傍ら、医師という立場から美容と健康を医療として追求し、美しく生きる為の啓蒙活動を雑誌・TV などで展開中。<br>
                                2004年第36回準ミス日本という経歴をもつ、美貌の新進医師。<br>
                                美と健康に関する著書も多数あり、近著に『女性のキレイと健康をつくる 美肌タイミングジュース』（保健同人社より2011年6月10日発刊）がある。</dd>
                            </dl>
                            <dl class="right_box">
                                <dt class="title">所属学会など</dt>
                                <dd>
                                    <ul class="cf">
                                        <li>・日本内科学会会員</li>
                                        <li>・日本糖尿病学会会員</li>
                                        <li>・日本皮膚科学会会員</li>
                                        <li>・抗加齢学会会員</li>
                                    </ul>
                                </dd>
                            </dl>
                        </div>
                        <dl class="long">
                            <dt class="title">友利先生から一言</dt>
                            <dd>女性が綺麗になりたいという思いは変わらないものです。そんな願いを手軽に、そして確実に医療の技術で叶えたい。そう思いながら日々の診療をさせてもらっています。 一人でも多くの女性の笑顔に自信を持っていただけるように、皆さんの美の主治医としていろいろな悩みにお応えしていきます。</dd>
                        </dl>
                    </div>
                </div>
            </div>
        </div>
    </article>
    <div class="btn_box btn_box08 cf">
        <dl class="cf sp_none">
            <dt><img src="images/tk_cv_pc1.gif" width="480" height="75" alt="0120-334-270 受付時間11:00～20:00・日祝10:30～18:00/水曜休診"></dt>
            <dd><a href="#section15"><img src="img/linkarea_btn.png" width="299" height="59" alt="無料カウンセリングのご予約"></a></dd>
        </dl>
        <dl class="cf pc_none">
            <dt>

<a href="tel:0120334270" onclick="javascript:goog_report_conversion('tel:0120-334-270');yahoo_report_conversion(undefined);ga('send','event','sp','tel-tap','tel');return false;"><img src="images/tk_cv_sp1.gif" width="100%" alt="0120-334-270"></a>

<a href="tel:0120931911" onclick="javascript:goog_report_conversion('tel:0120-931-911');yahoo_report_conversion(undefined);ga('send','event','sp','tel-tap','tel');return false;"><img src="images/tk_cv_sp2.gif" width="100%" alt="0120-334-270"></a>

</dt>
            <dd><a href="#section15"><img src="img/sp_linkarea_btn.png" width="100%" alt="無料カウンセリングのご予約"></a></dd>
        </dl>
    </div>
</div>
<div id="section11" class="box">
	<article class="block13">
        <div class="block13_in">
            <h2>
            <img class="sp_none" src="img/block13_title01.png" width="534" height="56" alt="アクセス">
            <img class="pc_none" src="img/sp_block13_title01.png" width="100%" alt="アクセス">
            </h2>

			<p class="tc mb30">〒150-0001　東京都渋谷区神宮前5丁目95−9−13　<br class="pc_none">喜多重ビル 4F　TEL：0120-334-270</p>

            <div class="sp_area">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3241.39689876405!2d139.707413!3d35.667228!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x60188ca3e16206fd%3A0x92146d294d00108b!2z6KGo5Y-C6YGT44K544Kt44Oz44Kv44Oq44OL44OD44Kv!5e0!3m2!1sja!2sjp!4v1433309200061" width="1200" height="350" frameborder="0" style="border:0"></iframe>
                <div class="access_area cf">
                    <dl class="access01">
                        <dt class="sp_none">電車でお越しの方</dt>
                        <dt class="pc_none"><img class="pc_none" src="img/sp_block13_title02.png" width="100%" alt="電車でお越しの方"></dt>
                        <dd>
                            <ul>
                                <li>・JR山手線「原宿駅」表参道口より徒歩約5分</li>
                                <li>・東京メトロ各線「表参道駅」A1出口より徒歩約3分</li>
                                <li>・東京メトロ各線「明治神宮前駅」4番出口より徒歩約3分</li>
                            </ul>
                        </dd>
                    </dl>
                    <dl class="access02">
                        <dt class="sp_none">お車でお越しの方</dt>
                        <dt class="pc_none"><img class="pc_none" src="img/sp_block13_title03.png" width="100%" alt="お車でお越しの方"></dt>
                        <dd>当クリニック専用の駐車はございません。<br>
                        お近くの有料駐車場のご利用をお願いいたします。</dd>
                    </dl>
                    <dl class="access03">
                        <dt class="sp_none">電車でお越しの方</dt>
                        <dt class="pc_none"><img class="pc_none" src="img/sp_block13_title04.png" width="100%" alt="電車でお越しの方"></dt>
                        <dd>
                            <p><span>診療時間</span><br>
                            平日 AM11:00～PM8:00(完全予約制)<br>
                            日祝 AM10:30～PM6:00(完全予約制)</p>
                            <p><span>休診日</span><br>水曜日</p>
                        </dd>
                    </dl>
                </div>
            </div>
        </div>
    </article>
    <div class="btn_box btn_box09 cf">
        <dl class="cf sp_none">
            <dt><img src="images/tk_cv_pc1.gif" width="480" height="75" alt="0120-334-270 受付時間11:00～20:00・日祝10:30～18:00/水曜休診"></dt>
            <dd><a href="#section15"><img src="img/linkarea_btn.png" width="299" height="59" alt="無料カウンセリングのご予約"></a></dd>
        </dl>
        <dl class="cf pc_none">
            <dt>

<a href="tel:0120334270" onclick="javascript:goog_report_conversion('tel:0120-334-270');yahoo_report_conversion(undefined);ga('send','event','sp','tel-tap','tel');return false;"><img src="images/tk_cv_sp1.gif" width="100%" alt="0120-334-270"></a>

<a href="tel:0120931911" onclick="javascript:goog_report_conversion('tel:0120-931-911');yahoo_report_conversion(undefined);ga('send','event','sp','tel-tap','tel');return false;"><img src="images/tk_cv_sp2.gif" width="100%" alt="0120-334-270"></a>

</dt>
            <dd><a href="#section15"><img src="img/sp_linkarea_btn.png" width="100%" alt="無料カウンセリングのご予約"></a></dd>
        </dl>
    </div>
</div>

-->

<?php include '../tabs/index.html'; ?>


<!-----------------------------------------------------------------------------------------------------------------------------------------------------------
　コンテンツEND
----------------------------------------------------------------------------------------------------------------------------------------------------------->


<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/smoothness/jquery-ui.css" >
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1/i18n/jquery.ui.datepicker-ja.min.js"></script>
<script>
$(function() {
	$("#datepicker_1").datepicker({
		dateFormat: 'yy年mm月dd日 (DD)',
		dayNames: ['日', '月', '火', '水', '木', '金', '土']
	});
	$("#datepicker_2").datepicker({
		dateFormat: 'yy年mm月dd日 (DD)',
		dayNames: ['日', '月', '火', '水', '木', '金', '土']
	});
});
</script>

<div class="form">
    <div id="section15" class="box">
        <article class="block14">
            <div class="block14_in">
                <h2>
                <img class="sp_none" src="img/block14_title01.png" width="937" height="59" alt="無料カウンセリングご予約フォーム">
                <img class="pc_none" src="img/sp_block14_title01.png" width="100%" alt="無料カウンセリングご予約フォーム">
                </h2>
                    <div class="sp_area">
                    <img class="flow_img sp_none" src="img/block14_img01.png" width="1000" height="63" alt="1.情報の入力 2.内容の確認 3.送信完了">
                    <img class="flow_img pc_none" src="img/sp_block14_img01.png" width="100%" alt="1.情報の入力 2.内容の確認 3.送信完了">
                    <p class="caution">※お手数ですが、必須項目をすべてご入力の上、確認画面へお進みください。</p>
                    <form method="post">
                      <table>
                        <tr>
                          <th class="hissu">お名前（ひらがな）</th>
                          <td>
                <input type="text" name="name_req" value="<?php echo $formTool->h($_POST['name_req']); ?>" placeholder="例）おもてさんどうはなこ" />
                            <?php if( !empty($formTool->messages['name_req']) ) : ?>
                              <p class="error"><?php echo $formTool->h($formTool->messages['name_req']); ?></p>
                            <?php endif; ?>
                          </td>
                        </tr>
                        <tr>
                          <th class="hissu">電話番号</th>
                          <td>
                            <input type="tel" name="tel_req" value="<?php echo $formTool->h($_POST['tel_req']); ?>" placeholder="例）01-2345-6789">
                            <?php if( !empty($formTool->messages['tel_req']) ) : ?>
                              <p class="error"><?php echo $formTool->h($formTool->messages['tel_req']); ?></p>
                            <?php endif; ?>
                          </td>
                        </tr>
                        <tr>
                          <th class="hissu">メールアドレス</th>
                          <td>
                            <input type="email" name="email_req" value="<?php echo $formTool->h($_POST['email_req']); ?>" placeholder="例）info@abc.com">
                            <?php if( !empty($formTool->messages['email_req']) ) : ?>
                              <p class="error"><?php echo $formTool->h($formTool->messages['email_req']); ?></p>
                            <?php endif; ?>
                          </td>
                        </tr>
                        <tr>
                          <th class="hissu">年齢</th>
                          <td>
                            <span><input type="text" name="age_req" value="<?php echo $formTool->h($_POST['age_req']); ?>" class="number" /> 歳</span>　例）30歳
                            <?php if( !empty($formTool->messages['age_req']) ) : ?>
                              <p class="error"><?php echo $formTool->h($formTool->messages['age_req']); ?></p>
                            <?php endif; ?>
                          </td>
                        </tr>
                        <tr>
                          <th class="hissu">ご希望のクリニック</th>
                          <td>
                            <?php echo $formTool->makeRadioHtml($formTool->hopeArr, 'array', $_POST['hope_req'], 'hope_req'); ?>
                            <?php if( !empty($formTool->messages['hope_req']) ) : ?>
                              <p class="error"><?php echo $formTool->h($formTool->messages['hope_req']); ?></p>
                            <?php endif; ?>
                          </td>
                        </tr>
                        <tr>
                          <th class="nini">相談日 第一希望</th>
                          <td>
                            <input type="text" name="day1" value="<?php echo $formTool->h($_POST['day1']); ?>" placeholder="YYYY年MM月DD日" class="calendar" id="datepicker_1" readonly/>
                            <select name="day1_period">
                              <option value="">時間帯を選択</option>
                              <?php echo $formTool->makeOptionHtml($formTool->timeArr, 'array', $_POST['day1_period']); ?>
                            </select>
                          </td>
                        </tr>
                        <tr>
                          <th class="nini">相談日 第二希望</th>
                          <td>
                            <input type="text" name="day2" value="<?php echo $formTool->h($_POST['day2']); ?>" placeholder="YYYY年MM月DD日" class="calendar" id="datepicker_2" readonly/>
                            <select name="day2_period">
                              <option value="">時間帯を選択</option>
                              <?php echo $formTool->makeOptionHtml($formTool->timeArr, 'array', $_POST['day2_period']); ?>
                            </select>
                          </td>
                        </tr>
                        <tr>
                          <th class="nini">連絡方法のご希望</th>
                          <td class="radiostyle">
                            <?php echo $formTool->makeCheckBoxHtml($formTool->typeArr, 'array', $_POST['type'], 'type'); ?>
                          </td>
                        </tr>
                        <tr>
                          <th class="nini last">お問い合わせ内容</th>
                          <td class="last">
                            <textarea name="message" placeholder="お問い合わせ内容をご記入ください。" cols="20" rows="5"><?php echo $formTool->h($_POST['message']); ?></textarea>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                    <input type="hidden" name="mode" value="conf">
                    <p class="btn sp_none"><input type="image" src="img/block14_link.png" width="361" height="70" alt="確認画面へ"></p>
                    <p class="btn pc_none"><input type="image" src="img/sp_block14_link.png" width="100%" alt="確認画面へ"></p>
                    </form>
                </div>
            </div>
        </article>
    </div>
</div>

<article class="block15">
    <p>表参道スキンクリニックは、<br>どこまでも綺麗への想いに<br class="pc_none">お応えし続けます</p>
</article>

<p id="page-top"><a href="#wrapper"><img src="img/page_top.png" width="44" height="44" alt="TOP"></a></p>

<script>
$('.accordion_ul ul').hide();
    $('.accordion_ul p').click(function(e){
    $(this).toggleClass("active");
    $(this).next("ul").slideToggle();
});
</script>

</div>