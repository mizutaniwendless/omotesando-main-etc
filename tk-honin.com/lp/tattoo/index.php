<?php
require('formClass.php');
$formTool = new formClass();
$mode = (empty($_POST['mode'])) ? "" : $_POST['mode'] ;
$error = false;
$mode2 = '';
if($mode == 'comp') {
  $error = $formTool->is_inputerror();
  //完了画面のリロードで再送させないようにリダイレクト
  if(!$error){
    $formTool->send_return_mail();
    $formTool->send_admin_mail();
    $mode2 = true;
  }
} else if ($mode == 'conf') {
  $error = $formTool->is_inputerror();
}
if($mode2 == true) {
  include('index_end.php');
} else {
?>
<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="UTF-8">
  <title>表参道スキンクリニック | 傷跡を残さずに綺麗にタトゥーを除去「美しい肌」に。</title>
  <meta name="keywords" content="表参道スキンクリニック,タトゥー除去,刺青,レーザー治療,しみ・しわ" />
  <meta name="description" content="表参道スキンクリニックでは、最新のレーザー治療で傷跡を残さずにタトゥーやシミ、しわを綺麗に除去します。" />
  <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0">

  <link rel="stylesheet" type="text/css" href="./css/common.css" media="screen,print">
  <link rel="stylesheet" type="text/css" href="./css/style.css" media="screen,print">
  <link rel="stylesheet" type="text/css" href="./css/style_sp.css" media="screen,print">
  <link rel="stylesheet" type="text/css" href="./css/jquery.bxslider.css" media="screen,print"/>
  <link rel="stylesheet" type="text/css" href="./css/lightbox.css" media="screen,print" />

  
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>

  <script type="text/javascript" src="./js/lightbox.js"></script>
  
  <script src="./js/jquery.page-scroller-309.js"></script>
  <script src="./js/rollover.js"></script>

  <script src="./js/jquery.bxslider.min.js"></script>


  <script>
  $(document).ready(function(){
    $('.bxslider').bxSlider({
      auto: true,
      pause: 5000,
      speed: 1000,
      pager: false,
      displaySlideQty: 2,
      moveSlideQty: 1,
    });
  });
  </script>
  <script>
  $(window).scroll(function () {
    $("#navi").stop().animate({ opacity: 1 },500, function() {
      $(this).delay(1000).animate({ opacity: 0 });
    });
  });
  </script>

  <!--[if (gte IE 6)&(lte IE 8)]>
  <script src="/js/selectivizr-min.js"></script>
  <![endif]-->
  <!-- HTML5 IEハック IE7までOK -->
  <!--[if lt IE 9]>
  <script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE9.js"></script>
  <script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/ie7-squish.js"></script>
  <![endif]-->


<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-59451323-1', 'auto');
  ga('send', 'pageview');

</script>

</head>
<body>

  <div id="wrapper">

 	<div id="h_wrapper">
      <header id="PAGE_TOP">
        <h1><img src="images/header_logo.png" width="249" height="45" alt="表参道スキンクリニック　Omotesando Skin Clinic"></h1>
        <p><img src="images/header_tel2.jpg" width="450" height="59" alt="0120-334-270 受付時間11:00～20:00・日祝10:30～18:00/水曜休診" class="sp_none"><a href="tel:0120334270" onclick="javascript:goog_report_conversion('tel:0120-334-270');yahoo_report_conversion(undefined);ga('send','event','sp','tel-tap','tel');return false;" class="pc_none"><!--<img src="images/header_tel_sp.png" width="100%" alt="0120-334-270 受付時間11:00～20:00・日祝10:30～18:00/水曜休診" style="z-index:9999;">--></a></p>
        <ul class="cf">
          <li class="sp_right"><a href="#section15"><img src="images/header_btn01_off.gif" width="249" height="40" alt="無料カウンセリングのご予約" class="sp_none"><img src="images/header_btn01_sp2.gif" width="100%" alt="無料カウンセリングのご予約" class="pc_none"></a></li>
          <li class="sp_ab"><a href="#access_box"><img src="images/header_btn02_off.gif" width="112" height="40" alt="アクセス" class="sp_none"><img src="images/header_btn02_sp.gif" width="100%" alt="アクセス" class="pc_none"></a></li>

<li class="sp_ab1"><a href="tel:0120334270"><img src="images/header_btn02_sp1.gif" width="100%" class="pc_none" alt="表参道院"></a></li>

<li class="sp_ab2"><a href="tel:0120931911"><img src="images/header_btn02_sp2.gif" width="100%" class="pc_none" alt="福岡院"></a></li>
        </ul>
      </header>
    </div>


    <?php if(basename($_SERVER['REQUEST_URI']) == 'comp.html') : ?>
      <?php include('comp.php'); ?>
    <?php elseif($_POST['mode'] == 'conf' && !$error) : ?>
      <?php include('conf.php'); ?>
    <?php else : ?>
      <?php include('edit.php'); ?>
      <?php if($error) : ?>
        <script type="text/javascript">
        window.location.hash = 'section15';
        </script>
      <?php endif; ?>
    <?php endif; ?>

    <div id="f_wrapper">
      <footer id="f_cnt">
        <p class="copy">Copyright &copy; Omotesando Skin Clinic. All Rights Reserved.</p>
      </footer>
    </div>


  </div>
  <script src="./js/custom.js"></script>

  <!-- YDN用リマーケティングタグ -->
  <script type="text/javascript" language="javascript">
  /* <![CDATA[ */
  var yahoo_retargeting_id = 'A15O0FYNPU';
  var yahoo_retargeting_label = '';
  /* ]]> */
  </script>
  <script type="text/javascript" language="javascript" src="//b92.yahoo.co.jp/js/s_retargeting.js"></script>

  <!-- Yahoo用電話CVタグ -->
  <!-- Yahoo Code for your Conversion Page
  In your html page, add the snippet and call
  yahoo_report_conversion when someone clicks on the
  phone number link or button. -->
  <script type="text/javascript">
  /* <![CDATA[ */
  yahoo_snippet_vars = function() {
    var w = window;
    w.yahoo_conversion_id = 1000199666;
    w.yahoo_conversion_label = "UemxCNzLnFsQiLG4zQM";
    w.yahoo_conversion_value = 0;
    w.yahoo_remarketing_only = false;
  }
  // IF YOU CHANGE THE CODE BELOW, THIS CONVERSION TAG MAY NOT WORK.
  yahoo_report_conversion = function(url) {
    yahoo_snippet_vars();
    window.yahoo_conversion_format = "3";
    window.yahoo_is_call = true;
    var opt = new Object();
    opt.onload_callback = function() {
      if (typeof(url) != 'undefined') {
        window.location = url;
      }
    }
    var conv_handler = window['yahoo_trackConversion'];
    if (typeof(conv_handler) == 'function') {
      conv_handler(opt);
    }
  }
  /* ]]> */
  </script>
  <script type="text/javascript"
  src="http://i.yimg.jp/images/listing/tool/cv/conversion_async.js">
  </script>

  <!-- Google用ALL電話計測タグ -->
  <!-- Google Code for &#38651;&#35441;CV Conversion Page
  In your html page, add the snippet and call
  goog_report_conversion when someone clicks on the
  phone number link or button. -->
  <script type="text/javascript">
  /* <![CDATA[ */
  goog_snippet_vars = function() {
    var w = window;
    w.google_conversion_id = 952358852;
    w.google_conversion_label = "JyGuCMvZpFsQxK-PxgM";
    w.google_remarketing_only = false;
  }
  // DO NOT CHANGE THE CODE BELOW.
  goog_report_conversion = function(url) {
    goog_snippet_vars();
    window.google_conversion_format = "3";
    window.google_is_call = true;
    var opt = new Object();
    opt.onload_callback = function() {
      if (typeof(url) != 'undefined') {
        window.location = url;
      }
    }
    var conv_handler = window['google_trackConversion'];
    if (typeof(conv_handler) == 'function') {
      conv_handler(opt);
    }
  }
  /* ]]> */
  </script>
  <script type="text/javascript"
  src="//www.googleadservices.com/pagead/conversion_async.js">
  </script>

</body>
</html>
<?php } ?>
