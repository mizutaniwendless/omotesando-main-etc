<?php
require('formClass.php');
$formTool = new formClass();
$mode = (empty($_POST['mode'])) ? "" : $_POST['mode'] ;
$error = false;
if($mode == 'comp') {
  $error = $formTool->is_inputerror();
  if(!$error){
    $formTool->send_return_mail();
    $formTool->send_admin_mail();
    header('Location: ./index_end.html');
  }
} else if ($mode == 'conf') {
  $error = $formTool->is_inputerror();
}
?>
<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="UTF-8">
<title>表参道スキンクリニック | 切らない痩身美容で部分痩せ</title>
  <meta name="keywords" content="表参道スキンクリニック,部分痩せ,刺切らない痩身,脂肪溶解注射,ＢＮＬＳ・ダイエット処方" />
  <meta name="description" content="表参道スキンクリニックでは、切らない痩身美容で部分やせを実現します。" />
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0">
<meta name="robots" content="noindex, nofollow">
  <link rel="stylesheet" type="text/css" href="./css/common.css" media="screen,print">
  <link rel="stylesheet" type="text/css" href="./css/style.css" media="screen,print">
  <link rel="stylesheet" type="text/css" href="./css/style_sp.css" media="screen,print">
  <link rel="stylesheet" type="text/css" href="./css/jquery.bxslider.css" media="screen,print"/>
  <link rel="stylesheet" type="text/css" href="./css/lightbox.css" media="screen,print" />

  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
  <script src="./js/smooth.pack.js"></script>
  <script src="./js/rollover.js"></script>
  <script src="./js/jquery.bxslider.min.js"></script>
  <script type="text/javascript" src="./js/lightbox.js"></script>
  <script>
  $(document).ready(function(){
    $('.bxslider').bxSlider({
      auto: true,
      pause: 5000,
      speed: 1000,
      pager: false,
      displaySlideQty: 2,
      moveSlideQty: 1,
    });
  });
  </script>
  <script>
  $(window).scroll(function () {
    $("#navi").stop().animate({ opacity: 1 },500, function() {
      $(this).delay(1000).animate({ opacity: 0 });
    });
  });
  </script>

  <!--[if (gte IE 6)&(lte IE 8)]>
  <script src="/js/selectivizr-min.js"></script>
  <![endif]-->
  <!-- HTML5 IEハック IE7までOK -->
  <!--[if lt IE 9]>
  <script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE9.js"></script>
  <script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/ie7-squish.js"></script>
  <![endif]-->


<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-59451323-1', 'auto');
  ga('send', 'pageview');

</script>

<script>
//pagetop
$(function() {
    var topBtn = $('.footer_link_box');
    topBtn.hide();
    //スクロールが100に達したらボタン表示
    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
        //ボタンの表示方法
            topBtn.fadeIn();
        } else {
        //ボタンの非表示方法
            topBtn.fadeOut();
        }
    });

});
</script>


</head>
<body>

  <div id="wrapper">

    <div id="h_wrapper">
      <header id="PAGE_TOP">
        <h1><img src="img/header_logo.png" width="249" height="45" alt="表参道スキンクリニック　Omotesando Skin Clinic"></h1>
        <p><img src="img/header_tel.png" width="320" height="auto" alt="0120-334-270 受付時間11:00～20:00・日祝10:30～18:00/水曜休診" class="sp_none"></p>
        <ul class="cf">
          <li class="sp_right"><a href="#section09"><img src="img/header_btn01_off.gif" width="249" height="40" alt="無料カウンセリングのご予約" class="sp_none">
            <img src="img/header_btn01_sp2.gif" width="100%" alt="無料カウンセリングのご予約" class="pc_none">
          </a>

          </li>
          <li class="sp_ab"><a href="#section08"><img src="img/header_btn02_off.gif" width="112" height="40" alt="アクセス" class="sp_none"><img src="img/header_btn02_sp.gif" width="100%" alt="アクセス" class="pc_none"></a></li>
  <li class="sp_ab1"><a href="tel:0120-334-270" onclick="ga('send', 'event', 'click', 'tel', 'omotesando')"><img src="img/header_btn02_sp3.gif" width="100%" alt="表参道院" class="pc_none"></a></li>
    <!-- <li class="sp_ab1"><a href="tel:0120-229-239" onclick="ga('send', 'event', 'click', 'tel', 'ginza')"><img src="img/header_ginza_sp.png" width="100%" alt="銀座院" class="pc_none"></a></li> -->

        </ul>
      </header>
    </div>

    <?php if(basename($_SERVER['REQUEST_URI']) == 'comp.html') : ?>
      <?php include('comp.php'); ?>
    <?php elseif($_POST['mode'] == 'conf' && !$error) : ?>
      <?php include('conf.php'); ?>
    <?php else : ?>
      <?php include('edit_wy.php'); ?>
      <?php if($error) : ?>
        <script type="text/javascript">
        window.location.hash = 'section09';
        </script>
      <?php endif; ?>
    <?php endif; ?>

    <div id="f_wrapper">
      <footer id="f_cnt">
        <p class="copy">Copyright &copy; TOKYO COSMETIC SERGERY . All Rights Reserved.</p>
      </footer>
    </div>


  </div>
  <script src="./js/custom.js"></script>






<script id="tagjs" type="text/javascript">
  (function () {
    var tagjs = document.createElement("script");
    var s = document.getElementsByTagName("script")[0];
    tagjs.async = true;
    tagjs.src = "//s.yjtag.jp/tag.js#site=dMbVkCo";
    s.parentNode.insertBefore(tagjs, s);
  }());
</script>
<noscript>
  <iframe src="//b.yjtag.jp/iframe?c=dMbVkCo" width="1" height="1" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
</noscript>

<!-- <div class="footer_link_box sp_none">
  <div class="footer_bg">
<img src="img/footer_fix.png" width="1000" height="auto" alt="フッターリンク" class="sp_none">
<p class=" footer_btn"><a href="#section09"><img src="img/footer_fix_btn.png" alt="フッターbtn"></a></p>
  </div>
</div> -->
<div class="fotter_link_sp">
<ul>
    <li>  <a href="tel:0120334270" onclick="ga('send', 'event', 'click', 'tel', 'omotesando')"><img src="img/icon_tel_omotesando.png" alt="表参道"></a></li>
    <!-- <li><a href="tel:0120229239" onclick="ga('send', 'event', 'click', 'tel', 'ginza')"><img src="img/ico_tel_ginza.gif" alt="銀座院"></a></li> -->
  <li><a href="#section09"><img src="img/footer_fix_btn.png" alt="フッターbtn"></a></li>
</ul>
</div>
</body>
</html>
