// form check
function chkNull(obj,msgObj){
	var val=obj.val();
	if(val=="" || val==null){
		msgObj.addClass('db');	
	}	else{
		msgObj.removeClass('db');	
	}
}
function chkNull_tel(){
		var telFlg=0;
		for(var i=0;i<3;i++){
			if($(".tel .mst").eq(i).val()=="" ||$(".tel .mst").eq(i).val()==null){
				telFlg=1;
			}
		}
		if(telFlg==1){
			$(".tel .error").addClass('db');
		}else{
			$(".tel .error").removeClass('db');	
		}
}


$(document).ready(function() {
	$(".name .mst").each(function(index, element) {		
		$(this).focusout(function(e) {
				chkNull($(this),$(".name .error").eq(index));		
    })    
  });

	$(".tel .mst").last().focusout(function(e) {
				chkNull_tel();					
    });

	$(".mail .mst").each(function(index, element) {		
		$(this).focusout(function(e) {
				chkNull($(this),$(".mail .error").eq(index));				
    })    
  });
	
	$("#submit").click(function(e) {

			$(".name .mst").each(function(index, element) {		
					chkNull($(this),$(".name .error").eq(index));		
			});
			
			chkNull_tel();
	
			$(".mail .mst").each(function(index, element) {		
					chkNull($(this),$(".mail .error").eq(index));				
			});
	
			if($(".error").hasClass('db')){
				$('html,body').animate({scrollTop:$("#form").offset().top}, 800);
				return false;
			}else	{
				location.href='confirm.html';
			}
		

  });

});
