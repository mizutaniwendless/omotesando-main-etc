/*
 * COMMON FUNCTIONS
 */

// header gnav on 
$(document).ready(function() {
	
	$(".gnav_btn").click(function() {
		$(".menu").slideToggle("normal");
		$("#mask").slideToggle("slow");
	});
	
	$(".backTopBlock a").click(function(){
		$('body,html').animate({scrollTop:0},800);
		return false;
	});
	$('.slideto a').click(function(){
		var target = $(this).attr('href');
		$('html,body').animate({scrollTop:$(target).offset().top}, 800);
		return false;
	}); 	
});

