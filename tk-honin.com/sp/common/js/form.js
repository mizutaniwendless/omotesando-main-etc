// form check
function chkNull(obj,msgObj){
	var val=obj.val();
	if(val=="" || val==null){
		msgObj.addClass('db');	
	}	else{
		msgObj.removeClass('db');	
	}
}
function chkNull_tel(){
		var telFlg=0;
		for(var i=0;i<1;i++){
			if($(".tel .mst").eq(i).val()=="" ||$(".tel .mst").eq(i).val()==null){
				telFlg=1;
			}
		}
		if(telFlg==1){
			$(".tel .error").addClass('db');
		}else{
			$(".tel .error").removeClass('db');	
		}
}

function chkNull_parts(){
		var selected = false;
        $('[name^="parts"]').each(function(){
			if ($(this).prop('checked')) {
                $(this).parent().find('.NFCheck').addClass('NFh');
				selected = true;
			} else {
                $(this).parent().find('.NFCheck').removeClass('NFh');
            }
        });
		if (! selected) {
			$(".checkchoice .error").addClass('db');
		} else {
			$(".checkchoice .error").removeClass('db');	
		}
}


$(document).ready(function() {
	$(".name .mst").each(function(index, element) {		
		$(this).focusout(function(e) {
				chkNull($(this),$(".name .error").eq(index));		
    })    
  });

    $(".age .mst").each(function(index, element) {		
        $(this).focusout(function(e) {
            chkNull($(this), $(".age .error").eq(index));				
        })    
    });

    $('[name^="parts"]').click(function() {		
        chkNull_parts();				
    });

	$(".tel .mst").last().focusout(function(e) {
				chkNull_tel();					
    });

	$(".mail .mst").each(function(index, element) {		
		$(this).focusout(function(e) {
				chkNull($(this),$(".mail .error").eq(index));				
    })    
  });

  $(".postalcode .mst").each(function(index, element) {		
		$(this).focusout(function(e) {
				chkNull($(this),$(".postalcode .error").eq(index));				
    })    
  });
	
	$("#submit").click(function(e) {

			$(".name .mst").each(function(index, element) {		
					chkNull($(this),$(".name .error").eq(index));		
			});
			
            $(".age .mst").each(function(index, element) {		
                chkNull($(this), $(".age .error").eq(index));				
            });

            chkNull_parts();				

			chkNull_tel();
	
			$(".mail .mst").each(function(index, element) {		
					chkNull($(this),$(".mail .error").eq(index));				
			});
	
			if($(".error").hasClass('db')){
				$('html,body').animate({scrollTop:$("#form").offset().top}, 800);
				return false;
			}else	{
				if(location.href.indexOf(".html") >= 0){
					location.href='confirm.html';
				}else{
					return true;
				}
			}
		

  });
  
  var sample_text = "例）シミとしわが目立つようになったので、レーザー治療を考えています。肌が弱い人でもレーザー治療は可能でしょうか？";
	$("textarea.sampletext").val(sample_text);
	$("textarea.sampletext").css('color', '#988e88');
	$("textarea.sampletext").focus(function() {
		if($(this).val().indexOf(sample_text) >= 0){
			$(this).css('color', '#574134');
			$(this).val('');
			$("textarea.sampletext").blur(function() {
				if($(this).val() == '') {
					$(this).val(sample_text);
					$(this).css('color', '#988e88');
				}
			});
		}
	});

});
