/*
 * COMMON FUNCTIONS
 */

// header gnav on 
$(document).ready(function() {
	
	$(".gnav_btn").click(function() {
		$(".menu").slideToggle("normal");
		$("#mask").slideToggle("normal");
	});
	
	$(".backTopBlock a").click(function(){
		$('body,html').animate({scrollTop:0},800);
		return false;
	});
	/*$('.slideto a').click(function(){
	*	var target = $(this).attr('href');
	*	$('html,body').animate({scrollTop:$(target).offset().top}, 800);
	*	return false;
	*}); 	
	*/
	
});


$(function () {
    var headerHight = 90; //ヘッダの高さ
   $('.slideto a').click(function(){
        var href= $(this).attr("href");
        var target = $(href == "#" || href == "" ? 'html' : href);
        var position = target.offset().top-headerHight; //ヘッダの高さ分位置をずらす
        $("html, body").animate({scrollTop:position}, 550, "swing");
        return false;
    });
});