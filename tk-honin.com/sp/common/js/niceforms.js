/*#############################################################
Name: Niceforms
Version: 2.0
Author: Lucian Slatineanu
URL: http://www.emblematiq.com/projects/niceforms/

Feel free to use and modify but please keep this copyright intact.
#################################################################*/

//Theme Variables - edit these to match your theme
//var imagesPath = "/cmn/img/";

//Global Variables
var NF = new Array();
var isIE = false;
var resizeTest = 1;

//Initialization function
function NFInit() {
	try {
		document.execCommand('BackgroundImageCache', false, true);
	} catch(e) {}
	if(!document.getElementById) {return false;}
	//alert("click me first");
	NFDo('start');
}
function NFDo(what) {
	var niceforms = document.getElementsByTagName('form');
	var identifier = new RegExp('(^| )'+'niceform'+'( |$)');
	if (/MSIE (\d+\.\d+);/.test(navigator.userAgent)) {
		var ieversion=new Number(RegExp.$1);
		if(ieversion < 7) {return false;} //exit script if IE6
		isIE = true;
	}
	for(var q = 0; q < niceforms.length; q++) {if(identifier.test(niceforms[q].className)) {
		if(what == "start") { //Load Niceforms
			NF[q] = new niceform(niceforms[q]);
			niceforms[q].start();
		}
		else { //Unload Niceforms
			niceforms[q].unload();
			NF[q] = "";
		}
	}}
}
function NFFix() {
	NFDo('stop');
	NFDo('start');
}
function niceform(nf) {
	nf._inputRadio = new Array(); 
	nf._inputCheck = new Array(); 
	nf.add_inputRadio = function(obj) {this._inputRadio[this._inputRadio.length] = obj; inputRadio(obj);}
	nf.add_inputCheck = function(obj) {this._inputCheck[this._inputCheck.length] = obj; inputCheck(obj);}
	nf.start = function() {
		//Separate and assign elements
		var allInputs = this.getElementsByTagName('input');
		for(var w = 0; w < allInputs.length; w++) {
			switch(allInputs[w].type) {
				case "radio": {this.add_inputRadio(allInputs[w]); break;}
				case "checkbox": {this.add_inputCheck(allInputs[w]); break;}
			}
		}
		for(w = 0; w < this._inputRadio.length; w++) {this._inputRadio[w].init();}
		for(w = 0; w < this._inputCheck.length; w++) {this._inputCheck[w].init();}
	}
	nf.unload = function() {
		//Stop
		for(w = 0; w < this._inputRadio.length; w++) {this._inputRadio[w].unload();}
		for(w = 0; w < this._inputCheck.length; w++) {this._inputCheck[w].unload();}
	}
}
function inputRadio(el) { //extent Radio buttons
	el.oldClassName = el.className;
	el.dummy = document.createElement('div');
	if(el.checked) {el.dummy.className = "NFRadio NFh";}
	else {el.dummy.className = "NFRadio";}
	el.dummy.ref = el;

	el.dummy.onclick = function() {
		if(!this.ref.checked) {
			var siblings = getInputsByName(this.ref.name);
			for(var q = 0; q < siblings.length; q++) {
				siblings[q].checked = false;
				siblings[q].dummy.className = "NFRadio";
			}
			this.ref.checked = true;
			this.className = "NFRadio NFh";
		}
	}
	el.onclick = function() {
		if(this.checked) {
			var siblings = getInputsByName(this.name);
			for(var q = 0; q < siblings.length; q++) {
				siblings[q].dummy.className = "NFRadio";
			}
			this.dummy.className = "NFRadio NFh";
		}
	}
	el.onfocus = function() {this.dummy.className += " NFfocused";}
	el.onblur = function() {this.dummy.className = this.dummy.className.replace(/ NFfocused/g, "");}
	el.init = function() {
		this.parentNode.insertBefore(this.dummy, this);
		el.className = "NFhidden";
	}
	el.unload = function() {
		this.parentNode.removeChild(this.dummy);
		this.className = this.oldClassName;
	}
}
function inputCheck(el) { //extend Checkboxes
	el.oldClassName = el.className;
//	el.dummy = document.createElement('img');
//	el.dummy.src = imagesPath + "0.png";
	el.dummy = document.createElement('div');
	if(el.checked) {
		el.dummy.className = "NFCheck NFh";
	}	else {
		el.dummy.className = "NFCheck";
	}
	el.dummy.ref = el;
	
	el.dummy.onclick = function() {
		if(this.ref.checked == false) {
			this.checked = true;
			this.className = "NFCheck NFh";
		}else {
			this.checked = false;
			this.className = "NFCheck";
		}
	}
/*
	el.onclick = function() {
		if(this.checked) {
			this.dummy.className = "NFCheck NFh";alert("el-1");
		}else {
			this.dummy.className = "NFCheck NFh";alert(this.checked+"==el-2");
		}
	}
*/
	el.onfocus = function() {
	this.dummy.className += " NFfocused";
	//alert("onfocus-1");
	}
	el.onblur = function() {
	this.dummy.className = this.dummy.className.replace(/ NFfocused/g, "");
	//alert("onblur-1");
	}
	el.init = function() {
		this.parentNode.insertBefore(this.dummy, this);
		el.className = "NFhidden";
	} 
	el.unload = function() {
		this.parentNode.removeChild(this.dummy);
		this.className = this.oldClassName;
	}
}

//Get Position
function findPosY(obj) {
	var posTop = 0;
	do {posTop += obj.offsetTop;} while (obj = obj.offsetParent);
	return posTop;
}
function findPosX(obj) {
	var posLeft = 0;
	do {posLeft += obj.offsetLeft;} while (obj = obj.offsetParent);
	return posLeft;
}
//Get Siblings
function getInputsByName(name) {
	var inputs = document.getElementsByTagName("input");
	var w = 0; var results = new Array();
	for(var q = 0; q < inputs.length; q++) {if(inputs[q].name == name) {results[w] = inputs[q]; ++w;}}
	return results;
}

//Add events
var existingLoadEvent = window.onload || function () {};
var existingResizeEvent = window.onresize || function() {};
window.onload = function () {
    existingLoadEvent();
    NFInit();

}
window.onresize = function() {
	if(resizeTest != document.documentElement.clientHeight) {
		existingResizeEvent();
		NFFix();
	}
	resizeTest = document.documentElement.clientHeight;
}